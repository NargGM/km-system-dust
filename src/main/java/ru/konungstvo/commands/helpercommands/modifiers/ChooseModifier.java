package ru.konungstvo.commands.helpercommands.modifiers;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.equipment.BodyPart;
import ru.konungstvo.commands.helpercommands.player.turn.ToRoot;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;

public class ChooseModifier extends CommandBase {
    private static String START_MESSAGE = "Выберите %s!\n";

    @Override
    public String getName() {
        return "choosemodifier";
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        String modifierType = args[0];
        String oldContainer = "";
        if(args.length > 1) oldContainer = args[1];

        String frmt = "";
        if (modifierType.equals("dice")) {
            frmt = "модификатор уровня навыка";
        } else if (modifierType.equals("percent")) {
            frmt = "модификатор процентного бонуса";

        } else if (modifierType.equals("damage")) {
            frmt = "урон";
        } else if (modifierType.equals("bodypart")) {
            frmt = "часть тела";
        } else if (modifierType.equals("cover")) {
            frmt = "укрытие";
        }

        // REMOVE PREVIOUS PANEL
        ClickContainerMessage remove = new ClickContainerMessage("ModifiersButton", true);
        KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));


        // CREATE CLICK PANEL
        Message message = new Message(String.format(START_MESSAGE, frmt), ChatColor.COMBAT);
        if (modifierType.equals("bodypart")) {
            for (BodyPart bodyPart : BodyPart.values()) {
                MessageComponent mes = new MessageComponent("[" + bodyPart.getName() + "] ", ChatColor.BLUE);
                mes.setClickCommand(String.format("/%s %s %s %s %s", SetModifier.NAME, modifierType, bodyPart.getNum(), "ModifiersButton", oldContainer));
                message.addComponent(mes);
            }
        } else {
            int start = -5;
            int end = 5;
            if (modifierType.equals("damage")) {
                start = -10;
                end = 10;
            }
//            if (modifierType.equals("percent")) {
//                start = -99;
//                end = 99;
//            }
//            if (modifierType.equals("cover")) {
//                start = 0;
//                end = 1;
//            }
            for (int i = start; i <= end; i++) {
                String iStr = String.valueOf(i);
                if (i > 0) iStr = "+" + iStr;
                MessageComponent mes2 = new MessageComponent("[" + iStr + "] ", ChatColor.BLUE);
                mes2.setClickCommand(String.format("/%s %s %s %s %s", SetModifier.NAME, modifierType, i, "ModifiersButton", oldContainer));
                message.addComponent(mes2);
            }
        }

        MessageComponent toRoot = new MessageComponent("[Назад]", ChatColor.GRAY);
        //toRoot.setBold(true);
        toRoot.setClickCommand("/" + ToRoot.NAME + " ChooseModifier" + " ModifiersButton " + oldContainer);
        message.addComponent(toRoot);

        // SEND PACKET TO CLIENT
        Helpers.sendClickContainerToClient(message, "ChooseModifier", (EntityPlayerMP) sender);
    }
}
