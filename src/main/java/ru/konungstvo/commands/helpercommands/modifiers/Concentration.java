package ru.konungstvo.commands.helpercommands.modifiers;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.Skill;
import ru.konungstvo.combat.StatusEnd;
import ru.konungstvo.combat.StatusType;
import ru.konungstvo.combat.equipment.BodyPart;
import ru.konungstvo.combat.movement.ChargeHistory;
import ru.konungstvo.combat.movement.MovementHistory;
import ru.konungstvo.commands.executor.NextExecutor;
import ru.konungstvo.commands.helpercommands.player.turn.ToRoot;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.player.Player;

import java.util.Arrays;
import java.util.List;

public class Concentration extends CommandBase {
    private static String START_MESSAGE = "Выберите %s!\n";

    @Override
    public String getName() {
        return "concentration";
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        //String modifierType = args[0];
        String oldContainer = "";
        if(args.length > 0) oldContainer = args[0];
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player subordinate = DataHolder.inst().getPlayer(sender.getName());
        if (subordinate.getSubordinate() != null) subordinate = subordinate.getSubordinate();

        System.out.println(String.join(" ", Arrays.copyOfRange(args, 0, args.length)));
        // REMOVE PREVIOUS PANEL

        if (subordinate.courage < 1 && !DataHolder.inst().isNpc(subordinate.getName())) {
            TextComponentString error = new TextComponentString("Недостаточно куража!");
            error.getStyle().setColor(TextFormatting.RED);
            sender.sendMessage(error);
            return;
        }

        if ((args.length > 1 && !args[1].equals("ready")) || args.length < 2) {
            if (args.length > 1 && args[1].equals("clear")) {
                subordinate.removeStatusEffect(StatusType.COURAGE);
                player.performCommand("/modifiersbutton " + oldContainer);
                return;
            }
            ClickContainerMessage remove = new ClickContainerMessage("ModifiersButton", true);
            System.out.println("test2");
            KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
        } else {
            String skill = String.join(" ", Arrays.copyOfRange(args, 2, args.length));
//            ClickContainerMessage remove = new ClickContainerMessage("Concentration", true);
//            KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
//            System.out.println(subordinate.getAttachedCombatID() != -1);
//            System.out.println(DataHolder.inst().getCombat(subordinate.getAttachedCombatID()) != null);
            //System.out.println(DataHolder.inst().getCombat(subordinate.getAttachedCombatID()).getReactionList().getQueue().getNext() == subordinate);
            if (DataHolder.inst().getCombatForPlayer(subordinate.getName()) != null && DataHolder.inst().getCombatForPlayer(subordinate.getName()).getReactionList().getQueue().getNext() == subordinate) {
                subordinate.addStatusEffect("кураж", StatusEnd.TURN_END, 1, StatusType.COURAGE, skill);
            } else {
                subordinate.addStatusEffect("кураж", StatusEnd.TURN_END, 1, StatusType.COURAGE, skill);
            }
            player.performCommand("/" + ToRoot.NAME + " Concentration" + " ModifiersButton " + oldContainer);
            return;
        }



        // CREATE CLICK PANEL
        Message message = new Message(String.format(START_MESSAGE, "навык, на который хотите использовать кураж"), ChatColor.COMBAT);
        List<Skill> skills = subordinate.getSkills();

        for (Skill sk : skills) {
            String skillStr = Character.toUpperCase(sk.getName().charAt(0)) + sk.getName().substring(1);
            MessageComponent mes = new MessageComponent("[" + skillStr + "] ", ChatColor.BLUE);
            mes.setClickCommand("/" + "concentration " + oldContainer + " ready " + sk.getName());
            message.addComponent(mes);
        }
        MessageComponent mes = new MessageComponent("[Скорость] ", ChatColor.BLUE);
        mes.setClickCommand("/" + "concentration " + oldContainer  + " ready " + "атлетика");
        message.addComponent(mes);


        MessageComponent toRoot = new MessageComponent("[Назад]", ChatColor.GRAY);
        //toRoot.setBold(true);
        toRoot.setClickCommand("/" + ToRoot.NAME + " Concentration" + " ModifiersButton " + oldContainer);
        message.addComponent(toRoot);

        // SEND PACKET TO CLIENT
        Helpers.sendClickContainerToClient(message, "Concentration", (EntityPlayerMP) sender, subordinate);
//        String json = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(message));
//        ClickContainerMessage result = new ClickContainerMessage(, json);
//        KMPacketHandler.INSTANCE.sendTo(result, (EntityPlayerMP) sender);


    }
}
