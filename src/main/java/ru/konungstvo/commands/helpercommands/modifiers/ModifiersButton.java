package ru.konungstvo.commands.helpercommands.modifiers;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.StatusType;
import ru.konungstvo.commands.executor.CoverExecutor;
import ru.konungstvo.commands.executor.CycleTagExecutor;
import ru.konungstvo.commands.executor.MagicExecutor;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.player.turn.ToRoot;
import ru.konungstvo.commands.helpercommands.player.turn.ToolPanel;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.ModifiersState;
import ru.konungstvo.player.Player;

public class ModifiersButton  extends CommandBase {
    private static String START_MESSAGE = "Выберите, что хотите модифицировать!\n";


    @Override
    public String getName() {
        return "modifiersbutton";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }
    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        sender.sendMessage(new TextComponentString("На реконструкции"));
        if (false) return;
        String oldContainer = "";
        if (args.length > 0) {
            oldContainer = args[0];
            ClickContainer.deleteContainer(oldContainer, getCommandSenderAsPlayer(sender));
        }
        // GET PLAYER OR NPC
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player subordinate = player;
        if (player.getSubordinate() != null) {
            subordinate = player.getSubordinate();
        }

        EntityLivingBase forgePlayer = player.getEntity();

        System.out.println(player.getModifiersState().toString());

        // CREATE CLICK PANEL
        Message message = new Message(START_MESSAGE, ChatColor.COMBAT);
        MessageComponent mes = new MessageComponent("[Уровень навыка] ", ChatColor.BLUE);
        mes.setClickCommand("/choosemodifier dice " + oldContainer);
        message.addComponent(mes);

//        MessageComponent mes1 = new MessageComponent("[Процентный бонус] ", ChatColor.BLUE);
//        mes1.setClickCommand("/choosemodifier percent");
//        message.addComponent(mes1);

        MessageComponent mes2 = new MessageComponent("[Урон] ", ChatColor.BLUE);
        mes2.setClickCommand("/choosemodifier damage " + oldContainer);
        message.addComponent(mes2);

        MessageComponent mes3 = new MessageComponent("[Точечная] ", ChatColor.BLUE);
        mes3.setClickCommand("/choosemodifier bodypart " + oldContainer);
        message.addComponent(mes3);

        if (player.courage < 1) {
            MessageComponent mes33 = new MessageComponent("[Кураж] ", ChatColor.GRAY);
//            mes33.setClickCommand("/concentration " + oldContainer);
            message.addComponent(mes33);
        } else if (player.hasStatusEffect(StatusType.COURAGE)) {
            MessageComponent mes33 = new MessageComponent("[Убрать кураж] ", ChatColor.RED);
            mes33.setClickCommand("/concentration " + oldContainer + " clear");
            message.addComponent(mes33);
        } else {
            MessageComponent mes33 = new MessageComponent("[Кураж] ", ChatColor.OOC);
            mes33.setClickCommand("/concentration " + oldContainer);
            message.addComponent(mes33);
        }

        MessageComponent mes50 = new MessageComponent("[Осмотр] ", ChatColor.BLUE);
        mes50.setClickCommand("/examine " + "%IGNORE% " + "ModifiersButton " + oldContainer);
        mes50.setHoverText("[Осмотреть персонажей в пределах 16 блоков]", TextFormatting.GRAY);
        message.addComponent(mes50);

        if (player.getModifiersState().getModifier(ModifiersState.serial) != null && player.getModifiersState().getModifier(ModifiersState.serial) == 1) {
            MessageComponent mes4 = new MessageComponent("[Выкл. серийный огонь] ", ChatColor.DARK_BLUE);
            mes4.setClickCommand("/" + SetModifier.NAME + " serial 0" + " ModifiersButton " + oldContainer);
            message.addComponent(mes4);
        } else {
            MessageComponent mes4 = new MessageComponent("[Вкл. серийный огонь] ", ChatColor.BLUE);
            mes4.setClickCommand("/" + SetModifier.NAME + " serial 1" + " ModifiersButton " + oldContainer);
            message.addComponent(mes4);
        }

        if (player.getModifiersState().getModifier(ModifiersState.serial) != null && player.getModifiersState().getModifier(ModifiersState.serial) == 1) {
            if (player.getModifiersState().getModifier(ModifiersState.suppressing) != null && player.getModifiersState().getModifier(ModifiersState.suppressing) == 1) {
                MessageComponent mes44 = new MessageComponent("[Выкл. огонь на подавление] ", ChatColor.DARK_BLUE);
                mes44.setClickCommand("/" + SetModifier.NAME + " suppressing 0" + " ModifiersButton " + oldContainer);
                message.addComponent(mes44);
            } else {
                MessageComponent mes44 = new MessageComponent("[Вкл. огонь на подавление] ", ChatColor.BLUE);
                mes44.setClickCommand("/" + SetModifier.NAME + " suppressing 1" + " ModifiersButton " + oldContainer);
                message.addComponent(mes44);
            }
        }

        if (player.getModifiersState().getModifier(ModifiersState.stationary) != null && player.getModifiersState().getModifier(ModifiersState.stationary) == 1) {
            MessageComponent mes5 = new MessageComponent("[Выкл. стационарность] ", ChatColor.DARK_BLUE);
            mes5.setClickCommand("/" + SetModifier.NAME + " stationary 0" + " ModifiersButton " + oldContainer);
            message.addComponent(mes5);
        } else {
            MessageComponent mes5 = new MessageComponent("[Вкл. стационарность] ", ChatColor.BLUE);
            mes5.setClickCommand("/" + SetModifier.NAME + " stationary 1" + " ModifiersButton " + oldContainer);
            message.addComponent(mes5);
        }

        if (player.getModifiersState().getModifier(ModifiersState.cover) != null && player.getModifiersState().getModifier(ModifiersState.cover) == 1) {
            MessageComponent mes6 = new MessageComponent("[Выкл. укрытие] ", ChatColor.DARK_BLUE);
            mes6.setClickCommand("/" + SetModifier.NAME + " cover 0" + " ModifiersButton " + oldContainer);
            message.addComponent(mes6);
        } else {
            MessageComponent mes6 = new MessageComponent("[Вкл. укрытие] ", ChatColor.BLUE);
            mes6.setClickCommand("/" + SetModifier.NAME + " cover 1" + " ModifiersButton " + oldContainer);
            message.addComponent(mes6);
        }

        MessageComponent mes666 = new MessageComponent("[Полное укрытие] ", ChatColor.BLUE);
        mes666.setClickCommand("/" + CoverExecutor.NAME + " %IGNORE% ModifiersButton " + oldContainer);
        message.addComponent(mes666);

        if (player.getModifiersState().getModifier(ModifiersState.optic) != null && player.getModifiersState().getModifier(ModifiersState.optic) == 1) {
            MessageComponent mes7 = new MessageComponent("[Выкл. прицеливание] ", ChatColor.DARK_BLUE);
            mes7.setClickCommand("/" + SetModifier.NAME + " optic 0" + " ModifiersButton " + oldContainer);
            message.addComponent(mes7);
        } else {
            MessageComponent mes7 = new MessageComponent("[Вкл. прицеливание] ", ChatColor.BLUE);
            mes7.setClickCommand("/" + SetModifier.NAME + " optic 1" + " ModifiersButton " + oldContainer);
            message.addComponent(mes7);
        }

        if (player.getModifiersState().getModifier(ModifiersState.bipod) != null && player.getModifiersState().getModifier(ModifiersState.bipod) == 1) {
            MessageComponent mes8 = new MessageComponent("[Выкл. сошки] ", ChatColor.DARK_BLUE);
            mes8.setClickCommand("/" + SetModifier.NAME + " bipod 0" + " ModifiersButton " + oldContainer);
            message.addComponent(mes8);
        } else {
            MessageComponent mes8 = new MessageComponent("[Вкл. сошки] ", ChatColor.BLUE);
            mes8.setClickCommand("/" + SetModifier.NAME + " bipod 1" + " ModifiersButton " + oldContainer);
            mes8.setHoverText("Не забывайте, что установка сошек на поверхность занимает ход!", TextFormatting.RED);
            message.addComponent(mes8);
        }

        if (player.getModifiersState().getModifier(ModifiersState.stunning) != null && player.getModifiersState().getModifier(ModifiersState.stunning) == 1) {
            MessageComponent mes9 = new MessageComponent("[Выкл. оглушение] ", ChatColor.DARK_BLUE);
            mes9.setClickCommand("/" + SetModifier.NAME + " stunning 0" + " ModifiersButton " + oldContainer);
            message.addComponent(mes9);
        } else {
            MessageComponent mes9 = new MessageComponent("[Вкл. оглушение] ", ChatColor.BLUE);
            mes9.setClickCommand("/" + SetModifier.NAME + " stunning 1" + " ModifiersButton " + oldContainer);
            mes9.setHoverText("Модификатор неподходящего оружия снизится автоматически.", TextFormatting.DARK_GREEN);
            message.addComponent(mes9);
        }

        if (player.getModifiersState().getModifier(ModifiersState.capture) != null && player.getModifiersState().getModifier(ModifiersState.capture) == 1) {
            MessageComponent mes10 = new MessageComponent("[Выкл. призыв к сдаче] ", ChatColor.DARK_BLUE);
            mes10.setClickCommand("/" + SetModifier.NAME + " capture 0" + " ModifiersButton " + oldContainer);
            message.addComponent(mes10);
        } else {
            MessageComponent mes10 = new MessageComponent("[Вкл. призыв к сдаче] ", ChatColor.BLUE);
            mes10.setClickCommand("/" + SetModifier.NAME + " capture 1" + " ModifiersButton " + oldContainer);
            message.addComponent(mes10);
        }

        ItemStack leftHand;
        ItemStack rightHand;
        try {
            leftHand = forgePlayer.getHeldItemOffhand();
            rightHand = forgePlayer.getHeldItemMainhand();
            if (!leftHand.isEmpty() || !rightHand.isEmpty()) {
                WeaponTagsHandler leftWeapon = new WeaponTagsHandler(leftHand);
                WeaponTagsHandler rightWeapon = new WeaponTagsHandler(rightHand);
                if((!leftHand.isEmpty() && leftWeapon.isBigShield()) || (!rightHand.isEmpty() && rightWeapon.isBigShield())) {
                    if (player.hasDefensiveStance()) {
                        MessageComponent mes11 = new MessageComponent("[Выкл. глухую оборону] ", ChatColor.DARK_BLUE);
                        mes11.setClickCommand("/" + "defensivestance" + " turnoff ModifiersButton " + oldContainer);
                        message.addComponent(mes11);
                        MessageComponent mes111 = new MessageComponent("[Продолжать глухую оборону] ", ChatColor.BLUE);
                        mes111.setHoverText("Заканчивает ход. Во время глухой обороны нельзя бегать и действовать.", TextFormatting.RED);
                        mes111.setClickCommand("/defensivestance" + " ModifiersButton " + oldContainer);
                        message.addComponent(mes111);
                    } else {
                        MessageComponent mes11 = new MessageComponent("[Вкл. глухую оборону] ", ChatColor.BLUE);
                        mes11.setHoverText("Заканчивает ход. Во время глухой обороны нельзя бегать и действовать.", TextFormatting.RED);
                        mes11.setClickCommand("/defensivestance" + " ModifiersButton " + oldContainer);
                        message.addComponent(mes11);
                    }
                }
            }
        } catch (NullPointerException ignored) {

        }

        if (DataHolder.inst().getDuelForDefender(player.getName()) != null) {
            if (player.getModifiersState().getModifier(ModifiersState.noriposte) != null && player.getModifiersState().getModifier(ModifiersState.noriposte) == 1) {
                MessageComponent mes12 = new MessageComponent("[Использовать рипост снова] ", ChatColor.DARK_BLUE);
                mes12.setClickCommand("/" + SetModifier.NAME + " noriposte 0" + " ModifiersButton " + oldContainer);
                message.addComponent(mes12);
            } else {
                MessageComponent mes12 = new MessageComponent("[Не использовать рипост] ", ChatColor.BLUE);
                mes12.setClickCommand("/" + SetModifier.NAME + " noriposte 1" + " ModifiersButton " + oldContainer);
                message.addComponent(mes12);
            }
        }

        if (DataHolder.inst().isNpc(player.getName())) {
            MessageComponent mes13 = new MessageComponent("[Переключить тег оружия] ", ChatColor.BLUE);
            mes13.setClickCommand("/" + CycleTagExecutor.NAME + " ModifiersButton " + oldContainer);
            EntityLivingBase elb = DataHolder.inst().getNpcEntity(player.getName());
            ItemStack is = elb.getHeldItemMainhand();
            if(is != null && !is.isEmpty()) {
                WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(is);
                if (weaponTagsHandler.hasWeaponTags()) {
                    mes13.setHoverText("Текущий тег: " + weaponTagsHandler.getDefaultWeaponName(), TextFormatting.GRAY);
                    message.addComponent(mes13);
                }
            }
        }

        MessageComponent mes14 = new MessageComponent("[Магия] ", ChatColor.BLUE);
        mes14.setClickCommand("/" + MagicExecutor.NAME + " ModifiersButton " + oldContainer);
        message.addComponent(mes14);

        MessageComponent toRoot = new MessageComponent("[Назад] ", ChatColor.GRAY);
        //toRoot.setBold(true);
        toRoot.setClickCommand("/" + ToRoot.NAME + " ModifiersButton " + oldContainer);
        message.addComponent(toRoot);

        // SEND PACKET TO CLIENT
        Helpers.sendClickContainerToClient(message, "ModifiersButton", (EntityPlayerMP) sender, subordinate);
    }
}

