package ru.konungstvo.commands.helpercommands.gm.npcadder;

import com.mongodb.client.MongoCursor;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.server.permission.PermissionAPI;
import org.bson.Document;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.bridge.MongoBridge;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ChooseNpcCommand extends CommandBase {
    public static final String NAME = "choosenpc";
    public static final String START_MESSAGE = "Выберите набор статов для %s: \n";
    public static final String END_MESSAGE = "Непись %s добавлен в систему со скиллсетом %s.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }


    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permission.GM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        // choosenpc Суровый разбойник 82.1 34.0 -100.4
        //           0       1         2    3    4       length = 5

        // npc add Бандит Суровый разбойник 82.1 34.0 -100.4

        Player player = DataHolder.inst().getPlayer(sender.getName());

        String coords = "";
        try {
            double pX = Double.parseDouble(args[args.length-3]);
            double pY = Double.parseDouble(args[args.length-2]);
            double pZ = Double.parseDouble(args[args.length-1]);
            coords = pX + " " + pY + " " + pZ;
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
            Message error = new Message("Ошибка! " + e.toString(), ChatColor.RED);
            player.sendMessage(error);
            return;
        }

        String customName = String.join(" ", Arrays.copyOfRange(args, 0, args.length-3));
        System.out.println(customName);
        int symbolCount = 0;

        // GENERATE CLICK PANEL
        Message result = new Message(String.format(START_MESSAGE, customName), ChatColor.COMBAT);
        String firstletter = "new";
        ChatColor chatColor = ChatColor.DARK_GREEN;
        int messageCount = 0;
        for (String stat : getListOfNpcStats()) {
            symbolCount += stat.length();

            //очень плохой код. Красит слова чтобы новая буква начиналась с нового цвета
            if (firstletter.equals("new")) {
                firstletter = stat.substring(0,1);
                chatColor = ChatColor.DARK_GREEN;
            } else if (!firstletter.equals(stat.substring(0,1))) {
                firstletter = stat.substring(0,1);
                if (chatColor == ChatColor.DARK_GREEN) {
                    chatColor = ChatColor.NICK;
                } else {
                    chatColor = ChatColor.DARK_GREEN;
                }
            }


            MessageComponent line = new MessageComponent("[" + stat + "] ", chatColor);
            line.setClickCommand("/" + NpcCommand.NAME + " add " + stat + " " + customName + " " + coords);
            result.addComponent(line);
            //иначе может быть слишком огромный пакет
            if (symbolCount > 500) {
                symbolCount = 0;
                System.out.println(result);
                // SEND PACKET TO CLIENT
                String json = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(result));
                ClickContainerMessage clickContainerMessage = new ClickContainerMessage("ChooseNpc" + messageCount, json);
                KMPacketHandler.INSTANCE.sendTo(clickContainerMessage, (EntityPlayerMP) sender);
                result = new Message(String.format("", customName), ChatColor.COMBAT);
                messageCount++;
            }
        }

        // SEND PACKET TO CLIENT
        if (result.getComponents().size() > 0) {
            System.out.println(result);
            String json = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(result));
            ClickContainerMessage clickContainerMessage = new ClickContainerMessage("ChooseNpc" + messageCount, json);
            KMPacketHandler.INSTANCE.sendTo(clickContainerMessage, (EntityPlayerMP) sender);
        }
        //DataHolder.getInstance().getPlayer(sender.getName()).sendMessage(result);

    }

    private List<String> getListOfNpcStats() {
        ArrayList<String> result = new ArrayList<>();

        try (MongoCursor<Document> cursor = MongoBridge.INSTANCE.client.getDatabase("dustrpmongo").getCollection("charsheets").find(new Document("isnpc", true)).iterator()) {
            while (cursor.hasNext()) {
                Document document = cursor.next();
                if ((boolean) document.getOrDefault("isnpc", false)) {
                    result.add((String) document.getOrDefault("character", "NO CHARACTER NAME FOR SOME REASON WTF"));
                }
            }
        }

        Collections.sort(result);
        return result;
    }
}
