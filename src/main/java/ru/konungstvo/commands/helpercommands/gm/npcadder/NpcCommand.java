package ru.konungstvo.commands.helpercommands.gm.npcadder;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.server.permission.PermissionAPI;
import org.json.simple.parser.ParseException;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.combat.Combat;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.exceptions.DataException;
import ru.konungstvo.player.NPC;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

import java.io.IOException;
import java.util.Arrays;

public class NpcCommand extends CommandBase {
    public static final String NAME = "npc";
    private static ICommandSender iCommandSender;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender iCommandSender) {
        return "";
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permission.GM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void execute(MinecraftServer minecraftServer, ICommandSender iCommandSender, String[] strings) throws CommandException {
        this.iCommandSender = iCommandSender;
        Message answer = execute(DataHolder.inst().getPlayer(iCommandSender.getName()), strings);
        DataHolder.inst().getPlayer(iCommandSender.getName()).sendMessage(answer);
        // REMOVE PREVIOUS PANEL
        ClickContainerMessage remove = new ClickContainerMessage("ChooseNpc", true);
        try {
            KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(iCommandSender));
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
        }

    }

    public static Message execute(Player executor, String... args) {
        if (args.length == 0) {
            return new Message("Тут будет хлеб");
        }

        if (args.length < 2) {
            return new Message("Ошибка! Мало аргументов!", ChatColor.RED);
        }

        System.out.println("args: " + Arrays.toString(args));

        // check for combat
        Combat combat = DataHolder.inst().getCombat(executor.getAttachedCombatID());
        if (combat == null) {
            combat = DataHolder.inst().createCombat(executor.getName());
            executor.setAttachedCombatID(combat.getId());
//            return new Message("Ошибка! У вас нет привязанного боя!", ChatColor.RED);
        }

        String stat, customName;
        int startingIndex;
        boolean react = false;
        switch (args[0]) {
            case "addr":
                react = true;
            case "add":
                // npc add Бандит Суровый разбойник 82.1 34.0 -100.4
                //     0   1      2       3         4    5     6     length=7

                // npc add Бандит Суровый  82.1 34.0 -100.4
                //     0   1      2        3     4    5     length=6


                stat = args[1];

                startingIndex = 2;
                customName = String.join(" ", Arrays.copyOfRange(args, startingIndex, args.length - 3));
                System.out.println("customName : " + customName);
                if (customName.isEmpty()) {
                    customName = stat;
                }

                NPC npc;
                try {
                    npc = combat.spawnNpc(stat, customName, !args[0].equals("spawn"));
                } catch (DataException e) {
                    return new Message("Ошибка! " + e.getMessage(), ChatColor.RED);

                }

//                npc.setLocation(BukkitCore.getBukkitPlayer(executor.getName()).getLocation());

//                double pX, pY, pZ;
//                try {
//                    pX = Double.parseDouble(args[args.length - 3]);
//                    pY = Double.parseDouble(args[args.length - 2]);
//                    pZ = Double.parseDouble(args[args.length - 1]);
//                } catch (NumberFormatException e) {
//                    return new Message("Ошибка! " + e.getMessage(), ChatColor.RED);
//                }
//
//                npc.setPosX(pX);
//                npc.setPosY(pY);
//                npc.setPosZ(pZ);
//                System.out.println("NPC location set to " + pX + ";" + pY + ";" + pZ);

                try {
                    npc.updateSet();
                    DataHolder.inst().updateWoundPyramid(npc.getName());
                } catch (IOException | ParseException e) {
                    e.printStackTrace();
                    DataHolder.inst().removeNpc(npc.getName());
                    DataHolder.inst().removePlayer(npc.getName());
                    combat.removeFighter(npc.getName());
                    return new Message(e.getCause().toString(), ChatColor.RED);
                }

                // REMOVE PREVIOUS PANEL
                ClickContainerMessage remove = new ClickContainerMessage("ChooseNpc", true);
                try {
                    KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(iCommandSender));
                } catch (PlayerNotFoundException e) {
                    e.printStackTrace();
                }

                if (react) {
                    executor.sendMessage(new Message(String.format(ChooseNpcCommand.END_MESSAGE, npc.getName(), stat), ChatColor.COMBAT));
                    DataHolder.inst().getPlayer(executor.getName()).performCommand("queue add " + npc.getName());
                    return null;
                }
                return new Message(String.format(ChooseNpcCommand.END_MESSAGE, npc.getName(), stat), ChatColor.COMBAT);
            //return new Message("Непись " + npc.getName() + " добавлен в систему. Его " + ChatColor.BDCHAT + "координаты" + ChatColor.GRAY + " там, где вы стоите.", ChatColor.GRAY);

            case "remove" :
                customName = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
                DataHolder.inst().removeNpc(customName);
                return new Message("Непись " + args[1] + " удалён из системы.", ChatColor.GRAY);

            case "show":
                customName = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
                System.out.println("customName : " + customName);

                // combat = DataHolder.getInstance().getCombatForPlayer(customName);
                Player pl = DataHolder.inst().getPlayer(customName);
                if (pl == null) {
                    return new Message("Ошибка! Непись " + customName + " не найден!", ChatColor.RED);
                }
                String response = ChatColor.COMBAT.toString() + pl.getName() + ":\n" +
                        "В бою: [" + combat.getId() + "] " + combat.getName() +
                        "\nЛокация: " + pl.getLocation().toString(); //TODO работает ли вообще?
                return new Message(response);
            case "move": //DEPRECATED
                customName = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
                System.out.println("customName : " + customName);

                pl = DataHolder.inst().getPlayer(customName);
//                pl = DataHolder.inst().getPlayer(args[1]);
                // Deprecated
                if (pl == null) {
//                    return new Message("Ошибка! Непись " + args[1] + " не найден!");
                    return new Message("Ошибка! Непись " + customName + " не найден!");
                }
//                pl.setLocation(executor.getLocation());
                //executor.performCommand("noppes npc " + customName + " home");
                return new Message("Непись " + customName + " подозван к вам.", ChatColor.GRAY);
            case "tp":
                customName = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
                System.out.println("customName : " + customName);
                Player tpNPC = DataHolder.inst().getPlayer(customName);
                if (tpNPC == null) {
                    return new Message("Ошибка! Непись " + customName + " не найден!", ChatColor.RED);
                }
                EntityLivingBase npcEntity = DataHolder.inst().getNpcEntity(customName);
                BlockPos blockPos = npcEntity.getPosition();

                DataHolder.inst().getPlayer(iCommandSender.getName()).performCommand("tpn " + blockPos.getX() + " " + blockPos.getY() + " " + blockPos.getZ());
//                ServerProxy.getForgePlayer(executor.getName()).attemptTeleport(blockPos.getX(), blockPos.getY(), blockPos.getZ());
                //  BukkitCore.getBukkitPlayer(executor.getName()).teleport(DataHolder.getInstance().getPlayer(npcName).getLocation());
                return new Message("Вы телепортированы к " + customName + ".", ChatColor.GRAY);

        }


        return null;
    }


}
