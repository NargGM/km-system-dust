package ru.konungstvo.commands.helpercommands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.control.network.PacketMessage;

public class PacketTestCommand extends CommandBase {
    @Override
    public String getName() {
        return "packet";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        MessageComponent mes = new MessageComponent("[" + "владение тестом" + "] ", ChatColor.BLUE);
        mes.setClickCommand("/selectopponent " + "владение тестом");
        Message test = new Message();
        test.addComponent(mes);
        TextComponentString textComponents = MessageGenerator.generate(test);
        TextComponentString sibl = new TextComponentString("TEST");
        sibl.getStyle().setColor(TextFormatting.RED);
        textComponents.appendSibling(sibl);

        System.out.println("Before serializing: ");
        System.out.println(textComponents);
        String result = ITextComponent.Serializer.componentToJson(textComponents);
        System.out.println("Sending packet: " + result);

        ClickContainerMessage message = new ClickContainerMessage("Duel", result);
        KMPacketHandler.INSTANCE.sendTo(message, (EntityPlayerMP) sender);
    }
}
