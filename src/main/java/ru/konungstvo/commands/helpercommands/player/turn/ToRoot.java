package ru.konungstvo.commands.helpercommands.player.turn;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import ru.konungstvo.combat.duel.DuelMaster;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.player.attack.InitAttackCommand;
import ru.konungstvo.commands.helpercommands.player.defense.SelectDefenseCommand;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.player.Player;

public class ToRoot extends CommandBase {

    public static final String NAME = "toroot";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }
    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;

    }
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        String panelToRemove = args[0];
        String rootPanel = "";
        String arg = "";
        if (args.length > 1) rootPanel = args[1];
        if (args.length > 2) arg = args[2];

        Player player = DataHolder.inst().getPlayer(sender.getName());


        // REMOVE PREVIOUS PANEL
        ClickContainer.deleteContainer(panelToRemove, getCommandSenderAsPlayer(sender));
//        ClickContainerMessage remove = new ClickContainerMessage(panelToRemove, true);
//        KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));

        switch (panelToRemove) {
            case "SelectDefense":
            case "ActiveHandDefense":
                player.performCommand("/"+ SelectDefenseCommand.NAME);
                return;
            case "ChooseSkill":
            case "ActiveHand":
                DuelMaster.end(DataHolder.inst().getDuelForAttacker(args[1]));
                player.performCommand("/"+ PerformAction.NAME);
                return;
            case "ToolPanel":
            case "PerformAction":
            case "MovementPanel":
                player.performCommand("/"+ ToolPanel.NAME);
                return;


        }

        // probably doesn't work //всё работает лол
        if(rootPanel.equals("MovementPanel")){
            player.performCommand("/performmovement");
        }
        else if(!rootPanel.equals("")) {
            player.performCommand("/" + rootPanel + " " + arg);
        }

    }
}
