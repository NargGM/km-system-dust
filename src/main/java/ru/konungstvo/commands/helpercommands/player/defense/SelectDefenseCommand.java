package ru.konungstvo.commands.helpercommands.player.defense;

import de.cas_ual_ty.gci.item.ItemGun;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.FudgeDiceMessage;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.Skill;
import ru.konungstvo.combat.StatusType;
import ru.konungstvo.combat.dice.modificator.ModificatorType;
import ru.konungstvo.combat.duel.DefenseType;
import ru.konungstvo.combat.duel.Duel;
import ru.konungstvo.combat.equipment.AttackType;
import ru.konungstvo.commands.executor.SurrenderExecutor;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.player.ModifiersState;
import ru.konungstvo.player.Player;

import java.util.ArrayList;
import java.util.List;

public class SelectDefenseCommand extends CommandBase {
    public static final String NAME = "selectdefense";
    public static String START_MESSAGE = "Выберите способ защиты:\n";
    public static String END_MESSAGE = "Вы выбрали %s.";

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public List<String> getAliases() {
        ArrayList<String> result = new ArrayList();
        result.add("selectdefence");
        return result;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        DataHolder dh = DataHolder.inst();

        Player player = dh.getPlayer(sender.getName());
        Player defender = dh.getPlayer(sender.getName());


        // GET GM
        if (defender.getSubordinate() != null) {
            defender = defender.getSubordinate();
        }
        Skill defense = defender.getAutoDefense();

        Duel duel = dh.getDuelForDefender(defender.getName());

        if (duel.getAttacker().getModifiersState().isModifier(ModifiersState.capture)) {
            try {
                ItemStack is = duel.getAttacker().getItemForHand(duel.getActualActiveHand());
                ItemGun gun = (ItemGun) is.getItem();
                if (gun.hasStockOptions()) {
                    boolean needStock = duel.getActiveAttack().weapon.getFirearm().needStock();
                    boolean hasStock = gun.hasStock(is);
                    System.out.println("needStock " + needStock + " " + "hasStock " + hasStock);
                    double distance = ServerProxy.getDistanceBetween(duel.getAttacker(), defender);
                    if(hasStock) {
                        if (!needStock) {
                            if (distance <= 5) duel.getAttacker().getPercentDice().setStock(-10);
                            if (distance >= 10) duel.getAttacker().getPercentDice().setStock(10);
                        }
                    } else {
                        if (needStock) {
                            if (distance <= 5) duel.getAttacker().getPercentDice().setStock(10);
                            if (distance >= 10) duel.getAttacker().getPercentDice().setStock(-10);
                        }
                    }
                }
            } catch (Exception ignored) {

            }
            try {
                if (duel.getActiveAttack().getWeapon().getMelee().isTwohanded()) {
                    duel.getAttacker().getPercentDice().checkAndGetAndSetLimbInjurty(2, duel.getAttacker().getName());
                } else {
                    duel.getAttacker().getPercentDice().checkAndGetAndSetLimbInjurty(duel.getActualActiveHand(), duel.getAttacker().getName());
                }
            } catch (Exception ignored) {
                duel.getAttacker().getPercentDice().checkAndGetAndSetLimbInjurty(duel.getActualActiveHand(), duel.getAttacker().getName());
            }
            duel.getActiveAttack().getDiceMessage().getDice().cast();
//            duel.getActiveAttack().getDiceMessage().build();
            FudgeDiceMessage fdm = duel.getActiveAttack().getDiceMessage();
            Message capture = new Message(duel.getAttacker().getName() + " пытается принудить " + defender.getName() + " к сдаче с броском ", ChatColor.RED);
            MessageComponent c2 = new MessageComponent(fdm.getDice().getResultAsString(), ChatColor.DICE);
            c2.setUnderlined(true);
            c2.setHoverText(new TextComponentString("§e(( " + duel.getAttacker().getName() + " бросает 4df " + fdm.getDice().getInfo() + " от " +
                    fdm.getDice().getInitial() + " [" + fdm.getDice().getSkillName() + "]. Результат: " + fdm.getDice().getResultAsString() + " ))" +
                    "\n§cБросок совершен без учета модификаторов."));
            capture.addComponent(c2);
            capture.addComponent(new MessageComponent(".", ChatColor.RED));
            ServerProxy.sendMessageFromTwoSourcesAndInformMasters(duel.getAttacker(), defender, duel.getAttacker().getDefaultRange(), capture);
            //ServerProxy.informDistantMasters(duel.getAttacker(), capture, duel.getAttacker().getDefaultRange().getDistance());
        } else {
            Message info = new Message(duel.getAttacker().getName() + " атакует " + defender.getName() + (duel.getActiveAttack().getDiceMessage().getDice().mods.hasMod(ModificatorType.COURAGE) ? " §dна кураже§c" : "") + "!", ChatColor.RED);
            Message masterInfo = new Message(duel.getAttacker().getName() + " атакует ", ChatColor.RED);
            MessageComponent oppComponent = new MessageComponent(defender.getName(), ChatColor.RED);
            if (DataHolder.inst().isNpc(defender.getName())) {
                oppComponent.setUnderlined(true);
                oppComponent.setClickCommand("/sub " + defender.getName());
            }
            masterInfo.addComponent(oppComponent);
            masterInfo.addComponent(new MessageComponent(  (duel.getActiveAttack().getDiceMessage().getDice().mods.hasMod(ModificatorType.COURAGE) ? " §dна кураже§c" : "") + "!", ChatColor.RED));
            ServerProxy.sendMessageFromTwoSourcesAndInformMastersWithUniqueMessage(duel.getAttacker(), defender, duel.getAttacker().getDefaultRange(), info, masterInfo);
            //ServerProxy.informDistantMasters(duel.getAttacker(), info, duel.getAttacker().getDefaultRange().getDistance());
        }

        if (defense != null && !duel.getAttacker().getModifiersState().isModifier(ModifiersState.capture)) {
            switch (defense.getSkillType()) {
                case EVADE:
                    player.performCommand("/" + PerformDefenseCommand.NAME + " " + DefenseType.EVADE.toString());
                    return;
                case PARRY:
                    player.performCommand("/" + SelectDefenseItem.NAME + " " + DefenseType.PARRY.toString());
                    return;
                case BLOCK:
                    player.performCommand("/" + SelectDefenseItem.NAME + " " + DefenseType.BLOCKING.toString());
                    return;
                case CONSTITUTION:
                    player.performCommand("/" + PerformDefenseCommand.NAME + " " + DefenseType.CONSTITUTION.toString());
                    return;
                case PHYSICAL:
                    player.performCommand("/" + PerformDefenseCommand.NAME + " " + DefenseType.PHYSICAL.toString());
                    return;
                case MENTAL:
                    player.performCommand("/" + PerformDefenseCommand.NAME + " " + DefenseType.PSYCH.toString());
                    return;
            }

        }

        // CREATE CLICK PANEL
        Message message = new Message(START_MESSAGE, ChatColor.COMBAT);

        if (duel.getAttacker().getModifiersState().isModifier("suppressing")) {
            message = new Message("Вас пытаются подавить огнём!\n", ChatColor.RED);
            MessageComponent oppComponent = new MessageComponent("[Держаться] ", ChatColor.DARK_AQUA);
            oppComponent.setClickCommand("/" + PerformDefenseCommand.NAME + " " + DefenseType.WILLPOWER.toString());
            oppComponent.setHoverText("Защищаться силой воли от подавления", TextFormatting.AQUA);
            message.addComponent(oppComponent);

            oppComponent = new MessageComponent("[Укрыться] ", ChatColor.GLOBAL);
            oppComponent.setClickCommand("/" + SurrenderExecutor.NAME + " " + "cover");
            oppComponent.setHoverText("Укрыться и пропустить следующий ход", TextFormatting.WHITE);
            message.addComponent(oppComponent);

        } else {
            MessageComponent oppComponent;
            if (duel.getActiveAttack().getAttackType().equals(AttackType.PHYSIC)) {
                oppComponent = new MessageComponent("[Физическая защита] ", ChatColor.BLUE);
                oppComponent.setClickCommand("/" + PerformDefenseCommand.NAME + " " + DefenseType.PHYSICAL.toString());
                message.addComponent(oppComponent);
            } else {
                oppComponent = new MessageComponent("[Психическая защита] ", ChatColor.DARK_PURPLE);
                oppComponent.setClickCommand("/" + PerformDefenseCommand.NAME + " " + DefenseType.PSYCH.toString());
                message.addComponent(oppComponent);
            }


//            Attack activeAttack = duel.getActiveAttack();
//            if (activeAttack.getDiceMessage().getDice().getSkillName().toLowerCase()
//                    .equals(DefenseType.HAND_TO_HAND.toString().toLowerCase())
//                    || activeAttack.getWeapon().isRanged()
//                    && ServerProxy.getDistanceBetween(defender, duel.getAttacker()) < 2) {
//
//                oppComponent = new MessageComponent("[Рукопашный бой] ", ChatColor.BLUE);
//                oppComponent.setClickCommand("/" + PerformDefenseCommand.NAME + " " + DefenseType.HAND_TO_HAND.toString());
//                message.addComponent(oppComponent);
//            }



//            oppComponent = new MessageComponent("[Блокирование] ", ChatColor.BLUE);
//            oppComponent.setClickCommand("/" + SelectDefenseItem.NAME + " " + DefenseType.BLOCKING.toString());
//            message.addComponent(oppComponent);
//
//            oppComponent = new MessageComponent("[Группировка] ", ChatColor.BLUE);
//            oppComponent.setClickCommand("/" + PerformDefenseCommand.NAME + " " + DefenseType.CONSTITUTION.toString());
//            message.addComponent(oppComponent);

//            if (DataHolder.inst().eligibleForCounter(defender) && !duel.isUncommon()) {
//                oppComponent = new MessageComponent("[Контратака] ", ChatColor.BLUE);
//                oppComponent.setClickCommand("/" + SelectDefenseItem.NAME + " " + DefenseType.COUNTER.toString());
//                message.addComponent(oppComponent);
//            } else if (duel.getAttacks().size() <= 1 && defender.getCombatState() == CombatState.SHOULD_WAIT && !duel.isUncommon() && (!defender.hasStatusEffect(StatusType.STUNNED) || defender.cantBeStunned())) {
//                oppComponent = new MessageComponent("[Контратака] ", ChatColor.GRAY);
//                oppComponent.setClickCommand("/" + SelectDefenseItem.NAME + " " + DefenseType.COUNTER.toString());
//                oppComponent.setHoverText("Контратака недоступна, но если длина вашего оружия превышает длину оружия противника, то вы можете использовать контратаку.", TextFormatting.GRAY);
//                message.addComponent(oppComponent);
//            }

//            if (duel.getAttacker().getModifiersState().isModifier(ModifiersState.capture)) {
//                oppComponent = new MessageComponent("[Сдаться] ", ChatColor.DEFAULT);
//                oppComponent.setClickCommand("/" + SurrenderExecutor.NAME);
//                message.addComponent(oppComponent);
//            }

            oppComponent = new MessageComponent("[Ничего] ", ChatColor.BLUE);
            oppComponent.setClickCommand("/" + PerformDefenseCommand.NAME + " " + DefenseType.NOTHING.toString());
            message.addComponent(oppComponent);
        }

        MessageComponent modifiers = new MessageComponent("[Мод] ", ChatColor.GRAY);
        modifiers.setClickCommand("/modifiersbutton");
        message.addComponent(modifiers);


        // SEND PACKET TO CLIENT
        EntityPlayerMP client = getCommandSenderAsPlayer(sender);
        //if (dh.isNpc(sender.getName())) {
        //    client = ServerProxy.getForgePlayer(mastermind.getName());
        //}
        Helpers.sendClickContainerToClient(message, "SelectDefense", client, defender);
        /*
        String json = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(message));
        ClickContainerMessage result = new ClickContainerMessage("SelectDefense", json);
        KMPacketHandler.INSTANCE.sendTo(result, getCommandSenderAsPlayer(sender));
         */
    }
}
