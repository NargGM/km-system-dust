package ru.konungstvo.commands.helpercommands.player.attack;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.StatusType;
import ru.konungstvo.combat.movement.MovementHistory;
import ru.konungstvo.commands.executor.FeintExecutor;
import ru.konungstvo.commands.executor.NextExecutor;
import ru.konungstvo.commands.helpercommands.player.turn.ToRoot;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

public class InitAttackCommand extends CommandBase {
    public static final String NAME = "attack";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }
    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        // ASSEMBLE PANEL
        Message message = new Message("Выберите тип атаки!\n", ChatColor.COMBAT);

        Player player = DataHolder.inst().getPlayer(sender.getName());

        if (args.length > 0 && player.hasPermission(Permission.GM)) {
            String subordinateName = args[0];
            player.setSubordinate(subordinateName);
        }

        if (player.getSubordinate() != null) player = player.getSubordinate();
        Player subordinate = player;

            MessageComponent attack = new MessageComponent("[Атака оружием] ", ChatColor.BLUE);
            attack.setClickCommand("/" + AttackCommand.NAME + " armed");
            message.addComponent(attack);

            MessageComponent unarmed = new MessageComponent("[Без оружия] ", ChatColor.BLUE);
            unarmed.setClickCommand("/" + AttackCommand.NAME + " unarmed");
            message.addComponent(unarmed);

        if (player.hasStatusEffect(StatusType.RELENTLESS_PSYCH) || player.hasStatusEffect(StatusType.RELENTLESS) || player.hasStatusEffect(StatusType.LUNGE)) {
            MessageComponent feint = new MessageComponent("[Выбрать приём] ", ChatColor.GRAY);
            feint.setHoverText("Вы уже использовали приём в этом раунде!", TextFormatting.RED);
            message.addComponent(feint);
        } else if (player.hasRemovableFeints()) {
            MessageComponent feint = new MessageComponent("[Отменить приём] ", ChatColor.RED);
            feint.setHoverText("Не использовать выбранный приём в этом ходу.", TextFormatting.RED);
            feint.setClickCommand("/" + FeintExecutor.NAME + " clearall PerformAction");
            message.addComponent(feint);
        } else {
            MessageComponent feint = new MessageComponent("[Выбрать приём] ", ChatColor.GMCHAT);
            feint.setClickCommand("/" + FeintExecutor.NAME + " %IGNORE% PerformAction");
            message.addComponent(feint);
        }

            if (player.hasStatusEffect(StatusType.BURNING)) {
                MessageComponent extinguish = new MessageComponent("[Потушиться] ", ChatColor.RED);
                extinguish.setClickCommand("/" + NextExecutor.NAME + " extinguish");
                extinguish.setHoverText("Упасть и кататься!", TextFormatting.BLUE);
                message.addComponent(extinguish);
            }

        if (!player.getMovementHistory().equals(MovementHistory.NONE)) {
            MessageComponent customAction = new MessageComponent("[Конец движения] ", ChatColor.BLUE);
            customAction.setClickCommand("/" + NextExecutor.NAME + " resetmovement");
            message.addComponent(customAction);
        }

        MessageComponent modifiers = new MessageComponent("[Мод] ", ChatColor.GRAY);
        modifiers.setClickCommand("/modifiersbutton PerformAction");
        message.addComponent(modifiers);


        if (player.getMovementHistory().equals(MovementHistory.NONE)) {
            MessageComponent toRoot = new MessageComponent("[Назад]", ChatColor.GRAY);
            //toRoot.setBold(true);
            toRoot.setClickCommand("/" + ToRoot.NAME + " PerformAction" + " ToolPanel");
            message.addComponent(toRoot);
        }

        // SEND PACKET TO CLIENT
        Helpers.sendClickContainerToClient(message, "PerformAction", (EntityPlayerMP) sender, subordinate);


    }
}
