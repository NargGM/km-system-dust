package ru.konungstvo.commands.helpercommands.player.defense;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.server.MinecraftServer;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.FudgeDiceMessage;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.Skill;
import ru.konungstvo.combat.SkillType;
import ru.konungstvo.combat.duel.DefenseType;
import ru.konungstvo.combat.duel.Duel;
import ru.konungstvo.combat.equipment.Equipment;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.kmrp_lore.helpers.SkillSaverHandler;
import ru.konungstvo.player.Player;

import java.util.Arrays;
import java.util.List;

public class ChooseDefenseSkill extends CommandBase {
    public static String START_MESSAGE = "Выберите навык!\n";
    public static final String NAME = "choosedefenseskill";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        String skill;
        String type = args[0];

        // GET PLAYER OR NPC
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player defender = DataHolder.inst().getPlayer(sender.getName());
        EntityLivingBase forgePlayer = getCommandSenderAsPlayer(sender);
        if (defender.getSubordinate() != null) {
            defender = defender.getSubordinate();
            forgePlayer = (EntityLivingBase) DataHolder.inst().getNpcEntity(defender.getName());
            if (!DataHolder.inst().isNpc(defender.getName())) {
                forgePlayer = ServerProxy.getForgePlayer(defender.getName());
            }
        }
        Duel duel = DataHolder.inst().getDuelForDefender(defender.getName());

        Equipment defenseItem = duel.getDefense().getDefenseItem();
        SkillSaverHandler skillSaver = new SkillSaverHandler(forgePlayer.getHeldItemOffhand());
        System.out.println(forgePlayer.getHeldItemOffhand().getDisplayName());
        if (duel.getDefense().getActiveHand() == 1) {
            skillSaver = new SkillSaverHandler(forgePlayer.getHeldItemMainhand());
            System.out.println(forgePlayer.getHeldItemOffhand().getDisplayName());
        }



        // СФОРМИРОВАТЬ ДАЙС ЗАЩИТЫ
        if (args[0].equals("default")) { // ЕСЛИ ОТ ПЛОХО
            FudgeDiceMessage dice = new FudgeDiceMessage(defender.getName(), "% плохо"); //TODO: SUS уже в PerformDefense делается
//            dice.build();
            duel.getDefense().setDiceMessage(dice); //TODO: SUS

            ClickContainer.deleteContainer("ChooseSkill", getCommandSenderAsPlayer(sender));
            player.performCommand(String.format("/%s default", PerformDefenseCommand.NAME));

        } else if (args[0].equals("ready")) { // ЕСЛИ ОТ НАВЫКА
            skill = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
            SkillDiceMessage dice = new SkillDiceMessage(defender.getName(), "% " + skill); //TODO: SUS

            System.out.println("WEAPON: " + defenseItem.toString());
            System.out.println("skill: " + skill);
            duel.getDefense().setDiceMessage(dice); //TODO: SUS

            Message answer = new Message("Вы выбрали " + skill, ChatColor.GRAY);
            player.sendMessage(answer);
            System.out.println("TEST");
            //System.out.println("FullSkillSaver: " + skillSaver.getFullSkillSaverForWeapon(defender.getName(), defenseItem.getName())); TODO: проблема или нет?
            System.out.println("TEST2");
            try {
                skillSaver.setSkillSaver(defender.getName(), duel.getDefense().getDefenseType().toNameString(), skill); //TODO: найти проблему
                System.out.println("Saved " + skill);
                System.out.println("FullSkillSaver: " + skillSaver.getFullSkillSaverForWeapon(defender.getName(), defenseItem.getName()));
            } catch (Exception e) {
                e.printStackTrace();
            }


            ClickContainer.deleteContainer("ChooseSkill", getCommandSenderAsPlayer(sender));

            player.performCommand(String.format("/%s %s", PerformDefenseCommand.NAME, type));

       // } else if (type.equals(SkillType.BLOCK.toString())) {
       //     originalSender.performCommand(String.format("/%s ready %s", NAME, SkillType.BLOCK.toString()));


        } else { // ЕСЛИ ЕЩЁ НЕ ВЫБРАЛ
            // GENERATE CLICK PANEL
            Message message = new Message(START_MESSAGE, ChatColor.COMBAT);
            List<Skill> defenseSkills;
            System.out.println("Type: " + type);


            // Ищем навык парирования в SkillSaver
            if (type.equals(DefenseType.PARRY.toString())) {
                String savedSkill = skillSaver.getSkillSaverFor(forgePlayer.getName(), "парирование");
                if (!savedSkill.isEmpty()) {
                    player.performCommand(String.format("/%s ready %s", NAME, savedSkill));
                    return;
                }
                defenseSkills = defender.getParrySkills();

                // Ищем навык блокирования в SkillSaver
            } else if (type.equals(DefenseType.BLOCKING.toString())) {
                player.performCommand(String.format("/%s ready %s", NAME, SkillType.BLOCK));
                return;

                // Ищем навык владения (контратаки) в SkillSaver
            } else if (type.equals(DefenseType.COUNTER.toString())) {
                String savedSkill = skillSaver.getSkillSaverFor(forgePlayer.getName(), "владение");
                if (!savedSkill.isEmpty()) {
                    player.performCommand(String.format("/%s ready %s", NAME, savedSkill));
                    return;
                }
                defenseSkills = defender.getAttackSkills();

            } else {
                defenseSkills = defender.getDefenseSkills();
            }

            // Если нужно показать все навыки
            if (args[0].equals("more")) {
                defenseSkills = defender.getSkills();
            }

            // Создаем кнопки
            for (Skill sk : defenseSkills) {
                String skillStr = Character.toUpperCase(sk.getName().charAt(0)) + sk.getName().substring(1);
                MessageComponent mes = new MessageComponent("[" + skillStr + "] ", ChatColor.BLUE);
                mes.setClickCommand("/" + NAME + " ready " + sk.getName());
                message.addComponent(mes);
            }

            if (!args[0].equals("more")) {
                MessageComponent res = new MessageComponent("[Другое] ", ChatColor.BLUE);
                res.setClickCommand("/" + NAME + " more");
                message.addComponent(res);
            }

            MessageComponent res = new MessageComponent("[Плохо] ", ChatColor.BLUE);
            res.setClickCommand("/" + NAME + " default");
            message.addComponent(res);

            MessageComponent modifiers = new MessageComponent("[Мод] ", ChatColor.GRAY);
            modifiers.setClickCommand("/modifiersbutton");
            message.addComponent(modifiers);

            MessageComponent toRoot = new MessageComponent("[Назад] ", ChatColor.GRAY);
            toRoot.setClickCommand("/toroot ChooseSkill");
            message.addComponent(toRoot);

            // SEND PACKET TO CLIENT
            Helpers.sendClickContainerToClient(message, "ChooseSkill", getCommandSenderAsPlayer(sender), defender);

        }


    }
}
