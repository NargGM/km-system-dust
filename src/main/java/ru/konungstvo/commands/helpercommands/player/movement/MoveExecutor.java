package ru.konungstvo.commands.helpercommands.player.movement;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.server.MinecraftServer;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.combat.Skill;
import ru.konungstvo.combat.StatusEnd;
import ru.konungstvo.combat.StatusType;
import ru.konungstvo.combat.dice.modificator.Modificator;
import ru.konungstvo.combat.dice.modificator.ModificatorType;
import ru.konungstvo.combat.movement.GoExecutor;
import ru.konungstvo.combat.movement.MovementCalculator;
import ru.konungstvo.combat.movement.MovementHistory;
import ru.konungstvo.combat.movement.MovementTrait;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.gm.npcadder.NpcCommand;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.player.ModifiersState;
import ru.konungstvo.player.Player;

public class MoveExecutor extends CommandBase {
    public static final String NAME = "kmmove";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;

    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        ClickContainer.deleteContainer("MovementPanel", getCommandSenderAsPlayer(sender));

        String movementType = args[0];

        // GET PLAYER OR NPC
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player runner = DataHolder.inst().getPlayer(sender.getName());
        Player rider = null;
        EntityLivingBase forgeEntity = ServerProxy.getForgePlayer(sender.getName());
        //EntityLivingBase forgePlayer = getCommandSenderAsPlayer(sender);
        if (runner.getSubordinate() != null) {
            runner = player.getSubordinate();
            System.out.println("sub " + runner.getName());

            EntityLivingBase forgeEntityNPC = DataHolder.inst().getNpcEntity(runner.getName());
            if (forgeEntityNPC.getRidingEntity() != null) {
                Entity mountEntity = forgeEntityNPC.getRidingEntity();
                Player mount =  DataHolder.inst().getPlayer(mountEntity.getName());
//                System.out.println("mount? " + (mount != null));
//                assert mount != null;
//                System.out.println("NPC? " + DataHolder.inst().isNpc(mount.getName()));
//                System.out.println("Combat? " + (mount.getAttachedCombatID() == runner.getAttachedCombatID()));
//                System.out.println("Combat mount " + mount.getAttachedCombatID());
//                System.out.println("Combat runner " + runner.getAttachedCombatID());

                if (mount != null) System.out.println(mount.getAttachedCombatID());
                System.out.println(runner.getAttachedCombatID());
                if (mount != null && DataHolder.inst().isNpc(mount.getName()) && mount.getAttachedCombatID() == runner.getAttachedCombatID()) {
                    System.out.println("Same queque " + DataHolder.inst().inSameQueque(runner, mount));
                    if(!DataHolder.inst().inSameQueque(runner, mount)) {
                        rider = runner;
                        runner = mount;
                    }
                }
            }
            player.performCommand(NpcCommand.NAME + " tp " + runner.getName());

            //forgePlayer = (EntityLivingBase) DataHolder.getInstance().getNpcEntity(defender.getName());
        } else {
            if (forgeEntity.getRidingEntity() != null) {
                System.out.println("добрались сюда, ура " + runner.getName());
                Entity mountEntity = forgeEntity.getRidingEntity();
                System.out.println("mount name? " + mountEntity.getName());
                Player mount =  DataHolder.inst().getPlayer(mountEntity.getName());
//                System.out.println("mount? " + (mount != null));
//                assert mount != null;
//                System.out.println("NPC? " + DataHolder.inst().isNpc(mount.getName()));
//                System.out.println("Combat? " + (mount.getAttachedCombatID() == runner.getAttachedCombatID()));
//                System.out.println("Combat mount " + mount.getAttachedCombatID());
//                System.out.println("Combat runner " + runner.getAttachedCombatID());
                if (mount != null && DataHolder.inst().isNpc(mount.getName())) {
                    if(!DataHolder.inst().inSameQueque(runner, mount)) {
                        rider = runner;
                        runner = mount;
                    }
                }
            }
        }
        System.out.println("moving " + runner.getName());

        SkillDiceMessage diceMessage;
        boolean jump = false;
        boolean jumpbomb = false;
        if (movementType.equals("jump") || movementType.equals("jumpbomb")) {
            ClickContainerMessage remove = new ClickContainerMessage("BoomDefense", true);
            KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));

            diceMessage = new SkillDiceMessage(runner.getName(), "% акробатика");
            if (runner.isLying()) diceMessage.getDice().addMod(new Modificator(-1, ModificatorType.LYING));
            jump = true;
            if (movementType.equals("jumpbomb")) jumpbomb = true;
            //if (runner.getMovementHistory().equals(MovementHistory.NOW)) movementType = "jump_run";
        } else {
            diceMessage = new SkillDiceMessage(runner.getName(), "% скорость");
        }
        // TODO: ADD MODIFIERS SUPPORT HERE
        if (runner.getModifiersState().getModifier(ModifiersState.dice) != null) {
            if (runner.getModifiersState().getModifier(ModifiersState.dice) != -666) {
                diceMessage.getDice().addMod(new Modificator(
                        runner.getModifiersState().getModifier(ModifiersState.dice),
                        ModificatorType.CUSTOM
                ));
            }

        }
        /*diceMessage.getDice().getPercentDice().checkAndGetAndSetLimbInjurty(3, runner.getName());*/
        diceMessage.getDice().cast();
        double traitmod = 1;
        if(runner.getMovementTrait() == MovementTrait.FAST) {
            traitmod = 1.5;
        } else if(runner.getMovementTrait() == MovementTrait.SUPERFAST) {
            traitmod = 2;
        }
        String riding = "";
        String freecrawl = "";

        int blocks = MovementCalculator.getAmountOfBlocksFromDice(diceMessage, movementType, traitmod);

        if (movementType.equals("lunge")) {
            runner.addStatusEffect("Рывок", StatusEnd.TURN_START, 1, StatusType.LUNGE);
            if (rider != null) rider.addStatusEffect("Рывок", StatusEnd.TURN_START, 1, StatusType.LUNGE);
        }

        if (movementType.equals("free") && runner.isLying()) {
            blocks = 1;
            freecrawl = " (ползком)";
        }
        int initialBlocks = blocks;
        if (false && rider != null && !DataHolder.inst().inSameQueque(rider, runner)) {
            Skill skill = rider.getSkill("верховая езда");
            int level = 0;
            if (skill != null) level = skill.getLevel();
            int debuff = 0;
            switch (level) {
                case 0:
                    debuff = (int) Math.floor(blocks * 0.25);
                    blocks = blocks - debuff;
                    riding = " (- " + debuff + " от верховой езды)";
                    break;
                case 1:
                    debuff = (int) Math.floor(blocks * 0.1);
                    blocks = blocks - debuff;
                    riding = " (-" + debuff + " от верховой езды)";
                    break;
                case 2:
                    break;
                case 3:
                    debuff = (int) Math.floor(blocks * 0.05);
                    blocks = blocks + debuff;
                    riding = " (+" + debuff + " от верховой езды)";
                    break;
                case 4:
                    debuff = (int) Math.floor(blocks * 0.1);
                    blocks = blocks + debuff;
                    riding = " (+" + debuff + " от верховой езды)";
                    break;
                case 5:
                    debuff = (int) Math.floor(blocks * 0.15);
                    blocks = blocks + debuff;
                    riding = " (+" + debuff + " от верховой езды)";
                    break;
                default:
                    if (level < 0) {
                        debuff = (int) Math.floor(blocks * 0.5);
                        blocks = blocks - debuff;
                        riding = " (-" + debuff + " от верховой езды)";
                        break;
                    } else if (level > 5) {
                        debuff = (int) Math.floor(blocks * 0.2);
                        blocks = blocks + debuff;
                        riding = " (+" + debuff + " от верховой езды)";
                        break;
                    }
            }
        }

        Message result = diceMessage;
        if (movementType.equals("free")) {
            result = new Message(runner.getName() + " использует свободное передвижение (" + initialBlocks + riding + freecrawl + ").", ChatColor.COMBAT);
        } else {
            String prevCom = "";
            if (!diceMessage.getComment().isEmpty()) {
                prevCom = diceMessage.getComment();
            }
            //diceMessage.setComment(prevCom + "блоки: " + initialBlocks + riding + (movementType.equals("jump_run") ? " (с разбегу)" : ""));
        }
        String addinf = "Движение: ";


        switch (movementType) {
            case "free":
                //runner.setMovementHistory(MovementHistory.FREELY);
                runner.setMovementHistory(MovementHistory.LAST_TIME);
                runner.removeStatusEffect(StatusType.CONCENTRATED);
                break;
            case "crawl":
                addinf = "Ползком: ";
                //runner.setMovementHistory(MovementHistory.FREELY);
                runner.setMovementHistory(MovementHistory.LAST_TIME);
                runner.removeStatusEffect(StatusType.CONCENTRATED);
                break;
            case "start":
                //runner.setMovementHistory(MovementHistory.RUN_UP);
                runner.setMovementHistory(MovementHistory.LAST_TIME);
                runner.removeStatusEffect(StatusType.CONCENTRATED);
                break;
            case "run":
                runner.setMovementHistory(MovementHistory.SPRINT);
                runner.removeStatusEffect(StatusType.CONCENTRATED);
                addinf = "Спринт: ";
                break;
            case "lunge":
                runner.setMovementHistory(MovementHistory.LUNGE);
                runner.removeStatusEffect(StatusType.CONCENTRATED);
                addinf = "Рывок: ";
                break;
            case "jump":
            case "jump_run":
                runner.setMovementHistory(MovementHistory.JUMP);
                runner.removeStatusEffect(StatusType.CONCENTRATED);
                addinf = "Прыжок: ";
                break;
            case "jumpbomb":
                runner.setMovementHistory(MovementHistory.JUMPBOMB);
                runner.removeStatusEffect(StatusType.CONCENTRATED);
                addinf = "Прыжок: ";
            default:
                //runner.setMovementHistory(MovementHistory.MOVED);
                break;
        }

        if (rider != null) rider.setMovementHistory(MovementHistory.LAST_TIME);

        diceMessage.setBlocksInfo(addinf + initialBlocks + " " + getPlural(initialBlocks) + riding);

        diceMessage.build();
        ServerProxy.sendMessageFromAndInformMasters(player, result);


        if (rider != null) {
            rider.setMovementHistory(runner.getMovementHistory());
        }
        /*diceMessage.getDice().getPercentDice().setLimbInjury(0);*/
        player.performCommand("/" + GoExecutor.NAME + " " + blocks + (jumpbomb ? " jumpbomb" : (jump ? " jump" : "")));


    }

    public String getPlural(int num) {

        int preLastDigit = num % 100 / 10;
        if (preLastDigit == 1) {
            return "блоков";
        }

        switch (num % 10) {
            case 1:
                return "блок";
            case 2:
            case 3:
            case 4:
                return "блока";
            default:
                return "блоков";
        }

    }

}
