package ru.konungstvo.commands.helpercommands.player.turn;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.Combat;
import ru.konungstvo.combat.StatusType;
import ru.konungstvo.combat.movement.ChargeHistory;
import ru.konungstvo.combat.movement.MovementHistory;
import ru.konungstvo.combat.reactions.Queue;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.player.Player;

import java.util.List;

public class PerformShift extends CommandBase {
    public static final String NAME = "performshift";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;

    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        ClickContainer.deleteContainer("ToolPanel", getCommandSenderAsPlayer(sender));
        // GET PLAYER OR NPC
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player attacker = DataHolder.inst().getPlayer(sender.getName());
        if (attacker.getSubordinate() != null) {
            attacker = attacker.getSubordinate();
        }

        // get combat
        Combat combat = DataHolder.inst().getCombatForPlayer(player.getName());
        if (combat == null) {
            player.sendError(new Exception("Вы не находитесь в бою. No combat found."));
            return;
        }

        // get queue
        Queue queue = combat.getReactionList().getQueue();

        if (attacker.hasShifted()) {
            player.sendError(new Exception("Вы уже смещались в этом раунде."));
            return;
        }
        // shifting
        if (args.length > 0) {
            String whereToShift = args[0];
            queue.shift(attacker, whereToShift);
            ClickContainer.deleteContainer("ShiftContainer", getCommandSenderAsPlayer(sender));
            Message info = new Message("Игрок " + attacker.getName() + " сместился в очереди и ходит после " + whereToShift + ".\n", ChatColor.COMBAT);
            info.addComponent(queue.toMessage());
            ServerProxy.informMasters(info, attacker);

            for (SkillDiceMessage sdm : queue.getResults()) {
                DataHolder.inst().getPlayer(sdm.getPlayerName()).sendMessage(info);
            }
            attacker.setMovementHistory(MovementHistory.NONE);
            attacker.setChargeHistory(ChargeHistory.NONE);
            attacker.removeStatusEffect(StatusType.CONCENTRATED);
            return;
        }


        // creating choice panel
        Message result = new Message("После кого вы хотите ходить?\n", ChatColor.COMBAT);
        List<String> players = queue.getListForShift(attacker.getName());
        for (String s : players) {
            MessageComponent mc = new MessageComponent("[" + s + "] ", ChatColor.BLUE);
            mc.setClickCommand("/performshift " + s);
            result.addComponent(mc);
        }
        MessageComponent toRoot = new MessageComponent("[Назад] ", ChatColor.GRAY);
        //toRoot.setBold(true);
        toRoot.setClickCommand("/" + ToRoot.NAME + " ShiftContainer" + " ToolPanel");
        result.addComponent(toRoot);

        Helpers.sendClickContainerToClient(result, "ShiftContainer", getCommandSenderAsPlayer(sender), attacker);
    }

}
