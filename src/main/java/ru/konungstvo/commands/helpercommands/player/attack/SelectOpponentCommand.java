package ru.konungstvo.commands.helpercommands.player.attack;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.SkillType;
import ru.konungstvo.combat.Trait;
import ru.konungstvo.combat.duel.Duel;
import ru.konungstvo.combat.duel.DuelMaster;
import ru.konungstvo.combat.equipment.FirearmDistance;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.player.turn.ToRoot;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.kmrp_lore.helpers.SkillSaverHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.ModifiersState;
import ru.konungstvo.player.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SelectOpponentCommand extends CommandBase {
    public static final String NAME = "selectopponent";
    public static final String START_MESSAGE = "Выберите цель: \n";
    public static final String END_MESSAGE = "Вы атакуете %s с помощью %s.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        //String skillName = String.join(" ", args);

        // GET PLAYER OR NPC
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player attacker = DataHolder.inst().getPlayer(sender.getName());
        EntityLivingBase forgePlayer = getCommandSenderAsPlayer(sender);
        if (attacker.getSubordinate() != null) {
            attacker = attacker.getSubordinate();
            forgePlayer = (EntityLivingBase) DataHolder.inst().getNpcEntity(attacker.getName());
            if (!DataHolder.inst().isNpc(attacker.getName())) {
                forgePlayer = ServerProxy.getForgePlayer(attacker.getName());
            }
        }


        Duel duel = DataHolder.inst().getDuelForAttacker(attacker.getName());

        if (args.length > 0 && args[0].equals("multicast")) {
            ClickContainer.deleteContainer("ChooseSkill", getCommandSenderAsPlayer(sender));
            ClickContainerMessage remove = new ClickContainerMessage("MagicButton", true);
            KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
            int maxtargets = Integer.parseInt(args[1]);
            int radius = -1;
            int chained = -1;
            if (args.length > 2) {
                if (args[2].startsWith("0123радиус=")) radius = Integer.parseInt(args[2].split("=")[1]);
                if (args[2].startsWith("0123цепь=")) chained = Integer.parseInt(args[2].split("=")[1]);
            }
            int n = 2;
            if (chained > -1 || radius > -1) n = 3;


            // CREATE CLICK PANEL
            Message message = new Message("");
            MessageComponent close = new MessageComponent("[Назад] ", ChatColor.GRAY);
            close.setClickCommand("/" + ToRoot.NAME + " SelectOpponent" + " ToolPanel");

            MessageComponent start = new MessageComponent("Выберите цели:\n", ChatColor.COMBAT);
            message.addComponent(start);

            String[] targets = Arrays.copyOfRange(args, n, args.length);
            String targetsUntil = "";
            String targetsString = String.join(" ", targets);
            System.out.println("targ " + targetsString);
            for (String target : targets) {
                MessageComponent oppComponent = new MessageComponent("[" + target + "] ", ChatColor.DARK_BLUE);
                if (radius > 0) {
                    oppComponent.setClickCommand(
                            //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                            String.format("/%s multicast %s %s %s", NAME, maxtargets, "0123радиус=" + radius, targetsString.replaceFirst("(\\s|^)" + target + "(\\s|$)", " ")).replaceAll("\\s+", " ")
                    );
                } else if (chained > 0) {
                    oppComponent.setClickCommand(
                            //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                            String.format("/%s multicast %s %s %s", NAME, maxtargets, "0123цепь=" + chained, String.join(" ", targetsUntil))
                    );
                } else {
                    oppComponent.setClickCommand(
                            //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                            String.format("/%s multicast %s %s", NAME, maxtargets, targetsString.replaceFirst("(\\s|^)" + target + "(\\s|$)", " ")).replaceAll("\\s+", " ")
                    );
                }
                message.addComponent(oppComponent);
                if (targetsUntil.isEmpty()) targetsUntil += target;
                else targetsUntil += (" " + target);
            }
            Player ult = attacker;
            int lrange = 999;
            if (radius > 0 && targets.length > 0) {
                ult = DataHolder.inst().getPlayer(targets[0]);
                lrange = radius;
            } else if (chained > 0 && targets.length > 0) {
                ult = DataHolder.inst().getPlayer(targets[targets.length-1]);
                lrange = chained;
            }

            if (targets.length < maxtargets) {
                List<Player> newtargets = new ArrayList<>();
                newtargets = ServerProxy.getAllFightersInRange(ult, lrange);

                for (Player opponent : newtargets) {
                    if ((" " + targetsString + " ").contains(" " + opponent.getName() + " ")) continue;
                    if (opponent.equals(attacker)) continue;
                    //if (opponent.getName().equals(sender.getName())) continue;
                    MessageComponent oppComponent = new MessageComponent("[" + opponent.getName() + "] ", ChatColor.BLUE);
                    if (radius > 0) {
                        oppComponent.setClickCommand(
                                //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                                String.format("/%s multicast %s %s %s", NAME, maxtargets, "0123радиус=" + radius, (targetsString + " " + opponent.getName()).trim())
                        );
                    } else if (chained > 0) {
                        oppComponent.setClickCommand(
                                //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                                String.format("/%s multicast %s %s %s", NAME, maxtargets, "0123цепь=" + chained, (targetsString + " " + opponent.getName()).trim())
                        );
                    } else {
                        oppComponent.setClickCommand(
                                //                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                                String.format("/%s multicast %s %s", NAME, maxtargets, (targetsString + " " + opponent.getName()).trim())
                        );
                    }
                    message.addComponent(oppComponent);

                }
            }

            if (targets.length > 0) {
                MessageComponent ready = new MessageComponent("[Готово] ", ChatColor.RED);
                ready.setClickCommand(
                        String.format("/%s %s", PerformAttackCommand.NAME, "%MULTICAST%:" + targetsString.replaceAll(" ", ":"))
                );
                message.addComponent(ready);
            }

            message.addComponent(close);

            // SEND PACKET TO CLIENT
            Helpers.sendClickContainerToClient(message, "SelectOpponent", getCommandSenderAsPlayer(sender), attacker);
            return;

        }



        double weaponRange = 1;
        // GET WEAPON RANGE
        List<Player> targets = new ArrayList<>();
        int activeHand = duel.getActiveHand();
        if (activeHand == 2) activeHand = 1;
        ItemStack itemStack = forgePlayer.getHeldItem(
                Helpers.getEnumHand(activeHand)
        );

        WeaponTagsHandler weaponTagsHandler = null;
        if (WeaponTagsHandler.hasWeaponTags(itemStack)) {
            weaponTagsHandler = new WeaponTagsHandler(itemStack);
            weaponRange = weaponTagsHandler.getWeaponRange();
            weaponRange++;
        } else {
            weaponRange = 2;
        }


        if (weaponTagsHandler != null && weaponTagsHandler.isFirearm()) {
            targets = new ArrayList<>(DataHolder.inst().getCombatForPlayer(attacker.getName()).getFighters());
            targets.remove(attacker);
//            targets = ServerProxy.getVisibleTargets(attacker);
        } else {
            targets = ServerProxy.getAllFightersInRange(attacker, weaponRange);
        }


        if (targets.isEmpty()) {
            TextComponentString error;
            if (weaponTagsHandler != null && weaponTagsHandler.isFirearm()) {
                error = new TextComponentString("Ошибка! Нет врагов в поле зрения.");
            } else {
                error = new TextComponentString("Ошибка! В радиусе дальности вашего оружия (<" + weaponRange + ") нет врагов.");

            }
            error.getStyle().setColor(TextFormatting.RED);
            sender.sendMessage(error);
            player.performCommand(ToRoot.NAME + " ToolPanel");
            DuelMaster.end(duel);
            return;
        }


        // CREATE CLICK PANEL
        Message message = new Message("");
        MessageComponent close = new MessageComponent("[Назад] ", ChatColor.GRAY);
        //close.setBold(true);
        close.setClickCommand("/" + ToRoot.NAME + " SelectOpponent" + " ToolPanel");
        MessageComponent start = new MessageComponent(START_MESSAGE, ChatColor.COMBAT);
        message.addComponent(start);
        for (Player opponent : targets) {
            if (opponent.getName().equals(sender.getName())) continue;
            MessageComponent oppComponent = new MessageComponent("[" + opponent.getName() + "] ", ChatColor.BLUE);
            if (opponent.getDefendedBy().size() > 0) {
                oppComponent.setUnderlined(true);
                StringBuilder defendedList = new StringBuilder();
                for (Player engagedPl : opponent.getDefendedBy()) {
                    defendedList.append("§c[").append(engagedPl.getName()).append("]\n");
                }
                oppComponent.setHoverText("Под защитой:\n" + defendedList.toString().trim(), TextFormatting.GRAY);
            }
            oppComponent.setClickCommand(
//                    String.format("/%s %s %s", PerformAttackCommand.NAME, opponent.getName(), skillName)
                    String.format("/%s %s", PerformAttackCommand.NAME, opponent.getName())
            );
            if (attacker.riposted == opponent) {
                oppComponent.setColor(ChatColor.RED);
                oppComponent.setHoverText("Рипост доступен!\nБонус выставится автоматически.\nНе стакается с концентрацией.", TextFormatting.DARK_RED);
            }
            message.addComponent(oppComponent);

        }

        if (weaponTagsHandler != null && weaponTagsHandler.isFirearm() && weaponTagsHandler.getDefaultWeapon().hasKey("rangedFirearm") && weaponTagsHandler.getDefaultWeapon().getCompoundTag("rangedFirearm").hasKey("serial") && weaponTagsHandler.getDefaultWeapon().getCompoundTag("rangedFirearm").getBoolean("serial") &&
                !weaponTagsHandler.getDefaultWeapon().getCompoundTag("rangedFirearm").hasKey("double") && attacker.getModifiersState().isModifier("serial")) {
            ArrayList<String> multitargets = new ArrayList<>();
            StringBuilder multitargetsstr = new StringBuilder();
            FirearmDistance fd = FirearmDistance.getByName(weaponTagsHandler.getDefaultWeapon().getCompoundTag("rangedFirearm").getString("range"));

            if (fd != null) {
                String type = weaponTagsHandler.getDefaultWeapon().getCompoundTag("rangedFirearm").getString("type");
                boolean machinegun = type.equals("пулемёт");
                boolean pistol = type.equals("пистолет") || type.equals("автоматический пистолет") || type.equals("револьвер");
                int maxrange = fd.getValueForDifficuly(1);
                if (machinegun) maxrange = fd.getValueForDifficuly(2);
                if (pistol) maxrange = fd.getValueForDifficuly(0);
                targets = ServerProxy.getAllFightersInRange(attacker, maxrange);
                for (Player opponent : targets) {
                    EntityLivingBase d;
                    if (DataHolder.inst().isNpc(opponent.getName())) {
                        d = DataHolder.inst().getNpcEntity(opponent.getName());
                    } else {
                        d = ServerProxy.getForgePlayer(opponent.getName());
                    }
                    try {
                        Vec3d vector = d.getPositionVector().subtract(forgePlayer.getPositionVector());
                        Vec3d playerDirection = forgePlayer.getLookVec();
                        double angleDir = (Math.atan2(vector.z, vector.x) / 2 / Math.PI * 360 + 360) % 360;
                        double angleLook = (Math.atan2(playerDirection.z, playerDirection.x) / 2 / Math.PI * 360 + 360) % 360;
                        double angle = (angleDir - angleLook + 360) % 360;
                        System.out.println("angle: " + angle);
                        if ((!machinegun && !pistol && (angle <= 20 || angle >= 340)) || (angle <= 10 || angle >= 350)) { //40 и 20
                            multitargets.add(opponent.getName());
                            multitargetsstr.append(opponent.getName()).append(":");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (multitargets.size() > 1) {
                    MessageComponent splash;
                    splash = new MessageComponent("[Расстрел] ", ChatColor.RED);
                    splash.setHoverText(multitargets.toString(), TextFormatting.BLUE);
                    splash.setClickCommand(String.format("/%s %s", PerformAttackCommand.NAME, "%MULTISHOT%:" + multitargetsstr.toString()));
                    message.addComponent(splash);
                }
                MessageComponent refresh = new MessageComponent("[Обновить] ", ChatColor.BLUE);
                //refresh.setHoverText("splashtargets.toString()", TextFormatting.BLUE);
                refresh.setClickCommand("/" + NAME + " " + String.join(" ", Arrays.copyOfRange(args, 0, args.length)));
                message.addComponent(refresh);
            }
        }

        if (weaponTagsHandler != null && !weaponTagsHandler.isFirearm() && weaponTagsHandler.getDefaultWeapon().hasKey("melee")
                && (attacker.hasTrait(Trait.SPLASH) || attacker.hasTrait(Trait.PIERCING))) {

            if (attacker.hasTrait(Trait.SPLASH)) {
                ArrayList<String> splashtargets = new ArrayList<>();
                StringBuilder splashstring = new StringBuilder();

                targets = ServerProxy.getAllFightersInRange(attacker, weaponRange);

                for (Player opponent : targets) {
                    EntityLivingBase d;
                    if (DataHolder.inst().isNpc(opponent.getName())) {
                        d = DataHolder.inst().getNpcEntity(opponent.getName());
                    } else {
                        d = ServerProxy.getForgePlayer(opponent.getName());
                    }
                    try {
                        Vec3d vector = d.getPositionVector().subtract(forgePlayer.getPositionVector());
                        Vec3d playerDirection = forgePlayer.getLookVec();
                        double angleDir = (Math.atan2(vector.z, vector.x) / 2 / Math.PI * 360 + 360) % 360;
                        double angleLook = (Math.atan2(playerDirection.z, playerDirection.x) / 2 / Math.PI * 360 + 360) % 360;
                        double angle = (angleDir - angleLook + 360) % 360;
                        System.out.println("angle: " + angle);
                        if (angle <= 67.5 || angle >= 292.5) {
                            splashtargets.add(opponent.getName());
                            splashstring.append(opponent.getName()).append(":");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (splashtargets.size() > 1) {
                    MessageComponent splash;
                    splash = new MessageComponent("[Размах] ", ChatColor.RED);
                    splash.setClickCommand(String.format("/%s %s", PerformAttackCommand.NAME, "%SPLASH%:" + splashstring.toString()));
                    splash.setHoverText(splashtargets.toString(), TextFormatting.BLUE);
                    message.addComponent(splash);
                }
            }
            if (attacker.hasTrait(Trait.PIERCING)) {
                ArrayList<String> splashtargets = new ArrayList<>();
                StringBuilder splashstring = new StringBuilder();

                targets = ServerProxy.getAllFightersInRange(attacker, weaponRange);

                for (Player opponent : targets) {
                    EntityLivingBase d;
                    if (DataHolder.inst().isNpc(opponent.getName())) {
                        d = DataHolder.inst().getNpcEntity(opponent.getName());
                    } else {
                        d = ServerProxy.getForgePlayer(opponent.getName());
                    }
                    try {
                        Vec3d vector = d.getPositionVector().subtract(forgePlayer.getPositionVector());
                        Vec3d playerDirection = forgePlayer.getLookVec();
                        double angleDir = (Math.atan2(vector.z, vector.x) / 2 / Math.PI * 360 + 360) % 360;
                        double angleLook = (Math.atan2(playerDirection.z, playerDirection.x) / 2 / Math.PI * 360 + 360) % 360;
                        double angle = (angleDir - angleLook + 360) % 360;
                        System.out.println("angle: " + angle);
                        if ((!weaponTagsHandler.getDefaultWeapon().getCompoundTag("melee").getString("category").equals("колющее") && (angle <= 67.5 || angle >= 292.5)) || (angle <= 10 || angle >= 350)) {
                            splashtargets.add(opponent.getName());
                            splashstring.append(opponent.getName()).append(":");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (splashtargets.size() > 1) {
                    MessageComponent splash;
                    splash = new MessageComponent("[Пронзание] ", ChatColor.RED);
                    splash.setClickCommand(String.format("/%s %s", PerformAttackCommand.NAME, "%SPLASHP%:" + splashstring.toString()));
                    splash.setHoverText(splashtargets.toString(), TextFormatting.BLUE);

                    message.addComponent(splash);
                }
            }
            MessageComponent refresh = new MessageComponent("[Обновить] ", ChatColor.BLUE);
            //refresh.setHoverText("splashtargets.toString()", TextFormatting.BLUE);
            refresh.setClickCommand("/" + NAME + " " + String.join(" ", Arrays.copyOfRange(args, 0, args.length)));
            message.addComponent(refresh);
        }

        message.addComponent(close);

        // SEND PACKET TO CLIENT
        Helpers.sendClickContainerToClient(message, "SelectOpponent", getCommandSenderAsPlayer(sender), attacker);

    }
}
