package ru.konungstvo.commands.helpercommands.player.defense;

import de.cas_ual_ty.gci.GunCus;
import de.cas_ual_ty.gci.item.ItemGun;
import de.cas_ual_ty.gci.network.MessageSound;
import org.apache.commons.lang3.tuple.MutablePair;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import noppes.npcs.api.constants.AnimationType;
import noppes.npcs.entity.EntityNPCInterface;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.FudgeDiceMessage;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.*;
import ru.konungstvo.combat.duel.*;
import ru.konungstvo.combat.equipment.Weapon;
import ru.konungstvo.combat.movement.ChargeHistory;
import ru.konungstvo.combat.movement.MovementHistory;
import ru.konungstvo.combat.reactions.Queue;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponDurabilityHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;
import ru.konungstvo.player.wounds.WoundType;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PerformDefenseCommand extends CommandBase {
    public static final String NAME = "performdefense";
    private static final Pattern pattern = Pattern.compile("\\[.*?\\]");
    private static final Pattern pattern2 = Pattern.compile("\\{.*?\\}");
    private static final Pattern pattern3 = Pattern.compile("\\<.*?\\>");

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public List<String> getAliases() {
        ArrayList<String> result = new ArrayList();
        result.add("performdefence");
        result.add("perfromdefense");
        return result;
    }


    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        if (player.hasPermission(Permission.GM) && player.getSubordinate() != null) {
            player = player.getSubordinate();
        }

        String skillName = args[0];

        // REMOVE PREVIOUS PANEL
        {
            ClickContainerMessage remove = new ClickContainerMessage("SelectDefense", true);
            KMPacketHandler.INSTANCE.sendTo(remove, (EntityPlayerMP) sender);
//            Message info = new Message(String.format(SelectDefenseCommand.END_MESSAGE, skillName), ChatColor.GRAY);
//            DataHolder.inst().getPlayer(sender.getName()).sendMessage(info);
        }

        Duel duel = DataHolder.inst().getDuelForDefender(player.getName());

        System.out.println("duel " + player.getName() + " " + (duel != null));
        System.out.println("duel get defense " + (duel.getDefense() != null));
        System.out.println("duel get defense type " + (duel.getDefense().getDefenseType() != null));

        // НИЧЕГО
        if (skillName.equals(DefenseType.NOTHING.toString())) {
            duel.getDefense().setDefenseType(DefenseType.NOTHING);


            // УКЛОНЕНИЕ
        } else if (skillName.equals(DefenseType.EVADE.toString())) {

            duel.getDefense().setDefenseType(DefenseType.EVADE);

            // СТАВИМ ДАЙС
            Skill evadeSkill = player.getEvadeSkill();
            if (evadeSkill == null) { //Если навыка уклонения нет, кидаем от плохо
                FudgeDiceMessage fudgeDiceMessage = new FudgeDiceMessage(player.getName(), "% плохо");
                duel.getDefense().setDiceMessage(fudgeDiceMessage);
            } else {
                SkillDiceMessage skillDiceMessage = new SkillDiceMessage(player.getName(), "% " + evadeSkill.getName());
                duel.getDefense().setDiceMessage(skillDiceMessage);
            }

        } else if (skillName.equals(DefenseType.PHYSICAL.toString())) {

            duel.getDefense().setDefenseType(DefenseType.PHYSICAL);

            // СТАВИМ ДАЙС
            Skill evadeSkill = player.getPhysicSkill();
            if (evadeSkill == null) { //Если навыка уклонения нет, кидаем от плохо
                FudgeDiceMessage fudgeDiceMessage = new FudgeDiceMessage(player.getName(), "% плохо");
                duel.getDefense().setDiceMessage(fudgeDiceMessage);
            } else {
                SkillDiceMessage skillDiceMessage = new SkillDiceMessage(player.getName(), "% " + evadeSkill.getName());
                duel.getDefense().setDiceMessage(skillDiceMessage);
            }

        }  else if (skillName.equals(DefenseType.PSYCH.toString())) {

            duel.getDefense().setDefenseType(DefenseType.PSYCH);

            // СТАВИМ ДАЙС
            Skill evadeSkill = player.getMentalSkill();
            if (evadeSkill == null) { //Если навыка уклонения нет, кидаем от плохо
                FudgeDiceMessage fudgeDiceMessage = new FudgeDiceMessage(player.getName(), "% плохо");
                duel.getDefense().setDiceMessage(fudgeDiceMessage);
            } else {
                SkillDiceMessage skillDiceMessage = new SkillDiceMessage(player.getName(), "% " + evadeSkill.getName());
                duel.getDefense().setDiceMessage(skillDiceMessage);
            }

        }  else if (duel.getDefense().getDefenseType() == DefenseType.ANTICOUNTER) {
            // handled elsewhere, basically

        } else if (duel.getDefense().getDefenseType() == DefenseType.PARRY) {
            // handled elsewhere, basically

            // РУКОПАШНЫЙ БОЙ
        } else if (skillName.equals(DefenseType.HAND_TO_HAND.toString())) {
            duel.getDefense().setDefenseType(DefenseType.HAND_TO_HAND);
            // СТАВИМ ДАЙС
            Skill handToHandSkill = player.getHandToHandSkill();
            if (handToHandSkill == null) { //Если навыка рукопашного боя нет, кидаем от плохо
                FudgeDiceMessage fudgeDiceMessage = new FudgeDiceMessage(player.getName(), "% плохо");
                duel.getDefense().setDiceMessage(fudgeDiceMessage);
            } else {
                SkillDiceMessage skillDiceMessage = new SkillDiceMessage(player.getName(), "% " + handToHandSkill.getName());
                duel.getDefense().setDiceMessage(skillDiceMessage);
            }

            // БЛОКИРОВАНИЕ
        } else if (duel.getDefense().getDefenseType() == DefenseType.BLOCKING) {
//            duel.getDefense().setDefenseType(DefenseType.BLOCKING);
            // handled elsewhere, basically
            // СТАВИМ ДАЙС
            /*Skill blockSkill = player.getBlockSkill();
            if (blockSkill == null) { //Если навыка блокирования нет, кидаем от плохо
                FudgeDiceMessage fudgeDiceMessage = new FudgeDiceMessage(player.getName(), "% плохо");
                duel.getDefense().setDiceMessage(fudgeDiceMessage);
            } else {
                SkillDiceMessage skillDiceMessage = new SkillDiceMessage(player.getName(), "% " + blockSkill.getName());
                duel.getDefense().setDiceMessage(skillDiceMessage);
            }*/

            // ГРУППИРОВКА
        } else if (skillName.equals(DefenseType.CONSTITUTION.toString())) {

            duel.getDefense().setDefenseType(DefenseType.CONSTITUTION);
            // СТАВИМ ДАЙС
            Skill groupSkill = player.getGroupSkill();
            if (groupSkill == null) { //Если навыка блокирования нет, кидаем от плохо
                FudgeDiceMessage fudgeDiceMessage = new FudgeDiceMessage(player.getName(), "% плохо");
                duel.getDefense().setDiceMessage(fudgeDiceMessage);
            } else {
                SkillDiceMessage skillDiceMessage = new SkillDiceMessage(player.getName(), "% " + groupSkill.getName());
                duel.getDefense().setDiceMessage(skillDiceMessage);
            }
        } else if (skillName.equals(DefenseType.WILLPOWER.toString())) {

            duel.getDefense().setDefenseType(DefenseType.WILLPOWER);
            // СТАВИМ ДАЙС
            Skill willpowerSkill = player.getWillpowerSkill();
            if (willpowerSkill == null) { //Если навыка блокирования нет, кидаем от плохо
                FudgeDiceMessage fudgeDiceMessage = new FudgeDiceMessage(player.getName(), "% плохо");
                duel.getDefense().setDiceMessage(fudgeDiceMessage);
            } else {
                SkillDiceMessage skillDiceMessage = new SkillDiceMessage(player.getName(), "% " + willpowerSkill.getName());
                duel.getDefense().setDiceMessage(skillDiceMessage);
            }
        }

        // GET ARMOR FROM ITEMS
        player.purgeArmor();
        DataHolder.inst().updatePlayerArmor(player.getName());
//        for (ItemStack armorPiece :
//                ((EntityPlayerMP) sender).getArmorInventoryList()) {
//            WeaponTagsHandler armorHandler = new WeaponTagsHandler(armorPiece);
//            if (armorHandler.isArmor()) {
//                for (NBTTagCompound piece : armorHandler.getAllWeapons()) {
//                    System.out.println("piece : " + piece.toString());
//                    try {
//                        player.addToArmor(piece.getCompoundTag("armor"));
//                    } catch (ParseException e) {
//                        sender.sendMessage(new TextComponentString("Ошибка при добавлении брони! " + e.getMessage()));
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }
        Player attacker = duel.getAttacker();
        Combat combat = DataHolder.inst().getCombatForPlayer(attacker.getName());

        boolean isRanged = false;
        int recoil = 0;
        int recoilTurns = 0;
        boolean multishot = duel.hasMultishot();

        for (Attack currentAttack : duel.getAttacks()) {
            if (currentAttack.getWeapon().isRanged()) {
                isRanged = true;
                if ((duel.getAttacks().size() > 1 && duel.getActiveHand() != 2) || currentAttack.getWeapon().getFirearm().hasSingleRecoil() || multishot || true) {
                    recoil = currentAttack.getWeapon().getFirearm().getRecoil();
                    recoilTurns = currentAttack.getWeapon().getFirearm().getRecoilTurns();
                    System.out.println("recoil " + recoil);
                }
            }
//            System.out.println("Current attack: " + currentAttack.toString());
            try {
//                System.out.println(player.getName() + "'s thorax:");
//                player.getArmor().getArmorPiece(BodyPart.THORAX);
                duel.performDefense(currentAttack);
                duel.getDefender().getPercentDice().setPreviousDefenses(0);
            } catch (Exception e) {
                player.sendError(e);
                e.printStackTrace();
            }
            if (duel.mustEnd && duel.successfulCounter) {
                //duel.getDefender().setRecoil(recoil);
                attacker.getPercentDice().setBipod(0);
                attacker.getPercentDice().setAmmo(0);
                attacker.getPercentDice().setLimbInjury(0);
                attacker.getPercentDice().setStock(0);
                attacker.getPercentDice().setHighground(0);
                duel.getDefender().getPercentDice().setHighground(0);
                attacker.setMovementHistory(MovementHistory.NONE);
                attacker.setChargeHistory(ChargeHistory.NONE);
                attacker.riposted = null;
                attacker.processTurnEnd(attacker, "combat", DataHolder.inst().getCombatForPlayer(attacker.getName()));
                duel.getDefender().processTurnStart(duel.getDefender(), DataHolder.inst().getCombatForPlayer(attacker.getName()));
                DuelMaster.end(duel);
                if (DataHolder.inst().isNpc(attacker.getName())) {
                    Player master = DataHolder.inst().getMasterForNpcInCombat(attacker);
                    master.setSubordinate(attacker);
                    master.performCommand(String.format("/%s %s", PerformDefenseCommand.NAME, DefenseType.ANTICOUNTER));
                } else {
                    attacker.performCommand(String.format("/%s %s", PerformDefenseCommand.NAME, DefenseType.ANTICOUNTER));
                }
                return;
            }
            Player actualDefender = duel.getDefender(); // this is here because of CounterAttacks
            Player actualAttacker = duel.getAttacker();
            /*COUNTER*/
//            if (duel.successfulCounter) {
//                actualDefender = duel.getAttacker();
//                actualAttacker = duel.getDefender();
//            }
            // INFORM PLAYERS
            duel.originalAttack.getDiceMessage().build();
//            System.out.println("trying to send " + duel.originalAttack.getDiceMessage().toString()) ;
            if (!duel.mustBreak) {
                if (duel.successfulCounter) {
                    //ServerProxy.informDistantMasters(actualAttacker, duel.originalAttack.getDiceMessage(), actualAttacker.getDefaultRange().getDistance());

//            System.out.println("trying to send " + duel.getDefense().getDiceMessage().toString());
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(), duel.getOldAttackResult());
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(), duel.getOldDefenseResult());
                    Message counter = new Message("Контратака!", ChatColor.RED);
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(), counter);
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(), duel.originalAttack.getDiceMessage());
                    if (duel.getDefense().getDefenseType() != DefenseType.NOTHING) {
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(),
                                duel.getDefense().getDiceMessage());
                        //ServerProxy.informDistantMasters(actualAttacker, duel.getDefense().getDiceMessage(), actualAttacker.getDefaultRange().getDistance());

                    } else {
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(),
                                new Message(actualDefender.getName() + " не защищается.", ChatColor.COMBAT));
                        //ServerProxy.informDistantMasters(actualAttacker, new Message(actualDefender.getName() + " не защищается.", ChatColor.COMBAT), actualAttacker.getDefaultRange().getDistance());
                    }
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(), duel.getDamageInfo());
                    //ServerProxy.informDistantMasters(actualAttacker, duel.getDamageInfo(), actualAttacker.getDefaultRange().getDistance());

                    System.out.println(duel.getRangeInfo());
                    if (duel.getRangeInfo() != null) {
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(), duel.getRangeInfo());
                        //ServerProxy.informDistantMasters(actualAttacker, duel.getRangeInfo(), actualAttacker.getDefaultRange().getDistance());
                    }
                } else {
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(), duel.originalAttack.getDiceMessage());
                    //ServerProxy.informDistantMasters(actualAttacker, duel.originalAttack.getDiceMessage(), actualAttacker.getDefaultRange().getDistance());

//            System.out.println("trying to send " + duel.getDefense().getDiceMessage().toString());
                    if (duel.getDefense().getDefenseType() != DefenseType.NOTHING) {
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(),
                                duel.getDefense().getDiceMessage());
                        //ServerProxy.informDistantMasters(actualAttacker, duel.getDefense().getDiceMessage(), actualAttacker.getDefaultRange().getDistance());

                    } else {
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(),
                                new Message(actualDefender.getName() + " не защищается.", ChatColor.COMBAT));
                        //ServerProxy.informDistantMasters(actualAttacker, new Message(actualDefender.getName() + " не защищается.", ChatColor.COMBAT), actualAttacker.getDefaultRange().getDistance());
                    }
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(), duel.getDamageInfo());
                    //ServerProxy.informDistantMasters(actualAttacker, duel.getDamageInfo(), actualAttacker.getDefaultRange().getDistance());

                    System.out.println(duel.getRangeInfo());
                    if (duel.getRangeInfo() != null) {
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(), duel.getRangeInfo());
                        //ServerProxy.informDistantMasters(actualAttacker, duel.getRangeInfo(), actualAttacker.getDefaultRange().getDistance());
                    }
                }
            }

            if (duel.shouldBreak()) {
                if (duel.mustBreak) break;
                if (!duel.shouldNotBreak()) {
                    if (currentAttack.weapon.getFirearm() != null && currentAttack.weapon.getFirearm().isBreakable()
                            && (actualAttacker.getModifiersState().getModifier("stationary") == null || actualAttacker.getModifiersState().getModifier("stationary") != 1)
                    ) {
                        break;
                    } else if (!currentAttack.weapon.isRanged()) {
                        break;
                    }
                }
            }
            duel.incrementAttackNumber();
        }

        Player actualDefender1 = duel.getDefender(); // this is here because of CounterAttacks. And yes, it's copypaste. Sue me.
        Player actualAttacker1 = duel.getAttacker();

        if ((multishot || duel.mustBreak || duel.shouldBreak()) && !(duel.jammed() && duel.getActiveHand() != 2)) {
            System.out.println("attacks: " + duel.getAttacks().size());
            System.out.println("active: " + duel.getActiveAttackNumber());
            int attacksLeft = duel.getAttacks().size() - duel.getActiveAttackNumber() - 1;
            for (int i = 0; i < attacksLeft; i++) {
                System.out.println("i: " + i);
                int attack = duel.getActiveAttackNumber() + i + 1;
                if (multishot && attack > 2) break; //не тратим 5 патрон у пулемёта
                int hand = duel.getActiveHand();
                if(duel.getActiveHand() == 2) hand = 0; //вторая атака в парной всегда левая
                actualAttacker1.fireProjectile(hand);
                EntityLivingBase attEnt = actualAttacker1.getEntity();
                if (duel.getAttacks().get(attack).weapon.isRanged()) GunCus.channel.sendToAllAround(new MessageSound(attEnt, duel.getAttacks().get(attack).weapon.getSound() + "_shoot", 6F, 1F), new NetworkRegistry.TargetPoint(attEnt.world.provider.getDimension(), attEnt.posX, attEnt.posY, attEnt.posZ, duel.getAttacks().get(attack).weapon.getFirearm().getSoundRange()));
                System.out.println("ACTIVE ATTACK " + (duel.getActiveAttackNumber() + i + 1));
                System.out.println(duel.mustBreak + " duel.mustBreak duel.mustBreak duel.mustBreak duel.mustBreak");
                System.out.println((actualAttacker1.getItemForHand(hand).getItem() instanceof ItemGun) + " actualAttacker1.getItemForHand(hand).getItem() instanceof ItemGunactualAttacker1.getItemForHand(hand).getItem() instanceof ItemGunactualAttacker1.getItemForHand(hand).getItem() instanceof ItemGun");
//                    if (!duel.mustBreak && actualAttacker1.getItemForHand(hand).getItem() instanceof ItemGun) {
//                        System.out.println("TRUE TRUE TRUE TRUE TRUE");
//                        if (duel.getAttacks().get(attack).weapon.isRanged() && duel.getAttacks().get(attack).weapon.getFirearm().shouldBoom()) {
//                            System.out.println("TRUE TRUE TRUE TRUE TRUE");
//                            ItemStack is = actualAttacker1.getItemForHand(hand);
//                            ItemGun ig = (ItemGun) is.getItem();
//                            ig.doShoot(actualAttacker1.getEntity(), is, false, false, duel.getAttacks().get(attack).weapon.getFirearm().getSort());
//                        }
//                    } else if (!duel.mustBreak) {
//                        if (duel.getAttacks().get(attack).weapon.isRanged() && duel.getAttacks().get(attack).weapon.getFirearm().shouldBoom()) {
//                            EntityLivingBase attEnt = actualAttacker1.getEntity();
//                            switch (duel.getAttacks().get(attack).weapon.getFirearm().getSort()) {
//                                case 3:
//                                    GunCus.channel.sendToAllAround(new MessageSound(attEnt, GunCus.SOUND_SHOOT_HEAVY, 8F, 1F), new NetworkRegistry.TargetPoint(attEnt.world.provider.getDimension(), attEnt.posX, attEnt.posY, attEnt.posZ, 150F));
//                                    break;
//                                case 2:
//                                    GunCus.channel.sendToAllAround(new MessageSound(attEnt, GunCus.SOUND_SHOOT_MEDIUM, 7F, 1F), new NetworkRegistry.TargetPoint(attEnt.world.provider.getDimension(), attEnt.posX, attEnt.posY, attEnt.posZ, 100F));
//                                    break;
//                                case 1:
//                                    GunCus.channel.sendToAllAround(new MessageSound(attEnt, GunCus.SOUND_SHOOT_LIGHT, 6F, 1F), new NetworkRegistry.TargetPoint(attEnt.world.provider.getDimension(), attEnt.posX, attEnt.posY, attEnt.posZ, 80F));
//                                    break;
//                            }
//                        }
//                    }
            }
            if (!multishot && attacksLeft > 0 && duel.getActiveAttackNumber() == 0) { //отбираем прочность за второй выстрел, но не за расстрел
                int hand = duel.getActiveHand();
                if(duel.getActiveHand() == 2) hand = 0; //вторая атака в парной всегда левая\
                try {
                    WeaponDurabilityHandler weaponDurabilityHandler = actualAttacker1.getDurHandlerForHand(hand);
                    if (weaponDurabilityHandler.hasDurabilityForTag(duel.getAttacks().get(1).getWeapon().getName()))
                        weaponDurabilityHandler.takeAwayDurabilityForTag(duel.getAttacks().get(1).getWeapon().getName(), 1);
                } catch (Exception ignored) {

                }

            }
        }

        actualAttacker1.getPercentDice().setModules(0);
        actualDefender1.getPercentDice().setModules(0);

//        if (duel.hasReachedDifficulty() && duel.getDefense().getDefenseType() != DefenseType.NOTHING && !duel.getDefender().isCollected() && !duel.dontGivePreviousDebuff()) {
        if (duel.getDefense().getDefenseType() != DefenseType.NOTHING && !duel.getDefender().isCollected() && !duel.dontGivePreviousDebuff()) {
            duel.getDefender().setPreviousDefenses(duel.getDefender().getPreviousDefenses() + 1);
        }

        // MANAGE WOUNDS
        boolean alreadyFallen = false;


        List<MutablePair<Integer, String>> wounds = duel.getWounds();

        if (duel.suppressing) {
            MutablePair<Integer, String> bestPair = null;
            Integer dam = 0;
            for (MutablePair<Integer, String> pair : wounds) {
                Integer damage = (Integer) pair.getKey();
                if (damage > dam) {
                    bestPair = pair;
                    dam = damage;
                }
            }
            if (bestPair != null) {
                wounds.removeAll(wounds);
                wounds.add(bestPair);
            }
        }

        boolean rip = false;
        for (MutablePair<Integer, String> pair : wounds) {
            Player actualDefender = duel.getDefender(); // this is here because of CounterAttacks. And yes, it's copypaste. Sue me.
            Player actualAttacker = duel.getAttacker();
//            if (duel.successfulCounter) {
//                actualDefender = duel.getAttacker();
//                actualAttacker = duel.getDefender();
//            }
            Integer damage = (Integer) pair.getKey();
            //if (damage < 0) continue;
            String description = (String) pair.getValue();

            if (actualDefender.hasCourage() && damage > 6) {
                description += " <потеря концентрации>";
                actualDefender.removeStatusEffect(StatusType.CONCENTRATED);
            }
            if (actualDefender.hasStatusEffect(StatusType.PREPARING) && damage > 6) {
                description += " <потеря подготовки>";
                actualDefender.removeStatusEffect(StatusType.PREPARING);
            }

            StringBuilder modsStr = new StringBuilder();
            StringBuilder effectsStr = new StringBuilder();
            StringBuilder statusEffectsStr = new StringBuilder();

            WoundType woundType = WoundType.LETHAL;



            if (description.contains("[нелетальное оружие]")) woundType = WoundType.NONLETHAL;
            else if (description.contains("[критическое оружие]")) woundType = WoundType.CRITICAL;

            if (damage <= 0) description += " [погашенная атака]";;

            boolean forcedDeadly = false;
            if (description.contains("смертельная рана")) forcedDeadly = true;

            if (duel.isOpportunity()) {
                damage = damage/2;
                description += " [атака по возможности: 50% урона]";
            }

            if (duel.hasSplash()) {
                damage = (int) (damage*0.75);
                description += " [" + (duel.hasPiercing() ? "пронзание" : "размах") + ": 75% урона]";
            }

            if (description.contains("[")) {
                Matcher matcher = pattern.matcher(description);
                while (matcher.find()) {
                    if (modsStr.toString().isEmpty()) modsStr.append("§4На рану наложены модификаторы:\n");
                    modsStr.append("§c").append(matcher.group()).append("\n");
                }
            }
            if (description.contains("{")) {
                Matcher matcher = pattern2.matcher(description);
                while (matcher.find()) {
                    if (effectsStr.toString().isEmpty()) effectsStr.append("§6На атаку воздействовали эффекты:\n");
                    effectsStr.append("§5").append(matcher.group()).append("\n");
                }
                if(description.contains("{иссушение}")) {
                    actualDefender.setBloodloss(actualDefender.getBloodloss() + 5);
                    DataHolder.inst().getModifiers().updateBloodloss(actualDefender.getName(), actualDefender.getBloodloss());
                }
                if(description.contains("{вампиризм}")) {
                    actualAttacker.vampiricHeal(damage, actualDefender.getName());
                }
                if(description.contains("{кража маны}")) {
                    if(DataHolder.inst().useMana(actualDefender.getName(), 5)) DataHolder.inst().addMana(actualAttacker.getName(), 5);
                }
                description = matcher.replaceAll("");
            }
            if (description.contains("<")) {
                Matcher matcher = pattern3.matcher(description);
                while (matcher.find()) {
                    if (statusEffectsStr.toString().isEmpty()) statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                    statusEffectsStr.append("§3").append(matcher.group()).append("\n");
                }
                description = matcher.replaceAll("");
            }
            boolean persisted = false;
            SkillDiceMessage persistentDice = null;
            if (actualDefender.isFragile()) forcedDeadly = true;
//            if (damage > 9 || forcedDeadly) {
//                if (actualDefender.isPersistent() && !actualDefender.isIrresolute()) {
//                    persisted = true;
//                } else if (!actualDefender.isPersistent() && actualDefender.isIrresolute()) {
//                    persisted = false;
//                } else {
//                    persistentDice = new SkillDiceMessage(actualDefender.getName(), "% выносливость");
//                    persistentDice.build();
//                    if ((damage > 12 || forcedDeadly) && persistentDice.getDice().getResult() > 4) {
//                        persisted = true;
//                    } else if (!forcedDeadly && damage < 13 && persistentDice.getDice().getResult() > 1) {
//                        persisted = true;
//                    }
//                }
//            }
            String type = actualDefender.inflictDamage(damage, description, persisted, forcedDeadly, woundType);
            System.out.println("Inflicted " + damage + " damage");


            if (type.contains("нелетальн") && actualDefender.getWoundPyramid().getNumberOfWounds(WoundType.NONLETHAL) + actualDefender.getWoundPyramid().getNumberOfWounds(WoundType.LETHAL)
                    + actualDefender.getWoundPyramid().getNumberOfWounds(WoundType.CRITICAL) >= actualDefender.getWoundPyramid().getHealthPool()) {
                if (statusEffectsStr.toString().isEmpty()) statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                statusEffectsStr.append("§3").append("<оглушение>").append("\n");
                actualDefender.addStatusEffect("Оглушение", StatusEnd.TURN_END, 1, StatusType.STUNNED);
            }
            //Отнимаем разбег за раны
            Message fellOver = null;
            if(damage > 3) {
                try {
                    boolean defriding = false; //не отбираем разгон у всадников
                    if (DataHolder.inst().isNpc(actualDefender.getName())) {
                        Entity npc = DataHolder.inst().getNpcEntity(actualDefender.getName());
                        if (npc.getRidingEntity() != null) defriding = true;
                    } else {
                        Entity playerMP = ServerProxy.getForgePlayer(actualDefender.getName());
                        if (playerMP.getRidingEntity() != null) defriding = true;
                    }
                    if (!defriding && actualDefender.getMovementHistory() != MovementHistory.NONE) {
                        if (actualDefender.getMovementHistory() == MovementHistory.RUN_UP || actualDefender.getMovementHistory() == MovementHistory.LAST_TIME) {
                            actualDefender.setMovementHistory(MovementHistory.NONE);
                            //lostRun = new Message(" " + actualDefender.getName() + " теряет разбег.", ChatColor.RED);
                            if (statusEffectsStr.toString().isEmpty()) statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                            statusEffectsStr.append("§3").append("<потеря разбега>").append("\n");
                        } else if (damage > 6 && actualDefender.getMovementHistory() == MovementHistory.NOW) {
                            actualDefender.setMovementHistory(MovementHistory.NONE);
                            if (statusEffectsStr.toString().isEmpty()) statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                            statusEffectsStr.append("§3").append("<потеря разбега>").append("\n");
                            //lostRun = new Message(" " + actualDefender.getName() + " теряет разбег.", ChatColor.RED);
                        }
                    }
                    if (duel.hasCharged() && !alreadyFallen) {
                        boolean attriding = false; //всадник бьет круче.
                        if (DataHolder.inst().isNpc(actualAttacker.getName())) {
                            Entity npc = DataHolder.inst().getNpcEntity(actualAttacker.getName());
                            if (npc.getRidingEntity() != null) attriding = true;
                        } else {
                            Entity playerMP = ServerProxy.getForgePlayer(actualAttacker.getName());
                            if (playerMP.getRidingEntity() != null) attriding = true;
                        }
                        if (attriding || damage > 6) {
                            if (!actualDefender.isLying() && !actualDefender.cantBeCharged()) {
                                actualDefender.makeFall();
                                if (statusEffectsStr.toString().isEmpty()) statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                                statusEffectsStr.append("§3").append("<падение>").append("\n");
                                if (type.equals(WoundType.CRITICAL_DEPR.getDesc())) {
                                    fellOver = new Message(" Натиск успешен! " + actualDefender.getName() + " сражен!", ChatColor.RED);
                                } else if (type.equals(WoundType.DEADLY.getDesc())) {
                                    fellOver = new Message(" Натиск успешен! " + actualDefender.getName() + " повержен! Ура!", ChatColor.RED);
                                } else {
                                    fellOver = new Message(" Натиск успешен! " + actualDefender.getName() + " падает!", ChatColor.RED);
                                }
                                if (actualDefender.getEngagedWith() != null) {
                                    if (statusEffectsStr.toString().isEmpty()) statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                                    statusEffectsStr.append("§3").append("<потеря связывания боем>").append("\n");
                                    actualDefender.clearOpportunities();
                                }
                                if (actualDefender.hasCourage()) {
                                    if (statusEffectsStr.toString().isEmpty()) statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                                    statusEffectsStr.append("§3").append("<потеря концентрации>").append("\n");
                                    actualDefender.removeStatusEffect(StatusType.CONCENTRATED);
                                }
                                if (actualDefender.hasStatusEffect(StatusType.PREPARING)) {
                                    if (statusEffectsStr.toString().isEmpty()) statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                                    statusEffectsStr.append("§3").append("<потеря подготовки>").append("\n");
                                    actualDefender.removeStatusEffect(StatusType.PREPARING);
                                }
                            }
                            alreadyFallen = true;
                        }

                    } else if (effectsStr.toString().contains("{сногсшибательная сила}")) {
                        if (damage > 6 && !actualDefender.isLying() && !actualDefender.cantBeCharged()) {
                            actualDefender.makeFall();
                            if (statusEffectsStr.toString().isEmpty()) statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                            statusEffectsStr.append("§3").append("<падение>").append("\n");
                            if (actualDefender.getEngagedWith() != null) {
                                if (statusEffectsStr.toString().isEmpty()) statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                                statusEffectsStr.append("§3").append("<потеря связывания боем>").append("\n");
                                actualDefender.clearOpportunities();
                            }
                            if (actualDefender.hasCourage()) {
                                if (statusEffectsStr.toString().isEmpty()) statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                                statusEffectsStr.append("§3").append("<потеря концентрации>").append("\n");
                                actualDefender.removeStatusEffect(StatusType.CONCENTRATED);
                            }
                            if (actualDefender.hasStatusEffect(StatusType.PREPARING)) {
                                if (statusEffectsStr.toString().isEmpty()) statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                                statusEffectsStr.append("§3").append("<потеря подготовки>").append("\n");
                                actualDefender.removeStatusEffect(StatusType.PREPARING);
                            }
                            alreadyFallen = true;
                        }
                    } else {
                        if (actualDefender.hasCourage() && damage > 6) {
                            if (statusEffectsStr.toString().isEmpty())
                                statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                            statusEffectsStr.append("§3").append("<потеря концентрации>").append("\n");
                            actualDefender.removeStatusEffect(StatusType.CONCENTRATED);
                        }
                        if (actualDefender.hasStatusEffect(StatusType.PREPARING) && damage > 6) {
                            if (statusEffectsStr.toString().isEmpty())
                                statusEffectsStr.append("§9На защитника наложены эффекты:\n");
                            statusEffectsStr.append("§3").append("<потеря подготовки>").append("\n");
                            actualDefender.removeStatusEffect(StatusType.PREPARING);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

//            String type = WoundType.getDescFromDamage(damage);
            Message wound = new Message("Игроку " + actualDefender.getName() + " ", ChatColor.RED);
            MessageComponent typeC = new MessageComponent(type, ChatColor.RED);

            String ccc = (modsStr.toString().isEmpty() ? "" : modsStr.toString()) + (effectsStr.toString().isEmpty() ? "" : (modsStr.toString().isEmpty() ? "" : "\n") + effectsStr.toString()) + (statusEffectsStr.toString().isEmpty() ? "" : (modsStr.toString().isEmpty() || effectsStr.toString().isEmpty() ? "" : "\n") + statusEffectsStr.toString());
            if (!type.equals(WoundType.CRITICAL.getDesc())) persisted = false;
            if (!type.equals(WoundType.DEADLY.getDesc())) ccc = ccc.replace("§c[смертельная рана]\n", "");
            if (!type.equals(WoundType.DEADLY.getDesc()) && !type.equals(WoundType.CRITICAL_DEPR.getDesc())) ccc = ccc.replace("§c[нокаут]\n", "§c[оглушение]\n");
            StringBuilder persString = new StringBuilder();
            if (persistentDice != null) {
                persString.append("§6§nПроверка выносливости:\n");
                if (persisted) {
                    persString.append("§e").append(persistentDice.toString()).append("\n§eСложность была ").append(damage > 12 || forcedDeadly ? "§lпревосходно§e.\n" : "§lнормально§e.\n");
                } else {
                    persString.append("§e").append(persistentDice.toString()).append("\n§eСложность была ").append(damage > 12 || forcedDeadly ? "§lпревосходно§e.\n" : "§lнормально§e.\n")
                            .append("§4Не удалось выстоять перед §c").append(damage > 12 || forcedDeadly ? "смертельной раной" : "критической раной").append("§4!").append(damage > 12 || forcedDeadly ? " §4§lМгновенная смерть." : "");;
                }
            }
            if (persisted) {
                if (!persString.toString().isEmpty()) persString.append("\n");
                persString.append("§2§lУдалось выстоять перед §c§l").append(damage > 12 || forcedDeadly ? "смертельной раной" : "критической раной").append("§2§l! §2Рана снижена до §cтяжелой§2.");
            }
            if (!persString.toString().isEmpty()) {
                if (!ccc.isEmpty()) {
                    ccc += "\n";
                    ccc += persString.toString();
                } else {
                    ccc = persString.toString();
                }
            }
            if(!ccc.isEmpty()) {
                typeC.setHoverText(new TextComponentString(ccc.trim()));
                typeC.setUnderlined(true);
            }

            MessageComponent dot = new MessageComponent(".", ChatColor.RED);
            wound.addComponent(typeC);
            wound.addComponent(new MessageComponent(" " + actualDefender.getWoundPyramid().toNiceString(actualDefender.getName())));
            wound.addComponent(dot);
            if(fellOver != null) {
                wound.addComponent(fellOver);
            }
            ServerProxy.sendMessageFromTwoSourcesAndInformMasters(actualAttacker, actualDefender, actualAttacker.getDefaultRange(), wound);
            //ServerProxy.informDistantMasters(actualAttacker, wound, actualAttacker.getDefaultRange().getDistance());
            if (actualDefender.getWoundPyramid().getNumberOfWounds(WoundType.CRITICAL) >= actualDefender.getWoundPyramid().getHealthPool() && !player.hasTrait(Trait.LAST_CHANCE)) {
                try {
                    if (DataHolder.inst().isNpc(duel.getDefender().getName())) {
                        EntityNPCInterface npc = (EntityNPCInterface) DataHolder.inst().getNpcEntity(duel.getDefender().getName());
                        npc.wrappedNPC.getAi().setAnimation(AnimationType.SLEEP);
                        String desc = npc.wrappedNPC.getDisplay().getTitle();
                        npc.wrappedNPC.getDisplay().setTitle((desc + " [в отключке]").trim());
                        npc.wrappedNPC.getDisplay().setHasLivingAnimation(false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //actualDefender.makeFall();
                try {
                    Player gm = DataHolder.inst().getMasterForPlayerInCombat(actualDefender);
                    gm.performCommand("/queue remove " + actualDefender.getName());
                    gm.performCommand("/combat remove " + actualDefender.getName());
                    rip = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


        }

        // RIPOSTE
        if(duel.successfulRiposte()) {
            Player actualDefender = duel.getDefender();
            Player actualAttacker = duel.getAttacker();
            if (actualDefender.getCombatState() == CombatState.SHOULD_WAIT && !actualDefender.getModifiersState().isModifier("noriposte")) {
                Message riposte = new Message("Успешный рипост! " + actualAttacker.getName() + " открыт для ответного удара!", ChatColor.BLUE);
                ServerProxy.sendMessageFromTwoSourcesAndInformMasters(attacker, actualDefender, actualAttacker.getDefaultRange(), riposte);
                //ServerProxy.informDistantMasters(attacker, riposte, actualAttacker.getDefaultRange().getDistance());
                try {
                    Queue queue = DataHolder.inst().getCombatForPlayer(actualDefender.getName()).getReactionList().getQueue();
                    queue.shift(actualDefender, actualAttacker.getName(), true);
                    //ServerProxy.sendMessage(actualDefender, "§9Вы можете атаковать " + actualAttacker.getName() + " с +1 от концентрации (не стакается).");
                } catch (Exception ignored) {
                    ignored.printStackTrace();
                }
                actualDefender.riposted = actualAttacker;
            } else {
                Message riposte = new Message("Атака отбита!", ChatColor.BLUE);
                ServerProxy.sendMessageFromTwoSourcesAndInformMasters(attacker, actualDefender, actualAttacker.getDefaultRange(), riposte);
                //ServerProxy.informDistantMasters(attacker, riposte, actualAttacker.getDefaultRange().getDistance());
            }
        }

        // SET RECOIL
        if (!duel.noRecoil) {
            if (isRanged) {
//            if (attacker.getModifiersState().getModifier(ModifiersState.serial) != null && attacker.getModifiersState().getModifier(ModifiersState.serial) == 1) {
                System.out.println("setting recoil to " + recoil);
                if (!multishot) {
                    attacker.setRecoil(recoil);
                    if (recoil < 0 && recoilTurns > 0) attacker.addStatusEffect("Отдача", StatusEnd.TURN_END, recoilTurns,  StatusType.RECOIL, -Math.abs(recoil), (attacker.getCombatState() == CombatState.SHOULD_ACT));
                }
//            } else {
//                attacker.setRecoil(0);
//            }
            } else {
                attacker.setRecoil(0);
            }
        } else {
            attacker.setRecoil(0);
        }

        attacker.getPercentDice().setBipod(0);
        attacker.getPercentDice().setAmmo(0);
        attacker.getPercentDice().setLimbInjury(0);
        attacker.getPercentDice().setStock(0);
        attacker.getPercentDice().setHighground(0);
        actualDefender1.getPercentDice().setHighground(0);
        attacker.setMovementHistory(MovementHistory.NONE);
        attacker.setChargeHistory(ChargeHistory.NONE);
        attacker.riposted = null;
//        if (DataHolder.inst().isNpc(duel.getDefender().getName())) {
//            NPC npc = (NPC) DataHolder.inst().getPlayer(duel.getDefender().getName());
//            npc.setWoundsInDescription();
//        }
        if (duel.unsuccessfulCounter) {
            actualDefender1.setCombatState(CombatState.ACTED);
            try {
                int recoil1 = ((Weapon) duel.getDefense().getDefenseItem()).getFirearm().getRecoil();
                if (((Weapon) duel.getDefense().getDefenseItem()).getFirearm().hasSingleRecoil()) actualDefender1.setRecoil(recoil1);
                int hand = duel.getDefense().getActiveHand();
                actualDefender1.fireProjectile(duel.getDefense().getActiveHand());
                Weapon weapon = (Weapon) duel.getDefense().getDefenseItem();
                EntityLivingBase forgePlayer = actualDefender1.getEntity();
                WeaponDurabilityHandler weaponDurabilityHandler = new WeaponDurabilityHandler(actualDefender1.getItemForHand(hand));
                if (weaponDurabilityHandler.hasDurabilityForTag(weapon.getName()))
                    weaponDurabilityHandler.takeAwayDurabilityForTag(weapon.getName(), 1);
                ServerProxy.sendMessageFromAndInformMasters(actualDefender1, new Message(player.getName() + " " + "стреляет!", ChatColor.RED));
                if (weapon.isRanged()) GunCus.channel.sendToAllAround(new MessageSound(forgePlayer, weapon.getSound() + "_shot", 6F, 1F), new NetworkRegistry.TargetPoint(forgePlayer.world.provider.getDimension(), forgePlayer.posX, forgePlayer.posY, forgePlayer.posZ, weapon.getFirearm().getSoundRange()));
//                if (weapon.getFirearm().shouldBoom()) {
//                    if (actualDefender1.getItemForHand(hand).getItem() instanceof ItemGun) {
//                        try {
//                            ItemStack is = actualDefender1.getItemForHand(hand);
//                            ItemGun gun = (ItemGun) is.getItem();
//                            //System.out.println(gun.hasOptic(is));
//
//                            gun.doShoot(forgePlayer, is, false, false, weapon.getFirearm().getSort());
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    } else {
//                        switch (weapon.getFirearm().getSort()) {
//                            case 3:
//                                GunCus.channel.sendToAllAround(new MessageSound(forgePlayer, GunCus.SOUND_SHOOT_HEAVY, 8F, 1F), new NetworkRegistry.TargetPoint(forgePlayer.world.provider.getDimension(), forgePlayer.posX, forgePlayer.posY, forgePlayer.posZ, 150F));
//                                break;
//                            case 2:
//                                GunCus.channel.sendToAllAround(new MessageSound(forgePlayer, GunCus.SOUND_SHOOT_MEDIUM, 7F, 1F), new NetworkRegistry.TargetPoint(forgePlayer.world.provider.getDimension(), forgePlayer.posX, forgePlayer.posY, forgePlayer.posZ, 100F));
//                                break;
//                            case 1:
//                                GunCus.channel.sendToAllAround(new MessageSound(forgePlayer, GunCus.SOUND_SHOOT_LIGHT, 6F, 1F), new NetworkRegistry.TargetPoint(forgePlayer.world.provider.getDimension(), forgePlayer.posX, forgePlayer.posY, forgePlayer.posZ, 80F));
//                                break;
//                        }
//                    }
//                }
            } catch (Exception ignored) {

            }
        } else if (duel.successfulCounter) {
            try {
                WeaponTagsHandler defWeaponh = new WeaponTagsHandler(duel.getDefender().getItemForHand(duel.getDefense().getActiveHand()));
                Weapon weapon = new Weapon(defWeaponh.getDefaultWeaponName(), defWeaponh.getDefaultWeapon());
                if (weapon.isRanged()) {
                    int recoil1 = weapon.getFirearm().getRecoil();
                    if (weapon.getFirearm().hasSingleRecoil()) actualDefender1.setRecoil(recoil1);
                }
            } catch (Exception exception) {

            }
        }
        boolean splash = duel.hasSplash();
        boolean multicast = duel.getMulticast() > 0;
        boolean isOpportunity = duel.isOpportunity();
        List<MutablePair<String, Integer>> cooldowns = duel.getCooldowns();
        boolean mustbr = duel.mustBreak;
        DuelMaster.end(duel);

        if (multishot && mustbr) {
            if (DataHolder.inst().hasRealDuelAttacker(attacker.getName())) {
                ArrayList<Duel> attackerDuels = DataHolder.inst().getAllDuelsForAttacker(attacker.getName());
                for (Duel duel1 : attackerDuels) {
                    DuelMaster.end(duel1);
                }
                Message riposte = new Message("Все последующие атаки отменены!", ChatColor.RED);
                ServerProxy.sendMessageFromAndInformMasters(attacker, riposte);
            }
        }

        if (combat != null) {
            System.out.println("Combat state of " + attacker.getName() + " " + attacker.getCombatState());
            if (duel.getNextDuelDefended() != null) {
                duel.getNextDuelDefended().getActiveAttack().getDiceMessage().unbuild();
                duel.getNextDuelDefended().getActiveAttack().getDiceMessage().getDice().setAlreadyCast(false);
                DataHolder.inst().registerDuel(duel.getNextDuelDefended());
                Message succ = new Message(attacker.getName() + " проходит через защиту " + actualDefender1.getName() + "!", ChatColor.BLUE);
                ServerProxy.sendMessageFromTwoSourcesAndInformMasters(attacker, actualDefender1, attacker.getDefaultRange(), succ);
                actualDefender1.addStatusEffect("Растерян", StatusEnd.ROUND_START, 2, StatusType.CONFUSED);
                duel.getNextDuelDefended().initDefense();
                return;
            } if (isOpportunity && !rip) {
                boolean proced = false;
                if (actualDefender1.getEngagedBy().size() > 0) {
                        ArrayList<Player> opp = new ArrayList<Player>(actualDefender1.getEngagedBy());
                        for (Player opponent : opp) {
                            proced = opponent.executeOpportunity();
                            if (proced) break;
                        }
                }
                if (!proced) {
                    if (actualDefender1.getCombatState() == CombatState.SHOULD_ACT) {
                        if (DataHolder.inst().isNpc(actualDefender1.getName())) {
                            if (actualDefender1.hasStatusEffect(StatusType.STUNNED)) {
                                DataHolder.inst().getMasterForNpcInCombat(actualDefender1).performCommand("/endturn combat");
                            } else {
                                DataHolder.inst().getMasterForNpcInCombat(actualDefender1).performCommand("/toolpanel");
                            }
                        } else {
                            actualDefender1.performCommand("/toolpanel");
                            if (actualDefender1.hasStatusEffect(StatusType.STUNNED)) {
                                actualDefender1.performCommand("/endturn combat");
                            } else {
                                actualDefender1.performCommand("/toolpanel");
                            }
                        }
                    }
                }
            } else if (attacker.getCombatState() == CombatState.SHOULD_ACT && ((!splash && !multicast && !multishot) || !DataHolder.inst().hasRealDuelAttacker(attacker.getName()))) {
                for (MutablePair<String, Integer> cooldown : cooldowns) {
                    attacker.addStatusEffect("Восстановление", StatusEnd.TURN_END, cooldown.getValue(), StatusType.COOLDOWN, cooldown.getKey(), (attacker.getCombatState() == CombatState.SHOULD_ACT));
                }
                if (multishot) attacker.setRecoil(recoil);
                DataHolder.inst().getMasterForCombat(combat.getId()).performCommand("/endturn combat");
            } else if (attacker.getCombatState() != CombatState.SHOULD_ACT && ((!splash && !multicast && !multishot && duel.getBeingDefended() == null) || !DataHolder.inst().hasRealDuelAttacker(attacker.getName()))) {
                for (MutablePair<String, Integer> cooldown : cooldowns) {
                    attacker.addStatusEffect("Восстановление", StatusEnd.TURN_END, cooldown.getValue(), StatusType.COOLDOWN, cooldown.getKey(), (attacker.getCombatState() == CombatState.SHOULD_ACT));
                }
                if (multishot) attacker.setRecoil(recoil);
            }
        }
        if (duel.successfulCounter) {
            actualAttacker1.setCombatState(CombatState.ACTED);
            actualAttacker1.processTurnEnd(actualAttacker1, "combat", DataHolder.inst().getCombatForPlayer(actualAttacker1.getName()));
            if (actualDefender1.getCombatState() == CombatState.SHOULD_ACT) {
                DataHolder.inst().getMasterForPlayerInCombat(actualDefender1).performCommand("/endturn combat");
            }
        }
    }


}
