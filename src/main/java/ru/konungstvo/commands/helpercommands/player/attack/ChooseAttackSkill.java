package ru.konungstvo.commands.helpercommands.player.attack;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import org.json.simple.parser.ParseException;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.FudgeDiceMessage;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.Skill;
import ru.konungstvo.combat.SkillType;
import ru.konungstvo.combat.dice.modificator.Modificator;
import ru.konungstvo.combat.dice.modificator.ModificatorType;
import ru.konungstvo.combat.duel.Attack;
import ru.konungstvo.combat.duel.DefenseType;
import ru.konungstvo.combat.duel.Duel;
import ru.konungstvo.combat.equipment.Equipment;
import ru.konungstvo.combat.equipment.Fist;
import ru.konungstvo.combat.equipment.Shield;
import ru.konungstvo.combat.equipment.Weapon;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.kmrp_lore.helpers.SkillSaverHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.ModifiersState;
import ru.konungstvo.player.Player;

import java.util.Arrays;
import java.util.List;

public class ChooseAttackSkill extends CommandBase {
    public static String START_MESSAGE = "Выберите навык!\n";
    public static final String NAME = "chooseskill";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        // Скиллы
        String skill = "";
        if (args.length > 1) {
            skill = args[1];
            skill = skill.replaceAll("_", " ");
        }

        if(skill.equals("!EMPTY!")) skill = "";
        System.out.println("Skill is: " + skill);

        boolean unarmed = false;
        if (skill.endsWith("!REALONE!")) {
            unarmed = true;
            skill = skill.replace("!REALONE!", "");
        }

        String skillSecond = "";
        if (args.length > 2) {
            skillSecond = args[2];
            skillSecond = skillSecond.replaceAll("_", " ");
        }
        if(skillSecond.equals("!EMPTY!")) skillSecond = "";
        System.out.println("SkillSecond is: " + skillSecond);

        boolean isPairedAttack = !skillSecond.isEmpty();
        if(args.length > 0 && args[0].equals("!PleaseBePaired!")) isPairedAttack = true;
        if(skillSecond.equals(("!PleaseBePaired!"))) {
            isPairedAttack = true;
            skillSecond = "";
        }


        // GET PLAYER OR NPC
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player attacker = DataHolder.inst().getPlayer(sender.getName());
        EntityLivingBase forgePlayer = getCommandSenderAsPlayer(sender);
        if (attacker.getSubordinate() != null) {
            attacker = attacker.getSubordinate();
            forgePlayer = (EntityLivingBase) DataHolder.inst().getNpcEntity(attacker.getName());
            if (!DataHolder.inst().isNpc(attacker.getName())) {
                forgePlayer = ServerProxy.getForgePlayer(attacker.getName());
            }
        }

        // GET DUEL
        Duel duel = DataHolder.inst().getDuelForAttacker(attacker.getName());
        WeaponTagsHandler weaponTagsHandler = null;
        WeaponTagsHandler weaponTagsHandlerSecond = null;
        Weapon weapon = null;
        Weapon weaponSecond = null;
        SkillSaverHandler skillSaver = null;
        SkillSaverHandler skillSaverSecond = null;

        // Берём итемы из рук
        ItemStack activeItem = null;
        ItemStack activeItemSecond = null;
        if (duel.getActiveHand() == 0) {
            activeItem = forgePlayer.getHeldItemOffhand();
        } else if (duel.getActiveHand() == 1) {
            activeItem = forgePlayer.getHeldItemMainhand();
        } else if (duel.getActiveHand() == 2) {
            // ПАРНОЕ
            activeItem = forgePlayer.getHeldItemMainhand();
            activeItemSecond = forgePlayer.getHeldItemOffhand();
        }

        System.out.println("activeItem is : " + activeItem.toString());
        if (isPairedAttack && activeItemSecond != null)
            System.out.println("activeItemSecond is : " + activeItemSecond.toString());

        weaponTagsHandler = new WeaponTagsHandler(activeItem);
        skillSaver = new SkillSaverHandler(activeItem);
        if (isPairedAttack && activeItemSecond != null) {
            weaponTagsHandlerSecond = new WeaponTagsHandler(activeItemSecond);
            skillSaverSecond = new SkillSaverHandler(activeItemSecond);
        }

        // ЕСЛИ АТАКА ОРУЖИЕ
        System.out.println("Skill is " + skill);
        System.out.println("HTH is " + DefenseType.HAND_TO_HAND.toNameString());
        if (skill.equals(DefenseType.HAND_TO_HAND.toNameString()) && weaponTagsHandler.isShield()) {
            weapon = new Shield(weaponTagsHandler.getDefaultWeaponName(), weaponTagsHandler.getDefaultWeapon());
            // Если рукопашка на кулаках
        } else if (!unarmed) {
            // Создать оружие
            weapon = new Weapon(weaponTagsHandler.getDefaultWeaponName(), weaponTagsHandler.getDefaultWeapon(), weaponTagsHandler);
            if (weaponTagsHandler.isMachineGun()) {
                System.out.println(weaponTagsHandler.getDefaultWeapon().getCompoundTag("rangedFirearm").getString("sort") + " weaponTagsHandler.getDefaultWeapon().getCompoundTag(\"rangedFirearm\").getString(\"sort\")");
                System.out.println(weapon.getFirearm().getSort() + " weapon.getFirearm().getSort()");
                if (weapon.getFirearm().getSort() != 1) { //Легкий пулемёт необязательно стреляет серийкой
                    attacker.getModifiersState().setModifier(ModifiersState.serial, 1);
                }
            }
            if (isPairedAttack && skillSaverSecond != null)
                weaponSecond = new Weapon(weaponTagsHandlerSecond.getDefaultWeaponName(), weaponTagsHandlerSecond.getDefaultWeapon(), weaponTagsHandlerSecond);

            // Сохранить навык владения в лоре оружия
            if (!skill.isEmpty()) skillSaver.setSkillSaver(attacker.getName(), SkillType.MASTERING.toString(), skill);
            if (isPairedAttack && skillSaverSecond != null) skillSaverSecond.setSkillSaver(attacker.getName(), SkillType.MASTERING.toString(), skill); //TODO переделать как-то
            System.out.println("Saved " + skill);
            System.out.println("2Saved " + skillSecond);
            // ЕСЛИ РУКОПАШКА с щитом
        } else {
            weapon = new Fist();
        }


        // Установить количество атак
        int amountOfAttacks = 1;
        System.out.println("serial: " + attacker.getModifiersState().getModifier(ModifiersState.serial));

        // Чекаем режим серийного огня
        boolean serial = false;
        if (attacker.getModifiersState().getModifier(ModifiersState.serial) != null && attacker.getModifiersState().getModifier(ModifiersState.serial) == 1) {
            //  Если у оружия нет серийного огня!
            if (!(weaponTagsHandler.isSerial() || weaponTagsHandler.isDouble())) {
                System.out.println("not serial");
                TextComponentString noSerial = new TextComponentString("Серийный огонь недоступен, будет произведен одиночный выстрел.");
                noSerial.getStyle().setColor(TextFormatting.GRAY);
                sender.sendMessage(noSerial);
                amountOfAttacks = 1;
            } else if (weaponTagsHandler.isMachineGun()) {
                System.out.println("machine");
                amountOfAttacks = 5;
                serial = true;
            } else if (weaponTagsHandler.isDouble() && !isPairedAttack) {
                System.out.println("double");
                amountOfAttacks = 2;
                duel.setShouldNotBreak(true);
                serial = true;
            } else {
                System.out.println("regular");
                amountOfAttacks = 3;
                serial = true;
            }
        }

        // СФОРМИРОВАТЬ ДАЙС АТАКИ

        // От плохо
        if (args.length > 0 && args[0].equals("default")) { // Не может быть Рукопашным боем
            //dice.build();
            for (int i = 0; i < amountOfAttacks; i++) {
                // Добавить атаку
                // Создаём первый дайс
                FudgeDiceMessage dice = new FudgeDiceMessage(attacker.getName(), "% плохо");
                if (attacker.getModifiersState().getModifier("dice") != null) {
                    Modificator custom = new Modificator(attacker.getModifiersState().getModifier("dice"), ModificatorType.CUSTOM);
                    dice.getDice().addMod(custom);
                    System.out.println("Adding attackerDiceMod which is " + custom);
                }
                if (attacker.getModifiersState().getModifier("percent") != null) {
                    attacker.getPercentDice().setCustom(attacker.getModifiersState().getModifier("percent"));
                    System.out.println("Custom percent set to " + attacker.getPercentDice().getCustom());
                }
                if (!weaponTagsHandler.isUnarmed()) {
                    if (serial && weaponTagsHandler.isDouble()) {
                        dice.getDice().addMod(new Modificator(-1, ModificatorType.DOUBLE));
                        duel.setShouldNotBreak(true);
                    } else if (i > 0) {
                        dice.getDice().addMod(new Modificator(-i, ModificatorType.SERIAL));
                        System.out.println("Added serial mod " + (-i));
                    }
                }
                duel.addAttack(new Attack(dice, weapon));
                // FIRE PROJECTILE!!!
//                weaponTagsHandler.fireProjectile();
            }
            if (isPairedAttack) {
                for (int i = 0; i < amountOfAttacks; i++) {
                    // Добавить атаку
                    // Создаём второй дайс
                    FudgeDiceMessage diceSecond = null;
                    diceSecond = new FudgeDiceMessage(attacker.getName(), "% плохо");
                    if (attacker.getModifiersState().getModifier("dice") != null) {
                        Modificator custom = new Modificator(attacker.getModifiersState().getModifier("dice"), ModificatorType.CUSTOM);
                        diceSecond.getDice().addMod(custom);
                        System.out.println("Adding attackerDiceMod which is " + custom);
                    }
                    if (attacker.getModifiersState().getModifier("percent") != null) {
                        attacker.getPercentDice().setCustom(attacker.getModifiersState().getModifier("percent"));
                        System.out.println("Custom percent set to " + attacker.getPercentDice().getCustom());
                    }
                    if (weaponTagsHandler.isDouble() && serial) {
                        diceSecond.getDice().addMod(new Modificator(-1, ModificatorType.DOUBLE));
                        duel.setShouldNotBreak(true);
                    } else if (i > 0) {
                        diceSecond.getDice().addMod(new Modificator(-i, ModificatorType.SERIAL));
                        System.out.println("Added serial mod " + (-i));
                    }
                    duel.addAttack(new Attack(diceSecond, weaponSecond));
                    // FIRE PROJECTILE!!!
//                    weaponTagsHandlerSecond.fireProjectile();
                }
            }

            // Переходим к выбору оппонента
            ClickContainer.deleteContainer("ChooseSkill", getCommandSenderAsPlayer(sender));
            System.out.println("TEST " + (!isPairedAttack && weaponTagsHandler.isFirearm() && weaponTagsHandler.getDefaultWeapon().hasKey("magic") && weaponTagsHandler.getDefaultWeapon().getCompoundTag("magic").hasKey("multicast") &&
                    weaponTagsHandler.getDefaultWeapon().getCompoundTag("magic").getInteger("multicast") > 0));
            if (!isPairedAttack && weaponTagsHandler.isFirearm() && weaponTagsHandler.getDefaultWeapon().hasKey("magic") && weaponTagsHandler.getDefaultWeapon().getCompoundTag("magic").hasKey("multicast") &&
                    weaponTagsHandler.getDefaultWeapon().getCompoundTag("magic").getInteger("multicast") > 0) {
                NBTTagCompound tag = weaponTagsHandler.getDefaultWeapon().getCompoundTag("magic");
                int multicast = 0;
                int chain = 0;
                int radius = 0;
                if (tag.hasKey("multicast")) multicast = tag.getInteger("multicast");
                if (tag.hasKey("chain")) chain = tag.getInteger("chain");
                if (tag.hasKey("radius")) radius = tag.getInteger("radius");
                player.performCommand(SelectOpponentCommand.NAME + " multicast " + multicast + (radius > 0 ? " 0123радиус=" + radius: "") + (radius == 0 && chain > 0 ? " 0123цепь=" + chain : ""));
            } else {
                player.performCommand(SelectOpponentCommand.NAME);
            }

            // От навыка, если он уже выбран
        } else if (args.length > 0 && args[0].equals("ready") && !skill.isEmpty() && (!isPairedAttack || !skillSecond.isEmpty())) {

            System.out.println("Weapon is : " + weapon.toString());

            // Добавляем все атаки
            for (int i = 0; i < amountOfAttacks; i++) {
                // Добавить атаку
                // Создаём дайс
                SkillDiceMessage dice = new SkillDiceMessage(attacker.getName(), "% " + skill);
                if (attacker.getModifiersState().getModifier("dice") != null) {
                    Modificator custom = new Modificator(attacker.getModifiersState().getModifier("dice"), ModificatorType.CUSTOM);
                    dice.getDice().addMod(custom);
                }
                if (attacker.getModifiersState().getModifier("percent") != null) {
                    attacker.getPercentDice().setCustom(attacker.getModifiersState().getModifier("percent"));
                    System.out.println("Custom percent set to " + attacker.getPercentDice().getCustom());
                }

                if (!weaponTagsHandler.isUnarmed()) {
                    if (serial && weaponTagsHandler.isDouble()) {
                        dice.getDice().addMod(new Modificator(-1, ModificatorType.DOUBLE));
                        duel.setShouldNotBreak(true);
                    } else if (i > 0) {
                        dice.getDice().addMod(new Modificator(-i, ModificatorType.SERIAL));
                        System.out.println("Added serial mod " + (-i));
                    }
                }
                System.out.println("Created dice: " + dice.getDice());
                duel.addAttack(new Attack(dice, weapon));
                // FIRE PROJECTILE!!!
//                weaponTagsHandler.fireProjectile();
            }
            if (isPairedAttack) {
                for (int i = 0; i < amountOfAttacks; i++) {
                    // Добавить атаку
                    // Создаём второй дайс
                    SkillDiceMessage diceSecond = null;
                    diceSecond = new SkillDiceMessage(attacker.getName(), "% " + skillSecond);

                    if (attacker.getModifiersState().getModifier("dice") != null) {
                        Modificator custom = new Modificator(attacker.getModifiersState().getModifier("dice"), ModificatorType.CUSTOM);
                        diceSecond.getDice().addMod(custom);
                    }
                    if (attacker.getModifiersState().getModifier("percent") != null) {
                        attacker.getPercentDice().setCustom(attacker.getModifiersState().getModifier("percent"));
                        System.out.println("Custom percent set to " + attacker.getPercentDice().getCustom());
                    }
                    if (i > 0) {
                        diceSecond.getDice().addMod(new Modificator(-i, ModificatorType.SERIAL));
                        System.out.println("Added serial mod " + (-i));
                    }
                    duel.addAttack(new Attack(diceSecond, weaponSecond));
                    // FIRE PROJECTILE!!!
//                    weaponTagsHandlerSecond.fireProjectile();
                }
            }


            // FIRE PROJECTILE!!!
            if (weaponTagsHandler.isFirearm()) {
//                weaponTagsHandler.fireProjectile();
            }
            if (isPairedAttack && weaponTagsHandlerSecond.isFirearm()) {
//                weaponTagsHandlerSecond.fireProjectile();
            }

//            Message answer = new Message("Вы выбрали " + skill, ChatColor.GRAY);
//            player.sendMessage(answer);

            // Перейти к выбору оппонента
            ClickContainer.deleteContainer("ChooseSkill", getCommandSenderAsPlayer(sender));
//            System.out.println("1" + !isPairedAttack);
//            System.out.println("1" + weaponTagsHandler.isFirearm());
//            System.out.println("1" + weaponTagsHandler.getDefaultWeapon().hasKey("magic"));
//            System.out.println("1" + weaponTagsHandler.getDefaultWeapon().getCompoundTag("magic").hasKey("multicast"));
//            System.out.println("1" + (weaponTagsHandler.getDefaultWeapon().getCompoundTag("magic").getInteger("multicast") > 0));
//            System.out.println("TEST " + (!isPairedAttack && weaponTagsHandler.isFirearm() && weaponTagsHandler.getDefaultWeapon().hasKey("magic") && weaponTagsHandler.getDefaultWeapon().getCompoundTag("magic").hasKey("multicast") &&
//                    weaponTagsHandler.getDefaultWeapon().getCompoundTag("magic").getInteger("multicast") > 0));
            if (!isPairedAttack && weaponTagsHandler.isFirearm() && weaponTagsHandler.getDefaultWeapon().hasKey("magic") && weaponTagsHandler.getDefaultWeapon().getCompoundTag("magic").hasKey("multicast") &&
                    weaponTagsHandler.getDefaultWeapon().getCompoundTag("magic").getInteger("multicast") > 0) {
                NBTTagCompound tag = weaponTagsHandler.getDefaultWeapon().getCompoundTag("magic");
                int multicast = 0;
                int chain = 0;
                int radius = 0;
                if (tag.hasKey("multicast")) multicast = tag.getInteger("multicast");
                if (tag.hasKey("chain")) chain = tag.getInteger("chain");
                if (tag.hasKey("radius")) radius = tag.getInteger("radius");
                player.performCommand(SelectOpponentCommand.NAME + " multicast " + multicast + (radius > 0 ? " 0123радиус=" + radius: "") + (radius == 0 && chain > 0 ? " 0123цепь=" + chain : ""));
            } else {
                player.performCommand(SelectOpponentCommand.NAME);
            }

            // Сгенерировать выбор навыка
        } else if (skill.isEmpty()) {

            // GENERATE CLICK PANEL
            Message message = new Message(START_MESSAGE, ChatColor.COMBAT);
            List<Skill> attackSkills = attacker.getAttackSkills();

            if(skillSecond.isEmpty() && isPairedAttack) skillSecond = "!PleaseBePaired!";

            if (args.length > 0 && args[0].equals("more")) {
                attackSkills = attacker.getSkills();
                ClickContainer.deleteContainer("ChooseSkill", getCommandSenderAsPlayer(sender));
            }

            for (Skill sk : attackSkills) {
                String skillStr = Character.toUpperCase(sk.getName().charAt(0)) + sk.getName().substring(1);
                MessageComponent mes = new MessageComponent("[" + skillStr + "] ", ChatColor.BLUE);
                mes.setClickCommand(String.format("/%s ready %s %s", NAME, sk.getName().replace(" ", "_"), skillSecond.replace(" ", "_")));
                message.addComponent(mes);
            }


            if (args.length == 0 || !args[0].equals("more")) {
                MessageComponent more = new MessageComponent("[Другое] ", ChatColor.BLUE);
                more.setClickCommand(String.format("/%s more", NAME));
                message.addComponent(more);
            }


            MessageComponent res = new MessageComponent("[Плохо] ", ChatColor.BLUE);
            res.setClickCommand(String.format("/%s default", NAME));
            message.addComponent(res);

            MessageComponent modifiers = new MessageComponent("[Мод] ", ChatColor.GRAY);
            modifiers.setClickCommand("/modifiersbutton ChooseSkill");
            message.addComponent(modifiers);

            MessageComponent toRoot = new MessageComponent("[Назад] ", ChatColor.GRAY);
            //toRoot.setBold(true);
            toRoot.setClickCommand("/toroot ChooseSkill " + attacker.getName());
            message.addComponent(toRoot);


            // SEND PACKET TO CLIENT
            Helpers.sendClickContainerToClient(message, "ChooseSkill", getCommandSenderAsPlayer(sender), attacker);


        } else if (skillSecond.isEmpty() && isPairedAttack) {
            // GENERATE CLICK PANEL
            Message message = new Message("Выберите навык для второго оружия!\n", ChatColor.COMBAT);
            List<Skill> attackSkills = attacker.getAttackSkills();

            if (args.length > 0 && args[0].equals("more")) {
                attackSkills = attacker.getSkills();
                ClickContainer.deleteContainer("ChooseSkill", getCommandSenderAsPlayer(sender));
            }

            for (Skill sk : attackSkills) {
                String skillStr = Character.toUpperCase(sk.getName().charAt(0)) + sk.getName().substring(1);
                MessageComponent mes = new MessageComponent("[" + skillStr + "] ", ChatColor.BLUE);
                mes.setClickCommand(String.format("/%s ready %s %s", NAME, skill.replace(" ", "_"), sk.getName().replace(" ", "_")));
                message.addComponent(mes);
            }


//            if (args.length == 0 || !args[0].equals("more")) {
//                MessageComponent more = new MessageComponent("[Другое] ", ChatColor.BLUE); //TODO допилить
//                more.setClickCommand(String.format("/%s more", NAME));
//                message.addComponent(more);
//            }
//
//
//            MessageComponent res = new MessageComponent("[Плохо] ", ChatColor.BLUE); //TODO допилить
//            res.setClickCommand(String.format("/%s default", NAME));
//            message.addComponent(res);
//
//            MessageComponent modifiers = new MessageComponent("[Мод] ", ChatColor.GRAY); //TODO допилить
//            modifiers.setClickCommand("/modifiersbutton ChooseSkill");
//            message.addComponent(modifiers);

            MessageComponent toRoot = new MessageComponent("[Назад] ", ChatColor.GRAY);
            //toRoot.setBold(true);
            toRoot.setClickCommand("/toroot ChooseSkill " + attacker.getName());
            message.addComponent(toRoot);


            // SEND PACKET TO CLIENT
            Helpers.sendClickContainerToClient(message, "ChooseSkill", getCommandSenderAsPlayer(sender), attacker);
        }


    }
}
