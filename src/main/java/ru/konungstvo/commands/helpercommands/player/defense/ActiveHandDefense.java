package ru.konungstvo.commands.helpercommands.player.defense;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import org.json.simple.parser.ParseException;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.combat.duel.DefenseType;
import ru.konungstvo.combat.duel.Duel;
import ru.konungstvo.combat.equipment.Equipment;
import ru.konungstvo.combat.equipment.Shield;
import ru.konungstvo.combat.equipment.Weapon;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.player.turn.ToRoot;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.Player;

public class ActiveHandDefense extends CommandBase {
    public static final String NAME = "activehanddefense";
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }
    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        String hand = args[0];
        String type = args[1];
        boolean parry = false;
        boolean counter = false;
        // GET PLAYER OR NPC
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player defender = DataHolder.inst().getPlayer(sender.getName());
        EntityLivingBase forgePlayer = getCommandSenderAsPlayer(sender);
        if (defender.getSubordinate() != null) {
            defender = defender.getSubordinate();
            forgePlayer = (EntityLivingBase) DataHolder.inst().getNpcEntity(defender.getName());
            if (!DataHolder.inst().isNpc(defender.getName())) {
                forgePlayer = ServerProxy.getForgePlayer(defender.getName());
            }
        }
        Duel duel = DataHolder.inst().getDuelForDefender(defender.getName());

        ClickContainer.deleteContainer("SelectDefense", getCommandSenderAsPlayer(sender));

        ItemStack leftHand = forgePlayer.getHeldItemOffhand();
        ItemStack rightHand = forgePlayer.getHeldItemMainhand();
        WeaponTagsHandler leftWeapon = new WeaponTagsHandler(leftHand);
        WeaponTagsHandler rightWeapon = new WeaponTagsHandler(rightHand);
        Weapon weapon = null;
        Message message;

        if (hand.equals("0")) {
            if (type.equals(DefenseType.PARRY.name())) {
                weapon = new Weapon(leftWeapon.getDefaultWeaponName(), leftWeapon.getDefaultWeapon());
                parry = true;
            } else if (type.equals(DefenseType.BLOCKING.name())) {
                weapon = new Shield(leftWeapon.getDefaultWeaponName(), leftWeapon.getDefaultWeapon());
            } else if (type.equals(DefenseType.COUNTER.name())) {
                weapon = new Weapon(leftWeapon.getDefaultWeaponName(), leftWeapon.getDefaultWeapon());
                counter = true;
            }
            /*
            try {
                weapon.fillFromNBT(leftWeapon.getDefaultWeapon());
            } catch (ParseException e) {
                e.printStackTrace();
            }

             */

            if (parry && duel.getActiveAttack().getWeapon().isRanged() && weapon != null) {
                if (weapon.getReach() < ServerProxy.getDistanceBetween(duel.getAttacker(), defender)) {
                    player.sendError(new Exception("Дальность вашего оружия (" + weapon.getReach() + ") не позволяет парировать эту дальнюю атаку."));
                    player.performCommand(ToRoot.NAME + " ActiveHandDefense");
                    return;
                }
            }

            if (counter) {
                if (duel.isUncommon()) {
                    if (weapon.getReach() < ServerProxy.getDistanceBetween(duel.getAttacker(), defender)) {
                        player.sendError(new Exception("Нельзя контратаковать атаку этого типа."));
                        player.performCommand(ToRoot.NAME + " SelectDefense");
                        return;
                    }
                }
                if (!duel.getActiveAttack().getWeapon().isRanged()) {
                    if (weapon.getReach() < ServerProxy.getDistanceBetween(duel.getAttacker(), defender)) {
                        player.sendError(new Exception("Дальность вашего оружия (" + weapon.getReach() + ") не позволяет контратаковать эту дальнюю атаку."));
                        player.performCommand(ToRoot.NAME + " SelectDefense");
                        return;
                    }
                }


                if (duel.getActiveAttack().getWeapon().isMelee() && weapon.getReach() < duel.getActiveAttack().getWeapon().getReach()) {
                    player.sendError(new Exception("Дальность вашего оружия (" + weapon.getReach() + ") не позволяет контратаковать."));
                    player.performCommand(ToRoot.NAME + " SelectDefense");
                    return;
                }

                if (!DataHolder.inst().eligibleForCounter(defender)) {
                    System.out.println(weapon.isMelee() && (duel.getActiveAttack().getWeapon().isRanged() || duel.getActiveAttack().getWeapon().isFist() || weapon.getMelee().getReach() > duel.getActiveAttack().getWeapon().getMelee().getReach()));
                    System.out.println(weapon.isMelee());
                    System.out.println(duel.getActiveAttack().getWeapon().isRanged());
                    System.out.println(weapon.isFist());
                    if (duel.getActiveAttack().getWeapon().isMelee()) System.out.println(weapon.getMelee().getReach() > duel.getActiveAttack().getWeapon().getMelee().getReach());
                    if (weapon.isMelee() && (duel.getActiveAttack().getWeapon().isRanged() || duel.getActiveAttack().getWeapon().isFist() || weapon.getMelee().getReach() > duel.getActiveAttack().getWeapon().getMelee().getReach())) {

                    } else {
                        player.sendError(new Exception("Контратака недоступна."));
                        player.performCommand(ToRoot.NAME + " SelectDefense");
                        return;
                    }
                }
            }

            if (parry) duel.getDefense().setDefenseType(DefenseType.PARRY); //костыль но пиздец
            if (counter) duel.getDefense().setDefenseType(DefenseType.COUNTER);
            duel.getDefense().setActiveHand(0);
            duel.getDefense().setDefenseItem(weapon);

            message = new Message("Вы выбрали предмет в левой руке.");
            player.sendMessage(message);
            ClickContainer.deleteContainer("ActiveHandDefense", getCommandSenderAsPlayer(sender));

            player.performCommand("/" + ChooseDefenseSkill.NAME + " " + type);

        } else if (hand.equals("1")) {
            if (type.equals(DefenseType.PARRY.name())) {
                weapon = new Weapon(rightWeapon.getDefaultWeaponName(), rightWeapon.getDefaultWeapon());
                parry = true;
            } else if (type.equals(DefenseType.BLOCKING.name())) {
                weapon = new Shield(rightWeapon.getDefaultWeaponName(), rightWeapon.getDefaultWeapon());
            }else if (type.equals(DefenseType.COUNTER.name())) {
                weapon = new Weapon(rightWeapon.getDefaultWeaponName(), rightWeapon.getDefaultWeapon());
                counter = true;
            }
            /*
            try {
                weapon.fillFromNBT(leftWeapon.getDefaultWeapon());
            } catch (ParseException e) {
                e.printStackTrace();
            }

             */

            if (parry && duel.getActiveAttack().getWeapon().isRanged() && weapon != null) {
                if (weapon.getReach() < ServerProxy.getDistanceBetween(duel.getAttacker(), defender)) {
                    player.sendError(new Exception("Дальность вашего оружия (" + weapon.getReach() + ") не позволяет парировать эту дальнюю атаку."));
                    player.performCommand(ToRoot.NAME + " ActiveHandDefense");
                    return;
                }
            }

            if (counter) {
                if (!duel.getActiveAttack().getWeapon().isRanged()) {
                    if (weapon.getReach() < ServerProxy.getDistanceBetween(duel.getAttacker(), defender)) {
                        player.sendError(new Exception("Дальность вашего оружия (" + weapon.getReach() + ") не позволяет контратаковать эту дальнюю атаку."));
                        player.performCommand(ToRoot.NAME + " SelectDefense");
                        return;
                    }
                }


                if (duel.getActiveAttack().getWeapon().isMelee() && weapon.getReach() < duel.getActiveAttack().getWeapon().getReach()) {
                    player.sendError(new Exception("Дальность вашего оружия (" + weapon.getReach() + ") не позволяет контратаковать."));
                    player.performCommand(ToRoot.NAME + " SelectDefense");
                    return;
                }

                if (!DataHolder.inst().eligibleForCounter(defender)) {
                    System.out.println(weapon.isMelee() && (duel.getActiveAttack().getWeapon().isRanged() || duel.getActiveAttack().getWeapon().isFist() || weapon.getMelee().getReach() > duel.getActiveAttack().getWeapon().getMelee().getReach()));
                    System.out.println(weapon.isMelee());
                    System.out.println(duel.getActiveAttack().getWeapon().isRanged());
                    System.out.println(weapon.isFist());
                    if (duel.getActiveAttack().getWeapon().isMelee()) System.out.println(weapon.getMelee().getReach() > duel.getActiveAttack().getWeapon().getMelee().getReach());
                    if (weapon.isMelee() && (duel.getActiveAttack().getWeapon().isRanged() || duel.getActiveAttack().getWeapon().isFist() || weapon.getMelee().getReach() > duel.getActiveAttack().getWeapon().getMelee().getReach())) {

                    } else {
                        player.sendError(new Exception("Контратака недоступна."));
                        player.performCommand(ToRoot.NAME + " SelectDefense");
                        return;
                    }
                }
            }

            if (parry) duel.getDefense().setDefenseType(DefenseType.PARRY); //костыль но пиздец
            if (counter) duel.getDefense().setDefenseType(DefenseType.COUNTER);
            duel.getDefense().setActiveHand(1);
            duel.getDefense().setDefenseItem(weapon);

            message = new Message("Вы выбрали предмет в правой руке.");
            player.sendMessage(message);
            ClickContainer.deleteContainer("ActiveHandDefense", getCommandSenderAsPlayer(sender));

            player.performCommand("/" + ChooseDefenseSkill.NAME + " " + type);
        }


    }
}
