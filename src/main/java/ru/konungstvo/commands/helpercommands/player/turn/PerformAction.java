package ru.konungstvo.commands.helpercommands.player.turn;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.StatusType;
import ru.konungstvo.combat.movement.MovementHistory;
import ru.konungstvo.commands.executor.FeintExecutor;
import ru.konungstvo.commands.executor.NextExecutor;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.player.attack.AttackCommand;
import ru.konungstvo.commands.helpercommands.player.movement.MoveExecutor;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.player.Player;

public class PerformAction  extends CommandBase {
    public static final String NAME = "performaction";
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }
    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;

    }
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player subordinate = player;
        if (player.getSubordinate() != null) {
            player = player.getSubordinate();
            subordinate = player;
        }
        // REMOVE PREVIOUS PANEL
        ClickContainer.deleteContainer("ToolPanel", getCommandSenderAsPlayer(sender));
        boolean from_movement = false;

        if (args.length > 0 && args[0].equals("from_movement")) {
            System.out.println("Завершаем движение при выборе действия");
            from_movement = true;
            DataHolder.inst().getPlayer(sender.getName()).performCommand("/go end antiforced");
        } else if (args.length > 0 && args[0].equals("stopprep")) {
            DataHolder.inst().getPlayer(sender.getName()).performCommand("/go end antiforced");
            player.removeStatusEffect(StatusType.PREPARING);
        } else if (args.length > 0 && args[0].equals("stopdef")) {
            DataHolder.inst().getPlayer(sender.getName()).performCommand("/go end antiforced");
            player.setDefensiveStance(false);
        }

        // ASSEMBLE PANEL
        Message panel = new Message("Выберите действие!\n", ChatColor.COMBAT);

        if (player.hasStatusEffect(StatusType.PREPARING)) {
            MessageComponent action = new MessageComponent("[Прервать подготовку] ", ChatColor.RED);
            action.setClickCommand("/performaction stopprep");

            MessageComponent pass = new MessageComponent("[Продолжать подготовку] ", ChatColor.BLUE);
            pass.setClickCommand("/" + NextExecutor.NAME + " magic");

            MessageComponent modifiers = new MessageComponent("[Мод] ", ChatColor.GRAY);
            modifiers.setClickCommand("/modifiersbutton ToolPanel");

            panel.addComponent(action);
            panel.addComponent(pass);
            panel.addComponent(modifiers);

            // SEND PACKET TO CLIENT
            Helpers.sendClickContainerToClient(panel, "PerformAction", (EntityPlayerMP) sender, subordinate);
        } else if (player.hasDefensiveStance()) {
            MessageComponent action = new MessageComponent("[Прервать глухую оборону] ", ChatColor.RED);
            action.setClickCommand("/performaction stopdef");

            MessageComponent pass = new MessageComponent("[Продолжать глухую оборону] ", ChatColor.BLUE);
            pass.setClickCommand("/defensivestance continue");

            MessageComponent modifiers = new MessageComponent("[Мод] ", ChatColor.GRAY);
            modifiers.setClickCommand("/modifiersbutton ToolPanel");

            panel.addComponent(action);
            panel.addComponent(pass);
            panel.addComponent(modifiers);

            // SEND PACKET TO CLIENT
            Helpers.sendClickContainerToClient(panel, "PerformAction", (EntityPlayerMP) sender, subordinate);
        } else {
            if (player.hasStatusEffect(StatusType.RELENTLESS_PSYCH) || player.hasStatusEffect(StatusType.RELENTLESS)) {
                MessageComponent attack = new MessageComponent("[Атака оружием] ", ChatColor.GRAY);
                panel.addComponent(attack);
                MessageComponent unarmed = new MessageComponent("[Без оружия] ", ChatColor.GRAY);
                panel.addComponent(unarmed);
            } else {
                MessageComponent attack = new MessageComponent("[Атака оружием] ", ChatColor.BLUE);
                attack.setClickCommand("/" + AttackCommand.NAME + " armed");
                panel.addComponent(attack);

                MessageComponent unarmed = new MessageComponent("[Без оружия] ", ChatColor.BLUE);
                unarmed.setClickCommand("/" + AttackCommand.NAME + " unarmed");
                panel.addComponent(unarmed);
            }


            if (player.hasStatusEffect(StatusType.RELENTLESS_PSYCH) || player.hasStatusEffect(StatusType.RELENTLESS) || player.hasStatusEffect(StatusType.LUNGE)) {
                MessageComponent feint = new MessageComponent("[Выбрать приём] ", ChatColor.GRAY);
                feint.setHoverText("Вы уже использовали приём в этом раунде!", TextFormatting.RED);
                panel.addComponent(feint);
            } else if (player.hasRemovableFeints()) {
                MessageComponent feint = new MessageComponent("[Отменить приём] ", ChatColor.RED);
                feint.setHoverText("Не использовать выбранный приём в этом ходу.", TextFormatting.RED);
                feint.setClickCommand("/" + FeintExecutor.NAME + " clearall PerformAction");
                panel.addComponent(feint);
            } else {
                MessageComponent feint = new MessageComponent("[Выбрать приём] ", ChatColor.GMCHAT);
                feint.setClickCommand("/" + FeintExecutor.NAME + " %IGNORE% PerformAction");
                panel.addComponent(feint);
            }

            if (player.hasStatusEffect(StatusType.BURNING)) {
                MessageComponent extinguish = new MessageComponent("[Потушиться] ", ChatColor.RED);
                extinguish.setClickCommand("/" + NextExecutor.NAME + " extinguish");
                extinguish.setHoverText("Упасть и кататься!", TextFormatting.BLUE);
                panel.addComponent(extinguish);
            }


            if (!player.getMovementHistory().equals(MovementHistory.NONE)) {
                MessageComponent customAction = new MessageComponent("[Конец движения] ", ChatColor.BLUE);
                customAction.setClickCommand("/" + NextExecutor.NAME + " resetmovement");
                panel.addComponent(customAction);
            } else {
//                MessageComponent customAction = new MessageComponent("[Конец хода] ", ChatColor.BLUE);
//                customAction.setClickCommand("/" + NextExecutor.NAME + " resetmovement");
//                panel.addComponent(customAction);
            }

//        MessageComponent pass = new MessageComponent("[Закончить ход] ", ChatColor.BLUE);
//        pass.setClickCommand("/" + NextExecutor.NAME);

            MessageComponent modifiers = new MessageComponent("[Мод] ", ChatColor.GRAY);
            modifiers.setClickCommand("/modifiersbutton PerformAction");
            panel.addComponent(modifiers);

            if (player.getMovementHistory().equals(MovementHistory.NONE)) {
                MessageComponent toRoot = new MessageComponent("[Назад] ", ChatColor.GRAY);
                //toRoot.setBold(true);
                toRoot.setClickCommand("/" + ToRoot.NAME + " PerformAction" + " ToolPanel");
                panel.addComponent(toRoot);
            }


            // SEND PACKET TO CLIENT
            Helpers.sendClickContainerToClient(panel, "PerformAction", (EntityPlayerMP) sender, subordinate);
        }

    }
}
