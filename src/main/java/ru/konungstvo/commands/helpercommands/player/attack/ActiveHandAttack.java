package ru.konungstvo.commands.helpercommands.player.attack;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.combat.SkillType;
import ru.konungstvo.combat.duel.DefenseType;
import ru.konungstvo.combat.duel.Duel;
import ru.konungstvo.combat.equipment.Weapon;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.kmrp_lore.helpers.SkillSaverHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.Player;

public class ActiveHandAttack extends CommandBase {
    public static final String NAME = "activehand";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }
    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        // REMOVE PREVIOUS CONTAINER
        ClickContainer.deleteContainer("ActiveHand", getCommandSenderAsPlayer(sender));


        int hand = Integer.parseInt(args[0]);
        String type = args[1];

        // GET PLAYER OR NPC
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player attacker = DataHolder.inst().getPlayer(sender.getName());
        EntityLivingBase forgePlayer = getCommandSenderAsPlayer(sender);
        if (attacker.getSubordinate() != null) {
            attacker = attacker.getSubordinate();
            forgePlayer = (EntityLivingBase) DataHolder.inst().getNpcEntity(attacker.getName());
            if (!DataHolder.inst().isNpc(attacker.getName())) {
                forgePlayer = ServerProxy.getForgePlayer(attacker.getName());
            }
        }

        // GET DUEL
        Duel duel = DataHolder.inst().getDuelForAttacker(attacker.getName());
        duel.setActiveHand(hand);

        if (type.equals("unarmed")) {
            player.performCommand(String.format("/%s ready " + "сила+ловкость" + "!REALONE!", ChooseAttackSkill.NAME));
        } else {

            String skill;
            String skillSecond = "";
//            SkillSaverHandler skillSaver = null;
//            SkillSaverHandler skillSaverSecond = null;
            WeaponTagsHandler weaponTagsHandler = null;
            Weapon weapon = null;
            ItemStack activeItem = null;
            if (hand == 0) {
                activeItem = forgePlayer.getHeldItemOffhand();
            } else {
                activeItem = forgePlayer.getHeldItemMainhand();
            }
            weaponTagsHandler = new WeaponTagsHandler(activeItem);
            weapon = new Weapon(weaponTagsHandler.getDefaultWeaponName(), weaponTagsHandler.getDefaultWeapon(), weaponTagsHandler);

            skill = weapon.getSkill();
            System.out.println("test123 " + duel.getAttacks().size());
            if (hand == 2) {
                activeItem = forgePlayer.getHeldItemOffhand();
                weaponTagsHandler = new WeaponTagsHandler(activeItem);
                weapon = new Weapon(weaponTagsHandler.getDefaultWeaponName(), weaponTagsHandler.getDefaultWeapon(), weaponTagsHandler);
                skillSecond = weapon.getSkill();
            }

            System.out.println(skill);
            System.out.println(skillSecond);

            if (skill.isEmpty()) {
                SkillSaverHandler skillSaver = null;
                SkillSaverHandler skillSaverSecond = null;
                if (hand == 0) {
                    skillSaver = new SkillSaverHandler(forgePlayer.getHeldItemOffhand());
                } else if (hand == 1) {
                    skillSaver = new SkillSaverHandler(forgePlayer.getHeldItemMainhand());
                } else if (hand == 2) {
                    // ПАРНОЕ
                    // БЕРЁМ ДВА СКИЛЛСЕЙВЕРА
                    skillSaver = new SkillSaverHandler(forgePlayer.getHeldItemMainhand());
                    skillSaverSecond = new SkillSaverHandler(forgePlayer.getHeldItemOffhand());
                }

                if (skillSaver == null || skillSaver.isEmpty()) {
                    // CHOOSE SKILL
                    if (hand != 2) {
                        player.performCommand(ChooseAttackSkill.NAME);
                        return;
                    } else {
                        player.performCommand(ChooseAttackSkill.NAME + " !PleaseBePaired!");
                        return;
                    }
                }

                // GET SKILL FROM WEAPON
                skill = skillSaver.getSkillSaverFor(attacker.getName(), SkillType.MASTERING.toString());
                skill = skill.replaceAll(" ", "_");
                if (skillSaverSecond != null) {
                    skillSecond = skillSaverSecond.getSkillSaverFor(attacker.getName(), SkillType.MASTERING.toString());
                    skillSecond = skillSecond.replaceAll(" ", "_");
                }
            }
//            if (hand == 0) {
//                skillSaver = new SkillSaverHandler(forgePlayer.getHeldItemOffhand());
//            } else if (hand == 1) {
//                skillSaver = new SkillSaverHandler(forgePlayer.getHeldItemMainhand());
//            } else if (hand == 2) {
//                // ПАРНОЕ
//                // БЕРЁМ ДВА СКИЛЛСЕЙВЕРА
//                skillSaver = new SkillSaverHandler(forgePlayer.getHeldItemMainhand());
//                skillSaverSecond = new SkillSaverHandler(forgePlayer.getHeldItemOffhand());
//            }
//
//            if (skillSaver == null || skillSaver.isEmpty()) {
//                // CHOOSE SKILL
//                if (hand != 2) {
//                    player.performCommand(ChooseAttackSkill.NAME);
//                    return;
//                } else {
//                    player.performCommand(ChooseAttackSkill.NAME + " !PleaseBePaired!");
//                    return;
//                }
//            }

//             GET SKILL FROM WEAPON
            skill = skill.replaceAll(" ", "_");
            if (!skillSecond.isEmpty()) {
                skillSecond = skillSecond.replaceAll(" ", "_");
            }
            if (skill.equals("")) skill = "!EMPTY!";
            if (skillSecond.equals("")) skillSecond = "!EMPTY!";

            // ЕСЛИ ПАРНАЯ, ОТПРАВЛЯЕМ ДВА СКИЛЛА
            if (hand == 2 && skillSecond.equals("!EMPTY!")) {
                player.performCommand(ChooseAttackSkill.NAME + " ready " + skill + " " + "!PleaseBePaired!");
            } else {
                player.performCommand(ChooseAttackSkill.NAME + " ready " + skill + " " + skillSecond);
            }
        }

    }
}
