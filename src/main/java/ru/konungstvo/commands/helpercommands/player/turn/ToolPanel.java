package ru.konungstvo.commands.helpercommands.player.turn;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.StatusType;
import ru.konungstvo.combat.movement.ChargeHistory;
import ru.konungstvo.combat.movement.GoExecutor;
import ru.konungstvo.combat.movement.MovementHistory;
import ru.konungstvo.commands.executor.NextExecutor;
import ru.konungstvo.commands.executor.TriggerOpportunityExecutor;
import ru.konungstvo.commands.helpercommands.player.movement.MoveExecutor;
import ru.konungstvo.commands.helpercommands.player.movement.PerformMovement;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.player.Player;

public class ToolPanel extends CommandBase {
    public static final String NAME = "toolpanel";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }
    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;

    }
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        if (player.getSubordinate() != null) player = player.getSubordinate();
        Player subordinate = player;
        // GET EFFECTS
        DataHolder.inst().getPlayer(sender.getName()).performCommand("/geteffects");

        // ASSEMBLE PANEL
        if (args.length > 0 && args[0].equals("stopprep")) player.removeStatusEffect(StatusType.PREPARING);
        if (args.length > 0 && args[0].equals("stopdef")) player.setDefensiveStance(false);
        Message toolPanel = new Message("Ваш ход!\n", ChatColor.COMBAT);

        if (player.hasStatusEffect(StatusType.PREPARING)) {
            MessageComponent action = new MessageComponent("[Прервать подготовку] ", ChatColor.RED);
            action.setClickCommand("/toolpanel stopprep");

            MessageComponent movement = null;
            if (player.getEngagedBy().size() > 0) {
                movement = new MessageComponent("[Разорвать бой] ", ChatColor.RED);
                movement.setClickCommand("/" + TriggerOpportunityExecutor.NAME + " ToolPanel");
                StringBuilder opportunities = new StringBuilder();
                for (Player engaged : player.getEngagedBy()) {
                    opportunities.append(engaged.getName()).append(" ");
                }
                movement.setHoverText("[" + opportunities.toString().trim().replaceAll(" ", ", ") + "]", TextFormatting.BLUE);
            } else {
                //if (player.getMovementHistory() == MovementHistory.NONE || player.getMovementHistory() == MovementHistory.LAST_TIME) { //TODO: sus
                    movement = new MessageComponent("[Свободное передвижение] ", ChatColor.BLUE);
                    movement.setClickCommand("/" + MoveExecutor.NAME + " free");
                //}
            }

            MessageComponent pass = new MessageComponent("[Продолжать подготовку] ", ChatColor.BLUE);
            pass.setClickCommand("/" + NextExecutor.NAME + " magic");

            MessageComponent modifiers = new MessageComponent("[Мод] ", ChatColor.GRAY);
            modifiers.setClickCommand("/modifiersbutton ToolPanel");

            MessageComponent close = new MessageComponent("[Закрыть] ", ChatColor.GRAY);
            close.setClickCommand("/" + "containerpurge");

            toolPanel.addComponent(action);
            toolPanel.addComponent(movement);
            toolPanel.addComponent(pass);
            toolPanel.addComponent(modifiers);
            toolPanel.addComponent(close);

            Helpers.sendClickContainerToClient(toolPanel, "ToolPanel", (EntityPlayerMP) sender, subordinate);
        } else {
            MessageComponent action = new MessageComponent("[Действие] ", ChatColor.BLUE);
            action.setClickCommand("/performaction");

            MessageComponent stopdef = new MessageComponent("[Прервать глухую оборону] ", ChatColor.RED);
            stopdef.setClickCommand("/toolpanel stopdef");

            MessageComponent movement = null;
            if (player.hasActingMount()) {
                movement = new MessageComponent("[Передвижение] ", ChatColor.GRAY);
                movement.setStrikethrough(true);
                movement.setHoverText("Вы верхом на существе, которое участвует в очереди", TextFormatting.RED);
            } else {
                boolean hasMountOrRidersEngaged = player.hasMountOrRidersEngaged();
                if (player.getEngagedBy().size() > 0 || hasMountOrRidersEngaged) {
                    movement = new MessageComponent("[Разорвать бой] ", ChatColor.RED);
                    movement.setClickCommand("/" + TriggerOpportunityExecutor.NAME + " ToolPanel" + (hasMountOrRidersEngaged ? " riders" : ""));
                    StringBuilder opportunities = new StringBuilder();
                    for (Player engaged : player.getEngagedBy()) {
                        opportunities.append(engaged.getName()).append(" ");
                    }
                    movement.setHoverText("[" + opportunities.toString().trim().replaceAll(" ", ", ") + (hasMountOrRidersEngaged ? (player.getEngagedBy().size() > 0 ? ", " : "") + "Кто-то из пассажиров/маунтов втянут в бой" : "") +"]", TextFormatting.BLUE);
                } else {
                    //if (player.getMovementHistory() == MovementHistory.NONE) { //TODO: sus
                        movement = new MessageComponent("[Передвижение] ", ChatColor.BLUE);
                        if (player.getMovementHistory() != MovementHistory.NONE) {
                            movement.setHoverText("Вы уже двигались в этом раунде!", TextFormatting.BLUE);
                            movement.setColor(ChatColor.GRAY);
                        }
                        movement.setClickCommand("/" + PerformMovement.NAME);
                    //}
                }
            }

            MessageComponent shift = new MessageComponent("[Отложить ход] ", ChatColor.BLUE);
            shift.setClickCommand("/performshift");

            MessageComponent charge = null;
            if (player.getMovementHistory() == MovementHistory.NOW) {
                if (player.getChargeHistory() == ChargeHistory.NONE) {
                    charge = new MessageComponent("[Начать натиск] ", ChatColor.RED);
                } else {
                    action.setColor(ChatColor.RED);
                    action.setHoverText("Натиск готов.\nВперёд!", TextFormatting.DARK_RED);
                    charge = new MessageComponent("[Продолжить натиск] ", ChatColor.BLUE);
                }
                charge.setHoverText("Закончит ваш ход. Если на следующий ход ваша скорость сохранится, вы сможете атаковать оружием ближнего боя с бонусом.", TextFormatting.DARK_GREEN);
                charge.setClickCommand("/" + NextExecutor.NAME + " charge");
            }

            MessageComponent pass = new MessageComponent("[Конец хода] ", ChatColor.BLUE);
            pass.setClickCommand("/" + NextExecutor.NAME + " resetrecoil");

            MessageComponent modifiers = new MessageComponent("[Мод] ", ChatColor.GRAY);
            modifiers.setClickCommand("/modifiersbutton ToolPanel");

            MessageComponent close = new MessageComponent("[Закрыть] ", ChatColor.GRAY);
            close.setClickCommand("/" + "containerpurge");

        if (movement != null) {
            if (player.getChargeHistory() != ChargeHistory.NONE && (player.getMovementHistory() == MovementHistory.LAST_TIME || player.getMovementHistory() == MovementHistory.NOW)) {
                movement.setColor(ChatColor.RED);
                movement.setHoverText("Пробегите ещё, чтобы использовать натиск!", TextFormatting.DARK_RED);
            }
        }

        if (!player.hasDefensiveStance()) {
            toolPanel.addComponent(action);
        } else {
            toolPanel.addComponent(stopdef);
        }
        toolPanel.addComponent(movement);
        if (!player.hasShifted()) toolPanel.addComponent(shift);
        toolPanel.addComponent(pass);
        if (charge != null) toolPanel.addComponent(charge);
        toolPanel.addComponent(modifiers);
        toolPanel.addComponent(close);

        // SEND PACKET TO CLIENT
        Helpers.sendClickContainerToClient(toolPanel, "ToolPanel", (EntityPlayerMP) sender, subordinate);
    }





    }
}
