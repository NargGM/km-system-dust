package ru.konungstvo.commands.helpercommands.player.attack;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.combat.duel.Duel;
import ru.konungstvo.combat.duel.DuelMaster;
import ru.konungstvo.combat.equipment.BodyPart;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.player.turn.ToRoot;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.Player;

import javax.management.remote.rmi._RMIConnection_Stub;
import java.util.ArrayList;
import java.util.List;

public class PerformAttackCommand extends CommandBase {
    public static final String NAME = "performattack";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }
    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        String opponentName = args[0];

        // GET PLAYER OR NPC
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player attacker = DataHolder.inst().getPlayer(sender.getName());
        if (attacker.getSubordinate() != null) {
            attacker = attacker.getSubordinate();
        }

        Duel duel = DataHolder.inst().getDuelForAttacker(attacker.getName());


//        //DOUBLE CHECK
//        double weaponRange = 1;
//        // GET WEAPON RANGE
//        List<Player> targets = new ArrayList<>();
//        int activeHand = duel.getActiveHand();
//        if (activeHand == 2) activeHand = 1;
//        EntityLivingBase forgePlayer = attacker.getEntity();
//        ItemStack itemStack = forgePlayer.getHeldItem(
//                Helpers.getEnumHand(activeHand)
//        );
//
//        WeaponTagsHandler weaponTagsHandler = null;
//        if (WeaponTagsHandler.hasWeaponTags(itemStack)) {
//            weaponTagsHandler = new WeaponTagsHandler(itemStack);
//            weaponRange = weaponTagsHandler.getWeaponRange();
//            weaponRange++;
//        } else {
//            weaponRange = 2;
//        }
//
//
//        if (weaponTagsHandler != null && weaponTagsHandler.isFirearm()) {
//            targets = new ArrayList<>(DataHolder.inst().getCombatForPlayer(attacker.getName()).getFighters());
////            targets = ServerProxy.getVisibleTargets(attacker);
//        } else {
//            targets = ServerProxy.getAllFightersInRange(attacker, weaponRange);
//        }
//
//        if(!targets.contains(DataHolder.inst().getPlayer(opponentName))) {
//            TextComponentString error;
//            if (weaponTagsHandler != null && weaponTagsHandler.isFirearm()) {
//                error = new TextComponentString("Ошибка! " + opponentName + " нет в поле вашего зрения!");
//            } else {
//                error = new TextComponentString("Ошибка! В радиусе дальности вашего оружия (<" + weaponRange + ") нет " + opponentName + ".");
//
//            }
//            error.getStyle().setColor(TextFormatting.RED);
//            sender.sendMessage(error);
//            player.performCommand(ToRoot.NAME + " ToolPanel");
//            DuelMaster.end(duel);
//            return;
//        }
//
//        //


        if (opponentName.contains("%SPLASH%") || opponentName.contains("%SPLASHP%")) {
            String[] opponents = opponentName.split(":");
            boolean piercing = false;
            if (opponentName.contains("%SPLASHP%")) piercing = true;
            for (String opponent : opponents) {
                System.out.println(opponentName);
                System.out.println(opponent);
                if (opponent.equals("%SPLASH%") || opponent.equals("%SPLASHP%") || opponent.isEmpty()) continue;
                Duel newDuel = duel.copy();
                DataHolder.inst().registerDuel(newDuel);
                newDuel.setSplash(true);
                if (piercing) newDuel.setPiercing(true);
                newDuel.setDefender(DataHolder.inst().getPlayer(opponent));
                newDuel.initDefense();
            }
            DuelMaster.end(duel);
        } else if (opponentName.contains("%MULTICAST%")) {
            System.out.println("MULTIREEE");
            System.out.println("MULTIREEE" + duel != null);
            String[] opponents = opponentName.split(":");
            int counter = 0;
            for (String opponent : opponents) {
                if (opponent.equals("%MULTICAST%") || opponent.isEmpty()) continue;
                counter++;
                System.out.println(opponentName);
                System.out.println(opponent);
                Duel newDuel = duel.copy();
                DataHolder.inst().registerDuel(newDuel);
                newDuel.setMulticast(counter);
                newDuel.setDefender(DataHolder.inst().getPlayer(opponent));
                newDuel.initDefense();
            }
        } else if (opponentName.contains("%MULTISHOT%")) {
            System.out.println("MULTIREEE");
            System.out.println("MULTIREEE" + duel != null);
            String[] opponents = opponentName.split(":");
            for (String opponent : opponents) {
                System.out.println(opponentName);
                System.out.println(opponent);
                if (opponent.equals("%MULTISHOT%") || opponent.isEmpty()) continue;
                Duel newDuel = duel.copy();
                DataHolder.inst().registerDuel(newDuel);
                newDuel.setMultishot(true);
                newDuel.setDefender(DataHolder.inst().getPlayer(opponent));
                newDuel.initDefense();
            }
        } else {
            System.out.println(DataHolder.inst().getPlayer(opponentName));
            Player defender = DataHolder.inst().getPlayer(opponentName);
            while (defender.getDefendedBy().size() > 0) {
                if (ServerProxy.getDistanceBetween(defender, defender.getDefendedBy().get(defender.getDefendedBy().size()-1)) > 3) {
                    defender.getDefendedBy().get(defender.getDefendedBy().size()-1).clearDefends();
                    continue;
                }
                duel.setBeingDefended(defender);
                duel.setDefended(true);
                Message def = new Message(attacker.getName() + " пытается атаковать " + defender.getName() + ", но сперва вынужден пройти через защитников.", ChatColor.GRAY);
                ServerProxy.sendMessageFromTwoSourcesAndInformMasters(attacker, defender, attacker.getDefaultRange(), def);
                defender = defender.getDefendedBy().get(defender.getDefendedBy().size()-1);
                break;
            }
            duel.setDefender(defender);
//        System.out.println(opponentName + "'s thorax:");
//        DataHolder.inst().getPlayer(opponentName).getArmor().getArmorPiece(BodyPart.THORAX);
//        ClickContainer.deleteContainer("Modifiers", getCommandSenderAsPlayer(sender));
            duel.initDefense();

        }

        // REMOVE PREVIOUS PANEL
        ClickContainer.deleteContainer("SelectOpponent", getCommandSenderAsPlayer(sender));


    }
}
