package ru.konungstvo.commands.executor;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.registries.GameData;
import ru.konungstvo.bridge.ClientProxy;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.kmrp_lore.helpers.AttachmentsHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UnloadCommand extends CommandBase {
    public static final String NAME = "unload";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }


    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        boolean unloadall = false;

        if (args.length > 0) {
            if (args[0].equals("all")) {
                unloadall = true;
            }
        }

        ItemStack itemStack = ((EntityPlayerMP) sender).getHeldItemMainhand();

        if (itemStack.isEmpty() || !WeaponTagsHandler.hasWeaponTags(itemStack) && !WeaponTagsHandler.isMagazine(itemStack)) {
            TextComponentString error = new TextComponentString("Нет магазина или оружия в руках.");
            error.getStyle().setColor(TextFormatting.RED);
            sender.sendMessage(error);
            return;
        }

        WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(itemStack);
        AttachmentsHandler magazineTagsHandler = new AttachmentsHandler(itemStack);

        if (WeaponTagsHandler.hasWeaponTags(itemStack)) {
            NBTTagCompound projectile = weaponTagsHandler.getLastProjectile();
            System.out.println(projectile);
            String itemRL = projectile.getString("itemrl");
            if (itemRL.isEmpty()) {
                System.out.println("itemRL is null, THE FUCK?");
                itemRL = "gci:pistol_med_dyeable";
            }
            Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(itemRL));
            ItemStack itemStackToAdd = new ItemStack(item);
//        NBTTagCompound tagCompound = new NBTTagCompound();
//        tagCompound.setTag("projectile", projectile);
//            projectile.removeTag("itemrl");
            projectile.removeTag("itemrl");
            itemStackToAdd.setTagCompound(projectile);

            ((EntityPlayerMP) sender).inventory.addItemStackToInventory(itemStackToAdd);
            weaponTagsHandler.fireProjectile();

            if (unloadall) {
                try {
                   NBTTagList ammo = itemStack.getTagCompound().getCompoundTag("weapontags").getCompoundTag(weaponTagsHandler.getDefaultTag()).getCompoundTag("rangedFirearm").getTagList("loadedProjectilesList", 10);
                   int tagCount = ammo.tagCount();
                   for (int i = 0; i < tagCount; i++) {
                       projectile = weaponTagsHandler.getLastProjectile();
                       System.out.println(projectile);
                       itemRL = projectile.getString("itemrl");
                       if (itemRL.isEmpty()) {
                           System.out.println("itemRL is null, THE FUCK?");
                           itemRL = "gci:pistol_med_dyeable";
                       }
                       item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(itemRL));
                       itemStackToAdd = new ItemStack(item);
//        NBTTagCompound tagCompound = new NBTTagCompound();
//        tagCompound.setTag("projectile", projectile);
//            projectile.removeTag("itemrl");
                       projectile.removeTag("itemrl");
                       itemStackToAdd.setTagCompound(projectile);

                       ((EntityPlayerMP) sender).inventory.addItemStackToInventory(itemStackToAdd);
                       weaponTagsHandler.fireProjectile();
                   }
                } catch (Exception ignored) {

                }
            }

        } else {

            NBTTagCompound projectile = magazineTagsHandler.getLastProjectileCompound();
            System.out.println(projectile);
            String itemRL = projectile.getString("itemrl");
            if (itemRL.isEmpty()) {
                System.out.println("itemRL is null, THE FUCK?");
                itemRL = "gci:pistol_med_dyeable";
            }
            Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(itemRL));
            ItemStack itemStackToAdd = new ItemStack(item);
//        NBTTagCompound tagCompound = new NBTTagCompound();
//        tagCompound.setTag("projectile", projectile);
//            projectile.removeTag("itemrl");
            projectile.removeTag("itemrl");
            itemStackToAdd.setTagCompound(projectile);

            ((EntityPlayerMP) sender).inventory.addItemStackToInventory(itemStackToAdd);

            magazineTagsHandler.popLastProjectile();

            if (unloadall) {
                try {
                    NBTTagList ammo = itemStack.getTagCompound().getTagList("loadedProjectilesList", 10);
                    int tagCount = ammo.tagCount();
                    for (int i = 0; i < tagCount; i++) {
                        projectile = magazineTagsHandler.getLastProjectileCompound();
                        System.out.println(projectile);
                        itemRL = projectile.getString("itemrl");
                        if (itemRL.isEmpty()) {
                            System.out.println("itemRL is null, THE FUCK?");
                            itemRL = "gci:pistol_med_dyeable";
                        }
                        item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(itemRL));
                        itemStackToAdd = new ItemStack(item);
//        NBTTagCompound tagCompound = new NBTTagCompound();
//        tagCompound.setTag("projectile", projectile);
//            projectile.removeTag("itemrl");
                        projectile.removeTag("itemrl");
                        itemStackToAdd.setTagCompound(projectile);

                        ((EntityPlayerMP) sender).inventory.addItemStackToInventory(itemStackToAdd);

                        magazineTagsHandler.popLastProjectile();
                    }
                } catch (Exception ignored) {

                }
            }

        }


        // Удаляем патрон из инвентаря

//        TextComponentString info = new TextComponentString( "Вы достали " + projectile.getSt.");
//        info.getStyle().setColor(TextFormatting.GRAY);
//        sender.sendMessage(info);


    }
}
