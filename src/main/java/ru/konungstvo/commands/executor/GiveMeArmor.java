package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTException;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.kmrp_lore.command.LoreException;
import ru.konungstvo.kmrp_lore.helpers.Permissions;
import ru.konungstvo.kmrp_lore.helpers.WeaponDurabilityHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.kmrp_lore.helpers.WeightHandler;

import java.util.Arrays;

public class GiveMeArmor extends CommandBase {
    public static final String NAME = "givemearmor";

    @Override
    public String getName() { return NAME; }

    @Override
    public String getUsage(ICommandSender sender) { return null; }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }


    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) {
            //
        }

        String rl = "";
        switch (args[0]) {
            case "close":
                ClickContainer.deleteContainer("GiveMeArmor", getCommandSenderAsPlayer(sender));
                return;
            case "helmet":
                rl = "gci:invisible_helmet";
                break;
            case "chest":
                rl = "gci:invisible_chest";
                break;
            case "leggings":
                rl = "gci:invisible_leggings";
                break;
            case "boots":
                rl = "gci:invisible_boots";
                break;
        }
        Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(rl));
        if(item == null) return;
        ItemStack itemStack = new ItemStack(item);
        WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(itemStack);
        NBTTagCompound weapontag;
        try {
            weapontag  = JsonToNBT.getTagFromJson("{\"Броня\":{\"armor\":{}}}");
        } catch (NBTException e) {
            TextComponentString error = new TextComponentString(e.toString());
            error.getStyle().setColor(TextFormatting.RED);
            sender.sendMessage(error);
            return;
        }
        weaponTagsHandler.addWeaponTag(weapontag);
        ((EntityPlayerMP) sender).dropItem(itemStack, true);
        ClickContainer.deleteContainer("GiveMeArmor", getCommandSenderAsPlayer(sender));
    }
}
