package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import ru.konungstvo.bridge.ClientProxy;
import ru.konungstvo.commands.helpercommands.ClickContainer;

public class ServerPurgeContainersExecutor extends CommandBase {
    public static final String NAME = "servercontainerpurge";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;

    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        ClickContainer.deleteContainer("PURGE", getCommandSenderAsPlayer(sender));

    }

}
