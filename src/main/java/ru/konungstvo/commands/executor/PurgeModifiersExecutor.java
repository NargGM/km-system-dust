package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import ru.konungstvo.bridge.ClientProxy;
import ru.konungstvo.bridge.Modifiers;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.modifiers.SetModifier;
import ru.konungstvo.commands.helpercommands.modifiers.SyncModifiers;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.ModifiersState;
import ru.konungstvo.player.Player;

public class PurgeModifiersExecutor extends CommandBase {
    public static final String NAME = "modspurge";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;

    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player sub = player;
        if (player.getSubordinate() != null) {
            sub = player.getSubordinate();
        }

//        if (!sub.getModifiersState().isNoModifier(ModifiersState.damage))
//            player.performCommand("/" + SetModifier.NAME + " damage -666");
//        if (!sub.getModifiersState().isNoModifier(ModifiersState.bodypart))
//            player.performCommand("/setmodifier bodypart -666");
//        if (!sub.getModifiersState().isNoModifier(ModifiersState.serial))
//            player.performCommand("/setmodifier serial -666");
////        if (player.getModifiersState().isNoModifier(ModifiersState.percent))
////            player.performCommand("/setmodifier percent -666");
//        if (!sub.getModifiersState().isNoModifier(ModifiersState.dice))
//            player.performCommand("/setmodifier dice -666");
//        if (!sub.getModifiersState().isNoModifier(ModifiersState.cover))
//            player.performCommand("/setmodifier cover -666");
//        if (!sub.getModifiersState().isNoModifier(ModifiersState.optic))
//            player.performCommand("/setmodifier optic -666");
//        if (!sub.getModifiersState().isNoModifier(ModifiersState.bipod))
//            player.performCommand("/setmodifier bipod -666");
//        if (!sub.getModifiersState().isNoModifier(ModifiersState.stunning))
//            player.performCommand("/setmodifier stunning -666");
//        if (!sub.getModifiersState().isNoModifier(ModifiersState.capture))
//            player.performCommand("/setmodifier capture -666");

        player.performCommand(SyncModifiers.NAME + " purge");
        sub.purgeModifierSet();

        ClickContainer.deleteContainer("Modifiers", getCommandSenderAsPlayer(sender));

    }

}
