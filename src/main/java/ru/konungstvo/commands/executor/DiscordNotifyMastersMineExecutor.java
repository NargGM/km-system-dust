package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.bridge.discord.DiscordBridge;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Player;

import java.util.Arrays;
import java.util.List;

public class DiscordNotifyMastersMineExecutor extends CommandBase {


    @Override
    public String getName() {
        return "dmsggmmine";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
//
//        Player player = DataHolder.inst().getPlayer(sender.getName());
        if (args.length < 1) {
//            player.sendMessage(new Message("Нельзя отправить пустое сообщение."));
            return;
        }

        String name = sender.getName();
        if (name.isEmpty()) name = "**SERVER**";

        if (args[0].equals("help")) {
//            player.sendMessage(new Message("§e/dmsggm Сообщение"));
            return;
        } else {
            String type = args[0];
            int x = Integer.parseInt(args[1]);
            int y = Integer.parseInt(args[2]);
            int z = Integer.parseInt(args[3]);
            int d = Integer.parseInt(args[4]);
            String playername = String.join(" ", Arrays.copyOfRange(args, 5, args.length));

            Message message;

            if (type.contains("капкан")) {
                message = new Message("§4" + playername + " наступает в капкан!");
            } else {
                message = new Message("§4" + playername + " наступает на мину!");
            }

            List<Player> players = ServerProxy.findAllPlayersInRange(new BlockPos(x, y, z), 16);
            for (Player player : players) {
                player.sendMessage(message);
            }

            String resultStr = playername + " наступает " + (type.contains("капкан") ? "в" : "на") + " " + (type.contains("капкан") ? "капкан" : "мину") + " по координатам x: " + x + " y: " + y + " z: " + z + " d: " + d;
//            if(DiscordBridge.sendMessageToPlayer(player.getName(), reciever, resultStr)) {
                String gamemsg = "§2[Discord] [" + name + "] §6[@GameMaster " + resultStr + "§6]";
//                if(!ServerProxy.hasPermission(player.getName(), "km.gm"))
//                    player.sendMessage(new Message(gamemsg));
                ServerProxy.sendToAllMastersIgnoreDiscord(new Message(gamemsg));
                String discmsg = "[Discord] [" + name + "] [" + resultStr + "]";
                DiscordBridge.sendMessageToMasters(discmsg);
            }
        }

}

