package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.commands.CommandEvent;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

@Deprecated
public class RecursiveExecutor extends CommandBase {
    public static String execute(Player executor, String... args) {
        Player pl = DataHolder.inst().getPlayer(args[0]);
        if (args[1].equals("exec")) {
            return ChatColor.RED + "Опасность рекурсии.";
        }

        String command = args[1];
        String[] newArgs = new String[args.length-2];
        System.out.println("args = " + String.join(" ", args));
        for (int i = 2; i < args.length; i++) {
            System.out.println(i + " - " + args[i]);
            newArgs[i-2] = args[i];
        }
        System.out.println("com = " + command);
        System.out.println("newArgs = " + String.join(" ", newArgs));
        if (args.length <= 2) {
            newArgs = new String[0];
        }

        CommandEvent commandEvent = new CommandEvent(pl, command, newArgs);
        return commandEvent.getResponse();

    }

    @Override
    public String getName() {
        return "execold";
    }

    @Override
    public String getUsage(ICommandSender iCommandSender) {
        return "";
    }

    @Override
    public void execute(MinecraftServer minecraftServer, ICommandSender iCommandSender, String[] strings) throws CommandException {
        String answer = execute(DataHolder.inst().getPlayer(iCommandSender.getName()), strings);
        iCommandSender.sendMessage(new TextComponentString(answer));
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permission.GM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

}
