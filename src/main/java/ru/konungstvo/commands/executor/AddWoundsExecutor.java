package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.bridge.Modifiers;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.exceptions.GenericException;
import ru.konungstvo.player.NPC;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;
import ru.konungstvo.player.wounds.WoundType;

import java.util.Arrays;

public class AddWoundsExecutor extends CommandBase {

    @Override
    public String getName() {
        return "addwounds";
    }

    @Override
    public String getUsage(ICommandSender iCommandSender) {
        return "";
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        if (sender.getName().equals("CustomNPC")) return true;
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permission.GM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }


        @Override
    public void execute(MinecraftServer minecraftServer, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());

        Message message = new Message();
        if (args.length < 1) {
            message.addComponent(new MessageComponent("Пример использования: /addwounds Зверолов deal/heal 2 лр (нанесет/вылечит 2 летальные раны).", ChatColor.GRAY));
            player.sendMessage(message);
            return;
        }
        Player npc = DataHolder.inst().getPlayer(args[0]);
        if (npc == null) {
            message.addComponent(new MessageComponent("НПС или игрок не найден!", ChatColor.RED));
            player.sendMessage(message);
            return;
        }
        if (!(npc instanceof NPC)) {
            //return ChatColor.RED + "Применимо только к НПС! Пожалуйста, управляйте ранами игроков на сайте.";
        }
        if (args.length < 2 || (!args[1].equals("deal") && !args[1].equals("heal") && !args[1].equals("healscaled"))) {
            message.addComponent(new MessageComponent("Вы не указали deal или heal.", ChatColor.RED));
            player.sendMessage(message);
            return;
        }
        if (args.length < 3) {
            message.addComponent(new MessageComponent("Вы не указали количество ран.", ChatColor.RED));
            player.sendMessage(message);
            return;
        }
        if (args.length < 4) {
            message.addComponent(new MessageComponent("Вы не указали тип раны (нр, лр, кр).", ChatColor.RED));
            player.sendMessage(message);
        }

        int number = Integer.parseInt(args[2]);

        WoundType woundType = null;

        if (args.length > 3) {
            switch (args[3]) {
                case "нр":
                    woundType = WoundType.NONLETHAL;
                    break;
                case "лр":
                    woundType = WoundType.LETHAL;
                    break;
                case "кр":
                    woundType = WoundType.CRITICAL;
                    break;
            }
        }

        if (woundType == null) {
            if (npc.getWoundPyramid().getNumberOfWounds(WoundType.CRITICAL) > 0) {
                woundType = WoundType.CRITICAL;
                if (args[1].equals("healscaled")) number = (int) number/2;
            } else if (npc.getWoundPyramid().getNumberOfWounds(WoundType.LETHAL) > 0) {
                woundType = WoundType.LETHAL;
            } else {
                woundType = WoundType.NONLETHAL;
                if (args[1].equals("healscaled")) number = number * 2;
            }
        }

        if (args[1].equals("deal")) {
            npc.inflictDamage(number, "", false, false, woundType);
        } else {
            DataHolder.inst().getModifiers().healWounds(npc.getName(), number, woundType);
            npc.changeWoundsNumber(woundType, Math.max(0, npc.getWoundPyramid().getNumberOfWounds(woundType) - number));
        }

        if (npc instanceof NPC) {
            NPC npc1 = (NPC) npc;
            npc1.setWoundsInDescription();
        }

        message.addComponent(new MessageComponent("Раны " + npc.getName() + " обновлены: \n", ChatColor.GMCHAT));
        message.addComponent(npc.getWoundPyramid().toNiceMessage());
        player.sendMessage(message);
        return;
    }
}
