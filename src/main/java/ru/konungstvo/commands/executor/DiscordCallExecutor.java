package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.fml.server.FMLServerHandler;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.bridge.discord.DiscordBridge;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Player;

public class DiscordCallExecutor extends CommandBase {


    @Override
    public String getName() {
        return "dcall";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        if (args.length < 1 || args[0].equals("help")) {
            player.sendMessage(new Message("§e/dcall Ник_игрока"));
            return;
        } else {
            String reciever = args[0];
            if (FMLServerHandler.instance().getServer().getPlayerList().getPlayerByUsername(reciever) != null) {
                player.sendMessage(new Message("§4Игрок уже в игре!"));
                return;
            }
            if(DiscordBridge.callPlayer(player.getName(), reciever)) {
                // REMOVE PREVIOUS PANEL
                ClickContainer.deleteContainer("CallPlayer", getCommandSenderAsPlayer(sender));
                String gamemsg = "§2[Discord] §8[§a" + player.getName() + "§8 зовёт в игру §2" + reciever + "§8]";
                if(!ServerProxy.hasPermission(player.getName(), "km.gm"))
                    player.sendMessage(new Message(gamemsg));
                ServerProxy.sendToAllMasters(new Message(gamemsg));
                String discmsg = "[Discord] [" + player.getName() + " зовёт в игру " + reciever + "]";
                DiscordBridge.sendMessage(discmsg);
            } else {
                player.sendMessage(new Message("§4Вызвать игрока не удалось. Вероятно, у игрока не привязан discord."));
            }
        }

    }
}
