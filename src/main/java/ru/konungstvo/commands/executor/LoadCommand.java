package ru.konungstvo.commands.executor;

import de.cas_ual_ty.gci.GunCus;
import de.cas_ual_ty.gci.network.MessageSound;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.equipment.Weapon;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.kmrp_lore.helpers.AttachmentsHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.Player;

import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LoadCommand extends CommandBase {
    public static final String NAME = "load";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }


    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        boolean loadall = false;
        if (args.length > 0) {
            if (args[0].equals("all")) {
                loadall = true;
            }

            if (args[0].equals("close")) {
                ClickContainer.deleteContainer("ChooseAmmo", getCommandSenderAsPlayer(sender));
                return;
            }

        }

        Player playerG = DataHolder.inst().getPlayer(sender.getName());
        Player subordinateG = playerG;
        if (playerG.getSubordinate() != null) subordinateG = playerG.getSubordinate();
        EntityLivingBase playerEntity;
        if (DataHolder.inst().isNpc(subordinateG.getName())) {
            playerEntity = DataHolder.inst().getNpcEntity(subordinateG.getName());
        } else {
            playerEntity = ServerProxy.getForgePlayer(subordinateG.getName());
        }

        boolean loadSoundPlayed = false;

        System.out.println("HEY!");
        ItemStack itemStack = ((EntityPlayerMP) sender).getHeldItemMainhand();

        if (itemStack.isEmpty() || !WeaponTagsHandler.hasWeaponTags(itemStack) && !WeaponTagsHandler.isMagazine(itemStack)) {
            Player player = DataHolder.inst().getPlayer(sender.getName());
            boolean fixed = false;
            if (player.getSubordinate() != null) {
                player = player.getSubordinate();
                if (DataHolder.inst().isNpc(player.getName())) {
                    EntityLivingBase npc = DataHolder.inst().getNpcEntity(player.getName());
                    itemStack = npc.getHeldItemMainhand();
                    if (!itemStack.isEmpty() && (WeaponTagsHandler.hasWeaponTags(itemStack) || !WeaponTagsHandler.isMagazine(itemStack))) fixed = true;
                    if (fixed) {
                        TextComponentString npcmes = new TextComponentString("Зарядка в " + itemStack.getDisplayName() + " " + player.getName() + ".");
                        npcmes.getStyle().setColor(TextFormatting.BLUE);
                        sender.sendMessage(npcmes);
                    }
                }
            }
            if (!fixed) {
                TextComponentString error = new TextComponentString("Нет оружия или магазина в руках.");
                error.getStyle().setColor(TextFormatting.RED);
                sender.sendMessage(error);
                return;
            }
        }

        WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(itemStack);
        AttachmentsHandler magazineTagsHandler = new AttachmentsHandler(itemStack);


        if (args.length == 0 || args[0].equals("all")) {
            String caliber = "";
            if (WeaponTagsHandler.hasWeaponTags(itemStack)) {
                caliber = weaponTagsHandler.getDefaultWeapon().getCompoundTag(WeaponTagsHandler.FIREARM_KEY).getString("caliber");
            } else {
                caliber = magazineTagsHandler.getCaliber();
            }
            System.out.println("caliber:  " + caliber);
           // System.out.println("test: " + weaponTagsHandler.getDefaultWeapon().getString("caliber"));

            String caliberSecond = "-666";

            if (caliber.equals("мрп")) caliberSecond = "мрд";
            if (caliber.equals("срп")) caliberSecond = "срд";
            if (caliber.equals("крп")) caliberSecond = "крд";
            if (caliber.equals("мрд")) caliberSecond = "мрп";
            if (caliber.equals("срд")) caliberSecond = "срп";
            if (caliber.equals("крд")) caliberSecond = "крп";

            System.out.println("caliberSecond:  " + caliberSecond);

            List<ItemStack> availableAmmo = new ArrayList<>();
            for (ItemStack ammo : ((EntityPlayerMP) sender).inventory.mainInventory) {
                if (ammo.isEmpty()) continue;
                if (AttachmentsHandler.isProjectile(ammo)) {
                    AttachmentsHandler attachmentsHandler = new AttachmentsHandler(ammo);
                    if (attachmentsHandler.getProjectileOrCreate().getString("caliber").equals(caliber) || attachmentsHandler.getProjectileOrCreate().getString("caliber").equals(caliberSecond)) {
                        availableAmmo.add(ammo);
                    }
                }
            }

            if (availableAmmo.isEmpty()) {
                TextComponentString error = new TextComponentString("Нет патронов калибра " + caliber  + " в инвентаре.");
                error.getStyle().setColor(TextFormatting.RED);
                sender.sendMessage(error);
                return;
            }
            Message message = new Message("Выберите патроны: ", ChatColor.COMBAT);
            for (ItemStack ammo : availableAmmo) {
                if (ammo.isEmpty()) continue;
                MessageComponent mc = new MessageComponent(" [" + ammo.getDisplayName() + "§9]", ChatColor.BLUE);
                mc.setClickCommand("/" + NAME + " chose" + (loadall ? "all " : " ") + ammo.getDisplayName().replaceAll("§.", ""));
                message.addComponent(mc);
            }
            MessageComponent close = new MessageComponent(" [Закрыть]", ChatColor.GRAY);
            close.setClickCommand("/load close");
            message.addComponent(close);

            Helpers.sendClickContainerToClient(message, "ChooseAmmo", getCommandSenderAsPlayer(sender));

        } else if (args[0].equals("chose")) {

            String name = String.join(" ", Arrays.copyOfRange(args, 1, args.length));

            // Берём стек нужного патрона
            ItemStack chosenAmmo = null;
            for (ItemStack ammo : ((EntityPlayerMP) sender).inventory.mainInventory) {
                if (ammo.isEmpty()) continue;
                System.out.println("name: " + name);
                System.out.println("item name: " + ammo.getDisplayName());
                if (ammo.getDisplayName().replaceAll("§.", "").equals(name)) {
                    chosenAmmo = ammo;
                    break;
                }
            }
            if (chosenAmmo == null || chosenAmmo.isEmpty()) {
                System.out.println("!!! null");
                ClickContainer.deleteContainer("ChooseAmmo", getCommandSenderAsPlayer(sender));
                return;
            }
            System.out.println("air? " + chosenAmmo.getItem().getRegistryName().toString());
            if (chosenAmmo.getItem().getRegistryName().toString().equals("air")) {
                System.out.println("Ammo is air, return");
                return;
            }
            AttachmentsHandler chosenAmmoHandler = new AttachmentsHandler(chosenAmmo);

            // ЕСЛИ В РУКАХ ОРУЖИЕ
            if (WeaponTagsHandler.hasWeaponTags(itemStack)) {

                // Проверяем, подходит ли патрон к оружию
                if (!weaponTagsHandler.doesProjectileFit(chosenAmmoHandler.getProjectileOrCreate())) {
                    String wcaliber = "-666";
                    try {
                        wcaliber = weaponTagsHandler.getCaliber();
                    } catch (Exception e) {
                        wcaliber = "error";
                    }
                    String acaliber;
                    try {
                        acaliber = chosenAmmoHandler.getProjectileOrCreate().getString("caliber");
                    } catch (Exception e) {
                        acaliber = "error1";
                    }
                    System.out.println("???:" + "wcaliber " + acaliber + " fits " + wcaliber + " ???");
                    if (wcaliber.equals("мрп") && acaliber.equals("мрд") ||
                            wcaliber.equals("срп") && acaliber.equals("срд") ||
                            wcaliber.equals("крп") && acaliber.equals("крд") ||
                            wcaliber.equals("мрд") && acaliber.equals("мрп") ||
                            wcaliber.equals("срд") && acaliber.equals("срп") ||
                            wcaliber.equals("крд") && acaliber.equals("крп")
                    ) {
                        System.out.println("!!!:" + chosenAmmoHandler.getCaliber() + " fits " + weaponTagsHandler.getCaliber());
                    }
                    else{

                        System.out.println("!!! no fit");
                        return;
                    }
                }

                // Проверяем, есть ли ещё место для патронов
                if (weaponTagsHandler.isFull()) {
                    TextComponentString info = new TextComponentString("Больше нет места для патронов.");
                    info.getStyle().setColor(TextFormatting.RED);
                    sender.sendMessage(info);
                    return;
                }

                // Заряжаем в оружие (в патронник)
                weaponTagsHandler.loadProjectileToChamber(
                        chosenAmmo.getTagCompound(),
//                        chosenAmmoHandler.getProjectileOrCreate(),
                        ForgeRegistries.ITEMS.getKey(chosenAmmo.getItem()).toString()
                );
            } else {


                // Проверяем совместимость
                if (!magazineTagsHandler.doesProjectileFit(chosenAmmoHandler.getProjectileOrCreate())) {
                    String wcaliber = "-666";
                    try {
                        wcaliber = magazineTagsHandler.getCaliber();
                    } catch (Exception e) {
                        wcaliber = "error";
                    }
                    String acaliber;
                    try {
                        acaliber = chosenAmmoHandler.getProjectileOrCreate().getString("caliber");
                    } catch (Exception e) {
                        acaliber = "error1";
                    }
                    System.out.println("???:" + "wcaliber " + acaliber + " fits " + wcaliber + " ???");
                    if (wcaliber.equals("мрп") && acaliber.equals("мрд") ||
                            wcaliber.equals("срп") && acaliber.equals("срд") ||
                            wcaliber.equals("крп") && acaliber.equals("крд") ||
                            wcaliber.equals("мрд") && acaliber.equals("мрп") ||
                            wcaliber.equals("срд") && acaliber.equals("срп") ||
                            wcaliber.equals("крд") && acaliber.equals("крп")
                    ) {
                        System.out.println("!!!:" + chosenAmmoHandler.getCaliber() + " fits " + weaponTagsHandler.getCaliber());
                    }
                    else {
                        System.out.println("!!! no fit");
                        return;
                    }
                }

                // Проверяем, есть ли место
                if (magazineTagsHandler.isFull()) {
                    TextComponentString info = new TextComponentString("Больше нет места для патронов.");
                    info.getStyle().setColor(TextFormatting.RED);
                    sender.sendMessage(info);
                    return;
                }

                // Заряжаем в магазин
                magazineTagsHandler.loadProjectile(
                        chosenAmmo.getTagCompound(),
//                        chosenAmmoHandler.getProjectileOrCreate(),
                        ForgeRegistries.ITEMS.getKey(chosenAmmo.getItem()).toString()
                );
            }

            // Удаляем патрон из инвентаря
            chosenAmmo.setCount(chosenAmmo.getCount() - 1);
            if (chosenAmmo.isEmpty()) {
                ClickContainer.deleteContainer("ChooseAmmo", getCommandSenderAsPlayer(sender));
            }

            TextComponentString info = new TextComponentString(name + " заряжен.");

            TextComponentString masterInfo = new TextComponentString("[GM] " + sender.getName() + " зарядил " + name);
            if (!loadSoundPlayed) {
                loadSoundPlayed = true;
                if (WeaponTagsHandler.hasWeaponTags(itemStack)) {
                    Weapon weapon = new Weapon(weaponTagsHandler.getDefaultWeaponName(), weaponTagsHandler.getDefaultWeapon(), weaponTagsHandler);
                    GunCus.channel.sendToAllAround(new MessageSound(playerEntity, weapon.getSound() + "_reload", 2F, 1F), new NetworkRegistry.TargetPoint(playerEntity.world.provider.getDimension(), playerEntity.posX, playerEntity.posY, playerEntity.posZ, 16F));
                } else {
                    GunCus.channel.sendToAllAround(new MessageSound(playerEntity, GunCus.SOUND_MAG_LOAD, 2F, 1F), new NetworkRegistry.TargetPoint(playerEntity.world.provider.getDimension(), playerEntity.posX, playerEntity.posY, playerEntity.posZ, 16F));
                }
            }
            masterInfo.getStyle().setColor(TextFormatting.GOLD);
            DataHolder.inst().informMasterForCombat(sender.getName(), masterInfo);

            info.getStyle().setColor(TextFormatting.GRAY);
            sender.sendMessage(info);


        } else if (args[0].equals("choseall")) {
            loadall = true;
            int count = 0;
            String name = String.join(" ", Arrays.copyOfRange(args, 1, args.length));

            // Берём стек нужного патрона
            ItemStack chosenAmmo = null;
            for (ItemStack ammo : ((EntityPlayerMP) sender).inventory.mainInventory) {
                if (ammo.isEmpty()) continue;
                System.out.println("name: " + name);
                System.out.println("item name: " + ammo.getDisplayName());
                if (ammo.getDisplayName().replaceAll("§.", "").equals(name)) {
                    chosenAmmo = ammo;
                    break;
                }
            }
            if (chosenAmmo == null || chosenAmmo.isEmpty()) {
                System.out.println("!!! null");
                ClickContainer.deleteContainer("ChooseAmmo", getCommandSenderAsPlayer(sender));
                return;
            }
            System.out.println("air? " + chosenAmmo.getItem().getRegistryName().toString());
            if (chosenAmmo.getItem().getRegistryName().toString().equals("air")) {
                System.out.println("Ammo is air, return");
                return;
            }
            AttachmentsHandler chosenAmmoHandler = new AttachmentsHandler(chosenAmmo);

            int stackSize = chosenAmmo.getCount();
            for (int i = 0; i < stackSize; i++) {
                // ЕСЛИ В РУКАХ ОРУЖИЕ
                if (WeaponTagsHandler.hasWeaponTags(itemStack)) {

                    // Проверяем, подходит ли патрон к оружию
                    if (!weaponTagsHandler.doesProjectileFit(chosenAmmoHandler.getProjectileOrCreate())) {
                        String wcaliber = "-666";
                        try {
                            wcaliber = weaponTagsHandler.getCaliber();
                        } catch (Exception e) {
                            wcaliber = "error";
                        }
                        String acaliber;
                        try {
                            acaliber = chosenAmmoHandler.getProjectileOrCreate().getString("caliber");
                        } catch (Exception e) {
                            acaliber = "error1";
                        }
                        System.out.println("???:" + "wcaliber " + acaliber + " fits " + wcaliber + " ???");
                        if (wcaliber.equals("мрп") && acaliber.equals("мрд") ||
                                wcaliber.equals("срп") && acaliber.equals("срд") ||
                                wcaliber.equals("крп") && acaliber.equals("крд") ||
                                wcaliber.equals("мрд") && acaliber.equals("мрп") ||
                                wcaliber.equals("срд") && acaliber.equals("срп") ||
                                wcaliber.equals("крд") && acaliber.equals("крп")
                        ) {
                            System.out.println("!!!:" + chosenAmmoHandler.getCaliber() + " fits " + weaponTagsHandler.getCaliber());
                        } else {

                            System.out.println("!!! no fit");
                            return;
                        }
                    }

                    // Проверяем, есть ли ещё место для патронов
                    if (weaponTagsHandler.isFull()) {
                        if (count == 0) {
                            TextComponentString info = new TextComponentString("Больше нет места для патронов.");
                            ClickContainer.deleteContainer("ChooseAmmo", getCommandSenderAsPlayer(sender));
                            info.getStyle().setColor(TextFormatting.RED);
                            sender.sendMessage(info);
                        } else {
                            ClickContainer.deleteContainer("ChooseAmmo", getCommandSenderAsPlayer(sender));
                            TextComponentString info = new TextComponentString(count + " " + name + " заряжено. Больше нет места.");

                            TextComponentString masterInfo = new TextComponentString("[GM] " + sender.getName() + " зарядил " + count + " " + name);
                            if (!loadSoundPlayed) {
                                loadSoundPlayed = true;
                                if (WeaponTagsHandler.hasWeaponTags(itemStack)) {
                                    Weapon weapon = new Weapon(weaponTagsHandler.getDefaultWeaponName(), weaponTagsHandler.getDefaultWeapon(), weaponTagsHandler);
                                    GunCus.channel.sendToAllAround(new MessageSound(playerEntity, weapon.getSound() + "_reload", 2F, 1F), new NetworkRegistry.TargetPoint(playerEntity.world.provider.getDimension(), playerEntity.posX, playerEntity.posY, playerEntity.posZ, 16F));
                                } else {
                                    GunCus.channel.sendToAllAround(new MessageSound(playerEntity, GunCus.SOUND_MAG_LOAD, 2F, 1F), new NetworkRegistry.TargetPoint(playerEntity.world.provider.getDimension(), playerEntity.posX, playerEntity.posY, playerEntity.posZ, 16F));
                                }
                            }
                            masterInfo.getStyle().setColor(TextFormatting.GOLD);
                            DataHolder.inst().informMasterForCombat(sender.getName(), masterInfo);

                            info.getStyle().setColor(TextFormatting.GRAY);
                            sender.sendMessage(info);
                        }
                        return;
                    }

                    // Заряжаем в оружие (в патронник)
                    weaponTagsHandler.loadProjectileToChamber(
                            chosenAmmo.getTagCompound(),
//                        chosenAmmoHandler.getProjectileOrCreate(),
                            ForgeRegistries.ITEMS.getKey(chosenAmmo.getItem()).toString()
                    );
                } else {


                    // Проверяем совместимость
                    if (!magazineTagsHandler.doesProjectileFit(chosenAmmoHandler.getProjectileOrCreate())) {
                        String wcaliber = "-666";
                        try {
                            wcaliber = magazineTagsHandler.getCaliber();
                        } catch (Exception e) {
                            wcaliber = "error";
                        }
                        String acaliber;
                        try {
                            acaliber = chosenAmmoHandler.getProjectileOrCreate().getString("caliber");
                        } catch (Exception e) {
                            acaliber = "error1";
                        }
                        System.out.println("???:" + "wcaliber " + acaliber + " fits " + wcaliber + " ???");
                        if (wcaliber.equals("мрп") && acaliber.equals("мрд") ||
                                wcaliber.equals("срп") && acaliber.equals("срд") ||
                                wcaliber.equals("крп") && acaliber.equals("крд") ||
                                wcaliber.equals("мрд") && acaliber.equals("мрп") ||
                                wcaliber.equals("срд") && acaliber.equals("срп") ||
                                wcaliber.equals("крд") && acaliber.equals("крп")
                        ) {
                            System.out.println("!!!:" + chosenAmmoHandler.getCaliber() + " fits " + weaponTagsHandler.getCaliber());
                        } else {
                            System.out.println("!!! no fit");
                            return;
                        }
                    }

                    // Проверяем, есть ли место
                    if (magazineTagsHandler.isFull()) {
                        if (count == 0) {
                            TextComponentString info = new TextComponentString("Больше нет места для патронов.");
                            ClickContainer.deleteContainer("ChooseAmmo", getCommandSenderAsPlayer(sender));
                            info.getStyle().setColor(TextFormatting.RED);
                            sender.sendMessage(info);
                        } else {
                            ClickContainer.deleteContainer("ChooseAmmo", getCommandSenderAsPlayer(sender));
                            TextComponentString info = new TextComponentString(count + " " + name + " заряжено. Больше нет места.");

                            TextComponentString masterInfo = new TextComponentString("[GM] " + sender.getName() + " зарядил " + count + " " + name);
                            if (!loadSoundPlayed) {
                                loadSoundPlayed = true;
                                if (WeaponTagsHandler.hasWeaponTags(itemStack)) {
                                    Weapon weapon = new Weapon(weaponTagsHandler.getDefaultWeaponName(), weaponTagsHandler.getDefaultWeapon(), weaponTagsHandler);
                                    GunCus.channel.sendToAllAround(new MessageSound(playerEntity, weapon.getSound() + "_reload", 2F, 1F), new NetworkRegistry.TargetPoint(playerEntity.world.provider.getDimension(), playerEntity.posX, playerEntity.posY, playerEntity.posZ, 16F));
                                } else {
                                    GunCus.channel.sendToAllAround(new MessageSound(playerEntity, GunCus.SOUND_MAG_LOAD, 2F, 1F), new NetworkRegistry.TargetPoint(playerEntity.world.provider.getDimension(), playerEntity.posX, playerEntity.posY, playerEntity.posZ, 16F));
                                }
                            }

                            masterInfo.getStyle().setColor(TextFormatting.GOLD);
                            DataHolder.inst().informMasterForCombat(sender.getName(), masterInfo);

                            info.getStyle().setColor(TextFormatting.GRAY);
                            sender.sendMessage(info);
                        }
                        return;
                    }

                    // Заряжаем в магазин
                    magazineTagsHandler.loadProjectile(
                            chosenAmmo.getTagCompound(),
//                        chosenAmmoHandler.getProjectileOrCreate(),
                            ForgeRegistries.ITEMS.getKey(chosenAmmo.getItem()).toString()
                    );
                }

                // Удаляем патрон из инвентаря
                chosenAmmo.setCount(chosenAmmo.getCount() - 1);
                count++;
            }
            ClickContainer.deleteContainer("ChooseAmmo", getCommandSenderAsPlayer(sender));
            TextComponentString info = new TextComponentString(count + " " + name + " заряжено");

            TextComponentString masterInfo = new TextComponentString("[GM] " + sender.getName() + " зарядил " + count + " " + name);
            if (!loadSoundPlayed) {
                loadSoundPlayed = true;
                if (WeaponTagsHandler.hasWeaponTags(itemStack)) {
                    Weapon weapon = new Weapon(weaponTagsHandler.getDefaultWeaponName(), weaponTagsHandler.getDefaultWeapon(), weaponTagsHandler);
                    GunCus.channel.sendToAllAround(new MessageSound(playerEntity, weapon.getSound() + "_reload", 2F, 1F), new NetworkRegistry.TargetPoint(playerEntity.world.provider.getDimension(), playerEntity.posX, playerEntity.posY, playerEntity.posZ, 16F));
                } else {
                    GunCus.channel.sendToAllAround(new MessageSound(playerEntity, GunCus.SOUND_MAG_LOAD, 2F, 1F), new NetworkRegistry.TargetPoint(playerEntity.world.provider.getDimension(), playerEntity.posX, playerEntity.posY, playerEntity.posZ, 16F));
                }
            }
            masterInfo.getStyle().setColor(TextFormatting.GOLD);
            DataHolder.inst().informMasterForCombat(sender.getName(), masterInfo);

            info.getStyle().setColor(TextFormatting.GRAY);
            sender.sendMessage(info);


        }
    }
}
