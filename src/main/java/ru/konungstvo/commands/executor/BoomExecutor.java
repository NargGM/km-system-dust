package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.server.permission.PermissionAPI;
import noppes.npcs.api.NpcAPI;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.combat.*;
import ru.konungstvo.combat.equipment.AttackType;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.player.attack.PerformAttackCommand;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;
import ru.konungstvo.player.wounds.WoundType;
import scala.Int;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BoomExecutor extends CommandBase {


    @Override
    public String getName() {
        return "boom";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permission.GM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        int damage = 10;
        BoomType boomType = BoomType.DEFAULT;
        boolean defendable = true;
        boolean ignorearmor = false;
        WoundType woundType = WoundType.LETHAL;
        AttackType attackType = AttackType.PHYSIC;
        if (args.length > 0) {
            if (args[0].equals("help")) {
                sender.sendMessage(new TextComponentString("/boom {урон} {тип взрыва} {можно ли защищаться}\n" +
                        "§eДоступные типы: осколочная (дефолт)\n, зажигательная, газовая, слабеющий_газ, свето-шумовая" +
                        "§eЧтобы от взрыва нельзя было защищаться третий аргумент должен быть 'нз'" +
                        ""));
                return;
            }
            damage = Integer.parseInt(args[0]);
            if (args.length > 1) {
                switch (args[1]) {
                    case "дефолт":
                    case "о":
                    case "осколочная":
                        boomType = BoomType.DEFAULT;
                        break;
                    case "г":
                    case "гор":
                    case "горение":
                    case "заж":
                    case "зажигательная":
                    case "зажигательный":
                        boomType = BoomType.FIRE;
                        break;
                    case "газ":
                    case "газовая":
                    case "газовый":
                        boomType = BoomType.GAS;
                        break;
                    case "сг":
                    case "слабыйгаз":
                    case "слабеющийгаз":
                    case "слабый_газ":
                    case "слабеющий_газ":
                    case "газик":
                    case "газок":
                        boomType = BoomType.WEAK_GAS;
                        break;
                    case "сш":
                    case "свето-шумовая":
                    case "свето-шумовой":
                    case "нл":
                    case "нелетальная":
                    case "нелетальный":
                    case "не летальная":
                    case "не летальный":
                        boomType = BoomType.STUNNING;
                        woundType = WoundType.NONLETHAL;
                    case "крит":
                    case "критическая":
                    case "критический":
                }
            }
            for (String arg : args) {
                if (arg.equals("нз")) defendable = false;
                if (arg.equals("иб")) ignorearmor = true;
                if (arg.equals("псих")) attackType = AttackType.PSYCHIC;
            }
        }
        Combat combat = DataHolder.inst().getCombat(player.getAttachedCombatID());
        if (combat == null) {
            player.sendMessage("§4Нет боя.");
            EntityPlayerMP playerMP = ServerProxy.getForgePlayer(sender.getName());
            if (boomType == BoomType.DEFAULT || boomType == BoomType.STUNNING) NpcAPI.Instance().getIWorld(playerMP.dimension).explode(Math.floor(playerMP.posX) + 0.5, Math.floor(playerMP.posY), Math.floor(playerMP.posZ) + 0.5, 0, false, false);
            else if (boomType == BoomType.GAS || boomType == BoomType.FIRE || boomType == BoomType.WEAK_GAS) NpcAPI.Instance().getIWorld(playerMP.dimension).playSoundAt(NpcAPI.Instance().getIPos(Math.floor(playerMP.posX) + 0.5, Math.floor(playerMP.posY), Math.floor(playerMP.posZ) + 0.5), "minecraft:entity.creeper.primed",1, 1);
            return;
        }

        ArrayList<Player> targets = (ArrayList<Player>) ServerProxy.getAllFightersInRangeForCombat(player, damage);

        if (targets.size() == 0) {
            Message message = new Message("§4Происходит взрыв (" + boomType.color() + boomType.toString() + " §8[" + (attackType.equals(AttackType.PSYCHIC) ? "§d" : "§c") + damage + "§8]§4)! В зоне взрыва нет целей.");
            ServerProxy.sendMessageFromAndInformMasters(player, message);
            EntityPlayerMP playerMP = ServerProxy.getForgePlayer(sender.getName());
            if (boomType == BoomType.DEFAULT || boomType == BoomType.STUNNING) NpcAPI.Instance().getIWorld(playerMP.dimension).explode(Math.floor(playerMP.posX) + 0.5, Math.floor(playerMP.posY), Math.floor(playerMP.posZ) + 0.5, 0, false, false);
            else if (boomType == BoomType.GAS || boomType == BoomType.FIRE || boomType == BoomType.WEAK_GAS) NpcAPI.Instance().getIWorld(playerMP.dimension).playSoundAt(NpcAPI.Instance().getIPos(Math.floor(playerMP.posX) + 0.5, Math.floor(playerMP.posY), Math.floor(playerMP.posZ) + 0.5), "minecraft:entity.creeper.primed",1, 1);            return;
        }

        Boom boom = new Boom(sender.getPosition(), boomType, damage, player.getAttachedCombatID(), ServerProxy.getForgePlayer(sender.getName()).dimension, defendable, ignorearmor, woundType, attackType);
        combat.addBoom(boom);
        DataHolder.inst().spawnBoomNpc("ВЗРЫВ-" + boom.getUuid(), ServerProxy.getForgePlayer(player.getName()), sender.getPosition(), damage, boomType.toString());

        boom.addTargets(targets);

        StringBuilder targetsName = new StringBuilder();
        for (Player target : targets) {
            targetsName.append(target.getName()).append(" ");
            if (DataHolder.inst().isNpc(target.getName())) continue;
            target.performCommand("/boomd");
        }

        Message message = new Message("§4Происходит взрыв (" + boomType.color() + boomType.toString() + " §8[" + (attackType.equals(AttackType.PSYCHIC) ? "§d" : "§c") + damage + "§8]§4)! §4Затронуты взрывом:\n§с[§a" + targetsName.toString().trim() + "§с]" +
                (defendable ? "" : "\n§4§lНЕЛЬЗЯ ЗАЩИЩАТЬСЯ!"));
        ServerProxy.sendMessageFromAndInformMasters(player, message);
        for (Player target : targets) {
            if (DataHolder.inst().isNpc(target.getName())) {
                Message test = new Message("", ChatColor.BLUE);
                MessageComponent testing = new MessageComponent("/sub " + target.getName(), ChatColor.BLUE);
                testing.setUnderlined(true);
                testing.setClickCommand("/sub " + target.getName());
                test.addComponent(testing);
                ServerProxy.sendMessage(DataHolder.inst().getMasterForCombat(combat.getId()), test);
                break;
            };
        }
        //ServerProxy.informDistantMasters(player, message, Range.NORMAL.getDistance());
    }
}
