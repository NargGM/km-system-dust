package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.exceptions.PermissionException;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

public class SetRangeExecutor extends CommandBase {

    @Override
    public String getName() {
        return "setrange";
    }

    @Override
    public String getUsage(ICommandSender iCommandSender) {
        return "";
    }

    @Override
    public void execute(MinecraftServer minecraftServer, ICommandSender iCommandSender, String[] strings) throws CommandException {
        String answer = execute(DataHolder.inst().getPlayer(iCommandSender.getName()), strings);
        iCommandSender.sendMessage(new TextComponentString(answer));
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permission.GM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String execute(Player executor, String[] args) {


        if (args.length == 0 || args[0].equals("help")) {
            return "§e----------- §fHelp: setrange §e--------------------§8\n" +
                    "§fПозволяет установить предел слышимости чата. Сбрасывается после перезахода.\n" +
                    "§e/setrange <number> §f— установить предел слышимости.\n" +
                    "§e/setrange show§f — показать предел.\n" +
                    "§e/setrange remove §fили §ereset§f — убрать предел.\n";
        }

        if (args[0].equals("show")) {
            double range = executor.getRangePreference();
            if (range == 0) return "Предел слышимости не установлен.";
            return "Установленный предел слышимости: " + range;
        }

        if (args[0].equals("reset") || args[0].equals("remove")) {
            executor.setRangePreference(0);
            return "Предел слышимости был убран.";
        }

        double range;
        try {
            range = Double.parseDouble(args[0]);
        } catch (Exception e) {
            return  "Нужно указать число!";
        }
        if (range <= 0) {
            return "Число должно быть положительным!";
        }

        executor.setRangePreference(range);
        return "Предел слышимости установлен: " + range;
    }


}
