package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.combat.Skill;
import ru.konungstvo.combat.equipment.Shield;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.NPC;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;



public class UpdateExecutor extends CommandBase {

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }


    public static String execute(Player executor, String[] args) {
        switch (args[0]) {
            case "wounds":
                String name;
                if (args.length > 1) {
                    name = args[1];
                } else {
                    name = executor.getName();
                }
                DataHolder.inst().updatePlayerWounds(name);
                return "Раны " + name + " обновлены";
            case "armor":
                //return "";
                if (args.length > 1) {
                    DataHolder.inst().updatePlayerArmor(args[1]);
                    return "§5Armor for " + args[1] + " was updated";

                }
                DataHolder.inst().updatePlayerArmor(executor.getName());
                return "§5Armor for " + executor.getName() + " was updated with mod " + executor.getArmorMod() + " and defense " + executor.getDefenseMod() + ".";
            case "skills":
                String nameToUpdate;
                if (args.length > 1 && (executor == null || executor.hasPermission(Permission.GM))) {
                    nameToUpdate = args[1];
                } else {
                    nameToUpdate = executor.getName();
                }
                Player player = DataHolder.inst().getPlayer(nameToUpdate);
                DataHolder.inst().updatePlayerNewSkills(nameToUpdate);
                StringBuilder skillsStr = new StringBuilder();
                for (Skill skill : player.getSkills()) {
                    skillsStr.append("§8" + skill.getName() + ": ")
                            .append(skill.getLevelAsString())
                            .append(" " + skill.getPercent() + "%")
                            .append("\n");
                }
                return "§5Skills for " + nameToUpdate + " were updated!\n" + skillsStr;
            case "shield":
                Shield shield = DataHolder.inst().updatePlayerShield(executor.getName());
                if (shield == null) {
                    return TextFormatting.GRAY + "Shield not found.";
                }
                return TextFormatting.GRAY + "Shield updated with defense " + shield.getDamage();

        }
        return "Тут будет хелп"; //TODO
    }

    @Override
    public String getName() {
        return "update";
    }

    @Override
    public String getUsage(ICommandSender iCommandSender) {
        return "[Usage of upgrade]";
    }

    @Override
    public void execute(MinecraftServer minecraftServer, ICommandSender iCommandSender, String[] strings) throws CommandException {
        String answer = execute(DataHolder.inst().getPlayer(iCommandSender.getName()), strings);
        iCommandSender.sendMessage(new TextComponentString(answer));
    }
}
