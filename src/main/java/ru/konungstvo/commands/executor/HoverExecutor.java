package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.control.DataHolder;

public class HoverExecutor extends CommandBase {
    public static final String NAME = "hover";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;

    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Message message = new Message("test ");
        MessageComponent c = new MessageComponent(" hover");
        c.setHoverText("тест", TextFormatting.GRAY);
        message.addComponent(c);
        DataHolder.inst().getPlayer(sender.getName()).sendMessage(message);
    }

}
