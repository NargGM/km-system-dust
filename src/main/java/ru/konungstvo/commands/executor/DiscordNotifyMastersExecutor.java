package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.client.ClientCommandHandler;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.bridge.discord.DiscordBridge;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Player;

public class DiscordNotifyMastersExecutor extends CommandBase {


    @Override
    public String getName() {
        return "dmsggm";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
//
//        Player player = DataHolder.inst().getPlayer(sender.getName());
        if (args.length < 1) {
//            player.sendMessage(new Message("Нельзя отправить пустое сообщение."));
            return;
        }

        String name = sender.getName();
        if (name.isEmpty()) name = "**SERVER**";

        if (args[0].equals("help")) {
//            player.sendMessage(new Message("§e/dmsggm Сообщение"));
            return;
        } else {
            StringBuilder result = new StringBuilder();
            for (String arg : args) {
                result.append(" ").append(arg);
            }

            String resultStr = result.toString().trim();
//            if(DiscordBridge.sendMessageToPlayer(player.getName(), reciever, resultStr)) {
                String gamemsg = "§2[Discord] [" + name + "] §6[@GameMaster " + resultStr + "§6]";
//                if(!ServerProxy.hasPermission(player.getName(), "km.gm"))
//                    player.sendMessage(new Message(gamemsg));
                ServerProxy.sendToAllMasters(new Message(gamemsg));
                String discmsg = "[Discord] [" + name + "] [" + resultStr + "]";
                DiscordBridge.sendMessageToMasters(discmsg);
                sender.sendMessage(new TextComponentString(gamemsg));
            }
        }

}

