package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.combat.CombatState;
import ru.konungstvo.combat.StatusEnd;
import ru.konungstvo.combat.StatusType;
import ru.konungstvo.combat.duel.Attack;
import ru.konungstvo.combat.duel.Duel;
import ru.konungstvo.combat.duel.DuelMaster;
import ru.konungstvo.combat.equipment.Weapon;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.Player;

public class SurrenderExecutor extends CommandBase {
    public static final String NAME = "surrender";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;

    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        if(player.getSubordinate() != null) player = player.getSubordinate();

        // REMOVE PREVIOUS PANEL
        ClickContainerMessage remove = new ClickContainerMessage("SelectDefense", true);
        KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));

        boolean cover = false;
        if (args.length > 0 && args[0].equals("cover")) {
            Message capitulation = new Message(player.getName() + " §fукрывается §cот огня на подавление!", ChatColor.RED);
            ServerProxy.sendMessageFromAndInformMasters(player, capitulation);
            player.removeStatusEffect(StatusType.CONCENTRATED);
            player.addStatusEffect("подавление", StatusEnd.TURN_END, 1, StatusType.STUNNED, "подавление");
            try {
                Player gm = DataHolder.inst().getMasterForPlayerInCombat(player);
                Duel duel = DataHolder.inst().getDuelForDefender(player.getName());
                if (duel.getAttacker().getCombatState() == CombatState.SHOULD_ACT) {
                    if (DataHolder.inst().isNpc(duel.getAttacker().getName())) {
                        for (Attack attack : duel.getAttacks()) {
                            gm.setSubordinate(duel.getAttacker());
                            gm.performCommand("/shoot" + (duel.getActualActiveHand() == 0 ? " left" : ""));
                        }
                        if (duel.getAttacks().size() > 1 && duel.getActiveHand() != 2) {
                            try {
                                int recoil = duel.getActiveAttack().getWeapon().getFirearm().getRecoil();
                                if (recoil != 0) {
                                    player.setRecoil(recoil);
                                }
                            } catch (Exception ignored) {

                            }
                        }
                        DuelMaster.end(duel);
                        gm.performCommand("/queue next combat");
                    } else {
                        for (Attack attack : duel.getAttacks()) {
                            duel.getAttacker().performCommand("/shoot" + (duel.getActualActiveHand() == 0 ? " left" : ""));
                        }
                        DuelMaster.end(duel);
                        duel.getAttacker().performCommand("/queue next combat");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }

        Message capitulation = new Message("Игрок " + player.getName() + " §fсдаётся §cна милость победителя!", ChatColor.RED);
        ServerProxy.sendMessageFromAndInformMasters(player, capitulation);
        try {
            Player gm = DataHolder.inst().getMasterForPlayerInCombat(player);
            gm.performCommand("/queue remove " + player.getName());
            gm.performCommand("/combat remove " + player.getName());
            Duel duel = DataHolder.inst().getDuelForDefender(player.getName());
            if (duel.getAttacker().getCombatState() == CombatState.SHOULD_ACT && DataHolder.inst().getMasterForNpcInCombat(duel.getAttacker()) == gm) {
                DuelMaster.end(duel);
                gm.performCommand("/queue next");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
