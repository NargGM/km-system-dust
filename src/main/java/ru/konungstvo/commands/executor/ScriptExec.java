package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.bridge.discord.DiscordBridge;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.combat.CombatState;
import ru.konungstvo.combat.StatusEffect;
import ru.konungstvo.combat.StatusEnd;
import ru.konungstvo.combat.StatusType;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.NPC;
import ru.konungstvo.player.Player;
import ru.konungstvo.player.wounds.WoundType;

import javax.xml.crypto.Data;

public class ScriptExec extends CommandBase {


    @Override
    public String getName() {
        return "scrnpc";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        System.out.println("TESTTESTTEST");
//
//        Player player = DataHolder.inst().getPlayer(sender.getName());
        if (args.length < 2) {
//            player.sendMessage(new Message("Нельзя отправить пустое сообщение."));
            return;
        }

        String name = sender.getName();
        System.out.println(name);
        //if (!name.contains("CustomNPC")) return;

        if (args[0].equals("stimulator")) {
            Player player = DataHolder.inst().getPlayer(args[1]);
            Player sub = player;
            if (player.getSubordinate() != null) sub = player.getSubordinate();
            EntityPlayer ep = ServerProxy.getForgePlayer(player.getName());
            ItemStack stimulator = ep.getHeldItemMainhand();
            if (!stimulator.isEmpty() && stimulator.hasTagCompound()) {
                if (stimulator.getTagCompound() != null && stimulator.getTagCompound().hasKey("useable")) {
                    if (stimulator.getTagCompound().getCompoundTag("useable").getString("type").equals("stimulator")) {
                        NBTTagCompound stim = stimulator.getTagCompound().getCompoundTag("useable");
                        if (DataHolder.inst().getCombatForPlayer(sub.getName()) == null) {
                            player.sendMessage("§4Нет смысла использовать вне боя (сожгите если очень надо).");
                            return;
                        }
                        if (sub.hasStatusEffect(StatusType.MEDS) || sub.hasStatusEffect(StatusType.MEDS2)) {
                            player.sendMessage("§4Уже под действием стимулятора. Применение приведет к летальному исходу.");
                            return;
                        }
                        if (sub.hasStatusEffect(StatusType.HANGOVER)) {
                            player.sendMessage("§4Вы еще не отошли от прошлого стимулятора. Применение приведет к летальному исходу.");
                            return;
                        }
                        boolean justActivated = (sub.getCombatState() == CombatState.SHOULD_ACT);
                        int turns = stim.getInteger("turns");
                        if (turns == 0) {
                            turns = 1;
                            justActivated = false;
                        }
                        if (stim.hasKey("trait")) {
                            sub.addStatusEffect(stim.getString("trait"), StatusEnd.TURN_END, turns, StatusType.MEDS, stim.getString("trait"), -666,
                                    new StatusEffect(StatusType.HANGOVER.toString(), StatusEnd.TURN_END, stim.getInteger("withdrawal"), StatusType.HANGOVER, "", stim.getInteger("debuff"), null, false), justActivated);

                        }
                        if (stim.hasKey("skills")) {
                            sub.addStatusEffect("Стимулятор навыков", StatusEnd.TURN_END, turns, StatusType.MEDS2, stim.getString("skills"), stim.getInteger("buff"),
                                    (stim.hasKey("trait") ? null : new StatusEffect(StatusType.HANGOVER.toString(), StatusEnd.TURN_END, stim.getInteger("withdrawal"), StatusType.HANGOVER, "", stim.getInteger("debuff"), null, false)), justActivated);
                        }
                        stimulator.setCount(stimulator.getCount() - 1);
                        ServerProxy.sendMessageFromAndInformMasters(sub, new Message("§a" + sub.getName() + " §2использует стимулятор!"));
                        if (stim.hasKey("lore")) {
                            player.sendMessage(new Message(stim.getString("lore"), ChatColor.DARK_GREEN));
                        }
                        DiscordBridge.sendMessage(sub.getName() + " использует стимулятор: " + (stim.hasKey("trait") ? stim.getString("trait") : "") + (stim.hasKey("skills") ? stim.getString("skills") + " Бафф: " + stim.getInteger("buff") : "") + " Ходы: " + stim.getInteger("turns") + " Отходняк: " + stim.getInteger("withdrawal") + " Штраф: " + stim.getInteger("debuff"));
                    } else if (stimulator.getTagCompound().getCompoundTag("useable").getString("type").equals("heal")) {
                        NBTTagCompound heal = stimulator.getTagCompound().getCompoundTag("useable");
                        int courage = sub.maxcourage;
                        int cost = 0;
                        if (!DataHolder.inst().isNpc(sub.getName())) {
                            courage = DataHolder.inst().getModifiers().getCourage(sub.getName())[0];
                        }
                        if (DataHolder.inst().getCombatForPlayer(sub.getName()) != null) {
                            cost = 0;
                        }
                        if (courage < cost) {
                            player.sendMessage("§4Недостаточно куража для использования! Требуется " + cost + " для использования" + ((DataHolder.inst().getCombatForPlayer(sub.getName()) != null) ? " в бою!" : "!"));
                            return;
                        }
                        int healamount = heal.getInteger("amount");
                        WoundType woundType = null;
                        if (sub.getWoundPyramid().getNumberOfWounds(WoundType.CRITICAL) > 0) {
                            woundType = WoundType.CRITICAL;
                            healamount = (int) healamount / 2;
                        } else if (sub.getWoundPyramid().getNumberOfWounds(WoundType.LETHAL) > 0) {
                            woundType = WoundType.LETHAL;
                        } else if (sub.getWoundPyramid().getNumberOfWounds(WoundType.NONLETHAL) > 0) {
                            woundType = WoundType.NONLETHAL;
                            healamount = healamount * 2;
                        }
                        if (woundType == null) {
                            player.sendMessage("§4Нет ран!");
                            return;
                        }
                        DataHolder.inst().getModifiers().healWounds(sub.getName(), healamount, woundType);
                        sub.changeWoundsNumber(woundType, Math.max(0, sub.getWoundPyramid().getNumberOfWounds(woundType) - healamount));
                        if (sub instanceof NPC) {
                            NPC npc1 = (NPC) sub;
                            npc1.setWoundsInDescription();
                        }
                        if (!DataHolder.inst().isNpc(sub.getName())) {
                            DataHolder.inst().getModifiers().updateCourage(sub.getName(), courage - cost, sub.maxcourage);
                        }
                        stimulator.setCount(stimulator.getCount() - 1);
                        ServerProxy.sendMessageFromAndInformMasters(sub, new Message("§a" + sub.getName() + " §2использует " + stimulator.getDisplayName() + " §2по назначению!"));
                        DiscordBridge.sendMessage(sub.getName() + " использует хилку на " + healamount);
                    } else if (stimulator.getTagCompound().getCompoundTag("useable").getString("type").equals("heal2")) {
                        NBTTagCompound heal = stimulator.getTagCompound().getCompoundTag("useable");
                        int courage = sub.maxcourage;
                        int cost = 0;
                        if (!DataHolder.inst().isNpc(sub.getName())) {
                            courage = DataHolder.inst().getModifiers().getCourage(sub.getName())[0];
                        }
                        if (DataHolder.inst().getCombatForPlayer(sub.getName()) != null) {
                            cost = 0;
                        }
                        if (courage < cost) {
                            player.sendMessage("§4Недостаточно куража для использования! Требуется " + cost + " для использования" + ((DataHolder.inst().getCombatForPlayer(sub.getName()) != null) ? " в бою!" : "!"));
                            return;
                        }
                        int crit = 0;
                        int leth = 0;
                        int nonleth = 0;
                        if (heal.hasKey("crit")) crit = heal.getInteger("crit");
                        if (heal.hasKey("leth")) leth = heal.getInteger("leth");
                        if (heal.hasKey("nonleth")) nonleth = heal.getInteger("nonleth");
                        int healamount = 0;
                        WoundType woundType = null;
                        if (sub.getWoundPyramid().getNumberOfWounds(WoundType.CRITICAL) > 0 && crit > 0) {
                            woundType = WoundType.CRITICAL;
                            healamount = crit;
                        } else if (sub.getWoundPyramid().getNumberOfWounds(WoundType.LETHAL) > 0 && leth > 0) {
                            woundType = WoundType.LETHAL;
                            healamount = leth;
                        } else if (sub.getWoundPyramid().getNumberOfWounds(WoundType.NONLETHAL) > 0 && nonleth > 0) {
                            woundType = WoundType.NONLETHAL;
                            healamount = nonleth;
                        }
                        if (woundType == null || healamount == 0) {
                            player.sendMessage("§4Нет подходящих ран для лечения!");
                            return;
                        }
                        DataHolder.inst().getModifiers().healWounds(sub.getName(), healamount, woundType);
                        sub.changeWoundsNumber(woundType, Math.max(0, sub.getWoundPyramid().getNumberOfWounds(woundType) - healamount));
                        if (sub instanceof NPC) {
                            NPC npc1 = (NPC) sub;
                            npc1.setWoundsInDescription();
                        }
                        if (!DataHolder.inst().isNpc(sub.getName())) {
                            DataHolder.inst().getModifiers().updateCourage(sub.getName(), courage - cost, sub.maxcourage);
                        }
                        stimulator.setCount(stimulator.getCount() - 1);
                        ServerProxy.sendMessageFromAndInformMasters(sub, new Message("§a" + sub.getName() + " §2использует " + stimulator.getDisplayName() + " §2по назначению!"));
                        DiscordBridge.sendMessage(sub.getName() + " использует хилку на " + healamount + ", чтобы вылечить " + woundType.getDesc());
                    }
                }
            }
        }

    }
}

