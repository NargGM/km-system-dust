package ru.konungstvo.commands.executor;

import de.cas_ual_ty.gci.GunCus;
import de.cas_ual_ty.gci.SoundEventGCI;
import de.cas_ual_ty.gci.item.ItemGun;
import de.cas_ual_ty.gci.network.MessageSound;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.combat.dice.modificator.Modificator;
import ru.konungstvo.combat.dice.modificator.ModificatorType;
import ru.konungstvo.combat.equipment.Firearm;
import ru.konungstvo.combat.equipment.Weapon;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.exceptions.PermissionException;
import ru.konungstvo.kmrp_lore.helpers.WeaponDurabilityHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.kmrp_lore.helpers.WeightHandler;
import ru.konungstvo.player.ModifiersState;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

import java.util.ArrayList;
import java.util.Random;

public class ShootExecutor extends CommandBase {


    @Override
    public String getName() {
        return "shoot";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        int hand = 1;
        if (args.length > 0 && args[0].equals("left")) hand = 0;
        if (player.getSubordinate() != null) player = player.getSubordinate();
        System.out.println("test1");
        EntityLivingBase forgePlayer = player.getEntity();
        WeaponTagsHandler weaponTagsHandler = null;
        Weapon weapon = null;
        ItemStack activeItem = null;
        if (hand == 1) activeItem = forgePlayer.getHeldItemMainhand();
        else activeItem = forgePlayer.getHeldItemOffhand();
        if (activeItem == null || activeItem.isEmpty()) return;
        weaponTagsHandler = new WeaponTagsHandler(activeItem);
        System.out.println("test2");
        if (!weaponTagsHandler.isWeapon()) return;
        if (!weaponTagsHandler.isFirearm()) return;
        if (!weaponTagsHandler.isFirearmLoaded()) {
            Message clack = new Message("Клац! Похоже, оружие разряжено…", ChatColor.RED);
//                attacker.sendMessage(clack);
            ServerProxy.sendMessageFromAndInformMasters(player, clack);
            GunCus.channel.sendToAllAround(new MessageSound(forgePlayer, GunCus.SOUND_KLIN, 4F, 1F), new NetworkRegistry.TargetPoint(forgePlayer.world.provider.getDimension(), forgePlayer.posX, forgePlayer.posY, forgePlayer.posZ, 16F));
            return;
        }
        weapon = new Weapon(weaponTagsHandler.getDefaultWeaponName(), weaponTagsHandler.getDefaultWeapon(), weaponTagsHandler);
        WeaponDurabilityHandler weaponDurabilityHandler = null;
        weaponDurabilityHandler = player.getDurHandlerForHand(hand);
        if (weaponDurabilityHandler.hasDurabilityForTag(weaponTagsHandler.getDefaultWeaponName())) {
            double ratio = weaponDurabilityHandler.getPercentageRatioForTag(weaponTagsHandler.getDefaultWeaponName());
            if(ratio <= 0) {
                Message clack = new Message("Клац! Похоже, оружие заклинило…", ChatColor.RED);
                ServerProxy.sendMessageFromAndInformMasters(player, clack);
                GunCus.channel.sendToAllAround(new MessageSound(forgePlayer, GunCus.SOUND_KLIN, 3F, 1F), new NetworkRegistry.TargetPoint(forgePlayer.world.provider.getDimension(), forgePlayer.posX, forgePlayer.posY, forgePlayer.posZ, 16F));
                return;
            } else if(ratio <= 25 ) {
                Random random = new Random();
                int randomResult = random.nextInt(100) + 1;
                System.out.println(randomResult);
                if(randomResult <= (26 - ratio)) {
                    Message clack = new Message("Клац! Похоже, оружие заклинило…", ChatColor.RED);
                    ServerProxy.sendMessageFromAndInformMasters(player, clack);
                    GunCus.channel.sendToAllAround(new MessageSound(forgePlayer, GunCus.SOUND_KLIN, 3F, 1F), new NetworkRegistry.TargetPoint(forgePlayer.world.provider.getDimension(), forgePlayer.posX, forgePlayer.posY, forgePlayer.posZ, 16F));
                    return;
                }
            }
        }

        if(weapon.getFirearm().getHomemade() > 0) {
            Random random = new Random();
            if(weapon.getFirearm().getHomemade() >= (random.nextInt(100) + 1)) {
                Message clack = new Message("Клац! Похоже, оружие заклинило…", ChatColor.RED);
                ServerProxy.sendMessageFromAndInformMasters(player, clack);
                GunCus.channel.sendToAllAround(new MessageSound(forgePlayer, GunCus.SOUND_KLIN, 3F, 1F), new NetworkRegistry.TargetPoint(forgePlayer.world.provider.getDimension(), forgePlayer.posX, forgePlayer.posY, forgePlayer.posZ, 16F));
                return;
            }
        }
        Random random = new Random();
        if(weapon.getFirearm().getLoadedProjectile().getHomemade() >= (random.nextInt(100) + 1)) {
            Message clack = new Message("Клац! Похоже, оружие заклинило…", ChatColor.RED);
            ServerProxy.sendMessageFromAndInformMasters(player, clack);
            GunCus.channel.sendToAllAround(new MessageSound(forgePlayer, GunCus.SOUND_KLIN, 3F, 1F), new NetworkRegistry.TargetPoint(forgePlayer.world.provider.getDimension(), forgePlayer.posX, forgePlayer.posY, forgePlayer.posZ, 16F));
            return;
        }

        if (weapon.getFirearm().getProjectile().getCost() >= 0) {
            if(!DataHolder.inst().useMana(player.getName(), weapon.getFirearm().getProjectile().getCost())) { //TODO Сделать только для магии
                if(weapon.getFirearm().getProjectile().getCaliber().equals("магия")) {
                    Message clack = new Message("Пуф! Ничего не произошло… (недостаточно маны)", ChatColor.RED);
                    ServerProxy.sendMessageFromAndInformMasters(player, clack);
                    return;
                }
            }
        }

        player.fireProjectile(hand);
        if (weaponDurabilityHandler.hasDurabilityForTag(weapon.getName()))
            weaponDurabilityHandler.takeAwayDurabilityForTag(weapon.getName(), 1);
        ServerProxy.sendMessageFromAndInformMasters(player, new Message(player.getName() + " " + "стреляет!", ChatColor.RED));
        System.out.println("TEST1 " + weapon.getSound() + "_shot");
        System.out.println("TEST2 " + GunCus.getIDFromRl(weapon.getSound() + "_shot"));
        if (!weapon.getSound().isEmpty()) GunCus.channel.sendToAllAround(new MessageSound(forgePlayer, weapon.getSound() + "_shot", 7F, 1F), new NetworkRegistry.TargetPoint(forgePlayer.world.provider.getDimension(), forgePlayer.posX, forgePlayer.posY, forgePlayer.posZ, weapon.getFirearm().getSoundRange()));
//        if (weapon.getFirearm().shouldBoom()) {
//            if (player.getItemForHand(hand).getItem() instanceof ItemGun) {
//                try {
//                    ItemStack is = player.getItemForHand(hand);
//                    ItemGun gun = (ItemGun) is.getItem();
//                    //System.out.println(gun.hasOptic(is));
//
//                    gun.doShoot(forgePlayer, is, false, false, weapon.getFirearm().getSort());
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            } else {
//                switch (weapon.getFirearm().getSort()) {
//                    case 3:
//                        GunCus.channel.sendToAllAround(new MessageSound(forgePlayer, GunCus.SOUND_SHOOT_HEAVY, 8F, 1F), new NetworkRegistry.TargetPoint(forgePlayer.world.provider.getDimension(), forgePlayer.posX, forgePlayer.posY, forgePlayer.posZ, 150F));
//                        break;
//                    case 2:
//                        GunCus.channel.sendToAllAround(new MessageSound(forgePlayer, GunCus.SOUND_SHOOT_MEDIUM, 7F, 1F), new NetworkRegistry.TargetPoint(forgePlayer.world.provider.getDimension(), forgePlayer.posX, forgePlayer.posY, forgePlayer.posZ, 100F));
//                        break;
//                    case 1:
//                        GunCus.channel.sendToAllAround(new MessageSound(forgePlayer, GunCus.SOUND_SHOOT_LIGHT, 6F, 1F), new NetworkRegistry.TargetPoint(forgePlayer.world.provider.getDimension(), forgePlayer.posX, forgePlayer.posY, forgePlayer.posZ, 80F));
//                        break;
//                }
//            }
//        }

    }
}
