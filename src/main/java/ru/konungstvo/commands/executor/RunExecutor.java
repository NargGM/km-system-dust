package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.combat.movement.MovementHistory;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

public class RunExecutor extends CommandBase {
    public static String execute(Player executor, String... args) {
        if (args.length == 0) {
            return "Тут будет хелб";
        }
        Player pl = DataHolder.inst().getPlayer(args[0]);
        if (pl == null) {
            return ChatColor.RED + "Игрок " + args[0] + " не найден!";
        }
        if (args.length == 1) {
            if (pl.getMovementHistory() == MovementHistory.RUN_UP || pl.getMovementHistory() == MovementHistory.NOW || pl.getMovementHistory() == MovementHistory.LAST_TIME) {
                return ChatColor.GRAY + "Игрок " + args[0] + " уже разбежался.";
            }
            return ChatColor.GRAY + "Игрок " + args[0] + " ещё не разбежался.";
        }
        if (args[1].equals("on")) {
            pl.setMovementHistory(MovementHistory.RUN_UP);
            return ChatColor.GRAY + "Теперь у игрока " + args[0] + " есть разбег.";
        }
        if (args[1].equals("off")) {
            pl.setMovementHistory(MovementHistory.NONE);
            return ChatColor.GRAY + "У игрока " + args[0] + " больше нет разбега.";
        }
        return ChatColor.RED + "Неверный ввод!";
    }

    @Override
    public String getName() {
        return "run";
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permission.GM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }


    @Override
    public String getUsage(ICommandSender iCommandSender) {
        return "";
    }

    @Override
    public void execute(MinecraftServer minecraftServer, ICommandSender iCommandSender, String[] strings) throws CommandException {
        String answer = execute(DataHolder.inst().getPlayer(iCommandSender.getName()), strings);
        iCommandSender.sendMessage(new TextComponentString(answer));
    }
}
