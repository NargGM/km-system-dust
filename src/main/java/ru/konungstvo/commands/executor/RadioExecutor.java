package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Player;

public class RadioExecutor extends CommandBase {
    public static String execute(Player executor, String... args) {
    /*
        if (!executor.hasPermission(Permission.RADIO)) {
            return ChatColor.RED + "У вас нет рации!";
        }

        RoleplayMessage radioRoleplayMessage = new RoleplayMessage(executor.getName(), String.join(" ", args), Range.RADIO);
        radioRoleplayMessage.build();
        for (Player pl : DataHolder.getInstance().getPlayerList()) {
            if (pl.hasPermission(Permission.RADIO)) {
                pl.sendMessage(radioRoleplayMessage.getResult());
            }
        }
        DiscordBridge.sendMessage(radioRoleplayMessage.getResult());

     */
        return null;
    }

    @Override
    public String getName() {
        return "radio";
    }

    @Override
    public String getUsage(ICommandSender iCommandSender) {
        return "";
    }

    @Override
    public void execute(MinecraftServer minecraftServer, ICommandSender iCommandSender, String[] strings) throws CommandException {
        String answer = execute(DataHolder.inst().getPlayer(iCommandSender.getName()), strings);
        iCommandSender.sendMessage(new TextComponentString(answer));
    }
}
