package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.bridge.discord.DiscordBridge;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Player;

public class TellExecutor extends CommandBase {
    public static String execute(Player executor, String[] args) {
        if (args.length == 0 || args[0].equals("help")) return "Тут будет хелп"; //TODO
        String receiverName = args[0];
        Player receiver = DataHolder.inst().getPlayer(receiverName);
        if (receiver == null) return "Нет игрока с ником " + receiverName + "!";
        if (args.length < 2) return "Нельзя отправить пустое сообщение!";
        args[0] = "§8[§a" + executor.getName() + "§8->§a" + receiverName + "§8]:§f";
        StringBuilder result = new StringBuilder();
        for (String arg : args) {
            result.append(" ").append(arg);
        }
        receiver.sendMessage(result.toString());
//        executor.sendMessage(result.toString());
        ServerProxy.informMasters(result.toString());
        //DiscordBridge.sendMessage(result.toString());
        return null;
    }

    @Override
    public String getName() {
        return "tell";
    }

    @Override
    public String getUsage(ICommandSender iCommandSender) {
        return "";
    }

    @Override
    public void execute(MinecraftServer minecraftServer, ICommandSender iCommandSender, String[] strings) throws CommandException {
        String answer = execute(DataHolder.inst().getPlayer(iCommandSender.getName()), strings);
        iCommandSender.sendMessage(new TextComponentString(answer));
    }
}
