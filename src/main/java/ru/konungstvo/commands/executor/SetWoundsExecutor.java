package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.exceptions.GenericException;
import ru.konungstvo.player.NPC;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;
import ru.konungstvo.player.wounds.WoundType;

import java.util.Arrays;

public class SetWoundsExecutor extends CommandBase {

    @Override
    public String getName() {
        return "setwounds";
    }

    @Override
    public String getUsage(ICommandSender iCommandSender) {
        return "";
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permission.GM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }


        @Override
    public void execute(MinecraftServer minecraftServer, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());

        Message message = new Message();
        if (args.length < 1) {
            message.addComponent(new MessageComponent("Пример использования: /setwounds Зверолов light 2 (кол-во лёгких ран будет равно 2).", ChatColor.GRAY));
            player.sendMessage(message);
            return;
        }
        Player npc = DataHolder.inst().getPlayer(args[0]);
        if (npc == null) {
            message.addComponent(new MessageComponent("НПС или игрок не найден!", ChatColor.RED));
            player.sendMessage(message);
            return;
        }
        if (!(npc instanceof NPC)) {
            //return ChatColor.RED + "Применимо только к НПС! Пожалуйста, управляйте ранами игроков на сайте.";
        }

        if (args.length < 2) {
            message.addComponent(new MessageComponent("Вы не указали тип раны (scratch, light, severe, critical) и их количество.", ChatColor.RED));
            player.sendMessage(message);
            return;
        }
        if (args.length < 3) {
            message.addComponent(new MessageComponent("Вы не указали количество ран.", ChatColor.RED));
            player.sendMessage(message);
            return;
        }

        WoundType woundType = null;
        boolean bloodloss = false;
        boolean burning = false;
        switch (args[1]) {
            case "scratch":
            case "scr":
            case "царапина":
            case "цар":
                woundType = WoundType.NONLETHAL;
                break;
            case "light":
            case "l":
            case "легкая":
            case "лег":
                woundType = WoundType.LETHAL;
                break;
            case "severe":
            case "svr":
            case "тяжелая":
            case "тяж":
                woundType = WoundType.CRITICAL;
                break;
            case "critical":
            case "crit":
            case "критическая":
            case "крит":
                woundType = WoundType.CRITICAL_DEPR;
                break;
            case "deadly":
            case "dead":
            case "смертельная":
            case "смерть":
                woundType = WoundType.DEADLY;
                break;
            case "blood":
            case "bloodloss":
            case "кровь":
            case "кровопотеря":
                DataHolder.inst().getModifiers().updateBloodloss(npc.getName(), Integer.parseInt(args[2]));
                bloodloss = true;
                break;
/*            case "burn":
            case "burning":
            case "горение":
            case "горит":
                npc.addStatusEffect("");
                burning = true;
                break;*/
            case "add":
                if (args[2].equals("?") || args[2].equals("help")) {
                    message.addComponent(new MessageComponent("Синтаксис: /setwounds [ник игрока/имя нпц] add [урон цифрой] {описание, в котором могут быть теги модификаторов ран}\n", ChatColor.GMCHAT));
                    message.addComponent(new MessageComponent("Пример: /setwounds Зверолов add 3 [горение] загорелся от зажигательной смеси\n", ChatColor.GMCHAT));
                    message.addComponent(new MessageComponent("Команда из примера даст Зверолову царапину с модификатором горения и указанным описанием. Если пирамидка забита, рана автоматически переходит на уровень выше.\n", ChatColor.GMCHAT));
                    message.addComponent(new MessageComponent("Список тегов модификаторов ран (в скобках сокращения (работают только при использовании команды, на сайте пишите полностью)):\n", ChatColor.GMCHAT));

                    message.addComponent(new MessageComponent("[слабое кровотечение] ([слк]), [сильное кровотечение] ([сик]), [критическое кровотечение] ([кк]), [горение] ([г]), \n", ChatColor.NICK));
                    message.addComponent(new MessageComponent("[оглушение:станет переходной через 5 ходов] ([о5]), [оглушение:станет переходной через 4/3/2 хода] ([о4]) ([о3]) ([о2]), [оглушение:станет переходной через ход] (о1), \n", ChatColor.DARK_GREEN));
                    message.addComponent(new MessageComponent("[пропуск следующего хода] ([п1]), [пропуск следующих двух ходов] ([п2]), [пропуск следующих трех ходов] ([п3]), \n", ChatColor.NICK));
                    message.addComponent(new MessageComponent("[легкое повреждение правой руки] ([лппр]), [тяжелое повреждение правой руки] ([тппр]), [легкое повреждение левой руки] ([лплр]), [тяжелое повреждение левой руки] ([тплр]), \n", ChatColor.DARK_GREEN));
                    message.addComponent(new MessageComponent("[легкое повреждение правой ноги] ([лппн]), [тяжелое повреждение правой ноги] ([тппн]), [легкое повреждение левой ноги] ([лплн]), [тяжелое повреждение левой ноги] ([тплн]), \n", ChatColor.NICK));
                    message.addComponent(new MessageComponent("[ментальная рана] ([мр]). \n", ChatColor.DARK_GREEN));
                    player.sendMessage(message);
                    return;
                }
                String desc = String.join(" ", Arrays.copyOfRange(args, 3, args.length));
                desc = desc.replace("[слк]","[слабое кровотечение]").replace("[сик]","[сильное кровотечение]").replace("[кк]", "[критическое кровотечение]").replace("[г]", "[горение]")
                        .replace("[о5]", "[оглушение:станет переходной через 5 ходов]").replace("[о4]", "[оглушение:станет переходной через 4 хода]").replace("[о3]", "[оглушение:станет переходной через 3 хода]").replace("[о2]", "[оглушение:станет переходной через 2 хода]").replace("[о1]", "[оглушение:станет переходной через ход]")
                        .replace("[п1]", "[пропуск следующего хода]").replace("[п2]", "[пропуск следующих двух ходов]").replace("[п3]", "[пропуск следующих трех ходов]")
                        .replace("[лппр]", "[легкое повреждение правой руки]").replace("[тппр]", "[тяжелое повреждение правой руки]").replace("[лплр]", "[легкое повреждение левой руки]").replace("[тплр]", "[тяжелое повреждение левой руки]")
                        .replace("[лппн]", "[легкое повреждение правой ноги]").replace("[тппн]", "[тяжелое повреждение правой ноги]").replace("[лплн]", "[легкое повреждение левой ноги]").replace("[тплн]", "[тяжелое повреждение левой ноги]")
                        .replace("[мр]", "[ментальная рана]");
                npc.inflictDamage(Integer.parseInt(args[2]), desc);
                message.addComponent(new MessageComponent("Рана с уроном " + Integer.parseInt(args[2]) + " и описанием '" + desc + "' добавлена!\n", ChatColor.GMCHAT));
                message.addComponent(npc.getWoundPyramid().toNiceMessage());
                player.sendMessage(message);
                return;

        }
        if (bloodloss) {
            message.addComponent(new MessageComponent("Уровень кровопотери " + npc.getName() + " обновлен: " + Integer.parseInt(args[2]), ChatColor.GMCHAT));
            player.sendMessage(message);
            return;
        }
        if (burning) {
            message.addComponent(new MessageComponent("Игроку " + npc.getName() + " добавлен модификатор горения", ChatColor.GMCHAT));
            player.sendMessage(message);
            return;
        }
        if (woundType == null) {
            throw new GenericException("1", "Тип раны не указан!");
        }

        int number = Integer.parseInt(args[2]);

        npc.changeWoundsNumber(woundType, number);

        if (npc instanceof NPC) {
            NPC npc1 = (NPC) npc;
            npc1.setWoundsInDescription();
        }

        message.addComponent(new MessageComponent("Раны " + npc.getName() + " обновлены: \n", ChatColor.GMCHAT));
        message.addComponent(npc.getWoundPyramid().toNiceMessage());
        player.sendMessage(message);
        return;
    }
}
