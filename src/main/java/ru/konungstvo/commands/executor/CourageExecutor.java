package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.exceptions.PermissionException;
import ru.konungstvo.kmrp_lore.helpers.WeightHandler;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

public class CourageExecutor extends CommandBase {


    @Override
    public String getName() {
        return "courage";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player sub = player;
        if (player.getSubordinate() != null) sub = player.getSubordinate();
        DataHolder dh = DataHolder.inst();
        if (args.length < 1) {
            player.sendMessage(new Message("Уровень вашего куража: " + sub.courage + "/" + sub.maxcourage, ChatColor.OOC));
            return;
        }
        if (!player.hasPermission(Permission.GM) && !player.hasPermission(Permission.CM)) {
            throw new PermissionException("1", "Недостаточно прав!");
        }

        switch (args[0]) {
            case "setmax":
                if (args.length < 3) return;
                int maxcourage = Integer.parseInt(args[2]);
                String playername = args[1];
                Player pl = DataHolder.inst().getPlayer(playername);
                pl.maxcourage = maxcourage;
                DataHolder.inst().getModifiers().updateCourage(playername, pl.courage, pl.maxcourage);
                return;
            case "remove":
                if (args.length < 3) return;
                int courage = Integer.parseInt(args[2]);
                String playername1 = args[1];
                Player pl1 = DataHolder.inst().getPlayer(playername1);
                pl1.courage = Math.max(0, pl1.courage - courage);
                DataHolder.inst().getModifiers().updateCourage(playername1, pl1.courage, pl1.maxcourage);
                return;
            case "add":
                if (args.length < 3) return;
                int courage1 = Integer.parseInt(args[2]);
                String playername12 = args[1];
                Player pl12 = DataHolder.inst().getPlayer(playername12);
                pl12.courage = Math.max(0, pl12.courage + courage1);
                DataHolder.inst().getModifiers().updateCourage(playername12, pl12.courage, pl12.maxcourage);
                return;
        }
    }
}
