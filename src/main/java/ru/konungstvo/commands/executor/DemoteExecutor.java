package ru.konungstvo.commands.executor;

import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

@Deprecated
public class DemoteExecutor {
    public static String execute(Player executor, String[] args) {
        if (args.length == 0 || args[0].equals("help")) return "Тут будет хелп"; //TODO
        if (args.length == 1) return "§8Кого вы хотите понизить? Укажите ник, например: /demote gm NickHere";
        switch (args[0]) {
            case "gm":
                DataHolder.inst().getPlayer(args[1]).removePermission(Permission.GM);
//                DataHolder.getInstance().promoteGameMaster(args[1]);
                return "§8" + args[1] + " больше не GameMaster!";
            case "builder":
                DataHolder.inst().getPlayer(args[1]).removePermission(Permission.BD);
//                DataHolder.getInstance().promoteBuilder(args[1]);
                return "§8" + args[1] + " больше не Builder!";
        }
        return "§8Ничего не произошло. Убедитесь, что вы правильно ввели команду.";
    }
}
