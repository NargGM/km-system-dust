package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import ru.konungstvo.combat.StatusType;
import ru.konungstvo.combat.movement.ChargeHistory;
import ru.konungstvo.combat.movement.MovementHistory;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Player;

import java.util.Arrays;

public class NextExecutor extends CommandBase {
    public static final String NAME = "endturn";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;

    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {

        DataHolder.inst().getPlayer(sender.getName()).performCommand("/" + PurgeModifiersExecutor.NAME);
        String acted = "";
        if (args.length > 0) {
            Player pl = DataHolder.inst().getPlayer(sender.getName());
            switch (args[0]) {
                case "combat":
                    acted = " combat";
                    break;
                case "resetrecoil":
                    if (pl.getSubordinate() != null) {
                        pl.getSubordinate().setRecoil(0);
                    } else {
                        pl.setRecoil(0);
                    }
                    break;
                case "resetmovement":
                    if (pl.getSubordinate() != null) {
                        pl.getSubordinate().setMovementHistory(MovementHistory.NONE);
                        pl.getSubordinate().setRecoil(0);
                    } else {
                        pl.setMovementHistory(MovementHistory.NONE);
                        pl.setRecoil(0);
                    }
                    acted = " resetmovement";
                    break;
                case "extinguish":
                    if (pl.getSubordinate() != null) {
                        pl.getSubordinate().setMovementHistory(MovementHistory.NONE);
                        pl.getSubordinate().makeFall(); //тушение в queueexecutor
                        pl.getSubordinate().setRecoil(0);
                    } else {
                        pl.setMovementHistory(MovementHistory.NONE);
                        pl.makeFall(); //тушение в queueexecutor
                        pl.setRecoil(0);
                    }
                    acted = " extinguish";
                    break;
                case "charge":
                    if (pl.getSubordinate() != null) {
                        pl.getSubordinate().setChargeHistory(ChargeHistory.CHARGE);
                        pl.getSubordinate().setRecoil(0);
                    } else {
                        pl.setChargeHistory(ChargeHistory.CHARGE);
                        pl.setRecoil(0);
                    }
                    acted = " charge";
                    break;
                case "concentrated":
                    acted = " concentrated";
                    if (args.length > 1) {
                        String skill = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
                        acted = acted + " " + skill;
                    }
                    if (pl.getSubordinate() != null) {
                        pl.getSubordinate().setRecoil(0);
                    } else {
                        pl.setRecoil(0);
                    }
                    break;
                case "magic":
                    acted = " magic";
                    if (pl.getSubordinate() != null) {
                        pl.getSubordinate().setRecoil(0);
                    } else {
                        pl.setRecoil(0);
                    }
                    break;
                case "defensivestance":
                    acted = " defensivestance";
                    if (pl.getSubordinate() != null) {
                        pl.getSubordinate().setRecoil(0);
                        //pl.getSubordinate().setDefensiveStance(true);
                    } else {
                        pl.setRecoil(0);
                        //pl.setDefensiveStance(true);
                    }
                    break;
                default:
                    if (pl.getSubordinate() != null) {
                        pl.getSubordinate().setRecoil(0);
                    } else {
                        pl.setRecoil(0);
                    }
            }
        }

        ClickContainer.deleteContainer("ToolPanel", getCommandSenderAsPlayer(sender));
        ClickContainer.deleteContainer("PerformAction", getCommandSenderAsPlayer(sender));
        DataHolder.inst().getPlayer(sender.getName()).performCommand("/queue next" + acted);
    }

}
