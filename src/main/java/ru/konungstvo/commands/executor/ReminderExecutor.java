package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.Reminder;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

import java.util.List;

public class ReminderExecutor extends CommandBase {

    @Override
    public String getName() {
        return "remind";
    }

    @Override
    public String getUsage(ICommandSender iCommandSender) {
        return "";
    }

    @Override
    public void execute(MinecraftServer minecraftServer, ICommandSender iCommandSender, String[] strings) throws CommandException {
        String answer = execute(DataHolder.inst().getPlayer(iCommandSender.getName()), strings);
        iCommandSender.sendMessage(new TextComponentString(answer));
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permission.GM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String execute(Player executor, String[] args) {
        if (args.length == 0 || args[0].equals("help")) {
            return "§e----------- §fHelp: remind §e--------------------§8" +
                    "\nRemind usage: Use this to remind player of something when he goes online." +
                    "\n§e/remind Player You suffer from nausea." +
                    "\n/remind show§f - show current reminders." +
                    "\n§e/remind remove Player§f or §e/remind delete Player§f - delete reminder." +
                    "\n§e/remind edit Player You are hungry§f - overwrite an existing reminder." +
                    "\n§e/remind§f or §e/remind help§f - show this message.\n";
        }
        List<Reminder> reminders = DataHolder.inst().getReminders();
        switch (args[0]) {
            case "show":
                StringBuilder result = new StringBuilder();
                result.append("§eCurrent reminders:§f\n");
                if (reminders.isEmpty()) {
                    return result.append("§8No reminders!").toString();
                }
                for (Reminder reminder : reminders) {
                    result.append("§f").append(reminder.getPlayerName()).append(": §f").append(reminder.getContent()).append("\n");
                }
                return result.toString();
            case "remove":
            case "delete":
                if (args.length < 2) return "§4Not enough arguments!§f";
                //TODO: more efficient way to do this?
                Reminder toRemove = null;
                for (Reminder reminder : reminders) {
                    if (reminder.getPlayerName().equals(args[1])) {
                        toRemove = reminder;
                    }
                }
                if (toRemove == null) {
                    return "§4Reminder not found!§f";
                }
                reminders.remove(toRemove);
                return "§eReminder successfully deleted!§f";
            case "edit":
                if (args.length < 3) return "§4Not enough arguments!§f";
                Reminder toEdit = null;
                for (Reminder reminder : reminders) {
                    if (reminder.getPlayerName().equals(args[1])) {
                        toEdit = reminder;
                    }
                }
                if (toEdit == null) {
                    return "§4Reminder not found!§f";
                }
                StringBuilder content = new StringBuilder();
                for (int i = 2; i < args.length; i++) {
                    content.append(args[i]).append(" ");
                }
                toEdit.setContent(content.toString());
                return "§eReminder successfully edited!§f";
        }
        String playerName = args[0];
        for (Reminder reminder : reminders) {
            if (reminder.getPlayerName().equals(playerName)) {
                return "§4Existing reminder for this player found! Please use /remind edit to edit.§f";
            }
        }
        StringBuilder content = new StringBuilder();
        for (int i = 1; i < args.length; i++) {
            content.append(args[i]).append(" ");
        }
        reminders.add(new Reminder(playerName, content.toString()));
        return "§eReminder successfully added!§f";
    }

}
