package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.*;
import ru.konungstvo.combat.duel.Duel;
import ru.konungstvo.commands.helpercommands.player.turn.ToRoot;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

import java.util.List;

public class FeintExecutor extends CommandBase {
    private static String START_MESSAGE = "Выберите прием:\n";
    public static final String NAME = "setfeint";

    @Override
    public String getName() {
        return "setfeint";
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        //String modifierType = args[0];
        String oldContainer = "";
        String oldoldContainer = "";
        Player target = null;
        String feint = "";
        if (args.length > 0 && !args[0].equals("%IGNORE%")) {
            feint = args[0];
        }
        if (args.length > 1) {
            oldContainer = args[1];
        }

        if (args.length > 2) {
            oldoldContainer = args[2];
        }

        if (!oldContainer.isEmpty()) {
            ClickContainerMessage remove = new ClickContainerMessage(oldContainer, true);
            KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
        }


        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player subordinate = DataHolder.inst().getPlayer(sender.getName());
        if (subordinate.getSubordinate() != null) subordinate = subordinate.getSubordinate();

        if (!oldContainer.isEmpty()) oldContainer = " " + oldContainer;
        if (!oldoldContainer.isEmpty()) oldoldContainer = " " + oldoldContainer;

        if (!feint.isEmpty()) {
//
            switch (feint) {
                case "reckless":
                    subordinate.addStatusEffect("Безрассудная атака", StatusEnd.TURN_END, 1, StatusType.RECKLESS_ATTACK, "",-666, new StatusEffect("Безрассудная защита", StatusEnd.TURN_START, 1, StatusType.RECKLESS_DEFENCE));
                    break;
                case "feint":
                    subordinate.addStatusEffect("Финт", StatusEnd.TURN_START, 1, StatusType.FEINT);
                    break;
                case "defense":
                    SkillDiceMessage message = new SkillDiceMessage(subordinate.getName(), "% физическая защита");
                    message.build();
                    ServerProxy.sendMessageFromAndInformMasters(subordinate, new Message(subordinate.getName() + " входит в глухую оборону.", ChatColor.COMBAT));
                    ServerProxy.sendMessageFromAndInformMasters(subordinate, message);
                    if (message.getDice().getResult() > 0) subordinate.addStatusEffect("Глухая оборона", StatusEnd.TURN_START, 1, StatusType.RELENTLESS, message.getDice().getResult());

                    break;
                case "psychdefense":
                    SkillDiceMessage message1 = new SkillDiceMessage(subordinate.getName(), "% психическая защита");
                    message1.build();
                    ServerProxy.sendMessageFromAndInformMasters(subordinate, new Message(subordinate.getName() + " входит в психическую оборону.", ChatColor.COMBAT));
                    ServerProxy.sendMessageFromAndInformMasters(subordinate, message1);
                    if (message1.getDice().getResult() > 0) subordinate.addStatusEffect("Пси-оборона", StatusEnd.TURN_START, 1, StatusType.RELENTLESS_PSYCH, message1.getDice().getResult());
                    break;
                case "allydefense":
                    Message messagedef = new Message("Выберите цель защиты:\n", ChatColor.COMBAT);
                    EntityLivingBase forgePlayer;
                    EntityLivingBase forgeTarget;
                    if (!DataHolder.inst().isNpc(subordinate.getName())) {
                        forgePlayer = ServerProxy.getForgePlayer(subordinate.getName());
                    } else {
                        forgePlayer = DataHolder.inst().getNpcEntity(subordinate.getName());
                    }
                    List<Player> targets = DataHolder.inst().getCombatForPlayer(subordinate.getName()).getFighters();
                    for (Player potentialTarget : targets) {
                        if (potentialTarget == subordinate) continue;
                        if (potentialTarget != null) {
                            if (!DataHolder.inst().isNpc(potentialTarget.getName())) {
                                forgeTarget = ServerProxy.getForgePlayer(potentialTarget.getName());
                            } else {
                                forgeTarget = (EntityLivingBase) DataHolder.inst().getNpcEntity(potentialTarget.getName());
                            }
                        } else {
                            continue;
                        }
                        if (forgeTarget.getDistance(forgePlayer) <= 3) {
                            MessageComponent potTarg = new MessageComponent("[" + potentialTarget.getName() + "] ", ChatColor.BLUE);
                            potTarg.setClickCommand("/setfeint allydefenseready " + potentialTarget.getName());
                            messagedef.addComponent(potTarg);

                        }
                    }
                    MessageComponent back = new MessageComponent("[Назад]", ChatColor.GRAY);
                    back.setClickCommand("/setfeint %IGNORE% " + oldContainer + " " + oldoldContainer);
                    messagedef.addComponent(back);
                    Helpers.sendClickContainerToClient(messagedef, "Feint", (EntityPlayerMP) sender, subordinate);
                    return;
                case "allydefenseready":
                    ServerProxy.sendMessageFromAndInformMasters(subordinate, new Message(subordinate.getName() + " решает защищать " + args[1] + "!", ChatColor.DARK_GREEN));
                    subordinate.clearDefends();
                    subordinate.setDefends(DataHolder.inst().getPlayer(args[1]));
                    player.performCommand("/" + ToRoot.NAME + " Feint");
                    if (DataHolder.inst().getCombatForPlayer(subordinate.getName()) != null && subordinate.getCombatState() == CombatState.SHOULD_ACT && !subordinate.hasTrait(Trait.ALLY_DEFENCE)) player.performCommand("/endturn combat");
                    else {
                        subordinate.performCommand("/toolpanel");
                    }
                    return;
                case "clearall":
                    subordinate.removeFeints();
                    break;
                case "cleardefends":
                    if (subordinate.getDefends() != null) {
                        ServerProxy.sendMessageFromAndInformMasters(subordinate, new Message(subordinate.getName() + " решает больше не защищать " + subordinate.getDefends().getName() + "!", ChatColor.RED));
                    }
                    subordinate.clearDefends();
                    player.performCommand("/geteffects");
                    return;
            }
            player.performCommand("/" + ToRoot.NAME + " Feint"  + oldContainer + oldoldContainer);
            return;
        }

        Message message = new Message(START_MESSAGE, ChatColor.COMBAT);
        MessageComponent oppComponent22 = new MessageComponent("[Безрассудная атака] ", ChatColor.RED);
        oppComponent22.setClickCommand("/setfeint reckless " + oldContainer + oldoldContainer);
        oppComponent22.setHoverText(new TextComponentString("Тут будет текст"));
        message.addComponent(oppComponent22);
        oppComponent22 = new MessageComponent("[Финт] ", ChatColor.COMBAT);
        oppComponent22.setClickCommand("/setfeint feint " + oldContainer + oldoldContainer);
        oppComponent22.setHoverText(new TextComponentString("Тут будет текст"));
        message.addComponent(oppComponent22);

        oppComponent22 = new MessageComponent("[Глухая оборона] ", ChatColor.DARK_GRAY);
        oppComponent22.setClickCommand("/setfeint defense " + oldContainer + oldoldContainer);
        oppComponent22.setHoverText(new TextComponentString("Тут будет текст"));
        message.addComponent(oppComponent22);

        oppComponent22 = new MessageComponent("[Психическая оборона] ", ChatColor.DARK_PURPLE);
        oppComponent22.setClickCommand("/setfeint psychdefense " + oldContainer + oldoldContainer);
        oppComponent22.setHoverText(new TextComponentString("Тут будет текст"));
        message.addComponent(oppComponent22);

        oppComponent22 = new MessageComponent("[Защита союзника] ", ChatColor.DARK_GREEN);
        oppComponent22.setClickCommand("/setfeint allydefense " + oldContainer + oldoldContainer);
        oppComponent22.setHoverText(new TextComponentString("Тут будет текст"));
        message.addComponent(oppComponent22);

        MessageComponent toRoot = new MessageComponent("[Назад]", ChatColor.GRAY);
        toRoot.setClickCommand("/" + ToRoot.NAME + " Feint" + oldContainer + oldoldContainer);
        message.addComponent(toRoot);

        // SEND PACKET TO CLIENT
        Helpers.sendClickContainerToClient(message, "Feint", (EntityPlayerMP) sender, subordinate);
//        String json = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(message));
//        ClickContainerMessage result = new ClickContainerMessage("Feint", json);
//        KMPacketHandler.INSTANCE.sendTo(result, (EntityPlayerMP) sender);
    }
}
