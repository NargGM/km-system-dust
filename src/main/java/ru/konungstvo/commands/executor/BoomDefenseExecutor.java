package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.combat.Boom;
import ru.konungstvo.combat.BoomType;
import ru.konungstvo.combat.Combat;
import ru.konungstvo.combat.duel.DefenseType;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.player.defense.SelectDefenseItem;
import ru.konungstvo.commands.helpercommands.player.movement.MoveExecutor;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

import java.util.ArrayList;
import java.util.List;

public class BoomDefenseExecutor extends CommandBase {

    public static String START_MESSAGE = "Выберите способ защиты от взрыва:\n";
    public static String NAME = "boomd";
    @Override
    public String getName() {
        return "boomd";
    }
    @Override
    public List<String> getAliases() {
        List<String> res = new ArrayList();
        res.add("BoomDefense");
        return res;
    }
    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player subordinate = player;
        if (player.getSubordinate() != null) {
            player = player.getSubordinate();
            subordinate = player;
        }
        Combat combat =  DataHolder.inst().getCombatForPlayer(player.getName());
        if (combat == null) return;

        Boom boom = combat.getBoomForPlayer(player);

        if (boom == null) return;



        if (args.length > 0) {
            ClickContainerMessage remove = new ClickContainerMessage("BoomDefense", true);
            KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
            Message def;
            MessageComponent def2;
            String targetsStr = "";
            switch (args[0]) {
                case "cover":
                    String oldcontainer = "";
                    if (args.length > 1) {
                        try
                        {
                            int cover = Integer.parseInt(args[1]);
                            if (cover < 1) {
                                boom.removeCover(player);
                            } else {
                                boom.addCover(player, cover);
                            }
                            if (args.length > 2) {
                                if (DataHolder.inst().isNpc(player.getName())) {
                                    DataHolder.inst().getPlayer(sender.getName()).performCommand("/BoomDefense " + args[2]);
                                } else {
                                    player.performCommand("/BoomDefense " + args[2]);
                                }
                            } else {
                                if (DataHolder.inst().isNpc(player.getName())) {
                                    DataHolder.inst().getPlayer(sender.getName()).performCommand("/BoomDefense");
                                } else {
                                    player.performCommand("/BoomDefense");
                                }
                            }
                            return;
                        } catch (NumberFormatException ex)
                        {
                            oldcontainer = args[1];
                        }
                    }
                    Message message1 = new Message("Выберите прочность укрытия:\n", ChatColor.COMBAT);
                    MessageComponent oppComponent22 = new MessageComponent("[Непробиваемое] ", ChatColor.COMBAT);
                    oppComponent22.setClickCommand("/boomd cover 5 " + oldcontainer);
                    oppComponent22.setHoverText(new TextComponentString("§9Пример: сталь и металлокаркас толщиной в блок"));
                    message1.addComponent(oppComponent22);

                    oppComponent22 = new MessageComponent("[Высокопрочное] ", ChatColor.YELLOW);
                    oppComponent22.setClickCommand("/boomd cover 4 " + oldcontainer);
                    oppComponent22.setHoverText(new TextComponentString("§9Пример: железобетонная поверхность толщиной в блок"));
                    message1.addComponent(oppComponent22);

                    oppComponent22 = new MessageComponent("[Среднепрочное] ", ChatColor.GLOBAL);
                    oppComponent22.setClickCommand("/boomd cover 3 " + oldcontainer);
                    oppComponent22.setHoverText(new TextComponentString("§9Пример: кирпичная поверхность толщиной в блок"));
                    message1.addComponent(oppComponent22);

                    oppComponent22 = new MessageComponent("[Низкопрочное] ", ChatColor.DARK_AQUA);
                    oppComponent22.setClickCommand("/boomd cover 2 " + oldcontainer);
                    oppComponent22.setHoverText(new TextComponentString("§9Пример: деревянная поверхность толщиной в блок"));
                    message1.addComponent(oppComponent22);

                    oppComponent22 = new MessageComponent("[Ничтожное] ", ChatColor.DARK_GRAY);
                    oppComponent22.setClickCommand("/boomd cover 1 " + oldcontainer);
                    oppComponent22.setHoverText(new TextComponentString("§9Пример: ткань или стекло толщиной в блок"));
                    message1.addComponent(oppComponent22);

                    oppComponent22 = new MessageComponent("[Нет укрытия] ", ChatColor.BLUE);
                    oppComponent22.setClickCommand("/boomd cover 0 " + oldcontainer);
                    message1.addComponent(oppComponent22);

                    Helpers.sendClickContainerToClient(message1, "BoomDefense", (EntityPlayerMP) sender, subordinate);
//                    String json22 = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(message1));
//                    ClickContainerMessage result22 = new ClickContainerMessage("BoomDefense", json22);
//                    KMPacketHandler.INSTANCE.sendTo(result22, (EntityPlayerMP) sender);
                    return;
                case "jumped":
                    Message message = new Message("Итог:\n", ChatColor.COMBAT);
                    MessageComponent oppComponent = new MessageComponent("[Готово] ", ChatColor.BLUE);
                    oppComponent.setClickCommand("/boomd jump");
                    message.addComponent(oppComponent);

                    oppComponent = new MessageComponent("[В безопасности] ", ChatColor.BLUE);
                    oppComponent.setClickCommand("/boomd safej");
                    oppComponent.setHoverText("Выбирайте только если в полной безопасности от взрыва!", TextFormatting.RED);
                    message.addComponent(oppComponent);

                    if (boom.getBoomType() != BoomType.GAS && boom.getBoomType() != BoomType.FIRE && boom.getBoomType() != BoomType.WEAK_GAS) {
                        switch (boom.getCover(player)) {
                            case 1:
                                oppComponent = new MessageComponent("[Ничтожное укрытие] ", ChatColor.DARK_GRAY);
                                break;
                            case 2:
                                oppComponent = new MessageComponent("[Низкопрочное укрытие] ", ChatColor.DARK_AQUA);
                                break;
                            case 3:
                                oppComponent = new MessageComponent("[Среднепрочное укрытие] ", ChatColor.GLOBAL);
                                break;
                            case 4:
                                oppComponent = new MessageComponent("[Высокопрочное укрытие] ", ChatColor.YELLOW);
                                break;
                            case 5:
                                oppComponent = new MessageComponent("[Непробиваемое укрытие] ", ChatColor.COMBAT);
                                break;
                            default:
                                oppComponent = new MessageComponent("[Выбрать укрытие] ", ChatColor.BLUE);
                                break;
                        }
                        oppComponent.setClickCommand("/boomd cover jumped");
                        message.addComponent(oppComponent);
                    }
                    Helpers.sendClickContainerToClient(message, "BoomDefense", (EntityPlayerMP) sender, subordinate);
//                    String json = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(message));
//                    ClickContainerMessage result = new ClickContainerMessage("BoomDefense", json);
//                    KMPacketHandler.INSTANCE.sendTo(result, (EntityPlayerMP) sender);
                    return;
                case "jump":
                    boom.addDefense(player, "jump");
                    def = new Message("§8[ВЗРЫВ] §a" + player.getName() + " ");
                    def2 = new MessageComponent("§4отпрыгнул!");
                    targetsStr = boom.getTargetsString();
                    if (!targetsStr.isEmpty()) def2.setHoverText("§4НЕ ЗАЩИТИЛИСЬ: " + "\n§с[§a" + targetsStr + "§с]" +
                            (boom.isDefendable() ? "" : "\n§4§lНЕЛЬЗЯ ЗАЩИЩАТЬСЯ!"), TextFormatting.RED);
                    if (!targetsStr.isEmpty()) def2.setUnderlined(true);
                    def.addComponent(def2);
                    ServerProxy.sendMessageFromAndInformMasters(player, def);
                    for (Player target : boom.getTargetsList()) {
                        if (DataHolder.inst().isNpc(target.getName())) {
                            Message test = new Message("", ChatColor.BLUE);
                            MessageComponent testing = new MessageComponent("/sub " + target.getName(), ChatColor.BLUE);
                            testing.setUnderlined(true);
                            testing.setClickCommand("/sub " + target.getName());
                            test.addComponent(testing);
                            ServerProxy.sendMessage(DataHolder.inst().getMasterForCombat(combat.getId()), test);
                            break;
                        };
                    }
                    break;
                case "nothing":
                    boom.addDefense(player, "nothing");
                    def = new Message("§8[ВЗРЫВ] §a" + player.getName() + " ");
                    def2 = new MessageComponent("§4остается на месте!");
                    targetsStr = boom.getTargetsString();
                    if (!targetsStr.isEmpty()) def2.setHoverText("§4НЕ ЗАЩИТИЛИСЬ: " + "\n§с[§a" + targetsStr + "§с]" +
                            (boom.isDefendable() ? "" : "\n§4§lНЕЛЬЗЯ ЗАЩИЩАТЬСЯ!"), TextFormatting.RED);
                    if (!targetsStr.isEmpty()) def2.setUnderlined(true);
                    def.addComponent(def2);
                    ServerProxy.sendMessageFromAndInformMasters(player, def);
                    for (Player target : boom.getTargetsList()) {
                        if (DataHolder.inst().isNpc(target.getName())) {
                            Message test = new Message("", ChatColor.BLUE);
                            MessageComponent testing = new MessageComponent("/sub " + target.getName(), ChatColor.BLUE);
                            testing.setUnderlined(true);
                            testing.setClickCommand("/sub " + target.getName());
                            test.addComponent(testing);
                            ServerProxy.sendMessage(DataHolder.inst().getMasterForCombat(combat.getId()), test);
                            break;
                        };
                    }
                    break;
//                case "blocking":
//                    if (args.length < 2) {
//                        Message message2 = new Message("Выберите руку:\n", ChatColor.COMBAT);
//
//                        MessageComponent oppComponent2 = new MessageComponent("[Левой рукой] ", ChatColor.BLUE);
//                        oppComponent2.setClickCommand("/boomd blocking 0");
//                        message2.addComponent(oppComponent2);
//
//                        oppComponent2 = new MessageComponent("[Правой рукой] ", ChatColor.BLUE);
//                        oppComponent2.setClickCommand("/boomd blocking 1");
//                        message2.addComponent(oppComponent2);
//
//                        oppComponent2 = new MessageComponent("[Назад] ", ChatColor.BLUE);
//                        oppComponent2.setClickCommand("/boomd");
//                        message2.addComponent(oppComponent2);
//                        Helpers.sendClickContainerToClient(message2, "BoomDefense", (EntityPlayerMP) sender, subordinate);
////                        String json2 = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(message2));
////                        ClickContainerMessage result2 = new ClickContainerMessage("BoomDefense", json2);
////                        KMPacketHandler.INSTANCE.sendTo(result2, (EntityPlayerMP) sender);
//                        return;
//                    }
//                    boom.addDefense(player, "blocking:"+args[1]);
//                    def = new Message("§8[ВЗРЫВ] §a" + player.getName() + " ");
//                    def2 = new MessageComponent("§4блокирует!");
//                    targetsStr = boom.getTargetsString();
//                    if (!targetsStr.isEmpty()) def2.setHoverText("§4НЕ ЗАЩИТИЛИСЬ: " + "\n§с[§a" + targetsStr + "§с]" +
//                    (boom.isDefendable() ? "" : "\n§4§lНЕЛЬЗЯ ЗАЩИЩАТЬСЯ!"), TextFormatting.RED);
//                    if (!targetsStr.isEmpty()) def2.setUnderlined(true);
//                    def.addComponent(def2);
//                    ServerProxy.sendMessageFromAndInformMasters(player, def);
//                    for (Player target : boom.getTargetsList()) {
//                        if (DataHolder.inst().isNpc(target.getName())) {
//                            Message test = new Message("", ChatColor.BLUE);
//                            MessageComponent testing = new MessageComponent("/sub " + target.getName(), ChatColor.BLUE);
//                            testing.setUnderlined(true);
//                            testing.setClickCommand("/sub " + target.getName());
//                            test.addComponent(testing);
//                            ServerProxy.sendMessage(DataHolder.inst().getMasterForCombat(combat.getId()), test);
//                            break;
//                        };
//                    }
//                    break;
                case "safe":
                    boom.addDefense(player, "safe");
                    def = new Message("§8[ВЗРЫВ] §a" + player.getName() + " ");
                    def2 = new MessageComponent("§4в безопасности!");
                    targetsStr = boom.getTargetsString();
                    if (!targetsStr.isEmpty()) def2.setHoverText("§4НЕ ЗАЩИТИЛИСЬ: " + "\n§с[§a" + targetsStr + "§с]" +
                            (boom.isDefendable() ? "" : "\n§4§lНЕЛЬЗЯ ЗАЩИЩАТЬСЯ!"), TextFormatting.RED);
                    if (!targetsStr.isEmpty()) def2.setUnderlined(true);
                    def.addComponent(def2);
                    ServerProxy.sendMessageFromAndInformMasters(player, def);
                    for (Player target : boom.getTargetsList()) {
                        if (DataHolder.inst().isNpc(target.getName())) {
                            Message test = new Message("", ChatColor.BLUE);
                            MessageComponent testing = new MessageComponent("/sub " + target.getName(), ChatColor.BLUE);
                            testing.setUnderlined(true);
                            testing.setClickCommand("/sub " + target.getName());
                            test.addComponent(testing);
                            ServerProxy.sendMessage(DataHolder.inst().getMasterForCombat(combat.getId()), test);
                            break;
                        };
                    }
                    break;
                case "safej":
                    boom.addDefense(player, "safe");
                    def = new Message("§8[ВЗРЫВ] §a" + player.getName() + " ");
                    def2 = new MessageComponent("§4отпрыгнул в безопасность!");
                    targetsStr = boom.getTargetsString();
                    if (!targetsStr.isEmpty()) def2.setHoverText("§4НЕ ЗАЩИТИЛИСЬ: " + "\n§с[§a" + targetsStr + "§с]" +
                            (boom.isDefendable() ? "" : "\n§4§lНЕЛЬЗЯ ЗАЩИЩАТЬСЯ!"), TextFormatting.RED);
                    if (!targetsStr.isEmpty()) def2.setUnderlined(true);
                    def.addComponent(def2);
                    ServerProxy.sendMessageFromAndInformMasters(player, def);
                    for (Player target : boom.getTargetsList()) {
                        if (DataHolder.inst().isNpc(target.getName())) {
                            Message test = new Message("", ChatColor.BLUE);
                            MessageComponent testing = new MessageComponent("/sub " + target.getName(), ChatColor.BLUE);
                            testing.setUnderlined(true);
                            testing.setClickCommand("/sub " + target.getName());
                            test.addComponent(testing);
                            ServerProxy.sendMessage(DataHolder.inst().getMasterForCombat(combat.getId()), test);
                            break;
                        };
                    }
                    break;
            }
            if (boom.getTargetsList().size() == 0) {
                System.out.println("test");
                boom.doBoom();
            }
        } else {
            Message message = new Message(START_MESSAGE, ChatColor.COMBAT);
            MessageComponent oppComponent;
            if (boom.isDefendable()) {
                if(subordinate.isLying()) {
                    oppComponent = new MessageComponent("[Отпрыгнуть] ", ChatColor.GRAY);
                    oppComponent.setHoverText("Лежачие не могут отпрыгивать! Но можешь отпрыгнуть, если это ошибка.", TextFormatting.RED);
                } else {
                    oppComponent = new MessageComponent("[Отпрыгнуть] ", ChatColor.BLUE);
                }
                oppComponent.setClickCommand("/" + MoveExecutor.NAME + " " + "jumpbomb");
                message.addComponent(oppComponent);

//                if (boom.getBoomType() != BoomType.GAS && boom.getBoomType() != BoomType.WEAK_GAS) {
//                    oppComponent = new MessageComponent("[Блокирование] ", ChatColor.BLUE);
//                    oppComponent.setClickCommand("/boomd blocking");
//                    message.addComponent(oppComponent);
//                }
            }

            oppComponent = new MessageComponent("[Остаться на месте] ", ChatColor.BLUE);
            oppComponent.setClickCommand("/boomd nothing");
            message.addComponent(oppComponent);

            oppComponent = new MessageComponent("[В безопасности] ", ChatColor.BLUE);
            oppComponent.setClickCommand("/boomd safe");
            oppComponent.setHoverText("Выбирайте только если в полной безопасности от взрыва!", TextFormatting.RED);
            message.addComponent(oppComponent);

            if (boom.getBoomType() != BoomType.GAS && boom.getBoomType() != BoomType.FIRE && boom.getBoomType() != BoomType.WEAK_GAS) {
                switch (boom.getCover(player)) {
                    case 1:
                        oppComponent = new MessageComponent("[Ничтожное укрытие] ", ChatColor.GRAY);
                        break;
                    case 2:
                        oppComponent = new MessageComponent("[Низкопрочное укрытие] ", ChatColor.DARK_AQUA);
                        break;
                    case 3:
                        oppComponent = new MessageComponent("[Среднепрочное укрытие] ", ChatColor.GLOBAL);
                        break;
                    case 4:
                        oppComponent = new MessageComponent("[Высокопрочное укрытие] ", ChatColor.YELLOW);
                        break;
                    case 5:
                        oppComponent = new MessageComponent("[Непробиваемое укрытие] ", ChatColor.COMBAT);
                        break;
                    default:
                        oppComponent = new MessageComponent("[Выбрать укрытие] ", ChatColor.BLUE);
                        break;
                }
                oppComponent.setClickCommand("/boomd cover");
                message.addComponent(oppComponent);
            }

            oppComponent = new MessageComponent("[Мод] ", ChatColor.GRAY);
            oppComponent.setClickCommand("/modifiersbutton BoomDefense");
            message.addComponent(oppComponent);

            Helpers.sendClickContainerToClient(message, "BoomDefense", (EntityPlayerMP) sender, subordinate);
//            String json = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(message));
//            ClickContainerMessage result = new ClickContainerMessage("BoomDefense", json);
//            KMPacketHandler.INSTANCE.sendTo(result, (EntityPlayerMP) sender);
        }

    }
}
