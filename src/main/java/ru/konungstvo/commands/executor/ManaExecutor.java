package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.bridge.discord.DiscordBridge;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.exceptions.PermissionException;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

public class ManaExecutor extends CommandBase {


    @Override
    public String getName() {
        return "mana";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        DataHolder dh = DataHolder.inst();
        if (args.length < 1) {
            player.sendMessage(new Message("§bУровень маны: " + dh.getMana(player.getName()) + "/" + dh.getMaxMana(player.getName())));
            return;
        } else if (args[0].equals("help")) {
            player.sendMessage(new Message("§e/mana проверяет количество, /mana use [число] тратит столько, если есть."));
            return;
        } else if (args[0].equals("use")) {
            try {
                int used = 1;
                if (args.length > 1 && Integer.parseInt(args[1]) > 0) {
                    used = Integer.parseInt(args[1]);
                }
                if (dh.useMana(player.getName(), used)) {
                    player.sendMessage(new Message("§bИспользовано §4" + used + "§b ед. маны. Уровень маны: §9" + dh.getMana(player.getName()) + "/" + dh.getMaxMana(player.getName())));
                    ServerProxy.informMasters(new Message("§a" + player.getName() + " §bиспользует " + used + "§b ед. маны. Уровень маны: §9" + dh.getMana(player.getName()) + "/" + dh.getMaxMana(player.getName()), ChatColor.DARK_GREEN), player);
                } else {
                    player.sendMessage(new Message("§4Недостаточно маны. §bУровень маны §a" + player.getName() + "§b: §9" + dh.getMana(player.getName()) + "/" + dh.getMaxMana(player.getName())));
                }
                return;
            } catch (Exception e) {
                return;
            }
        }
        if (!player.hasPermission(Permission.GM)) {
            throw new PermissionException("1", "Недостаточно прав!");
        }
        String playername = args[0];
        if (args.length < 2) {
            player.sendMessage(new Message("§bУровень маны §a" + playername + "§b: §9" + dh.getMana(playername) + "/" + dh.getMaxMana(playername)));
            return;
        } else if (args[1].equals("use")) {
            try {
                int used = 1;
                if (args.length > 2 && Integer.parseInt(args[2]) > 0) {
                    used = Integer.parseInt(args[2]);
                }
                if (dh.useMana(playername, used)) {
                    player.sendMessage(new Message("§bИспользовано §4" + used + "§b ед. маны у игрока §a" + playername + "§b. Уровень маны: §9" + dh.getMana(playername) + "/" + dh.getMaxMana(playername)));
                } else {
                    player.sendMessage(new Message("§4Недостаточно маны. §bУровень маны §a" + playername + "§b: §9" + dh.getMana(playername) + "/" + dh.getMaxMana(playername)));
                }
                return;
            } catch (Exception e) {
                return;
            }
        } else if (args[1].equals("add")) {
            try {
                int added = 1;
                if (args.length > 2 && Integer.parseInt(args[2]) > 0) {
                    added = Integer.parseInt(args[2]);
                }
                if (dh.addMana(playername, added)) {
                    player.sendMessage(new Message("§bДобавлено §2" + added + "§b ед. маны игроку §a" + playername + "§b. Уровень маны: §9" + dh.getMana(playername) + "/" + dh.getMaxMana(playername)));
                } else {
                    player.sendMessage(new Message("§4Ошибка."));
                }
                return;
            } catch (Exception e) {
                return;
            }
        } else if (args[1].equals("setmax")) {
            try {
                if (args.length > 2 && Integer.parseInt(args[2]) > 0) {
                    int max = Integer.parseInt(args[2]);
                    dh.setMaxMana(playername, max);
                    player.sendMessage(new Message("§bМаксимум маны §a" + playername + "§b теперь §2" + max + "§b. Уровень маны: §9" + dh.getMana(playername) + "/" + dh.getMaxMana(playername)));
                } else {
                    player.sendMessage(new Message("§4Нужно число."));
                }
                return;
            } catch (Exception e) {
                return;
            }
        }

    }
}
