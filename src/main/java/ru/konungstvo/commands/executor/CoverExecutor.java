package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.CombatState;
import ru.konungstvo.combat.duel.Duel;
import ru.konungstvo.combat.equipment.Shield;
import ru.konungstvo.combat.equipment.Weapon;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.player.turn.ToRoot;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

import java.util.List;

public class CoverExecutor extends CommandBase {
    private static String START_MESSAGE = "Выбор укрытия:\n";
    public static final String NAME = "setcover";

    @Override
    public String getName() {
        return "setcover";
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        //String modifierType = args[0];
        String oldContainer = "";
        String oldoldContainer = "";
        Player target = null;
        int cover = -666;
        if (args.length > 0 && !args[0].equals("%IGNORE%")) {
            cover = parseInt(args[0]);
        }
        if (args.length > 1) {
            oldContainer = args[1];
        }

        if (args.length > 2) {
            oldoldContainer = args[2];
        }

        if (!oldContainer.isEmpty()) {
            ClickContainerMessage remove = new ClickContainerMessage(oldContainer, true);
            KMPacketHandler.INSTANCE.sendTo(remove, getCommandSenderAsPlayer(sender));
        }


        Player player = DataHolder.inst().getPlayer(sender.getName());
        Player subordinate = DataHolder.inst().getPlayer(sender.getName());
        if (subordinate.getSubordinate() != null) subordinate = subordinate.getSubordinate();

        if (!oldContainer.isEmpty()) oldContainer = " " + oldContainer;
        if (!oldoldContainer.isEmpty()) oldoldContainer = " " + oldoldContainer;

        if (cover != -666) {
            Duel duel = DataHolder.inst().getDuelForDefender(subordinate.getName());
            if (duel == null) {
                player.sendMessage("Нет дуэлей.");
                return;
            }
            duel.setFullcover(cover);
            switch (cover) {
                case 999:
                    ServerProxy.sendMessageFromAndInformMasters(subordinate, new Message(subordinate.getName() + " заявляет, что находится в безопасности!", ChatColor.RED));
                    break;
                case 5:
                    ServerProxy.sendMessageFromAndInformMasters(subordinate, new Message(subordinate.getName() + " заявляет, что находится за непробиваемым укрытием!", ChatColor.RED));
                    break;
                case 4:
                    ServerProxy.sendMessageFromAndInformMasters(subordinate, new Message(subordinate.getName() + " заявляет, что находится за высокопрочным укрытием!", ChatColor.RED));
                    break;
                case 3:
                    ServerProxy.sendMessageFromAndInformMasters(subordinate, new Message(subordinate.getName() + " заявляет, что находится за среднепрочным укрытием!", ChatColor.RED));
                    break;
                case 2:
                    ServerProxy.sendMessageFromAndInformMasters(subordinate, new Message(subordinate.getName() + " заявляет, что находится за низкопрочным укрытием!", ChatColor.RED));
                    break;
                case 1:
                    ServerProxy.sendMessageFromAndInformMasters(subordinate, new Message(subordinate.getName() + " заявляет, что находится за ничтожным укрытием!", ChatColor.RED));
                    break;
                case 0:
                    ServerProxy.sendMessageFromAndInformMasters(subordinate, new Message(subordinate.getName() + " заявляет, что не находится в укрытии!", ChatColor.RED));
                    break;
            }
            player.performCommand("/" + ToRoot.NAME + " Cover"  + oldContainer + oldoldContainer);
            return;
        }

        Message message = new Message(START_MESSAGE, ChatColor.COMBAT);
        MessageComponent oppComponent22 = new MessageComponent("[В безопасности] ", ChatColor.COMBAT);
        oppComponent22.setClickCommand("/setcover 999 " + oldContainer + oldoldContainer);
        oppComponent22.setHoverText(new TextComponentString("§6§lЗаявить, что вы недосягаемы для противника"));
        message.addComponent(oppComponent22);
        oppComponent22 = new MessageComponent("[Непробиваемое] ", ChatColor.COMBAT);
        oppComponent22.setClickCommand("/setcover 5 " + oldContainer + oldoldContainer);
        oppComponent22.setHoverText(new TextComponentString("§9Пример: сталь и металлокаркас толщиной в блок\n§4§lНе пробивается огнестрельным оружием в обычной ситуации, выбирайте \"в безопасности\""));
        message.addComponent(oppComponent22);

        oppComponent22 = new MessageComponent("[Высокопрочное] ", ChatColor.YELLOW);
        oppComponent22.setClickCommand("/setcover 4 " + oldContainer + oldoldContainer);
        oppComponent22.setHoverText(new TextComponentString("§9Пример: железобетонная поверхность толщиной в блок\n§4§lНе пробивается огнестрельным оружием в обычной ситуации, выбирайте \"в безопасности\""));
        message.addComponent(oppComponent22);

        oppComponent22 = new MessageComponent("[Среднепрочное] ", ChatColor.GLOBAL);
        oppComponent22.setClickCommand("/setcover 3 " + oldContainer + oldoldContainer);
        oppComponent22.setHoverText(new TextComponentString("§9Пример: кирпичная поверхность толщиной в блок\n§4§lНе пробивается огнестрельным оружием в обычной ситуации, выбирайте \"в безопасности\""));
        message.addComponent(oppComponent22);

        oppComponent22 = new MessageComponent("[Низкопрочное] ", ChatColor.DARK_AQUA);
        oppComponent22.setClickCommand("/setcover 2 " + oldContainer + oldoldContainer);
        oppComponent22.setHoverText(new TextComponentString("§9Пример: деревянная поверхность толщиной в блок"));
        message.addComponent(oppComponent22);

        oppComponent22 = new MessageComponent("[Ничтожное] ", ChatColor.DARK_GRAY);
        oppComponent22.setClickCommand("/setcover 1 " + oldContainer + oldoldContainer);
        oppComponent22.setHoverText(new TextComponentString("§9Пример: ткань или стекло толщиной в блок"));
        message.addComponent(oppComponent22);

        oppComponent22 = new MessageComponent("[Нет укрытия] ", ChatColor.BLUE);
        oppComponent22.setClickCommand("/setcover 0 " + oldContainer + oldoldContainer);
        message.addComponent(oppComponent22);
        MessageComponent toRoot = new MessageComponent("[Назад]", ChatColor.GRAY);

        toRoot.setClickCommand("/" + ToRoot.NAME + " Cover" + oldContainer + oldoldContainer);
        message.addComponent(toRoot);

        // SEND PACKET TO CLIENT
//        String json = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(message));
//        ClickContainerMessage result = new ClickContainerMessage("Cover", json);
//        KMPacketHandler.INSTANCE.sendTo(result, (EntityPlayerMP) sender);
        Helpers.sendClickContainerToClient(message, "Cover", (EntityPlayerMP) sender, subordinate);
    }
}
