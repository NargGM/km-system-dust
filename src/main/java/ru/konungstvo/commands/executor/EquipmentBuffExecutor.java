package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.exceptions.PermissionException;
import ru.konungstvo.kmrp_lore.helpers.EquipmentBuffTagHandler;
import ru.konungstvo.kmrp_lore.helpers.SubcompEq;
import ru.konungstvo.kmrp_lore.helpers.WeightHandler;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

public class EquipmentBuffExecutor extends CommandBase {


    @Override
    public String getName() {
        return "equipmentbuff";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        DataHolder dh = DataHolder.inst();
        if (!player.hasPermission(Permission.GM) && !player.hasPermission(Permission.CM)) {
            throw new PermissionException("1", "Недостаточно прав!");
        }

        switch (args[0]) {
            case "set":
                if (args.length < 4) return;
                switch (args[1]) {
                    case "common":
                        ItemStack item11 = ((EntityPlayerMP) sender).getHeldItemMainhand();
                        EquipmentBuffTagHandler eb = new EquipmentBuffTagHandler(item11);
                        eb.setBuff(SubcompEq.SUBCOMPOUND_COMMON, args[2], Integer.parseInt(args[3]));
                        break;
                    case "equipment":
                        ItemStack item12 = ((EntityPlayerMP) sender).getHeldItemMainhand();
                        EquipmentBuffTagHandler eb1 = new EquipmentBuffTagHandler(item12);
                        eb1.setBuff(SubcompEq.SUBCOMPOUND_EQUIPMENT, args[2], Integer.parseInt(args[3]));
                        break;
                    case "inventory":
                        ItemStack item13 = ((EntityPlayerMP) sender).getHeldItemMainhand();
                        EquipmentBuffTagHandler eb2 = new EquipmentBuffTagHandler(item13);
                        eb2.setBuff(SubcompEq.SUBCOMPOUND_INVENTORY, args[2], Integer.parseInt(args[3]));
                        break;
                }
                return;
            case "remove":
                ItemStack item1 = ((EntityPlayerMP) sender).getHeldItemMainhand();
                EquipmentBuffTagHandler wh1 = new EquipmentBuffTagHandler(item1);
                wh1.removeEbTag();
                return;
        }
    }
}
