package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.bridge.discord.DiscordBridge;
import ru.konungstvo.chat.message.Message;

public class DiscordNotifySilentExecutor extends CommandBase {


    @Override
    public String getName() {
        return "dmsggms";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
//
//        Player player = DataHolder.inst().getPlayer(sender.getName());
        if (args.length < 1) {
//            player.sendMessage(new Message("Нельзя отправить пустое сообщение."));
            return;
        }

        String name = sender.getName();
        if (name.isEmpty()) name = "**SERVER**";

        if (args[0].equals("help")) {
//            player.sendMessage(new Message("§e/dmsggm Сообщение"));
            return;
        } else {
            StringBuilder result = new StringBuilder();
            for (String arg : args) {
                result.append(" ").append(arg);
            }

            String resultStr = result.toString().trim();
            String gamemsg = "§2[Discord] [" + name + "] §7[" + resultStr + "§7]";
//                if(!ServerProxy.hasPermission(player.getName(), "km.gm"))
//                    player.sendMessage(new Message(gamemsg));
            ServerProxy.sendToAllMasters(new Message(gamemsg));
//            if(DiscordBridge.sendMessageToPlayer(player.getName(), reciever, resultStr)) {
//                String gamemsg = "§2[Discord] [" + name + "] §6[" + resultStr + "§6]";
//                if(!ServerProxy.hasPermission(player.getName(), "km.gm"))
//                    player.sendMessage(new Message(gamemsg));
//                ServerProxy.informMasters(new Message(gamemsg));
                String discmsg = "[Discord] [" + name + "] [" + resultStr + "]";
                DiscordBridge.sendMessage(discmsg);
            }
        }

}

