package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.chat.range.RangeFactory;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

import java.util.ArrayList;
import java.util.List;

public class AutoDefenseExecutor extends CommandBase {


    @Override
    public String getName() {
        return "autodefense";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public List<String> getAliases() {
        ArrayList<String> result = new ArrayList<String>();
        result.add("autodefence");
        return result;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permission.GM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        Message result = execute(player, args);
        player.sendMessage(result);

    }

    public static Message execute(Player executor, String... args) {
        if (args.length < 1) {
            if (executor.getAutoDefense() == null) {
                return new Message("Автозащита не установлена", ChatColor.GRAY);
            }
            return new Message("Автозащита: " + executor.getAutoDefense().getName() + " в рейндж " + executor.getAutoDefenseRange().toString() + ".", ChatColor.GRAY);
        }

        if (args[0].equals("delete") || args[0].equals("remove") || args[0].equals("off")) {
            executor.setAutoDefense(null);
            return new Message("Автоматическая защита отключена.", ChatColor.GRAY);
        }

        Range range = RangeFactory.build(args[0]);
        String skill = executor.getSkillNameFromContext(args[0].substring(range.getNumberOfSymbolsToStrip()));
        executor.setAutoDefense(skill);
        executor.setAutoDefenseRange(range);
        return new Message("Навык \"" + skill + "\" установлен автоматической защитой с рейнджем " + range.toString() + ".", ChatColor.GRAY);

    }
}
