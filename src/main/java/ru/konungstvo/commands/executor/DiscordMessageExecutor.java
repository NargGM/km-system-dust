package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.bridge.discord.DiscordBridge;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

public class DiscordMessageExecutor extends CommandBase {


    @Override
    public String getName() {
        return "dmsg";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player player = DataHolder.inst().getPlayer(sender.getName());
        if (args.length < 1 || args[0].equals("help")) {
            player.sendMessage(new Message("§e/dmsg Ник_получателя Сообщение"));
            return;
        } else {
            String reciever = args[0];

            if (args.length < 2) {
                player.sendMessage(new Message("Нельзя отправить пустое сообщение."));
                return;
            }

            StringBuilder result = new StringBuilder();
            for (int i = 1; i < args.length; i++) {
                result.append(" ").append(args[i]);
            }

            String resultStr = result.toString().trim();
            if(DiscordBridge.sendMessageToPlayer(player.getName(), reciever, resultStr)) {
                String gamemsg = "§2[Discord] §8[§a" + player.getName() + "§8->§2" + reciever + "§8]:§f " + resultStr;
                if(!ServerProxy.hasPermission(player.getName(), "km.gm"))
                    player.sendMessage(new Message(gamemsg));
                ServerProxy.sendToAllMasters(new Message(gamemsg));
                String discmsg = "[Discord] [" + player.getName() + "->" + reciever + "]: " + resultStr;
                DiscordBridge.sendMessage(discmsg);
            } else {
                player.sendMessage(new Message("§4Сообщение не отправлено. Вероятно, у игрока не привязан discord."));
            }
        }

    }
}
