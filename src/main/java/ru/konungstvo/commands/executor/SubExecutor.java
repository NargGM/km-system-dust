package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.server.permission.PermissionAPI;
import noppes.npcs.entity.EntityNPCInterface;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.combat.Combat;
import ru.konungstvo.combat.equipment.Weapon;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.player.defense.SelectDefenseCommand;
import ru.konungstvo.commands.helpercommands.player.turn.ToolPanel;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

import javax.xml.crypto.Data;

public class SubExecutor extends CommandBase {
    public static final String NAME = "sub";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permission.GM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        Player gm = DataHolder.inst().getPlayer(sender.getName());
        if (args.length > 1 && args[1].equals("tp")) {
            EntityNPCInterface sub = DataHolder.inst().getNpcEntity(args[0]);

            double posX = Math.floor(sub.posX) + 0.5;
            double posY = Math.floor(sub.posY);
            double posZ = Math.floor(sub.posZ) + 0.5;


            if (ServerProxy.getForgePlayer(gm.getName()).attemptTeleport(posX, posY, posZ)) return; //gm.sendMessage("§7Телепортируемся к " + args[0] + "...");
            return;
        } else if (args.length > 1 && args[1].equals("clear")) {
            gm.removeSubordinate();
            return;
        }
        gm.setSubordinate(args[0]);
        if (gm.getSubordinate() != null) {
            Player sub = gm.getSubordinate();
            Combat combat = DataHolder.inst().getCombatForPlayer(sub.getName());
            if (combat != null) {
                if (DataHolder.inst().getDuelForDefender(sub.getName()) != null) {
                    gm.performCommand("/" + SelectDefenseCommand.NAME);
                    return;
                }
                if (combat.getBoomForPlayer(sub) != null) {
                    gm.performCommand("/" + BoomDefenseExecutor.NAME);
                    return;
                }
            }
        }
        gm.performCommand("/" + ToolPanel.NAME);
        if (gm.getSubordinate() != null) {
            EntityLivingBase forgePlayer = gm.getSubordinate().getEntity();
            WeaponTagsHandler weaponTagsHandler = null;
            Weapon weapon = null;
            ItemStack activeItem = null;
            activeItem = forgePlayer.getHeldItemMainhand();
            if (activeItem == null || activeItem.isEmpty()) return;
            weaponTagsHandler = new WeaponTagsHandler(activeItem);
            System.out.println("test2");
            if (!weaponTagsHandler.isWeapon()) return;
            if (!weaponTagsHandler.isFirearm()) return;
            if (weaponTagsHandler.isWeapon() && weaponTagsHandler.isFirearm()) {
                weapon = new Weapon(weaponTagsHandler.getDefaultWeaponName(), weaponTagsHandler.getDefaultWeapon(), weaponTagsHandler);
                if (!weapon.isLoaded()) gm.sendMessage(new Message("§4§lВНИМАНИЕ! ОРУЖИЕ " + gm.getSubordinate().getName() + " РАЗРЯЖЕНО!"));
            }
        }
    }

}
