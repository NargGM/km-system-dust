package ru.konungstvo.commands.executor;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.kmrp_lore.helpers.Permissions;
import ru.konungstvo.kmrp_lore.helpers.WeaponDurabilityHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.kmrp_lore.helpers.WeightHandler;
import ru.konungstvo.player.Player;

import java.lang.management.PlatformLoggingMXBean;
import java.util.Arrays;
import java.util.List;

public class VectorTestCommand extends CommandBase {
    public static final String NAME = "vectortest";

    @Override
    public String getName() { return NAME; }

    @Override
    public String getUsage(ICommandSender sender) { return null; }

    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permissions.GM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }


    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) {
            //
        }
        TextComponentString result;
        EntityPlayerMP player = (EntityPlayerMP) sender;
        EntityPlayerMP target = ServerProxy.getForgePlayer(args[0]);

        Vec3d vector = target.getPositionVector().subtract(player.getPositionVector());
        Vec3d dir = player.getLookVec();

        double angleDir = (Math.atan2(vector.z, vector.x) / 2 / Math.PI * 360 + 360) % 360;
// This is probably related to yaw somehow but whatever
        double angleLook = (Math.atan2(dir.z, dir.x) / 2 / Math.PI * 360 + 360) % 360;

        double angle1 = (angleDir - angleLook + 360) % 360;

        result = new TextComponentString(angle1+ "");
        result.getStyle().setColor(TextFormatting.GRAY);
        sender.sendMessage(result);

    }
}
