package ru.konungstvo.commands.debug;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.server.permission.PermissionAPI;
import ru.konungstvo.combat.duel.Duel;
import ru.konungstvo.combat.duel.DuelMaster;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Permission;

import java.util.Iterator;

public class DuelDebug extends CommandBase {

    @Override
    public String getName() {
        return "dueldebug";
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return "todo";
    }


    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        try {
            return PermissionAPI.hasPermission(getCommandSenderAsPlayer(sender), Permission.GM.get());
        } catch (PlayerNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }


    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        String arg = "";
        if (args.length > 0) {
            arg = args[0];
        }

        switch (arg) {
            case "":
            case "show":
                String result = "";
                for (Duel duel : DataHolder.inst().getDuels()) {
                    result += duel.toString() + "\n";
                }
                if (result.equals("")) result = "Сейчас нет дуэлей.";
                TextComponentString tcs = new TextComponentString(result);
                tcs.getStyle().setColor(TextFormatting.GRAY);
                sender.sendMessage(tcs);
                return;
            case "remove":
            case "delete":
                String playerName = args[1];
                DuelMaster.end(DataHolder.inst().getDuelForAttacker(playerName));
                tcs = new TextComponentString("Дуэль для игрока " + playerName + " удалена.");
                tcs.getStyle().setColor(TextFormatting.GRAY);
                sender.sendMessage(tcs);
                return;
            case "killall":
                String result1 = "";
                Iterator iterator = DataHolder.inst().getDuels().iterator();
                while (iterator.hasNext())
                {
                    Duel duel = (Duel) iterator.next();
                    result1 += "Дуэль " + duel.toString() + " удалена.\n";
                    DuelMaster.end(duel);
                }
                if (result1.equals("")) result1 = "Сейчас нет дуэлей.";
                tcs = new TextComponentString(result1);
                tcs.getStyle().setColor(TextFormatting.GRAY);
                sender.sendMessage(tcs);
                return;

        }


    }
}
