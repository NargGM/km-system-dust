package ru.konungstvo.bridge.discord;

import javafx.concurrent.Task;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.*;

import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;
import net.dv8tion.jda.api.entities.emoji.Emoji;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;


import net.dv8tion.jda.api.events.session.ReadyEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.minecraft.command.ServerCommandManager;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.fml.common.FMLCommonHandler;
import org.jetbrains.annotations.NotNull;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Logger;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

import javax.annotation.Nonnull;
import javax.security.auth.login.LoginException;
import javax.xml.crypto.Data;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.regex.Pattern;


/**
 * Main class for Discord integration, creates connection and uses it to send messages.
 */
public class DiscordBridge extends ListenerAdapter {
    private static long channel_id;
    private static TextChannel channel;
    private static JDA jda;
    private static Logger logger = new Logger("Discord");

    public static void create(String token, String ch_id) throws LoginException, InterruptedException {
        channel_id = Long.parseLong(ch_id);
        //mewtchannel_id = Long.parseLong("811234853479645214");//для мяута

        logger.debug("Registering discord bot with channel_id " + channel_id);

        jda = JDABuilder.createDefault(token).addEventListeners(new DiscordBridge())
                .setActivity(Activity.playing("DustRP")).enableIntents(GatewayIntent.GUILD_MESSAGES)
                .enableIntents(GatewayIntent.DIRECT_MESSAGES).enableIntents(GatewayIntent.MESSAGE_CONTENT).build().awaitReady();

    }

    @Override
    public void onReady(ReadyEvent event) {
        channel = jda.getTextChannelById(channel_id);
        System.out.println(channel);
        //channel.sendMessage("test");
    }

//    @Override
//    public void PrivateMessageReceivedEvent(PrivateMessageReceivedEvent event) {
//        Message msg = event.getMessage();
//        String content = msg.getContentRaw();
//        if (event.getAuthor().isBot()) return;
//        if (content.startsWith("!r") || content.startsWith("!о")) {
//            event.getChannel().getHistory().retrievePast(20).queue(messages -> {
//                if (messages.size() > 0)
//                    for (Message message : messages) {
//                        if (message.getAuthor().isBot()) {
//                            String cont = message.getContentDisplay();
//                            if (cont.contains("сообщение") || cont.contains("зовёт")) {
//                                String[] conts = cont.split(" ");
//                                String sender = conts[2].replace("*", "");
//                                String receiver = conts[0].replace("*", "");
//                                ;
//                                String strippedContent = content.substring(3);
//
//                                final String gamemsg = "§2[Discord] §8[§2" + sender + "§8->§a" + receiver + "§8]:§f " + strippedContent;
//                                final String discmsg = "[Discord] [" + sender + "->" + receiver + "]: " + strippedContent;
//                                if (!DataHolder.inst().getPlayer(receiver).hasPermission(Permission.GM)) {
//                                    ServerProxy.sendMessageTo(receiver, new ru.konungstvo.chat.message.Message(gamemsg));
//                                }
//                                ServerProxy.sendToAllMasters(new ru.konungstvo.chat.message.Message(gamemsg));
//                                sendMessage(discmsg);
//                                event.getMessage().addReaction("\u2705").queue();
//                                break;
//                            }
//                        }
//                    }
//
//            });
//        }
////      else if (content.startsWith("!msg")) {
////            String name = content.split(" ")[1];
////            if (FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUsername(name) != null && DataHolder.inst().getPlayer(name) != null) {
////                int counter = 0;
////                StringBuilder strippedContent = new StringBuilder();
////                String strippedContent2 = "";
////                ArrayList<String> contentL = new ArrayList<>();
////                for (String word : content.split(" ")) {
////                    counter++;
////                    if (counter < 3) continue;
////                    strippedContent.append(word).append(" ");
////                }
////                strippedContent2 = strippedContent.toString().trim();
////                if (!strippedContent2.isEmpty()) {
////                    final String gamemsg = "§2[Discord] §8[§2" + sender + "§8->§a" + receiver + "§8]:§f " + strippedContent;
////                    final String discmsg = "[Discord] [" + sender + "->" + receiver + "]: " + strippedContent;
////                    if (!DataHolder.inst().getPlayer(receiver).hasPermission(Permission.GM)) {
////                        ServerProxy.sendMessageTo(receiver, new ru.konungstvo.chat.message.Message(gamemsg));
////                    }
////                }
////            }
////        }
//    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {

        if (!event.getChannel().getName().equals("logs")) { // HARDCODED CHANNEL NAME!!!
            return;
        }

        if (event.getChannel().getIdLong() != channel_id) {
            return;
        }
        if (event.getAuthor().equals(jda.getSelfUser())) {
            return;
        }

        Message msg = event.getMessage();
        String content = msg.getContentRaw();
        System.out.println("test:::" + msg);
        System.out.println("test:::" + msg.getContentStripped());
        System.out.println("test:::" + jda.getGatewayIntents());
        System.out.println("test:::" + content);
        if (!content.startsWith("!") && !content.startsWith("^")) {
            ServerProxy.sendToAllMastersIgnoreDiscord(new ru.konungstvo.chat.message.Message("[" + event.getAuthor().getName() + "(to GM)] " + content, ChatColor.DARK_GREEN));
            event.getMessage().addReaction(Emoji.fromUnicode("\u2705")).queue();
            return;
        }

        if (content.equals("!ping")) {
            MessageChannel channel = event.getChannel();
            long time = System.currentTimeMillis();
            channel.sendMessage("Pong!") /* => RestAction<Message> */
                    .queue(response /* => Message */ -> {
                        response.editMessageFormat("Pong: %d ms", System.currentTimeMillis() - time).queue();
                    });
            return;
        } else if (content.startsWith("!exec")) {
            for (Player pl : DataHolder.inst().getPlayerList()) {
                if (content.startsWith("!exec " + pl.getName())) {
                    String command = content.substring(6 + pl.getName().length() + 1);
                    pl.performCommand("/" + command);
                    sendMessage(pl.getName() + " выполнил команду \"" + command + '\"');
                    return;
                }
            }
            String command = content.substring(6);
            MinecraftServer cm = FMLCommonHandler.instance().getMinecraftServerInstance();
            cm.getCommandManager().executeCommand(cm, "/" + command);
            sendMessage("Не найден игрок, попытка исполнить команду от лица сервера");
            return;
        /*
        String custom_exec_res = null;
        for (ru.konungstvo.player.Player pl : DataHolder.getInstance().getPlayerList()) {
            if (content.startsWith("!exec " + pl.getName())) {
                String command = content.substring(6 + pl.getName().length() + 1);
                logger.debug(pl.getName() + " was made to execute command \"" + command + "\" by " + event.getAuthor().getName());
                if (pl.forgeExecute(command)) {
                    custom_exec_res = pl.getName() + " выполнил команду \"" + command + '\"';
                } else {
                    custom_exec_res = "ERROR: " + pl.getName() + " не смог выполнить команду " + command;
                }
                BotUtils.sendMessage(event.getChannel(), custom_exec_res);
                return;
            }

        }

        if (content.startsWith("!exec msg")) {
            BotUtils.sendMessage(event.getChannel(), "Так не получится. Пользуйтесь !msg {player} {message}");
        } else {
            String mes = "";
            try {
                content.substring(6);
            } catch (Exception e) {
                return;
            }
            Bukkit.getServer().dispatchCommand(new DiscordCommandSender(event.getChannel()), event.getMessage().getContent().substring(6));
        }

         */

        } else if (content.startsWith("!msg")) {
            String strippedContent = content.substring(5);
            String receiver = strippedContent.substring(0, strippedContent.indexOf(" "));

            ru.konungstvo.chat.message.Message message = new ru.konungstvo.chat.message.Message();
            message.addComponent(new MessageComponent("<"));
            message.addComponent(new MessageComponent(event.getAuthor().getName(), ChatColor.DARK_GREEN));
            message.addComponent(new MessageComponent("->"));
            message.addComponent(new MessageComponent(receiver, ChatColor.NICK));
            message.addComponent(new MessageComponent(">"));
            message.addComponent(new MessageComponent(strippedContent.substring(receiver.length())));


            //    message = "<§2" + event.getAuthor().getName() + "§f->§a" + receiver + "§f>" + message.substring(receiver.length());
            if (!DataHolder.inst().getPlayer(receiver).hasPermission(Permission.GM)) {
                ServerProxy.sendMessageTo(receiver, message);
            }
            ServerProxy.sendToAllMastersIgnoreDiscord(message);
            sendMessage(message.toString());
            return;


        } else if (content.startsWith("!online") || content.startsWith("!онлайн")) {
            String res;
            StringBuilder online = new StringBuilder("Текущий онлайн (%s): ");
            int i = 0;
            for (String player : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getOnlinePlayerNames()) {
                i++;
                if (DataHolder.inst().getPlayer(player) != null && DataHolder.inst().getPlayer(player).hasPermission(Permission.GM))
                    online.append("**").append(player).append("**");
                else
                    online.append(player);
                if (i == FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getOnlinePlayerNames().length)
                    online.append(".");
                else
                    online.append(", ");
            }
            if (i == 0)
                res = "Никого онлайн!";
            else
                res = String.format(online.toString(), i);
            sendMessage(res);
            return;

        } else if (content.startsWith("!rad")) {
            try {
                String name = content.split(" ")[1].replace("_", " ");
                String channel = content.split(" ")[2];
                int dimension = 6666;
                if (isNumeric(content.split(" ")[3])) dimension = Integer.parseInt(content.split(" ")[3].replace("#",""));
                int i = 0;
                StringBuilder mestsage = new StringBuilder();
                for (String word : content.split(" ")) {
                    i++;
                    if (i < 5 && dimension != 6666 || i < 4) continue;
                    mestsage.append(word).append(" ");
                }
                String rad = mestsage.toString().trim();
                Range range = Range.NORMAL;
                if (rad.startsWith("!!!")) {
                    range = Range.SCREAM;
                    rad = rad.substring(3);
                } else if (rad.startsWith("!!")) {
                    range = Range.SHOUT;
                    rad = rad.substring(2);
                } else if (rad.startsWith("!")) {
                    range = Range.LOUD;
                    rad = rad.substring(1);
                } else if (rad.startsWith("===")) {
                    range = Range.HUSHED;
                    rad = rad.substring(3);
                } else if (rad.startsWith("==")) {
                    range = Range.WHISPER;
                    rad = rad.substring(2);
                } else if (rad.startsWith("=")) {
                    range = Range.QUIET;
                    rad = rad.substring(1);
                }
                boolean ooc = false;
                if (rad.startsWith("((") && rad.endsWith("))")) {
                    ooc = true;
                    rad = rad.substring(2, rad.length() - 2);
                } else if (rad.startsWith("_")) {
                    ooc = true;
                    rad = rad.substring(1);
                }
                ServerProxy.sendRadioFromDiscordMessage(name, channel, rad, range, ooc, dimension);
            } catch (Exception e) {
                sendMessage(String.valueOf(e));
            }
            return;

        } else if (content.startsWith("!help")) {
            sendMessage(
            "Все сообщения в этот канал транслируются в ГМ-чат игры.\n" +
                    "Чтобы отправить в игру глобальное сообщение, поставьте в начале символ ^.\n\n" +
                    "Все команды начинаются с символа !\n" +
                    //"!exec {command} — выполнить команду в игре;\n" +
                    "!online либо !онлайн — показать текущий онлайн\n" +
                    "!msg {ник} {сообщение} — написать игроку в лс\n" +
                    "!exec {ник} {команда} — выполнить команду от лица игрока\n" +
                    "!exec {команда} — выполнить команду от лица сервера\n" +
                    "!rad {имя} {частота} {id мира} {сообщение} — отправить радиосообщение в игру\n" +
                    "!help — вывести эту справку.");

        } else {
            ru.konungstvo.chat.message.Message res = new ru.konungstvo.chat.message.Message();
            if (content.startsWith("^")) {
                res.addComponent(new MessageComponent("<", ChatColor.GLOBAL));
                res.addComponent(new MessageComponent(event.getAuthor().getName(), ChatColor.DARK_GREEN));
                res.addComponent(new MessageComponent("> (( " + content.substring(1) + " ))", ChatColor.GLOBAL));
                for (Player player : DataHolder.inst().getPlayerList()) {
                    player.sendMessage(res);
                }
                event.getMessage().addReaction(Emoji.fromUnicode("\uD83D\uDCE3")).queue();

            }
            return;
        }
    }
//    public static void sendMessage(String message) {
//        return;
//    }

    public static void sendMessage(String message) {

        System.out.println("test " + (jda.getGuildById("1101569772640026756") != null));
        System.out.println("test " + (jda.getGuilds()));
        System.out.println("test " + (jda.getTextChannels()));
        message = message.replaceAll("§.", "");
        if (channel == null) {
            if (jda.getGuildById("1101569772640026756") != null) channel = jda.getGuildById("1101569772640026756").getTextChannelById(channel_id);channel = jda.getTextChannelById(channel_id);
        }
        if (channel == null) {
            System.out.println("Error! Discord channel is null. Please check config.json " + channel_id);
            if (jda.getTextChannelsByName("logs", false).size()>0) {
                channel = jda.getTextChannelsByName("logs", false).get(0);
            }
        }
        if (channel == null) {
            System.out.println("Error! Discord channel is null. Please check config.json " + channel_id);
            if (jda.getTextChannels().size()>0) {
                System.out.println("test ");
                System.out.println("test " + jda.getTextChannels().get(0).getName());
                for (TextChannel channel1 : jda.getTextChannels()) {
                    System.out.println("test " + channel1.getName());
                }
                channel = jda.getTextChannels().get(0);
            }
        }
        if (channel == null) {
            System.out.println("Error! Discord channel is null. Please check config.json " + channel_id);
            return;
        }
        channel.sendMessage(message).queue();
//        BotUtils.sendMessage(client.getChannelByID(channel_id), message.replaceAll("§.", ""));
    }


    public static boolean sendMessageToPlayer(String sender, String receiver, String message) {
        File slave = new File(FMLCommonHandler.instance().getMinecraftServerInstance().getDataDirectory(), "/playerdata/slaves/" + receiver);
        String masterName = receiver;
        System.out.println("TEST");
        if (slave.exists()) {
            try {
                FileReader fileReader = new FileReader(slave);
                BufferedReader reader = new BufferedReader(fileReader);
                masterName = reader.readLine();
                masterName = masterName.replace("!","");
                reader.close();
                fileReader.close();
            } catch (Exception e) {
                System.out.println("Error!" + e);
                return false;
            }
        }
        if (masterName != null) {
            try {
                File discord = new File(FMLCommonHandler.instance().getMinecraftServerInstance().getDataDirectory(), "/playerdata/discords/" + masterName);
                if(!discord.exists()) return false;
                FileReader fileReader2 = new FileReader(discord);
                BufferedReader reader2 = new BufferedReader(fileReader2);
                String discordtag = reader2.readLine();
                discordtag = discordtag.replace("!СКРЫТО!","");
                reader2.close();
                fileReader2.close();
                User user = jda.getUserByTag(discordtag);
                if (user != null) {
                    user.openPrivateChannel().queue(channel -> {
                        channel.sendMessage("**" + sender + "** *прислал* **" + receiver + "** *сообщение*: \n" +
                                message +
                                "\n*Используйте !r или !о для ответа.*").queue();
                    });
                    return true;
                }
            } catch (Exception e) {
                System.out.println("Error!" + e);
                return false;
            }
        }
        return false;
    }

    public static boolean callPlayer(String sender, String receiver) {
        File slave = new File(FMLCommonHandler.instance().getMinecraftServerInstance().getDataDirectory(), "/playerdata/slaves/" + receiver);
        String masterName = receiver;
        if (slave.exists()) {
            try {
                FileReader fileReader = new FileReader(slave);
                BufferedReader reader = new BufferedReader(fileReader);
                masterName = reader.readLine();
                masterName = masterName.replace("!","");
                reader.close();
                fileReader.close();
            } catch (Exception e) {
                System.out.println("Error!" + e);
                return false;
            }
        }
        if (masterName != null) {
            try {
                File discord = new File(FMLCommonHandler.instance().getMinecraftServerInstance().getDataDirectory(), "/playerdata/discords/" + masterName);
                if(!discord.exists()) return false;
                FileReader fileReader2 = new FileReader(discord);
                BufferedReader reader2 = new BufferedReader(fileReader2);
                String discordtag = reader2.readLine();
                discordtag = discordtag.replace("!СКРЫТО!","");
                reader2.close();
                fileReader2.close();
                User user = jda.getUserByTag(discordtag);
                if (user != null) {
                    user.openPrivateChannel().queue(channel -> {
                        channel.sendMessage("**" + sender + "** зовёт **" + receiver + "** в игру!" +
                                "\n*Используйте !r или !о для ответа.*").queue();
                    });
                    return true;
                }
            } catch (Exception e) {
                System.out.println("Error!" + e);
                return false;
            }
        }
        return false;
    }

    public static void sendMessageToMasters(String message) {
        if (jda == null) {
            return;
        }
        message = message.replaceAll("§.", "");
        if (channel == null) {
            channel = jda.getTextChannelById(channel_id);
        }
        if (channel == null) {
            System.out.println("Error! Discord channel is null. Please check config.json");
            return;
        }
        channel.sendMessage("<@&962022055073677384> " + message).queue();
//        BotUtils.sendMessage(client.getChannelByID(channel_id), message.replaceAll("§.", ""));
    }

    public static long getChannelID() {
        return channel_id;
    }

    public static void close() {
    }

    public static void updateOnline(boolean leave) {
        int online = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getCurrentPlayerCount();
        if (leave) online -= 1;
        System.out.println(Arrays.toString(FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getOnlinePlayerNames()));
        if (online > 0) {
            jda.getPresence().setActivity(Activity.playing("Онлайн: " + online));
        } else {
            jda.getPresence().setActivity(Activity.playing("DustRP"));
        }
    }

    private Pattern pattern = Pattern.compile("#\\d+");

    public boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        return pattern.matcher(strNum).matches();
    }

}