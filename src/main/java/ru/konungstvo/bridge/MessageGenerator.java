package ru.konungstvo.bridge;

import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.event.ClickEvent;
import net.minecraft.util.text.event.HoverEvent;
import ru.konungstvo.chat.message.RoleplayMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.message.Message;

/**
 * Convert local Message object to game-specific format.
 * Currently works with Minecraft 1.12.2
 */
public class MessageGenerator {


    /*
    public static TextComponentString generateComponents(MessageComponent messageComponent) {
        TextComponentString result = new TextComponentString(messageComponent.getText());
        result.getStyle().setColor(messageComponent.getColor().get());
        if (messageComponent.hasSiblings()) {
            for (MessageComponent sibling : messageComponent.getSiblings()) {
                TextComponentString siblingStr = new TextComponentString(sibling.getText());
                siblingStr.getStyle().setColor(sibling.getColor().get());
                result.appendSibling(siblingStr);
            }
        }
        return result;
    }



    public static TextComponentString generate(RoleplayMessage roleplayMessage) {
        TextComponentString result = null;
        for (MessageComponent localComponent : roleplayMessage.getTemplate()) {
            if (result == null) {
                result = new TextComponentString(localComponent.getText());
                result.getStyle().setColor(localComponent.getColor().get());
            } else {
                TextComponentString sibling = new TextComponentString(localComponent.getText());
                sibling.getStyle().setColor(localComponent.getColor().get());
                result.appendSibling(sibling);
            }
        }

        return result;
    }
     */

    public static TextComponentString generate(Message message) {
        TextComponentString result = null;
        for (MessageComponent component : message.getComponents()) {
            TextComponentString sibling = new TextComponentString(component.getText());
            sibling.getStyle().setColor(component.getColor().get());
            sibling.getStyle().setBold(component.isBold());
            sibling.getStyle().setItalic(component.isItalic());
            sibling.getStyle().setStrikethrough(component.isStrikethrough());
            sibling.getStyle().setUnderlined(component.isUnderlined());

            if (!component.getClickCommand().isEmpty()) {
                sibling.getStyle().setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, component.getClickCommand()));
            }

            if (component.getHoverText() != null) {
                sibling.getStyle().setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, component.getHoverText()));
            }

            if (result == null) {
                result = sibling;
            } else {
                result.appendSibling(sibling);
            }
        }
        return result;

    }
}
