package ru.konungstvo.bridge;

import com.google.gson.Gson;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;

import net.minecraft.entity.player.EntityPlayerMP;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.codecs.*;
import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import ru.konungstvo.player.Player;

import java.util.concurrent.TimeUnit;

public enum MongoBridge {
    INSTANCE;
    public MongoCollection<Document> sheets;
    public MongoClient client;

    //static final Logger LOG = LogManager.getLogger("Misca-Keeper");


    public static void reload() {

        final String conn = "mongodb://localhost:27017/";

        try {
            System.out.println("Try to connect to DB...");

            final MongoClientSettings settings = MongoClientSettings.builder()
                    .applyConnectionString(new ConnectionString(conn))
                    .applyToConnectionPoolSettings(builder -> builder.maxWaitTime(5, TimeUnit.SECONDS))
                    .codecRegistry(CodecRegistries.fromProviders(new org.bson.codecs.DocumentCodecProvider(), new BsonCodecProvider(),
                            PojoCodecProvider.builder()
                                    .register(Charsheet.class)
                                    .build(),
                            new ValueCodecProvider()
                    ))
                    .build();
            INSTANCE.client = MongoClients.create(settings);
            INSTANCE.sheets = INSTANCE.client.getDatabase("dustrpmongo")
                    .getCollection("charsheets", Document.class);
            System.out.println("Connection to DB is successful");
            System.out.println(INSTANCE.client.getDatabase("dustrpmongo").listCollectionNames().first());
            System.out.println(INSTANCE.client.getDatabase("dustrpmongo").getCollection("charsheets"));

        } catch (Exception e) {
            System.out.println("Failed to connect to DB " + e);
        }
    }

    public void mongoUpdateSkills(String username) {
        if (sheets == null) return;
        System.out.println("Sync with DB... " + "PrayingWanderer");

        try {
            System.out.println(sheets.toString());
            final Document sheet = sheets.find(Filters.eq("character", "PrayingWanderer")).first();
            if (sheet == null) {
                System.out.println("Player entry is not found");
                return;
            }
            System.out.println("Found entry: ");
            System.out.println("Found entry: " + new Gson().toJson(sheet));

        } catch (Exception e) {
            System.out.println("Failed to get info from DB " + e);
        }
    }
}