package ru.konungstvo.bridge;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppingEvent;
import ru.konungstvo.item.ToolsTab;

@Mod(modid = Core.MODID, name = Core.NAME, version = Core.VERSION, acceptableRemoteVersions = "*")
public class Core {

    public static final String MODID = "kmsystem";
    public static final String NAME = "kmsystem";
    public static final String VERSION = "2.1";
    public static final CreativeTabs TOOLS_TAB = new ToolsTab();


    @SidedProxy(
            clientSide = "ru.konungstvo.bridge.ClientProxy",
            serverSide = "ru.konungstvo.bridge.ServerProxy"
    )
    protected static ServerProxy proxy;

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init(event);
    }

    @Mod.EventHandler
    public void onServerStart(FMLServerStartingEvent event) {
        proxy.startServer(event);
    }

    @Mod.EventHandler
    public void onServerStop(FMLServerStoppingEvent event) {
        proxy.stopServer(event);
    }


}
