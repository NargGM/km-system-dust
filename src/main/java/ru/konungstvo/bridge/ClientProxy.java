package ru.konungstvo.bridge;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiNewChat;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.event.ClickEvent;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.client.event.ClientChatReceivedEvent;
import net.minecraftforge.client.event.FOVUpdateEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.server.permission.DefaultPermissionLevel;
import net.minecraftforge.server.permission.PermissionAPI;
import noppes.npcs.api.event.PlayerEvent;
import ru.konungstvo.combat.equipment.BodyPart;
import ru.konungstvo.combat.movement.ClientGo;
import ru.konungstvo.commands.executor.PurgeContainersExecutor;
import ru.konungstvo.commands.executor.PurgeModifiersExecutor;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.gm.npcadder.ChooseNpcCommand;
import ru.konungstvo.commands.helpercommands.player.attack.AttackCommand;
import ru.konungstvo.commands.helpercommands.player.attack.SelectOpponentCommand;
import ru.konungstvo.control.network.*;

import java.util.*;

public class ClientProxy extends ServerProxy {
    private static List<ClickContainer> activeClickContainers = new LinkedList<>();
    private static int ID_COUNT = 666;
    private static final int EFFECTS_ID = ID_COUNT - 1;
    private static final int DEBUG_ID = ID_COUNT - 2;
    private static final int EXAMINE_ID = ID_COUNT - 3;
    private static final int SUBORDINATE_ID = ID_COUNT - 4;
    private static NBTTagCompound effects = null;

    private static boolean thirdPersonBlocked = false;
    private static int thirdPerson = 0;

    private static int modifiedDamage = -666;
    private static int modifiedDice = -666;
    private static int bodyPart = -666;
    private static int serial = -666;
    private static int percent = -666;
    private static int stationary = -666;
    private static int cover = -666;
    private static int optic = -666;
    private static int bipod = -666;
    private static int stunning = -666;
    private static int capture = -666;
    private static int noriposte = -666;
    private static int shieldwall = -666;
    private static int suppressing = -666;

    public static ClickContainer createClickContainer(String name) {
        // even more mess
        String originalName = name;
        if (name.startsWith("ChooseNpc") && !name.equals("ChooseNpc0")) {
            name = "ChooseNpc";
        }
        // this is a mess
        switch (name) {
            case "Duel":
                return new ClickContainer(ID_COUNT++, "Duel", AttackCommand.START_MESSAGE, AttackCommand.END_MESSAGE);
            case "SelectOpponent":
                return new ClickContainer(ID_COUNT++, "SelectOpponent", SelectOpponentCommand.START_MESSAGE, SelectOpponentCommand.END_MESSAGE);

            case "ChooseNpc0":
                return new ClickContainer(ID_COUNT++, originalName, ChooseNpcCommand.START_MESSAGE, ChooseNpcCommand.END_MESSAGE);

            case "ChooseNpc":
                return new ClickContainer(ID_COUNT++, originalName, "", ChooseNpcCommand.END_MESSAGE);


            case "SelectDefense":
                return new ClickContainer(ID_COUNT++, "SelectDefense");

            case "ModifiersButton":
                return new ClickContainer(ID_COUNT++, "ModifiersButton");
            case "MagicButton":
                return new ClickContainer(ID_COUNT++, "MagicButton");
            case "BoomDefense":
                return new ClickContainer(ID_COUNT++, "BoomDefense");
            case "Cover":
                return new ClickContainer(ID_COUNT++, "Cover");
            case "Feint":
                return new ClickContainer(ID_COUNT++, "Feint");
            case "Examine":
                return new ClickContainer(EXAMINE_ID, "Examine");
            case "ChooseModifier":
                return new ClickContainer(ID_COUNT++, "ChooseModifier");

            case "ToolPanel":
                return new ClickContainer(ID_COUNT++, "ToolPanel");
            case "PerformAction":
                return new ClickContainer(ID_COUNT++, "PerformAction");

            case "MovementCounter":
                return new ClickContainer(ID_COUNT++, "MovementCounter");

            case "ActiveHand":
                return new ClickContainer(ID_COUNT++, "ActiveHand");
            case "ActiveHandDefense":
                return new ClickContainer(ID_COUNT++, "ActiveHandDefense");
            case "ChooseSkill":
                return new ClickContainer(ID_COUNT++, "ChooseSkill");
            case "GiveMeArmor":
                return new ClickContainer(ID_COUNT++, "GiveMeArmor");
            case "ArmorMerge":
                return new ClickContainer(ID_COUNT++, "ArmorMerge");

            case "Subordinate":
                return new ClickContainer(SUBORDINATE_ID, "Subordinate");
            case "MovementPanel":
                return new ClickContainer(ID_COUNT++, "MovementPanel");
            case "ShiftContainer":
                return new ClickContainer(ID_COUNT++, "ShiftContainer");
            case "ChooseAmmo":
                return new ClickContainer(ID_COUNT++, "ChooseAmmo");
            case "CallPlayer":
                return new ClickContainer(ID_COUNT++, "CallPlayer");
            case "Concentration":
                return new ClickContainer(ID_COUNT++, "Concentration");
            case "Effects":
                return new ClickContainer(EFFECTS_ID, "Effects");
            case "Debug":
                return new ClickContainer(DEBUG_ID, "Debug");

            default:
                return null;
        }
    }


    public static void purgeContainers() {
        GuiNewChat chat = Minecraft.getMinecraft().ingameGUI.getChatGUI();
        for (ClickContainer cc : activeClickContainers) {
            System.out.println("Purging container: " + cc.getId());
            chat.deleteChatLine(cc.getId());
        }
        chat.deleteChatLine(DEBUG_ID);
        activeClickContainers = new LinkedList<>();
    }

    public static void purgeContainersChooseNpc() {
        GuiNewChat chat = Minecraft.getMinecraft().ingameGUI.getChatGUI();
        for (ClickContainer cc : activeClickContainers) {
            if (cc.getName().startsWith("ChooseNpc")) {
                System.out.println("Purging container: " + cc.getId());
                chat.deleteChatLine(cc.getId());
            }
        }
        chat.deleteChatLine(EFFECTS_ID);
        chat.deleteChatLine(DEBUG_ID);
        activeClickContainers = new LinkedList<>();
    }

    public static void setThirdPersonBlocked(boolean blocked) {
        thirdPersonBlocked = blocked;
    }

    public static void setThirdPerson(int person) {
        thirdPerson = person;
    }

    public static int getBodyPart() {
        return bodyPart;
    }

    public static int getCover() {
        return cover;
    }

    public static void setCover(int cover) {
        ClientProxy.cover = cover;
    }

    public static int getOptic() {
        return optic;
    }

    public static void setOptic(int optic) {
        ClientProxy.optic = optic;
    }

    public static int getBipod() {
        return bipod;
    }

    public static void setBipod(int bipod) {
        ClientProxy.bipod = bipod;
    }

    public static void setStunning(int stunning) {
        ClientProxy.stunning = stunning;
    }

    public static int getStunning() {
        return stunning;
    }

    public static void setCapture(int capture) {
        ClientProxy.capture = capture;
    }

    public static int getCapture() {
        return capture;
    }

    public static void setNoRiposte(int noriposte) {
        ClientProxy.noriposte = noriposte;
    }

    public static int getNoRiposte() {
        return noriposte;
    }

    public static void setShieldwall(int shieldwall) {
        ClientProxy.shieldwall = shieldwall;
    }

    public static int getShieldwall() {
        return shieldwall;
    }

    public static void setSuppressing(int suppressing) {
        ClientProxy.suppressing = suppressing;
    }

    public static int getSuppressing() {
        return suppressing;
    }



    public void init(FMLInitializationEvent event) {
        MinecraftForge.EVENT_BUS.register(this);

        PermissionAPI.registerNode("km.gm", DefaultPermissionLevel.OP, "Game Master permission");
        PermissionAPI.registerNode("km.tell", DefaultPermissionLevel.NONE, "Private messages permission");
        PermissionAPI.registerNode("km.builder", DefaultPermissionLevel.NONE, "Builder permission");

        KMPacketHandler.INSTANCE.registerMessage(PacketMessageHandler.class, PacketMessage.class, 1, Side.CLIENT);
        KMPacketHandler.INSTANCE.registerMessage(ClickContainerPacketMessageHandler.class, ClickContainerMessage.class, 2, Side.CLIENT);
        KMPacketHandler.INSTANCE.registerMessage(ModifierMessageHandler.class, ModifierMessage.class, 3, Side.CLIENT);
        KMPacketHandler.INSTANCE.registerMessage(ClientGoMessageHandler.class, ClientGoMessage.class, 4, Side.CLIENT);
        KMPacketHandler.INSTANCE.registerMessage(ModifierSyncMessageHandler.class, ModifierSyncMessage.class, 5, Side.CLIENT);
        KMPacketHandler.INSTANCE.registerMessage(ThirdPersonMessage.ThirdPersonMessageHandler.class, ThirdPersonMessage.class, 6, Side.CLIENT);
        KMPacketHandler.INSTANCE.registerMessage(ThirdPersonClientMessage.ThirdPersonClientMessageHandler.class, ThirdPersonClientMessage.class, 7, Side.CLIENT);


        //ClientCommandHandler.instance.registerCommand(new MovementExecutor());
        ClientCommandHandler.instance.registerCommand(new ClientGo());
        ClientCommandHandler.instance.registerCommand(new PurgeContainersExecutor());
//        ClientCommandHandler.instance.registerCommand(new LoadCommand());
//        ClientCommandHandler.instance.registerCommand(new UnloadCommand());


    }


    public ClientProxy() {
        MinecraftForge.EVENT_BUS.register(this);
    }


    private static String getModString() {
        // CREATE DEBUG MESSAGE IF NEEDED
        String mods = "";
        //    if (clickContainer.getName().equals("ToolPanel") ||
        //            clickContainer.getName().equals("PerformAction")
        //    ) {

        if (modifiedDamage != -666) {
            String mDamageStr = String.valueOf(modifiedDamage);
            if (modifiedDamage > 0) mDamageStr = "+" + mDamageStr;
            mods = " Урон: [" + mDamageStr + "] ";
        }
        if (modifiedDice != -666) {
            String mDiceStr = String.valueOf(modifiedDice);
            if (modifiedDice > 0) mDiceStr = "+" + mDiceStr;
            mods += " Уровень навыка: [" + mDiceStr + "] ";
        }
        if (percent != -666) {
            mods += " Процентный: " + percent + "%";
        }
        if (bodyPart != -666) {
            mods += "Точечная: " + BodyPart.getBodyPartFromInt(bodyPart).getName();
        }
        if (serial == 1) {
            mods += " Серийный: да";
        }
        if (stationary == 1) {
            mods += " Стационарно: да";
        }
        if (cover == 1) {
            mods += " Укрытие: да";
        }
        if (optic == 1) {
            mods += " Прицеливание: да";
        }
        if (bipod == 1) {
            mods += " Сошки установлены: да";
        }
        if (stunning == 1) {
            mods += " Оглушение: да";
        }
        if (capture == 1) {
            mods += " Призыв к сдаче: да";
        }
        if (noriposte == 1) {
            mods += " Не использовать рипост: да";
        }
        if (shieldwall == 1) {
            mods += " Глухая оборона: да";
        }
        if (suppressing == 1) {
            mods += " Огонь на подавление: да";
        }

        return mods;
        //   }
    }

    @SubscribeEvent
    public void onClientChat(ClientChatReceivedEvent event) {
        GuiNewChat chat = Minecraft.getMinecraft().ingameGUI.getChatGUI();
        ITextComponent message = event.getMessage();
//        String content = message.getFormattedText();


        //TODO: ERROR HANDLING!
        if (activeClickContainers.isEmpty()) {
            return;
        }

        event.setCanceled(true);
        chat.printChatMessage(message);


        try {
//            String mods = getModString();
//            TextComponentString debug = null;
//            if (!mods.isEmpty() && false) {
//                System.out.println("моды не empty");
//                debug = new TextComponentString(String.format("Модификаторы: %s", mods));
//                TextComponentString removeDebug = new TextComponentString(" [Убрать все]");
//                removeDebug.getStyle().setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/" + PurgeModifiersExecutor.NAME));
//                debug.appendSibling(removeDebug);
//                debug.getStyle().setColor(TextFormatting.GRAY);
//                debug.getStyle().setItalic(true);
//            }
//
//
//            if (debug != null) {
//                chat.printChatMessageWithOptionalDeletion(debug, DEBUG_ID);
//            } else {
//                chat.deleteChatLine(DEBUG_ID);
//            }

            List<ClickContainer> copy = new ArrayList<>(activeClickContainers); //ну да, костыль
            for (ClickContainer clickContainer : copy) {
                //if(clickContainer.getId() == DEBUG_ID) continue;
                // ADD CLICK CONTAINER TO BOTTOM
                System.out.println("Found active container with id " + clickContainer.getId());
                chat.printChatMessageWithOptionalDeletion(clickContainer.getMessage(), clickContainer.getId());
            }

        } catch (Exception ignored) {
            //e.printStackTrace();
//            TextComponentString error = new TextComponentString("ОШИБКА СИНХРОНИЗАЦИИ ПАНЕЛЕЙ! Отправьте Volpe свой лог: C:\\User\\имя_юзера\\KMRPLauncher\\updates\\1.12.2\\logs\\latest. Вы можете прописать /toolpanel, если вам нужна боевая панель управления.");
//            error.getStyle().setColor(TextFormatting.RED);
//            Minecraft.getMinecraft().player.sendMessage(error);
            //TODO Concurrent никуда не делся, переодически выскакивает, хотя ничего не ломает(?), как ни странно.
        }
    }

//    @SubscribeEvent
//    public void onFovUpdate(InputEvent.KeyInputEvent event) {
//        if (Minecraft.getMinecraft().gameSettings.keyBindTogglePerspective.isPressed()) {
////            if (thirdPersonBlocked) {
////                event.setCanceled(true);
////                Minecraft.getMinecraft().gameSettings.thirdPersonView = 0;
////                TextComponentString debug = new TextComponentString("В этом бою запрещено включать режим от третьего лица!");
////                debug.getStyle().setColor(TextFormatting.RED);
////                GuiNewChat chat = Minecraft.getMinecraft().ingameGUI.getChatGUI();
////                chat.printChatMessage(debug);
////                return;
////            }
//            System.out.println("я читер");
//            //System.out.println("test " + Minecraft.getMinecraft().gameSettings.thirdPersonView);
//            //KMPacketHandler.INSTANCE.sendToServer(new ThirdPersonMessage(Minecraft.getMinecraft().gameSettings.thirdPersonView));
//        }
//    }
    @SubscribeEvent
    public void clientTickEvent(TickEvent.ClientTickEvent event) {
        if (event.phase != TickEvent.Phase.END) return;
        if (Minecraft.getMinecraft().gameSettings.keyBindTogglePerspective.isKeyDown() && !Minecraft.getMinecraft().isSingleplayer()) {
            int newPerson = Minecraft.getMinecraft().gameSettings.thirdPersonView;
            if (newPerson != thirdPerson) {
                if (thirdPersonBlocked) {
                    Minecraft.getMinecraft().gameSettings.thirdPersonView = 0;
                    TextComponentString debug = new TextComponentString("В этом бою запрещено включать режим от третьего лица!");
                    debug.getStyle().setColor(TextFormatting.RED);
                    GuiNewChat chat = Minecraft.getMinecraft().ingameGUI.getChatGUI();
                    KMPacketHandler.INSTANCE.sendToServer(new ThirdPersonMessage(3));
                    chat.printChatMessage(debug);
                    return;
                }
                thirdPerson = newPerson;
                KMPacketHandler.INSTANCE.sendToServer(new ThirdPersonMessage(newPerson));
            }

        }
    }

    public static boolean isContainerActive(String name) {
        for (ClickContainer cc : activeClickContainers) {
            if (cc.getName().equals(name))
                return true;
        }
        return false;
    }

    public static ClickContainer getClickContainer(String name) {
        for (ClickContainer cc : activeClickContainers) {
            if (cc.getName().equals(name))
                return cc;
        }
        return null;
    }

    // INIT CLICK CONTAINER
    public static void addClickContainer(String containerName, ITextComponent message) {
        ClickContainer clickContainer;
        System.out.println("Activating container " + containerName);
        if (isContainerActive(containerName)) {
//            deleteClickContainer(containerName);
            System.out.println("Found old one");
            clickContainer = getClickContainer(containerName);
        } else {
            System.out.println("Created new one");
            clickContainer = createClickContainer(containerName);
        }

        GuiNewChat chat = Minecraft.getMinecraft().ingameGUI.getChatGUI();
        if (clickContainer == null) {
            TextComponentString error = new TextComponentString("Ошибка! Попытка добавить непредвиденный ClickContainer: " + containerName);
            error.getStyle().setColor(TextFormatting.RED);
            chat.printChatMessage(error);
            return;
        }

        try {
            clickContainer.setMessage(message);
        } catch (Exception e) {
            System.out.println(e.toString());
            TextComponentString error = new TextComponentString(e.toString());
            error.getStyle().setColor(TextFormatting.RED);
            chat.printChatMessageWithOptionalDeletion(error, clickContainer.getId());
            return;
        }

//        String mods = getModString();
//        TextComponentString debug = null;
//        if (!mods.isEmpty()) {
//            debug = new TextComponentString(String.format("Модификаторы: %s", mods));
//            TextComponentString removeDebug = new TextComponentString(" [Убрать все]");
//            removeDebug.getStyle().setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/" + PurgeModifiersExecutor.NAME));
//            debug.appendSibling(removeDebug);
//
//            debug.getStyle().setColor(TextFormatting.GRAY);
//            debug.getStyle().setItalic(true);
//        }
//        // ADD CLICK CONTAINER TO BOTTOM
//
//        try {
//            if (debug != null) {
//                chat.printChatMessageWithOptionalDeletion(debug, DEBUG_ID);
//            }
//            if (mods.isEmpty()) {
//                chat.deleteChatLine(DEBUG_ID);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


//        System.out.println(activeClickContainers);
        removeActiveContainer(containerName);
        activeClickContainers.add(clickContainer);
//        System.out.println(activeClickContainers);
        try {
            chat.printChatMessageWithOptionalDeletion(message, clickContainer.getId());
        } catch (ConcurrentModificationException e) {
            System.out.println("ConcurrentModification caught!");
            e.printStackTrace();
        } catch (NullPointerException e) {
            System.out.println("Caught null pointer!");
            e.printStackTrace();
        }

    }

    public static void deleteClickContainer(String containerName) {
        System.out.println("Disabling container " + containerName);
        GuiNewChat chat = Minecraft.getMinecraft().ingameGUI.getChatGUI();
        ClickContainer cc = null;
        for (ClickContainer clickContainer : activeClickContainers) {
            if (clickContainer.getName().equals(containerName)) {
//                chat.deleteChatLine(clickContainer.getId());
//                activeClickContainers.remove(clickContainer);
                System.out.println("cc = clickContainer");
                cc = clickContainer;
                break;
            }
        }
        if (cc == null) return;
        try {
            System.out.println("Trying to delete");
            chat.deleteChatLine(cc.getId());
            System.out.println("Deleted");
        } catch (Exception e) {
            e.printStackTrace();
        }
        //activeClickContainers.remove(cc);
        Iterator<ClickContainer> iterator = activeClickContainers.iterator();
        while(iterator.hasNext()){
            if(iterator.next() == cc) {
                System.out.println("Removing " + cc.getName());
                iterator.remove();
            }
        }
    }

    public static void removeActiveContainer(String name) {

        ClickContainer cc = null;
        for (ClickContainer clickContainer : activeClickContainers) {
            if (clickContainer.getName().equals(name)) {
                cc = clickContainer;
                System.out.println("cc = clickContainer 2");
                break;
            }
        }
        if (cc == null) return;
        System.out.println("Trying to delete 2");
        Iterator<ClickContainer> iterator = activeClickContainers.iterator();
        while(iterator.hasNext()){
            if(iterator.next() == cc) {
                System.out.println("Removing2 " + cc.getName());
                iterator.remove();
            }
        }
        System.out.println("Deleted 2???");

    }


    public static void purgeModifiers() {
        ClientProxy.modifiedDamage = -666;
        ClientProxy.modifiedDice = -666;
        ClientProxy.bodyPart = -666;
        ClientProxy.serial = -666;
        ClientProxy.percent = -666;
        ClientProxy.stationary = -666;
        ClientProxy.cover = -666;
        ClientProxy.optic = -666;
        ClientProxy.bipod = -666;
        ClientProxy.stunning = -666;
        ClientProxy.capture = -666;
        ClientProxy.noriposte = -666;
        ClientProxy.shieldwall = -666;
        ClientProxy.suppressing = -666;

        GuiNewChat chat = Minecraft.getMinecraft().ingameGUI.getChatGUI();
        System.out.println("Trying to delete 3");
        chat.deleteChatLine(DEBUG_ID);
        System.out.println("Deleted 3???");
    }

    public static void setModifiedDamage(int modifiedDamage) {
        ClientProxy.modifiedDamage = modifiedDamage;

    }

    public static void setModifiedDice(int modifiedDice) {
        ClientProxy.modifiedDice = modifiedDice;
    }

    public static void setBodyPart(int modifier) {
        ClientProxy.bodyPart = modifier;
    }

    public static void setStationary(int modifier) {
        ClientProxy.stationary = modifier;
    }

    public static void setSerial(int modifier) {
        ClientProxy.serial = modifier;
    }
}
