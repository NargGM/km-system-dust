package ru.konungstvo.combat;

import ru.konungstvo.control.DataHolder;

import java.io.Serializable;

public class StatusEffect implements Serializable {
    private String name;
    private StatusEnd statusEnd;
    private int turnsLeft;
    private StatusType statusType;
    private String context;
    private int contextNumber;
    private StatusEffect followUp;

    private boolean justActivated;


//    public StatusEffect(String name, int level) {
//        this(name, level, SkillType.SIMPLE, 0);
//    }

//    public StatusEffect(String name, int level, SkillType skillType) {
//        this(name, level, skillType, 0);
//    }

    public StatusEffect(StatusEnd statusEnd, int turnsLeft, StatusType statusType) {
        this(statusType.toString(), statusEnd, turnsLeft, statusType, "", -666, null, false);
    }

    public StatusEffect(String name, StatusEnd statusEnd, int turnsLeft, StatusType statusType) {
        this(name, statusEnd, turnsLeft, statusType, "", -666, null, false);
    }

    public StatusEffect(String name, StatusEnd statusEnd, int turnsLeft, StatusType statusType, String context, int contextNumber, StatusEffect followUp, boolean justActivated) {
        this.name = name;
        this.statusEnd = statusEnd;
        this.turnsLeft = turnsLeft;
        this.statusType = statusType;
        this.context = context;
        this.contextNumber = contextNumber;
        this.followUp = followUp;
        this.justActivated = justActivated;
    }

    public String getName() {
        return name;
    }

    public StatusEnd getStatusEnd() {
        return this.statusEnd;
    }

    public int getTurnsLeft() {
        return turnsLeft;
    }

    public int processTurnsLeft() {
        this.turnsLeft--;
        return turnsLeft;
    }

    public StatusType getStatusType() {
        return statusType;
    }

    public String getContext() {
        return this.context;
    }
    public int getContextNumber() {
        return this.contextNumber;
    }
    public void setContextNumber(int contextNumber) {
        this.contextNumber = contextNumber;
    }

    public StatusEffect getFollowUp() {
        return this.followUp;
    }

    public boolean isJustActivated() { return justActivated; }
    public void setJustActivated(boolean justActivated) {
        this.justActivated = justActivated;
    }
}
