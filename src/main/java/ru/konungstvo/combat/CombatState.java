package ru.konungstvo.combat;

public enum  CombatState {
    SHOULD_WAIT,
    SHOULD_ACT,
    ACTED,
}
