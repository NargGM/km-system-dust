package ru.konungstvo.combat.movement;

import ru.konungstvo.chat.message.SkillDiceMessage;

public class MovementCalculator {

    public static int getAmountOfBlocks(int diceResult) {
        switch (diceResult) {
            case -4:
                return 0;
            case -3:
                return 1;
            case -2:
                return 2;
            case -1:
                return 3;
            case 0:
                return 4;
            case 1:
                return 5;
            case 2:
                return 6;
            case 3:
                return 7;
            case 4:
                return 8;
            case 5:
                return 9;
            case 6:
                return 10;
            case 7:
                return 11;
        }
        return 0;
    }

    public static int getAmountOfBlocksFromDice(SkillDiceMessage diceMessage, String type, double traitmod) {
        System.out.println("getting blocks for " + diceMessage.getDice().getResult());

        //int standard = getAmountOfBlocks(diceResult);

        int blocks = Math.max(diceMessage.getDice().getResult() + diceMessage.getDice().getBase(false), 0) + 2;
        double movementmod = 1;
        switch (type) {
            case "run":
            case "lunge":
                movementmod = 2;
                break;
            case "start":
            case "walk":

            case "free":
                break;
            case "crawl":
                movementmod = 0.5;
            case "crouch":
                break;
            case "jump":
            case "jumpbomb":
                return (int) Math.ceil(1 + diceMessage.getDice().getResult() * movementmod);

        }

        return (int) Math.ceil(blocks * traitmod * movementmod);

    }
}
