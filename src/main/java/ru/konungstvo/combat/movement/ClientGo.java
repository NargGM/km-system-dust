package ru.konungstvo.combat.movement;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.FMLCommonHandler;

@Deprecated
public class ClientGo extends CommandBase {
    public static final String NAME = "clientgo";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getUsage(ICommandSender sender) {
        return null;
    }
    @Override
    public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
        return true;

    }
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        int blocksToCover;
        try {
            blocksToCover = Integer.parseInt(args[0]);
            MovementTracker.addSuspect(sender.getName());
            MovementTracker.attachMovementBadgeToClient(sender.getName(), blocksToCover);
            System.out.println("Added suspect and badge on " + FMLCommonHandler.instance().getSide());
        } catch (Exception e) {
            e.printStackTrace();
            TextComponentString error = new TextComponentString(e.toString());
            error.getStyle().setColor(TextFormatting.RED);
            sender.sendMessage(error);
        }
    }
}
