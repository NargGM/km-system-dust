package ru.konungstvo.combat.movement;

import net.minecraft.util.math.BlockPos;

public class MovementBadge {
    private int blocksToCover;
    private double distanceBehind;
    private BlockPos startingBlock;
    private double startedPosX;
    private double startedPosY;
    private double startedPosZ;
    private double prevPosX;
    private double prevPosY;
    private double prevPosZ;
    protected boolean freely;

    public MovementBadge(int blocksToCover, BlockPos startingBlock, double startedPosX, double startedPosY, double startedPosZ, boolean freely) {
        this.blocksToCover = blocksToCover;
        this.distanceBehind = 0;
        this.startingBlock = startingBlock;
        this.startedPosX = startedPosX;
        this.startedPosY = startedPosY;
        this.startedPosZ = startedPosZ;
        this.prevPosX = this.startedPosX;
        this.prevPosY = this.startedPosY;
        this.prevPosZ = this.startedPosZ;
        this.freely = freely;
        System.out.println("BADGE created!");
        System.out.println(this.toString());
        //Minecraft.getMinecraft().player.sendChatMessage("startedX,Y,Z: " + startedPosX + " " + startedPosY + " " + startedPosZ);
    }

    public void incrementDistanceBehind() {
        this.distanceBehind++;
    }

    public int getBlocksToCover() {
        return blocksToCover;
    }

    public void setBlocksToCover(int blocksToCover) {
        this.blocksToCover = blocksToCover;
    }

    public double getDistanceBehind() {
        return distanceBehind;
    }

    public void setDistanceBehind(double distanceBehind) {
        this.distanceBehind = distanceBehind;
    }

    public BlockPos getStartingBlock() {
        return startingBlock;
    }

    public void setStartingBlock(BlockPos startingBlock) {
        this.startingBlock = startingBlock;
    }

    public double getStartedPosX() {
        return startedPosX;
    }

    public void setStartedPosX(double startedPosX) {
        this.startedPosX = startedPosX;
    }

    public double getStartedPosY() {
        return startedPosY;
    }

    public void setStartedPosY(double startedPosY) {
        this.startedPosY = startedPosY;
    }

    public double getStartedPosZ() {
        return startedPosZ;
    }

    public void setStartedPosZ(double startedPosZ) {
        this.startedPosZ = startedPosZ;
    }

    public double getPrevPosX() {
        return prevPosX;
    }

    public void setPrevPosX(double prevPosX) {
        this.prevPosX = prevPosX;
    }

    public double getPrevPosY() {
        return prevPosY;
    }

    public void setPrevPosY(double prevPosY) {
        this.prevPosY = prevPosY;
    }

    public double getPrevPosZ() {
        return prevPosZ;
    }

    public void setPrevPosZ(double prevPosZ) {
        this.prevPosZ = prevPosZ;
    }
}
