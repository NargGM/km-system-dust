package ru.konungstvo.combat.movement;

public enum MovementHistory {
    NONE, RUN_UP, LAST_TIME, NOW, FREELY, JUMP, JUMPBOMB, MOVED, SPRINT, LUNGE; //Ничего, разбег, в прошлый ход бежал/разбегался, сейчас бежит, свободное, двигался
}
