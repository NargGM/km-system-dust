package ru.konungstvo.combat;

import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import noppes.npcs.api.NpcAPI;
import noppes.npcs.api.constants.AnimationType;
import noppes.npcs.entity.EntityNPCInterface;
import org.apache.commons.lang3.tuple.MutablePair;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.equipment.*;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.kmrp_lore.helpers.WeaponDurabilityHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.NPC;
import ru.konungstvo.player.Player;
import ru.konungstvo.player.wounds.WoundType;

import java.io.Serializable;
import java.util.*;

public class Boom implements Serializable, Cloneable {
    private long uuid;
    private int combatid;
    private ArrayList<Player> targetsList;
    private HashMap<Player, String> defenseMap;
    private HashMap<Player, Integer> coverMap;
    private BlockPos blockPos;
    private int damage;
    private int dimension;
    private boolean defendable;
    private boolean ignorearmor;


    private BoomType boomType;
    private WoundType woundType;
    private AttackType attackType;



    public Boom(BlockPos blockPos, BoomType boomType, int damage, int id, int dimension, boolean defendable, boolean ignorearmor, WoundType woundType, AttackType attackType) {
        this.uuid = UUID.randomUUID().getMostSignificantBits();
        this.targetsList = new ArrayList<>();
        this.defenseMap = new HashMap<Player, String>();
        this.coverMap = new HashMap<Player, Integer>();
        this.boomType = boomType;
        this.blockPos = blockPos;
        this.damage = damage;
        this.combatid = id;
        this.dimension = dimension;
        this.defendable = defendable;
        this.ignorearmor = ignorearmor;
        this.woundType = woundType;
        this.attackType = attackType;
    }

    public void addTarget(Player player) {
        if (!targetsList.contains(player)) targetsList.add(player);
    }

    public void addTargets(ArrayList<Player> targets) {
        targetsList = targets;
    }

    public void removeTarget(Player player) {
        targetsList.remove(player);
    }

    public ArrayList<Player> getTargetsList() {
        return targetsList;
    }

    public boolean hasTarget(Player player) {
        return targetsList.contains(player);
    }

    public void addDefense(Player player, String defense) {
        removeTarget(player);
        defenseMap.put(player, defense);
    }

    public void addCover(Player player, Integer cover) {
        //removeTarget(player);
        coverMap.put(player, cover);
    }

    public void removeCover(Player player) {
        //removeTarget(player);
        coverMap.remove(player);
    }

    public Integer getCover(Player player) {
        //removeTarget(player);
        if (!coverMap.containsKey(player)) return 0;
        return coverMap.get(player);
    }

    public String getTargetsString() {
        StringBuilder targetsName = new StringBuilder();
        for (Player target : targetsList) {
            targetsName.append(target.getName()).append(" ");
//            if (DataHolder.inst().isNpc(target.getName())) continue;
//            target.performCommand("/boomd");
        }

        return targetsName.toString().trim();
    }


    public void doBoom() {
        for (Map.Entry<Player, String> defense : defenseMap.entrySet()) {
            int armor = 0;
            int shield = 0;
            int magicShield = 0;
            if (defense.getValue().equals("safe")) {
                continue;
            } else {
                Player player = defense.getKey();
                System.out.println(player.getName());
                int distance = (int) Math.floor(ServerProxy.getDistanceBetween(blockPos, player));
                if (damage < distance) {
                    Message wound = new Message("", ChatColor.RED);
                    MessageComponent wound111 = new MessageComponent("Игрок " + player.getName() + " успешно защитился от взрыва (" + boomType.color() + boomType.toString() + "§c)", ChatColor.RED);
                    MessageComponent wound222 = new MessageComponent("§c! (§4" + damage + "§6-§7" + distance + "§6-§9" + armor + (shield != 0 ? "§6-§1" + shield : "") + "§6=§4" + (damage-distance) + "§c)");
                    wound.addComponent(wound111);
                    wound.addComponent(wound222);
                    ServerProxy.sendMessageFromAndInformMasters(player, wound);
                    continue;
                }

                if (boomType != BoomType.GAS && boomType != BoomType.WEAK_GAS) {
                    if (defense.getValue().contains("blocking")) {
                        int hand = Integer.parseInt(defense.getValue().split(":")[1]);
                        ItemStack shieldIs = player.getItemForHand(hand);
                        if (WeaponTagsHandler.hasWeaponTags(shieldIs)) {
                            WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(player.getItemForHand(hand));
                            if (weaponTagsHandler.isShield()) {
                                Shield shieldS = new Shield(weaponTagsHandler.getDefaultWeaponName(), weaponTagsHandler.getDefaultWeapon());
                                if (!shieldS.getType().equals("малый")) {
                                    SkillDiceMessage sdm = new SkillDiceMessage(player.getName(), "% блокирование");
                                    sdm.build();
                                    ServerProxy.sendMessageFromAndInformMasters(player, sdm);
                                    //ServerProxy.informDistantMasters(player, sdm, Range.NORMAL.getDistance());
                                    if (sdm.getDice().getResult() >= 1 && (boomType != BoomType.FIRE || sdm.getDice().getResult() >= 2)) {
                                        shield = shieldS.getDamage();
                                        WeaponDurabilityHandler defenderDurHandler = player.getDurHandlerForHand(hand);

                                        if (defenderDurHandler != null && defenderDurHandler.hasDurabilityDict()) {
                                            if (defenderDurHandler.getPercentageRatio() <= 25) {
                                                int ratio = defenderDurHandler.getPercentageRatio();
                                                if (ratio < 0) {
                                                    shield = 0;
                                                } else if (ratio == 0) {
                                                    shield = shield / 2;
                                                } else {
                                                    Random random = new Random();
                                                    int randomResult = random.nextInt(100) + 1;
                                                    System.out.println(randomResult);
                                                    if (randomResult <= (26 - ratio)) {
                                                        shield -= 1;
                                                    }
                                                }
                                            }
                                            if (damage - distance > shield) {
                                                defenderDurHandler.takeAwayDurability(Math.max(1, Math.min(damage - distance, shield)));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }


                    Armor armor1 = player.getArmor();

                    System.out.println(armor1 + " " + armor1.getDamageString() + " " + armor1.getPsychDamage());

                    try {
                        if (attackType.equals(AttackType.PHYSIC)) {
                            MutablePair<Integer, String> defenseModPair = DataHolder.rollMegaDice(armor1.getDamageString(), "§6Физическая броня");
                            armor = (int) defenseModPair.left;
                            //dicesMegaString += (String) defenseModPair.right;
                        } else {
                            MutablePair<Integer, String> psychDefenseModPair = DataHolder.rollMegaDice(armor1.getPsychDamage(), "§5Психическая броня");
                            armor = (int) psychDefenseModPair.left;
                        }
                        //dicesMegaString += (String) psychDefenseModPair.right;

                    } catch (Exception se) {
                        se.printStackTrace();
                    }

//                    Armor arm = player.getArmor();
//                    ArmorPiece armorPiece = null;
//                    if (arm != null) {
//                        armor = arm.getDefense();
//                        armorPiece = arm.getArmorPiece(BodyPart.THORAX);
//                    }
//
//                    if (armorPiece != null) {
//
//                        WeaponDurabilityHandler armorDurHandler = player.getDurHandlerForArmorPiece(armorPiece.getPart().getName());
//                        if (armorDurHandler != null && armorDurHandler.hasDurabilityDict()) {
//                            if (armorDurHandler.getPercentageRatio() <= 25) {
//                                int ratio = armorDurHandler.getPercentageRatio();
//                                if (ratio < 0) {
//                                    armor = 0;
//                                } else if (ratio == 0) {
//                                    armor = armor / 2;
//                                } else {
//                                    Random random = new Random();
//                                    int randomResult = random.nextInt(100) + 1;
//                                    System.out.println(randomResult);
//                                    if (randomResult <= (26 - ratio)) {
//                                        if (armor > 0) armor = -1;
//                                    }
//                                }
//                            }
//                            if (damage - (distance + shield) > armor) {
//                                armorDurHandler.takeAwayDurability();
//                            }
//                        }
//                    }
//
//                    armor += player.getHardened();
                }

                if (boomType != BoomType.GAS && boomType != BoomType.WEAK_GAS) {
                    magicShield = player.getMagicShield();
                    armor += magicShield;
                }

                Message wound = new Message("", ChatColor.RED);
                int cover = getCover(player);
                String type = "";
                if (ignorearmor) {
                    magicShield = 0;
                    armor = 0;
                    shield = 0;
                }

                int skill = 0;
                if (attackType.equals(AttackType.PHYSIC)) {
                    SkillDiceMessage sdm = new SkillDiceMessage(player.getName(), "% физическая защита-");
                    sdm.build();
                    skill = sdm.getDice().getBase();
                } else {
                    SkillDiceMessage sdm = new SkillDiceMessage(player.getName(), "% психическая защита-");
                    sdm.build();
                    skill = sdm.getDice().getBase();
                }

                int lyingMod = 0;
                if (player.isLying()) lyingMod = 2;

                switch (boomType) {
                    case DEFAULT:
                    case STUNNING:
                        int realdamage = damage - (distance + armor + skill + lyingMod + shield + cover);
                        String addDesc = "";
                        System.out.println("testTESTTESTESTTESTEST" + realdamage + " " + distance + " " + armor);
                        if (magicShield != 0 && (realdamage + magicShield) > 0) {
                            player.adjustMagicShield(realdamage + magicShield);
                        }
                        if (realdamage < 0) {
                            MessageComponent wound1 = new MessageComponent("Игрок " + player.getName() + " успешно защитился от взрыва (" + boomType.color() + boomType.toString() + "§c)", ChatColor.RED);
                            MessageComponent wound2 = new MessageComponent("§c! ("+(attackType.equals(AttackType.PSYCHIC) ? "§d" : "§c") + damage + "§6-§7" + distance + "§6-" + (attackType.equals(AttackType.PHYSIC) ? "§6" : "§5") + armor + "§6-" + (attackType.equals(AttackType.PHYSIC) ? "§7" : "§d") + skill + (lyingMod != 0 ? "§6-§b" + lyingMod : "") + (shield != 0 ? "§6-§1" + shield : "") + (cover != 0 ? "§6-§e" + cover : "") + "§6=§4" + realdamage + "§c)");
                            wound.addComponent(wound1);
                            wound.addComponent(wound2);
                            ServerProxy.sendMessageFromAndInformMasters(player, wound);
                            continue;
                        }
                        type = player.inflictDamage(realdamage, "взрыв", false, false, woundType);

                        boolean fallen = false;
//                        if (realdamage > 6 && !player.isLying() && !player.cantBeCharged()) {
//                            SkillDiceMessage sdm = new SkillDiceMessage(player.getName(), "% сила");
//                            sdm.build();
//                            ServerProxy.sendMessageFromAndInformMasters(player, sdm);
//                            //ServerProxy.informDistantMasters(player, sdm, Range.NORMAL.getDistance());
//                            if (sdm.getDice().getResult() < 4) {
//                                fallen = true;
//                                player.makeFall();
//                            }
//                        }

                        MessageComponent wound1 = new MessageComponent("Игроку " + player.getName() + " взрывом (" + boomType.color() + boomType.toString() + "§c) нанесено ", ChatColor.RED);
                        wound.addComponent(wound1);

                        MessageComponent typeC = new MessageComponent(type, ChatColor.RED);

//                        if (fallen) {
//                            addDesc += "\n§3<сбитие с ног>";
//                        }

                        if (!addDesc.isEmpty()) {
                            typeC.setHoverText(new TextComponentString("§3" + addDesc.trim()));
                            typeC.setUnderlined(true);
                        }
                        typeC.setUnderlined(true);

                        typeC.setHoverText((attackType.equals(AttackType.PSYCHIC) ? "§d" : "§c") + damage + "§6-§7" + distance + "§6-" + (attackType.equals(AttackType.PHYSIC) ? "§6" : "§5") + armor + "§6-" + (attackType.equals(AttackType.PHYSIC) ? "§7" : "§d") + skill + (lyingMod != 0 ? "§6-§b" + lyingMod : "") + (shield != 0 ? "§6-§1" + shield : "") + (cover != 0 ? "§6-§e" + cover : "") + "§6=§4" + realdamage + "§c)", TextFormatting.RED);
                        wound.addComponent(typeC);
                        wound.addComponent(new MessageComponent(" " + player.getWoundPyramid().toNiceString(player.getName())));
                        wound.addComponent(new MessageComponent("§c!"));
                        break;

                    case FIRE:
                        if (magicShield != 0 && (distance - magicShield) <= damage) {
                            player.adjustMagicShield(damage - distance);
                        }
                        if (distance + magicShield > damage) {
                            MessageComponent wound11 = new MessageComponent("Игрок " + player.getName() + " успешно защитился от взрыва (" + boomType.color() + boomType.toString() + "§c)", ChatColor.RED);
                            MessageComponent wound22 = new MessageComponent("§c! (§4" + damage + "§6-§7" + distance + "§6-§9" + armor + (shield != 0 ? "§6-§1" + shield : "") + (cover != 0 ? "§6-§e" + cover : "") + "§6=§4" + (damage-(distance+magicShield)) + "§c)");
                            wound.addComponent(wound11);
                            wound.addComponent(wound22);
                            ServerProxy.sendMessageFromAndInformMasters(player, wound);
                            continue;
                        }
                        if (player.cantBurn()) continue;

                        player.addStatusEffect("горение", StatusEnd.TURN_END, 5,  StatusType.BURNING);
                        if (shield > 0) {
                            MessageComponent woundb = new MessageComponent("Игрок " + player.getName() + " §cзакрылся от зажигательной гранаты щитом и избежал ранений, но всё равно §6загорается§c!", ChatColor.RED);
                            wound.addComponent(woundb);
                            break;
                        }
                        type = player.inflictDamage(5, "взрыв зажигательной");
                        MessageComponent woundb = new MessageComponent("Игроку " + player.getName() + " §cзажигательной гранатой нанесена " + type + ", игрок §6загорается§c!", ChatColor.RED);
                        wound.addComponent(woundb);
                        break;
                    case GAS:
                    case WEAK_GAS:
                        if (distance > damage) {
                            MessageComponent wound111 = new MessageComponent("Игрок " + player.getName() + " успешно защитился от взрыва (" + boomType.color() + boomType.toString() + "§c)", ChatColor.RED);
                            MessageComponent wound222 = new MessageComponent("§c! (§4" + damage + "§6-§7" + distance + "§6-§9" + armor + (shield != 0 ? "§6-§1" + shield : "") + (cover != 0 ? "§6-§e" + cover : "") + "§6=§4" + (damage-distance) + "§c)");
                            wound.addComponent(wound111);
                            wound.addComponent(wound222);
                            ServerProxy.sendMessageFromAndInformMasters(player, wound);
                            continue;
                        }
                        if (boomType != BoomType.WEAK_GAS) {
                            SkillDiceMessage sdm = new SkillDiceMessage(player.getName(), "% выносливость");
                            sdm.build();
                            ServerProxy.sendMessageFromAndInformMasters(player, sdm);
                            if (sdm.getDice().getResult() < 2) {
                                type = player.inflictDamage(7, "газ");
                            } else {
                                type = player.inflictDamage(4, "газ");
                            }
                        } else {
                            type = player.inflictDamage(4, "газ");
                        }

                        MessageComponent woundg = new MessageComponent("Игроку " + player.getName() + " " + boomType.color() + "газом§c нанесена ", ChatColor.RED);
                        wound.addComponent(woundg);
                        wound.addComponent(new MessageComponent(type, ChatColor.RED));
                }


                ServerProxy.sendMessageFromAndInformMasters(player, wound);
                if (player.getWoundPyramid().getNumberOfWounds(WoundType.CRITICAL) >= player.getWoundPyramid().getHealthPool() && !player.hasTrait(Trait.LAST_CHANCE)) {
                    try {
                        if (DataHolder.inst().isNpc(player.getName())) {
                            EntityNPCInterface npc = (EntityNPCInterface) DataHolder.inst().getNpcEntity(player.getName());
                            npc.wrappedNPC.getAi().setAnimation(AnimationType.SLEEP);
                            String desc = npc.wrappedNPC.getDisplay().getTitle();
                            npc.wrappedNPC.getDisplay().setTitle((desc + " [в отключке]").trim());
                            npc.wrappedNPC.getDisplay().setHasLivingAnimation(false);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    //actualDefender.makeFall();
                    try {
                        Player gm = DataHolder.inst().getMasterForPlayerInCombat(player);
                        gm.performCommand("/queue remove " + player.getName());
                        gm.performCommand("/combat remove " + player.getName());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    if (DataHolder.inst().isNpc(player.getName())) {
                        NPC npc = (NPC) DataHolder.inst().getPlayer(player.getName());
                        npc.setWoundsInDescription();
                    }
                }
            }
        }
        //DataHolder.inst().getCombat(combatid).removeBoom(this);
        System.out.println("DESPAWN DESPAWN DESPAWN");
        System.out.println("ВЗРЫВ-" + uuid);

        EntityNPCInterface npc = (EntityNPCInterface) DataHolder.inst().getNpcBoom("ВЗРЫВ-" + uuid, dimension);
        if (boomType == BoomType.DEFAULT || boomType == BoomType.STUNNING) NpcAPI.Instance().getIWorld(dimension).explode(blockPos.getX() + 0.5, blockPos.getY(), blockPos.getZ() + 0.5, 0, false, false);
        else if (boomType == BoomType.GAS || boomType == BoomType.FIRE || boomType == BoomType.WEAK_GAS) NpcAPI.Instance().getIWorld(dimension).playSoundAt(npc.wrappedNPC.getPos(), "minecraft:entity.creeper.primed",1, 1);
        //NpcAPI.Instance().getIWorld(dimension).playSoundAt(npc.wrappedNPC.getPos(), "customnpcs:misc.old_explode", 1 , 1);
        if (npc != null) {
            System.out.println("despawn");
            npc.wrappedNPC.despawn();
        } else {

        }
    }

    public long getUuid() {
        return uuid;
    }

    public BoomType getBoomType() {
        return boomType;
    }

    public boolean isDefendable() {
        return defendable;
    }

}
