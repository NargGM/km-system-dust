package ru.konungstvo.combat;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import noppes.npcs.api.constants.AnimationType;
import noppes.npcs.entity.EntityNPCInterface;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.NPC;
import ru.konungstvo.player.Player;
import ru.konungstvo.player.wounds.WoundType;

import java.io.Serializable;
import java.util.*;

public class Fire implements Serializable, Cloneable {
    private int combatid;
    private ArrayList<Player> targetsList;
    private HashMap<Player, String> defenseMap;
    private HashMap<Player, Integer> defendersDices;
    private HashMap<Player, Integer> coverMap;
    private int attackerDiceResult;
    private BlockPos attackerBlockPos;
    private Vec3d attackerVec;
    private Vec3d lookVec;
    private int fireDistance;
    private int dimension;



    public Fire(BlockPos blockPos, int id, int dimension, Vec3d attackerVec, Vec3d lookVec, int attackerDiceResult, int distance) {
        this.targetsList = new ArrayList<>();
        this.defenseMap = new HashMap<Player, String>();
        this.defendersDices = new HashMap<Player, Integer>();
        this.coverMap = new HashMap<Player, Integer>();
        this.combatid = id;
        this.dimension = dimension;
        this.attackerBlockPos = blockPos;
        this.attackerVec = attackerVec;
        this.lookVec = lookVec;
        this.attackerDiceResult = attackerDiceResult;
        this.fireDistance = distance;
    }

    public void addTarget(Player player) {
        if (!targetsList.contains(player)) targetsList.add(player);
    }

    public void addTargets(ArrayList<Player> targets) {
        targetsList = targets;
    }

    public void removeTarget(Player player) {
        targetsList.remove(player);
    }

    public ArrayList<Player> getTargetsList() {
        return targetsList;
    }

    public boolean hasTarget(Player player) {
        return targetsList.contains(player);
    }

    public void addDefense(Player player, String defense) {
        removeTarget(player);
        defenseMap.put(player, defense);
    }

    public void addCover(Player player, Integer cover) {
        //removeTarget(player);
        coverMap.put(player, cover);
    }

    public void removeCover(Player player) {
        //removeTarget(player);
        coverMap.remove(player);
    }

    public Integer getCover(Player player) {
        //removeTarget(player);
        if (!coverMap.containsKey(player)) return 0;
        return coverMap.get(player);
    }

    public String getTargetsString() {
        StringBuilder targetsName = new StringBuilder();
        for (Player target : targetsList) {
            targetsName.append(target.getName()).append(" ");
            if (DataHolder.inst().isNpc(target.getName())) continue;
            target.performCommand("/boomd");
        }

        return targetsName.toString().trim();
    }


    public void doFire() {
        for (Map.Entry<Player, String> defense : defenseMap.entrySet()) {
            int armor = 0;
            int shield = 0;
            if (defense.getValue().equals("safe")) {
                continue;
            } else {
                Player player = defense.getKey();
                System.out.println(player.getName());
                int distance = (int) Math.floor(ServerProxy.getDistanceBetween(attackerBlockPos, player));
                Message wound = new Message("", ChatColor.RED);
                EntityPlayerMP forgeplayer = (EntityPlayerMP) player.getEntity();
                Vec3d vector = forgeplayer.getPositionVector().subtract(attackerVec);
                double angleDir = (Math.atan2(vector.z, vector.x) / 2 / Math.PI * 360 + 360) % 360;
                double angleLook = (Math.atan2(lookVec.z, lookVec.x) / 2 / Math.PI * 360 + 360) % 360;
                double angle = (angleDir - angleLook + 360) % 360;

                String type = "";
                MessageComponent wound1;
                if (!(angle <= 12.5 || angle >= 347.5) || fireDistance < distance) {
                    if (attackerDiceResult < defendersDices.get(player)) continue;
                    type = player.inflictDamage(4, "Огнемёт");
                    player.addStatusEffect("горение", StatusEnd.TURN_END, 5,  StatusType.BURNING);
                    wound1 = new MessageComponent("Игроку " + player.getName() + " огнеметом нанесена " + type + ", игрок загорается!", ChatColor.RED);
                } else {
                    if (attackerDiceResult < defendersDices.get(player)) {
                        player.addStatusEffect("горение", StatusEnd.TURN_END, 5,  StatusType.BURNING);
                        wound1 = new MessageComponent("Игрок " + player.getName() + " загорается!", ChatColor.RED);
                    } else {
                        type = player.inflictDamage(7, "Огнемёт");
                        player.addStatusEffect("горение", StatusEnd.TURN_END, 5,  StatusType.BURNING);
                        wound1 = new MessageComponent("Игроку " + player.getName() + " огнеметом нанесена " + type + ", игрок загорается!", ChatColor.RED);
                    }
                }

                wound.addComponent(wound1);
                ServerProxy.sendMessageFromAndInformMasters(player, wound);
                if (type.equals(WoundType.CRITICAL_DEPR.getDesc()) || type.equals(WoundType.DEADLY.getDesc())) {
                    try {
                        if (DataHolder.inst().isNpc(player.getName())) {
                            EntityNPCInterface npc = (EntityNPCInterface) DataHolder.inst().getNpcEntity(player.getName());
                            npc.wrappedNPC.getAi().setAnimation(AnimationType.SLEEP);
                            String desc = npc.wrappedNPC.getDisplay().getTitle();
                            npc.wrappedNPC.getDisplay().setTitle((desc + " [крит]").trim());
                            npc.wrappedNPC.getDisplay().setHasLivingAnimation(false);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    //actualDefender.makeFall();
                    try {
                        Player gm = DataHolder.inst().getMasterForPlayerInCombat(player);
                        gm.performCommand("/queue remove " + player.getName());
                        gm.performCommand("/combat remove " + player.getName());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    if (DataHolder.inst().isNpc(player.getName())) {
                        NPC npc = (NPC) DataHolder.inst().getPlayer(player.getName());
                        npc.setWoundsInDescription();
                    }
                }
            }
        }
        //DataHolder.inst().getCombat(combatid).removeBoom(this);
    }


}
