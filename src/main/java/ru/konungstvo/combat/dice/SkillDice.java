package ru.konungstvo.combat.dice;

import ru.konungstvo.combat.dice.modificator.Modificator;
import ru.konungstvo.combat.dice.modificator.ModificatorSet;
import ru.konungstvo.combat.dice.modificator.ModificatorType;
import ru.konungstvo.combat.dice.modificator.PercentDice;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.exceptions.DataException;
import ru.konungstvo.player.Player;

import java.io.Serializable;

public class SkillDice extends FudgeDice implements Serializable {
    private String skillName;
    private String overrideskillname = "";
    private boolean attributeRules = false;
    public static String regex = "(\\s[+,-][0-9][0-9]?%?)?(.*)?";

    // constructor for when we have no mod
    public SkillDice(int initial, String skill) {
        this(initial, skill, 0);
    }

    public SkillDice(int initial, String skill, int customMod) {
        this(initial, skill, customMod, 0, 0, 0, 0, 0, 0, 0, new PercentDice(), "", 0, 0);
    }


    public SkillDice(int initial, String skill, int customMod, int woundsMod, int armorMod, int coverMod, int shieldMod, int weightMod, int psyWeightMod, int concentrationMod, PercentDice percentDice, String attribute, int attributeMod, int eqBuff) {
        this(initial, skill,
                new ModificatorSet()
                        .add(new Modificator(customMod, ModificatorType.CUSTOM))
                        .add(new Modificator(woundsMod, ModificatorType.WOUNDS))
                        .add(new Modificator(armorMod, ModificatorType.ARMOR))
                        .add(new Modificator(coverMod, ModificatorType.COVER))
                        .add(new Modificator(shieldMod, ModificatorType.SHIELD))
                        .add(new Modificator(weightMod, ModificatorType.WEIGHT))
                        .add(new Modificator(psyWeightMod, ModificatorType.PSY_WEIGHT))
                        .add(new Modificator(concentrationMod, ModificatorType.COURAGE))
                        .add(new Modificator(eqBuff, ModificatorType.EQUIPMENT))
        );
        setPercentDice(percentDice);
        setAttribute(attribute, attributeMod);


    }

    public SkillDice(int initial, String skill, ModificatorSet set) {
        super(initial, set);
        this.skillName = skill;

    }


    public String getSkillName() {
        return skillName;
    }

    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }

    /* Why was this even a thing? Superclass method should do it
    public String getModAsString() {
        String mod = String.valueOf(getMod());
        if (getMod() == 0) return  "";
        if (getMod() > 0) return " +" + mod;
        return " " + mod;
    }
    */

    public String getSkillInfo() {
        return ((overrideskillname.isEmpty() ? skillName : overrideskillname) +
                //getModAsString()
                (!getFinalModAsString().isEmpty() ? "] [" + getFinalModAsString() : "")).trim()
                ;
    }

    public String getSkillInfo2() {
        return ((overrideskillname.isEmpty() ? skillName : overrideskillname));
    }

    public String getFinalHover() {
        return getFinalHover(false);
    }

    public String getFinalHover(boolean defense) {
        String result = "";
        if (!getModAsString(defense).isEmpty()) {
            result += getModAsString(defense) + "\n§e=" + getBase(defense) + " " + getPlural3(getBase(defense));
        } else {
            result += "§e" + getBase(defense) + " " + getPlural3(getBase(defense));
        }
        if (!defense) result += getPercentInfo();
        return result;
    }



    @Override
    public String toString() {
        return "[" + getSkillName() + "] от " + getInitial() + " и результатом " + getResultAsString();
    }

    public static boolean matches(String playerName, String context) throws DataException {
        Player player = DataHolder.inst().getPlayer(playerName);
        String skillName = player.getSkillNameFromContext(context);
        return skillName != null;
    }

    @Override
    public boolean equals(Object obj) {
        SkillDice other = (SkillDice) obj;
        return this.getResult() == other.getResult();
    }

    public void setOverrideskillname(String overrideskillname) {
        this.overrideskillname = overrideskillname;
    }

    public String getPlural3(int num) {

        int preLastDigit = Math.abs(num) % 100 / 10;
        if (preLastDigit == 1) {
            return "Бросков";
        }

        switch (Math.abs(num) % 10) {
            case 1:
                return "Бросок";
            case 2:
            case 3:
            case 4:
                return "Броска";
            default:
                return "Бросков";
        }

    }

}
