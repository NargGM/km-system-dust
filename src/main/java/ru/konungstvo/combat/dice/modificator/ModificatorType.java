package ru.konungstvo.combat.dice.modificator;

public enum ModificatorType {
    WOUNDS("Штраф от ран"),
    ARMOR("Штраф от брони"),
    SHIELD("Штраф от щита"),
    WEIGHT("Штраф от нагрузки"),
    PSY_WEIGHT("Штраф от психической нагрузки"),
    COVER("Укрытие"),
    PARRY("Штраф от длины оружия"),
    BLOCK("Модификатор щита"),
    POINT_BLANK("Стрельба вблизи"),
    RUNNING("После бега"),
    OFFHAND("Стрельба навскидку"),
    CONCENTRATED("Концентрация"),
    COURAGE("Кураж"),
    NOTCONCENTRATED("Антиконцентрация (серийный огонь)"),
    MULTISHOT("Расстрел"),
    SHIFTED("Замешкался"),
    SAFE("Цель в безопасности"),
    RECOIL("Отдача"),
    SERIAL("Серийный огонь"),
    DOUBLE("Двойной выстрел"),
    PREVIOUS_DEFENSES("Предыдущие защиты"),
    TWOHANDED("Двуручное оружие"),
    FIREARM_STRENGTH("Требования к силе"),
    PERCENT("От процентных бонусов/штрафов"),
    LYING("Лежа"),
    NOMANA("Нет маны"),
    RIPOSTE("Рипост"),
    CUSTOM("Кастомный модификатор"),
    PREPARING("Подготовка заклинания"),
    OPPORTUNITY("Атака по возможности"),
    CONVENIENCE("Удобство"),
    ACCURACY("Точность"),
    DEFENCE_MOD("Защита от снаряжения"),
    EQUIPMENT("От снаряжения"),
    REACH_ADVANTAGE("Выгода от длины"),
    RANGE_TOO_FAR("От дальности"),
    BACKSTAB("Удар в спину"),
    MID_LUNGE("Защита во время рывка"),
    RECKLESS_ATTACK("Безрассудная атака"),
    RECKLESS_DEFENCE("Безрассудная защита"),
    SHAMED("Опозорен"),
    CONFUSED("Растерян"),
    RELENTLESS("Глухая оборона"),
    RELENTLESS_PSYCH("Глухая пси-оборона"),
    PAIRED("Парная атака"),
    OFF_HAND("Неведущая рука"),
    ;



    private String name;
    ModificatorType(String name) {
        this.name = name;
    }
    public String getDefaultName() {
        return name;
    }
}
