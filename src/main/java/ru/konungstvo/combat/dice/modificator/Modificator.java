package ru.konungstvo.combat.dice.modificator;

import org.json.simple.JSONObject;

public class Modificator {
    private int mod;
    private ModificatorType type;
    private String name;

    public Modificator(int mod, ModificatorType type) {
        this(mod, type, type.getDefaultName());
    }

    public Modificator(int mod, ModificatorType type, String name) {
        this.mod = mod;
        this.name = name;
        this.type = type;
    }

    public int getMod() {
        return mod;
    }

    public void setMod(int mod) {
        this.mod = mod;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ModificatorType getType() {
        return type;
    }

    public void setType(ModificatorType type) {
        this.type = type;
    }

    public void add(int mod) {
        this.mod += mod;
    }

    @Override
    public String toString() {
        return "Modificator{" +
                "mod=" + mod +
                ", type=" + type +
                ", name='" + name + '\'' +
                '}';
    }

    public static int compareTo(Modificator mod, Modificator mod2) {
        if (mod.getMod() > mod2.getMod()) return -1;
        else if (mod2.getMod() > mod.getMod()) return 1;
        return mod.getName().compareTo(mod2.getName());
        //return 0;
    }
}
