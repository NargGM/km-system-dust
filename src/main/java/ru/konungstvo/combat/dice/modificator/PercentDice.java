package ru.konungstvo.combat.dice.modificator;

import ru.konungstvo.control.DataHolder;

public class PercentDice {
    private int custom;
    private int wound;
    private int previousDefenses;
    private int skill;
    private int modules;
    private int bipod;
    private int bloodloss;
    private int limbInjury;
    private int ammo;
    private int mentalDebuff;
    private int weight;
    private int stock;
    private int highground;
    private int fatigue;
    private int weakened;
    private int buff;
    private int ripost;

    private int equipment;

    private int backstab;

    public int getSkill() {
        return skill;
    }

    public void setSkill(int skill) {
        this.skill = skill;
    }


    public PercentDice() {
        this.custom = 0;
        this.wound = 0;
        this.previousDefenses = 0;
        this.skill = 0;
        this.modules = 0;
        this.bipod = 0;
        this.bloodloss = 0;
        this.limbInjury = 0;
        this.ammo = 0;
        this.mentalDebuff = 0;
        this.weight = 0;
        this.stock = 0;
        this.highground = 0;
        this.fatigue = 0;
        this.weakened = 0;
        this.buff = 0;
        this.ripost = 0;
        this.equipment = 0;
        this.backstab = 0;
    }

    public int get() {
        return custom + wound + previousDefenses + skill + modules + bipod + bloodloss + limbInjury + ammo + mentalDebuff + weight + stock + highground + fatigue + weakened + buff + ripost + equipment + backstab;
    }

    public String getPercent() {
        int percentDice = get();
        if (percentDice >= 100) percentDice = percentDice % 100;
        if (percentDice <= -100) percentDice = percentDice % 100;
        if (percentDice == 0) return "§7";
        String dbuff = "§aБафф: ";
        if (percentDice < 0 ) dbuff = "§cДебафф: ";
        return "\n" + dbuff + percentDice + "%";
    }

    public int getCustom() {
        return custom;
    }

    public void addToCustom(int add) {
        this.custom += add;
    }

    public void setPreviousDefenses(int previousDefenses) {
        this.previousDefenses = previousDefenses;
    }

    public void setCustom(int custom) {
        this.custom = custom;
    }

    public int getWound() {
        return wound;
    }

    public void setWound(int wound) {
        this.wound = wound;
    }

    public void addToWound(int add) {
        this.wound += add;
    }

    @Override
    public String toString() {
        String result = "";
        if (custom != 0) {
            result += "\nКастом: " + custom + "%";
        }
        if (wound != 0) {
            result += "\nРаны: " + wound + "%";
        }
        if (previousDefenses != 0) {
            result += "\nПред. защиты: " + previousDefenses + "%";
        }
        if (skill != 0) {
            result += "\nНавык(и): " + skill + "%";
        }
        if (modules != 0) {
            result += "\nМодули: " + modules + "%";
        }
        if (bipod != 0) {
            result += "\nСошки: " + bipod + "%";
        }
        if (bloodloss != 0) {
            result += "\nКровопотеря: " + bloodloss + "%";
        }
        if (limbInjury != 0) {
            result += "\nПовреждения конечностей: " + limbInjury + "%";
        }
        if (ammo != 0) {
            result += "\nБоеприпас: " + ammo + "%";
        }
        if (mentalDebuff != 0) {
            result += "\nДоп. штраф от ментальных ран: " + mentalDebuff + "%";
        }
        if (weight != 0) {
            result += "\nНагрузка: " + weight + "%";
        }
        if (stock != 0) {
            result += "\nПриклад: " + stock + "%";
        }

        if (highground != 0) {
            result += "\nВысота: " + highground + "%";
        }

        if (fatigue != 0) {
            result += "\nУсталость: " + fatigue + "%";
        }

        if (weakened != 0) {
            result += "\nСлабость: " + weakened + "%";
        }

        if (buff != 0) {
            if (buff > 0) result += "\nУсиление: " + buff + "%";
            if (buff < 0) result += "\nОслабление: " + buff + "%";
        }

        if (ripost != 0) {
            result += "\nРипост: " + ripost + "%";
        }

        if (equipment != 0) {
            result += "\nСнаряжение: " + equipment + "%";
        }

        if (equipment != 0) {
            result += "\nСнаряжение: " + equipment + "%";
        }

        return result;

    }

    public int getModules() {
        return modules;
    }

    public void setModules(int modules) {
        this.modules = modules;
    }

    public int getBipod() {
        return bipod;
    }

    public void setBipod(int bipod) {
        this.bipod = bipod;
    }

    public int getAmmo() {
        return ammo;
    }

    public void setAmmo(int ammo) {
        this.ammo = ammo;
    }

    public int getBloodloss() {
        return bloodloss;
    }

    public void setBloodloss(int bloodloss) {
        this.bloodloss = bloodloss;
    }

    public int getLimbInjury() {
        return limbInjury;
    }

    public void setLimbInjury(int limbInjury) {
        this.limbInjury = limbInjury;
    }

    public int getMentalDebuff() {
        return mentalDebuff;
    }

    public void setMentalDebuff(int mentalDebuff) {
        this.mentalDebuff = mentalDebuff;
    }

    public int getWeightDebuff() {
        return weight;
    }

    public void setWeightDebuff(int weight) {
        this.weight = weight;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getHighground() {
        return highground;
    }

    public void setHighground(int highground) {
        this.highground = highground;
    }


    public int getFatigue() {
        return fatigue;
    }

    public void setFatigue(int fatigue) {
        this.fatigue = fatigue;
    }

    public int getWeakened() {
        return weakened;
    }

    public void setWeakened (int weakened) {
        this.weakened = weakened;
    }

    public void setEquipment (int equipment) {
        this.equipment = equipment;
    }
    public int getEquipment () {
        return equipment;
    }

    public void setBuff (int buff) {
        this.buff = buff;
    }

    public int getBuff() {return buff;}

    public int getRipost() {
        return ripost;
    }

    public void setRipost(int ripost) {
        this.ripost = ripost;
    }


    public void checkAndGetAndSetLimbInjurty(int limb, String name) {
        int debuff = 0;
        switch (limb) {
            case 0:
                debuff = DataHolder.inst().getModifiers().getLeftarmInjuriesDebuff(name);
                break;
            case 1:
                debuff = DataHolder.inst().getModifiers().getRightarmInjuriesDebuff(name);
                break;
            case 2:
                debuff = DataHolder.inst().getModifiers().getLeftarmInjuriesDebuff(name) + DataHolder.inst().getModifiers().getRightarmInjuriesDebuff(name);
                break;
            case 3:
                debuff = DataHolder.inst().getModifiers().getLegsInjuriesDebuff(name);
                break;
        }
        try {
            if (DataHolder.inst().getPlayer(name).cantGetLimbInjury()) debuff = 0;
        } catch (Exception ignored) {

        }
        this.limbInjury = -debuff;
    }
}
