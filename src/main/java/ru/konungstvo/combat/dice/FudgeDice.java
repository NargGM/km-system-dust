package ru.konungstvo.combat.dice;

import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.chat.message.FudgeDiceMessage;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.dice.modificator.Modificator;
import ru.konungstvo.combat.dice.modificator.ModificatorSet;
import ru.konungstvo.combat.dice.modificator.ModificatorType;
import ru.konungstvo.combat.dice.modificator.PercentDice;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Dice such as % poor.
 */
public class FudgeDice {
    public static String regex = "^%\\s?([0-9]+)(\\s[+-][0-9])?(.*)?";
    private int initial;
    private int initialInt;
    private int base;
    private int baseWoDef;
    private String info;
    private String info2 = "";
    private String dices;
    private String dices2 = "";
    private String diff = "";
    private int result;
    private int firstResult;
    private static Logger logger = new Logger("FudgeDice");
    private int defenseMod;
    private PercentDice percentDice;
    private int persentResult;
    private boolean wasChangedByPercentDice = false;
    private boolean alreadyCast = false;
    private boolean rerolled = false;
    private ArrayList<String> advantages = new ArrayList<>();
    private ArrayList<String> disadvantages = new ArrayList<>();
    private String attribute = "";
    private int attributeMod = 0;
    private boolean chanceDice = false;

    private boolean critFail = false;

    private SkillDiceMessage enemyDice = null;

    private boolean attributeRules = false;

    public ModificatorSet mods;

    public FudgeDice(int initial) {
        this(initial, 0);
    }

    public FudgeDice(int initial, int mod) {
        this(initial, mod, 0, 0, 0, new PercentDice());

    }

    public FudgeDice(int initial, int mod, int woundsMod) {
        this(initial, mod, woundsMod, 0, 0, new PercentDice());
    }

    public FudgeDice(int initial, int mod, int woundsMod, int armorMod, int coverMod, PercentDice percentDice) {
        this(initial,
                new ModificatorSet()
                        .add(new Modificator(mod, ModificatorType.CUSTOM))
                        .add(new Modificator(woundsMod, ModificatorType.WOUNDS))
                        .add(new Modificator(armorMod, ModificatorType.ARMOR))
                        .add(new Modificator(coverMod, ModificatorType.COVER))
        );
        this.percentDice = percentDice;

    }

    public FudgeDice(int initial, ModificatorSet set) {

        this.initial = initial;
        this.initialInt = initial;
        this.info = "(";
        this.dices = "";
        this.mods = set;

        this.base = this.initialInt;
        for (Modificator mod : mods.getMods()) {
            this.base += mod.getMod();
        }
        this.result = this.base;
        this.firstResult = 666;
    }

    public void cast() {
        cast(false);
    }

    public void cast(boolean reroll) {
        if (isAlreadyCast() && !reroll) return;
        if (isAlreadyCast() && reroll) rerolled = true;

        String newInfo = "";

        int percentDiceGet = percentDice.get();
        int percentResult = 0;
        int percentMod = 0;
        if (percentDiceGet  >= 100) {
            if (!isAlreadyCast()) percentMod = percentDiceGet /100;
            percentDiceGet = percentDiceGet % 100;
        }

        if (percentDiceGet  <= -100) {
            if (!isAlreadyCast()) percentMod = percentDiceGet /100;
            percentDiceGet = percentDiceGet % 100;
        }

        if (percentDiceGet != 0) {
            Random random2 = new Random();
            percentResult = random2.nextInt(100) + 1; //69
            setPersentResult(percentResult);

            if (percentDiceGet > 0 && percentResult <= percentDiceGet) {
                setWasChangedByPercentDice(true);
                // +5%, result 6/100, skip
                // +5%, result 5/100, success
                // +70%, result 70/100, skip
                // +70%, result 69/100, success
                percentMod++;

            } else if (percentDiceGet < 0 && percentResult <= -percentDiceGet) {
                setWasChangedByPercentDice(true);
                // -5%, result 5/100, reducing
                // -5%, result 6/100, skip
                // -70%, result 70/100, reducing
                // -70%, result 71/100, skip
                percentMod--;
            }

        }

        if (percentMod != 0 && !isAlreadyCast()) addMod(new Modificator(percentMod, ModificatorType.PERCENT));

        int enemyInt = 0;
        if (enemyDice != null) enemyInt = Math.max(0, enemyDice.getDice().getBase());

        baseWoDef = initialInt + getModsInt() + attributeMod;
        base = initialInt + getModsInt() + attributeMod - enemyInt;
        result = 0;
        if (base <= 0) {
            chanceDice = true;
            info = "§c[ШАНС-БРОСОК] ";
            Random random = new Random();
            int dice = random.nextInt(10) + 1;
            switch (dice) {
                case 10:
                    info += "§e(§ad10 > 10§e)";
                    result++;
                    break;
                case 1:
                    info += "§e(§4d10 > 1§e)";
                    critFail = true;
                    break;
                default:
                    info += "§e(§7d10 > " + dice + "§e)";
                    break;
            }
            if (disadvantages.size() > 0 || advantages.size() > 0) {
                int secResult = 0;
                Random random4 = new Random();
                int dice2 = random4.nextInt(10) + 1;
                switch (dice2) {
                    case 10:
                        info2 += "§e(§ad10 > 10§e)";
                        secResult++;
                        break;
                    case 1:
                        info2 += "§e(§4d10 > 1§e)";
                        break;
                    default:
                        info2 += "§e(§7d10 > " + dice2 + "§e)";
                        break;
                }

                String newSecInfo = "";

                if (disadvantages.size() > advantages.size()) {
                    if (dice < dice2) {
                        diff = "" + TextFormatting.DARK_RED + TextFormatting.BOLD + " < " + TextFormatting.YELLOW;
                    } else if (dice > dice2) {
                        diff = "" + TextFormatting.DARK_RED + TextFormatting.BOLD + " > " + TextFormatting.YELLOW;
                        dice = dice2;
                        result = secResult;
                        if (dice2 == 1) critFail = true;
                    } else {
                        diff = "" + TextFormatting.GRAY + " == " + TextFormatting.YELLOW;
                    }
                } else if (disadvantages.size() < advantages.size()) {
                    if (dice < dice2) {
                        diff = "" + TextFormatting.DARK_GREEN + TextFormatting.BOLD + " < " + TextFormatting.YELLOW;
                        dice = dice2;
                        result = secResult;
                        if (dice2 != 1) critFail = false;
                    } else if (dice > dice2) {
                        diff = "" + TextFormatting.DARK_GREEN + TextFormatting.BOLD + " > " + TextFormatting.YELLOW;
                    } else {
                        diff = "" + TextFormatting.GRAY + " == " + TextFormatting.YELLOW;
                    }
                }
            }
        } else {
            int remainingDices = base;
            info = "(";
            Random random = new Random();
            dices = "§7Результаты бросков:\n";
            while (remainingDices > 0) {
                int dice = random.nextInt(10) + 1;
                switch (dice) {
                    case 8:
                        dices += ("§ed10 > " + dice) + "\n";
                        info += "§e+";
                        result++;
                        break;
                    case 9:
                        dices += ("§2d10 > " + dice) + "\n";
                        info += "§2+";
                        result++;
                        break;
                    case 10:
                        dices += ("§a§ld10 > " + dice) + "\n";
                        info += "§a§l§n+";
                        result++;
                        remainingDices++;
                        break;
                    default:
                        dices += ("§7d10 > " + dice) + "\n";
                        info += "§7=";
                        break;
                }
                remainingDices--;
            }
            info += "§e)";


            if (disadvantages.size() > 0 || advantages.size() > 0) {
                int remainingDices2 = base;
                int secResult = 0;
                dices2 = "§7Результаты бросков:\n";
                info2 = "(";
                Random random3 = new Random();
                while (remainingDices2 > 0) {
                    int dice = random3.nextInt(10) + 1;
                    switch (dice) {
                        case 8:
                            dices2 += ("§ed10 > " + dice) + "\n";
                            info2 += "§e+";
                            secResult++;
                            break;
                        case 9:
                            dices2 += ("§2d10 > " + dice) + "\n";
                            info2 += "§2+";
                            secResult++;
                            break;
                        case 10:
                            dices2 += ("§a§ld10 > " + dice) + "\n";
                            info2 += "§a§l§n+";
                            secResult++;
                            remainingDices2++;
                            break;
                        default:
                            dices2 += ("§7d10 > " + dice) + "\n";
                            info2 += "§7=";
                            break;
                    }
                    remainingDices2--;
                }
                info2 += "§e)";
                String newSecInfo = "";

                if (disadvantages.size() > advantages.size()) {
                    if (result < secResult) {
                        diff = "" + TextFormatting.DARK_RED + TextFormatting.BOLD + " < " + TextFormatting.YELLOW;
                    } else if (result > secResult) {
                        diff = "" + TextFormatting.DARK_RED + TextFormatting.BOLD + " > " + TextFormatting.YELLOW;
                        result = secResult;
                    } else {
                        diff = "" + TextFormatting.GRAY + " == " + TextFormatting.YELLOW;
                    }
                } else if (disadvantages.size() < advantages.size()) {
                    if (result < secResult) {
                        diff = "" + TextFormatting.DARK_GREEN + TextFormatting.BOLD + " < " + TextFormatting.YELLOW;
                        result = secResult;
                    } else if (result > secResult) {
                        diff = "" + TextFormatting.DARK_GREEN + TextFormatting.BOLD + " > " + TextFormatting.YELLOW;
                    } else {
                        diff = "" + TextFormatting.GRAY + " == " + TextFormatting.YELLOW;
                    }
                }
            }
        }
        if (firstResult == 666) this.firstResult = result;
        setAlreadyCast(true);
        System.out.println("Cast dice with result " + result);
    }

    public String getResultAsString() {
        //return DataHolder.getInstance().getSkillTable().get(result) + " [" + DataHolder.getInstance().getSkillTable().get(firstResult) + "]";
//        if (result < -4) result = -5;
//        if (result > 9) result = 10;
        return Integer.toString(result);

    }

    public String getFirstResultAsString() {
        //return DataHolder.getInstance().getSkillTable().get(result) + " [" + DataHolder.getInstance().getSkillTable().get(firstResult) + "]";
        return Integer.toString(firstResult);
    }

    public String getInfo() {
        return info;
    }

    public int getInitial() {
        return initial;
    }

    public String getDices() {
        return (dices + "\n" + getAdvDisadvString()).trim();
    }

    public void addAdvantage(String advantage) {
        System.out.println("adding advantage " + advantage);
        advantages.add(advantage);
        if (this.alreadyCast) cast(true);
    }

    public void addDisadvantage(String disadvantage) {
        disadvantages.add(disadvantage);
        if (this.alreadyCast) cast(true);
    }

    public String getAdvDisadvString() {
        StringBuilder res = new StringBuilder();
        if (advantages.size() > 0) {
            if (advantages.size() > 1) res.append("§2§lПреимущества:\n");
            else res.append("§2§lПреимущество:\n");
            for (String advantage : advantages) {
                res.append(advantage).append("\n");
            }
        }
        if (disadvantages.size() > 0) {
            if (disadvantages.size() > 1) res.append("§4§lПомехи:\n");
            else res.append("§4§lПомеха:\n");
            for (String disadvantage : disadvantages) {
                res.append(disadvantage).append("\n");
            }
        }

        return res.toString();
    }

    @Deprecated
    public Message getModAsMessage() {
        Message mod = new Message("");
        Modificator custom = this.mods.getMod(ModificatorType.CUSTOM);
        if (custom != null && custom.getMod() > 0) mod = new Message(" +" + custom.getMod());
        else if (custom.getMod() < 0) mod = new Message(" " + custom.getMod());

        for (Modificator modificator : mods.getMods()) {
            MessageComponent comp = new MessageComponent(" " + modificator.getMod());
            mod.addComponent(comp);
        }

        /*
        MessageComponent armorModComponent =  new MessageComponent("");
        if (armorMod != 0) {
            armorModComponent = new MessageComponent(" " + armorMod, ChatColor.BLUE);
        }
        MessageComponent woundsModComponent =  new MessageComponent("");
        if (woundsMod != 0) {
            woundsModComponent = new MessageComponent(" " + woundsMod, ChatColor.RED);
        }
        MessageComponent defenseModComponent =  new MessageComponent("");
        if (defenseMod != 0) {
            defenseModComponent = new MessageComponent(" " + defenseMod, ChatColor.DARK_GREEN);
        }
        MessageComponent coverModComponent =  new MessageComponent("");
        if (coverMod != 0) {
            coverModComponent = new MessageComponent(" " + coverMod + "y", ChatColor.COMBAT);
        }
//        mod += armorModStr + woundsModStr + defenseModStr + coverModStr;
        mod.addComponent(armorModComponent);
        mod.addComponent(woundsModComponent);
        mod.addComponent(defenseModComponent);
        mod.addComponent(coverModComponent);

         */

        return mod;
    }

    public String getFinalModAsString() {
        return getFinalModAsString(false);
    }

    public String getFinalModAsString(boolean defence) {
        //if (getModAsString().isEmpty()) return "";

        String resultStr = "";

        String attributeString = "";
        if (attributeRules && !attribute.equals("")) {
            attributeString += attributeMod;
            resultStr += attributeString;
            if (initial >= 0) resultStr += "+" + initial;
            else resultStr += initial;
        } else if (!attributeRules && !attribute.equals("")) {
            resultStr += "" + initial;
            if (attributeMod >= 0) resultStr += "+" + attributeMod;
            else resultStr += attributeMod;
        }


        boolean something = false;
        int result = 0;
        for (Modificator modificator : mods.getMods()) {
            result += modificator.getMod();
            something = true;
        }
        String modsStr = String.valueOf(result);
        if (something && result >= 0) modsStr = "+" + modsStr;
        if (result != 0) resultStr += modsStr;
        if (!resultStr.isEmpty() && attribute.isEmpty()) resultStr = initial + resultStr;
        if (enemyDice != null && defence) {
            if (enemyDice.getDice().getBase() > 0) resultStr += TextFormatting.GRAY + "-" + enemyDice.getDice().getBaseAsString() + TextFormatting.YELLOW;
            else resultStr += TextFormatting.GRAY + "-0" + TextFormatting.YELLOW;
        }
        return "" + resultStr;
    }

    public String getModAsString() {
        return getModAsString(false);
    }

    public String getModAsString(boolean defence) {
        String result = "";
        if (attributeRules && !attribute.equals("")) {
            String modStr = String.valueOf(attributeMod);
            if (attributeMod > 0) modStr = TextFormatting.DARK_GREEN + "" + modStr;
            else modStr = TextFormatting.RED + modStr;
            result += modStr + " " + attribute.substring(0, 1).toUpperCase() + attribute.substring(1) + "\n";
            if (!getSkillName().equals("")) {
                String modStr1 = String.valueOf(initial);
                if (initial > 0) modStr1 = TextFormatting.DARK_GREEN + "+" + modStr1;
                else modStr1 = TextFormatting.RED + modStr1;
                result += modStr1 + " " + getSkillName().substring(0, 1).toUpperCase() + getSkillName().substring(1) + "\n";
            }
        } else if (!attributeRules && !attribute.equals(""))  {
            if (!getSkillName().equals("")) {
                String modStr1 = String.valueOf(initial);
                if (initial > 0) modStr1 = TextFormatting.DARK_GREEN + "" + modStr1;
                else modStr1 = TextFormatting.RED + modStr1;
                result += modStr1 + " " + getSkillName().substring(0, 1).toUpperCase() + getSkillName().substring(1) + "\n";
            }
            String modStr = String.valueOf(attributeMod);
            if (attributeMod > 0) modStr = TextFormatting.DARK_GREEN + "+" + modStr;
            else modStr = TextFormatting.RED + modStr;
            result += modStr + " " + attribute.substring(0, 1).toUpperCase() + attribute.substring(1) + "\n";
        }

        mods.getMods().sort(Modificator::compareTo);

        for (Modificator modificator : mods.getMods()) {
            if (modificator.getMod() == 0) continue;
            String modStr = String.valueOf(modificator.getMod());

            if (modificator.getMod() > 0) modStr = TextFormatting.DARK_GREEN + "+" + modStr;
            else modStr = TextFormatting.RED + modStr;

            result += modStr + " " + modificator.getType().getDefaultName() + "\n";
        }

        if (enemyDice != null && defence) {
            String def = enemyDice.getDice().getSkillName();
            if (enemyDice.getDice().getBase() > 0) result += TextFormatting.GRAY + "-" + enemyDice.getDice().getBaseAsString() + " " + def.substring(0, 1).toUpperCase() + def.substring(1);
            else result += TextFormatting.GRAY + "-0" + " " + def.substring(0, 1).toUpperCase() + def.substring(1);
        }

        if (attribute.isEmpty() && !result.isEmpty()) {
            String modStr1 = String.valueOf(initial);
            if (initial > 0) modStr1 = TextFormatting.DARK_GREEN + "+" + modStr1;
            else modStr1 = TextFormatting.RED + modStr1;
            result = modStr1 + " " + getSkillName().substring(0, 1).toUpperCase() + getSkillName().substring(1) + "\n" + result;
        }

        if (!result.isEmpty()) result = TextFormatting.DARK_GRAY + "Модификаторы бросков:\n" + result;



        return result.trim();
        /*
        String modStr = "";
        if (mod > 0) modStr = " +" + mod;
        else if (mod < 0) modStr = " " + mod;
        String armorModStr = "";
        if (armorMod < 0) armorModStr = "§8 " + armorMod;
        String woundsModStr = "";
        if (woundsMod < 0) woundsModStr = "§4 " + woundsMod;
        String defenseModStr = "";
        if (defenseMod < 0) defenseModStr = "§3 " + defenseMod;
        String coverModStr = "";
        if (coverMod != 0) coverModStr = " " + coverMod + "у";
        modStr += armorModStr + woundsModStr + defenseModStr + coverModStr;

        if (!modStr.equals("")) modStr += "§e";
        return modStr.trim();

         */
    }

    public int getMod() {
        int mod = 0;
        for (Modificator modificator : mods.getMods()) {
            mod += modificator.getMod();
        }
        return mod;
    }

    public String getBaseAsString() {
        return Integer.toString(base);
    }

    public String getBaseWoDefAsString() {
        return Integer.toString(baseWoDef);
    }

    public static boolean matches(String context) {
        logger.debug("Searching for match in context: " + context);
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(context);
        if (matcher.find()) {
            logger.debug("Success!");
            return true;
        } else {
            logger.debug("Failure!");
            return false;
        }
    }

    public int getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "FudgeDice of initial value " + initial + " and result " + getResultAsString();
    }

    public void reroll() {
        String snapshot = this.toString();
        this.cast(true);
        System.out.println ("Rerolled \"" + snapshot + "\" --> \"" + this.toString() + "\"");
    }

    public void considerDefenseMod(int previousDefenses) {
        if (previousDefenses <= 0) return;
        this.base -= previousDefenses;
        this.defenseMod = -previousDefenses;
        logger.debug("Set defenseMod to " + defenseMod);
    }

    public int getDefenseMod() {
        if (defenseMod < -3) return -3;
        return defenseMod;
    }

    public void setDefenseMod(int defenseMod) {
        this.defenseMod = defenseMod;
    }

    public String getSkillName() {
        return "";
    }

    public int getBase() {
        return base;
    }

    public int getBase(boolean defense) {
        if (defense) return base;
        return baseWoDef;
    }

    public void addMod(Modificator modificator) {
        System.out.println("FudgeDice: adding " + modificator);
        this.mods.add(modificator);
//        this.base += modificator.getMod();
//        System.out.println("old result: " + result);
//        if (firstResult != 666) {
//            firstResult += modificator.getMod();
//        }
//        this.result += modificator.getMod();
//        System.out.println("new result: " + result);
    }

    public int getModsInt() {
        int modsInt = 0;
        for (Modificator mod : mods.getMods()) {
            modsInt += mod.getMod();
        }
        return modsInt;
    }

    public void setPercentDice(PercentDice percentDice) {
        this.percentDice = percentDice;
    }

    public PercentDice getPercentDice() {
        return percentDice;
    }

    public boolean wasChangedByPercentDice() {
        return wasChangedByPercentDice;
    }

    public void setWasChangedByPercentDice(boolean wasChangedByPercentDice) {
        this.wasChangedByPercentDice = wasChangedByPercentDice;
    }

    public int getPersentResult() {
        return persentResult;
    }

    public void setPersentResult(int persentResult) {
        this.persentResult = persentResult;
    }

    public boolean isAlreadyCast() {
        return alreadyCast;
    }

    public boolean isRerolled() {
        return rerolled;
    }

    public int getFirstResult() {
        if (firstResult == 666) return result;
        return firstResult;
    }

    public void setAlreadyCast(boolean alreadyCast) {
        this.alreadyCast = alreadyCast;
    }

    public void setAttribute(String attribute, int attributeMod) {
        this.attribute = attribute;
        this.attributeMod = attributeMod;
    }

    public String getDiff() {
        return diff;
    }

    public String getInfo2() {
        return info2;
    }

    public String getDices2() {
        return (dices2 + "\n" + getAdvDisadvString()).trim();
    }

    public void setAttributeRules(boolean attributeRules) {
        this.attributeRules = attributeRules;
    }

    public String getPercentInfo() {
        int percentDice = getPercentDice().get();
        if (percentDice == 0) return "";
        if (percentDice >= 100) percentDice = percentDice % 100;
        if (percentDice <= -100) percentDice = percentDice % 100;
        String percentDiceStr = String.valueOf(percentDice);
        percentDiceStr += "%";

        String dbuff = "Бафф: ";
        if (percentDice < 0) dbuff = "Дебафф: ";
        return "\n" + TextFormatting.GRAY + dbuff + percentDiceStr +
                " " + (Math.abs(percentDice) >= getPersentResult() && percentDice != 0 ? (percentDice < 0 ? TextFormatting.RED : TextFormatting.GREEN) : "") + "Бросок: " + getPersentResult() + "/100" + TextFormatting.GRAY
                + "" + getPercentDice().toString()
                ;
    }

    public SkillDiceMessage getEnemyDice() {return enemyDice;}

    public void setEnemyDice(SkillDiceMessage enemyDice) {this.enemyDice = enemyDice;}

    public boolean isCritFail() {return critFail;}
}