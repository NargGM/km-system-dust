package ru.konungstvo.combat.moves;

import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.bridge.discord.DiscordBridge;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.FudgeDiceMessage;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.combat.Combat;
import ru.konungstvo.combat.RangedWeapon;
import ru.konungstvo.combat.Weapon;
import ru.konungstvo.combat.dice.FudgeDice;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.exceptions.RuleException;
import ru.konungstvo.exceptions.WoundsException;
import ru.konungstvo.player.Player;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Deprecated
public class DeprecatedAttack implements Serializable {
    private Player attacker;
    private Player defender;
    private int weaponMod;
    private RangedWeapon rangedType;
    private FudgeDiceMessage diceMessage;
    private String resultNotification;
    public static String gmNotice = ChatColor.COMBAT + "[" + ChatColor.DICE + "GM" + ChatColor.COMBAT + "] ";

    public DeprecatedAttack(Player attacker, Player defender, FudgeDiceMessage diceMessage) throws Exception {
        this(attacker, defender, diceMessage, -666, null);
    }

    public DeprecatedAttack(Player attacker, Player defender, FudgeDiceMessage diceMessage, int weaponMod, RangedWeapon rangedType) throws Exception {
        this.attacker = attacker;
        this.defender = defender;
        this.weaponMod = weaponMod;
        this.rangedType = rangedType;
        this.diceMessage = diceMessage;
        this.resultNotification = "";

        int realWeaponMod = this.weaponMod;
        if (realWeaponMod == -666) {
            realWeaponMod = attacker.getWeaponMod(diceMessage.getDice().getSkillName());
        }

        //inform GM's about attack
        String info = gmNotice +
                ChatColor.NICK + attacker.getName() + ChatColor.COMBAT + " атакует " + ChatColor.NICK + defender.getName() +
                ChatColor.COMBAT + " с уроном §c" + realWeaponMod
                + ChatColor.COMBAT + ", дайс: " + ChatColor.COMBAT
                + diceMessage.getResult().replace("))", "").substring(diceMessage.getResult().indexOf("F") - 2).
                replaceAll(ChatColor.DICE.toString(), ChatColor.COMBAT.toString());
        ServerProxy.informMasters(info);
//        DiscordBridge.sendMessage(info);
        Combat.getLogger().info(info);

        //inform players about attack
        //attacker.sendMessage(ChatColor.COMBAT + "Ты атакуешь " + defender.getDisplayedName() + "!");
        info = ChatColor.COMBAT + attacker.getDisplayedName() + " атакует " + defender.getDisplayedName() + "!";
//        ForgeCore.sendMessage(attacker, diceMessage.getRange(), info);
        defender.sendMessage("§9Тебя атакуют, защищайся!");

    }

    public FudgeDice getAttackDice() {
        return diceMessage.getDice();
    }

    public Player getAttacker() {
        return attacker;
    }

    public Player getDefender() {
        return defender;
    }

    public RangedWeapon getRangedType() {
        return rangedType;
    }

    public void defend(SkillDiceMessage defenseDiceMessage) throws RuleException {
        String info = diceMessage.getResult() + "\n" + defenseDiceMessage.getResult();
        Combat.getLogger().info("An attack is being defended.\n" + info);

        int attackResult = diceMessage.getDice().getResult();
        int defenseResult = defenseDiceMessage.getDice().getResult();
        //defenseResult -= defender.getPreviousDefenses();

        // inform GM's about defense
        String defInfo = gmNotice +
                ChatColor.NICK + defender.getName() + ChatColor.COMBAT +
                " защищается, штраф " + defender.getPreviousDefenses() + ", дайс: " +
                defenseDiceMessage.getResult().replace("))", "").substring(defenseDiceMessage.getResult().indexOf("F") - 2).
                        replaceAll(ChatColor.DICE.toString(), ChatColor.COMBAT.toString());
        Combat.getLogger().info(defInfo);
        ServerProxy.informMasters(defInfo);
        DiscordBridge.sendMessage(defInfo);

        defender.incrementPreviousDefenses();
        String skillName = diceMessage.getDice().getSkillName();

        // RANGED WEAPONS HERE

        Weapon weapon = attacker.getWeapon(skillName);
        RangedWeapon rangedWeapon = null;
        if (weapon != null) {
            rangedWeapon = weapon.getRangedWeapon();
        }
        if (rangedWeapon == null) {
            rangedWeapon = this.rangedType;
        }
        boolean missedRanged = false;
        String rangedInfoSimple = "";
        String rangedInfoGM = "";

        if (rangedWeapon != null) {

            if (defenseDiceMessage.getDice().getSkillName().equals("рукопашный бой") ||
                    defenseDiceMessage.getDice().getSkillName().equals("парирование")) {
                //if (ForgeCore.getDistanceSquaredBetween(attacker.getName(), defender.getName()) > Math.pow(3, 2)) {
                if (ServerProxy.getDistanceBetween(attacker.getName(), defender.getName()) > 3) {
                    throw new RuleException("Нельзя парировать атаку, находясь дальше трёх блоков от атакующего!");
                }
            }


//            double realDistance = ForgeCore.getDistanceSquaredBetween(attacker.getName(), defender.getName());
            double realDistance = ServerProxy.getDistanceBetween(attacker.getName(), defender.getName());
            int attackDifficulty = rangedWeapon.getAttackDifficulty(realDistance);

            // search for cover
            Pattern pattern = Pattern.compile("\\+[0-9]{1,2}у");
            Matcher matcher = pattern.matcher(defenseDiceMessage.getContent());
            int cover = 0;
            String coverStr = "";
            if (matcher.find()) {
                cover = Integer.parseInt(matcher.group(0).replace("у", ""));
                coverStr = " (" + DataHolder.inst().getSkillTable().get(attackDifficulty) + " +" + cover + " от укрытия)";
            }
            attackDifficulty += cover;

            Combat.getLogger().debug("Cover is " + cover);
            Combat.getLogger().debug("AttackDifficulty is " + attackDifficulty);


            // CHECK ATTACK
            if (attackResult < attackDifficulty) {
                rangedInfoSimple = ChatColor.COMBAT + "Сложность попадания не достигнута!";
                rangedInfoGM = gmNotice + "Попадание: [" + diceMessage.getDice().getResultAsString() +
                        "/" + DataHolder.inst().getSkillTable().get(attackDifficulty) + coverStr + "].";
                missedRanged = true;

            } else {
                rangedInfoSimple = ChatColor.COMBAT + "Сложность попадания достигнута!";
                rangedInfoGM = gmNotice + "Попадание: [" + diceMessage.getDice().getResultAsString() +
                        "/" + DataHolder.inst().getSkillTable().get(attackDifficulty) + coverStr + "].";


                // CHECK DEFENSE
//                if (ForgeCore.getDistanceSquaredBetween(attacker.getName(), defender.getName()) < 9) {
                if (ServerProxy.getDistanceBetween(attacker.getName(), defender.getName()) < 3) {
                    rangedInfoSimple += "\n" + ChatColor.COMBAT + "Сложность защиты не проверяется.";
                    if (attackDifficulty > defenseResult) {
                        defenseResult = Math.max(defenseResult, attackDifficulty);
                        rangedInfoGM += " Урон считается от " + DataHolder.inst().getSkillTable().get(defenseResult);
                    }

                } else if (defenseResult < rangedWeapon.getDifficulty()) {

                    rangedInfoSimple += "\n" + ChatColor.COMBAT + "Сложность защиты не достигнута!";
                    rangedInfoGM += "\n" + gmNotice + "Защита: [" + DataHolder.inst().getSkillTable().get(defenseResult) +
                            "/" + DataHolder.inst().getSkillTable().get(rangedWeapon.getDifficulty()) +
                            "]. Урон считается от сложности попадания " + DataHolder.inst().getSkillTable().get(attackDifficulty);
                    defenseResult = attackDifficulty;

                } else {
                    rangedInfoSimple += "\n" + ChatColor.COMBAT + "Сложность защиты достигнута!";
                    rangedInfoGM += "\n" + gmNotice + "Защита: [" + DataHolder.inst().getSkillTable().get(defenseDiceMessage.getDice().getResult()) +
                            "/" + DataHolder.inst().getSkillTable().get(rangedWeapon.getDifficulty());

                    defenseResult = Math.max(defenseResult, attackDifficulty);

                    rangedInfoGM += "]. Урон считается от " + DataHolder.inst().getSkillTable().get(defenseResult);
                }
            }
        }

        // calculate results of the defense
        if (!missedRanged && (
                attackResult >= defenseResult
                        || defenseDiceMessage.getDice().getSkillName().equals("блокирование"))) {

            // ATTACK SUCCESSFUL
            Combat.getLogger().info("Атака прошла успешно");
            StringBuilder damageInfo = new StringBuilder();

            //consider damage if attack was successful
            long damage;

            // get weapon mod
            int weaponMod = this.weaponMod;
            if (weaponMod == -666) {
                weaponMod = attacker.getWeaponMod(skillName);

            }
            long difference = attackResult - defenseResult;
            //difference /= 2
            int traitMod = 0;
            if (defender.isHardened()) {
                System.out.println("HARDENED");
                traitMod = 1;
            }
            damage = difference + weaponMod - defender.getDefenseMod() - traitMod;

            // consider shield
            boolean withShield = false;
            if (defenseDiceMessage.getDice().getSkillName().equals("блокирование") && attackResult <= defenseResult) {
                withShield = true;
//                damage -= defender.getShield().getMod();
            }

            // choose lowest range
            Range range = diceMessage.getRange();
            if (defenseDiceMessage.getRange().getDistance() < range.getDistance()) {
                range = defenseDiceMessage.getRange();
                diceMessage.setRange(range);
                diceMessage.build();
            } else {
                defenseDiceMessage.setRange(range);
                defenseDiceMessage.build();
            }

            // show dice
//            ForgeCore.sendMessage(attacker, range, diceMessage.getResult());
//           ForgeCore.sendMessage(defender, range, defenseDiceMessage.getResult());

            // CommonProxy.sendMessageFromTwoSources(attacker, defender, range, rangedInfoSimple);
            ServerProxy.informMasters(rangedInfoGM);

            // inform about results
            damageInfo.append
                    (ChatColor.RED).append(difference)
//                    .append(ChatColor.COMBAT).append("-")
                    //                   .append(ChatColor.BLUE).append(defenseResult)
                    .append(ChatColor.COMBAT).append('+')
                    .append("§c").append(weaponMod)
                    .append(ChatColor.COMBAT).append('-')
                    .append(ChatColor.BLUE).append(defender.getDefenseMod());
            if (withShield) {
                damageInfo.append(ChatColor.COMBAT).append('-')
                //     .append(ChatColor.BLUE).append(defender.getShield().getMod())
                ;
            }
            if (traitMod != 0) {
                damageInfo.append(ChatColor.COMBAT).append("-")
                        .append(ChatColor.PURPLE).append(traitMod);
            }
            damageInfo.append(ChatColor.COMBAT).append('=')
                    .append(ChatColor.DICE).append(damage);

            info = ChatColor.COMBAT + "Атака " + attacker.getDisplayedName() + " успешна! Урон: " + damageInfo.toString();
            //CommonProxy.sendMessageFromTwoSources(attacker, defender, range, info);

            // manage wound
            String wound = null;
            try {
                wound = defender.inflictDamage(damage, attacker.getName() + " " + skillName);
            } catch (WoundsException e) {
                e.printStackTrace();
            }
            if (wound != null) {
                //     CommonProxy.sendMessageFromTwoSources(attacker, defender, range,
                //ChatColor.COMBAT + "Игроку " + defender.getDisplayedName() + " нанесена " + ChatColor.RED + wound + "!");
                if ((wound.contains("критическая") || wound.contains("смертельная")) && !defender.isSuperFighter()) {
                    // CommonProxy.sendMessageFromTwoSources(attacker, defender, range,
                    //ChatColor.COMBAT + "Игрок " + defender.getDisplayedName() + " выбывает из боя!");
                    DataHolder.inst().getCombatForPlayer(defender.getName()).removeFighter(defender.getName());
                }
            }

//            ForgeCore.informMasters(ChatColor.COMBAT + "[" + ChatColor.DICE + "GM" + ChatColor.COMBAT + "] " +
//                    ChatColor.COMBAT + "Урон атаки " + attacker.getName() + "/" + defender.getName() + ": " + damageInfo.toString(), defender);

        } else {
            // DEFENSE SUCCESSFUL

            // choose lowest range
            Range range = diceMessage.getRange();
            if (defenseDiceMessage.getRange().getDistance() < range.getDistance()) {
                range = defenseDiceMessage.getRange();
                diceMessage.setRange(range);
                diceMessage.build();
            }

            // show dice
//            ForgeCore.sendMessage(attacker, range, diceMessage.getResult());
//            ForgeCore.sendMessage(defender, range, defenseDiceMessage.getResult());

            //CommonProxy.sendMessageFromTwoSources(attacker, defender, range, rangedInfoSimple);
            ServerProxy.informMasters(rangedInfoGM);

            // inform about results
            info = ChatColor.COMBAT + "Защита " + defender.getDisplayedName() + " успешна!";
            //CommonProxy.sendMessageFromTwoSources(attacker, defender, range, info);
        }
    }

    public String getResultNotification() {
        return resultNotification;
    }

    public void setResultNotification(String resultNotification) {
        this.resultNotification = resultNotification;
    }

    public int getWeaponMod() {
        return weaponMod;
    }

    public void setWeaponMod(int weaponMod) {
        this.weaponMod = weaponMod;
    }

    @Override
    public String toString() {
        String result = "";
        result += attacker.getName() + " атакует  " + defender.getName() + " с помощью " + diceMessage.getContent();
        return result;

    }
}