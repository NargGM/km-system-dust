package ru.konungstvo.combat;

public enum Trait {

    STAMINA("выдержка"),
    FRAGILE("хрупкий"),
    CONCENTRATED("сосредоточенность"),
    ISME("я видел свой предел"),
    ISM("я видел свою смерть"),
    LEFT_HANDED("левша"),
    RIGHT_HANDED("правша"),
    AMBIDEXTROUS("амбидекстр"),
    PAIRED("бой двумя оружиями"),
    ALLY_DEFENCE("защита союзника"),
    LAST_CHANCE("последний шанс"),
    CONCEIT("развязность"),
    SPLASH("размах"),
    PIERCING("пронзание"),
    BACKSTABBER("спинушник"),
    EXPIRIENCED("бывалый"),
    UNRECKLESS_ATTACK("рассудительная атака");

    private String desc;
    Trait(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return desc;
    }
}
