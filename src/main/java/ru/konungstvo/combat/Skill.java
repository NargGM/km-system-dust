package ru.konungstvo.combat;

import ru.konungstvo.control.DataHolder;

import java.io.Serializable;

public class Skill implements Serializable {
    private String name;
    private int level;
    private int percent;
    private SkillType skillType;

    private String attribute;

    public Skill(String name, int level) {
        this(name, level, SkillType.SIMPLE, "", 0);
    }

    public Skill(String name, int level, SkillType skillType) {
        this(name, level, skillType, "", 0);
    }

    public Skill(String name, int level, SkillType skillType, String attribute, int percent) {
        this.name = name;
        this.level = level;
        this.skillType = skillType;
        this.attribute = attribute;
        this.percent = percent;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public String getLevelAsString() {
        return DataHolder.inst().getSkillTable().get(level);
    }

    public SkillType getSkillType() {
        return skillType;
    }

    public void setSkillType(SkillType skillType) {
        this.skillType = skillType;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Skill) {
            return this.name.equals(((Skill) obj).getName());
        } else if (obj instanceof String) {
            return this.name.equals((String) obj);
        }
        return false;
    }

    public static class SkillComparator implements java.util.Comparator<Skill> {
        public int compare(Skill s1, Skill s2) {
            return s1.getName().length() - s2.getName().length();
        }
    }

    @Override
    public String toString() {
        return "Skill{" +
                "name='" + name + '\'' +
                '}';
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        return;
    }

    public String getAttribute() {return attribute;}
}
