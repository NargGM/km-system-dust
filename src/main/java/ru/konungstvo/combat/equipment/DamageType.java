package ru.konungstvo.combat.equipment;

import java.io.Serializable;

public enum DamageType implements Serializable {
    NONLETHAL("нелетальное", 0),
    LETHAL("летальное", 1),
    CRITICAL("критическое", 2)
    ;

    private String name;
    private int num;

    DamageType(String name, int num) {
        this.name = name;
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
