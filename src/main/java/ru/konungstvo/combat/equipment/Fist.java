package ru.konungstvo.combat.equipment;

public class Fist extends Weapon {

    public Fist() {
        this("fist");
    }
    public Fist(String name) {
        super(name);
    }
    // Damage and reach by Fist is hardcoded (always the same)
    @Override
    public int getDamage() {
        return 0;
    }

    @Override
    public String getDamageString() {
        return "0";
    }

    @Override
    public String getPsychDamage() {
        return "0";
    }
    @Override
    public String getSkill() {
        return "сила+ловкость";
    }

    @Override
    public double getReach() {
        return 1.8;
    }

    //@Override
    //public boolean isMelee() {return true;}

    @Override
    public boolean isFist() {return true;}

    @Override
    public AttackType getAttackType() { return AttackType.PHYSIC; }

    @Override
    public DamageType getDamageType() { return DamageType.LETHAL; }

    @Override
    public int getConvenience() {
        return 0;
    }

    @Override
    public String getSound() {return "punch"; }

}
