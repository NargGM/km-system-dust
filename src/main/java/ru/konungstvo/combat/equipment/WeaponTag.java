package ru.konungstvo.combat.equipment;

public interface WeaponTag {
    int getDamage();
    String getDamageString();

    String getSkill();

    AttackType getAttackType();

    double getReach();

    String getPsychDamage();

    int getAccuracy();

    DamageType getDamageType();

    String getSound();
}
