package ru.konungstvo.combat.equipment;

import java.io.Serializable;

public enum BodyPart implements Serializable {
    ARMOR_JOINT("стык брони", -1),
    HEAD("голова", 0),
    NECK("шея", 1),
    THORAX("торс", 2),
    STOMACH("живот", 3),
    GROIN("пах", 4),
    LEFT_HIP("левое бедро", 5),
    RIGHT_HIP("правое бедро", 6),
    LEFT_KNEE("левое колено", 7),
    RIGHT_KNEE("правое колено", 8),
    LEFT_SHIN("левая голень", 9),
    RIGHT_SHIN("правая голень", 10),
    LEFT_FOOT("левая стопа", 11),
    RIGHT_FOOT("правая стопа", 12),
    LEFT_SHOULDER("левое плечо", 13),
    RIGHT_SHOULDER("правое плечо", 14),
    LEFT_ELBOW("левый локоть", 15),
    RIGHT_ELBOW("правый локоть", 16),
    LEFT_FOREARM("левое предплечье", 17),
    RIGHT_FOREARM("правое предплечье", 18),
    LEFT_HAND("левая кисть", 19),
    RIGHT_HAND("правая кисть", 20),
    ;

    private String name;
    private int num;

    BodyPart(String name, int num) {
        this.name = name;
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public static BodyPart getFromName(String name) {
        for (BodyPart part : BodyPart.values()) {
            if (part.getName().equals(name)) {
                return part;
            }
        }
        return null;
    }
    public static int getDifficultyFor(BodyPart bodyPart) {
        switch (bodyPart) {
            case HEAD:
            case NECK:
            case THORAX:
            case GROIN:
                return 4; // отлично
            case LEFT_HAND:
            case RIGHT_HAND:
            case LEFT_KNEE:
            case RIGHT_KNEE:
            case LEFT_FOOT:
            case RIGHT_FOOT:
            case LEFT_ELBOW:
            case RIGHT_ELBOW:
            case ARMOR_JOINT:
                return 3; // хорошо
            default:
                return 2; // нормально
        }
    }

    public static BodyPart getBodyPartFromInt(int num) {
        for (BodyPart bodyPart : BodyPart.values()) {
            if (bodyPart.getNum() == num) {
                return bodyPart;
            }
        }
        return null;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
