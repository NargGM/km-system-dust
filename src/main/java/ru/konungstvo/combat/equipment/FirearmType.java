package ru.konungstvo.combat.equipment;

import java.io.Serializable;

public enum FirearmType implements Serializable {
    WHIMSY("Прихотливое"),
    TWOHANDED("Двуручное"),
    ONEHANDED("Одноручное")
    ;

    private String name;

    FirearmType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}
