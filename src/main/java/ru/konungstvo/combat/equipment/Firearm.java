package ru.konungstvo.combat.equipment;

import net.minecraft.nbt.NBTTagCompound;
import org.json.simple.parser.ParseException;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;

public class Firearm implements WeaponTag {
    private String range;
    private String weaponType;
    private int sort = 1;
    private int recoil = 0;
    private int recoilTurns = 0;

    private int accuracy = 0;
    private int defencemod = 0;

    private String sound = "";

    private double soundRange = 16F;

    private Module optic;
    private Module barrel;
    private Module underbarrel;
    private Module stock;
    private Module magazine;
    private Module extra;
    private Module extra2;

    private String damage;
    private String psychdamage;

    private String skill = "";

    private AttackType attackType;

    private FirearmType firearmType;

    private DamageType damageType = DamageType.LETHAL;


    private boolean infinite;
    private boolean bulletForShotgun;
    private Projectile loadedProjectile;
    private int homemade = 0;
    protected Firearm(String name, Projectile projectile) {
        //super(name);
        this.loadedProjectile = projectile;
    }

    public boolean isBullet() {
        if (loadedProjectile.getCaliber() == null) return false;
        if (loadedProjectile.getCaliber().equals("мрп")) return true;
        if (loadedProjectile.getCaliber().equals("срп")) return true;
        if (loadedProjectile.getCaliber().equals("крп")) return true;
        return false;
    }

    public boolean isBuck() {
        if (loadedProjectile.getCaliber() == null) return false;
        if (loadedProjectile.getCaliber().equals("мрд")) return true;
        if (loadedProjectile.getCaliber().equals("срд")) return true;
        if (loadedProjectile.getCaliber().equals("крд")) return true;
        return false;
    }

    public Projectile getProjectile() {
        return loadedProjectile;
    }

    public void fillFromNBT(NBTTagCompound nbtTagCompound, int accuracy, int defencemod, String sound) throws ParseException {
        System.out.println("Creating firearm: " + nbtTagCompound.toString());
        this.range = nbtTagCompound.getString("range");
        this.weaponType = nbtTagCompound.getString("type");
        this.accuracy = accuracy;
        this.defencemod = defencemod;
        this.sound = sound;
        damage = nbtTagCompound.getString("damage");
        psychdamage = nbtTagCompound.getString("psychdamage");
        this.skill = nbtTagCompound.getString("skill");
        if (nbtTagCompound.hasKey("skill")) this.skill = skill.replaceAll("подлое", "дальнее подлое").replaceAll("яростное", "дальнее яростное").replaceAll("тайное", "дальнее тайное").replaceAll("бойкое", "дальнее бойкое");
        this.attackType = (nbtTagCompound.getString("attacktype").equals("психический") ? AttackType.PSYCHIC : AttackType.PHYSIC);

        if (nbtTagCompound.hasKey("homemade")) this.homemade = nbtTagCompound.getInteger("homemade");

        if (nbtTagCompound.getString("damagetype").equals("нелетальный")) this.damageType = DamageType.NONLETHAL;
        else if (nbtTagCompound.getString("damagetype").equals("критический")) this.damageType = DamageType.CRITICAL;
        firearmType = FirearmType.TWOHANDED;
        if (nbtTagCompound.getString("category").equals("прихотливое")) this.firearmType = FirearmType.WHIMSY;
        else if (nbtTagCompound.getString("category").equals("одноручное")) this.firearmType = FirearmType.ONEHANDED;

        this.recoil = -Math.abs(Integer.parseInt(nbtTagCompound.getString("recoil")));
        this.recoilTurns = Integer.parseInt(nbtTagCompound.getString("recoilTurns"));
        if (nbtTagCompound.hasKey("soundrange")) soundRange = (double) nbtTagCompound.getInteger("soundrange");
    }

    public int getModulesMods() {
        return this.optic.getMod() +
                this.stock.getMod() +
                this.barrel.getMod() +
                this.underbarrel.getMod() +
                this.extra.getMod() +
                this.extra2.getMod() +
                this.magazine.getMod();
    }

    public void fillModules(WeaponTagsHandler handler) {
        this.optic = new Module(handler.getOptic(), ModuleType.OPTIC);
        this.stock = new Module(handler.getAccessory(), ModuleType.STOCK);
        this.barrel = new Module(handler.getBarrel(), ModuleType.BARREL);
        this.underbarrel = new Module(handler.getUnderbarrel(), ModuleType.UNDERBARREL);
        this.extra = new Module(handler.getExtra(), ModuleType.EXTRA);
        this.extra2 = new Module(handler.getAmmunition(), ModuleType.EXTRA2);
        this.magazine = new Module(handler.getMagazine().getCompoundTag("module"), ModuleType.MAGAZINE);
    }

    public int getDamage() {
        return 0;
    }

    @Override
    public String getSkill() {
        return skill;
    }

    @Override
    public double getReach() {
        return 0;
    }

    @Override
    public String getDamageString() {
        return damage;
    }

    @Override
    public String getPsychDamage() {
        return psychdamage;
    }

    @Override
    public int getAccuracy() {
        return accuracy;
    }

    public Projectile getLoadedProjectile() {
        return loadedProjectile;
    }


    public String getRange() {
        return range;
    }

    public boolean isBreakable() {
        return !weaponType.equals("пулемёт") || sort != 3;
    }

    public int getRecoil() {
        return recoil;
    }

    public int getRecoilTurns() {
        return recoilTurns;
    }

    public int getStrengthReq() {
        switch (weaponType) {
            case "автоматический пистолет":
            case "пистолет-пулемёт":
            case "пистолет":
            case "револьвер":
                if (sort == 3) {
                    return 1;
                }
                return -4;
            case "винтовка":
            case "штурмовая винтовка":
                if (sort == 3) {
                    return 2;
                }
                return -4;
            case "гранатомёт":
            case "дробовик":
                if (sort == 2) {
                    return 2;
                } else if (sort == 3) {
                    return 3;
                }
                return -4;
            case "обрез":
                if (sort == 2) {
                    return 1;
                } else if (sort == 3) {
                    return 2;
                }
                return -4;
            case "пулемёт":
                if (sort == 1) return 1;
                if (sort == 2) return 2;
                if (sort == 3) return 3;
            default:
                return -4;
        }

    }

    public boolean isModern() {
        switch (weaponType) {
            case "устаревшее":
            case "устаревший огнестрел":
            case "другое":
            case "метательное":
            case "магия":
                return false;
            default:
                return true;
        }
    }

    public boolean shouldBoom() {
        return (isModern() || weaponType.equals("устаревший огнестрел"));
    }

    public boolean needStock() {
        switch (weaponType) {
            case "автоматический пистолет":
            case "пистолет":
            case "револьвер":
                return false;
            default:
                return true;
        }
    }

    public boolean isTwoHanded() {
        switch (weaponType) {
            case "автоматический пистолет":
            case "пистолет":
            case "револьвер":
            case "метательное":
            case "магия":
                return false;
            case "устаревший огнестрел":
                return !range.toLowerCase().contains("пистолет");
            case "обрез":
                return sort != 1;
            default:
                return true;
        }
    }

    public boolean isPistol() {
        switch (weaponType) {
            case "автоматический пистолет":
            case "пистолет":
            case "револьвер":
                return true;
            default:
                return false;
        }
    }

    public boolean hasSingleRecoil() {
        switch (weaponType) {
            case "дробовик":
            case "обрез":
                return true;
            case "винтовка":
                return sort == 3;
            default:
                return false;
        }
    }

    public boolean isPistolOrSawoff() {
        return (weaponType.equals("пистолет") || weaponType.equals("автоматический пистолет") || weaponType.equals("револьвер") || weaponType.equals("обрез"));
    }

    public String getWeaponType() {return weaponType;}
    public Integer getSort() {return sort;}

    public String getSortString() {
        switch (sort) {
            case 1:
                if (weaponType.equals("винтовка")) return "легкая";
                return "легкий";
            case 2:
                if (weaponType.equals("винтовка")) return "средняя";
                return "средний";
            case 3:
                if (weaponType.equals("винтовка")) return "тяжелая";
                return "тяжелый";
            default:
                return "???";
        }
    }
    public int getHomemade() {
        return homemade;
    }

    @Override
    public AttackType getAttackType() { return attackType; }

    @Override
    public DamageType getDamageType() { return damageType; }

    @Override
    public String getSound() {
        return sound;
    }

    public double getSoundRange() {
        return soundRange;
    }

    public FirearmType getFirearmType() {return firearmType; }
}
