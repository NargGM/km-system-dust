package ru.konungstvo.combat.equipment;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import org.json.simple.parser.ParseException;

public class Projectile extends Equipment {
    private String damage;
    private String psychdamage;

    private String rangemod;
    private String caliber;
    private String mod;
    private boolean infinite = false;
    private String modsplus = "";
    private int cost = -1;
    private int cooldown = -1;
    private int homemade = 0;
    private int difficulty = -666;
    private boolean prepare = false;
    //private String spellname = "spellname";
    private AttackType attackType;
    private DamageType damageType;
    public Projectile(String name) {
        super(name);
    }

    public void fillFromNBT(NBTTagCompound nbtTagCompound) throws ParseException {

    }

    public void fillFromNBTList(NBTTagList nbtTagList) throws ParseException {
        NBTTagCompound tagCompound = nbtTagList.getCompoundTagAt(nbtTagList.tagCount()-1);
        NBTTagCompound ammo = (NBTTagCompound) tagCompound.getTag("projectile");
        System.out.println(nbtTagList);
        System.out.println(ammo);
        System.out.println(tagCompound);
        damage = ammo.getString("damage");
        psychdamage = ammo.getString("psychdamage");
        caliber = ammo.getString("caliber");
        rangemod = ammo.getString("rangemod");
        mod = ammo.getString("mod");
        infinite = ammo.hasKey("infinite");
        if (ammo.hasKey("attacktype")) {
            attackType = (ammo.getString("attacktype").equals("психический") ? AttackType.PSYCHIC : AttackType.PHYSIC);
        }
        if(ammo.hasKey("modsplus")) modsplus = ammo.getString("modsplus");
        if(ammo.hasKey("cost")) {
            cost = ammo.getInteger("cost");
        }
        if(ammo.hasKey("cooldown")) {
            cooldown = ammo.getInteger("cooldown");
        }
        if(ammo.hasKey("handmade")) homemade = ammo.getInteger("homemade");
        if (!mod.isEmpty()) mod = mod.toLowerCase();
        System.out.println("CREATED WEAPON: " + this.toString());
        if (ammo.hasKey("diff")) difficulty = ammo.getInteger("diff");
        if (ammo.hasKey("prep")) prepare = ammo.getBoolean("prep");
        this.damageType = DamageType.LETHAL;
        //if (ammo.hasKey("spellname")) spellname = ammo.getString("spellname");

        if (ammo.getString("damagetype").equals("нелетальный")) this.damageType = DamageType.NONLETHAL;
        else if (ammo.getString("damagetype").equals("критический")) this.damageType = DamageType.CRITICAL;
    }

    @Override
    public void fillFromJson(String jsonStr) throws ParseException {

    }

    @Override
    public int getDamage() {
        return 0;
    }

    @Override
    public String getSkill() {
        return "";
    }

    @Override
    public String getDamageString() {
        return damage;
    }

    @Override
    public String getPsychDamage() {
        return psychdamage;
    }

    @Override
    public int getConvenience() {
        return 0;
    }

    @Override
    public double getReach() {
        return 0;
    }

    public String getCaliber() {
        return caliber;
    }

    public String getMod() {
        return mod;
    }

    public boolean isInfinite() {
        return infinite;
    }

    public String getModsplus() {
        return modsplus;
    }
    public String getRangemod() {
        return rangemod;
    }

    public int getCost() {
        return cost;
    }

    public int getCooldown() {
        return cooldown;
    }
    public int getHomemade() {
        return homemade;
    }

    public int getDifficulty() { return difficulty; }
    public boolean needPrepare() { return prepare; }

    public AttackType getAttackType() {
        return attackType;
    }

    public DamageType getDamageType() {
        return damageType;
    }
    //public String getSpellname() { return spellname; }
}
