package ru.konungstvo.combat.equipment;

import net.minecraft.nbt.NBTTagCompound;

public class Module {
    private String name;
    private int mod;
    private ModuleType moduleType;

    public Module(NBTTagCompound tagCompound, ModuleType moduleType) {
        this.name = tagCompound.getString("type");
        this.mod = tagCompound.getInteger("mod");
        this.moduleType = moduleType;
    }

    public int getMod() {
        return mod;
    }
}
