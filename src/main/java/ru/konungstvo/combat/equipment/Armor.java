package ru.konungstvo.combat.equipment;

import net.minecraft.nbt.NBTTagCompound;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class Armor extends Equipment implements Serializable {
    private List<ArmorPiece> pieces = new LinkedList<>();

    public Armor(String name) {
        super(name);
    }
    private int convenience;
    private int defencemod;
    private String armored = "0";
    private String psychdefence = "0";

    public int getMod() {
        if (getDamage() == 2) return -1;
        if (getDamage() == 3) return -2;
        return 0;
    }


    public int getDefense() {
        return getDamage();
    }

    public void fillFromNBT(NBTTagCompound nbtTagCompound) throws ParseException {
        this.convenience = nbtTagCompound.getInteger("convenience");
        this.defencemod = nbtTagCompound.getInteger("defencemod");
        this.armored = nbtTagCompound.getString("armored");
        this.psychdefence = nbtTagCompound.getString("psychdefence");
        System.out.println("ARMOR TEST " + convenience + " " + defencemod + " " + armored + " " + psychdefence);
    }

    @Override
    public void fillFromJson(String jsonStr) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONObject json;
        json = (JSONObject) parser.parse(jsonStr);
        JSONObject armor = (JSONObject) json.get("armor");

        for (Object key : armor.keySet()) {
            String keyStr = (String) key;
            JSONObject i = (JSONObject) armor.get(key);
            String value = (String) i.get("t");

            BodyPart part = BodyPart.getFromName(keyStr);
            boolean modern = value.contains("современная");
            value = value.replace("современная ", "");
            value = value.replaceAll("ё", "е");
            System.out.println(value);
            ArmorType type = ArmorType.getFromName(value);

            ArmorPiece armorPiece = new ArmorPiece(part, modern, type);
            addArmorPiece(armorPiece);

        }

        System.out.println("CREATED ARMOR: " + this.toString());
    }

    public void addArmorPiece(ArmorPiece armorPiece) {
        System.out.println("adding armor piece " + armorPiece);
        this.pieces.add(armorPiece);
    }

    private static int getDefenseFromString(String str) {
        switch (str) {
            case "лёгкая":
            case "легкая":
                return 1;
            case "тяжёлая":
            case "тяжелая":
                return 3;
            case "средняя":
                return 2;
        }
        return 0;
    }

    @Override
    public int getDamage() {
        return 0;
    }

    @Override
    public String getSkill() {
        return "";
    }

    @Override
    public String getDamageString() {
        return armored;
    }

    @Override
    public String getPsychDamage() {
        return psychdefence;
    }

    @Override
    public int getConvenience() {
        return convenience;
    }

    public int getDamageFor(BodyPart bodyPart) {
        if (bodyPart == BodyPart.ARMOR_JOINT) bodyPart = BodyPart.THORAX;
        int protection = 0;
//        System.out.println("Searching for " + bodyPart.getName());
        for (ArmorPiece piece : pieces) {
//            System.out.println("Through " + piece.getPart().getName());
            if (piece.getPart() == bodyPart) {
//                System.out.println("Found " + piece.getPart().getName() + " whith protection " + piece.getType().getDefense());
                protection = piece.getType().getDefense();
            }
        }
        return protection;
    }

    public ArmorPiece getArmorPiece(BodyPart bodyPart) {
        if (bodyPart == BodyPart.ARMOR_JOINT) bodyPart = BodyPart.THORAX;
        System.out.println("pieces length: " + pieces.size());
        for (ArmorPiece piece : pieces) {
            System.out.println("Through " + piece.getPart().getName());
            if (piece.getPart() == bodyPart) {
                return piece;

            }
        }
        return null;
    }

//    public ArmorPiece getBestPiece(DamageType damageType, int result) {
//        ArmorPiece thePiece = getArmorPiece(BodyPart.THORAX);
//        int theDefenceMod = 0;
//        if (thePiece != null) {
//            theDefenceMod = thePiece.getType().getDefense();
//            if (damageType != DamageType.DEFAULT) {
//                if (damageType == DamageType.MELEE && thePiece.isModern()) theDefenceMod--;
//                if (damageType == DamageType.MODERN_FIREARM && !thePiece.isModern()) theDefenceMod--;
//            }
//        }
//        List<ArmorPiece> piecesShuffled = pieces;
//        Collections.shuffle(piecesShuffled);
//        for (ArmorPiece piece : piecesShuffled) {
//            if (result < 3 && piece.getPart() != BodyPart.HEAD && BodyPart.getDifficultyFor(piece.getPart()) != 2) continue;
//            int defenceMod = piece.getType().getDefense();
//            if (damageType != DamageType.DEFAULT) {
//                if (damageType == DamageType.MELEE && piece.isModern()) defenceMod--;
//                if (damageType == DamageType.MODERN_FIREARM && !piece.isModern()) defenceMod--;
//            }
//            if (theDefenceMod < defenceMod) {
//                theDefenceMod = defenceMod;
//                thePiece = piece;
//            }
//
//        }
//        return thePiece;
//    }

    @Override
    public double getReach() {
        return 0;
    }

    public int getStrengthRequirement() {
        ArmorPiece ap = getArmorPiece(BodyPart.THORAX);
        if (ap == null) return -10;
        switch (ap.getType()) {
            case LIGHT:
                return -10;
            case MEDIUM:
                return 1;
            case HEAVY:
                return 2;
            default:
                return -10;
        }
    }

    @Override
    public String toString() {
        String piecesStr = "";
        for (ArmorPiece piece : pieces) {
            piecesStr += piece.toString() + "\n";
        }
        return "Armor{" +
                ", pieces=" +
                piecesStr.trim() +
                '}';
    }

    public String toNiceString() {
        StringBuilder lol = new StringBuilder();
        lol.append("§f[Нет брони] §e[Легкая] §7[Средняя] §8[Тяжелая] §a[С. Легкая] §9[С. Средняя] §5[С. Тяжелая]\n");
        lol.append(getNiceString(BodyPart.HEAD)).append(getNiceString(BodyPart.NECK)).append(getNiceString(BodyPart.THORAX))
                .append(getNiceString(BodyPart.STOMACH)).append(getNiceString(BodyPart.GROIN)).append("\n");
        lol.append(getNiceString(BodyPart.LEFT_SHOULDER)).append(getNiceString(BodyPart.LEFT_ELBOW)).append(getNiceString(BodyPart.LEFT_FOREARM))
                .append(getNiceString(BodyPart.LEFT_HAND)).append("\n");
        lol.append(getNiceString(BodyPart.RIGHT_SHOULDER)).append(getNiceString(BodyPart.RIGHT_ELBOW)).append(getNiceString(BodyPart.RIGHT_FOREARM))
                .append(getNiceString(BodyPart.RIGHT_HAND)).append("\n");
        lol.append(getNiceString(BodyPart.LEFT_HIP)).append(getNiceString(BodyPart.LEFT_KNEE)).append(getNiceString(BodyPart.LEFT_SHIN))
                .append(getNiceString(BodyPart.LEFT_FOOT)).append("\n");
        lol.append(getNiceString(BodyPart.RIGHT_HIP)).append(getNiceString(BodyPart.RIGHT_KNEE)).append(getNiceString(BodyPart.RIGHT_SHIN))
                .append(getNiceString(BodyPart.RIGHT_FOOT)).append("\n");
        return lol.toString();
    }

    public String getNiceString(BodyPart part) {
        ArmorPiece piece = getArmorPiece(part);
        if (piece == null) return "§f["+ part.getName() +"] ";
        return piece.toNiceString() + " ";
    }
}
