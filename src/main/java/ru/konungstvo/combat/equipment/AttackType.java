package ru.konungstvo.combat.equipment;

import java.io.Serializable;

public enum AttackType implements Serializable {
    PHYSIC("Физическая атака"),
    PSYCHIC("Психическая атака"),
    COMBINED("Физическая-психическая атака")
    ;

    private String name;

    AttackType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}
