package ru.konungstvo.combat.equipment;
import net.minecraft.nbt.NBTTagCompound;
import org.json.simple.parser.ParseException;

public abstract class Equipment {
    private String name;

    public Equipment(String name) {
        this.name = name;
    }


    //public abstract void fillFromNBT(NBTTagCompound nbtTagCompound) throws ParseException;
    @Deprecated
    public abstract void fillFromJson(String jsonStr) throws ParseException;
    public abstract int getDamage();

    public abstract String getSkill();

    public abstract String getDamageString();

    public abstract String getPsychDamage();

    public abstract int getConvenience();
    public abstract double getReach();

    @Override
    public String toString() {
        return "Equipment{" +
                "name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
     public boolean isRanged() { return false; }

}
