package ru.konungstvo.combat.duel;

import de.cas_ual_ty.gci.GunCus;
import de.cas_ual_ty.gci.item.ItemGun;
import de.cas_ual_ty.gci.network.MessageSound;
import de.cas_ual_ty.gci.network.MessageSoundReg;
import org.apache.commons.lang3.tuple.MutablePair;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import noppes.npcs.entity.EntityNPCInterface;
import org.json.simple.parser.ParseException;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.FudgeDiceMessage;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.combat.StatusEnd;
import ru.konungstvo.combat.StatusType;
import ru.konungstvo.combat.Trait;
import ru.konungstvo.combat.dice.modificator.Modificator;
import ru.konungstvo.combat.dice.modificator.ModificatorType;
import ru.konungstvo.combat.equipment.*;
import ru.konungstvo.combat.movement.ChargeHistory;
import ru.konungstvo.combat.movement.MovementHistory;
import ru.konungstvo.commands.helpercommands.player.defense.SelectDefenseCommand;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.exceptions.CombatException;
import ru.konungstvo.exceptions.DataException;
import ru.konungstvo.kmrp_lore.helpers.WeaponDurabilityHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.ModifiersState;
import ru.konungstvo.player.Player;

import javax.script.ScriptException;
import java.io.IOException;
import java.util.*;

public class Duel {
    private Player attacker, defender;
    private LinkedList<Attack> attacks;
    public Attack originalAttack;
    private Defense defense = new Defense();
    private int activeAttackNumber = 0;
    private boolean initiated = false;
    private boolean ended;
    private int activeHand = 1; // 0 left hand, 1 right hand, 2 both
    private int damage = -1;
    private boolean isAboutToClack = false;
    public boolean successfulCounter = false;
    public boolean unsuccessfulCounter = false;
    private boolean splash = false;
    private boolean piercing = false;
    private boolean multishot = false;

    private int multicast = -666;
    private int fullcover = 0;
    private boolean isTargeted = false;
    private int targetedPartInt = 2;
    private boolean shouldBreak = false;
    private boolean shouldNotBreak = false;
    public boolean mustBreak = false;
    public boolean mustEnd = false;

    public boolean noRecoil = false;

    private boolean reachedDifficulty = false;
    private boolean hasCharged = false;
    private boolean riposte = false;
    public boolean suppressing = false;
    private boolean jammed = false;

    private FudgeDiceMessage oldAttackResult = null; //TODO: костыль пиздец
    private FudgeDiceMessage oldDefenseResult = null;

    private Range range = Range.NORMAL;

    private boolean stunOp = false; //для оппорты
    private boolean isOpportunity = false; //для оппорты

    private Player beingDefended = null;
    private boolean defended = false;
    private Duel nextDuel = null;

    private ArrayList<String> doAfterAttack = new ArrayList<String>();

    private List<MutablePair<String, Integer>> cooldowns = new ArrayList<>();


    //private HashMap<Integer, String> wounds = new HashMap<>();
    private List<MutablePair<Integer, String>> wounds = new ArrayList<>();

    private String dicesMegaString = "";

    public Message getDamageInfo() {
        if (damageInfo == null) {
            System.out.println("DamageInfo is null for some reason!");
        }
        return damageInfo;
    }

    public void setDamageInfo(Message damageInfo) {
        this.damageInfo = damageInfo;
    }

    private Message damageInfo = null;
    private Message rangeInfo = null;
    private boolean defenseFromFirearmDifficulty = false;

    public Duel(Player attacker) {
        this.attacker = attacker;
        this.attacks = new LinkedList<>();
    }

    public void addAttack(Attack attack) {
        System.out.println("Adding attack " + attack.toString());
        if (attack.getWeapon().isRanged() && !attack.getWeapon().isLoaded()) {
            System.out.println("Adding some empty fucker");
            isAboutToClack = true;
        }
        attacks.add(attack);
    }


    public void performDefense(Attack currentAttack) throws IOException, ParseException {
        if (successfulCounter) {
            System.out.println("DEFENDER " + defender.getName());
            try {
                defense.setDiceMessage(new SkillDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange(), defender.getPreviousDefenses()));
            } catch (Exception e) {
                defense.setDiceMessage(new FudgeDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange()));
            }
//            try {
//                currentAttack.setDiceMessage(new SkillDiceMessage(currentAttack.getDiceMessage().getPlayerName(), currentAttack.getDiceMessage().getContent(), currentAttack.getDiceMessage().getRange(), attacker.getPreviousDefenses()));
//            } catch (Exception e) {
//                currentAttack.setDiceMessage(new FudgeDiceMessage(currentAttack.getDiceMessage().getPlayerName(), currentAttack.getDiceMessage().getContent(), currentAttack.getDiceMessage().getRange()));
//            }


        }
        System.out.println("currentAttack is dice cast" + currentAttack.getDiceMessage().getDice().isAlreadyCast());
        System.out.println("currentAttack is built " + currentAttack.getDiceMessage().isBuilt());
        int diff = 0;
        if (multishot) {
            EntityLivingBase forgePlayer = (EntityLivingBase) DataHolder.inst().getNpcEntity(attacker.getName());
            if (!DataHolder.inst().isNpc(attacker.getName())) {
                forgePlayer = ServerProxy.getForgePlayer(attacker.getName());
            }

            WeaponTagsHandler weaponTagsHandler = null;
            Weapon weapon = null;
            ItemStack activeItem = null;
            if (activeHand == 0) {
                activeItem = forgePlayer.getHeldItemOffhand();
            } else if (activeHand == 1) {
                activeItem = forgePlayer.getHeldItemMainhand();
            }
            weaponTagsHandler = new WeaponTagsHandler(activeItem);
            weapon = new Weapon(weaponTagsHandler.getDefaultWeaponName(), weaponTagsHandler.getDefaultWeapon(), weaponTagsHandler);
            currentAttack.setWeapon(weapon);
            if (!currentAttack.getWeapon().isLoaded()) isAboutToClack = true;
        }


//        Message success = new Message();
        DuelCalculator.Damage damage = new DuelCalculator.Damage();

        range = attacker.getDefaultRange();

        try {
            if (activeAttackNumber > 0) {
                currentAttack.checkAmmo();
            }
        } catch (Exception ignored) {

        }
        try {
            if (currentAttack.weapon.isRanged()) {
                if (currentAttack.getWeapon().getFirearm().isTwoHanded()) {
                    attacker.getPercentDice().checkAndGetAndSetLimbInjurty(2, attacker.getName());
                } else {
                    attacker.getPercentDice().checkAndGetAndSetLimbInjurty(getActualActiveHand(), attacker.getName());
                }
            } else if (currentAttack.weapon.isMelee()) {
                if (currentAttack.getWeapon().getMelee().isTwohanded()) {
                    attacker.getPercentDice().checkAndGetAndSetLimbInjurty(2, attacker.getName());
                } else {
                    attacker.getPercentDice().checkAndGetAndSetLimbInjurty(getActualActiveHand(), attacker.getName());
                }
            } else {
                attacker.getPercentDice().checkAndGetAndSetLimbInjurty(getActualActiveHand(), attacker.getName());
            }
        } catch (Exception ignored) {
            attacker.getPercentDice().checkAndGetAndSetLimbInjurty(getActualActiveHand(), attacker.getName());
        }
        double distance = ServerProxy.getDistanceBetween(attacker, defender);
        boolean stock = false;
        boolean forceShield = false;
        boolean backstab = false;

        boolean attackerlying = attacker.isLying();

        EntityLivingBase attackerEntity;
        if (DataHolder.inst().isNpc(attacker.getName())) {
            attackerEntity = DataHolder.inst().getNpcEntity(attacker.getName());
        } else {
            attackerEntity = ServerProxy.getForgePlayer(attacker.getName());
        }

        EntityLivingBase defenderEntity;
        if (DataHolder.inst().isNpc(defender.getName())) {
            defenderEntity = DataHolder.inst().getNpcEntity(defender.getName());
        } else {
            defenderEntity = ServerProxy.getForgePlayer(defender.getName());
        }

        if (true) {
            System.out.println("test123");
            Vec3d vector = attackerEntity.getPositionVector().subtract(defenderEntity.getPositionVector());
            Vec3d playerDirection = defenderEntity.getLookVec();
            double angleDir = (Math.atan2(vector.z, vector.x) / 2 / Math.PI * 360 + 360) % 360;
            double angleLook = (Math.atan2(playerDirection.z, playerDirection.x) / 2 / Math.PI * 360 + 360) % 360;
            double angle = (angleDir - angleLook + 360) % 360;
            System.out.println("angle: " + angle);
//            if ((angle <= 67.5 || angle >= 292.5) && defender.hasDefensiveStance()) {
//                forceShield = true;
//            } else
            if (angle <= 247.5 && angle >= 112.5) {
                System.out.println("test321");
                currentAttack.getDiceMessage().getDice().addMod(new Modificator((attacker.hasTrait(Trait.BACKSTABBER) ? 2 : 1), ModificatorType.BACKSTAB));
            }
        }

        if (getActiveHand() == 2) {
            switch (attacker.getTraitLevel(Trait.PAIRED)) {
                case 1:
                    currentAttack.getDiceMessage().getDice().mods.add(new Modificator(-1, ModificatorType.PAIRED));
                    break;
                case 2:
                case 3:
                    break;
                default:
                    currentAttack.getDiceMessage().getDice().mods.add(new Modificator(-2, ModificatorType.PAIRED));
            }
        }

        if (!currentAttack.getDiceMessage().getDice().mods.hasMod(ModificatorType.WEIGHT) && attacker.getWeight() > attacker.getMaxWeight()) {
            int w = attacker.getWeight();
            int debuff = 0;
            while (w > attacker.getMaxWeight()) {
                debuff--;
                w -= 5;
            }
            currentAttack.getDiceMessage().getDice().addMod(new Modificator(debuff, ModificatorType.WEIGHT));
        }

        if (!currentAttack.getDiceMessage().getDice().mods.hasMod(ModificatorType.PSY_WEIGHT) && attacker.getPsychweight() > attacker.getMaxPsychweight()) {
            int w = attacker.getPsychweight();
            int debuff = 0;
            while (w > attacker.getMaxPsychweight()) {
                debuff--;
                w -= 5;
            }
            currentAttack.getDiceMessage().getDice().addMod(new Modificator(debuff, ModificatorType.PSY_WEIGHT));
        }

        double c = distance;
        double a = attackerEntity.posY - defenderEntity.posY;
        if (a > 0) {
            double angle = 0;
            angle = Math.sin(a/c);
            System.out.println("Высота " + angle);
            if (angle * 100 >= 30) currentAttack.getDiceMessage().getDice().getPercentDice().setHighground(10);

        } else if (defense.getDefenseType() == DefenseType.COUNTER || defense.getDefenseType() == DefenseType.ANTICOUNTER) {
            a = defenderEntity.posY - attackerEntity.posY;
            double angle = 0;
            angle = Math.sin(a/c);
            if (angle * 100 >= 30) defense.getDiceMessage().getDice().getPercentDice().setHighground(10);
        }

        if (attacker.getEngagedWith() != defender && attacker.getEngagedWithFading() != defender) {
            double defenderReach = 0.75;
            ItemStack lefthand = defenderEntity.getHeldItemOffhand();
            ItemStack righthand = defenderEntity.getHeldItemMainhand();
            if (!lefthand.isEmpty()) {
                WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(lefthand);
                if (weaponTagsHandler.isWeapon()) {
                    Weapon weapon = new Weapon(weaponTagsHandler.getDefaultWeaponName(), weaponTagsHandler.getDefaultWeapon(), weaponTagsHandler);
                    defenderReach = Math.max(defenderReach, weapon.getReach());
                }
            }
            if (!righthand.isEmpty()) {
                WeaponTagsHandler weaponTagsHandler = new WeaponTagsHandler(righthand);
                if (weaponTagsHandler.isWeapon()) {
                    Weapon weapon = new Weapon(weaponTagsHandler.getDefaultWeaponName(), weaponTagsHandler.getDefaultWeapon(), weaponTagsHandler);
                    defenderReach = Math.max(defenderReach, weapon.getReach());
                }
            }

            if (currentAttack.weapon.getReach() < defenderReach && (currentAttack.getWeapon().isMelee() || currentAttack.getWeapon().isFist())) {
                defense.getDiceMessage().getDice().addMod(new Modificator((int) (defenderReach - currentAttack.weapon.getReach()), ModificatorType.REACH_ADVANTAGE));
            } else if (currentAttack.getWeapon().isRanged()) {
                if (!currentAttack.getWeapon().getFirearm().getFirearmType().equals(FirearmType.ONEHANDED)) {
                    if (distance <= defenderReach || distance <= 2) currentAttack.getDiceMessage().getDice().addMod(
                            new Modificator(-1, ModificatorType.POINT_BLANK)
                    );
                }
            }
        }

        if (activeHand == 0 && !attacker.hasTrait(Trait.LEFT_HANDED) && !attacker.hasTrait(Trait.AMBIDEXTROUS)) {
            currentAttack.getDiceMessage().getDice().addMod(new Modificator(-2, ModificatorType.OFF_HAND));
        } else if (activeHand == 1 && attacker.hasTrait(Trait.LEFT_HANDED) && !attacker.hasTrait(Trait.AMBIDEXTROUS)) {
            currentAttack.getDiceMessage().getDice().addMod(new Modificator(-2, ModificatorType.OFF_HAND));
        }

        if (currentAttack.weapon.isRanged()) {
            if (currentAttack.weapon.getFirearm().isModern() && attacker.getItemForHand(getActualActiveHand()).getItem() instanceof ItemGun) {
                try {
                    ItemStack is = attacker.getItemForHand(getActualActiveHand());
                    ItemGun gun = (ItemGun) is.getItem();
                    System.out.println("hasStockOptions " + gun.hasStockOptions());
                    if (gun.hasStockOptions()) {
                        boolean needStock = currentAttack.weapon.getFirearm().needStock();
                        boolean hasStock = gun.hasStock(is);
                        System.out.println("needStock " + needStock + " " + "hasStock " + hasStock);
                        if (hasStock) {
                            if (!needStock) {
//                                double distance = ServerProxy.getDistanceBetween(attacker, defender);
                                if (distance <= 5) attacker.getPercentDice().setStock(-10);
                                if (distance >= 10) attacker.getPercentDice().setStock(10);
                                stock = true;
                            }
                        } else {
                            if (needStock) {
//                                double distance = ServerProxy.getDistanceBetween(attacker, defender);
                                if (distance <= 5) attacker.getPercentDice().setStock(10);
                                if (distance >= 10) attacker.getPercentDice().setStock(-10);
                                stock = true;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        if (splash || multicast > 0 || multishot || defended) {
            try {
                currentAttack.setDiceMessage(new SkillDiceMessage(currentAttack.getDiceMessage().getPlayerName(), currentAttack.getDiceMessage().getContent(), currentAttack.getDiceMessage().getRange()));
            } catch (DataException e) {
                currentAttack.setDiceMessage(new FudgeDiceMessage(currentAttack.getDiceMessage().getPlayerName(), currentAttack.getDiceMessage().getContent(), currentAttack.getDiceMessage().getRange()));
            }
        }

        if (attacker.riposted == defender) {
            attacker.riposted = null;
            //currentAttack.getDiceMessage().getDice().addMod(new Modificator(1, ModificatorType.RIPOSTE));
            if (!attacker.hasCourage(currentAttack.getDiceMessage().getDice().getSkillName())) {
                attacker.getPercentDice().setRipost(50);
            }
        }

        if (defense.getDefenseType().equals(DefenseType.NOTHING)) defense.setDiceMessage(new FudgeDiceMessage(defender.getName(), "% 0"));

        //currentAttack.getDiceMessage().getDice().cast();

        damageInfo = null;
        originalAttack = currentAttack;
        originalAttack = currentAttack;
        attacker.setHasAttacked(true);

        if (!initiated) {
            System.out.println("Duel not initiated, returning…");
            return;
        }


        // HERE IS ALL THE MAGIC!
        int rangedResult = -666;
        int attackDifficulty = -666;
        int globalAttackDifficulty = -666;;
        // RECOIL AND CLACK
        int cost = -1; //for literally magic
        int cooldown = -1;
        //boolean crawling = false;
        boolean lying = defender.isLying();
        boolean magic;
        WeaponDurabilityHandler weaponDurabilityHandler = null;
        weaponDurabilityHandler = attacker.getDurHandlerForHand(getActualActiveHand());
        //String[] modsplus = null;
        //String modsplus = "";
        List<String> modsplus = new ArrayList<>();

        if(activeAttackNumber > 0) {
            if (!currentAttack.getDiceMessage().getDice().getSkillName().isEmpty()) attacker.getPercentDice().setSkill(attacker.getSkill(currentAttack.getDiceMessage().getDice().getSkillName()).getPercent()); //Тупой баг, при очередях не подцепляется кастомный дебафф
            if (defense.getDefenseType() == DefenseType.CONSTITUTION) {
                try {
                    defense.setDiceMessage(new SkillDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange(), 0));
                } catch (Exception e) {
                    defense.setDiceMessage(new FudgeDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange()));
                }
            } else if (defense.getDefenseType() != DefenseType.NOTHING) {
                try {
                    defense.setDiceMessage(new SkillDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange(), defender.getPreviousDefenses()));
                } catch (Exception e) {
                    defense.setDiceMessage(new FudgeDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange()));
                }
            }
        }

        if (currentAttack.getWeapon().isRanged()) {
            if (weaponDurabilityHandler.hasDurabilityForTag(currentAttack.getWeapon().getName())) {
                double ratio = weaponDurabilityHandler.getPercentageRatioForTag(currentAttack.getWeapon().getName());
                if (ratio <= 0) {
                    Message clack = new Message("Клац! Похоже, оружие заклинило…", ChatColor.RED);
                    if (activeAttackNumber == 0) noRecoil = true;
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(attacker, defender, range, clack);
                    //ServerProxy.informDistantMasters(attacker, clack, range.getDistance());
                    jammed = true;
                    shouldBreak = true;
                    mustBreak = true;
                    GunCus.channel.sendToAllAround(new MessageSound(attackerEntity, GunCus.SOUND_KLIN, 3F, 1F), new NetworkRegistry.TargetPoint(attackerEntity.world.provider.getDimension(), attackerEntity.posX, attackerEntity.posY, attackerEntity.posZ, 16F));
                    return;
                } else if (ratio <= 25) {
                    Random random = new Random();
                    int randomResult = random.nextInt(100) + 1;
                    System.out.println(randomResult);
                    if (randomResult <= (26 - ratio)) {
                        Message clack = new Message("Клац! Похоже, оружие заклинило…", ChatColor.RED);
                        if (activeAttackNumber == 0) noRecoil = true;
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(attacker, defender, range, clack);
                        //ServerProxy.informDistantMasters(attacker, clack, range.getDistance());
                        jammed = true;
                        shouldBreak = true;
                        mustBreak = true;
                        GunCus.channel.sendToAllAround(new MessageSound(attackerEntity, GunCus.SOUND_KLIN, 3F, 1F), new NetworkRegistry.TargetPoint(attackerEntity.world.provider.getDimension(), attackerEntity.posX, attackerEntity.posY, attackerEntity.posZ, 16F));
                        return;
                    }
                }
            }

            if(currentAttack.getWeapon().getFirearm().getHomemade() > 0) {
                Random random = new Random();
                if(currentAttack.getWeapon().getFirearm().getHomemade() >= (random.nextInt(100) + 1)) {
                    Message clack = new Message("Клац! Похоже, оружие заклинило…", ChatColor.RED);
                    if (activeAttackNumber == 0) noRecoil = true;
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(attacker, defender, range, clack);
                    //ServerProxy.informDistantMasters(attacker, clack, range.getDistance());
                    jammed = true;
                    shouldBreak = true;
                    mustBreak = true;
                    GunCus.channel.sendToAllAround(new MessageSound(attackerEntity, GunCus.SOUND_KLIN, 3F, 1F), new NetworkRegistry.TargetPoint(attackerEntity.world.provider.getDimension(), attackerEntity.posX, attackerEntity.posY, attackerEntity.posZ, 16F));
                    return;
                }
            }

            boolean stillLoaded;
            if (currentAttack.weapon.isLoaded()) {
                if (!currentAttack.weapon.getFirearm().getProjectile().isInfinite()) { //не тратим снаряд для холодного и магии, пусть игрок сам следит
                    stillLoaded = attacker.fireProjectile(getActualActiveHand());
                } else {
                    stillLoaded = currentAttack.weapon.isLoaded();
                }
            } else {
               // if (activeHand == 2 && activeAttackNumber == 1) isAboutToClack = true; //не факт нужно ли но костыль. Чтобы второе оружие не стреляло если парная и второе оружие не заряжено но хз надо ли
                stillLoaded = currentAttack.weapon.isLoaded();
            }

            try {
                modsplus = Arrays.asList(currentAttack.getWeapon().getFirearm().getLoadedProjectile().getModsplus().split(","));
            } catch (Exception ignored) {

            }

            cost = currentAttack.weapon.getFirearm().getProjectile().getCost();
            if (multicast > 1) cost = (int) Math.ceil(cost/2);
            cooldown = currentAttack.weapon.getFirearm().getProjectile().getCooldown();

            //if (successfulCounter) defender.fireProjectile(getActiveHand());
            System.out.println("isAboutToClack: " + isAboutToClack);
            System.out.println("getActiveHand: " + getActiveHand());
            System.out.println("getActiveAttackNumber: " + getActiveAttackNumber());
            /*CLACK*/
            if (isAboutToClack) {
                System.out.println("clacking so fucking hard");
                Message clack = new Message("Клац! Похоже, оружие разряжено…", ChatColor.RED);
                if (activeAttackNumber == 0) noRecoil = true;
//                attacker.sendMessage(clack);
                ServerProxy.sendMessageFromTwoSourcesAndInformMasters(attacker, defender, range, clack);
                //ServerProxy.informDistantMasters(attacker, clack, range.getDistance());
                shouldBreak = true;
                mustBreak = true;
                isAboutToClack = false;
                GunCus.channel.sendToAllAround(new MessageSound(attackerEntity, GunCus.SOUND_KLIN, 4F, 1F), new NetworkRegistry.TargetPoint(attackerEntity.world.provider.getDimension(), attackerEntity.posX, attackerEntity.posY, attackerEntity.posZ, 16F));
                return;
            }

            if (currentAttack.weapon.getFirearm().getProjectile().needPrepare()) {
                if(!attacker.hasPrepared(currentAttack.weapon.getName())) {
                    System.out.println(currentAttack.weapon.getName());
                    modsplus = new ArrayList<>();
                    Message clack = new Message("Пуф! Ничего не произошло… (не подготовлено)", ChatColor.RED);
                    if (activeAttackNumber == 0) noRecoil = true;
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(attacker, defender, range, clack);
                    //ServerProxy.informDistantMasters(attacker, clack, range.getDistance());
                    shouldBreak = true;
                    mustBreak = true;
                    isAboutToClack = false;
                    return;
                }
            }

            if (attacker.onCooldown(currentAttack.weapon.getName())) {
                Message clack = new Message("Пуф! Ничего не произошло… (на восстановлении)", ChatColor.RED);
                if (activeAttackNumber == 1) noRecoil = true;
                ServerProxy.sendMessageFromTwoSourcesAndInformMasters(attacker, defender, range, clack);
                //ServerProxy.informDistantMasters(attacker, clack, range.getDistance());
                shouldBreak = true;
                mustBreak = true;
                isAboutToClack = false;
                return;
            }

            if (cost >= 0) {
                if(!DataHolder.inst().useMana(attacker.getName(), cost)) { //TODO Сделать только для магии
                    modsplus = new ArrayList<>();
                    if(currentAttack.weapon.getFirearm().getProjectile().getCaliber().equals("магия")) {
                        Message clack = new Message("Пуф! Ничего не произошло… (недостаточно маны)", ChatColor.RED);
                        if (activeAttackNumber == 0) noRecoil = true;
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(attacker, defender, range, clack);
                        //ServerProxy.informDistantMasters(attacker, clack, range.getDistance());
                        shouldBreak = true;
                        mustBreak = true;
                        isAboutToClack = false;
                        return;
                    }
                }
            }

            if(currentAttack.getWeapon().getFirearm().getLoadedProjectile().getHomemade() > 0) {
                Random random = new Random();
                if(currentAttack.getWeapon().getFirearm().getLoadedProjectile().getHomemade() >= (random.nextInt(100) + 1)) {
                    Message clack = new Message("Клац! Похоже, оружие заклинило…", ChatColor.RED);
                    if (activeAttackNumber == 0) noRecoil = true;
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(attacker, defender, range, clack);
                    //ServerProxy.informDistantMasters(attacker, clack, range.getDistance());
                    shouldBreak = true;
                    mustBreak = true;
                    GunCus.channel.sendToAllAround(new MessageSound(attackerEntity, GunCus.SOUND_KLIN, 3F, 1F), new NetworkRegistry.TargetPoint(attackerEntity.world.provider.getDimension(), attackerEntity.posX, attackerEntity.posY, attackerEntity.posZ, 16F));
                    return;
                }
            }

            System.out.println("1: " + currentAttack.getDiceMessage().toString());
            if (activeAttackNumber < 2) {
                if (weaponDurabilityHandler.hasDurabilityForTag(currentAttack.getWeapon().getName()))
                    weaponDurabilityHandler.takeAwayDurabilityForTag(currentAttack.getWeapon().getName(), 1);
            }
            /*RECOIL*/
            if (true) {
                int recoil = Math.abs(attacker.getRecoil());
                /*STATIONARY*/
                if (attacker.getModifiersState().getModifier(ModifiersState.stationary) != null) {
                    if (attacker.getModifiersState().getModifier(ModifiersState.stationary) == 1) {
                        recoil -= 1;
                        System.out.println("recoil reduced");
                    }
                }
                /*ЛЕЖАНИЕ*/
                if (attackerlying) {
                    recoil -= 1;
                    System.out.println("recoil reduced");
                }

                if (currentAttack.getWeapon().getFirearm().getWeaponType().equals("пулемёт")) {
                    if (currentAttack.getWeapon().getFirearm().getSort() == 2) {
                        if(!attackerlying && !attacker.getModifiersState().isModifier(ModifiersState.stationary) && attacker.getModifiersState().isModifier(ModifiersState.bipod)) {
                            recoil -= 1;
                            System.out.println("recoil reduced");
                        }
                    } else if (currentAttack.getWeapon().getFirearm().getSort() == 3) {
                        if(!attacker.getModifiersState().isModifier(ModifiersState.stationary)) {
                            recoil += 2;
                            System.out.println("recoil increased");
                        }
                    }
                }

                if (recoil > 0)
                    currentAttack.getDiceMessage().getDice().addMod(
                        new Modificator(-recoil, ModificatorType.RECOIL)
                    );
                System.out.println("2: " + currentAttack.getDiceMessage().toString());

                //attacker.setRecoil(0);
            }


// КЛАЦ НА БУДУЩЕЕ + трата патронов
            // TODO Counter

            if (!stillLoaded && activeHand != 2) isAboutToClack = true; //вторая проверка нужна иначе парное ломается

//            rangedResult = currentAttack.getDiceMessage().getDice().getResult();
//            double distance = ServerProxy.getDistanceBetween(attacker, defender);

            // Get distance from distance tables
            int rangeStep = 10;
            int rangeProjMod = 0;
            try {
                MutablePair<Integer, String> rangeStepPair = DataHolder.rollMegaDice(currentAttack.getWeapon().getFirearm().getRange(), "§7Шаг дальности");
                rangeStep = (int) rangeStepPair.left;
                dicesMegaString += (String) rangeStepPair.right;
                MutablePair<Integer, String> rangeProjModPair = DataHolder.rollMegaDice(currentAttack.getWeapon().getFirearm().getLoadedProjectile().getRangemod(), "§7Модификатор шага дальности снаряда");
                rangeProjMod = (int) rangeProjModPair.left;
                dicesMegaString += (String) rangeProjModPair.right;
            } catch (ScriptException se) {
                se.printStackTrace();
            }
            rangeStep += rangeProjMod;
//            System.out.println("\'" + firearmName + "\'");

//            if (currentAttack.weapon.getFirearm().isBullet()) {
//                firearmName = firearmName.replace("дробь/", "");
//                System.out.println("!: " + firearmName);
//
//            }

            /* БЕРЁМ ДАЛЬНОСТЬ ДЛЯ ОГНЕСТРЕЛА */
            //FirearmDistance fd = FirearmDistance.getByName(firearmName);
//            if (fd == null) {
//                Message error = new Message("Ошибка! Не найдена дистанция: " + firearmName, ChatColor.RED);
//                attacker.sendMessage(error);
//                return;
//            }

            int rangeDebuff = (int) Math.floor(distance / rangeStep);
            if (currentAttack.getWeapon().getFirearm().getFirearmType().equals(FirearmType.WHIMSY)) {
                rangeDebuff = Math.abs(rangeDebuff-1);
            }
            if (rangeDebuff > 0) currentAttack.getDiceMessage().getDice().addMod(new Modificator(-rangeDebuff, ModificatorType.RANGE_TOO_FAR));


//            attackDifficulty = fd.getAttackDifficulty(distance);
//            int falloff = fd.getFalloff(currentAttack.getWeapon().getFirearm().getLoadedProjectile().getCaliber(), attackDifficulty);
//            if (falloff != 0) damage.setFalloff(-falloff, "излёт");

            String diffstr = "";
            boolean usedCover = false;
            Integer cover = null;
            if (defender.getModifiersState().getModifier(ModifiersState.cover) != null)
                cover = defender.getModifiersState().getModifier(ModifiersState.cover);
            if (cover != null) {
                if (cover != -666) {
//                    attackDifficulty += 1;
//                    usedCover = true;
//                    diffstr += "§c+1 Укрытие\n";
                    if (!defense.getDefenseType().equals(DefenseType.NOTHING)) defense.getDiceMessage().getDice().addMod(new Modificator(1, ModificatorType.COVER));
                }
//                damage.setCover(-cover, "укрытие");
            }



//            boolean usedOptic = false;
//            Integer optic = null;
////            if (attacker.getModifiersState().isModifier(ModifiersState.optic)) {
////                    //if (attacker.getMovementHistory() != MovementHistory.NONE && attacker.getMovementHistory() != MovementHistory.LAST_TIME) currentAttack.getDiceMessage().getDice().addMod(new Modificator(-1, ModificatorType.OFFHAND));
////                    if (distance >= 20) {
////                        attackDifficulty -= 1;
////                        usedOptic = true;
////                        diffstr += "§2-1 Прицеливание\n";
////                    } else if (distance <= 10) {
////                        currentAttack.getDiceMessage().getDice().addMod(new Modificator(-1, ModificatorType.OFFHAND));
////                    }
////            }

            if (defender.isBig()) {
                attackDifficulty -= 1;
                diffstr += "§2-1 Гигант\n";
            } else if (defender.isSmall()) {
                attackDifficulty += 1;
                diffstr += "§c+1 Кроха\n";
            }

            boolean running = false;
            if (defender.getMovementHistory() == MovementHistory.NOW) {
                attackDifficulty += 1;
                running = true;
                diffstr += "§c+1 По бегущему\n";
            } else {
                try {
                    EntityLivingBase rider = null;
                    if (DataHolder.inst().isNpc(defender.getName())) {
                        rider = DataHolder.inst().getNpcEntity(defender.getName());
                    } else {
                        rider = ServerProxy.getForgePlayer(defender.getName());
                    }
                    if (rider != null && rider.getRidingEntity() != null) {
                        Entity mount = rider.getLowestRidingEntity();
                        Player mountPlayer = null;
                        if(mount instanceof EntityNPCInterface) {
                            EntityNPCInterface npc = (EntityNPCInterface) mount;
                            mountPlayer = DataHolder.inst().getPlayer(npc.wrappedNPC.getName());
                            if (mountPlayer.getMovementHistory() == MovementHistory.NOW) {
                                attackDifficulty += 1;
                                running = true;
                                diffstr += "§c+1 По бегущему\n";
                            }
                        } else if (mount instanceof EntityPlayerMP) {
                            EntityPlayerMP playerMP = (EntityPlayerMP) mount;
                            Player player = DataHolder.inst().getPlayer(playerMP.getName());
                            if (player.getMovementHistory() == MovementHistory.NOW) {
                                attackDifficulty += 1;
                                running = true;
                                diffstr += " §c+1 По бегущему\n";
                            }
                        }
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }

            if (lying) {
                if (distance > 20) {
                    attackDifficulty += 1;
                    diffstr += "§c+1 По лежащему\n";
                } else if (distance < 10) {
                    if (defense.getDefenseType() != DefenseType.NOTHING && defense.getDefenseType() != DefenseType.CONSTITUTION) defense.getDiceMessage().getDice().addMod(new Modificator(-1, ModificatorType.LYING));
                }
            }

            if (attacker.getModifiersState().isModifier("suppressing")) {
                if (attacks.size() > 2) {
                    suppressing = true;
                } else {
                    damageInfo = new Message("Подавление неудачно!", ChatColor.COMBAT);
                    shouldBreak = true;
                    return;
                }
            }

            if (suppressing) {
                attackDifficulty -= 1;
                diffstr += "§2-1 Подавление\n";
            }


            //СОШКИ
//            if (attacker.getModifiersState().getModifier(ModifiersState.bipod) != null && attacker.getModifiersState().getModifier(ModifiersState.bipod) == 1) {
//                attacker.getPercentDice().setBipod(20);
//            }



            /* Штраф от конечностей */
            //TODO: переписать
            //


            if(attacker.getMovementHistory() == MovementHistory.NOW) {
                currentAttack.getDiceMessage().getDice().addMod(new Modificator(-1, ModificatorType.RUNNING));
            }
            /* СТРЕЛЬБА ВБЛИЗИ */
            if (distance <= 1.8 && defense.getDefenseType() != DefenseType.PARRY && defense.getDefenseType() != DefenseType.COUNTER && defense.getDefenseType() != DefenseType.ANTICOUNTER && defense.getDefenseType() != DefenseType.HAND_TO_HAND && !(currentAttack.weapon.getFirearm().isPistolOrSawoff() && !stock)) {
                //currentAttack.getDiceMessage().getDice().addMod(new Modificator(-1, ModificatorType.POINT_BLANK));
            }
            rangeInfo = new Message("Дальность была " + Math.round(distance * 100.) / 100. + ", шаг ", ChatColor.COMBAT);
            MessageComponent rangeComp = new MessageComponent(Integer.toString(rangeStep), ChatColor.COMBAT);
            if (!diffstr.isEmpty()) {
                rangeComp.setUnderlined(true);
                diffstr = diffstr.substring(0, diffstr.length() - 1);
                rangeComp.setHoverText(new TextComponentString(diffstr));
            }
            rangeInfo.addComponent(rangeComp);
            rangeInfo.addComponent(new MessageComponent(" (" + currentAttack.getWeapon().getFirearm().getFirearmType().getName().toLowerCase() + ")" , ChatColor.COMBAT));
            rangeInfo.addComponent(new MessageComponent(".", ChatColor.COMBAT));
            GunCus.channel.sendToAllAround(new MessageSound(attackerEntity, currentAttack.weapon.getSound() + "_shot", 6F, 1F), new NetworkRegistry.TargetPoint(attackerEntity.world.provider.getDimension(), attackerEntity.posX, attackerEntity.posY, attackerEntity.posZ, currentAttack.getWeapon().getFirearm().getSoundRange()));
//                masterInfo.addComponent(new MessageComponent("Сложность попадания увеличена на 1 (укрытие)", ChatColor.GRAY));
//            if (currentAttack.weapon.getFirearm().shouldBoom()) {
//                if (attacker.getItemForHand(getActualActiveHand()).getItem() instanceof ItemGun) {
//                    try {
//                        ItemStack is = attacker.getItemForHand(getActualActiveHand());
//                        ItemGun gun = (ItemGun) is.getItem();
//                        //System.out.println(gun.hasOptic(is));
//
//                        gun.doShoot(attackerEntity, is, false, false, currentAttack.weapon.getFirearm().getSort());
//
////                        if (gun.hasOptic(is) && !attacker.getModifiersState().isModifier(ModifiersState.optic)) {
////                            currentAttack.getDiceMessage().getDice().addMod(new Modificator(-1, ModificatorType.OFFHAND));
////                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                } else {
//                    switch (currentAttack.weapon.getFirearm().getSort()) {
//                        case 3:
//                            GunCus.channel.sendToAllAround(new MessageSound(attackerEntity, GunCus.SOUND_SHOOT_HEAVY, 8F, 1F), new NetworkRegistry.TargetPoint(attackerEntity.world.provider.getDimension(), attackerEntity.posX, attackerEntity.posY, attackerEntity.posZ, 150F));
//                            break;
//                        case 2:
//                            GunCus.channel.sendToAllAround(new MessageSound(attackerEntity, GunCus.SOUND_SHOOT_MEDIUM, 7F, 1F), new NetworkRegistry.TargetPoint(attackerEntity.world.provider.getDimension(), attackerEntity.posX, attackerEntity.posY, attackerEntity.posZ, 100F));
//                            break;
//                        case 1:
//                            GunCus.channel.sendToAllAround(new MessageSound(attackerEntity, GunCus.SOUND_SHOOT_LIGHT, 6F, 1F), new NetworkRegistry.TargetPoint(attackerEntity.world.provider.getDimension(), attackerEntity.posX, attackerEntity.posY, attackerEntity.posZ, 80F));
//                            break;
//                    }
//                }
//            }


            rangedResult = currentAttack.getDiceMessage().getDice().getResult();
            /* НЕ ХВАТАЕТ ДАЛЬНОСТИ */
            if (rangedResult < attackDifficulty) {
                damageInfo = new Message("Сложность попадания не достигнута!", ChatColor.COMBAT);
                shouldBreak = true;

                System.out.println("3: " + currentAttack.getDiceMessage().toString());
                currentAttack.getDiceMessage().build();
                System.out.println("4: " + currentAttack.getDiceMessage().toString());

                //звук промаха
                GunCus.channel.sendToAllAround(new MessageSound(defenderEntity, currentAttack.getWeapon().getSound() + "_miss", 2F, 1F), new NetworkRegistry.TargetPoint(defenderEntity.world.provider.getDimension(), defenderEntity.posX, defenderEntity.posY, defenderEntity.posZ, 16F));
//                if (currentAttack.getWeapon().getFirearm().shouldBoom()) GunCus.channel.sendToAllAround(new MessageSound(defenderEntity, GunCus.SOUND_BULLETCRACK, 2F, 1F), new NetworkRegistry.TargetPoint(defenderEntity.world.provider.getDimension(), defenderEntity.posX, defenderEntity.posY, defenderEntity.posZ, 16F));
//                else GunCus.channel.sendToAllAround(new MessageSound(defenderEntity, GunCus.SOUND_FLY, 2F, 1F), new NetworkRegistry.TargetPoint(defenderEntity.world.provider.getDimension(), defenderEntity.posX, defenderEntity.posY, defenderEntity.posZ, 16F));
                return;
            } else {
//                reachedDifficulty = true;
            }

//            if (attacker.getModifiersState().isModifier("suppressing")) {
//                if (attacks.size() > 2) {
//                    suppressing = true;
//                } else {
//                    damageInfo = new Message("Подавление неудачно!", ChatColor.COMBAT);
//                    shouldBreak = true;
//                    return;
//                }
//            }

            if (rangedResult < currentAttack.weapon.getFirearm().getProjectile().getDifficulty()) {
                    modsplus = new ArrayList<>();
                    damageInfo = new Message("Не достигнута сложность заклинания!", ChatColor.RED);
                    currentAttack.getDiceMessage().build();
                //ServerProxy.informDistantMasters(attacker, clack, range.getDistance());
                    shouldBreak = true;
                    //mustBreak = true;
                    isAboutToClack = false;
                    return;
            }
        }

        if (successfulCounter) {
            currentAttack.getDiceMessage().getDice().addAdvantage("Контратака");
        }

        if (defense.getDefenseType() == DefenseType.COUNTER && defense.getDefenseItem().isRanged()) {
            try {
                Weapon defweapon = (Weapon) defense.getDefenseItem();
                if (!((Weapon) defense.getDefenseItem()).isLoaded()) {
                    Message clack = new Message("Клац! Похоже, оружие разряжено… Попытка контратаки провалилась.", ChatColor.RED);
//                attacker.sendMessage(clack);
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(attacker, defender, range, clack);
                    //ServerProxy.informDistantMasters(attacker, clack, range.getDistance());
                    GunCus.channel.sendToAllAround(new MessageSound(attackerEntity, GunCus.SOUND_KLIN, 4F, 1F), new NetworkRegistry.TargetPoint(defenderEntity.world.provider.getDimension(), defenderEntity.posX, defenderEntity.posY, defenderEntity.posZ, 16F));
                    defense.setDefenseType(DefenseType.NOTHING);
                }
//                if (weaponDurabilityHandler.hasDurabilityForTag(defweapon.getName())) {
//                    double ratio = weaponDurabilityHandler.getPercentageRatioForTag(currentAttack.getWeapon().getName());
//                    if (ratio <= 0) {
//                        Message clack = new Message("Клац! Похоже, оружие заклинило… Попытка контратаки провалилась.", ChatColor.RED);
//                        if (activeAttackNumber == 0) noRecoil = true;
//                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(attacker, defender, range, clack);
//                        //ServerProxy.informDistantMasters(attacker, clack, range.getDistance());
//                        jammed = true;
//                        shouldBreak = true;
//                        mustBreak = true;
//                        GunCus.channel.sendToAllAround(new MessageSound(attackerEntity, GunCus.SOUND_KLIN, 3F, 1F), new NetworkRegistry.TargetPoint(attackerEntity.world.provider.getDimension(), attackerEntity.posX, attackerEntity.posY, attackerEntity.posZ, 16F));
//                        return;
//                    } else if (ratio <= 25) {
//                        Random random = new Random();
//                        int randomResult = random.nextInt(100) + 1;
//                        System.out.println(randomResult);
//                        if (randomResult <= (26 - ratio)) {
//                            Message clack = new Message("Клац! Похоже, оружие заклинило… Попытка контратаки провалилась.", ChatColor.RED);
//                            if (activeAttackNumber == 0) noRecoil = true;
//                            ServerProxy.sendMessageFromTwoSourcesAndInformMasters(attacker, defender, range, clack);
//                            //ServerProxy.informDistantMasters(attacker, clack, range.getDistance());
//                            jammed = true;
//                            shouldBreak = true;
//                            mustBreak = true;
//                            GunCus.channel.sendToAllAround(new MessageSound(attackerEntity, GunCus.SOUND_KLIN, 3F, 1F), new NetworkRegistry.TargetPoint(attackerEntity.world.provider.getDimension(), attackerEntity.posX, attackerEntity.posY, attackerEntity.posZ, 16F));
//                            return;
//                        }
//                    }
//                }
//
//                if(currentAttack.getWeapon().getFirearm().getHomemade() > 0) {
//                    Random random = new Random();
//                    if(currentAttack.getWeapon().getFirearm().getHomemade() >= (random.nextInt(100) + 1)) {
//                        Message clack = new Message("Клац! Похоже, оружие заклинило… Попытка контратаки провалилась.", ChatColor.RED);
//                        if (activeAttackNumber == 0) noRecoil = true;
//                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(attacker, defender, range, clack);
//                        //ServerProxy.informDistantMasters(attacker, clack, range.getDistance());
//                        jammed = true;
//                        shouldBreak = true;
//                        mustBreak = true;
//                        GunCus.channel.sendToAllAround(new MessageSound(attackerEntity, GunCus.SOUND_KLIN, 3F, 1F), new NetworkRegistry.TargetPoint(attackerEntity.world.provider.getDimension(), attackerEntity.posX, attackerEntity.posY, attackerEntity.posZ, 16F));
//                        return;
//                    }
//                }
                defense.getDiceMessage().getDice().addMod(new Modificator(-Math.abs(defender.getRecoil()), ModificatorType.RECOIL));
            } catch (Exception ignored) {

            }
        }

        if (!currentAttack.getWeapon().isRanged() && currentAttack.getWeapon().getMelee() != null) {
            try {
                modsplus = Arrays.asList(currentAttack.getWeapon().getMelee().getModsplus().split(","));
            } catch (Exception ignored) {

            }
            if (currentAttack.weapon.getMelee().needPrepare()) {
                if(!attacker.hasPrepared(currentAttack.weapon.getName())) {
                    modsplus = new ArrayList<>();

                    Message clack = new Message("Пуф! Ничего не произошло… (не подготовлено)", ChatColor.RED);
                    ServerProxy.sendMessageFromTwoSourcesAndInformMasters(attacker, defender, range, clack);
                    //ServerProxy.informDistantMasters(attacker, clack, range.getDistance());
                    shouldBreak = true;
                    //mustBreak = true;
                    isAboutToClack = false;
                    return;

                }
            }

            if ((activeAttackNumber == 0 || activeHand == 2) && attacker.onCooldown(currentAttack.weapon.getName())) {
                Message clack = new Message("Пуф! Ничего не произошло… (на восстановлении)", ChatColor.RED);
                if (activeAttackNumber == 1) noRecoil = true;
                ServerProxy.sendMessageFromTwoSourcesAndInformMasters(attacker, defender, range, clack);
                //ServerProxy.informDistantMasters(attacker, clack, range.getDistance());
                shouldBreak = true;
                mustBreak = true;
                isAboutToClack = false;
                return;
            }

            cost = currentAttack.getWeapon().getMelee().getCost();
            cooldown = currentAttack.getWeapon().getMelee().getCooldown();
            if (cost >= 0) {
                if (!DataHolder.inst().useMana(attacker.getName(), cost)) {
                    modsplus = new ArrayList<>();
                    if(currentAttack.getWeapon().getMelee().isMagic()) {
                        //TODO: протеститьin
                        Message clack = new Message("Пуф! Ничего не произошло… (недостаточно маны)", ChatColor.RED);
                        ServerProxy.sendMessageFromTwoSourcesAndInformMasters(attacker, defender, range, clack);
                        //ServerProxy.informDistantMasters(attacker, clack, range.getDistance());
                        shouldBreak = true;
                        return;
                    }
                }
            }
            if (currentAttack.getDiceMessage().getDice().getResult() < currentAttack.weapon.getMelee().getDifficulty()) {
                modsplus = new ArrayList<>();

                damageInfo = new Message("Не достигнута сложность заклинания!", ChatColor.RED);
                currentAttack.getDiceMessage().build();
                //ServerProxy.informDistantMasters(attacker, clack, range.getDistance());
                shouldBreak = true;
                //mustBreak = true;
                isAboutToClack = false;
                return;

            }

            //if (isOpportunity) currentAttack.getDiceMessage().getDice().addMod(new Modificator(-2, ModificatorType.OPPORTUNITY));

            if (lying && defense.getDefenseType() != DefenseType.NOTHING && defense.getDefenseType() != DefenseType.CONSTITUTION) defense.getDiceMessage().getDice().addMod(new Modificator(-1, ModificatorType.LYING));
            if (attackerlying) {
                if (currentAttack.getWeapon().getReach() >= 4) {
                    currentAttack.getDiceMessage().getDice().addMod(new Modificator(-2, ModificatorType.LYING));
                } else if (currentAttack.getWeapon().getReach() >= 3) {
                    currentAttack.getDiceMessage().getDice().addMod(new Modificator(-1, ModificatorType.LYING));
                }
            }
            // Это надо перенести
            System.out.println("activeHand: " + activeHand + " debuff:" + attacker.getPercentDice().getLimbInjury());
        }

//        if (currentAttack.getWeapon().isFist()) { //если кулак
//            if (isOpportunity) currentAttack.getDiceMessage().getDice().addMod(new Modificator(-2, ModificatorType.OPPORTUNITY));
//        }

        /* ПРЕДЫДУЩИЕ ЗАЩИТЫ */

        if (attacks.size() > 1) {
            if (currentAttack.getDiceMessage().getDice().getModAsString().contains("+1 Концентрация\n")) {
                currentAttack.getDiceMessage().getDice().addMod(new Modificator(-1, ModificatorType.NOTCONCENTRATED)); //TODO: тупо
            }
        }

        if (multishot) {
            if (currentAttack.getWeapon().getFirearm().isPistol()) currentAttack.getDiceMessage().getDice().addMod(new Modificator(-2, ModificatorType.MULTISHOT));
            else currentAttack.getDiceMessage().getDice().addMod(new Modificator(-1, ModificatorType.MULTISHOT));
            shouldBreak = true;
            shouldNotBreak = false;
        }

        if (fullcover == 999) currentAttack.getDiceMessage().getDice().addMod(new Modificator(-9, ModificatorType.SAFE));

        Player actualDefender = defender;
        Player actualAttacker = attacker;

        Integer defenseDiceMod = null;
        if (actualDefender.getModifiersState().getModifier(ModifiersState.dice) != null)
            defenseDiceMod = actualDefender.getModifiersState().getModifier(ModifiersState.dice);
        if (defenseDiceMod != null && defenseDiceMod != -666 && defense.getDefenseType() != DefenseType.NOTHING) {
            defense.getDiceMessage().getDice().addMod(new Modificator(defenseDiceMod, ModificatorType.CUSTOM));
        }

        int preparingDebuff = defender.getPreparingDebuff();
        if (preparingDebuff != 0) defense.getDiceMessage().getDice().addMod(new Modificator(preparingDebuff, ModificatorType.PREPARING));

        if (currentAttack.weapon.isMelee()) {
            if (weaponDurabilityHandler.hasDurabilityForTag(currentAttack.weapon.getName())) {
                double ratio = weaponDurabilityHandler.getPercentageRatioForTag(currentAttack.weapon.getName());
                if(ratio < 0) {
                    damage.setBrokenWeapon(-currentAttack.weapon.getDamage(), "оружие развалилось");
                } else if (ratio == 0) {
                    damage.setBrokenWeapon(-(currentAttack.weapon.getDamage()/2), "оружие сломано");
                } else if(ratio <= 25 ) {
                    Random random = new Random();
                    int randomResult = random.nextInt(100) + 1;
                    System.out.println(randomResult);
                    if(randomResult <= (26 - ratio)) {
                        damage.setBrokenWeapon(-1, "оружие изношено");
                    }
                }
            }
        }

//        else if (splash) {
//            if (attacker.hasConcentration(currentAttack.getDiceMessage().getDice().getSkillName())) {
//                attacker.removeConcentration(currentAttack.getDiceMessage().getDice().getSkillName());
//                currentAttack.getDiceMessage().getDice().addMod(new Modificator(-1, ModificatorType.CONCENTRATED)); //TODO: тупо
//            }
//        }

        ///Финты и вы
        if (attacker.hasStatusEffect(StatusType.RECKLESS_ATTACK) && activeAttackNumber == 0) currentAttack.getDiceMessage().getDice().addMod(new Modificator(2, ModificatorType.RECKLESS_ATTACK));
        if (attacker.hasStatusEffect(StatusType.SHAMED)) currentAttack.getDiceMessage().getDice().addMod(new Modificator(-2, ModificatorType.SHAMED));
        if (attacker.hasShifted()) currentAttack.getDiceMessage().getDice().addMod(new Modificator(-1, ModificatorType.SHIFTED));

        int confused = defender.getConfused();
        if (defense.getDefenseType() != DefenseType.NOTHING) {
            if (defender.hasShifted())
                defense.getDiceMessage().getDice().addMod(new Modificator(-1, ModificatorType.SHIFTED));
            if (defender.hasStatusEffect(StatusType.LUNGE))
                defense.getDiceMessage().getDice().addMod(new Modificator(-4, ModificatorType.MID_LUNGE));
            if (defender.hasStatusEffect(StatusType.SHAMED))
                defense.getDiceMessage().getDice().addMod(new Modificator(-2, ModificatorType.SHAMED));
            if (defender.hasStatusEffect(StatusType.RECKLESS_DEFENCE))
                defense.getDiceMessage().getDice().addMod(new Modificator((defender.hasTrait(Trait.UNRECKLESS_ATTACK) ? -2 : -4), ModificatorType.RECKLESS_DEFENCE));
            if (defender.hasStatusEffect(StatusType.RELENTLESS) && defense.getDefenseType().equals(DefenseType.PHYSICAL))
                defense.getDiceMessage().getDice().addMod(new Modificator(defender.getRelentless(), ModificatorType.RELENTLESS));
            if (defender.hasStatusEffect(StatusType.RELENTLESS_PSYCH) && defense.getDefenseType().equals(DefenseType.PSYCH))
                defense.getDiceMessage().getDice().addMod(new Modificator(defender.getRelentlessPsych(), ModificatorType.RELENTLESS_PSYCH));

            if (confused != 0)
                defense.getDiceMessage().getDice().addMod(new Modificator(confused, ModificatorType.CONFUSED));
        }




        currentAttack.getDiceMessage().getDice().addMod(new Modificator(attacker.countConvenience(), ModificatorType.CONVENIENCE));
        currentAttack.getDiceMessage().getDice().addMod(new Modificator(currentAttack.getWeapon().getConvenience(), ModificatorType.ACCURACY));
        if (currentAttack.getAttackType().equals(AttackType.PHYSIC)) defense.getDiceMessage().getDice().addMod(new Modificator(defender.countDef(), ModificatorType.DEFENCE_MOD));

        if (defense.getDefenseType() != DefenseType.NOTHING && defender.getPreviousDefenses() > 0 && !defender.hasTrait(Trait.CONCENTRATED)) {
            if (defender.isCollected() || dontCheckPreviousDebuff() || defense.getDefenseType() == DefenseType.CONSTITUTION) {
            } else if (defender.getPreviousDefenses() > 0) {
                int defenses = defender.getPreviousDefenses();
                if ((defenses + Math.abs(confused)) > 5) defenses = Math.max(5 - Math.abs(confused), 0);
                if (defenses > 0) defense.getDiceMessage().getDice().addMod(new Modificator(-Math.min(defenses, 5), ModificatorType.PREVIOUS_DEFENSES));
            }
        }

        if (defense.getDefenseType() != DefenseType.NOTHING) {
            currentAttack.getDiceMessage().getDice().setEnemyDice((SkillDiceMessage) defense.getDiceMessage());
            currentAttack.getDiceMessage().getDice().getEnemyDice().getDice().reroll();
        }



        currentAttack.getDiceMessage().getDice().reroll();
        //currentAttack.getDiceMessage().rebuild();
        System.out.println(currentAttack.getDiceMessage());
        //System.out.println(defense.getDiceMessage());

        //ServerProxy.sendMessage(attacker, currentAttack.getDiceMessage());

        System.out.println("getDefenseType: " + defense.getDefenseType());
        /* ОСОБЕННОСТИ ТИПОВ ЗАЩИТЫ */
        switch (defense.getDefenseType()) {
            case BLOCKING:
//                if (activeAttackNumber > 0)  {
//                    try {
//                        defense.setDiceMessage(new SkillDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange(), defender.getPreviousDefenses()));
//                    } catch (DataException e) {
//                        defense.setDiceMessage(new FudgeDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange()));
//                    }
//                }                //конечности
                defense.getDiceMessage().getDice().getPercentDice().checkAndGetAndSetLimbInjurty(defense.getActiveHand(), defender.getName());
                //

                defense.getDiceMessage().build();
                System.out.println("Блокируем...");
                diff += DuelCalculator.calcDifferense(currentAttack.getDiceMessage().getDice(), defense.getDiceMessage().getDice(), currentAttack.getWeapon().isRanged());
                break;
            case PARRY:
//                if (activeAttackNumber > 0)  {
//                    try {
//                        defense.setDiceMessage(new SkillDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange(), defender.getPreviousDefenses()));
//                    } catch (DataException e) {
//                        defense.setDiceMessage(new FudgeDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange()));
//                    }
//                }
                Equipment parryEq = defense.getDefenseItem();
                if (parryEq == null) {
                    throw new CombatException("У вас в руках нет ничего, чем можно парировать!");
                }
                System.out.println(defender.getName() + " парирует с помощью " + parryEq.getName() + " с длиной " + parryEq.getReach() +
                        " против длины " + currentAttack.getWeapon().getReach());
                int parryMod = (int) (parryEq.getReach() - currentAttack.getWeapon().getReach() + 1);
                if (parryMod > 0) parryMod = 0;
                defense.getDiceMessage().getDice().addMod(new Modificator(parryMod, ModificatorType.PARRY));

                if (currentAttack.getWeapon().isRanged()) {
                    //currentAttack.getDiceMessage().getDice().addMod(new Modificator(-1, ModificatorType.POINT_BLANK));
                }
                //конечности
                defense.getDiceMessage().getDice().getPercentDice().checkAndGetAndSetLimbInjurty(defense.getActiveHand(), defender.getName());
                //
                defense.getDiceMessage().build();
                System.out.println("Парируем...");
                diff += DuelCalculator.calcDifferense(currentAttack.getDiceMessage().getDice(), defense.getDiceMessage().getDice(), currentAttack.getWeapon().isRanged());


                break;
            case NOTHING:
                diff += DuelCalculator.calcDifferenseForNothing(currentAttack.getDiceMessage().getDice());
                break;
            case EVADE:
                System.out.println("Уклоняемся...");
//                if (activeAttackNumber > 0)  {
//                    try {
//                        defense.setDiceMessage(new SkillDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange(), defender.getPreviousDefenses()));
//                    } catch (DataException e) {
//                        defense.setDiceMessage(new FudgeDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange()));
//                    }
//                } /*else {
//                    defense.getDiceMessage().getDice().getPercentDice().setLimbInjury(3);
//                }*/
                defense.getDiceMessage().build();
                diff += DuelCalculator.calcDifferense(currentAttack.getDiceMessage().getDice(), defense.getDiceMessage().getDice(), currentAttack.getWeapon().isRanged());
                break;
            case HAND_TO_HAND:
                System.out.println("Рукопашкой...");
//                if (activeAttackNumber > 0)  {
//                    try {
//                        defense.setDiceMessage(new SkillDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange(), defender.getPreviousDefenses()));
//                    } catch (DataException e) {
//                        defense.setDiceMessage(new FudgeDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange()));
//                    }
//                }
                if (currentAttack.getWeapon().isRanged()) {
                    //currentAttack.getDiceMessage().getDice().addMod(new Modificator(-1, ModificatorType.POINT_BLANK));
                }
                defense.getDiceMessage().getDice().getPercentDice().checkAndGetAndSetLimbInjurty(2, defender.getName());
                defense.getDiceMessage().build();
                diff += DuelCalculator.calcDifferense(currentAttack.getDiceMessage().getDice(), defense.getDiceMessage().getDice(), currentAttack.getWeapon().isRanged());
                break;
            case CONSTITUTION:
//                if (activeAttackNumber > 0)  {
//                    try {
//                        defense.setDiceMessage(new SkillDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange(), defender.getPreviousDefenses()));
//                    } catch (DataException e) {
//                        defense.setDiceMessage(new FudgeDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange()));
//                    }
//                }
                //
                defense.getDiceMessage().build();
                System.out.println("Группируемся...");
                diff += DuelCalculator.calcDifferense(currentAttack.getDiceMessage().getDice(), defense.getDiceMessage().getDice(), currentAttack.getWeapon().isRanged());
                break;
            case WILLPOWER:
//                if (activeAttackNumber > 0)  {
//                    try {
//                        defense.setDiceMessage(new SkillDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange(), defender.getPreviousDefenses()));
//                    } catch (DataException e) {
//                        defense.setDiceMessage(new FudgeDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange()));
//                    }
//                }
                //
                defense.getDiceMessage().build();
                System.out.println("Держимся...");
                diff += DuelCalculator.calcDifferense(currentAttack.getDiceMessage().getDice(), defense.getDiceMessage().getDice(), currentAttack.getWeapon().isRanged());
                break;
            // КОНТРАТАКА
            case COUNTER:
                System.out.println("Контратакуем...");
//                if (activeAttackNumber > 0)  {
//                    try {
//                        defense.setDiceMessage(new SkillDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange(), defender.getPreviousDefenses()));
//                    } catch (DataException e) {
//                        defense.setDiceMessage(new FudgeDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange()));
//                    }
//                }
                //конечности
                try {
                    if (currentAttack.getWeapon().isRanged()) {
                        //if (((Weapon) defense.getDefenseItem()).isMelee()) currentAttack.getDiceMessage().getDice().addMod(new Modificator(-1, ModificatorType.POINT_BLANK));
                        //else if (distance <= 1.8 && !currentAttack.weapon.getFirearm().isPistolOrSawoff()) currentAttack.getDiceMessage().getDice().addMod(new Modificator(-1, ModificatorType.POINT_BLANK));
                    }
                    if (((Weapon) defense.getDefenseItem()).isRanged()) {
                        //if (currentAttack.getWeapon().isMelee()) defense.getDiceMessage().getDice().addMod(new Modificator(-1, ModificatorType.POINT_BLANK));
                        //else if (distance <= 1.8 && !currentAttack.weapon.getFirearm().isPistolOrSawoff()) defense.getDiceMessage().getDice().addMod(new Modificator(-1, ModificatorType.POINT_BLANK));
                    }
                } catch (Exception ignored) {
                    ignored.printStackTrace();
                }


                if (defense.getDefenseItem() instanceof Weapon && ((Weapon) defense.getDefenseItem()).isMelee() && ((Weapon) defense.getDefenseItem()).getMelee().isTwohanded()) {
                    attacker.getPercentDice().checkAndGetAndSetLimbInjurty(2, attacker.getName());
                } else if (defense.getDefenseItem() instanceof Weapon && ((Weapon) defense.getDefenseItem()).isRanged() && ((Weapon) defense.getDefenseItem()).getFirearm().isTwoHanded()) {
                    attacker.getPercentDice().checkAndGetAndSetLimbInjurty(2, attacker.getName());
                } else {
                    attacker.getPercentDice().checkAndGetAndSetLimbInjurty(getActualActiveHand(), attacker.getName());
                }
                defense.getDiceMessage().getDice().getPercentDice().checkAndGetAndSetLimbInjurty(defense.getActiveHand(), defender.getName());
                //
                defense.getDiceMessage().build();
                diff += DuelCalculator.calcDifferense(currentAttack.getDiceMessage().getDice(), defense.getDiceMessage().getDice(), currentAttack.getWeapon().isRanged());
                if (diff < 0) {
                    if (!successfulCounter) {
                        if (currentAttack.getWeapon().isRanged()) weaponDurabilityHandler.takeAwayDurabilityForTag(currentAttack.getWeapon().getName(), 1);
                        Duel counter = DuelMaster.createDuel(defender);
                        counter.setSuccessfulCounter(true);
                        counter.setAttacker(defender);
                        FudgeDiceMessage att;
                        try {
                            att = new SkillDiceMessage(currentAttack.getDiceMessage().getPlayerName(), currentAttack.getDiceMessage().getContent(), currentAttack.getDiceMessage().getRange(), attacker.getPreviousDefenses());
                        } catch (Exception e) {
                            att = new FudgeDiceMessage(currentAttack.getDiceMessage().getPlayerName(), currentAttack.getDiceMessage().getContent(), currentAttack.getDiceMessage().getRange());
                        }
                        FudgeDiceMessage def;
                        try {
                            def = new SkillDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange(), defender.getPreviousDefenses());
                        } catch (Exception e) {
                            def = new FudgeDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange());
                        }
                        counter.addAttack(new Attack(def, (Weapon) defense.getDefenseItem()));
                        counter.getActiveAttack().setWeapon((Weapon) defense.getDefenseItem());//TODO: реворк с пробросом новых дайсов?
                        counter.setActiveHand(defense.getActiveHand());
                        counter.setDefense(new Defense());
                        counter.setDefender(attacker);
                        counter.getDefense().setActiveHand(activeHand);
                        counter.getDefense().setDefenseType(DefenseType.ANTICOUNTER);
                        counter.getDefense().setDiceMessage(att);
                        currentAttack.getDiceMessage().build();
                        defense.getDiceMessage().build();
                        counter.setOldAttackResult(currentAttack.getDiceMessage());
                        counter.setOldDefenseResult(defense.getDiceMessage());
                        System.out.println("FORCED RESULT !@ " + defense.getDiceMessage());
                        counter.initiated = true;
                        successfulCounter = true;
                        mustEnd = true;
                        return;
                    }
                } else {
                    if (!successfulCounter) unsuccessfulCounter = true;
                }
                break;
            case ANTICOUNTER:
                System.out.println("Антиконтр...");
                try {

                        WeaponTagsHandler defWeaponh = new WeaponTagsHandler(defender.getItemForHand(defense.getActiveHand()));
                        Weapon weapon = new Weapon(defWeaponh.getDefaultWeaponName(), defWeaponh.getDefaultWeapon());
//                        if (currentAttack.getWeapon().isRanged()) {
//                            if (weapon.isMelee())
//                                currentAttack.getDiceMessage().getDice().addMod(new Modificator(-1, ModificatorType.POINT_BLANK));
//                            else if (distance <= 1.8 && !currentAttack.weapon.getFirearm().isPistolOrSawoff())
//                                currentAttack.getDiceMessage().getDice().addMod(new Modificator(-1, ModificatorType.POINT_BLANK));
//                        }
//                        if (!((Weapon) defense.getDefenseItem()).isFist() || !defense.getDiceMessage().getDice().getSkillName().equals(DefenseType.HAND_TO_HAND)) {
//                            if (weapon.isRanged()) {
//                                if (currentAttack.getWeapon().isMelee())
//                                    defense.getDiceMessage().getDice().addMod(new Modificator(-1, ModificatorType.POINT_BLANK));
//                                else if (distance <= 1.8 && !weapon.getFirearm().isPistolOrSawoff())
//                                    defense.getDiceMessage().getDice().addMod(new Modificator(-1, ModificatorType.POINT_BLANK));
//                            }
//                        }

                } catch (Exception ignored) {
                    ignored.printStackTrace();
                }


                if (defense.getDefenseItem() instanceof Weapon && ((Weapon) defense.getDefenseItem()).isMelee() && ((Weapon) defense.getDefenseItem()).getMelee().isTwohanded()) {
                    attacker.getPercentDice().checkAndGetAndSetLimbInjurty(2, attacker.getName());
                } else if (defense.getDefenseItem() instanceof Weapon && ((Weapon) defense.getDefenseItem()).isRanged() && ((Weapon) defense.getDefenseItem()).getFirearm().isTwoHanded()) {
                    attacker.getPercentDice().checkAndGetAndSetLimbInjurty(2, attacker.getName());
                } else {
                    attacker.getPercentDice().checkAndGetAndSetLimbInjurty(getActualActiveHand(), attacker.getName());
                }
                defense.getDiceMessage().getDice().getPercentDice().checkAndGetAndSetLimbInjurty(defense.getActiveHand(), defender.getName());
//                if (activeAttackNumber > 0)  {
//                    try {
//                        defense.setDiceMessage(new SkillDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange(), defender.getPreviousDefenses()));
//                    } catch (DataException e) {
//                        defense.setDiceMessage(new FudgeDiceMessage(defense.getDiceMessage().getPlayerName(), defense.getDiceMessage().getContent(), defense.getDiceMessage().getRange()));
//                    }
//                } /*else {
//                    defense.getDiceMessage().getDice().getPercentDice().setLimbInjury(3);
//                }*/
                System.out.println("currentAttack " + currentAttack.getDiceMessage().getDice().isAlreadyCast());
                System.out.println("currentAttack " + currentAttack.getDiceMessage().isBuilt());
                defense.getDiceMessage().build();
                diff += DuelCalculator.calcDifferense(currentAttack.getDiceMessage().getDice(), defense.getDiceMessage().getDice(), currentAttack.getWeapon().isRanged());
                break;
        }
        diff = currentAttack.getDiceMessage().getDice().getResult();
        System.out.println("DIFF1: " + diff);

        if (cooldown > 0 && (activeHand == 2 || activeAttackNumber == 0)) {
            cooldowns.add(new MutablePair<>(currentAttack.getWeapon().getName(), cooldown));
        }

        // BUILD ATTACK!! VERY IMPORTANT, NO ADDING MODIFIERS AFTER THIS!



        /* ФИНТ */
        if (currentAttack.weapon.isMelee() && attacker.hasStatusEffect(StatusType.FEINT) && !isOpportunity) {

            if (defense.getDefenseType() != DefenseType.NOTHING) {
                if (defender.hasTrait(Trait.EXPIRIENCED)) {
                    if (defender.getTraitLevel(Trait.EXPIRIENCED) > 1) {
                        damageInfo = new Message("Финт " + actualAttacker.getName() + " был с легкостью раскушен благодаря опыту " + defender.getName() + "! Какой позор!", ChatColor.GMCHAT);
                        defender.addStatusEffect("Растерян", StatusEnd.ROUND_START, 2, StatusType.CONFUSED);
                        GunCus.channel.sendToAllAround(new MessageSound(attackerEntity, currentAttack.weapon.getSound() + "_miss", 2F, 1F), new NetworkRegistry.TargetPoint(attackerEntity.world.provider.getDimension(), attackerEntity.posX, attackerEntity.posY, attackerEntity.posZ, 16F));
                    } else {
                        damageInfo = new Message("Атака " + actualAttacker.getName() + " была финтом! " + defender.getName() + ", впрочем, привык к подобному.", ChatColor.GMCHAT);
                        GunCus.channel.sendToAllAround(new MessageSound(attackerEntity, currentAttack.weapon.getSound() + "_miss", 2F, 1F), new NetworkRegistry.TargetPoint(attackerEntity.world.provider.getDimension(), attackerEntity.posX, attackerEntity.posY, attackerEntity.posZ, 16F));
                    }
                } else {
                    damageInfo = new Message("Атака " + actualAttacker.getName() + " была финтом! " + defender.getName() + " растерян!", ChatColor.GMCHAT);
                    defender.addStatusEffect("Растерян", StatusEnd.ROUND_START, 2, StatusType.CONFUSED);
                    GunCus.channel.sendToAllAround(new MessageSound(attackerEntity, currentAttack.weapon.getSound() + "_feint", 2F, 1F), new NetworkRegistry.TargetPoint(attackerEntity.world.provider.getDimension(), attackerEntity.posX, attackerEntity.posY, attackerEntity.posZ, 16F));
                }
            } else {
                damageInfo = new Message("Финт " + actualAttacker.getName() + " был раскушен! Какой позор!", ChatColor.GMCHAT);
                attacker.addStatusEffect("Опозорен", StatusEnd.ROUND_START, 2, StatusType.SHAMED);
                Random random = new Random();
                int randomResult = random.nextInt(100) + 1;
                if (randomResult == 1) {
                    GunCus.channel.sendToAllAround(new MessageSound(attackerEntity, GunCus.SOUND_LAUGH, 2F, 1F), new NetworkRegistry.TargetPoint(attackerEntity.world.provider.getDimension(), attackerEntity.posX, attackerEntity.posY, attackerEntity.posZ, 16F));
                } else {
                    GunCus.channel.sendToAllAround(new MessageSound(attackerEntity, currentAttack.weapon.getSound() + "_miss", 2F, 1F), new NetworkRegistry.TargetPoint(attackerEntity.world.provider.getDimension(), attackerEntity.posX, attackerEntity.posY, attackerEntity.posZ, 16F));
                }
            }
            attacker.removeStatusEffect(StatusType.FEINT);
            shouldBreak = true;
            //mustBreak = true;
            return;
        } else if (currentAttack.getDiceMessage().getDice().getResult() <= 0) {
            damageInfo = new Message("Атака " + actualAttacker.getName() + " неуспешна.", ChatColor.RED);
            if(defense.getDefenseType().equals(DefenseType.PARRY)) {
//                if (!currentAttack.getWeapon().getName().equals("fist")) weaponDurabilityHandler.takeAwayDurabilityForTag(currentAttack.getWeapon().getName(), 2);
//                WeaponDurabilityHandler defenderDurHandler = defender.getDurHandlerForHand(defense.getActiveHand());
//                int loss = 1;
//                if(modsplus.contains("коррозия")) loss = loss * 5;
//                defenderDurHandler.takeAwayDurabilityForTag(defense.getDefenseItem().getName(), loss);
//                System.out.println("maybe ripost?");
//                if(diff <= -2 && !isOpportunity) {
//                    riposte = true;
//                    System.out.println("RIPOST!!!");
//                    shouldBreak = true;
//                    shouldNotBreak = false;
//                }
//                GunCus.channel.sendToAllAround(new MessageSound(defenderEntity, GunCus.SOUND_PARIROVANIE, 2.5F, 1F), new NetworkRegistry.TargetPoint(defenderEntity.world.provider.getDimension(), defenderEntity.posX, defenderEntity.posY, defenderEntity.posZ, 16F));
            } else {
                if (currentAttack.weapon.getName().equals("fist")) {
                    GunCus.channel.sendToAllAround(new MessageSound(defenderEntity, GunCus.SOUND_PUNCH_MISS, 1.5F, 1F), new NetworkRegistry.TargetPoint(defenderEntity.world.provider.getDimension(), defenderEntity.posX, defenderEntity.posY, defenderEntity.posZ, 16F));
                } else if (currentAttack.getWeapon().isRanged()) {
                    GunCus.channel.sendToAllAround(new MessageSound(defenderEntity, currentAttack.getWeapon().getSound() + "_miss", 2F, 1F), new NetworkRegistry.TargetPoint(defenderEntity.world.provider.getDimension(), defenderEntity.posX, defenderEntity.posY, defenderEntity.posZ, 16F));
                } else if (currentAttack.getWeapon().isMelee()) {
                    GunCus.channel.sendToAllAround(new MessageSound(defenderEntity, currentAttack.getWeapon().getSound() + "_miss", 2F, 1F), new NetworkRegistry.TargetPoint(defenderEntity.world.provider.getDimension(), defenderEntity.posX, defenderEntity.posY, defenderEntity.posZ, 16F));
                }
            }
            if (successfulCounter) {
                damageInfo = new Message("Контратака " + actualAttacker.getName() + " неуспешна.", ChatColor.RED);
            }
            //shouldBreak = true;
            return;
        } else {
            reachedDifficulty = true;
        }

        if (activeAttackNumber == 0 && !isOpportunity && !splash && !attackerlying && !attacker.isSluggish() && !defender.hasTrait(Trait.CONCEIT) && (currentAttack.getWeapon().getMelee() != null || currentAttack.getWeapon().isFist())) {
            Duel opportunity = new Duel(attacker);
            opportunity.setOpportunity(true);
            opportunity.setAttacker(attacker);
            FudgeDiceMessage fdm;
            try {
                fdm = new SkillDiceMessage(currentAttack.getDiceMessage().getPlayerName(), currentAttack.getDiceMessage().getContent(), currentAttack.getDiceMessage().getRange());
            } catch (DataException e) {
                fdm = new FudgeDiceMessage(currentAttack.getDiceMessage().getPlayerName(), currentAttack.getDiceMessage().getContent(), currentAttack.getDiceMessage().getRange());
            }
            opportunity.addAttack(new Attack(fdm, currentAttack.getWeapon()));
            opportunity.setDefender(defender);
            if (attacker.getModifiersState().isModifier(ModifiersState.stunning)) opportunity.setStunOp(true);
            if (activeHand == 2) {
                opportunity.setActiveHand(1);
            } else {
                opportunity.setActiveHand(activeHand);
            }

            attacker.setOpportunity(opportunity, defender);
        }

        if (currentAttack.getWeapon().isRanged() && rangedResult != -666 && attackDifficulty != -666) {
            /* НЕ ХВАТАЕТ ДАЛЬНОСТИ */
            if (rangedResult < attackDifficulty) {
                damageInfo = new Message("Сложность попадания не достигнута!", ChatColor.COMBAT);
                shouldBreak = true;
                System.out.println("3: " + currentAttack.getDiceMessage().toString());
                currentAttack.getDiceMessage().build();
                System.out.println("4: " + currentAttack.getDiceMessage().toString());
                //промах
                GunCus.channel.sendToAllAround(new MessageSound(defenderEntity, currentAttack.getWeapon().getSound() + "_miss", 2F, 1F), new NetworkRegistry.TargetPoint(defenderEntity.world.provider.getDimension(), defenderEntity.posX, defenderEntity.posY, defenderEntity.posZ, 16F));
                return;

                /* ХВАТИЛО ДАЛЬНОСТИ */
            } else {
                reachedDifficulty = true;
                int defenseResult = -1;
                if (defense.getDefenseType() != DefenseType.NOTHING) {
                    defenseResult = defense.getDiceMessage().getDice().getResult();
                }
                // ЗАЩИТА НИЖЕ СЛОЖНОСТИ ПОПАДАНИЯ
                System.out.println("if: " + defenseResult + " < " + attackDifficulty);
                if (defenseResult < attackDifficulty) {
                    System.out.println("Modifying diff!");
                    diff += defenseResult - attackDifficulty;
                    //globalAttackDifficulty = attackDifficulty;
                    defenseFromFirearmDifficulty = true;
                    // сложность попадания 2, результат атаки 4, результат защиты 1. diff = 4-1 = 3
                    // прибавляем к diff 1-2=-1, 3-1 = 2
                    // новая разница это 4-2 = 2
                    // сложность попадания 0, результат атаки 3, результат защиты -4. diff = -4 - 3 = -7
                    // прибавляем к diff -4-0 = -4, -7--4= -3
                    // новая разница это 3 --3 = 6
                    // -4 - 0 = -4
                    // 3 -- 4 = 7
                    // -4 + 7 = 3

                    //-3 - 0 = -3
                    //3 -- 3 = 6
                    //-3 + 6 = 3

                    //-2 - 0 = -2
                    //6 -- 2 =

                    //-1 - 3 = -4
                    //5 -- 1 = 6
                    //-4 + 6 = 2


                }

            }
        }

        /* УСПЕШНАЯ ЗАЩИТА */
        if (!successfulCounter && diff < 0 && !defense.getDefenseType().equals(DefenseType.BLOCKING) && !defense.getDefenseType().equals(DefenseType.CONSTITUTION)) {
            damageInfo = new Message("Защита " + actualDefender.getName() + " успешна!", ChatColor.COMBAT);
            shouldBreak = true;
            if (!DataHolder.inst().isNpc(actualDefender.getName()) && !defense.getDiceMessage().getDice().getSkillName().isEmpty() && DataHolder.inst().areSkillsComparable(defense.getDiceMessage(), currentAttack.getDiceMessage())) {
                //тут прокач
                System.out.println("TEST EXP");
                if (Helpers.hasSpaceForXp(defender.getName())) {
                    int exp = 0;
                    boolean stunning = false;
                    if (stunOp) stunning = true;
                    else if (currentAttack.getWeapon().isRanged()) {
                        if (currentAttack.getWeapon().getFirearm().isModern()) {
                            Projectile ammo = currentAttack.getWeapon().getFirearm().getLoadedProjectile();
                            if (ammo.getCaliber().equals("травматический")) {
                                stunning = true;
                            }
                            else if (ammo.getMod().contains("нелетальн")) {
                                damage.setNonlethal(-1, "нелетальный патрон");
                                stunning = true;
                            }
                            else if (ammo.getMod().contains("резиновая пуля")) {
                                damage.setNonlethal(-1, "резиновая пуля");
                                stunning = true;
                            }
                            else if (ammo.getMod().contains("дротик")) {
                                /*attacker.getPercentDice().setAmmo(10);*/
                            }
                        } else {
                            Projectile ammo = currentAttack.getWeapon().getFirearm().getLoadedProjectile();
                            if (ammo.getMod().contains("затуплен")) {
                                damage.setNonlethal(-1, "затупленный наконечник");
                                stunning = true;
                            }

                        }
                    } else if ((attacker.getModifiersState().isModifier(ModifiersState.stunning) || (currentAttack.getWeapon().isMelee() && currentAttack.getWeapon().getMelee().isNonlethal()))) {
                        stunning = true;
                    }
                    int customDamage = 0;
                    if (actualAttacker.getModifiersState().isModifier("damage")) {
                        customDamage = actualAttacker.getModifiersState().getModifier("damage");
                    }
                    switch (defender.getSkillLevelAsInt(defense.getDiceMessage().getDice().getSkillName())) {
                        case -1:
                            if (currentAttack.getWeapon().getDamage() >= 0 || customDamage >= 0) {
                                exp = 8;
                            }
                            break;
                        case 0:
                            if (currentAttack.getWeapon().getDamage() >= 2 || customDamage >= 2) {
                                exp = 4;
                            }
                            break;
                        case 1:
                            if ((currentAttack.getWeapon().getDamage() >= 4 || customDamage >= 4) && ((!stunning && !suppressing) || defender.getSkill(defense.getDiceMessage().getDice().getSkillName()).getPercent() < 50)) {
                                exp = 2;
                            }
                            break;
                        case 2:
                            if ((currentAttack.getWeapon().getDamage() >= 6 || customDamage >= 6) && !stunning && !suppressing) {
                                exp = 1;
                            }
                            break;
                    }
                    if (exp != 0) {
                        if (Helpers.addXp(defender.getName(), defense.getDiceMessage().getDice().getSkillName() + ":" + exp)) {
                            damageInfo = new Message("Защита " + actualDefender.getName() + " ", ChatColor.COMBAT);
                            MessageComponent expComp = new MessageComponent("успешна", ChatColor.NICK);
                            expComp.setHoverText(actualDefender.getName() + ": +" + exp + "‰ опыта к " + defense.getDiceMessage().getDice().getSkillName(), TextFormatting.GREEN);
                            expComp.setUnderlined(true);

                            damageInfo.addComponent(expComp);
                            damageInfo.addComponent(new MessageComponent("!", ChatColor.COMBAT));
                        }
                    }
                }
            }
            //ServerProxy.informMasters(success);
            if(defense.getDefenseType().equals(DefenseType.PARRY)) {
                weaponDurabilityHandler.takeAwayDurabilityForTag(currentAttack.getWeapon().getName(), 2);
                WeaponDurabilityHandler defenderDurHandler = defender.getDurHandlerForHand(defense.getActiveHand());
                int loss = 1;
                if(modsplus.contains("коррозия")) loss = loss * 5;
                defenderDurHandler.takeAwayDurabilityForTag(defense.getDefenseItem().getName(), loss);
                if(diff <= -2 && !isOpportunity) {
                    riposte = true;
                    shouldBreak = true;
                    shouldNotBreak = false;
                }
                GunCus.channel.sendToAllAround(new MessageSound(defenderEntity, GunCus.SOUND_PARIROVANIE, 2.5F, 1F), new NetworkRegistry.TargetPoint(defenderEntity.world.provider.getDimension(), defenderEntity.posX, defenderEntity.posY, defenderEntity.posZ, 16F));
            } else if (defense.getDefenseType().equals(DefenseType.PHYSICAL) || defense.getDefenseType().equals(DefenseType.PSYCH)) {
                if (currentAttack.weapon.getName().equals("fist")) {
                    GunCus.channel.sendToAllAround(new MessageSound(defenderEntity, GunCus.SOUND_PUNCH_MISS, 2F, 1F), new NetworkRegistry.TargetPoint(defenderEntity.world.provider.getDimension(), defenderEntity.posX, defenderEntity.posY, defenderEntity.posZ, 16F));
                } else if (currentAttack.getWeapon().isRanged()) {
                    GunCus.channel.sendToAllAround(new MessageSound(defenderEntity, currentAttack.getWeapon().getSound() + "_miss", 2F, 1F), new NetworkRegistry.TargetPoint(defenderEntity.world.provider.getDimension(), defenderEntity.posX, defenderEntity.posY, defenderEntity.posZ, 16F));
                } else if (currentAttack.getWeapon().isMelee()) {
                    GunCus.channel.sendToAllAround(new MessageSound(defenderEntity, currentAttack.getWeapon().getSound() + "_miss", 2F, 1F), new NetworkRegistry.TargetPoint(defenderEntity.world.provider.getDimension(), defenderEntity.posX, defenderEntity.posY, defenderEntity.posZ, 16F));
                }
            } else if (defense.getDefenseType().equals(DefenseType.HAND_TO_HAND)) {
                GunCus.channel.sendToAllAround(new MessageSound(defenderEntity, GunCus.SOUND_PUNCH, 2F, 1F), new NetworkRegistry.TargetPoint(defenderEntity.world.provider.getDimension(), defenderEntity.posX, defenderEntity.posY, defenderEntity.posZ, 16F));
            }
            /* НЕУСПЕШНАЯ ЗАЩИТА */
        } else {

            if (beingDefended != null && !defense.getDefenseType().equals(DefenseType.NOTHING) && !attacker.hasStatusEffect(StatusType.FEINT)) {
                System.out.println("TESSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSST");
                defender.clearDefends();
                Duel newDuel = copy();
                boolean newDefender = false;
                while (beingDefended.getDefendedBy().size() > 0) {
                    if (ServerProxy.getDistanceBetween(beingDefended, beingDefended.getDefendedBy().get(beingDefended.getDefendedBy().size()-1)) > 3) {
                        beingDefended.getDefendedBy().get(beingDefended.getDefendedBy().size()-1).clearDefends();
                        continue;
                    }
                    newDefender = true;
                    newDuel.setBeingDefended(beingDefended);
                    newDuel.setDefender(beingDefended.getDefendedBy().get(beingDefended.getDefendedBy().size()-1));
                }
                if (!newDefender) {
                    newDuel.setDefender(beingDefended);
                }
                nextDuel = newDuel;
                mustEnd = true;
                return;
            }


            // GET TRAITS
            int traitMod = 0;
            int defenseMod = 0;
            int psychDefenseMod = 0;

            Armor armor = actualDefender.getArmor();

            System.out.println(armor + " " + armor.getDamageString() + " " + armor.getPsychDamage());

            try {
                MutablePair<Integer, String> defenseModPair = DataHolder.rollMegaDice(armor.getDamageString(), "§6Физическая броня");
                defenseMod = (int) defenseModPair.left;
                dicesMegaString += (String) defenseModPair.right;
                MutablePair<Integer, String> psychDefenseModPair = DataHolder.rollMegaDice(armor.getPsychDamage(), "§5Психическая броня");
                psychDefenseMod = (int) psychDefenseModPair.left;
                dicesMegaString += (String) psychDefenseModPair.right;

                System.out.println(defenseMod + " reeee");
                System.out.println(psychDefenseMod + " reeee");
            } catch (Exception se) {
                se.printStackTrace();
            }
//            ArmorPiece armorPiece = armor.getArmorPiece(BodyPart.THORAX);
            if (actualDefender.isHardened()) {
                traitMod = -actualDefender.getHardened();
                damage.setHardened(-actualDefender.getHardened(), "толстокожий");
            }



            Message redirectedInfo = null;
//            if (defense.getDefenseType() == DefenseType.CONSTITUTION && diff < 0 && armor != null) {
//                ArmorPiece armorPieceTest = null;
//                int constit = defense.getDiceMessage().getDice().getResult();
//                if (currentAttack.getWeapon().isMelee()) {
//                    armorPieceTest = armor.getBestPiece(DamageType.MELEE, constit);
//                } else if (currentAttack.weapon.isRanged() && currentAttack.weapon.getFirearm().getProjectile().getCaliber().equals("холодное")) {
//                    armorPieceTest = armor.getBestPiece(DamageType.MELEE, constit);
//                } else if (currentAttack.weapon.isRanged() && currentAttack.weapon.getFirearm().isModern()) {
//                    armorPieceTest = armor.getBestPiece(DamageType.MODERN_FIREARM, constit);
//                } else {
//                    armorPieceTest = armor.getBestPiece(DamageType.DEFAULT, constit);
//                }
//                if (armorPieceTest != null && armorPieceTest.getPart() != BodyPart.THORAX) {
//                    redirectedInfo = new Message("\nУспешная группировка: атака перенаправлена в сегмент брони " + armorPieceTest.getPart().getName() + "!", ChatColor.BLUE);
//                    armorPiece = armorPieceTest;
//                    defenseMod = armorPieceTest.getType().getDefense();
//                }
//            }

//            ArmorPiece globalArmorPiece = armorPiece;

            if(currentAttack.weapon.isMelee()) {
                weaponDurabilityHandler.takeAwayDurabilityForTag(currentAttack.getWeapon().getName(), 1);
            }

            // ДОП МОДИФИКАТОРЫ
            int targetedMod = 0;
            int additionalTargetedDamage = 0;
            int additionalArmorDamage = 0;
            // Оглушение
            boolean stunning = false;
            if (attacker.getModifiersState().isModifier(ModifiersState.stunning) || (currentAttack.getWeapon().isMelee() && currentAttack.getWeapon().getMelee().isNonlethal() || stunOp)) {
                stunning = true;
                // Автоматически снижать урон если оружие не подходит для оглушения
                if (currentAttack.getWeapon().isMelee() && !currentAttack.getWeapon().getMelee().isNonlethal()) {
                    if (!currentAttack.getWeapon().getMelee().getCategory().equals(MeleeCategory.CRUSHING) && !currentAttack.weapon.getName().equals("fist")) {
                        if (currentAttack.getWeapon().getMelee().getCategory().equals(MeleeCategory.PENETRATING)) {
                            damage.setStunningWithoutCrushing(-2, "ударное с пробойником при оглушении");
                        } else if (currentAttack.getWeapon().getMelee().getCategory().equals(MeleeCategory.FLEXIBLE)) {
                            damage.setStunningWithoutCrushing(-2, "гибко-суставчатое при оглушении");
                        } else {
                            damage.setStunningWithoutCrushing(-2, "не дробящее оружие при оглушении");
                        }
                    } else {
                        damage.setStunningWithoutCrushing(-1, "дробящее оружие при оглушении");
                    }
                    try {
                        // Острота не даёт никаких бонусов при оглушении
                        if (currentAttack.getWeapon().getMelee().isSharp()) {
                            damage.setStunningWithSharp(-1, "острое при оглушении");
                        }
                    } catch (Exception ignored) {

                    }
                }
            }

            if (attacker.getModifiersState().isModifier(ModifiersState.capture)) {
                damage.setCapturing(-1, "принуждение к сдаче");
            }

            if (currentAttack.weapon.isMelee() && attacker.getChargeHistory() != ChargeHistory.NONE && attacker.getMovementHistory() == MovementHistory.NOW) {
                damage.setCharging(1, "натиск");
                hasCharged = true;
            }

            if (fullcover != 0) {
                damage.setFullcover(-fullcover, "полное укрытие");
            }

            int globalArmorPierceDamage = 0;
            int globalArmor = 0;

            /* ТОЧЕЧНЫЕ */
            BodyPart globalBodyPart = null;
            Message targetedInfo = new Message("\nТочечная атака в ", ChatColor.GRAY);
            if (attacks.size() == 1 && actualAttacker.getModifiersState().getModifier(ModifiersState.bodypart) != null && actualAttacker.getModifiersState().getModifier(ModifiersState.bodypart) != -666) {

                BodyPart targetedPart = BodyPart.getBodyPartFromInt(actualAttacker.getModifiersState().getModifier(ModifiersState.bodypart));
//                armorPiece = actualDefender.getArmor().getArmorPiece(targetedPart);

                if (targetedPart == BodyPart.ARMOR_JOINT) {
                    Message failedJoint = new Message("У вашего противника нет брони на торсе. Атака в стык брони отменена, проводится обычная.");
                    actualAttacker.sendMessage(failedJoint);
                    targetedInfo = null;
                    targetedPart = null;
                } else {



//                    defenseMod = actualDefender.getDefenseModFor(targetedPart);
                    targetedInfo.addComponent(new MessageComponent(targetedPart.getName() + " проведена ", ChatColor.GRAY));
                    int difficulty = BodyPart.getDifficultyFor(targetedPart);
                    String targDifStr = "";

                    if (currentAttack.weapon.isMeleeCategory(MeleeCategory.PIERCING) && targetedPart == BodyPart.ARMOR_JOINT) {
                        difficulty -= 1; // если оружие колющее, снижаем сложность точечной в стык брони
                        targDifStr += "\n§2(-1 Колющее в стык брони)";
                    }
                    if (currentAttack.weapon.isMeleeCategory(MeleeCategory.PIERCING) && targetedPart == BodyPart.THORAX) {
                        difficulty -= 1; // если оружие колющее, снижаем сложность точечной в стык брони
                        targDifStr += "\n§2(-1 Колющее в торс)";
                    }
                    if (currentAttack.weapon.isMeleeCategory(MeleeCategory.CHOPPING) && BodyPart.getDifficultyFor(targetedPart) == 2) {
                        difficulty -= 1; // если оружие колющее, снижаем сложность точечной в стык брони
                        if (targetedPart == BodyPart.STOMACH) targDifStr += "\n§2(-1 Рубящее в живот)";
                        else targDifStr += "\n§2(-1 Рубящее по конечности)";
                    }
                    if (currentAttack.weapon.isRanged()) {
                        switch (difficulty) {
                            case 2:
                                difficulty = Math.max(attackDifficulty + 1, 2);
                                if (difficulty > 2) {
                                    targDifStr += "\n§с(+" + ((attackDifficulty + 1) - 2) +" Дальность)";
                                }
                                break;
                            case 3:
                                difficulty = Math.max(attackDifficulty + 2, 3);
                                if (difficulty > 3) {
                                    targDifStr += "\n§с(+" + ((attackDifficulty + 2) - 3) +" Дальность)";
                                }
                                break;
                            case 4:
                                difficulty = Math.max(attackDifficulty + 3, 4);
                                if (difficulty > 4) {
                                    targDifStr += "\n§с(+" + ((attackDifficulty + 3) - 4) +" Дальность)";
                                }
                                break;
                        }
                    }
                    if (attacker.isAccurate()) {
                        difficulty--;
                        targDifStr += "\n§2(-1 Точность)";
                    }
                    System.out.println("сложность точечной: " + difficulty);

                    if (currentAttack.getDiceMessage().getDice().getResult() < difficulty || (defense.getDefenseType().equals(DefenseType.CONSTITUTION) && diff < 0)) {
                        MessageComponent failedTargeted = new MessageComponent("неуспешно", ChatColor.GRAY);
                        failedTargeted.setUnderlined(true);
                        failedTargeted.setHoverText("Сложность точечной была: §4" + DataHolder.inst().getSkillTable().get(difficulty) + "§e." + targDifStr, TextFormatting.YELLOW);
                        targetedInfo.addComponent(failedTargeted);
                        targetedInfo.addComponent(new MessageComponent(". Урон снижен на 1.", ChatColor.GRAY));
                        damage.setFailedTargeted(-1, "провальная точечная");
//                        if (!(defense.getDefenseType().equals(DefenseType.CONSTITUTION) && diff < 0)) {
//                            armorPiece = armor.getArmorPiece(BodyPart.THORAX); //промазал? Бьешь по нагруднику
//                            if (armorPiece != null) defenseMod = actualDefender.getDefenseModFor(BodyPart.THORAX);
//                            else defenseMod = 0;
//                        } else {
//                            armorPiece = globalArmorPiece;
//                            if (armorPiece != null) defenseMod = actualDefender.getDefenseModFor(armorPiece.getPart());
//                            else defenseMod = 0;
//                        }
                    } else {
                        MessageComponent successfulTargeted = new MessageComponent("успешно", ChatColor.GRAY);
                        successfulTargeted.setUnderlined(true);
                        successfulTargeted.setHoverText("Сложность точечной была: §2" + DataHolder.inst().getSkillTable().get(difficulty) + "§e." + targDifStr, TextFormatting.YELLOW);
                        targetedInfo.addComponent(successfulTargeted);
                        targetedInfo.addComponent(new MessageComponent("!", ChatColor.GRAY));
                        globalBodyPart = targetedPart;
//                        if (currentAttack.getWeapon().getMelee() != null) {
//                            if (currentAttack.getWeapon().getMelee().getCategory().equals(MeleeCategory.CHOPPING) && difficulty == 3 && targetedPart != BodyPart.ARMOR_JOINT) {
//                                damage.setTargetedChop(1, "рубящее при точечной по хорошо");
//                            } else if (currentAttack.getWeapon().getMelee().getCategory().equals(MeleeCategory.PIERCING) && difficulty == 4){
//                                damage.setTargetedChop(1, "колющее при точечной по отлично");
//                            }
//                        }
                        if (stunning && (targetedPart == BodyPart.HEAD || targetedPart == BodyPart.GROIN)) {
                            if (targetedPart == BodyPart.HEAD) damage.setStunningHead(1, "оглушение по голове");
                            if (targetedPart == BodyPart.GROIN) damage.setStunningHead(1, "оглушение в пах");
                        }
                    }
                }
            }

            /* ПРОВЕРКА ПАРАМЕТРОВ БРОНИ */
            // Для ближнего боя
            if (currentAttack.weapon.isMelee()) {
                //if (stunning && currentAttack.weapon.getMelee().getCategory().equals(MeleeCategory.CRUSHING)) damage.setStunningCrush(1, "ударно-дробящее при оглушении");
//                if (armorPiece != null) {
//                    if (armorPiece.isModern()) {
////                    defenseMod = 0;
//                        damage.setDefense(0, "броня (холодное по современной легкой)");
//                        globalArmor = 0;
//                        if (armorPiece.getType().equals(ArmorType.MEDIUM)) {
//                            globalArmor = 1;
//                            damage.setDefense(1, "броня (холодное по современной средней)");
//                        } else if (armorPiece.getType().equals(ArmorType.HEAVY)) {
//                            globalArmor = 2;
//                            damage.setDefense(2, "броня (холодное по современной тяжелой)");
//                        }
//                    } else {
//                        globalArmor = defenseMod;
//                        damage.setDefense(defenseMod, "броня");
//                    }
//                } else {
//                    if (currentAttack.weapon.getMelee().getCategory().equals(MeleeCategory.DISSECTING)) {
////                        additionalArmorDamage = 1;
//                        damage.setArmor(1, "ударно-рассекающее, без брони");
//                    }
//                }

                // Для огнестрела
            } else {
//                if (armorPiece != null && currentAttack.weapon.isRanged() && currentAttack.weapon.getFirearm().getProjectile().getCaliber().equals("холодное")) {
//                    if (armorPiece.isModern()) {
//                        //                    defenseMod = 0;
//                        globalArmor = 0;
//                        damage.setDefense(0, "броня (холодное по современной легкой)");
//                        if (armorPiece.getType().equals(ArmorType.MEDIUM)) {
//                            globalArmor = 1;
//                            damage.setDefense(1, "броня (холодное по современной средней)");
//                        } else if (armorPiece.getType().equals(ArmorType.HEAVY)) {
//                            globalArmor = 2;
//                            damage.setDefense(2, "броня (холодное по современной тяжелой)");
//                        }
//                    } else {
//                        globalArmor = defenseMod;
//                        damage.setDefense(defenseMod, "броня");
//                    }
//                } else if (armorPiece != null && currentAttack.weapon.isRanged() && currentAttack.getWeapon().getFirearm().isModern()) {
//                    if (currentAttack.weapon.getFirearm().isModern() && !armorPiece.isModern()) {
////                    defenseMod = 0;
//                        globalArmor = 0;
//                        damage.setDefense(0, "броня (огнестрел по устаревшей легкой)");
//                        if (armorPiece.getType().equals(ArmorType.MEDIUM)) {
////                        defenseMod = 1;
//                            globalArmor = 1;
//                            damage.setDefense(1, "броня (огнестрел по устаревшей средней)");
//                        } else if (armorPiece.getType().equals(ArmorType.HEAVY)) {
//                            globalArmor = 2;
//                            damage.setDefense(2, "броня (огнестрел по устаревшей тяжелой)");
//                        }
//                    } else {
//                        damage.setDefense(defenseMod, "броня");
//                        globalArmor = defenseMod;
//                    }
//                    // Для устаревшего огнестрела
//                } else if (armorPiece != null && currentAttack.weapon.isRanged() && !currentAttack.getWeapon().getFirearm().isModern()) {
//                    globalArmor = defenseMod;
//                    damage.setDefense(defenseMod, "броня");
//                } else if (armorPiece != null && currentAttack.weapon.isFist()) {
//                    globalArmor = defenseMod;
//                    damage.setDefense(defenseMod, "броня");
//                }

            }

            boolean shieldAvoided = false;
            String shieldstr = "щит";
            int shieldMod = 0;
            if (getDefense().getDefenseType().equals(DefenseType.BLOCKING) && (diff <= 0 || forceShield)) {
                shieldMod = getDefense().getDefenseItem().getDamage();
                if (currentAttack.weapon.isMeleeCategory(MeleeCategory.FLEXIBLE)) {
                    if (forceShield) {
                        shieldMod--;
                        shieldstr = "щит (гибко-суставчатое)";
                    }
                    else if (diff == 0) {
                        shieldMod = 0;
                        shieldstr = "щит (гибко-суставчатое)";
                        shieldAvoided = true;
                    }
                }
                if (!shieldAvoided) {
                    WeaponDurabilityHandler defenderDurHandler = defender.getDurHandlerForHand(defense.getActiveHand());
                    if (defenderDurHandler != null && defenderDurHandler.hasDurabilityDict()) {
                        if (defenderDurHandler.getPercentageRatio() <= 25) {
                            int ratio = defenderDurHandler.getPercentageRatio();
                            if (ratio < 0) {
                                shieldMod = 0;
                                shieldstr = "щит (развалился)";
                            } else if (ratio == 0) {
                                shieldMod = shieldMod / 2;
                                shieldstr = "щит (сломан)";
                            } else {
                                Random random = new Random();
                                int randomResult = random.nextInt(100) + 1;
                                System.out.println(randomResult);
                                if (randomResult <= (26 - ratio)) {
                                    shieldMod -= 1;
                                    shieldstr = "щит (изношен)";
                                }
                            }
                        }

                        if (currentAttack.weapon.getDamage() > shieldMod || modsplus.contains("коррозия")) {
                            int loss = 1;
                            if (modsplus.contains("коррозия")) loss = loss * 5;
                            defenderDurHandler.takeAwayDurability(loss);
                        }
                    }
                }
                if (forceShield) shieldstr += " (глухая оборона)";
//                System.out.println("Shield mod = " + shieldMod);
//                damageInt -= shieldMod;
                if (shieldMod <= 0) { damage.setShield(0, shieldstr); }
                else {damage.setShield(-shieldMod, shieldstr);}
//                damageInfo.addComponent(new MessageComponent("-", ChatColor.COMBAT));
//                damageInfo.addComponent(new MessageComponent(String.valueOf(shieldMod), ChatColor.BLUE));
            }

//            if(armorPiece!=null && !suppressing) {
//                if(globalBodyPart != BodyPart.ARMOR_JOINT) {
//                    WeaponDurabilityHandler armorDurHandler = defender.getDurHandlerForArmorPiece(armorPiece.getPart().getName());
//                    if(armorDurHandler != null && armorDurHandler.hasDurabilityDict()) {
//                        if (armorDurHandler.getPercentageRatio() <= 25) {
//                            int ratio = armorDurHandler.getPercentageRatio();
//                            if (ratio < 0) {
//                                damage.setDefense(0, "броня (развалилась)");
//                                globalArmor = 0;
//                            } else if (ratio == 0) {
//                                damage.setDefense(Math.abs(damage.defenseInt)/2, "броня (сломана)");
//                                globalArmor = Math.abs(damage.defenseInt);
//                            } else {
//                                Random random = new Random();
//                                int randomResult = random.nextInt(100) + 1;
//                                System.out.println(randomResult);
//                                if(randomResult <= (26 - ratio)) {
//                                    damage.setDefense(Math.max(Math.abs(damage.defenseInt) - 1, 0), "броня (изношена)");
//                                    globalArmor = Math.abs(damage.defenseInt);
//                                }
//                            }
//                        }
//                        if(currentAttack.weapon.getDamage() > shieldMod + Math.abs(damage.defenseInt) + defender.getMagicShield() || (currentAttack.weapon.getDamage() > shieldMod + defender.getMagicShield() && modsplus.contains("коррозия"))) {
//                            int loss = 1;
//                            if(modsplus.contains("коррозия")) loss = loss * 5;
//                            defender.takeAwayDurFromAllArmor(loss);
//                        }
//                    }
//                }
//            }

            if (getDefense().getDefenseType().equals(DefenseType.CONSTITUTION) && diff < 0) {
                damage.setConstitution(-1, "группировка");
            }

            // МОДЫ ПО БРОНЕ
//            if(armorPiece != null) {
//                if (armorPiece.getType().equals(ArmorType.HEAVY) || armorPiece.getType().equals(ArmorType.MEDIUM)) {
//                    if (currentAttack.weapon.isMeleeCategory(MeleeCategory.PENETRATING)) {
////                        additionalArmorDamage = 1;
//                            if (globalArmor < 1) {
//                                damage.setArmor(0, "ударное-пробойное по средней/тяжелой (броня уже пробита)");
//                            } else {
//                                damage.setArmor(1, "ударное-пробойное по средней/тяжелой");
//                                globalArmor--;
//                            }
//                    }
//                }
//            }

            if(globalBodyPart != null) {
                if (globalBodyPart.equals(BodyPart.ARMOR_JOINT)) {
//                            if (defenseMod > 0) defenseMod--;
//                            targetedInfo.addComponent(new MessageComponent(" Защита брони снижена на 1.", ChatColor.GRAY));
                    if (defenseMod > 0) {
                        if(globalArmor < 1 || damage.defenseInt == 0) {
                            damage.setTargetedJoint(0, "удар встык брони (броня уже пробита)");
                        } else {
                            damage.setTargetedJoint(1, "удар встык брони");
                            globalArmor--;
                        }
                    }
                    else
                        damage.setTargetedJoint(0, "удар встык брони (броня уже пробита)");
                }
                if (currentAttack.getWeapon().isRanged()) {
                    if (currentAttack.getWeapon().getFirearm().getLoadedProjectile().getMod().contains("разрыв")) {
                        damage.setBurst(1, "разрывной патрон/наконечник");
                    }
                }
            }

            /* МОДЫ ПАТРОНОВ */
            // НЕЛЕТАЛЬНЫЙ
            if (currentAttack.getWeapon().isRanged()) {
                //if (currentAttack.getWeapon().getFirearm().isModern()) {
                    Projectile ammo = currentAttack.getWeapon().getFirearm().getLoadedProjectile();
                    if (ammo.getCaliber().equals("травматический")) {
                        stunning = true;
                    }
                    else if (ammo.getMod().contains("нелетальн")) {
                        damage.setNonlethal(-1, "нелетальный патрон");
                        stunning = true;
                    }
                    else if (ammo.getMod().contains("резиновая пуля")) {
                        damage.setNonlethal(-1, "резиновая пуля");
                        stunning = true;
                    }
                    else if (ammo.getMod().contains("дротик")) {
                        /*attacker.getPercentDice().setAmmo(10);*/
                    }
                //} else {
                    if (ammo.getMod().contains("затуплен")) {
                        damage.setNonlethal(-1, "затупленный наконечник");
                        stunning = true;
                    }

                //}
            }
            // СОВРЕМЕННЫЕ БРОНЕБОЙНЫЕ
//            if (armorPiece != null) {
//                if (currentAttack.getWeapon().isRanged() && currentAttack.getWeapon().getFirearm().isModern()) {
//                    Projectile ammo = currentAttack.getWeapon().getFirearm().getLoadedProjectile();
//                    if (ammo.getMod().contains("картечь")) {
//                        if (armorPiece.getType().getDefense() > 1) {
//                            damage.setExp(-1, "картечь (по броне)");
//                        } else {
//                            damage.setExp(0, "картечь (по легкой броне)");
//                        }
//                    }
//                    if (ammo.getMod().contains("экспансив")) {
//                        if (armorPiece.getType().getDefense() > 1) {
//                            damage.setExp(-1, "экспансивный патрон (по броне)");
//                        } else {
//                            damage.setExp(0, "экспансивный патрон (по легкой броне)");
//                        }
////                        else {
////                            damage.setExp(1, "экспансивный патрон");
////                        }
//                    }
//
//                    // СОВРЕМЕННЫЕ БРОНЕБОЙНЫЕ
//                    if (ammo.getMod().contains("бронебойн") || ammo.getMod().contains("бтз")) {
//                        if (armorPiece.getType().getDefense() > 1) {
//                            if (ammo.getCaliber().equals("мрп") || ammo.getCaliber().equals("срп") || ammo.getCaliber().equals("крп")) {
//                                if(globalArmor < 1) {
//                                    damage.setAp(0, "бронебойная пуля (броня уже пробита)");
//                                } else {
//                                    damage.setAp(1, "бронебойная пуля");
//                                    globalArmor--;
//                                }
//                            } else {
//                                if(globalArmor < 1) {
//                                    damage.setAp(0, "бронебойный патрон (броня уже пробита)");
//                                } else {
//                                    damage.setAp(1, "бронебойный патрон");
//                                    globalArmor--;
//                                }
//                            }
//                        }
//                        else {
//                            if (ammo.getCaliber().equals("мрп") || ammo.getCaliber().equals("срп") || ammo.getCaliber().equals("крп")) {
//                                damage.setAp(0, "бронебойная пуля (по легкой броне)");
//                            } else {
//                                damage.setAp(0, "бронебойный патрон (по легкой броне)");
//                            }
//                        }
//                    }
//                    //ружейные пули
//                    else if (ammo.getCaliber().equals("мрп") || ammo.getCaliber().equals("срп") || ammo.getCaliber().equals("крп") && ammo.getMod().isEmpty()) {
////                        if (armorPiece.isModern() && armorPiece.getType().getDefense() > 0 && armorPiece.getType().getDefense() < 3) {
////                            damage.setAp(1, "ружейная пуля");
////                        }
///*                        if (!armorPiece.isModern() && armorPiece.getType().getDefense() == 3) {
//                            damage.setAp(1, "ружейная пуля");
//                        }*/
//                    }
//                    //флешетты
//                    else if(ammo.getMod().contains("флешетты")) {
//                        if (armorPiece.isModern() && armorPiece.getType().getDefense() > 0) {
//                            if (armorPiece.getType().getDefense() == 3) {
//                                damage.setAp(0, "флешетты (по современной тяжелой)");
//                            } else {
//                                if (globalArmor < 1) {
//                                    damage.setAp(0, "флешетты (броня уже пробита)");
//                                } else {
//                                    damage.setAp(1, "флешетты");
//                                    globalArmor--;
//                                }
//                            }
//                        }
//                        if (!armorPiece.isModern() && armorPiece.getType().getDefense() > 0) {
//                            if(globalArmor < 1) {
//                                damage.setAp(0, "флешетты (броня уже пробита)");
//                            } else {
//                                damage.setAp(1, "флешетты");
//                                globalArmor--;
//                            }
//                        }
//                    }
//
//                    // УСТАРЕВШИЕ БРОНЕБОЙНЫЕ
//                } else if (currentAttack.getWeapon().isRanged() && !currentAttack.getWeapon().getFirearm().isModern()) {
//                    Projectile ammo = currentAttack.getWeapon().getFirearm().getLoadedProjectile();
//                    if (ammo.getMod().contains("бронебойн") || ammo.getMod().contains("бтз")) {
//                        if (armorPiece.getType().getDefense() > 0) {
//                            if(globalArmor < 1) {
//                                damage.setAp(0, "бронебойный наконечник (броня уже пробита)");
//                            } else {
//                                damage.setAp(1, "бронебойный наконечник");
//                                globalArmor--;
//                            }
//
//                        }
//                    } else if (ammo.getCaliber().equals("патрон") || ammo.getCaliber().equals("пуля")) {
//                        if (!armorPiece.isModern() && (armorPiece.getType().getDefense() == 3 || armorPiece.getType().getDefense() == 2)) {
//                            if(globalArmor < 1) {
//                                damage.setAp(0, "устаревший огнестрел по устаревшей (броня уже пробита)");
//                            } else {
//                                damage.setAp(1, "устаревший огнестрел по устаревшей");
//                                globalArmor--;
//                            }
//                        }
//                    } else if (ammo.getMod().contains("зазубр")) {
//                        damage.setExp(0, "зазубренный наконечник (по броне)");
//                    }
//                }
//
//
//
//            } else if (armorPiece == null) {
//                // СОВРЕМЕННЫЕ ЭКСПАНСИВНЫЕ
//                if (currentAttack.getWeapon().isRanged() && currentAttack.getWeapon().getFirearm().isModern()) {
//                    Projectile ammo = currentAttack.getWeapon().getFirearm().getLoadedProjectile();
//                    if (ammo.getMod().contains("экспансив")) {
//                        damage.setExp(1, "экспансивный патрон");
//                    }
//                    if (ammo.getMod().contains("ударно-рассекающее")) {
//                        damage.setExp(1, "ударно-рассекающее, без брони");
//                    }
//                    if (ammo.getMod().contains("картечь")) {
//                        damage.setExp(1, "картечь");
//                    }
//                    if (ammo.getMod().contains("бронебойн") || ammo.getMod().contains("бтз")) {
//                        // СОВРЕМЕННЫЕ БРОНЕБОЙНЫЕ ПО БЕЗ БРОНИ
//                            if (ammo.getCaliber().equals("мрп") || ammo.getCaliber().equals("срп") || ammo.getCaliber().equals("крп")) {
//                                damage.setAp(-1, "бронебойная пуля (нет брони)");
//                            } else {
//                                damage.setAp(-1, "бронебойный патрон (нет брони)");
//                            }
//
//                    }
//                    // УСТАРЕВШИЕ ЗАЗУБРЕННЫЕ
//                } else if (currentAttack.getWeapon().isRanged() && !currentAttack.getWeapon().getFirearm().isModern()) {
//                    Projectile ammo = currentAttack.getWeapon().getFirearm().getLoadedProjectile();
//                    if (ammo.getMod().contains("зазубр")) {
//                        damage.setExp(1, "зазубренный наконечник");
//                    } else if (ammo.getMod().contains("бронебойн") || ammo.getMod().contains("бтз")) {
//                        damage.setAp(0, "бронебойный наконечник (нет брони)");
//                    }
//
//                }
//                // СОВРЕМЕННЫЕ БРОНЕБОЙНЫЕ ПО БЕЗ БРОНИ
//
//
//            }
//
//            if (modsplus.contains("электричество")) {
//                if (armorPiece != null) {
//                    if (armorPiece.getType().equals(ArmorType.HEAVY) || armorPiece.getType().equals(ArmorType.MEDIUM)) {
//                        if (armorPiece.getType().equals(ArmorType.HEAVY)) {
//                            if (globalArmor < 1) {
//                                damage.setElectricity(0, "электричество по тяжелой (броня уже пробита)");
//                            } else {
//                                damage.setElectricity(1, "электричество по тяжелой");
//                                globalArmor--;
//                            }
//                        } else if (armorPiece.getType().equals(ArmorType.MEDIUM)) {
//                            if (globalArmor < 1) {
//                                damage.setElectricity(0, "электричество по средней (броня уже пробита)");
//                            } else {
//                                damage.setElectricity(1, "электричество по средней");
//                                globalArmor--;
//                            }
//                        } else {
//                            damage.setElectricity(0, "электричество по легкой");
//                        }
//                    }
//                } else {
//                    damage.setElectricity(0, "электричество по без брони");
//                }
//            }

            /* СОПРОТИВЛЯЕМОСТЬ ЧЕК */
            if (currentAttack.getWeapon().isMelee()) {
                if (defender.getMeleeRes() == -1) damage.setRes(1, "уязвимость к ближнему");
                else if (defender.getMeleeRes() == 1) damage.setRes(-1, "сопротивляемость к ближнему");
                MeleeCategory cat = currentAttack.getWeapon().getMelee().getCategory();
                switch (cat) {
                    case CHOPPING:
                    case SLASHING:
                        if (defender.getSlashingRes() == -1) damage.setWeaponTypeRes(1, "уязвимость к режуще-рубящему");
                        else if (defender.getSlashingRes() == 1) damage.setWeaponTypeRes(-1, "сопротивляемость к режуще-рубящему");
                        break;
                    case CRUSHING:
                    case FLEXIBLE:
                    case PENETRATING:
                        if (defender.getCrushingRes() == -1) damage.setWeaponTypeRes(1, "уязвимость к дробящему");
                        else if (defender.getCrushingRes() == 1) damage.setWeaponTypeRes(-1, "сопротивляемость к дробящему");
                        break;
                    case PIERCING:
                        if (defender.getPiercingRes() == -1) damage.setWeaponTypeRes(1, "уязвимость к колющему");
                        else if (defender.getPiercingRes() == 1) damage.setWeaponTypeRes(-1, "сопротивляемость к колющему");
                        break;
                }
            } else if (currentAttack.getWeapon().isRanged()) {
                if (defender.getRangedRes() == -1) damage.setRes(1, "уязвимость к дальнему");
                else if (defender.getRangedRes() == 1) damage.setRes(-1, "сопротивляемость к дальнему");
                if (currentAttack.getWeapon().getFirearm().isModern()) {
                    if (defender.getModernFirearmRes() == -1) damage.setWeaponTypeRes(1, "уязвимость к совр. огнестрелу");
                    else if (defender.getModernFirearmRes() == 1) damage.setWeaponTypeRes(-1, "сопротивляемость к совр. огнестрелу");
                } else {
                    if (defender.getRangedOldRes() == -1) damage.setWeaponTypeRes(1, "уязвимость к устар. дальнему");
                    else if (defender.getRangedOldRes() == 1) damage.setWeaponTypeRes(-1, "сопротивляемость к устар. дальнему");
                }
                String cat = currentAttack.getWeapon().getFirearm().getLoadedProjectile().getMod();
                switch (cat) {
                    case "рубящее":
                    case "режущее":
                        if (defender.getSlashingRes() == -1) damage.setWeaponTypeRes(1, "уязвимость к режуще-рубящему");
                        else if (defender.getSlashingRes() == 1) damage.setWeaponTypeRes(-1, "сопротивляемость к режуще-рубящему");
                        break;

                    case "ударно-дробящее":
                    case "ударно-пробойное":
                    case "гибко-суставчатое":
                        if (defender.getCrushingRes() == -1) damage.setWeaponTypeRes(1, "уязвимость к дробящему");
                        else if (defender.getCrushingRes() == 1) damage.setWeaponTypeRes(-1, "сопротивляемость к дробящему");
                        break;
                    case "колющее":
                        if (defender.getPiercingRes() == -1) damage.setWeaponTypeRes(1, "уязвимость к колющему");
                        else if (defender.getPiercingRes() == 1) damage.setWeaponTypeRes(-1, "сопротивляемость к колющему");
                        break;
                }
            }

            /* БЕРЁМ МОДИФИКАТОР УРОНА ОРУЖИЯ */
            if (currentAttack.getWeapon() == null) {
                throw new CombatException("[FATAL] Система не нашла оружия, которым на вас нападали.");
            }

            String weaponDamage = currentAttack.getWeapon().getDamageString();
            String weaponPsychDamage = currentAttack.getWeapon().getPsychDamage();
            System.out.println(weaponDamage + " " + weaponPsychDamage);
            int weaponMod = 0;
            int psychWeaponMod = 0;
            int projectileMod = 0;
            int psychProjectileMod = 0;
            try {
                MutablePair<Integer, String> weaponModPair = DataHolder.rollMegaDice(weaponDamage, "§4Физический урон оружия");
                weaponMod = (int) weaponModPair.left;
                dicesMegaString += (String) weaponModPair.right;
                MutablePair<Integer, String> psychWeaponModPair = DataHolder.rollMegaDice(weaponPsychDamage, "§dПсихический урон оружия");
                psychWeaponMod = (int) psychWeaponModPair.left;
                dicesMegaString += (String) psychWeaponModPair.right;

            } catch (ScriptException se) {
                se.printStackTrace();
            }


            String projectilePh = "";
            String projectilePs = "";
            if (currentAttack.weapon.isRanged()) {
                try {
                    MutablePair<Integer, String> projectileModPair = DataHolder.rollMegaDice(currentAttack.weapon.getFirearm().getProjectile().getDamageString(), "§4Физический урон снаряда");
                    projectileMod = (int) projectileModPair.left;
                    dicesMegaString += (String) projectileModPair.right;

                    if (projectileMod != 0) projectilePh = " (" + projectileMod + " от снаряда)";
                    MutablePair<Integer, String> psychProjectileModPair = DataHolder.rollMegaDice(currentAttack.weapon.getFirearm().getProjectile().getPsychDamage(), "§dПсихический урон снаряда");
                    psychProjectileMod = (int) psychProjectileModPair.left;
                    dicesMegaString += (String) psychProjectileModPair.right;
                    if (psychProjectileMod != 0) projectilePs = " (" + psychProjectileMod + " от снаряда)";
                    weaponMod += projectileMod;
                    psychWeaponMod += psychProjectileMod;
                    System.out.println(weaponMod + " reeee");
                } catch (ScriptException se) {
                    se.printStackTrace();
                }
            }

            damage.setWeapon(weaponMod, "физический урон" + projectilePh);
            damage.setPsychDamage(psychWeaponMod, "психический урон" + projectilePs);

            AttackType globalType = AttackType.PHYSIC;

            int globalDamage = weaponMod;

            if (psychWeaponMod > weaponMod) {
                globalType = AttackType.PSYCHIC;
                globalDamage = psychWeaponMod;
            } else if (psychWeaponMod == weaponMod && psychWeaponMod != 0) {
                Random random = new Random();
                if (random.nextInt(1) == 1) {
                    globalType = AttackType.PSYCHIC;
                    globalDamage = psychWeaponMod;
                }
            }

            damage.setPriority(globalType);


            globalDamage += diff;


            int physd = globalType.equals(AttackType.PHYSIC) ? globalDamage : weaponMod;
            System.out.println(physd + " physd");
            if (defenseMod > physd) {
                if (physd <= 0 && weaponMod > 0) damage.setDefense(0, "физическая броня (не тронута)");
                else if (physd > 0 && weaponMod <= 0) damage.setDefense(0, "физическая броня (не тронута)");
                else if (physd != 0)  damage.setDefense(physd, "физическая броня (частично)");
            } else {
                damage.setDefense(defenseMod, "физическая броня");
            }
            int psychd = globalType.equals(AttackType.PSYCHIC) ? globalDamage : psychWeaponMod;
            if (psychDefenseMod > psychd) {
                if (psychd <= 0 && psychWeaponMod > 0) damage.setPsychDefence(0, "психическая броня (не тронута)");
                else if (psychd > 0 && psychWeaponMod <= 0) damage.setDefense(0, "психическая броня (не тронута)");
                else if (psychd != 0) damage.setPsychDefence(psychd, "психическая броня (частично)");
            } else {
                damage.setPsychDefence(psychDefenseMod, "психическая броня");
            }

            if (splash) {
//                if (piercing) {
//                    damage.setWeapon((damage.weaponInt - 2), damage.weaponString + " (пронзание)");
//                } else {
//                    damage.setWeapon((damage.weaponInt - 2), damage.weaponString + " (размах)");
//                }
            }

            System.out.println("modifiers for " + actualAttacker.getName() + " " + actualAttacker.getModifiersState().toString());
            if (actualAttacker.getModifiersState().getModifier(ModifiersState.damage) != null &&
                    actualAttacker.getModifiersState().getModifier(ModifiersState.damage) != -666) {
                weaponMod = actualAttacker.getModifiersState().getModifier(ModifiersState.damage);
                damage.setWeapon(weaponMod, "модификатор урона оружия (кастомный)");
            }

            if (stunning && modsplus.contains("оглушение")) {
                damage.setStunningCrushPlus(1, "свойство - оглушение");
            }

            int magicShield = -Math.abs(defender.getMagicShield());
            if (magicShield != 0) damage.setMagicShield(magicShield, "магический щит");

            System.out.println("TEST");
            /* СЧИТАЕМ УРОН */
            int damageInt;
            damageInt = DuelCalculator.calcDamage(
                    diff,
                    weaponMod,
                    defenseMod,
                    traitMod,
                    targetedMod,
                    additionalTargetedDamage,
                    additionalArmorDamage
            );


            // Build damage message
            damageInfo = new Message("Атака " + actualAttacker.getName() + " успешна: ", ChatColor.RED);
            if (successfulCounter) {
                damageInfo = new Message("Контратака " + actualAttacker.getName() + " успешна: ", ChatColor.RED);
            }

            if (suppressing) {
                int dam = damage.weaponInt;
                damage = new DuelCalculator.Damage();
                damage.setWeapon(Math.max(dam-2, 0), "подавление");
                modsplus = new ArrayList<>();
            }

            if (suppressing || modsplus.contains("ментальное")) {
                if (defender.isApatic()) damage.setWeapon(-100, "невосприимчивость к ментальным ранам");
            }


            damage.setDiff(diff, "количество успехов");
//            damageInfo.addComponent(new MessageComponent(String.valueOf(diff), ChatColor.RED));
//            if (weaponMod >= 0)
//                damageInfo.addComponent(new MessageComponent("+", ChatColor.COMBAT));
//            damageInfo.addComponent(new MessageComponent(String.valueOf(weaponMod), ChatColor.RED));

//            if (targetedMod != 0) { // Снижаем урон при провальной точечной
//                damageInfo.addComponent(new MessageComponent(String.valueOf(targetedMod), ChatColor.RED));
//            }

//            if (additionalTargetedDamage != 0) { // Повышаем урон, если точечная проведена рубящим оружием
//                damageInfo.addComponent(new MessageComponent("+" + additionalTargetedDamage, ChatColor.RED));
//            }
//            if (additionalArmorDamage != 0) { // Повышаем урон, если атака проведена режущим по легкой броне, или ударно-пробойным по средней или тяжелой
//                damageInfo.addComponent(new MessageComponent("+" + additionalArmorDamage, ChatColor.RED));
//            }


//            damageInfo.addComponent(new MessageComponent("-", ChatColor.COMBAT));
//            damageInfo.addComponent(new MessageComponent(String.valueOf(defenseMod), ChatColor.BLUE));

//            if (traitMod != 0) {
//                damageInfo.addComponent(new MessageComponent("-", ChatColor.COMBAT));
//                damageInfo.addComponent(new MessageComponent(String.valueOf(traitMod), ChatColor.PURPLE));
//            }

//            damageInfo.addComponent(new MessageComponent("=", ChatColor.COMBAT));
//            damageInfo.addComponent(new MessageComponent(String.valueOf(damageInt), ChatColor.DICE));


//            this.damage = damage;
            damage.process();
            this.damage = damage.resultDamage;
            damageInfo.addComponent(damage.damageMessage);
            System.out.println("TEST");
            // Добавляем строчку про защиту от сложности попадания
            if (defenseFromFirearmDifficulty) {
                damageInfo.addComponent(new MessageComponent("\nУрон считался от сложности попадания " + DataHolder.inst().getSkillTable().get(attackDifficulty) + ".", ChatColor.GRAY));
            }

            if (magicShield != 0) {
                if ((damage.resultDamage - magicShield) > 0) {
                    defender.adjustMagicShield(damage.resultDamage - magicShield);
                }
            }



            int bleedingLevel = 0;

            List<String> propertyList = new ArrayList<>();

            if (currentAttack.getWeapon().isMelee()) {
                if (currentAttack.getWeapon().getDamageType().equals(DamageType.NONLETHAL)) {
                    propertyList.add("[нелетальное оружие]");
                } else if (currentAttack.getWeapon().getDamageType().equals(DamageType.CRITICAL)) {
                    propertyList.add("[критическое оружие]");
                }
            } else if (currentAttack.getWeapon().isRanged()) {
                DamageType dt = currentAttack.getWeapon().getDamageType();
                if (currentAttack.getWeapon().getDamageType().getNum() > currentAttack.getWeapon().getFirearm().getLoadedProjectile().getDamageType().getNum()) {
                    dt = currentAttack.getWeapon().getFirearm().getLoadedProjectile().getDamageType();
                }
                if (dt.equals(DamageType.NONLETHAL)) {
                    propertyList.add("[нелетальное оружие]");
                } else if (dt.equals(DamageType.CRITICAL)) {
                    propertyList.add("[критическое оружие]");
                }
            }

            if(suppressing || modsplus.contains("ментальное")) propertyList.add("[ментальная рана]");

            if (!stunning && modsplus.contains("оглушение")) {
                if (damage.resultDamage > 6) {
                    if (!defender.cantBeStunned()) {
                        propertyList.add("<пропуск следующего хода>");
                        defender.addStatusEffect("оглушение", StatusEnd.TURN_END, 1, StatusType.STUNNED);
                        if (defender.getEngagedWith() != null) {
                            propertyList.add("<потеря связывания боем>");
                            defender.clearOpportunities();
                        }
                        if (defender.hasCourage()) {
                            propertyList.add("<потеря концентрации>");
                            defender.removeStatusEffect(StatusType.CONCENTRATED);
                        }
                        if (defender.hasStatusEffect(StatusType.PREPARING)) {
                            propertyList.add("<потеря подготовки>");
                            defender.removeStatusEffect(StatusType.PREPARING);
                        }
                    }
                }
            }

            if (globalBodyPart != null && damage.resultDamage > 3 && !suppressing && !stunning && !splash &&  multicast < 1) {
                switch (globalBodyPart) {
                    case STOMACH:
                        if (damage.resultDamage > 9) {
                            if (!defender.cantBleed()) bleedingLevel = Math.max(bleedingLevel, 10);
                        } else if (globalBodyPart.equals(BodyPart.STOMACH) && damage.resultDamage > 6) {
                            if (!defender.cantBleed()) bleedingLevel = Math.max(bleedingLevel, 5);
                        }
                        break;

                    case NECK:
                    case THORAX:
                        if (damage.resultDamage > 9) {
                            propertyList.add("[смертельная рана]");
                            if (!defender.cantBleed()) bleedingLevel = Math.max(bleedingLevel, 40);
                        } else if (damage.resultDamage > 6) {
                            if (!defender.cantBleed()) bleedingLevel = Math.max(bleedingLevel, 20);
                        } else if (damage.resultDamage > 3) {
                            if (!defender.cantBleed()) bleedingLevel = Math.max(bleedingLevel, 10);
                        }
                        break;

                    case GROIN:
                    case HEAD:
                        if (damage.resultDamage > 9) {
                            propertyList.add("[смертельная рана]");
                            if (!defender.cantBleed()) bleedingLevel = Math.max(bleedingLevel, 20);
                            if (!defender.cantBeStunned()) {
                                propertyList.add("<пропуск следующих трёх ходов>");
                                defender.addStatusEffect("оглушение", StatusEnd.TURN_END, 3, StatusType.STUNNED);
                                if (defender.getEngagedWith() != null) {
                                    propertyList.add("<потеря связывания боем>");
                                    defender.clearOpportunities();
                                }
                                if (defender.hasCourage()) {
                                    propertyList.add("<потеря концентрации>");
                                    defender.removeStatusEffect(StatusType.CONCENTRATED);
                                }
                                if (defender.hasStatusEffect(StatusType.PREPARING)) {
                                    propertyList.add("<потеря подготовки>");
                                    defender.removeStatusEffect(StatusType.PREPARING);
                                }
                            }
                        }
                        else if (damage.resultDamage > 6) {
                            if (!defender.cantBleed()) bleedingLevel = Math.max(bleedingLevel, 10);
                            if (!defender.cantBeStunned()) {
                                propertyList.add("<пропуск следующих двух ходов>");
                                defender.addStatusEffect("оглушение", StatusEnd.TURN_END, 2, StatusType.STUNNED);
                                if (defender.getEngagedWith() != null) {
                                    propertyList.add("<потеря связывания боем>");
                                    defender.clearOpportunities();
                                }
                                if (defender.hasCourage()) {
                                    propertyList.add("<потеря концентрации>");
                                    defender.removeStatusEffect(StatusType.CONCENTRATED);
                                }
                                if (defender.hasStatusEffect(StatusType.PREPARING)) {
                                    propertyList.add("<потеря подготовки>");
                                    defender.removeStatusEffect(StatusType.PREPARING);
                                }
                            }
                        } else if (damage.resultDamage > 3) {
                            if (!defender.cantBleed()) bleedingLevel = Math.max(bleedingLevel, 5);
                            if (!defender.cantBeStunned()) {
                                propertyList.add("<пропуск следующего хода>");
                                defender.addStatusEffect("оглушение", StatusEnd.TURN_END, 1, StatusType.STUNNED);
                                if (defender.getEngagedWith() != null) {
                                    propertyList.add("<потеря связывания боем>");
                                    defender.clearOpportunities();
                                }
                                if (defender.hasCourage()) {
                                    propertyList.add("<потеря концентрации>");
                                    defender.removeStatusEffect(StatusType.CONCENTRATED);
                                }
                                if (defender.hasStatusEffect(StatusType.PREPARING)) {
                                    propertyList.add("<потеря подготовки>");
                                    defender.removeStatusEffect(StatusType.PREPARING);
                                }
                            }
                        }
                        break;
                    default:
                        if (!defender.cantGetLimbInjury()) { //TODO доделать
                            if (globalBodyPart.equals(BodyPart.LEFT_ELBOW) && damage.resultDamage > 9)
                                propertyList.add("[потеря левой руки по локоть]");
                            else if (globalBodyPart.equals(BodyPart.LEFT_ELBOW) && damage.resultDamage > 6)
                                propertyList.add("[тяжелое повреждение левой руки: локоть]");
                            else if (globalBodyPart.equals(BodyPart.LEFT_ELBOW) && damage.resultDamage > 3)
                                propertyList.add("[легкое повреждение левой руки: локоть]");
                            else if (globalBodyPart.equals(BodyPart.LEFT_HAND) && damage.resultDamage > 9)
                                propertyList.add("[потеря левой кисти]");
                            else if (globalBodyPart.equals(BodyPart.LEFT_HAND) && damage.resultDamage > 6)
                                propertyList.add("[тяжелое повреждение левой руки: кисть]");
                            else if (globalBodyPart.equals(BodyPart.LEFT_HAND) && damage.resultDamage > 3)
                                propertyList.add("[легкое повреждение левой руки: кисть]");
                            else if (globalBodyPart.equals(BodyPart.RIGHT_ELBOW) && damage.resultDamage > 9)
                                propertyList.add("[потеря правой руки по локоть]");
                            else if (globalBodyPart.equals(BodyPart.RIGHT_ELBOW) && damage.resultDamage > 6)
                                propertyList.add("[тяжелое повреждение правой руки: локоть]");
                            else if (globalBodyPart.equals(BodyPart.RIGHT_ELBOW) && damage.resultDamage > 3)
                                propertyList.add("[легкое повреждение правой руки: локоть]");
                            else if (globalBodyPart.equals(BodyPart.RIGHT_HAND) && damage.resultDamage > 9)
                                propertyList.add("[потеря правой кисти]");
                            else if (globalBodyPart.equals(BodyPart.RIGHT_HAND) && damage.resultDamage > 6)
                                propertyList.add("[тяжелое повреждение правой руки: кисть]");
                            else if (globalBodyPart.equals(BodyPart.RIGHT_HAND) && damage.resultDamage > 3)
                                propertyList.add("[легкое повреждение правой руки: кисть]");
                            else if (globalBodyPart.equals(BodyPart.LEFT_KNEE) && damage.resultDamage > 9)
                                propertyList.add("[потеря левой ноги по колено]");
                            else if (globalBodyPart.equals(BodyPart.LEFT_KNEE) && damage.resultDamage > 6)
                                propertyList.add("[тяжелое повреждение левой ноги: колено]");
                            else if (globalBodyPart.equals(BodyPart.LEFT_KNEE) && damage.resultDamage > 3)
                                propertyList.add("[легкое повреждение левой ноги: колено]");
                            else if (globalBodyPart.equals(BodyPart.LEFT_FOOT) && damage.resultDamage > 9)
                                propertyList.add("[потеря левой стопы]");
                            else if (globalBodyPart.equals(BodyPart.LEFT_FOOT) && damage.resultDamage > 6)
                                propertyList.add("[тяжелое повреждение левой ноги: стопа]");
                            else if (globalBodyPart.equals(BodyPart.LEFT_FOOT) && damage.resultDamage > 3)
                                propertyList.add("[легкое повреждение левой ноги: стопа]");
                            else if (globalBodyPart.equals(BodyPart.RIGHT_KNEE) && damage.resultDamage > 9)
                                propertyList.add("[потеря правой ноги по колено]");
                            else if (globalBodyPart.equals(BodyPart.RIGHT_KNEE) && damage.resultDamage > 6)
                                propertyList.add("[тяжелое повреждение правой ноги: колено]");
                            else if (globalBodyPart.equals(BodyPart.RIGHT_KNEE) && damage.resultDamage > 3)
                                propertyList.add("[легкое повреждение правой ноги: колено]");
                            else if (globalBodyPart.equals(BodyPart.RIGHT_FOOT) && damage.resultDamage > 9)
                                propertyList.add("[потеря правой стопы]");
                            else if (globalBodyPart.equals(BodyPart.RIGHT_FOOT) && damage.resultDamage > 6)
                                propertyList.add("[тяжелое повреждение правой ноги: стопа]");
                            else if (globalBodyPart.equals(BodyPart.RIGHT_FOOT) && damage.resultDamage > 3)
                                propertyList.add("[легкое повреждение правой ноги: стопа]");
                            else if (globalBodyPart.equals(BodyPart.LEFT_FOREARM) && damage.resultDamage > 9)
                                propertyList.add("[тяжелое повреждение левой руки: предплечье]");
                            else if (globalBodyPart.equals(BodyPart.LEFT_FOREARM) && damage.resultDamage > 6)
                                propertyList.add("[легкое повреждение левой руки: предплечье]");
                            else if (globalBodyPart.equals(BodyPart.LEFT_SHOULDER) && damage.resultDamage > 9)
                                propertyList.add("[тяжелое повреждение левой руки: плечо]");
                            else if (globalBodyPart.equals(BodyPart.LEFT_SHOULDER) && damage.resultDamage > 6)
                                propertyList.add("[легкое повреждение левой руки: плечо]");
                            else if (globalBodyPart.equals(BodyPart.RIGHT_FOREARM) && damage.resultDamage > 9)
                                propertyList.add("[тяжелое повреждение правой руки: предплечье]");
                            else if (globalBodyPart.equals(BodyPart.RIGHT_FOREARM) && damage.resultDamage > 6)
                                propertyList.add("[легкое повреждение правой руки: предплечье]");
                            else if (globalBodyPart.equals(BodyPart.RIGHT_SHOULDER) && damage.resultDamage > 9)
                                propertyList.add("[тяжелое повреждение правой руки: плечо]");
                            else if (globalBodyPart.equals(BodyPart.RIGHT_SHOULDER) && damage.resultDamage > 6)
                                propertyList.add("[легкое повреждение правой руки: плечо]");
                            else if (globalBodyPart.equals(BodyPart.LEFT_SHIN) && damage.resultDamage > 9)
                                propertyList.add("[тяжелое повреждение левой ноги: голень]");
                            else if (globalBodyPart.equals(BodyPart.LEFT_SHIN) && damage.resultDamage > 6)
                                propertyList.add("[легкое повреждение левой ноги: голень]");
                            else if (globalBodyPart.equals(BodyPart.LEFT_HIP) && damage.resultDamage > 9)
                                propertyList.add("[тяжелое повреждение левой ноги: бедро]");
                            else if (globalBodyPart.equals(BodyPart.LEFT_HIP) && damage.resultDamage > 6)
                                propertyList.add("[легкое повреждение левой ноги: бедро]");
                            else if (globalBodyPart.equals(BodyPart.RIGHT_SHIN) && damage.resultDamage > 9)
                                propertyList.add("[тяжелое повреждение правой ноги: голень]");
                            else if (globalBodyPart.equals(BodyPart.RIGHT_SHIN) && damage.resultDamage > 6)
                                propertyList.add("[легкое повреждение правой ноги: голень]");
                            else if (globalBodyPart.equals(BodyPart.RIGHT_HIP) && damage.resultDamage > 9)
                                propertyList.add("[тяжелое повреждение правой ноги: бедро]");
                            else if (globalBodyPart.equals(BodyPart.RIGHT_HIP) && damage.resultDamage > 6)
                                propertyList.add("[легкое повреждение правой ноги: бедро]");
                        }
                }
            }
            System.out.println("TEST");
            // Если уже нанесено более сильное кровотечение от точечной, то не меняем. Иначе меняем
            if (damage.resultDamage > 3 && !stunning && !suppressing){
                if (!currentAttack.weapon.isRanged()) {
                    if (currentAttack.weapon.isMelee() && !defender.cantBleed()) {
                        try {
                            if (currentAttack.weapon.getMelee().getCategory().equals(MeleeCategory.DISSECTING)) {
                                if (damage.resultDamage > 9) {
                                    bleedingLevel = Math.max(bleedingLevel, 20);
                                } else if (damage.resultDamage > 6) {
                                    bleedingLevel = Math.max(bleedingLevel, 10);
                                } else if (damage.resultDamage > 3) {
                                    bleedingLevel = Math.max(bleedingLevel, 5);
                                } else if (damage.resultDamage >= 0) {
                                    bleedingLevel = Math.max(bleedingLevel, 1);
                                }

                            } else if (currentAttack.weapon.getMelee().getCategory().equals(MeleeCategory.SLASHING)) { //TODO: тест
                                if (damage.resultDamage > 9) {
                                    bleedingLevel = Math.max(bleedingLevel, 10);
                                } else if (damage.resultDamage > 6) {
                                    bleedingLevel = Math.max(bleedingLevel, 5);
                                } else if (damage.resultDamage > 3) {
                                    bleedingLevel = Math.max(bleedingLevel, 1);
                                }
                            }

                        } catch (Exception e) {
                            System.out.println(e);
                        }
                    }
                } else if (!currentAttack.weapon.getFirearm().isModern()) {
                    Projectile ammo = currentAttack.getWeapon().getFirearm().getLoadedProjectile();
                    if (ammo.getMod().contains("зазубр") && !defender.cantBleed()) {
                        if (damage.resultDamage > 9) {
                            bleedingLevel = Math.max(bleedingLevel, 20);
                        } else if (damage.resultDamage > 6) {
                            bleedingLevel = Math.max(bleedingLevel, 10);
                        } else if (damage.resultDamage > 3) {
                            bleedingLevel = Math.max(bleedingLevel, 5);
                        } else if (damage.resultDamage >= 0) {
                            bleedingLevel = Math.max(bleedingLevel, 1);
                        }
                    }
                    if (ammo.getMod().contains("режущее") && !defender.cantBleed()) { //TODO: тест
                        if (damage.resultDamage > 9) {
                            bleedingLevel = Math.max(bleedingLevel, 10);
                        } else if (damage.resultDamage > 6) {
                            bleedingLevel = Math.max(bleedingLevel, 5);
                        } else if (damage.resultDamage > 3) {
                            bleedingLevel = Math.max(bleedingLevel, 1);
                        }
                    }
                    if (ammo.getCaliber().equals("сигнальный заряд")) {
                        if (damage.resultDamage > 3 && !defender.cantBurn()) {
                            propertyList.add("<горение>");
                            defender.addStatusEffect("горение", StatusEnd.TURN_END, 5, StatusType.BURNING);
                        }
                    } else if (ammo.getMod().contains("зажигательн")) {
                        if (damage.resultDamage > 6 && !defender.cantBurn()) {
                            propertyList.add("<горение>");
                            defender.addStatusEffect("горение", StatusEnd.TURN_END, 5, StatusType.BURNING);
                        }
                    }
                } else if (currentAttack.weapon.getFirearm().isModern()) {
                    Projectile ammo = currentAttack.getWeapon().getFirearm().getLoadedProjectile();
                    if (ammo.getMod().contains("зажигательн") || ammo.getMod().contains("бтз") || ammo.getCaliber().equals("сигнальный заряд")) {
                        if (damage.resultDamage > 6 && !defender.cantBurn()) {
                            propertyList.add("<горение>");
                            defender.addStatusEffect("горение", StatusEnd.TURN_END, 5, StatusType.BURNING);
                        }
                    }
                }
            }

            if(!modsplus.isEmpty()) {
                if (modsplus.contains("огонь") && !defender.cantBurn()) {
                    if (damage.resultDamage > 6) {
                        if (!propertyList.contains("<горение>")) {
                            propertyList.add("<горение>");
                            defender.addStatusEffect("горение", StatusEnd.TURN_END, 5, StatusType.BURNING);
                        }
                    }
                }
                for (String mod : modsplus) {
                    if(!mod.isEmpty()) propertyList.add("{" + mod + "} ");
                }
            }

            //Оглушение
            if (stunning && !suppressing) {
                bleedingLevel = 0;
                if ((globalBodyPart == BodyPart.HEAD || globalBodyPart == BodyPart.GROIN)) {
                    if (damage.resultDamage > 9) propertyList.add("[нокаут]");
                    else if (damage.resultDamage > 6) {
                        if (!defender.cantBeStunned()) {
                            propertyList.add("<пропуск следующих двух ходов>");
                            defender.addStatusEffect("оглушение", StatusEnd.TURN_END, 2, StatusType.STUNNED);
                            if (defender.getEngagedWith() != null) {
                                propertyList.add("<потеря связывания боем>");
                                defender.clearOpportunities();
                            }
                            if (defender.hasCourage()) {
                                propertyList.add("<потеря концентрации>");
                                defender.removeStatusEffect(StatusType.CONCENTRATED);
                            }
                            if (defender.hasStatusEffect(StatusType.PREPARING)) {
                                propertyList.add("<потеря подготовки>");
                                defender.removeStatusEffect(StatusType.PREPARING);
                            }
                        }
                    } else if (damage.resultDamage > 3) {
                        if (!defender.cantBeStunned()) {
                            propertyList.add("<пропуск следующего хода>");
                            defender.addStatusEffect("оглушение", StatusEnd.TURN_END, 1, StatusType.STUNNED);
                            if (defender.getEngagedWith() != null) {
                                propertyList.add("<потеря связывания боем>");
                                defender.clearOpportunities();
                            }
                            if (defender.hasCourage()) {
                                propertyList.add("<потеря концентрации>");
                                defender.removeStatusEffect(StatusType.CONCENTRATED);
                            }
                            if (defender.hasStatusEffect(StatusType.PREPARING)) {
                                propertyList.add("<потеря подготовки>");
                                defender.removeStatusEffect(StatusType.PREPARING);
                            }
                        }
                    }
                } else if (damage.resultDamage > 6) {
                    if (!defender.cantBeStunned()) {
                        propertyList.add("<пропуск следующего хода>");
                        defender.addStatusEffect("пропуск хода", StatusEnd.TURN_END, 1, StatusType.STUNNED);
                        if (defender.getEngagedWith() != null) {
                            propertyList.add("<потеря связывания боем>");
                            defender.clearOpportunities();
                        }
                        if (defender.hasCourage()) {
                            propertyList.add("<потеря концентрации>");
                            defender.removeStatusEffect(StatusType.CONCENTRATED);
                        }
                        if (defender.hasStatusEffect(StatusType.PREPARING)) {
                            propertyList.add("<потеря подготовки>");
                            defender.removeStatusEffect(StatusType.PREPARING);
                        }
                    }
                }
                if (damage.resultDamage > 9) {
                    propertyList.add("[нокаут]");
                } else if (damage.resultDamage > 6) {
                    propertyList.add("[оглушение:станет переходной через 5 ходов]");
                } else if (damage.resultDamage > 3) {
                    propertyList.add("[оглушение:станет переходной через 3 хода]");
                } else if (damage.resultDamage >= 0) {
                    propertyList.add("[оглушение:станет переходной через ход]");
                }
            }

            if (bleedingLevel != 0) {
                switch (bleedingLevel) {
                    case 1:
                        defender.addStatusEffect("незначительное кровотечение", StatusEnd.TURN_END, 5, StatusType.BLEEDING, bleedingLevel);
                        propertyList.add("<незначительное кровотечение>");
                        break;
                    case 5:
                        defender.addStatusEffect("слабое кровотечение", StatusEnd.TURN_END, 5, StatusType.BLEEDING, bleedingLevel);
                        propertyList.add("<слабое кровотечение>");
                        break;
                    case 10:
                        defender.addStatusEffect("сильное кровотечение", StatusEnd.TURN_END, 5, StatusType.BLEEDING, bleedingLevel);
                        propertyList.add("<сильное кровотечение>");
                        break;
                    case 20:
                        defender.addStatusEffect("критическое кровотечение", StatusEnd.TURN_END, 5, StatusType.BLEEDING, bleedingLevel);
                        propertyList.add("<критическое кровотечение>");
                        break;
                    case 40:
                        defender.addStatusEffect("смертельное кровотечение", StatusEnd.TURN_END, 5, StatusType.BLEEDING, bleedingLevel);
                        propertyList.add("<смертельное кровотечение>");
                        break;
                    default:
                        defender.addStatusEffect("кровотечение", StatusEnd.TURN_END, 5, StatusType.BLEEDING, bleedingLevel);
                        propertyList.add("<кровотечение>");
                        break;
                }
            }

            // Добавляем строчку с результатом точечной
            StringBuilder property = new StringBuilder();
            if (!propertyList.isEmpty()) {
                for (String pr : propertyList) {
                    if (!pr.equals("")) {
                        property.append(" ");
                        property.append(pr);
                    }
                }
            }
            if (actualAttacker.getModifiersState().getModifier(ModifiersState.bodypart) != null) {
                if (targetedInfo != null)
                    damageInfo.addComponent(targetedInfo);
            }
//            if (!property.toString().equals("")) {
//                damageInfo.addComponent(new MessageComponent("\nНа рану наложен модификатор" + property.toString(), ChatColor.RED));
//            }
            if (shieldMod != 0) {
                GunCus.channel.sendToAllAround(new MessageSound(defenderEntity, GunCus.SOUND_SHIELDBEATSOUND, 2F, 1F), new NetworkRegistry.TargetPoint(defenderEntity.world.provider.getDimension(), defenderEntity.posX, defenderEntity.posY, defenderEntity.posZ, 16F));
            } else if (currentAttack.weapon.getName().equals("fist")) {
                GunCus.channel.sendToAllAround(new MessageSound(defenderEntity, GunCus.SOUND_PUNCH_HIT, 2F, 1F), new NetworkRegistry.TargetPoint(defenderEntity.world.provider.getDimension(), defenderEntity.posX, defenderEntity.posY, defenderEntity.posZ, 16F));
            } else if (currentAttack.weapon.isMelee()) {
                GunCus.channel.sendToAllAround(new MessageSound(defenderEntity, currentAttack.getWeapon().getSound() + "_hit", 3F, 1F), new NetworkRegistry.TargetPoint(defenderEntity.world.provider.getDimension(), defenderEntity.posX, defenderEntity.posY, defenderEntity.posZ, 16F));
            } else if (currentAttack.weapon.isRanged()) {
                GunCus.channel.sendToAllAround(new MessageSound(defenderEntity, currentAttack.getWeapon().getSound() + "_hit", 3F, 1F), new NetworkRegistry.TargetPoint(defenderEntity.world.provider.getDimension(), defenderEntity.posX, defenderEntity.posY, defenderEntity.posZ, 16F));
            }

            if (redirectedInfo != null) damageInfo.addComponent(redirectedInfo);

            if (defense.getDefenseType() != DefenseType.NOTHING) {
                if ((currentAttack.getDiceMessage().getDice().getResult() < defense.getDiceMessage().getDice().getResult() && defense.getDefenseType() == DefenseType.CONSTITUTION) || (currentAttack.getDiceMessage().getDice().getResult() <= defense.getDiceMessage().getDice().getResult() && defense.getDefenseType() == DefenseType.BLOCKING)) {
                    if (!DataHolder.inst().isNpc(actualDefender.getName()) && !defense.getDiceMessage().getDice().getSkillName().isEmpty() && DataHolder.inst().areSkillsComparable(defense.getDiceMessage(), currentAttack.getDiceMessage())) {
                        //тут прокач
                        System.out.println("TEST EXP");
                        if (Helpers.hasSpaceForXp(defender.getName())) {
                            int exp = 0;
                            int customDamage = 0;
                            if (actualAttacker.getModifiersState().isModifier("damage")) {
                                customDamage = actualAttacker.getModifiersState().getModifier("damage");
                            }
                            switch (defender.getSkillLevelAsInt(defense.getDiceMessage().getDice().getSkillName())) {
                                case -1:
                                    if (currentAttack.getWeapon().getDamage() >= 0 || customDamage >= 0) {
                                        exp = 8;
                                    }
                                    break;
                                case 0:
                                    if (currentAttack.getWeapon().getDamage() >= 2 || customDamage >= 2) {
                                        exp = 4;
                                    }
                                    break;
                                case 1:
                                    if ((currentAttack.getWeapon().getDamage() >= 4 || customDamage >= 4) && ((!stunning && !suppressing) || defender.getSkill(defense.getDiceMessage().getDice().getSkillName()).getPercent() < 50)) {
                                        exp = 2;
                                    }
                                    break;
                                case 2:
                                    if ((currentAttack.getWeapon().getDamage() >= 6 || customDamage >= 6) && !stunning && !suppressing) {
                                        exp = 1;
                                    }
                                    break;
                            }
                            if (exp != 0) {
                                if (Helpers.addXp(defender.getName(), defense.getDiceMessage().getDice().getSkillName() + ":" + exp)) {
                                    damageInfo.addComponent(new MessageComponent(" "));
                                    MessageComponent expComp = new MessageComponent("[*]", ChatColor.NICK);
                                    expComp.setHoverText(actualDefender.getName() + ": +" + exp + "‰ опыта к " + defense.getDiceMessage().getDice().getSkillName(), TextFormatting.GREEN);
                                    expComp.setUnderlined(true);

                                    damageInfo.addComponent(expComp);
                                }
                            }
                        }
                    }
                } else {
                    if (currentAttack.getDiceMessage().getDice().getSkillName().startsWith("владение") || currentAttack.getDiceMessage().getDice().getSkillName().startsWith("метание") || currentAttack.getDiceMessage().getDice().getSkillName().startsWith("рукопашный бой")) {
                        if (!DataHolder.inst().isNpc(actualAttacker.getName()) && DataHolder.inst().areSkillsComparable(currentAttack.getDiceMessage(), defense.getDiceMessage())) {
                            //тут прокач
                            System.out.println("TEST EXP " + Helpers.hasSpaceForXp(attacker.getName()));
                            if (Helpers.hasSpaceForXp(attacker.getName())) {
                                int exp = 0;
                                switch (attacker.getSkillLevelAsInt(currentAttack.getDiceMessage().getDice().getSkillName())) {
                                    case -1:
                                        if (damage.resultDamage >= 0) {
                                            exp = 8;
                                        }
                                        break;
                                    case 0:
                                        if (damage.resultDamage >= 0) {
                                            exp = 4;
                                        }
                                        break;
                                    case 1:
                                        if (damage.resultDamage > 3 && ((!stunning && !suppressing) || attacker.getSkill(currentAttack.getDiceMessage().getDice().getSkillName()).getPercent() < 50)) {
                                            exp = 2;
                                        }
                                        break;
                                    case 2:
                                        if (damage.resultDamage > 6 && !stunning && !suppressing) {
                                            exp = 1;
                                        }
                                        break;
                                }
                                if (exp != 0) {
                                    if (Helpers.addXp(actualAttacker.getName(), currentAttack.getDiceMessage().getDice().getSkillName() + ":" + exp)) {
                                        damageInfo.addComponent(new MessageComponent(" "));
                                        MessageComponent expComp = new MessageComponent("[*]", ChatColor.NICK);
                                        expComp.setHoverText(actualAttacker.getName() + ": +" + exp + "‰ опыта к " + currentAttack.getDiceMessage().getDice().getSkillName(), TextFormatting.GREEN);
                                        expComp.setUnderlined(true);

                                        damageInfo.addComponent(expComp);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            System.out.println(dicesMegaString);
            if (!dicesMegaString.isEmpty()) {
                damageInfo.addComponent(new MessageComponent(" "));
                MessageComponent dice = new MessageComponent("[dX?]", ChatColor.STORY);
                dice.setUnderlined(true);
                dice.setHoverText(dicesMegaString.trim(), TextFormatting.RED);
                damageInfo.addComponent(dice);
            }

            // Передать раны вышестоящей функции
            if (damage.resultDamage >= 0) {
                wounds.add(new MutablePair(damage.resultDamage, actualAttacker.getName() + " " + currentAttack.getWeapon().getName() + property.toString()));
            }
        }

        System.out.println("TEST");


        attacker.getPercentDice().setAmmo(0);
//        attacker.getPercentDice().setBipod(0);
        attacker.getPercentDice().setLimbInjury(0);
        attacker.getPercentDice().setStock(0);
        attacker.getPercentDice().setRipost(0);
        return;
    }


    private void end() {
        this.ended = true;
    }


    public Player getAttacker() {
        return attacker;
    }

    public void setAttacker(Player attacker) {
        this.attacker = attacker;
    }

    public Player getDefender() {
        return defender;
    }

    public void setDefender(Player defender) {
        this.defender = defender;
    }

    public boolean isEnded() {
        return ended;
    }

    public void setEnded(boolean ended) {
        this.ended = ended;
    }

    public void setActiveHand(int hand) {
        System.out.println("Active hand set to " + hand);
        this.activeHand = hand;
    }

    public int getActiveHand() {
        return activeHand;
    }

    public int getActualActiveHand() {
        if (activeHand == 2){
            if(activeAttackNumber == 1) return 0; //второй раз стреляем левой при парной
            return 1;
        }
        return activeHand;
    }

    public void initDefense() {
        if (!DataHolder.inst().getDuels().contains(this)) DataHolder.inst().registerDuel(this);
        Message info = new Message("Тебя атакуют, защищайся!", ChatColor.BLUE);
        Player realDefender = defender;
        System.out.println("test" + defender == null);
        System.out.println("test" + defender.getName());
        if (DataHolder.inst().isNpc(defender.getName())) {
            realDefender = DataHolder.inst().getMasterForNpcInCombat(defender);
            realDefender.setSubordinate(defender);
            System.out.println("Set subordinate of " + realDefender.getName() + " as " + defender.getName());
        }
        realDefender.sendMessage(info);
        realDefender.performCommand(String.format("/%s", SelectDefenseCommand.NAME));
        GunCus.channel.sendTo(new MessageSoundReg(GunCus.SOUND_RING_LOW, 1F, 1F), ServerProxy.getForgePlayer(realDefender.getName()));
        initiated = true;
    }

    public void setDefense(Defense defense) {
        this.defense = defense;
    }

    public String getDefenseInfo() {
        return defense.getDiceMessage().toString();
    }

    public LinkedList<Attack> getAttacks() {
        return attacks;
    }

    public void setAttacks(LinkedList<Attack> attacks) {
        this.attacks = attacks;
    }

    public Defense getDefense() {
        return defense;
    }

    public int getActiveAttackNumber() {
        return activeAttackNumber;
    }

    public void setActiveAttackNumber(int activeAttackNumber) {
        this.activeAttackNumber = activeAttackNumber;
    }

    public void incrementAttackNumber() {
        this.activeAttackNumber++;
    }

    public String getAttackInfo() {
        return attacks.get(activeAttackNumber).getDiceMessage().toString();
    }

    public Attack getActiveAttack() {
        return attacks.get(activeAttackNumber);
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    @Override
    public String toString() {
        String defenderName = "null";
        if (defender != null) defenderName = defender.getDisplayedName();
        return "Duel{" +
                "attacker=" + attacker.getDisplayedName() +
                ", defender=" + defenderName +
                '}';
    }

    public void setRangeInfo(Message rangeInfo) {
        this.rangeInfo = rangeInfo;
    }

    public Message getRangeInfo() {
        return rangeInfo;
    }

//    public void setMasterInfo(Message masterInfo) {
//        this.masterInfo = masterInfo;
//    }

    public boolean shouldBreak() {
        return shouldBreak;
    }

    public boolean shouldNotBreak() {
        return shouldNotBreak;
    }
    public boolean isNoRecoil() {
        return noRecoil;
    }

    public void setShouldNotBreak(boolean notBreak) {
        this.shouldNotBreak = notBreak;
    }

    public List<MutablePair<Integer, String>> getWounds() {
        return wounds;
    }

    public List<MutablePair<String, Integer>> getCooldowns() {
        return cooldowns;
    }

    private static void swap(PlayerWrapper pw1, PlayerWrapper pw2) {
        Player temp = pw1.p;
        pw1.p = pw2.p;
        pw2.p = temp;
    }

    public boolean hasReachedDifficulty() {
        return reachedDifficulty;
    }

    public boolean hasCharged() {
        return hasCharged;
    }

    public void setSplash(boolean splash) {
        this.splash = splash;
    }

    public void setPiercing(boolean piercing) {
        this.piercing = piercing;
    }

    public void setMultishot(boolean multishot) {
        this.multishot = multishot;
    }

    public boolean hasMultishot() {
        return multishot;
    }


    public void setMulticast(int multicast) {
        this.multicast = multicast;
    }

    public int getMulticast() {
        return multicast;
    }

    public void setFullcover(int fullcover) {
        this.fullcover = fullcover;
    }

    public int getFullcover() {
        return fullcover;
    }

    public void setSuccessfulCounter(boolean successfulCounter) {
        this.successfulCounter = successfulCounter;
    }

    public boolean isSuccessfulCounter() {
        return successfulCounter;
    }

    public boolean hasSplash() {
        return splash;
    }
    public boolean hasPiercing() {
        return piercing;
    }
    public void setStunOp(boolean stunOp) {
        this.stunOp = stunOp;
    }

    public boolean stunOp() {
        return stunOp;
    }

    public void setOpportunity(boolean isOpportunity) {
        this.isOpportunity = isOpportunity;
    }

    public boolean isOpportunity() {
        return isOpportunity;
    }

    public boolean successfulRiposte() {
        return riposte;
    }

    public void setOldAttackResult(FudgeDiceMessage oldAttackResult) {
        this.oldAttackResult = oldAttackResult;
    }

    public void setOldDefenseResult(FudgeDiceMessage oldDefenseResult) {
        this.oldDefenseResult = oldDefenseResult;
    }

    public FudgeDiceMessage getOldAttackResult() {
        return oldAttackResult;
    }

    public FudgeDiceMessage getOldDefenseResult() {
        return oldDefenseResult;
    }

    public boolean jammed() {
        return jammed;
    }
    /*
    public void setCustomModifierFor(String name, String modifierType, int modifier) {
        System.out.println("modifierType: " + modifierType);
        if (modifierType.equals("bodypart")) {
            System.out.println("HEY HEY HEY");
            System.out.println("Bodypart set to " + modifier);
            this.isTargeted = true;
            this.targetedPartInt = modifier;
            return;
        }
        if (attacker.getName().equals(name)) {
            setCustomModifierForAttacker(modifierType, modifier);
            return;
        }
        if (defender.getName().equals(name)) {
            setCustomModifierForDefender(modifierType, modifier);
        }
    }

    private void setCustomModifierForDefender(String modifierType, int modifier) {
        switch (modifierType) {
            case "dice":
                defenderDiceMod = modifier;
                break;
            case "damage":
                defenderDamageMod = modifier;
                break;
        }
    }

    private void setCustomModifierForAttacker(String modifierType, int modifier) {
        switch (modifierType) {
            case "dice":
                attackerDiceMod = modifier;
                break;
            case "damage":
                attackerDamageMod = modifier;
                break;
        }
    }
    */

    public Duel copy() {
        Duel duel = new Duel(attacker);
        duel.setActiveHand(activeHand);
        duel.setDefender(defender);
        duel.setSplash(splash);
        duel.setPiercing(piercing);
        duel.setMultishot(multishot);
        duel.setMulticast(multicast);
        duel.setActiveAttackNumber(activeAttackNumber);
        duel.setAttacks(attacks);
        duel.setDamage(damage);
        duel.setDefense(defense);
        duel.setDamageInfo(damageInfo);
        duel.setEnded(ended);
        duel.setShouldNotBreak(shouldNotBreak);
        duel.setRangeInfo(rangeInfo);
        duel.setDefended(defended);
        return duel;
    }

    public boolean isUncommon() {
        return isOpportunity||multishot||multicast>0||splash;
    }

    public boolean dontGivePreviousDebuff() {
        return isOpportunity||multishot||multicast>0||splash;
    }

    public boolean dontCheckPreviousDebuff() {
        return multishot||multicast>0||splash;
    }

    public void setBeingDefended(Player beingDefended) {
        this.beingDefended = beingDefended;
    }

    public Player getBeingDefended() {
        return beingDefended;
    }

    public boolean getDefended() {
        return defended;
    }

    public void setDefended(boolean defended) {
        this.defended = defended;
    }

    public Duel getNextDuelDefended() {
        return nextDuel;
    }


}
