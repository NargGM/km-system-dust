package ru.konungstvo.combat.duel;

import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.dice.FudgeDice;
import ru.konungstvo.combat.equipment.AttackType;

public class DuelCalculator {

    static class Damage {

        public AttackType priority = AttackType.PHYSIC;
        public int resultDamage;
        public Message damageMessage;

        protected int diffInt = -666;
        protected String diffString = "";

        public void setPriority(AttackType priority) {
            this.priority = priority;
        }

        public void setDiff(int i, String s) {
            diffInt = i;
            diffString = s;
        }

        protected int weaponInt = -666;
        protected String weaponString = "";

        public void setWeapon(int i, String s) {
            weaponInt = i;
            weaponString = s;
        }

        protected int psychDamageInt = -666;
        protected String psychDamageString = "";

        public void setPsychDamage(int i, String s) {
            psychDamageInt = i;
            psychDamageString = s;
        }


        protected int defenseInt = 0;
        protected String defenseString = "физическая броня";

        public void setDefense(int i, String s) {
            defenseInt = -i;
            defenseString = s;
        }

        protected int psychDefenseInt = 0;
        protected String psychDefenseString = "";

        public void setPsychDefence(int i, String s) {
            psychDefenseInt = -i;
            psychDefenseString = s;
        }

        protected int armorInt = -666;
        protected String armorString = "";

        public void setArmor(int i, String s) {
            armorInt = i;
            armorString = s;
        }

        protected int targetedInt = -666;
        protected String targetedString = "";

        public void setFailedTargeted(int i, String s) {
            targetedInt = i;
            targetedString = s;
        }

        protected int targetedChopInt = -666;
        protected String targetedChopString = "";

        public void setTargetedChop(int i, String s) {
            targetedChopInt = i;
            targetedChopString = s;
        }

        protected int stunningCrushInt = -666;
        protected String stunningCrushString = "";

        public void setStunningCrush(int i, String s) {
            stunningCrushInt = i;
            stunningCrushString = s;
        }

        protected int targetedJointInt = -666;
        protected String targetedJointString = "";

        public void setTargetedJoint(int i, String s) {
            targetedJointInt = i;
            targetedJointString = s;
        }

        protected int hardenedInt = -666;
        protected String hardenedString = "";

        public void setHardened(int i, String s) {
            hardenedInt = i;
            hardenedString = s;
        }

        protected int shieldInt = -666;
        protected String shieldString = "";

        public void setShield(int i, String s) {
            shieldInt = i;
            shieldString = s;
        }

        protected int coverInt = -666;
        protected String coverString = "";

        public void setCover(int i, String s) {
            coverInt = i;
            coverString = s;
        }

        protected int apInt = -666;
        protected String apString = "";

        public void setAp(int i, String s) {
            apInt = i;
            apString = s;
        }


        protected int expInt = -666;
        protected String expString = "";

        public void setExp(int i, String s) {
            expInt = i;
            expString = s;
        }

        protected int nonlethalInt = -666;
        protected String nonlethalString = "";

        public void setNonlethal(int i, String s) {
            nonlethalInt = i;
            nonlethalString = s;
        }

        protected int burstInt = -666;
        protected String  burstString = "";

        public void setBurst(int i, String s) {
            burstInt = i;
            burstString = s;
        }


        protected int stunningWithoutCrushingInt = -666;
        protected String stunningWithoutCrushingString = "";

        public void setStunningWithoutCrushing(int i, String s) {
            stunningWithoutCrushingInt = i;
            stunningWithoutCrushingString = s;
        }

        protected int stunningWithSharpInt = -666;
        protected String stunningWithSharpString = "";

        public void setStunningWithSharp(int i, String s) {
            stunningWithSharpInt = i;
            stunningWithSharpString = s;
        }

        protected int brokenWeaponInt = -666;
        protected String brokenWeaponString = "";

        public void setBrokenWeapon(int i, String s) {
            stunningWithSharpInt = i;
            stunningWithSharpString = s;
        }

        protected int chargingInt = -666;
        protected String chargingString = "";

        public void setCharging(int i, String s) {
            chargingInt = i;
            chargingString = s;
        }

        protected int capturingInt = -666;
        protected String capturingString = "";

        public void setCapturing(int i, String s) {
            capturingInt = i;
            capturingString = s;
        }

        protected int stunningCrushPlusInt = -666;
        protected String stunningCrushPlusString = "";

        public void setStunningCrushPlus(int i, String s) {
            stunningCrushPlusInt = i;
            stunningCrushPlusString = s;
        }

        protected int electricityInt = -666;
        protected String electricityString = "";

        public void setElectricity(int i, String s) {
            electricityInt = i;
            electricityString = s;
        }

        protected int falloffInt = -666;
        protected String falloffString = "";

        public void setFalloff(int i, String s) {
            falloffInt = i;
            falloffString = s;
        }

        protected int resInt = -666;
        protected String resString = "";

        public void setRes(int i, String s) {
            resInt = i;
            resString = s;
        }

        protected int weaponTypeResInt = -666;
        protected String weaponTypeResString = "";

        public void setWeaponTypeRes(int i, String s) {
            weaponTypeResInt = i;
            weaponTypeResString = s;
        }

        protected int backstabInt = -666;
        protected String backstabString = "";

        public void setBackstab(int i, String s) {
            backstabInt = i;
            backstabString = s;
        }

        protected int stunningHeadInt = -666;
        protected String stunningHeadString = "";

        public void setStunningHead(int i, String s) {
            stunningHeadInt = i;
            stunningHeadString = s;
        }

        protected int magicShieldInt = -666;
        protected String magicShieldString = "";

        public void setMagicShield(int i, String s) {
            magicShieldInt = i;
            magicShieldString = s;
        }

        protected int constitutionInt = -666;
        protected String constitutionString = "";

        public void setConstitution(int i, String s) {
            constitutionInt = i;
            constitutionString = s;
        }

        protected int fullcoverInt = -666;
        protected String fullcoverString = "";

        public void setFullcover(int i, String s) {
            fullcoverInt = i;
            fullcoverString = s;
        }

        public void process() {
            processInts();
            processStrings();
        }

        public void processInts() {
            resultDamage =
                    diffInt +
                            weaponInt +
                            defenseInt + psychDefenseInt + psychDamageInt
            ;
            if (armorInt != -666) {
                resultDamage += armorInt;
            }
            if (targetedInt != -666) {
                resultDamage += targetedInt;
            }
            if (targetedChopInt != -666) {
                resultDamage += targetedChopInt;
            }
            if (stunningCrushInt != -666) {
                resultDamage += stunningCrushInt;
            }
            if (targetedJointInt != -666) {
                resultDamage += targetedJointInt;
            }
            if (hardenedInt != -666) {
                resultDamage += hardenedInt;
            }
            if (shieldInt != -666) {
                resultDamage += shieldInt;
            }
            if (coverInt != -666) {
                resultDamage += coverInt;
            }
            if (apInt != -666) {
                resultDamage += apInt;
            }
            if (expInt != -666) {
                resultDamage += expInt;
            }
            if (nonlethalInt != -666) {
                resultDamage += nonlethalInt;
            }
            if (burstInt != -666) {
                resultDamage += burstInt;
            }

            if (stunningWithoutCrushingInt != -666) {
                resultDamage += stunningWithoutCrushingInt;
            }
            if (stunningWithSharpInt != -666) {
                resultDamage += stunningWithSharpInt;
            }
            if (brokenWeaponInt != -666) {
                resultDamage += brokenWeaponInt;
            }
            if (chargingInt != -666) {
                resultDamage += chargingInt;
            }
            if (capturingInt != -666) {
                resultDamage += capturingInt;
            }

            if (stunningCrushPlusInt != -666) {
                resultDamage += stunningCrushPlusInt;
            }

            if (electricityInt != -666) {
                resultDamage += electricityInt;
            }

            if (falloffInt != -666) {
                resultDamage += falloffInt;
            }

            if (resInt != -666) {
                resultDamage += resInt;
            }

            if (weaponTypeResInt != -666) {
                resultDamage += weaponTypeResInt;
            }

            if (backstabInt != -666) {
                resultDamage += backstabInt;
            }

            if (stunningHeadInt != -666) {
                resultDamage += stunningHeadInt;
            }

            if (magicShieldInt != -666) {
                resultDamage += magicShieldInt;
            }

            if (constitutionInt != -666) {
                resultDamage += constitutionInt;
            }

            if (fullcoverInt != -666) {
                resultDamage += fullcoverInt;
            }
        }

        public void processStrings() {
            Message message = new Message("");
            MessageComponent component = new MessageComponent(String.valueOf(diffInt), ChatColor.YELLOW);

            String diffPlus = "";
            if (diffInt >= 0) diffPlus = "+";
            String weaponPlus = "";
            if (weaponInt >= 0) weaponPlus = "+";
            String psychPlus = "";
            if (psychDamageInt >= 0) psychPlus = "+";
            if (diffInt == -666) diffInt = 0;

            TextComponentString hover = new TextComponentString(TextFormatting.GRAY + "Расчет урона:");
            hover.appendSibling(new TextComponentString("\n" + TextFormatting.YELLOW + "" + diffInt + " " + diffString));
            if (priority.equals(AttackType.PHYSIC)) {
                hover.appendSibling(new TextComponentString("\n" + TextFormatting.DARK_RED + weaponPlus + weaponInt + " " + weaponString));
            }
            if (psychDamageInt != 0 || !psychDamageString.equals("психический урон"))
                hover.appendSibling(new TextComponentString("\n" + TextFormatting.LIGHT_PURPLE + psychPlus + psychDamageInt + " " + psychDamageString));
            if (!priority.equals(AttackType.PHYSIC)) {
                if (weaponInt != 0 || !weaponString.equals("физический урон")) hover.appendSibling(new TextComponentString("\n" + TextFormatting.DARK_RED + weaponPlus + weaponInt + " " + weaponString));
            }
            if (priority.equals(AttackType.PHYSIC)) {
                hover.appendSibling(new TextComponentString("\n" + TextFormatting.GOLD + "" + defenseInt + " " + defenseString));
            }
            if (psychDefenseInt != 0 || !priority.equals(AttackType.PHYSIC) || !psychDefenseString.equals("психическая броня")) hover.appendSibling(new TextComponentString( "\n" + TextFormatting.DARK_PURPLE + "" + psychDefenseInt + " " + psychDefenseString));
            if (!priority.equals(AttackType.PHYSIC)) {
                if (defenseInt != 0 || !defenseString.equals("физическая броня")) hover.appendSibling(new TextComponentString("\n" + TextFormatting.GOLD + "" + defenseInt + " " + defenseString));
            }
            if (armorInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (armorInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + armorInt + " " + armorString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }
            if (targetedInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (targetedInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + targetedInt + " " + targetedString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }
            if (targetedChopInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (targetedChopInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + color + sign + targetedChopInt + " " + targetedChopString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }
            if (stunningCrushInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (stunningCrushInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + color + sign + stunningCrushInt + " " + stunningCrushString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }
            if (targetedJointInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (targetedJointInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + color + sign + targetedJointInt + " " + targetedJointString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }
            if (hardenedInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (hardenedInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + hardenedInt + " " + hardenedString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }
            if (shieldInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (shieldInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + shieldInt + " " + shieldString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }
            if (coverInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (coverInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + coverInt + " " + coverString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }

            if (apInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (apInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + apInt + " " + apString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }

            if (expInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (expInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + expInt + " " + expString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }

            if (nonlethalInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (nonlethalInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + nonlethalInt + " " + nonlethalString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }

            if (burstInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (burstInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + burstInt + " " + burstString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }

            if (stunningWithoutCrushingInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (stunningWithoutCrushingInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + stunningWithoutCrushingInt + " " + stunningWithoutCrushingString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }

            if (stunningWithSharpInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (stunningWithSharpInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + stunningWithSharpInt + " " + stunningWithSharpString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }

            if (brokenWeaponInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (brokenWeaponInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + brokenWeaponInt + " " + brokenWeaponString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);

            }

            if (chargingInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (chargingInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + chargingInt + " " + chargingString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }

            if (capturingInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (capturingInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + capturingInt + " " + capturingString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }

            if (stunningCrushPlusInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (stunningCrushPlusInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + stunningCrushPlusInt + " " + stunningCrushPlusString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }

            if (electricityInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (electricityInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + electricityInt + " " + electricityString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }

            if (falloffInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (falloffInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + falloffInt + " " + falloffString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }

            if (resInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (resInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + resInt + " " + resString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }

            if (weaponTypeResInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (weaponTypeResInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + weaponTypeResInt + " " + weaponTypeResString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }

            if (backstabInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (backstabInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + backstabInt + " " + backstabString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }

            if (stunningHeadInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (stunningHeadInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + stunningHeadInt  + " " + stunningHeadString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }

            if (magicShieldInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (magicShieldInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + magicShieldInt  + " " + magicShieldString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }

            if (constitutionInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (constitutionInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + constitutionInt  + " " + constitutionString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }

            if (fullcoverInt != -666) {
                String sign = "+";
                TextFormatting color = TextFormatting.DARK_RED;
                if (fullcoverInt <= 0) {
                    sign = "";
                    color = TextFormatting.BLUE;
                }
                TextComponentString tcs = new TextComponentString("\n" + sign + fullcoverInt  + " " + fullcoverString);
                tcs.getStyle().setColor(color);
                hover.appendSibling(tcs);
            }

            component.setHoverText(hover);
            message.addComponent(component);

            String plus = "";
            if (priority.equals(AttackType.PHYSIC)) {
                if (weaponInt >= 0) plus = "+";
                MessageComponent weapon = new MessageComponent(plus + weaponInt, ChatColor.DARK_RED);
                weapon.setHoverText(hover);
                message.addComponent(weapon);
            }

            String defense = String.valueOf(defenseInt);
            if (psychDamageInt != 0 || !psychDamageString.equals("психический урон")) {
                MessageComponent psd = new MessageComponent((psychDamageInt >= 0 ? "+" : "") + psychDamageInt, ChatColor.PURPLE);
                psd.setHoverText(hover);
                message.addComponent(psd);
            }

            if (!priority.equals(AttackType.PHYSIC)) {
                if (weaponInt != 0 || !weaponString.equals("физический урон")) {
                    if (weaponInt >= 0) plus = "+";
                    MessageComponent weapon = new MessageComponent(plus + weaponInt, ChatColor.DARK_RED);
                    weapon.setHoverText(hover);
                    message.addComponent(weapon);
                }
            }

            if (priority.equals(AttackType.PHYSIC)) {
                if (defenseInt == 0) defense = "-" + defense;
                MessageComponent def = new MessageComponent(defense, ChatColor.GMCHAT);
                def.setHoverText(hover);
                message.addComponent(def);
            }

            if (psychDefenseInt != 0 || !priority.equals(AttackType.PHYSIC) || !psychDefenseString.equals("психическая броня")) {
                MessageComponent psd2 = new MessageComponent((psychDefenseInt == 0 ? "-" : (psychDefenseInt > 0 ? "+" : "")) + psychDefenseInt, ChatColor.DARK_PURPLE);
                psd2.setHoverText(hover);
                message.addComponent(psd2);
            }

            if (!priority.equals(AttackType.PHYSIC)) {
                if (defenseInt != 0 || !defenseString.equals("физическая броня")) {
                    if (defenseInt == 0) defense = "-" + defense;
                    else if (defenseInt > 0) defense = "+" + defense;
                    MessageComponent def = new MessageComponent(defense, ChatColor.GMCHAT);
                    def.setHoverText(hover);
                    message.addComponent(def);
                }
            }

            int other = resultDamage - diffInt - weaponInt - defenseInt - psychDamageInt - psychDefenseInt;
            String otherStr = String.valueOf(other);
            if (other >= 0) otherStr = "+" + otherStr;
            if (other != 0) {
                MessageComponent oth = new MessageComponent(otherStr, ChatColor.PURPLE);
                oth.setHoverText(hover);
                message.addComponent(oth);
            }
            MessageComponent res = new MessageComponent("=" + resultDamage, ChatColor.RED);
            hover.appendSibling(new TextComponentString("\n" + TextFormatting.RED + "=" + resultDamage  + " " +  "урон"));
            res.setHoverText(hover);
            message.addComponent(res);

            this.damageMessage = message;

        }

    }


    public static int calcDifferense(FudgeDice attack, FudgeDice defense, boolean ranged) {
        if (defense == null) return calcDifferenseForNothing(attack);
        System.out.println("attack result " + attack.getResult() + ", defense result " + defense.getResult());
        if (defense.getResult() < -1 && !ranged) {
            return attack.getResult() + 1; // минимальный порог в ближнем бою — ужасно
        } else {
            return attack.getResult() - defense.getResult();
        }

    }

    public static int calcDifferense(int attack, int defense, boolean ranged) {
        //if (defense == null) return calcDifferenseForNothing(attack);
        System.out.println("attack result " + attack + ", defense result " + defense);
        if (defense < -1 && !ranged) {
            return attack + 1; // минимальный порог в ближнем бою — ужасно
        } else {
            return attack - defense;
        }

    }


    public static int calcDamage(int diff, int weaponMod, int defenseMod, int traitMod,
                                 int targetedMod, int additionalTargetedDamage, int additionalArmorDamage
    ) {
        int result;
        result = diff + weaponMod - defenseMod - traitMod + targetedMod + additionalTargetedDamage + additionalArmorDamage;
        return result;
    }

    public static int calcDifferenseForNothing(FudgeDice dice) {
        System.out.println("Got dice with result " + dice.getResultAsString());
        return dice.getResult() + 1;
    }
}
