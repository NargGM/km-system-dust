package ru.konungstvo.combat.duel;

import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.exceptions.DataException;
import ru.konungstvo.player.Player;

public class DuelMaster {

    public static Duel createDuel(Player attacker) throws DataException {
        if (DataHolder.inst().getDuelForAttacker(attacker.getName()) != null) {
            System.out.println("!!!: duel for " +attacker.getName() + " ended automatically");
            DuelMaster.end(DataHolder.inst().getDuelForAttacker(attacker.getName()));
//            Message message = new Message("Игрок уже находится в дуэли!", ChatColor.RED);
//            MessageComponent remove = new MessageComponent(" [Удалить дуэль]");
//            remove.setClickCommand("/dueldebug remove " + attacker.getName());
//            attacker.sendMessage(message);
//
//            throw new DataException("1", "Игрок уже находится в дуэли!");
        }

        Duel duel = new Duel(attacker);
        DataHolder.inst().registerDuel(duel);

        return duel;
    }

    public static void end(Duel duel) {
        duel.setEnded(true);
        DataHolder.inst().removeDuel(duel);
    }
}
