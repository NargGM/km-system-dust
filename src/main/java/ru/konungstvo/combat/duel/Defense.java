package ru.konungstvo.combat.duel;

import ru.konungstvo.chat.message.FudgeDiceMessage;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.combat.equipment.Equipment;

public class Defense {
    private FudgeDiceMessage diceMessage;
    //    private ArmorHandler armor;
    private Equipment defenseItem;
    private DefenseType defenseType;
    private int diceModifier;
    private int defenseModifier;
    private int activeHand;

    public Defense() {};

    @Deprecated
    public void addDiceMessage(FudgeDiceMessage defenseDiceMessage) {
        this.diceMessage = defenseDiceMessage;
        String skillName = defenseDiceMessage.getDice().getSkillName().toLowerCase();
        if (skillName.contains("уклонение")) {
            this.defenseType = DefenseType.EVADE;
        } else if (skillName.contains("парирование")) {
            this.defenseType = DefenseType.PARRY;
        } else if (skillName.contains("блокирование")) {
            this.defenseType = DefenseType.BLOCKING;
        } else {
            this.defenseType = DefenseType.NOTHING;
        }
    }

    public FudgeDiceMessage getDiceMessage() {
        return diceMessage;
    }

    public void setDiceMessage(FudgeDiceMessage diceMessage) {
        this.diceMessage = diceMessage;
    }

    public int getDiceModifier() {
        return diceModifier;
    }

    public void setDiceModifier(int diceModifier) {
        this.diceModifier = diceModifier;
    }

    public int getDefenseModifier() {
        return defenseModifier;
    }

    public void setDefenseModifier(int defenseModifier) {
        this.defenseModifier = defenseModifier;
    }


    public DefenseType getDefenseType() {
        return defenseType;
    }

    public void setDefenseType(DefenseType defenseType) {
        System.out.println("DEFENSE SET " + defenseType);
        this.defenseType = defenseType;
    }

    public Equipment getDefenseItem() {
        return defenseItem;
    }

    public void setDefenseItem(Equipment defenseItem) {
        this.defenseItem = defenseItem;
    }

    public int getActiveHand() {
        return activeHand;
    }

    public void setActiveHand(int activeHand) {
        this.activeHand = activeHand;
    }
}
