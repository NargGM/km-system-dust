package ru.konungstvo.combat.reactions;

import de.cas_ual_ty.gci.GunCus;
import de.cas_ual_ty.gci.network.MessageSoundReg;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import noppes.npcs.api.constants.AnimationType;
import noppes.npcs.entity.EntityNPCInterface;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.bridge.discord.DiscordBridge;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.*;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.Combat;
import ru.konungstvo.combat.CombatState;
import ru.konungstvo.combat.StatusEnd;
import ru.konungstvo.combat.StatusType;
import ru.konungstvo.combat.equipment.Weapon;
import ru.konungstvo.combat.movement.MovementHistory;
import ru.konungstvo.commands.helpercommands.player.turn.ToolPanel;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Logger;
import ru.konungstvo.exceptions.RuleException;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.Player;
import ru.konungstvo.player.wounds.WoundType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Stores and handles queue of players. Contains list of fully built SkillDiceMessages.
 */
public class Queue implements Serializable, Cloneable {
    private List<SkillDiceMessage> results;
    private boolean autoRenewing;
    private boolean sorted;
    private boolean freezed;
    private Logger logger = new Logger(this.getClass().getSimpleName());

    private int roundNumber = 0;

    public Queue() {
        this.results = new ArrayList<>();
        this.autoRenewing = true;
        this.sorted = false;
        this.freezed = false;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public Integer getResultFor(String name) {
        for (SkillDiceMessage sdm : results) {
            if (sdm.getPlayerName().equals(name)) return sdm.getDice().getFirstResult();
        }
        return null;
    }

    public List<SkillDiceMessage> getResults() {
        return results;
    }

    public void removePlayer(String playerName) {
        if (getNext() == null) {
            this.proceed();
        }
        for (SkillDiceMessage s : results) {
            if (s.getPlayerName().equals(playerName)) {
                removeResult(s);
                return;
            }
        }
    }

    public void removePlayerBc(String playerName, String reason) {
        try {
            if (DataHolder.inst().isNpc(playerName)) {
                EntityNPCInterface npc = (EntityNPCInterface) DataHolder.inst().getNpcEntity(playerName);
                npc.wrappedNPC.getAi().setAnimation(AnimationType.SLEEP);
                String desc = npc.wrappedNPC.getDisplay().getTitle();
                npc.wrappedNPC.getDisplay().setTitle((desc + " [" + reason + "]").trim());
                npc.wrappedNPC.getDisplay().setHasLivingAnimation(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //actualDefender.makeFall();
        try {
            if (DataHolder.inst().getCombatForPlayer(playerName) != null) {
                Player gm = DataHolder.inst().getMasterForPlayerInCombat(DataHolder.inst().getPlayer(playerName));
                if (gm != null) {
                    gm.performCommand("/queue remove " + playerName);
                    gm.performCommand("/combat remove " + playerName);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void removeResult(SkillDiceMessage skillDiceMessage) {
        SkillDiceMessage toRemove = null;
        for (SkillDiceMessage result : results) {
            if (skillDiceMessage.getPlayerName().equals(result.getPlayerName())) {
                toRemove = result;
            }
        }
        this.results.remove(toRemove);
    }

    public void addResult(SkillDiceMessage skillDiceMessage) {
        removeResult(skillDiceMessage);
        this.results.add(skillDiceMessage);
    }

    public boolean contains(String playerName) {
        for (SkillDiceMessage s : results) {
            System.out.println("1: " + s.getPlayerName() + " 2: " + playerName + " " + s.getPlayerName().equals(playerName));
            if (s.getPlayerName().equals(playerName)) {
                return true;
            }
        }
        return false;
    }

    public Message toMessage() {
        if (results.isEmpty()) {
            return new Message("Очередь ещё не создана. Используйте \"/q go\".", ChatColor.RED);
        }
        DataHolder dh = DataHolder.inst();
        sort();

        Message answer = new Message("[" + roundNumber + "] ", ChatColor.GMCHAT);
        MessageComponent answer1 = new MessageComponent("Очередь: ", ChatColor.DICE);
        answer.addComponent(answer1);
        MessageComponent playerNick = null;
        MessageComponent diceResult = null;

//        StringBuilder str = new StringBuilder();
//        str.append(ChatColor.DICE).append("Очередь: ");
        for (SkillDiceMessage result : results) {

//            str.append(ChatColor.NICK);
            CombatState combatState = dh.getPlayer(result.getPlayerName()).getCombatState();

            switch (combatState) {
                case ACTED:
//                    playerNick = new MessageComponent(result.getPlayerName(), ChatColor.NICK);
//                    playerNick.setStrikethrough(true);
                    diceResult = new MessageComponent(result.getPlayerName() + " " + TextFormatting.YELLOW  +  TextFormatting.STRIKETHROUGH + result.getDice().getFirstResultAsString(), ChatColor.NICK);
                    diceResult.setStrikethrough(true);
                    if(results.size() < 20) {
                        diceResult.setHoverText(new TextComponentString(getResultAsMessage(result.getPlayerName())));
                    }
//                    str.append("§8");
//                    str.append("§m");
                    break;
                case SHOULD_ACT:
//                    playerNick = new MessageComponent(result.getPlayerName(), ChatColor.NICK);
//                    playerNick.setBold(true);
                    diceResult = new MessageComponent(result.getPlayerName() + " " + TextFormatting.YELLOW  +  TextFormatting.BOLD +  result.getDice().getFirstResultAsString(), ChatColor.NICK);
                    diceResult.setBold(true);
                    if(results.size() < 20) {
                        diceResult.setHoverText(new TextComponentString(getResultAsMessage(result.getPlayerName())));
                    }
//                    str.append("§n");
//                    str.append("§l");
                    break;
                case SHOULD_WAIT:
                    diceResult = new MessageComponent(result.getPlayerName() + " " + TextFormatting.YELLOW  + result.getDice().getFirstResultAsString(), ChatColor.NICK);
                    if (dh.getPlayer(result.getPlayerName()).hasShifted()) {
                        diceResult = new MessageComponent(result.getPlayerName() + " " + TextFormatting.YELLOW  + result.getDice().getFirstResultAsString(), ChatColor.DARK_GREEN);
//                        str.append("§2");
                    }
                    if(results.size() < 20) {
                        diceResult.setHoverText(new TextComponentString(getResultAsMessage(result.getPlayerName())));
                    }
                    break;
                default:
                    diceResult = new MessageComponent(result.getPlayerName() + " " + result.getDice().getFirstResultAsString(), ChatColor.DICE);
            }
//            str.append(result.getPlayerName());

//            answer.addComponent(playerNick);
//            answer.addComponent(new MessageComponent(" "));
            answer.addComponent(diceResult);
            boolean needSpace = false;
            for (int i = 0; i < dh.getPlayer(result.getPlayerName()).getPreviousDefenses(); i++) {
                answer.addComponent(new MessageComponent("'", ChatColor.NICK));
                needSpace = true;
//                str.append("'");
            }
            for (int i = 0; i > dh.getPlayer(result.getPlayerName()).getConfused(); i--) {
                answer.addComponent(new MessageComponent("'", ChatColor.DARK_RED));
                needSpace = true;
//                str.append("'");
            }
//
            if (results.indexOf(result) != (results.size() - 1)) answer.addComponent(new MessageComponent(" | ", ChatColor.NICK));

        }
//        return str.toString().substring(0, str.toString().length() - 3);
        return answer;
    }

    public void sort() {
        if (this.sorted) return;
        results = QueueSorter.sortResults(results);
        Collections.reverse(results);
        this.sorted = true;
    }

    public boolean isSorted() {
        return sorted;
    }

    public void setSorted(boolean sorted) {
        this.sorted = sorted;
    }

    public void reset() {
        this.sorted = false;
        for (SkillDiceMessage s : results) {
            Player p = DataHolder.inst().getPlayer(s.getPlayerName());
            p.setCombatState(CombatState.SHOULD_WAIT);
            p.setPreviousDefenses(0);
            p.getPercentDice().setPreviousDefenses(0);
            p.setShifted(false);
        }
        this.results = new ArrayList<>();
    }

    public Message getResultsAsMessage() {
        sort();
        Message result = new Message();
//        StringBuilder result = new StringBuilder();
        for (SkillDiceMessage message : results) {
            message.build();
            result.addComponentLn(message);
//            result.append(message.build().replace("4dF", ChatColor.COMBAT + "4dF" + ChatColor.DICE)).append("\n");
            // result.append(message.build().replace("4dF", ChatColor.COMBAT + "4dF" + ChatColor.DICE));
        }
        return result;
    }

    public String getResultAsMessage(String name) {
        String result = "";
        for (SkillDiceMessage sdm : results) {
            if (sdm.getPlayerName().equals(name)) {
                String mods = sdm.getDice().getModAsString();
                if (sdm.getDice().getPersentResult() != 0) return ("§e" + sdm.toString() + "\n" + (sdm.getDice().getModAsString() + "\n" + sdm.getPercentInfo().trim() + "\n" + sdm.getDice().getAdvDisadvString()).trim()).trim();
                if (!(sdm.getDice().getModAsString().isEmpty())) return ("§e" + sdm.toString() + "\n" + sdm.getDice().getModAsString()  + "\n" + sdm.getDice().getAdvDisadvString()).trim();
                return "§e" + sdm.toString();
            }
        }
        return result;
    }

    public void proceed() {
        boolean allowToAct = false;

        for (SkillDiceMessage message : results) {
            Player player = DataHolder.inst().getPlayer(message.getPlayerName());
            if (allowToAct && player.getCombatState() == CombatState.SHOULD_WAIT) {
                player.setCombatState(CombatState.SHOULD_ACT);
                return;
            }
            if (player.getCombatState() == CombatState.SHOULD_ACT) {
                player.setCombatState(CombatState.ACTED);
                allowToAct = true;
            }
        }
    }

    public Player getNext() {
        for (SkillDiceMessage s : results) {
            Player p = DataHolder.inst().getPlayer(s.getPlayerName());
            if (p == null) return null;
            if (p.getCombatState() == CombatState.SHOULD_ACT) {
                return p;
            }
        }
        return null;
    }

    public List<String> getListForShift(String playerName) {
        List<String> result = new ArrayList<>();
        boolean found = false;
        for (SkillDiceMessage sdm : results) {
            if (found) {
                result.add(sdm.getPlayerName());
            }

            if (!found && sdm.getPlayerName().equals(playerName)) found = true;
        }
        return result;
    }

    public void shift(Player whoToShift, String whereToShift) {
        shift(whoToShift, whereToShift, false, false);
    }

    public void shift(Player whoToShift, String whereToShift, boolean forced) {
        shift(whoToShift, whereToShift, forced, false);
    }

    public void shift(Player whoToShift, String whereToShift, boolean forced, boolean up) {
        if (whoToShift.getName().toLowerCase().equals(whereToShift.toLowerCase())) {
            if (forced) return;
            throw new RuleException("Вы пытаетесь встать в очереди после себя!");
        }
        if (whoToShift.getCombatState() == CombatState.ACTED) {
            throw new RuleException("Ты уже ходил!");
        }

        // shiftingIndex is index of player who wants to shift
        int shiftingIndex = -1;
        // index is index of player who will
        int index = -1;
        for (SkillDiceMessage s : results) {
            if (s.getPlayerName().toLowerCase().equals(whoToShift.getName().toLowerCase())) {
                shiftingIndex = results.indexOf(s);
                System.out.println("Found index of " + s.getPlayerName() + ": " + shiftingIndex);
            }
            if (s.getPlayerName().toLowerCase().equals(whereToShift.toLowerCase())) {
                index = results.indexOf(s);
                System.out.println("Found index of " + s.getPlayerName() + ": " + index);

            }
        }

        if (index == -1) {
            throw new RuleException("Ошибка 401: игрок " + whereToShift + " не найден в очереди.");
        }
        if (shiftingIndex == -1) {
            throw new RuleException("Ошибка 402: игрок " + whoToShift.getName() + " не найден в очереди.");
        }

        // check rules
        if (index < shiftingIndex && !forced) {
            throw new RuleException("Вы не можете передвигаться по очереди вверх!");
        }
        if (!up && shiftingIndex - index == 1) {
            throw new RuleException("Вы пытаетесь встать на своё же место в очереди!");
        }

        // if player who wants to shift is supposed to act, change his combat state
        if (!up && whoToShift.getCombatState() == CombatState.SHOULD_ACT) {
            Player nextInQueue;
            try {
                nextInQueue = DataHolder.inst().getPlayer(results.get(shiftingIndex + 1).getPlayerName());
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
                throw new RuleException("Вы последний в очереди, смещаться некуда!");
            }

            nextInQueue.setCombatState(CombatState.SHOULD_ACT);
            whoToShift.setCombatState(CombatState.SHOULD_WAIT);

            nextInQueue.processMovement();
            nextInQueue.clearOpportunities25();
            nextInQueue.clearDefends();
            nextInQueue.processStatusEffects(StatusEnd.TURN_START);

            DataHolder dh = DataHolder.inst();
            Combat combat = dh.getCombat(nextInQueue.getAttachedCombatID());
            if (combat == null) combat = dh.getCombatForPlayer(nextInQueue.getName());
            Queue queue = this;
            Message info = new Message();
            if (!dh.isNpc(nextInQueue.getName())) {
                //TODO перенести эту кашу в отдельную функцию

                try {
                    EntityLivingBase rider = ServerProxy.getForgePlayer(nextInQueue.getName());
                    if (rider != null && rider.getRidingEntity() != null) {
                        Entity mount = rider.getLowestRidingEntity();
                        Player mountPlayer = null;
                        if(mount instanceof EntityNPCInterface) {
                            EntityNPCInterface npc = (EntityNPCInterface) mount;
                            mountPlayer = DataHolder.inst().getPlayer(npc.wrappedNPC.getName());
                            if (rider.getRidingEntity() == mount) {
                                if(!queue.contains(mountPlayer.getName())) mountPlayer.processMovement();
                                nextInQueue.setMovementHistory(mountPlayer.getMovementHistory());
                            } else if (mountPlayer.getMovementHistory() == MovementHistory.NOW) {
                                nextInQueue.setMovementHistory(mountPlayer.getMovementHistory());
                            }
                        } else if (mount instanceof EntityPlayerMP) {
                            EntityPlayerMP playerMP = (EntityPlayerMP) mount;
                            Player player = DataHolder.inst().getPlayer(playerMP.getName());
                            if (player.getMovementHistory() == MovementHistory.NOW) {
                                nextInQueue.setMovementHistory(player.getMovementHistory());
                            }
                        }
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }

                if (nextInQueue.hasStatusEffect(StatusType.STUNNED) && !nextInQueue.cantBeStunned()) {
                    info.purge();
                    info.addComponent(new MessageComponent(queue.getNext().getName() + " пропускает ход из-за " + (nextInQueue.hasStatusEffect("подавление") ? "подавления" : "оглушения") + ".", ChatColor.RED));
                    for (Player p : combat.getFighters()) {
                        p.sendMessage(info);
                    }
                    ServerProxy.informMasters(info, nextInQueue);
                    /*DiscordBridge.sendMessage(info.toString());*/
                    logger.info(info.toString());

                    nextInQueue.performCommand("/endturn");

                } else {
                    nextInQueue.sendMessage(new Message("Твоя очередь, ходи!", ChatColor.BLUE));
                    nextInQueue.performCommand("/toolpanel");
                    GunCus.channel.sendTo(new MessageSoundReg(GunCus.SOUND_RING, 1F, 1F), ServerProxy.getForgePlayer(nextInQueue.getName()));
                }
            } else {

                try {
                    EntityLivingBase rider = null;
                    rider = (EntityLivingBase) DataHolder.inst().getNpcEntity(nextInQueue.getName());
                    if (rider != null && rider.getRidingEntity() != null) {
                        Entity mount = rider.getLowestRidingEntity();
                        Player mountPlayer = null;
                        if(mount instanceof EntityNPCInterface) {
                            EntityNPCInterface npc = (EntityNPCInterface) mount;
                            mountPlayer = DataHolder.inst().getPlayer(npc.wrappedNPC.getName());
                            if (rider.getRidingEntity() == mount) {
                                if(!queue.contains(mountPlayer.getName())) mountPlayer.processMovement();
                                nextInQueue.setMovementHistory(mountPlayer.getMovementHistory());
                            } else if (mountPlayer.getMovementHistory() == MovementHistory.NOW) {
                                nextInQueue.setMovementHistory(mountPlayer.getMovementHistory());
                            }
                        } else if (mount instanceof EntityPlayerMP) {
                            EntityPlayerMP playerMP = (EntityPlayerMP) mount;
                            Player player = DataHolder.inst().getPlayer(playerMP.getName());
                            if (player.getMovementHistory() == MovementHistory.NOW) {
                                nextInQueue.setMovementHistory(player.getMovementHistory());
                            }
                        }
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }

                Player gm = dh.getMasterForNpcInCombat(nextInQueue);
                if (gm == null) {
                    System.out.println("gm null wtf");
                }
                if (nextInQueue.hasStatusEffect(StatusType.STUNNED) && !nextInQueue.cantBeStunned()) {
                    info.purge();
                    info.addComponent(new MessageComponent(queue.getNext().getName() + " пропускает ход из-за " + (nextInQueue.hasStatusEffect("подавление") ? "подавления" : "оглушения") + ".", ChatColor.RED));
                    for (Player p : combat.getFighters()) {
                        p.sendMessage(info);
                    }
                    ServerProxy.informMasters(info, nextInQueue);
//                    DiscordBridge.sendMessage(info.toString());
                    logger.info(info.toString());
                    dh.getMasterForNpcInCombat(nextInQueue).performCommand("/endturn");

                } else {
                    gm.setSubordinate(nextInQueue);
                    gm.sendMessage(new Message(String.format("Ходит %s!", nextInQueue.getName()), ChatColor.BLUE));
                    if (gm.getSubordinate() != null) {
                        EntityLivingBase forgePlayer = gm.getSubordinate().getEntity();
                        WeaponTagsHandler weaponTagsHandler = null;
                        Weapon weapon = null;
                        ItemStack activeItem = null;
                        activeItem = forgePlayer.getHeldItemMainhand();
                        if (activeItem != null && !activeItem.isEmpty()) {
                            weaponTagsHandler = new WeaponTagsHandler(activeItem);
                            System.out.println("test2");
                            if (weaponTagsHandler.isWeapon() && weaponTagsHandler.isFirearm()) {
                                weapon = new Weapon(weaponTagsHandler.getDefaultWeaponName(), weaponTagsHandler.getDefaultWeapon(), weaponTagsHandler);
                                if (!weapon.isLoaded()) DataHolder.inst().getMasterForNpcInCombat(nextInQueue).sendMessage(new Message("§4§lВНИМАНИЕ! ОРУЖИЕ " + DataHolder.inst().getMasterForNpcInCombat(nextInQueue).getSubordinate().getName() + " РАЗРЯЖЕНО!"));
                            }
                        }
                    }
                    gm.performCommand("/" + ToolPanel.NAME);
                    GunCus.channel.sendTo(new MessageSoundReg(GunCus.SOUND_RING, 0.5F, 1F), ServerProxy.getForgePlayer(gm.getName()));
                }
            }
//            if (!DataHolder.inst().isNpc(nextInQueue.getName())) {
//                nextInQueue.sendMessage(new Message("Твоя очередь, ходи!", ChatColor.BLUE));
//                nextInQueue.performCommand("/" + ToolPanel.NAME);
//            } else {
//                Player gm = DataHolder.inst().getMasterForNpcInCombat(nextInQueue);
//                gm.setSubordinate(nextInQueue);
//                gm.sendMessage(new Message(String.format("Ходит %s!", nextInQueue.getName()), ChatColor.BLUE));
//                gm.performCommand("/" + ToolPanel.NAME);
//            }

            //nextInQueue.sendMessage(new Message("Твоя очередь, ходи!", ChatColor.COMBAT));
        }

        // actually make a shift in Queue
        SkillDiceMessage toShift = null;
        for (SkillDiceMessage s : results) {
            if (s.getPlayerName().equals(whoToShift.getName())) {
                toShift = s;
            }
        }



        results.remove(toShift);
        if (index < shiftingIndex && forced) {
            results.add(index + 1, toShift);
        } else {
            results.add(index, toShift);
        }
        
        if (!up) whoToShift.setShifted(true);

        whoToShift.riposted = null;
    }

    public boolean isAutoRenewing() {
        return autoRenewing;
    }

    public void setAutoRenewing(boolean autoRenewing) {
        this.autoRenewing = autoRenewing;
    }

    public boolean isFreezed() {
        return freezed;
    }

    public void setFreezed(boolean freezed) {
        this.freezed = freezed;
    }

    public int getRoundNumber() {
        return roundNumber;
    }

    public void setRoundNumber(int roundNumber) {
        this.roundNumber = roundNumber;
    }
}
