package ru.konungstvo.combat.reactions;

import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Logger;
import ru.konungstvo.player.Player;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * Contains list of reactions, queue and tools for handling them.
 */
public class ReactionList extends Observable implements Serializable, Cloneable {
    private List<Reaction> reactions;
    private Queue queue;
    private ViewType viewType;
    private int roundNumber = 0;
    private Logger logger;

    public ReactionList() {
        this.reactions = new ArrayList<>();
        this.queue = new Queue();
        this.viewType = ViewType.AUTO;
        this.logger = new Logger("ReactionList");
    }

    public static ReactionList getCopy(ReactionList other) {
        ReactionList result = new ReactionList();
        result.reactions = other.getReactions();
        result.queue = other.queue;
        return result;
    }

    public Object clone() throws CloneNotSupportedException{
        return super.clone();
    }

    public List<Reaction> getReactions() {
        return reactions;
    }

    public void setReactions(List<Reaction> reactions) {
        this.reactions = reactions;
    }

    public Player addReaction(Reaction reaction) {
        reactions.remove(reaction);
        reactions.add(reaction);
        DataHolder dh = DataHolder.inst();
        Player player = dh.getPlayer(reaction.getPlayerName());
        if (player == null) {
            player = dh.createNPC(reaction.getPlayerName());

        }
        logger.debug("Reaction " + reaction + " was added!");
        return player;
    }

    public void removeReaction(String reactionName) {
        removeReaction(new Reaction(reactionName, 0, ""));
    }

    public void removeReaction(Reaction reaction) {
        reactions.remove(reaction);
    }

    public Message toMessage() {
//        StringBuilder result = new StringBuilder(ChatColor.COMBAT + "Список реакций: \n");
        Message result = new Message();
        result.addComponent(new MessageComponent("Список реакций: \n", ChatColor.COMBAT));

        for (Reaction reaction : reactions) {
            result.addComponent(reaction.toMessage());
            result.addComponent(new MessageComponent("\n"));
        }
        return result;
    }

    public Queue getQueue() {
        return queue;
    }

    public void setQueue(Queue queue) {
        this.queue = queue;
    }

    public void setViewType(ViewType viewType) {
        this.viewType = viewType;
    }

    public ViewType getViewType() {
        return viewType;
    }

    public int getRoundNumber() {
        return roundNumber;
    }

    public void setRoundNumber(int roundNumber) {
        this.roundNumber = roundNumber;
    }
}
