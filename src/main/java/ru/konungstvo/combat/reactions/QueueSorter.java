package ru.konungstvo.combat.reactions;

import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.control.Logger;

import java.util.*;

/**
 * Sorts reactions, handles rerolls.
 */
public class QueueSorter {
    private static Logger logger = new Logger("QueueSorter");

    public static List<SkillDiceMessage> sortResults(List<SkillDiceMessage> results) {
        //results.sort(SkillDiceMessage::compareToButBetter); //EXPERIMENT java.lang.IllegalArgumentException: Comparison method violates its general contract!
        results.sort(SkillDiceMessage::compareTo);
        results = smartReroll(results);
        return results;
    }

    private static List<SkillDiceMessage> smartReroll(List<SkillDiceMessage> results) {
        List<SkillDiceMessage> list = new ArrayList<>(results);

        System.out.println("INITIAL: ");
        for (SkillDiceMessage m : list) {
            System.out.println(m + " " + m.getDice().getResultAsString());
        }

        // Find groups of dice that we need to reroll
        int similars[] = QueueSorter.findSimilars(list);
        //System.out.println("Similars: " + Arrays.toString(similars));

        // Put all found groups into a list
        List<List<SkillDiceMessage>> listOfLists = QueueSorter.getListOfListsForSimilars(list, similars);

        int num = 1;
        List<List<SkillDiceMessage>> listOfSortedLists = new ArrayList<>();

        // Reroll all the groups
        for (List<SkillDiceMessage> l : listOfLists) {
            //System.out.println("Rerolling list num " + num++);
            List<SkillDiceMessage> rerolledList = new ArrayList<>();
            for (SkillDiceMessage s : l) {
                s.getDice().reroll();
                rerolledList.add(s);
            }
            rerolledList.sort(SkillDiceMessage::compareTo);
            listOfSortedLists.add(rerolledList);
        }


        // Replace old dice with new, rerolled ones
        for (int i = 0; i < results.size(); i++) {
            for (SkillDiceMessage rerolledMessage : list) {
                if (results.get(i).getPlayerName().equals(rerolledMessage.getPlayerName())) {
                    results.set(i, rerolledMessage);
                }
            }
        }


        // Find indexes of rerolled dice, using similars[]
        Map<Integer, AbstractMap.SimpleEntry<Integer, Integer>> bordersToSubSort = new HashMap<>();
        int currentI = 1;
        //System.out.println(Arrays.toString(similars));
        for (int i = 0; i < similars.length; i++) {
            if (similars[i] == 0) continue;
            if (currentI != similars[i]) {
                currentI++;
            }
            if (!bordersToSubSort.containsKey(currentI)) {
                bordersToSubSort.put(currentI, new AbstractMap.SimpleEntry<Integer, Integer>(i, i));
            } else {
                bordersToSubSort.put(currentI, new AbstractMap.SimpleEntry<Integer, Integer>(bordersToSubSort.get(currentI).getKey(), i));
            }
        }
        //System.out.println("BORDERS: " + bordersToSubSort.toString());

        // Now that we know where in original list rerolled groups stay, subsort them
        for (Map.Entry<Integer, AbstractMap.SimpleEntry<Integer, Integer>> entry : bordersToSubSort.entrySet()) {
            int start = entry.getValue().getKey();
            int end = entry.getValue().getValue();
            Collections.sort(results.subList(start, end+1));
            smartReroll(results.subList(start, end+1)); // ACHTUNG! Very important part with recursion. If some of rerolled groups still contain "similars", use smartReroll() arlgorithm on them.
        }

        return results;
    }

    /**
     * @param list of SkillDiceMessages
     * @return array to show similar dice, in form of [0, 1, 1, 2, 2, 0, 3, 3, 3, 0]. Each group of "similars" has its own number.
     */
    public static int[] findSimilars(List<SkillDiceMessage> list) {
        int similars[] = new int[list.size()];
        int numberOfSim = 1;
        for (int i = 1; i < list.size(); i++) {
            if (list.get(i - 1).getDice().equals(list.get(i).getDice())) {
                similars[i] = numberOfSim;
                similars[i - 1] = numberOfSim;
            } else {
                similars[i] = 0;
                if (similars[i - 1] != 0) {
                    numberOfSim++;
                }
            }

        }
        return similars;
    }

    public static List<List<SkillDiceMessage>> getListOfListsForSimilars(List<SkillDiceMessage> list, int[] similars) {
        int currentNumber = 1;
        List<List<SkillDiceMessage>> listOfLists = new ArrayList<>();
        List<SkillDiceMessage> listOfDice = new ArrayList<>();
        for (int i = 0; i < similars.length; i++) {
//            System.out.println("i = " + i + ", currentNumber = " + currentNumber);
            if (similars[i] == 0) continue;
            if (similars[i] == currentNumber) {
//                System.out.println("Adding " + list.get(i) + " to listOfDice num " + currentNumber);
                listOfDice.add(list.get(i));
            } else if (similars[i] == currentNumber + 1 || i == similars.length - 1) {
//                System.out.println("Adding listOfDice num " + currentNumber + " to listOfLists");
                listOfLists.add(listOfDice);

                currentNumber++;
                listOfDice = new ArrayList<>();
//                System.out.println("Adding " + list.get(i) + " to listOfDice num " + currentNumber);
                listOfDice.add(list.get(i));
            }
        }
        listOfLists.add(listOfDice);
        return listOfLists;
    }

    public static List<SkillDiceMessage> rerollList(List<SkillDiceMessage> list) {
        for (SkillDiceMessage m : list) {
            //m.reroll();
        }
        return list;
    }


}
