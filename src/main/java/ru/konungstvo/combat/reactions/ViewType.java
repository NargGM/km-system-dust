package ru.konungstvo.combat.reactions;

public enum ViewType {
    AUTO,
    SIMPLE,
    SILENT
}
