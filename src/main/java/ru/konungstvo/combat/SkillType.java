package ru.konungstvo.combat;

public enum SkillType {

    ATTRIBUTE("атрибут"),
    SIMPLE("обычный"),
    MASTERING("владение"),
    PARRY("парирование"),
    BLOCK("блокирование"),
    EVADE("уклонение"),
    CONSTITUTION("группировка"),
    MISSILE("дальнобой"),
    MAGIC("магия"),
    PHYSICAL("физическая защита"),
    MENTAL("психическая защита");

    private String desc;
    SkillType(String desc) {
        this.desc = desc;
    }

    public static SkillType getType(String skill) {
        for (SkillType type : SkillType.values()) {
            if (type.toString().contains(skill))
                return type;
        }
        return SIMPLE;
    }

    @Override
    public String toString() {
        return desc;
    }
}
