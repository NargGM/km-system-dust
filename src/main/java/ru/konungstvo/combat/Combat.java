package ru.konungstvo.combat;

import net.minecraft.server.MinecraftServer;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.SkillDiceMessage;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.combat.moves.DeprecatedAttack;
import ru.konungstvo.combat.moves.Opportunity;
import ru.konungstvo.combat.reactions.ReactionList;
import ru.konungstvo.commands.executor.QueueExecutor;
import ru.konungstvo.control.CombatMemento;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Logger;
import ru.konungstvo.exceptions.CombatException;
import ru.konungstvo.exceptions.DataException;
import ru.konungstvo.player.NPC;
import ru.konungstvo.player.Player;


import java.io.Serializable;
import java.util.*;

/**
 * Represents a combat between players and/or npcs.
 */
public class Combat implements Serializable, Cloneable {
    private String name;
    private int id;
    private int stateID;
    private ReactionList reactionList;
    private List<Player> fighters;
    private List<DeprecatedAttack> attackList;
    private List<Opportunity> opportunityList;

    private List<Boom> booms;
    private static Logger logger = new Logger("COMBAT");
    private boolean thirdPersonBlocked = false;
    private long startingTime = 0;


    public Combat(String name, int id) {
        this.name = name;
        this.id = id;
        this.stateID = 0;
        this.reactionList = null;
        this.attackList = new ArrayList<>();
        this.opportunityList = new ArrayList<>();
        this.fighters = new ArrayList<>();
        this.booms = new ArrayList<>();
        this.startingTime = MinecraftServer.getCurrentTimeMillis();
        logger.info("Combat with name " + this.name + " was created");
    }

    public Object clone() throws CloneNotSupportedException {
        return (Combat) super.clone();
    }

    public static Combat getCopy(Combat other) {
        Combat result = new Combat(other.getName(), other.getId());
        result.stateID = other.stateID;
        result.reactionList = ReactionList.getCopy(other.reactionList);
        result.attackList = other.attackList;
        result.opportunityList = other.opportunityList;
        result.fighters = other.fighters;
        return result;
    }

    public Player addFighter(String name) {
        Player fighter = DataHolder.inst().getPlayer(name);
        if (hasFighter(name)) {
            System.out.println("Fighter " + name + " existed, skipping.");
            return fighter;
        }
        this.fighters.add(fighter);
        System.out.println("Fighter " + fighter.getName() + " joined Combat " + this.name);
        return fighter;
    }

    public void removeFighter(String fighterName) {
        fighters.remove(new Player(fighterName));
        reactionList.removeReaction(fighterName);
        reactionList.getQueue().removePlayer(fighterName);
    }

    public List<Player> getFighters() {
        return this.fighters;
    }


    public boolean isAttacked(String playerName) {
        return getAttackByDefender(playerName) != null;
    }

    public boolean isAttacker(String attackerName) {
        return getAttackByAttacker(attackerName) != null;
    }

    public boolean hasFighter(String fighterName) {
        if (getFighters().isEmpty()) return false;
        for (Player fighter : getFighters()) {
//            logger.debug("Found " + fighter.getName());
            if (fighter.getName().equals(fighterName))
                return true;
        }
        return false;
    }

    public ReactionList getReactionList() {
        return reactionList;
    }

    public void setReactionList(ReactionList reactionList) {
        this.reactionList = reactionList;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Combat)) return false;
        Combat other = (Combat) obj;
        return id == other.id;
    }

    public static Logger getLogger() {
        return logger;
    }

    public void addAttack(DeprecatedAttack attack) {
        logger.info("Attack was added to Combat " + name);
        this.attackList.add(attack);
    }


    public void defendAttack(String defenderName, SkillDiceMessage skillDiceMessage) throws Exception {
        DeprecatedAttack attack = getAttackByDefender(defenderName);
        if (attack == null) throw new CombatException("Вы пытаетесь защититься.", "На вас никто не нападает!");
        if (skillDiceMessage.getDice().getSkillName().equals("блокирование") && attack.getDefender().getShield() == null) {
            throw new CombatException("", "Сначала снарядите щит с помощью /equip! Например: /equip блокирование +1");
        }

        logger.info(defenderName + " is defending himself with " + skillDiceMessage.getDice().toString());

        attack.defend(skillDiceMessage);

        String previousDefenses = "";
        Player defender = DataHolder.inst().getPlayer(defenderName);
        if (defender.getPreviousDefenses() != 0) {
            previousDefenses = ChatColor.COMBAT + " с доп. штрафом " + defender.getPreviousDefenses() + ChatColor.DICE;
        }
        attack.setResultNotification(attack.getDefender().getDisplayedName() + ChatColor.COMBAT + " защищается от атаки " +
                ChatColor.NICK + attack.getDefender().getDisplayedName() + previousDefenses + ChatColor.COMBAT);
        this.attackList.remove(attack);
        if (!attack.getAttacker().isSuperFighter() && !(attack.getAttacker() instanceof NPC)) {
            QueueExecutor.execute(DataHolder.inst().getMasterForCombat(id), "next");
        }
        logger.debug("We got here 2222222");
    }


    //TODO: this might be really heavy on perfomance
    @Deprecated
    private List<Player> getResponsibleMasters() {
        List<Player> result = new ArrayList<>();
        for (Player player : DataHolder.inst().getPlayerList()) {
            if (player.getAttachedCombatID() == this.id) {
                result.add(player);
            }
        }
        return result;
    }

    public DeprecatedAttack getAttackByDefender(String defenderName) {
        for (DeprecatedAttack attack : attackList) {
            if (attack.getDefender().getName().equals(defenderName)) {
                return attack;
            }
        }
        return null;
    }

    public DeprecatedAttack getAttackByAttacker(String attackerName) {
        for (DeprecatedAttack attack : attackList) {
            if (attack.getAttacker().getName().equals(attackerName)) {
                return attack;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void sendNotification(String notification, List<Player> players) {
        Set<Player> receivers = new LinkedHashSet<>(players);
        for (Player receiver : receivers) {
            receiver.sendMessage("§5" + notification);
        }
    }

    public int getStateID() {
        return stateID;
    }

    public void setStateID(int stateID) {
        this.stateID = stateID;
    }

    public void incrementStateID() {
        this.stateID++;
    }

    public CombatMemento saveStateToMemento() {
        try {
            return new CombatMemento(this);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean getThirdPersonBlocked() {
        return thirdPersonBlocked;
    }

    public void setThirdPersonBlocked(boolean blocked) {
        this.thirdPersonBlocked = blocked;
    }


    public void removeAttack(DeprecatedAttack attack) {
        this.attackList.remove(attack);
    }

//    public NPC spawnNpc(String npcName) {
//       return spawnNpc(npcName, npcName);
//    }

    // create an NPC with unique (to the combat) name
    public NPC spawnNpc(String npcName, String customName, boolean magicName) throws DataException {
        int number = 1;

        for (Player pl : DataHolder.inst().getNpcList()) {
            if (pl.getName().startsWith(customName)) {
                String numberStr = pl.getName().substring(customName.length());
                try {
                    number = Integer.parseInt(numberStr) + 1;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        String displayName = customName + number;
        if (magicName) {
            displayName = customName;
        }
        NPC npc = DataHolder.inst().createNPC(displayName, npcName);

        this.addFighter(npc.getName());
        return npc;
    }

    public void removeNPCs() {
        List<Player> toRemove = new ArrayList<>();
        for (Player fighter : fighters) {
            if (fighter instanceof NPC) {
                logger.debug("Found " + fighter.getName() + " to remove.");
                toRemove.add(fighter);
            }
        }
        for (Player p : toRemove) {
            fighters.remove(p);
            DataHolder.inst().removePlayer(p.getName());
        }
    }

    public long getTime() {return startingTime;}

    public  List<DeprecatedAttack> getAttacks() {
        return attackList;
    }

    public void addBoom(Boom boom) {
        booms.add(boom);
    }

    public void removeBoom(Boom boom) {
        booms.remove(boom);
    }

    public Boom getBoomForPlayer(Player player) {
        for (Boom boom : booms) {
            if (boom.hasTarget(player)) return boom;
        }
        return null;
    }
}
