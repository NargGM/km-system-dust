package ru.konungstvo.combat;

import ru.konungstvo.chat.ChatColor;

public enum BoomType {
    DEFAULT("осколочная", "§7"),
    FIRE("зажигательная", "§6"),
    GAS("газовая", "§2"),
    WEAK_GAS("слабеющий газ", "§a"),
    STUNNING("свето-шумовая", "§3");


    private String desc;
    private String color;

    BoomType(String desc, String color) {
        this.desc = desc;
        this.color = color;
    }

    @Override
    public String toString() {
        return desc;
    }
    public String color() {
        return color;
    }
}

