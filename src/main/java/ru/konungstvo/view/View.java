package ru.konungstvo.view;

import java.util.Observable;
import java.util.Observer;


// a superclass for different views that output various information on screen
public class View implements Observer {
    @Override
    public void update(Observable o, Object arg) {

    }
}
