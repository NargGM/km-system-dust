package ru.konungstvo.view;

public class Envelope<T> {
    private String playerName;
    private T message;

    public Envelope(String playerName, T message) {
        this.playerName = playerName;
        this.message = message;
    }

    public String getPlayerName() {
        return playerName;
    }

    public T getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Envelope)) return false;
        Envelope other = (Envelope) obj;
        return this.getMessage().equals(other.getMessage()) && this.getPlayerName().equals(other.getPlayerName());
    }

    @Override
    public String toString() {
        return "["+ playerName + ":" + message +"]";
    }
}
