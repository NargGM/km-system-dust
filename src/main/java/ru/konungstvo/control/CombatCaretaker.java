package ru.konungstvo.control;

import java.util.ArrayList;
import java.util.List;

public class CombatCaretaker {
    private List<CombatMemento> mementoList = new ArrayList<>();
    private Logger logger = new Logger(this.getClass().getSimpleName());

    public void add(CombatMemento memento) {
        mementoList.add(memento);
    }

    public CombatMemento get(int combatID, int stateID) {
        logger.debug("Getting combat from caretaker, cID = " + combatID + ", sID = " + stateID);
        for (CombatMemento m : mementoList) {
            if (m.getSavedState().getId() == combatID && m.getSavedState().getStateID() == stateID) {
                return m;
            }
        }
        return null;
    }

}
