package ru.konungstvo.control;

import java.util.HashMap;

public class MovementHelper {
    private static HashMap<Integer, Integer> table = new HashMap<>();
    public static HashMap<String, Double> types = new HashMap<>();

    public MovementHelper() {
        table.put(-4, 0);
        table.put(-3, 0);
        table.put(-2, 1);
        table.put(-1, 2);
        table.put(0, 4);
        table.put(1, 6);
        table.put(2, 8);
        table.put(3, 10);
        table.put(4, 12);
        table.put(5, 14);
        table.put(6, 16);
        table.put(7, 18);

        types.put("ползком", 0.5);
        types.put("гуськом", 0.75);
        types.put("в приседе", 0.75);
        types.put("свободное передвижение", 0.5);
        types.put("сп", 0.5);
        types.put("разбег", 1.);
        types.put("разгон", 1.);
        types.put("шаг", 1.);
        types.put("передвижение", 2.);
        types.put("быстрый бег", 3.);
        types.put("супербег", 4.);
    }

    public static boolean isMovement(String dice, String alias) {
        for (String s : types.keySet()) {
            if (dice.contains(s) || alias.contains(s)) {
                return true;
            }
        }
        return false;
    }

    public int getNumberOfBlocks(int level) {
        return getNumberOfBlocks(level, 1.);
    }

    public int getNumberOfBlocks(int level, double modifier) {
        if (level == -4 && modifier > 1) return -1;
        return (int) (table.get(level)*modifier);
    }

    public String getMovementType(String message) {
        for (String m : types.keySet()) {
            if (message.contains(m)) {
                return m;
            }
        }
        return "";
    }
}
