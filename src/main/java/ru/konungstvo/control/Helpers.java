package ru.konungstvo.control;

import net.minecraft.block.BlockStairs;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.event.ClickEvent;
import org.apache.commons.codec.binary.Base64;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import ru.konungstvo.Reminder;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.commands.executor.PurgeModifiersExecutor;
import ru.konungstvo.commands.executor.RemoveSubordinateExecutor;
import ru.konungstvo.commands.executor.SubExecutor;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.control.network.ClickContainerMessage;
import ru.konungstvo.control.network.KMPacketHandler;
import ru.konungstvo.player.Player;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;


public class Helpers {

    public static void sendClickContainerToClient(Message message, String container, EntityPlayerMP client, Player subordinate) {
        // SEND PACKET TO CLIENT
        if (message == null) return;

        if (!client.getName().equals(subordinate.getName())) {
            Message message1 = new Message("Ваш подчинённый:");
            MessageComponent who = new MessageComponent( " [" + subordinate.getName() + "]", ChatColor.GRAY);
            who.setHoverText("ЛКМ для телепортации к подчиненному!", TextFormatting.BLUE);
            who.setClickCommand("/" + SubExecutor.NAME + " " + subordinate.getName() +" tp");
            message1.dim();
            MessageComponent click = new MessageComponent(" [Удалить]");
            click.setColor(ChatColor.GRAY);
            click.setClickCommand("/" + RemoveSubordinateExecutor.NAME);
            message1.addComponent(who);
            message1.addComponent(click);
            Helpers.sendClickContainerToClient(message1, "Subordinate", client);
        } else {
            ClickContainer.deleteContainer("Subordinate", client);
        }

        String mods = subordinate.getModString();
        Message debug = null;
        if (!mods.isEmpty()) {
            System.out.println("моды не empty");
            debug = new Message(String.format("Модификаторы: %s", mods));
            MessageComponent removeDebug = new MessageComponent(" [Убрать все]");
            removeDebug.setClickCommand("/" + PurgeModifiersExecutor.NAME);
            removeDebug.setUnderlined(true);
            debug.addComponent(removeDebug);
            debug.dim();
            debug.italic();
        }


        if (debug != null) {
            Helpers.sendClickContainerToClient(debug, "Debug", client);
        } else {
            ClickContainer.deleteContainer("Debug", client);
        }

        String json = subordinate.getEffectMessage(false);
        if (json.isEmpty()) {
            ClickContainer.deleteContainer("Effects", client);

        } else {
            ClickContainerMessage result = new ClickContainerMessage("Effects", json);
            KMPacketHandler.INSTANCE.sendTo(result, (EntityPlayerMP) client);
        }

        sendClickContainerToClient(message, container, client);
    }
    public static void sendClickContainerToClient(Message message, String container, EntityPlayerMP client) {
        // SEND PACKET TO CLIENT
        if (message == null) return;
        String json = ITextComponent.Serializer.componentToJson(MessageGenerator.generate(message));
        ClickContainerMessage result = new ClickContainerMessage(container, json);
        KMPacketHandler.INSTANCE.sendTo(result, client);
    }

    public static String FixColorFormatting(String message) {
        String res = message.replaceAll("§.", "");
        return res;

    }

    public static JSONObject readJsonFromUrl(String webpage, String authString) throws IOException, ParseException {
        URL url = new URL(webpage);

        byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
        String authStringEnc = new String(authEncBytes);
        URLConnection urlConnection = url.openConnection();

        urlConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);

        try (InputStream is = urlConnection.getInputStream()) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONParser parser = new JSONParser();
            return (JSONObject) parser.parse(jsonText);
        }
    }

    public static JSONArray readJsonArrayFromUrl(String webpage, String authString) throws IOException, ParseException {
        URL url = new URL(webpage);

        byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
        String authStringEnc = new String(authEncBytes);
        URLConnection urlConnection = url.openConnection();

        urlConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);

        try (InputStream is = urlConnection.getInputStream()) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONParser parser = new JSONParser();
            return (JSONArray) parser.parse(jsonText);
        }
    }

    public static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static void writeJson(String filename, String jsonText) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(jsonText);
        Files.write(Paths.get(filename), json.toJSONString().getBytes());
    }

    public static JSONObject readJson(String filename) throws IOException, ParseException {
        FileReader reader = null;
        try {
            reader = new FileReader(filename);
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            return new JSONObject();
        }
        JSONParser jsonParser = new JSONParser();
        return (JSONObject) jsonParser.parse(reader);
    }

    public static boolean hasSpaceForXp(String playerName) {
        File f = new File("/srv/www/main_site/data/expirience/" + playerName + ".exp");
        if(!f.exists() && !f.isDirectory()) {
            return true;
        }
        Path path = Paths.get("/srv/www/main_site/data/expirience/" + playerName + ".exp");
        System.out.println("TEST EXP");
        long lines = 0;
        try {
            System.out.println("TEST EXP1");
            lines = Files.lines(path).count();
            System.out.println("TEST EXP2");
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            System.out.println("TEST EXP3");
            return true;
        }  catch (IOException e) {
            System.out.println("TEST EXP4");
            return false;
        } catch (Exception e) {
            System.out.println("TEST EXP5");
            e.printStackTrace();
        }
        System.out.println("TEST EXP6");
        return lines < 5;
    }

    public static boolean addXp(String playerName, String xp) {
        String filename = "/srv/www/main_site/data/expirience/" + playerName + ".exp";
        long lines = 0;
        Writer output;
        try {
            output = new BufferedWriter(new FileWriter(filename, true));
        } catch (FileNotFoundException e) {
            File myObj = new File(filename);
            try {
                if (myObj.createNewFile()) {
                    System.out.println("File created: " + myObj.getName());
                    output = new BufferedWriter(new FileWriter(filename, true));
                } else {
                    System.out.println("File already exists.");
                    return false;
                }
            } catch (Exception ignored) {
                return false;
            }
        }  catch (IOException e) {
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        try {
            output.append(xp).append("\n");
            output.close();
        } catch (Exception ignored) {
            return false;
        }

        return true;
    }

    public static ArrayList<String> getXp(String playerName) {
        String filename = "/srv/www/main_site/data/expirience/" + playerName + ".exp";
        ArrayList<String> xp = new ArrayList<String>();
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(
                    filename));
            String line = reader.readLine();
            while (line != null) {
                xp.add(line);
                // read next line
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return xp;
    }

    public static void writeSerialized(String path, Object object) throws IOException {
        File dir = new File(path + "/");
        if (!dir.exists()) dir.mkdir();
        File file = null;
        if (object instanceof Player) {
            Player player = (Player) object;
            file = new File(path + "/" + player.getName() + ".slz");
        } else if (object instanceof Reminder) {
            Reminder reminder = (Reminder) object;
            file = new File(path + "/" + reminder.getPlayerName() + ".slz");
        }


        if (file == null) {
            System.out.println("Trying to write in path: " + path + " with empty file name!");
            return;
        }
        FileOutputStream fileOutputStream
                = new FileOutputStream(file);
        ObjectOutputStream objectOutputStream
                = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(object);
        objectOutputStream.flush();
        objectOutputStream.close();
    }

    public static Object readSerialized(String path, String filename) throws IOException, ClassNotFoundException {
        if (!filename.contains(".slz")) filename += ".slz";
        FileInputStream fileInputStream
                = new FileInputStream(path + "/" + filename);
        ObjectInputStream objectInputStream
                = new ObjectInputStream(fileInputStream);
        Object obj = objectInputStream.readObject();
        objectInputStream.close();
        return obj;
    }

    public static Player getSavedOrNewPlayer(String playerName) {
        Player player;
        try {
            player = (Player) readSerialized("saved_players", playerName);
        } catch (IOException | ClassNotFoundException e) {
            System.out.println(e.getMessage());
            return new Player(playerName);
        }
        return player;
    }

    public static boolean hasSavedPlayer(String playerName) {
        Player player;
        try {
            player = (Player) readSerialized("saved_players", playerName);
            return true;
        } catch (Exception ignored) {
            return false;
        }
    }

    public static Range convertToDieRange(Range defaultRange) {
        switch (defaultRange) {
            case NORMAL:
                return Range.NORMAL_DIE;
            case HUSHED:
                return Range.HUSHED_DIE;
            case WHISPER:
                return Range.WHISPER_DIE;
            case QUIET:
                return Range.QUIET_DIE;
            case LOUD:
                return Range.LOUD_DIE;
            case SHOUT:
                return Range.SHOUT_DIE;
            case SCREAM:
                return Range.SCREAM_DIE;
            case GM:
                return Range.GM_DIE;
            case CM:
                return Range.CM_DIE;
        }
        return defaultRange;
    }

    public static String sendJsonToUrl(JSONObject jsonObject, String url, String auth) {
        //return "666";

        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        try {
            HttpPost request = new HttpPost(url);
            StringEntity params = new StringEntity(jsonObject.toString(), "UTF-8");

            byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
            String authHeader = "Basic " + new String(encodedAuth);

            request.setHeader("AUTHORIZATION", authHeader);
            request.addHeader("content-type", "application/x-www-form-urlencoded");
            request.setHeader("charset", "UTF-8");
            request.setEntity(params);
            HttpResponse response = httpClient.execute(request);
            System.out.println(response.toString());

// handle response here...
        } catch (Exception ex) {
            // handle exception here
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "Succ";
   }

    public static String getUrl(String url, String auth) {
        //return "666";

        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        try {
            HttpGet request = new HttpGet(url);

            byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
            String authHeader = "Basic " + new String(encodedAuth);

            request.setHeader("AUTHORIZATION", authHeader);
            request.addHeader("content-type", "application/x-www-form-urlencoded");
            request.setHeader("charset", "UTF-8");
            HttpResponse response = httpClient.execute(request);
            System.out.println(response.toString());

// handle response here...
        } catch (Exception ex) {
            // handle exception here
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "Succ";
    }

    public static String getPlural(int num, String f1, String f2, String f5) {
        if (num > 10 && num < 20) return f5;
        if (num%10 > 1 && num%10 < 5) return f2;
        if (num%10 == 1) return f1;
        return f5;
    }

    public static EnumHand getEnumHand(int num) {
        switch (num) {
            case 0:
                return EnumHand.OFF_HAND;
            case 1:
                return EnumHand.MAIN_HAND;
        }
        return null;
    }
}
