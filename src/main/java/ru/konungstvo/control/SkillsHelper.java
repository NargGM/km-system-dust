package ru.konungstvo.control;

import java.util.ArrayList;
import java.util.List;

public class SkillsHelper {
    private static List<String> essentialSkills = new ArrayList<>();
    private static List<String> armorDependentSkills = new ArrayList<>();


    public SkillsHelper() {
        essentialSkills.add("рукопашный бой");
        essentialSkills.add("передвижение");
        essentialSkills.add("реакция");
        essentialSkills.add("уклонение");

        armorDependentSkills.add("реакция");
        armorDependentSkills.add("уклонение");
        armorDependentSkills.add("передвижение");
        armorDependentSkills.add("скрытность");
    }

    public static boolean isEssential(String skillName) {
        if (skillName.startsWith("владение") || skillName.startsWith("парирование")) return true;
        return essentialSkills.contains(skillName);
    }

    public static List<String> getEssentials() {
        return essentialSkills;
    }

    public static boolean isArmorDependent(String skillName) {
        return armorDependentSkills.contains(skillName);
    }

    public static List<String> getArmorDependentSkills() {
        return armorDependentSkills;
    }
}
