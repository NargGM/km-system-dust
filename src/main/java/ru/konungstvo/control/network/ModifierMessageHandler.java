package ru.konungstvo.control.network;

import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import ru.konungstvo.bridge.ClientProxy;

// The params of the IMessageHandler are <REQ, REPLY>
// This means that the first param is the packet you are receiving, and the second is the packet you are returning.
// The returned packet can be used as a "response" from a sent packet.
public class ModifierMessageHandler implements IMessageHandler<ModifierMessage, IMessage> {
    // Do note that the default constructor is required, but implicitly defined in this case

    @Override
    public IMessage onMessage(ModifierMessage message, MessageContext ctx) {
        System.out.println("Recieving modifiers packet on " + FMLCommonHandler.instance().getSide());

        if (FMLCommonHandler.instance().getSide().isClient()) {
            if (message.modifiedDamage) {
                System.out.println("HEY DAMAGE");
                ClientProxy.setModifiedDamage(message.modifier);
            } else if (message.bodyPart) {
                ClientProxy.setBodyPart(message.modifier);
                System.out.println("HEY BODYPART");
            } else if (message.serial) {
                ClientProxy.setSerial(message.modifier);
                System.out.println("HEY SERIAL");
            } else if (message.percent) {
            } else if (message.stationary) {
                ClientProxy.setStationary(message.modifier);
            } else if (message.cover) {
                ClientProxy.setCover(message.modifier);
            } else if (message.optic) {
                ClientProxy.setOptic(message.modifier);
            } else if (message.bipod) {
                ClientProxy.setBipod(message.modifier);
            } else if (message.stunning){
                ClientProxy.setStunning(message.modifier);
            } else if (message.capture){
                ClientProxy.setCapture(message.modifier);
            } else if (message.noriposte){
                ClientProxy.setNoRiposte(message.modifier);
            } else if (message.shieldwall){
                ClientProxy.setShieldwall(message.modifier);
            } else if (message.suppressing){
                ClientProxy.setSuppressing(message.modifier);
            } else {
                ClientProxy.setModifiedDice(message.modifier);
                System.out.println("HEY DICE " + message.modifier);
            }

    }

    // No response packet
        return null;

}
}

