package ru.konungstvo.control.network;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

public class ClientGoMessage implements IMessage {
    public int blocksToCover;

    public ClientGoMessage() {}

    public ClientGoMessage(int blocksToCover) {
        this.blocksToCover = blocksToCover;
    }


    @Override
    public void fromBytes(ByteBuf buf) {
        blocksToCover = buf.readInt();

    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(blocksToCover);


    }
}
