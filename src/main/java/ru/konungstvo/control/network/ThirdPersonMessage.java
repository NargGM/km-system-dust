package ru.konungstvo.control.network;

import de.cas_ual_ty.gci.GunCus;
import de.cas_ual_ty.gci.SoundEventGCI;
import de.cas_ual_ty.gci.network.MessageSound;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import ru.konungstvo.bridge.ClientProxy;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

public class ThirdPersonMessage implements IMessage {
    public int thirdPerson = 0;

    public ThirdPersonMessage() {
    }

    //public ModifierMessage(boolean modifiedDamage, int modifier, boolean bodyPart) {
    //    this(modifiedDamage, modifier, bodyPart);
    //}
    public ThirdPersonMessage(int thirdPerson) {
        this.thirdPerson = thirdPerson;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        thirdPerson = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(thirdPerson);
    }

    public static class ThirdPersonMessageHandler implements IMessageHandler<ThirdPersonMessage, IMessage>
    {
        @SideOnly(Side.SERVER)
        @Override
        public IMessage onMessage(ThirdPersonMessage message, MessageContext ctx)
        {
//            if (!Minecraft.getMinecraft().world.isRemote) {
                EntityPlayer forgePlayer = GunCus.proxy.getClientPlayer(ctx);
                if (forgePlayer != null) {
                    Player player = DataHolder.inst().getPlayer(forgePlayer.getName());
                    if (player.hasPermission(Permission.GM)) return null;
                    if (DataHolder.inst().getCombatForPlayer(player.getName()) != null) {
                        Player master = DataHolder.inst().getMasterForPlayerInCombat(player);
                        String view = "";
                        switch (message.thirdPerson) {
                            case 0:
                                view = "вид от первого лица";
                                break;
                            case 1:
                                view = "вид от третьего лица";
                                break;
                            case 2:
                                view = "вид от третьего лица (спереди)";
                                break;
                            case 3:
                                view = "вид от третьего лица (отменено)";
                                break;
                        }
                        TextComponentString masterInfo = new TextComponentString("[GM] " + player.getName() + " переключается на " + view);
                        masterInfo.getStyle().setColor(TextFormatting.RED);
                        DataHolder.inst().informMasterForCombat(player.getName(), masterInfo);
                    }
                }
//            } else {
//                if (message.thirdPerson == -666) {
//                    ClientProxy.setThirdPersonBlocked(true);
//                } else {
//                    ClientProxy.setThirdPersonBlocked(false);
//                }
//                Minecraft.getMinecraft().gameSettings.thirdPersonView = 0;
//            }

            return null;
        }

    }

}
