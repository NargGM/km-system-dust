package ru.konungstvo.control.network;

import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import ru.konungstvo.bridge.ClientProxy;

// The params of the IMessageHandler are <REQ, REPLY>
// This means that the first param is the packet you are receiving, and the second is the packet you are returning.
// The returned packet can be used as a "response" from a sent packet.
public class ModifierSyncMessageHandler implements IMessageHandler<ModifierSyncMessage, IMessage> {
    // Do note that the default constructor is required, but implicitly defined in this case

    @Override
    public IMessage onMessage(ModifierSyncMessage message, MessageContext ctx) {
        System.out.println("Recieving modifiers packet on " + FMLCommonHandler.instance().getSide());

        if (FMLCommonHandler.instance().getSide().isClient()) {
            System.out.println("HEY DAMAGE" + message.modifiedDamage);
            ClientProxy.setModifiedDamage(message.modifiedDamage);
            System.out.println("HEY BODYPART" + message.bodyPart);
            ClientProxy.setBodyPart(message.bodyPart);
            System.out.println("HEY DICE " + message.modifiedDice);
            ClientProxy.setModifiedDice(message.modifiedDice);
            if (message.serial) {
                ClientProxy.setSerial(1);
                System.out.println("HEY SERIAL");
            } else {
                ClientProxy.setSerial(-666);
            }
            if (message.percent != -666) {
            }
            if (message.stationary) {
                ClientProxy.setStationary(1);
            } else {
                ClientProxy.setStationary(-666);
            }
            if (message.cover) {
                ClientProxy.setCover(1);
            } else {
                ClientProxy.setCover(-666);
            }
            if (message.optic) {
                ClientProxy.setOptic(1);
            } else {
                ClientProxy.setOptic(-666);
            }
            if (message.bipod) {
                ClientProxy.setBipod(1);
            } else {
                ClientProxy.setBipod(-666);
            }
            if (message.stunning){
                ClientProxy.setStunning(1);
            } else {
                ClientProxy.setStunning(-666);
            }
            if (message.capture){
                ClientProxy.setCapture(1);
            } else {
                ClientProxy.setCapture(-666);
            }
            if (message.noriposte){
                ClientProxy.setNoRiposte(1);
            } else {
                ClientProxy.setNoRiposte(-666);
            }
            if (message.shieldwall){
                ClientProxy.setShieldwall(1);
            } else {
                ClientProxy.setShieldwall(-666);
            }
            if (message.suppressing){
                ClientProxy.setSuppressing(1);
            } else {
                ClientProxy.setSuppressing(-666);
            }


    }

    // No response packet
        return null;

}
}

