package ru.konungstvo.control.network;

import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

// The params of the IMessageHandler are <REQ, REPLY>
// This means that the first param is the packet you are receiving, and the second is the packet you are returning.
// The returned packet can be used as a "response" from a sent packet.
public class PacketMessageHandler implements IMessageHandler<PacketMessage, IMessage> {
    // Do note that the default constructor is required, but implicitly defined in this case

    @Override public IMessage onMessage(PacketMessage message, MessageContext ctx) {
        System.out.println("Recieving packet on " + FMLCommonHandler.instance().getSide());
        // This is the player the packet was sent to the server from

//        System.out.println("Packet content: " + message.toSend);
        /*




        if (FMLCommonHandler.instance().getSide().equals(Side.SERVER)) {
            EntityPlayerMP serverPlayer = ctx.getServerHandler().player;
            // The value that was sent
            int amount = message.toSend;
            System.out.println("RECEIVED PACKET!!! amount= " + amount);
            // Execute the action on the main server thread by adding it as a scheduled task
            serverPlayer.getServerWorld().addScheduledTask(() -> {
                serverPlayer.inventory.addItemStackToInventory(new ItemStack(Items.DIAMOND, amount));
            });
        }

         */
        // No response packet
        return null;

    }
}

