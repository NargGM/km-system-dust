package ru.konungstvo.control;

import ru.konungstvo.combat.Combat;

public class CombatMemento {
    private final Combat combat;
    private Logger logger = new Logger(this.getClass().getSimpleName());

    public CombatMemento(Combat combat) throws CloneNotSupportedException {
        this.combat = Combat.getCopy(combat);
        //this.combat = (Combat) combat.clone();
        logger.debug("Created Memento with combat " + combat.getName() + " of state " + combat.getStateID() + "[" + combat.getReactionList().getQueue().getNext().getName() + "]");
    }

    public Combat getSavedState() {
        return combat;
    }
}
