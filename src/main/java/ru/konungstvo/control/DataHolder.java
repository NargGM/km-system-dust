package ru.konungstvo.control;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.mongodb.client.model.Filters;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import noppes.npcs.api.IWorld;
import noppes.npcs.api.NpcAPI;
import noppes.npcs.api.constants.EntityType;
import noppes.npcs.api.entity.IEntity;
import noppes.npcs.api.entity.data.IData;
import noppes.npcs.api.wrapper.NPCWrapper;
import noppes.npcs.entity.EntityNPCInterface;
import org.apache.commons.lang3.tuple.MutablePair;
import org.bson.Document;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import ru.konungstvo.Reminder;
import ru.konungstvo.bridge.Charsheet;
import ru.konungstvo.bridge.MongoBridge;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.bridge.discord.DiscordBridge;
import ru.konungstvo.bridge.Modifiers;
import ru.konungstvo.chat.message.FudgeDiceMessage;
import ru.konungstvo.combat.*;
import ru.konungstvo.combat.dice.modificator.Modificator;
import ru.konungstvo.combat.dice.modificator.PercentDice;
import ru.konungstvo.combat.duel.Duel;
import ru.konungstvo.combat.equipment.Shield;
import ru.konungstvo.combat.movement.MovementTrait;
import ru.konungstvo.combat.reactions.Queue;
import ru.konungstvo.combat.reactions.ReactionList;
import ru.konungstvo.exceptions.DataException;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.*;
import ru.konungstvo.player.wounds.WoundPyramid;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This Singleton class handles a huge deal of static info's like player's lists.
 * Allow communication between various parts of plugin.
 */
public class DataHolder {
    private static DataHolder instance;
    private static int combatID;

    private static HashMap<Integer, String> skillTable = new HashMap<>();
    private static List<Player> playerList = new ArrayList<>();
    private static List<NPC> npcList = new ArrayList<>();
    private static List<Combat> combatList = new ArrayList<>();
    private static CombatCaretaker combatCaretaker = new CombatCaretaker();

    private static Config config;
    private static String skillPath;
    private static String npcTemplatesPath;
    private static Modifiers modifiers;
    private static Logger logger = new Logger("Data");
    private static SkillsHelper skillsHelper = new SkillsHelper();
    private static MovementHelper movementHelper = new MovementHelper();
    private static List<Reminder> reminders;
    private static DiscordBridge discordBridge;
    private static List<Duel> duels = new ArrayList<>();
    private static HashMap<String, EntityNPCInterface> npcsMap = new HashMap<>();


    private DataHolder() {
        config = new Config("config.json");
        combatID = 0;

        skillPath = config.readConfig("skillPath");
        npcTemplatesPath = config.readConfig("npcTemplatesPath");

        String modifiersURL = config.readConfig("modifiersURL");
        String modifiersAuth = config.readConfig("modifiersAuth");
        modifiers = new Modifiers(modifiersURL, modifiersAuth);
        reminders = new ArrayList<>();
        String token = config.readConfig("bot_token");
        String channel_id = config.readConfig("channel_id");
        //discordBridge = new DiscordBridge(token, channel_id);
        try {
            DiscordBridge.create(token, channel_id);
        } catch (Exception e) {
            //e.printStackTrace();
        }


        skillTable.put(-5, "абсолютно ублюдски");
        skillTable.put(-4, "ужасно---");
        skillTable.put(-3, "ужасно--");
        skillTable.put(-2, "ужасно-");
        skillTable.put(-1, "ужасно");
        skillTable.put(0, "плохо");
        skillTable.put(1, "посредственно");
        skillTable.put(2, "нормально");
        skillTable.put(3, "хорошо");
        skillTable.put(4, "отлично");
        skillTable.put(5, "превосходно");
        skillTable.put(6, "легендарно");
        skillTable.put(7, "легендарно+");
        skillTable.put(8, "легендарно++");
        skillTable.put(9, "легендарно+++");
        skillTable.put(10, "КАК АЛЛАХ");
    }

    public static String getNpcTemplatesPath() {
        return npcTemplatesPath;
    }

    public static void setNpcTemplatesPath(String npcTemplatesPath) {
        DataHolder.npcTemplatesPath = npcTemplatesPath;
    }

    @Deprecated
    public Duel getDuelForPlayer(String name) {
        for (Duel duel : duels) {
            //System.out.println("Searching duel for " + name);
            //System.out.println("Att: " + duel.getAttacker().getName());
            try {
                //System.out.println("Def: " + duel.getDefender().getName());
            } catch (Exception ignored) {
            }
            if (duel.getAttacker().getName().equals(name)) return duel;
            if (duel.getDefender() != null && duel.getDefender().getName().equals(name)) return duel;
        }
        return null;
    }

    public Duel getDuelForAttacker(String name) {
        for (Duel duel : duels) {
            //System.out.println("Searching duel for " + name);
            //System.out.println("Att: " + duel.getAttacker().getName());
            try {
                //System.out.println("Def: " + duel.getDefender().getName());
            } catch (Exception ignored) {
            }
            if (duel.getAttacker().getName().equals(name)) return duel;
        }
        return null;
    }

    public boolean hasRealDuelAttacker(String name) {
        for (Duel duel : duels) {
            //System.out.println("Searching duel for " + name);
            //System.out.println("Att: " + duel.getAttacker().getName());
            try {
                //System.out.println("Def: " + duel.getDefender().getName());
            } catch (Exception ignored) {
            }
            if (duel.getAttacker().getName().equals(name) && duel.getDefender() != null && !duel.isEnded()) return true;
        }
        return false;
    }

    public ArrayList<Duel> getAllDuelsForAttacker(String name) {
        ArrayList<Duel> attackerDuels = new ArrayList<>();
        for (Duel duel : duels) {
            //System.out.println("Searching duel for " + name);
            //System.out.println("Att: " + duel.getAttacker().getName());
            try {
                //System.out.println("Def: " + duel.getDefender().getName());
            } catch (Exception ignored) {
            }
            if (duel.getAttacker().getName().equals(name)) attackerDuels.add(duel);
        }
        return attackerDuels;
    }

    public Duel getDuelForDefender(String name) {
        for (Duel duel : duels) {
            //System.out.println("Searching duel for " + name);
            //System.out.println("Att: " + duel.getAttacker().getName());
            try {
                //System.out.println("Def: " + duel.getDefender().getName());
            } catch (Exception ignored) {
            }
            if (duel.getDefender() != null && duel.getDefender().getName().equals(name)) return duel;
        }
        return null;
    }

    //~~~~~~~~~~~~~~~~~Player management
    public Player getPlayer(String playerName) {
        //if (playerName.contains("[GM]")) playerName = playerName.replace("[GM]", "");
        for (Player p : playerList) {
            if (p.getName().equals(playerName))
                return p;
        }
        for (NPC npc : npcList) {
            if (npc.getName().equals(playerName)) {
                return npc;
            }
        }
        System.out.println("НПЦ/игрок не найден!!!: " + playerName);
        System.out.println(npcList);
        System.out.println(npcsMap);
        return null;
    }

    public void addPlayer(Player player) {
        if (getPlayer(player.getName()) != null) {
/*            DiscordBridge.sendMessage("!!!!!!!!!!!!!!!!!!!!!!!!\n" +
                    "КРИТИЧЕСКАЯ ОШИБКА!\n" +
                    "Игрок " + player.getName() + " зашёл в игру, но в игре уже существует игрок или непись с таким именем!" +
                    "Удалите непися (ищите с помощью /combat list) и попросите игрока ПЕРЕЗАЙТИ!");
*/
            throw new DataException("При добавлении игрока.", "Игрок " + player.getName() + " уже существует!");
        }
        playerList.add(player);
        updatePlayer(player.getName());
        logger.info("Player " + player.getName() + " (" + player.getClass().getName() + ") was added.");
    }


    public void removePlayer(String playerName) {
        Player player = getPlayer(playerName);
        if (player instanceof NPC) {
            npcList.remove(player);
            return;
        }
        playerList.remove(player);
    }

    public List<Player> getPlayerList() {
        return playerList;
    }

    //~~~~~~~~~~~~~~~~~~Combat and fighters' management
    //gets all the Queues of active Combats
    public List<ReactionList> getQueueList() {
        List<ReactionList> result = new ArrayList<>();
        for (Combat combat : combatList) {
            result.add(combat.getReactionList());
        }
        return result;
    }

    public boolean inSameQueque (Player playerOne, Player playerTwo) {
        try {
            Combat combat = DataHolder.inst().getCombatForPlayer(playerOne.getName());
            Queue queue = combat.getReactionList().getQueue();
            return (queue.contains(playerOne.getName()) && queue.contains(playerTwo.getName()));
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
        return false;
    }

    public Combat createCombat(String combatName) {
        Combat combat = new Combat(combatName, combatID++);
        addCombat(combat);
        return combat;
    }

    private void addCombat(Combat combat) {
        if (combatList.contains(combat))
            throw new DataException("1", "Такой бой уже существует!");

        for (Player fighter : combat.getFighters()) {
            if (getPlayer(fighter.getName()) == null)
                throw new DataException("Во время добавления боя: ", "Игрок " + fighter.getName() + " не найден в кеше!");
        }
        combatList.add(combat);
    }

    public List<Combat> getCombatList() {
        return combatList;
    }

    public Combat getCombatForPlayer(String playerName) {
        Player player = DataHolder.inst().getPlayer(playerName);
        try {
            if (player.hasPermission(Permission.GM)) {
                return DataHolder.inst().getCombat(player.getAttachedCombatID());
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        for (Combat combat : combatList) {
            if (combat.hasFighter(playerName))
                return combat;
        }
        return null;
    }

    public Combat getCombat(int id) {
        for (Combat combat : combatList) {
            if (combat.getId() == id) {
                return combat;
            }
        }
        return null;
    }

    public Player getMasterForCombat(int id) {
        for (Player player : playerList) {
            System.out.println("searching for master through " + player.getName());
            System.out.println("check " + id + " == " + player.getAttachedCombatID());

            if (player.hasPermission(Permission.GM) && player.getAttachedCombatID() == id) {
                System.out.println("found " + player.getName());
                return player;
            }
        }
        return null;
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Save/load combat stated for /combat undo
    public void undoCombat(Combat combat) {
        int index = combatList.indexOf(combat);
        logger.debug("WAS " + combat.getName() + " [" + combat.getStateID() + "]" + combat.getReactionList().getQueue().getNext().getName() +
                "\n WILL BE " + getUndoneCombat(combat).getName() + " [" + getUndoneCombat(combat).getStateID() + "]" +
                getUndoneCombat(combat).getReactionList().getQueue().getNext().getName());
        assert combat != getUndoneCombat(combat);
        assert combat.getReactionList() != getUndoneCombat(combat).getReactionList();
        assert !combat.getReactionList().getQueue().equals(getUndoneCombat(combat).getReactionList().getQueue());
        combatList.set(index, getUndoneCombat(combat));
    }

    public void saveCombat(Combat combat) {
        //\combatCaretaker.add(combat.saveStateToMemento());
        combat.incrementStateID();
    }

    public Combat getUndoneCombat(Combat combat) {
        return combatCaretaker.get(combat.getId(), combat.getStateID() - 1).getSavedState();
    }


    public static synchronized DataHolder inst() {
        if (instance == null)
            instance = new DataHolder();
        return instance;
    }


    //~~~~~~~~~~~~~~~~~Updates of all sorts



    public void mongoUpdateSkills(Player player, String username) {
        if (MongoBridge.INSTANCE.sheets == null) return;
        System.out.println("Sync with DB... " + username);

        try {
            System.out.println(MongoBridge.INSTANCE.client.listDatabases());
            System.out.println(MongoBridge.INSTANCE.client.getDatabase("dustrpmongo").listCollectionNames());
            System.out.println(MongoBridge.INSTANCE.client.getDatabase("dustrpmongo").getCollection("charsheets").countDocuments());
            System.out.println(MongoBridge.INSTANCE.client.getDatabase("dustrpmongo").getCollection("charsheets").getReadConcern());
            System.out.println(MongoBridge.INSTANCE.client.getDatabase("dustrpmongo").getCollection("charsheets").getCodecRegistry());
            System.out.println(MongoBridge.INSTANCE.client.getDatabase("dustrpmongo").getCollection("charsheets").toString());
            System.out.println(MongoBridge.INSTANCE.client.getDatabase("dustrpmongo").getCollection("charsheets").find(Filters.eq("character", username)));
            System.out.println(MongoBridge.INSTANCE.client.getDatabase("dustrpmongo").getCollection("charsheets").find(Filters.eq("character", username)).first().keySet());
            System.out.println(MongoBridge.INSTANCE.client.getDatabase("dustrpmongo").getCollection("charsheets").find(Filters.eq("character", username)).first().get("skills"));
            MongoBridge.INSTANCE.client.getDatabase("dustrpmongo").getCollection("charsheets").listIndexes()
                    .forEach((Consumer<Document>) System.out::println);
            MongoBridge.INSTANCE.client.getDatabase("dustrpmongo").getCollection("charsheets").find(new Document("character", new Document("$regex", username)))
                    .forEach((Consumer<Document>) System.out::println);
            //System.out.println(MongoBridge.INSTANCE.sheets.countDocuments());
            final Document sheet = MongoBridge.INSTANCE.sheets.find(Filters.eq("character", username)).first();
            System.out.println("test");
            if (sheet == null) {
                System.out.println("Player entry is not found");
                return;
            }
            System.out.println("Found entry: " + new Gson().toJson(sheet));
            Document stats = sheet.get("stats", Document.class);

            player.clearSkills();
            player.clearTraits();
            for (String key : stats.keySet()) {
                System.out.println(key);
                Document percentages = sheet.get("percentages", Document.class);
                System.out.println("test");
                player.addSkill(
                        key.toLowerCase().replace("strength", "сила").replace("dexterity", "ловкость").replace("psyche", "психика").replace("luck", "удача"),
                        stats.getInteger(key),
                        SkillType.ATTRIBUTE,
                        "",
                        percentages.getInteger(key)
                );
                System.out.println(key + " " + stats.getInteger(key) + " " + percentages.getInteger(key));
            }

            JSONArray skillSet;
            JSONParser jsonParser = new JSONParser();

            skillSet = (JSONArray) jsonParser.parse(new Gson().toJson(sheet.get("skills")));
            System.out.println(skillSet);
            for (Object skill : skillSet) {
                if (skill == null) continue;
                JSONObject skillJs = (JSONObject) skill;
                System.out.println(((JSONObject) skill).get("name").toString().toLowerCase() + " " + ((Long) ((JSONObject) skill).get("level")).intValue() + " " + ((JSONObject) skill).get("gmskillinfo").toString().toLowerCase() + " " + ((Long) ((JSONObject) skill).get("skill_percentage")).intValue());
                player.addSkill(
                        ((JSONObject) skill).get("name").toString().toLowerCase(),
                        ((Long) ((JSONObject) skill).get("level")).intValue(),
                        SkillType.SIMPLE,
                        ((JSONObject) skill).get("gmskillinfo").toString().toLowerCase(),
                        ((Long) ((JSONObject) skill).get("skill_percentage")).intValue()
                );
            }

            JSONArray talentSet;
            talentSet = (JSONArray) jsonParser.parse(new Gson().toJson(sheet.get("talents")));
            for (Object talent : talentSet) {
                if (talent == null) continue;
                JSONObject talentJs = (JSONObject) talent;
                System.out.println(((JSONObject) talent).get("name").toString().toLowerCase() + " " + ((Long) ((JSONObject) talent).get("value")).intValue());
                String name = ((JSONObject) talent).get("name").toString().toLowerCase();
                int value = ((Long) ((JSONObject) talent).get("value")).intValue();
                if (name.contains(" • ")) name = name.split(" • ")[0];
                player.addTrait(name, value);
//                player.addSkill(
//                        ((JSONObject) skill).get("name").toString().toLowerCase(),
//                        ((Long) ((JSONObject) skill).get("level")).intValue(),
//                        SkillType.SIMPLE,
//                        ((JSONObject) skill).get("gmskillinfo").toString().toLowerCase(),
//                        ((Long) ((JSONObject) skill).get("skill_percentage")).intValue()
//                );
            }

            for (Trait trait : Trait.values()) {
                System.out.println(trait.toString() + " " + player.getTraitLevel(trait));
            }
//            List<Document> skills = sheet.getList("skills", Document.class);
//            System.out.println("test");
//            for (Document skill : Objects.requireNonNull(skills)) {
//                if (skill == null) continue;
//                player.addSkill(
//                        skill.getString("name").toLowerCase(),
//                        skill.getInteger("level"),
//                        SkillType.SIMPLE,
//                        skill.getString("gmskillinfo"),
//                        skill.getInteger("skill_percentage")
//                );
//                System.out.println(skill.getString("name").toLowerCase() + " " + skill.getInteger("level") + " " + skill.getString("gmskillinfo") + " " + skill.getInteger("skill_percentage"));
//            }

            System.out.println((Integer) sheet.getOrDefault("size", 5) + " TEST TEST");
            player.setSize((Integer) sheet.getOrDefault("size", 5));


        } catch (Exception e) {
            System.out.println("Failed to get info from DB " + e);
        }
    }

    public void updatePlayerNewSkills(String playerName) {
        if (isNpc(playerName)) {
            mongoUpdateSkills(getPlayer(playerName), getNpc(playerName).getBaseName());
        } else {
            mongoUpdateSkills(getPlayer(playerName), playerName);
        }
//        String actualPath = skillPath;
//        String skillsetName = playerName;
//        if (isNpc(playerName)) {
//            actualPath = npcTemplatesPath;
//            skillsetName = DataHolder.inst().getNpc(playerName).getBaseName();
//            System.out.println("!!!:" + playerName);
//        }
//        Player player = getPlayer(playerName);
//
//        JSONObject skillSet;
//        //skillPath = "/srv/www/main_site/data/newskillsets/";
//        String path = actualPath + skillsetName + ".skills";
//        System.out.println("path: " + path);
//        try {
//            skillSet = Helpers.readJson(path);
//        } catch (IOException | ParseException e) {
//            e.printStackTrace();
//            return;
//        }
//
//        if (!isNpc(playerName)) {
//            try {
//                IData data = NpcAPI.Instance().getIWorld(ServerProxy.getForgePlayer(playerName).dimension).getTempdata();
//                data.put("skills-" + playerName, skillSet.toJSONString().toLowerCase());
//            } catch (Exception ignored) {
//
//            }
//        }
//        System.out.println(skillSet);
//        if (player != null) player.clearTraits();
//        for (Object entry : skillSet.entrySet()) {
////            System.out.println(entry);
//            Map.Entry skill = (Map.Entry) entry;
//            JSONObject value = (JSONObject) skill.getValue();
//            System.out.println(value);
//            int mod = 0;
//
//            String group = (String) value.get("group");
//            if (group.equals("creative")) continue;
//            if (group.equals("trait")) {
//                switch (((String) skill.getKey()).toLowerCase()) {
//                    case "нечувствительность": //чек
//                        player.setInsensitive(true);
//                        break;
//                    case "сосредоточенность": //чек
//                        player.setCollected(true);
//                        break;
//                    case "стойкость": //чек
//                        player.setPersistent(true);
//                        break;
//                    case "податливость": //чек
//                        player.setIrresolute(true);
//                        break;
//                    case "юркость": //чек
//                        player.setNimble(true);
//                        break;
//                    case "нерасторопность": //чек
//                        player.setSluggish(true);
//                        break;
//                    case "хрупкость": //чек
//                        player.setFragile(true);
//                        break;
//                    case "точность": //чек
//                        player.setAccurate(true);
//                        break;
//                    case "быстрота": //чек
//                        player.setMovementTrait(MovementTrait.FAST);
//                        break;
//                    case "сверхбыстрота": //чек
//                        player.setMovementTrait(MovementTrait.SUPERFAST);
//                        break;
//
//                    case "толстокожесть": //чек
//                        player.setHardened(1);
//                        break;
//                    case "толстокожесть+": //чек
//                        player.setHardened(2);
//                        break;
//                    case "толстокожесть++": //чек
//                        player.setHardened(3);
//                        break;
//
//                    case "невосприимчивость к натиску": //чек
//                        player.setCantBeCharged(true);
//                        break;
//                    case "невосприимчивость к кровотечению": //чек
//                        player.setCantBleed(true);
//                        break;
//                    case "невосприимчивость к оглушению": //чек
//                        player.setCantBeStunned(true);
//                        break;
//                    case "невосприимчивость к повреждению конечностей": //чек
//                        player.setCantGetLimbInjury(true);
//                        break;
//                    case "невосприимчивость к горению": //чек
//                        player.setCantBurn(true);
//                        break;
//                    case "невосприимчивость к ментальным ранам": //чек
//                        player.setApatic(true);
//                        break;
//
//                    case "крохотность": //чек
//                        player.setSmall(true);
//                        break;
//                    case "гигантизм": //чек
//                        player.setBig(true);
//                        break;
//
//                    case "сопротивляемость к дробящему": //чек
//                        player.setCrushingRes(1);
//                        break;
//                    case "уязвимость к дробящему": //чек
//                        player.setCrushingRes(-1);
//                        break;
//                    case "сопротивляемость к колющему": //чек
//                        player.setPiercingRes(1);
//                        break;
//                    case "уязвимость к колющему": //чек
//                        player.setPiercingRes(-1);
//                        break;
//                    case "сопротивляемость к режуще-рубящему": //чек
//                        player.setSlashingRes(1);
//                        break;
//                    case "уязвимость к режуще-рубящему": //чек
//                        player.setSlashingRes(-1);
//                        break;
//                    case "сопротивляемость к современному огнестрельному": //чек
//                        player.setModernFirearmRes(1);
//                        break;
//                    case "уязвимость к современному огнестрельному": //чек
//                        player.setModernFirearmRes(-1);
//                        break;
//                    case "сопротивляемость к устаревшему дальнему": //чек
//                        player.setRangedOldRes(1);
//                        break;
//                    case "уязвимость к устаревшему дальнему": //чек
//                        player.setRangedOldRes(-1);
//                        break;
//                    case "сопротивляемость к ближним атакам": //чек
//                        player.setMeleeRes(1);
//                        break;
//                    case "уязвимость к ближним атакам": //чек
//                        player.setMeleeRes(-1);
//                        break;
//                    case "сопротивляемость к дальним атакам": //чек
//                        player.setRangedRes(1);
//                        break;
//                    case "уязвимость к дальним атакам": //чек
//                        player.setRangedRes(-1);
//                        break;
//                }
//                continue;
//            }
//
////            try {
////                mod = Integer.parseInt((String) value.get("mod"));
////            } catch (ClassCastException e) {
////                System.out.println("mod was not a String; trying Long");
////                mod = Math.toIntExact((Long) value.get("mod"));
////            }
//
//            try {
//                mod = Integer.parseInt((String) value.get("mod"));
//            } catch (ClassCastException e) {
//                System.out.println("mod was not a String; trying Long");
//                mod = Math.toIntExact((Long) value.get("mod"));
//            }
//
//            player.addSkill(
//                    ((String) skill.getKey()).toLowerCase(),
//                    Integer.parseInt((String) value.get("level")),
//                    (group.equals("attribute") ? SkillType.ATTRIBUTE : SkillType.SIMPLE),
//                    (String) value.get("attribute"),
//                    mod
//            );
//
//        }
//
//        for (String skillName : SkillsHelper.getEssentials()) {
//            if (!player.getSkills().contains(new Skill(skillName, 0))) {
//                player.addSkill(skillName, 0);
//            }
//        }
//
//        if (player.getSkill("акробатика") != null) {
//            player.removeSkill("акробатика");
//        }
//        if (player.getSkill("внимательность") != null) {
//            player.removeSkill("внимательность");
//        }


    }


    private NPC getNpc(String playerName) {
        for (NPC npc : npcList) {
            if (npc.getName().equals(playerName)) return npc;
        }
        return null;
    }

    @Deprecated
    public void updatePlayerSkills(String playerName) {
        if (getPlayer(playerName) instanceof NPC) {
            try {
                ((NPC) getPlayer(playerName)).updateSet();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }
        logger.info("Fetching skills for " + playerName);
        //fetch skills from web-site's data
        Pattern skillpat = Pattern.compile("(.*):");
        Pattern levelpat = Pattern.compile(":(.*)");

        Player player = getPlayer(playerName);
        if (player == null) throw new DataException("1", "Игрок " + playerName + " не найден!");

        player.clearSkills();
        Path path = Paths.get(skillPath, playerName + ".skills");
        Charset charset = Charset.forName("UTF-8");
        String skill = null;
        String level = null;

        //parse skills from file
        try {
            List<String> lines = Files.readAllLines(path, charset);
            for (String line : lines) {
                Matcher skillmat = skillpat.matcher(line);
                while (skillmat.find()) {
                    skill = skillmat.group().replaceAll(":", ""); //get the name of the skill
                }
                Matcher levelmat = levelpat.matcher(line);
                while (levelmat.find()) {
                    level = levelmat.group().replace(":", ""); //get the level of the skill
                }
                int levelInt = DataHolder.inst().getDiceAsInteger(level);

                SkillType type = SkillType.getType(skill);
                player.addSkill(skill, levelInt, type, "", 0);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            //throw new DataException("Fetching skills:", e.getMessage());
        }

        //fill in all the essential skills that are missing
        for (String skillName : SkillsHelper.getEssentials()) {
            if (!player.getSkills().contains(new Skill(skillName, 0))) {
                player.addSkill(skillName, 0);
            }
        }
    }

    public Shield updatePlayerShield(String playerName) {
        logger.info("Fetching shield for " + playerName);

        Player player = getPlayer(playerName);

        if (player == null) throw new DataException("1", "Игрок не найден!");


        player.setShield(null);
        try {
            EntityLivingBase forgePlayer = ServerProxy.getForgePlayer(player.getName());
            WeaponTagsHandler leftHand = new WeaponTagsHandler(forgePlayer.getHeldItemOffhand());
            if (leftHand.isShield()) {
                System.out.println("Shield in offhand");
                Shield shield = new Shield("щит", leftHand.getDefaultWeapon());
//                shield.fillFromNBT(leftHand.getDefaultWeapon());
                player.setShield(shield);
                return shield;
            } else {
                WeaponTagsHandler rightHand = new WeaponTagsHandler(forgePlayer.getHeldItemMainhand());
                if (rightHand.isShield()) {
                    System.out.println("Shield in mainhand");
                    Shield shield = new Shield("щит", rightHand.getDefaultWeapon());
                    //shield.fillFromNBT(rightHand.getDefaultWeapon());
                    player.setShield(shield);
                    return shield;
                }
            }
        } catch (NullPointerException ignored) {
        }

        System.out.println("No shield was found.");
        return null;
    }

    public void updatePlayerArmor(String playerName) {
        logger.info("Fetching armor for " + playerName);

        Player player = getPlayer(playerName);

        if (player == null) throw new DataException("1", "Игрок не найден!");

        //fetch armor from NPC's config-file
        /*
        if (player instanceof NPC) {
            try {
                ((NPC) player).updateArmor();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }

         */

        player.purgeArmor();
        try {
            EntityLivingBase forgePlayer = ServerProxy.getForgePlayer(player.getName());
            if (isNpc(playerName)) forgePlayer = getNpcEntity(playerName);
            System.out.println("forgePlayer is " + forgePlayer.getName());

            for (ItemStack armorPiece : forgePlayer.getArmorInventoryList()) {
                if (armorPiece.isEmpty()) continue;
                WeaponTagsHandler armorHandler = new WeaponTagsHandler(armorPiece);
                if (armorHandler.isArmor()) {
                    System.out.println("armor is " + armorHandler.getDefaultWeapon());
                    player.addToArmor(armorHandler.getDefaultWeapon());
                    return;
                }
            }
        } catch (NullPointerException ignored) {
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        //int armorMod;
        //armorMod = modifiers.getArmorMod(playerName);
        //player.setArmorMod(armorMod);
        //int defenseMod;
        //defenseMod = modifiers.getDefenseMod(playerName);
        //player.setDefenseMod(defenseMod);

    }

    public void updatePlayerWounds(String playerName) {
        if (getCombatForPlayer(playerName) != null) {
            System.out.println(playerName + " is in combat, NOT canceling wounds update");
            //return;
        }
        logger.debug("Updating player wounds in " + this.getClass().getSimpleName());
        Player player = getPlayer(playerName);
        if (player instanceof NPC) {
            return;
        }

        if (player == null) throw new DataException("1", playerName + " not found");
        /*
        if (player instanceof NPC) {
            try {
                ((NPC) player).updateSet();
            } catch (IOException | ParseException e) {
                e.printStackTrace();
            }
            return;
        }
         */
        updateWoundPyramid(playerName);
        player.updateWounds(modifiers.getWounds(playerName));
        player.setBloodloss(modifiers.getBloodloss(playerName));
        player.setMentalDebuff(modifiers.getMentalDebuff(playerName));
        if (!isNpc(playerName)) {
            try {
                IData data = NpcAPI.Instance().getIWorld(ServerProxy.getForgePlayer(playerName).dimension).getTempdata();
                data.put("wounds-" + playerName, player.getWoundPyramid().getWoundsMod());
            } catch (Exception ignored) {

            }
        }
//        player.setWoundMod(modifiers.getWoundsMod(playerName));
        logger.debug("Player " + playerName + " was updated with wounds: " + player.getWoundPyramid());
    }

    public void updatePlayerCourage(String playerName) {
        try {
            if (getCombatForPlayer(playerName) != null) {
                System.out.println(playerName + " is in combat, NOT canceling courage update");
                //return;
            }
            logger.debug("Updating player courage in " + this.getClass().getSimpleName());
            Player player = getPlayer(playerName);
            if (player instanceof NPC) {
                return;
            }

            if (player == null) throw new DataException("1", playerName + " not found");
        /*
        if (player instanceof NPC) {
            try {
                ((NPC) player).updateSet();
            } catch (IOException | ParseException e) {
                e.printStackTrace();
            }
            return;
        }
         */

            int[] courage = modifiers.getCourage(playerName);
            if (courage[1] == 0) {
                player.courage = player.getSkill("психика").getLevel() + player.getSkill("удача").getLevel();
                player.maxcourage = player.courage;
                modifiers.updateCourage(playerName, player.courage, player.maxcourage);
            } else {
                player.courage = courage[0];
                player.maxcourage = courage[1];
            }

            if (player.courage < player.maxcourage) {
                IData data = NpcAPI.Instance().getIWorld(0).getStoreddata();

                if (data.has("courageGiven")) {
                    String line = (String) data.get("courageGiven");
                    if (!line.contains("[" + playerName + "]")) {
                        line += (" [" + playerName + "]");
                        data.put("courageGiven", line);
                        player.courage++;
                        modifiers.updateCourage(playerName, player.courage, player.maxcourage);
                    }
                }
            }

//        if (!isNpc(playerName)) {
//            try {
//                IData data = NpcAPI.Instance().getIWorld(ServerProxy.getForgePlayer(playerName).dimension).getTempdata();
//                data.put("wounds-" + playerName, player.getWoundPyramid().getWoundsMod());
//            } catch (Exception ignored) {
//
//            }
//        }
//        player.setWoundMod(modifiers.getWoundsMod(playerName));
            logger.debug("Player " + playerName + " was updated with courage: " + player.courage + " " + player.maxcourage);
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }


    @Deprecated
    public void updatePlayerWeapon(String playerName) {
        logger.info("Fetching weapon for " + playerName);
        Player fighter = getPlayer(playerName);
        if (fighter == null) throw new DataException("1", "Боец не найден!");
    }

    @Deprecated
    public void updateFighter(String fighterName) {
        updatePlayer(fighterName);
        updatePlayerWeapon(fighterName);
    }

    public void updatePlayer(String playerName) {
        updatePlayerNewSkills(playerName);
        //updateWoundPyramid(playerName);
        updatePlayerArmor(playerName);
        updatePlayerShield(playerName);
        updatePlayerWounds(playerName);
        updateMaxWeight(playerName);
        updatePlayerCourage(playerName);
    }

    public void updateWoundPyramid(String playerName) {
        Player player = getPlayer(playerName);
        int endurance;
        if (player.getSkill("выносливость") == null) {
            endurance = 1;
        } else {
            endurance = player.getSkill("выносливость").getLevel();
        }

        player.setWoundPyramind(WoundPyramid.createFromEndurance(endurance, player.getSize()));
    }


    //~~~~~~~~~~~~~~~Other stuff

    public HashMap<Integer, String> getSkillTable() {
        return skillTable;
    }

    public int getDiceAsInteger(String dice) {
        for (Map.Entry<Integer, String> entry : skillTable.entrySet()) {
            if (entry.getValue().equals(dice)) return entry.getKey();
        }
        return 0;
    }

    public List<Reminder> getReminders() {
        return reminders;
    }

    public void removeReminder(Reminder reminder) {
        reminders.remove(reminder);
    }

    @Deprecated
    public Combat getCombat(String combatName) {
        for (Combat combat : combatList) {
            if (combat.getName().equals(combatName)) {
                return combat;
            }
        }
        return null;
    }

    public void removeCombat(int id) {
        combatList.remove(getCombat(id));
    }

    public void initTest() {
        if (getPlayer("Tester") == null)
            addPlayer(new Player("Tester"));
        if (getPlayer("Tester2") == null)
            addPlayer(new Player("Tester2"));
    }

    public NPC createNPC(String playerName) {
        return createNPC(playerName, playerName);
    }

    public NPC createNPC(String playerName, String baseName) {
        if (getPlayer(playerName) != null) {
            throw new DataException("1", "Имя " + playerName + " уже занято либо игроком, либо другим NPC!");
        }
        NPC npc = new NPC(playerName, baseName);
        npcList.add(npc);
        logger.debug("NPC " + npc.getName() + " created and added to DataHolder with baseName " + baseName + "!");

        return npc;
    }

    public void registerNpc(String name, EntityNPCInterface entity) {
        if (npcsMap.containsKey(name)) {
            throw new DataException("НПС с именем " + name + " уже существует в системе!");
        }
        npcsMap.put(name, entity);
        System.out.println("Registered " + name + " with entity " + entity.getName());
    }

    public boolean npcExist(String name) {
        return npcsMap.containsKey(name);
    }

    public void forceRegisterNpc(String name, EntityNPCInterface entity) {
        if (npcsMap.containsKey(name)) {
            npcsMap.put(name, entity);
            System.out.println("Reregistered " + name + " with entity " + entity.getName());
        }
    }

    public void restartDiscordBot() {
        DiscordBridge.close();
        try {
            DiscordBridge.create(
                    config.readConfig("bot_token"),
                    config.readConfig("channel_id")
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public Modifiers getModifiers() {
        return modifiers;
    }

    public MovementHelper getMovementHelper() {
        return movementHelper;
    }

    public List<NPC> getNpcList() {
        return npcList;
    }

    // ~~~~~~~~~~ DUELS

    public void registerDuel(Duel duel) {
        duels.add(duel);
    }

    public void removeDuel(Duel duel) {
        duels.remove(duel);
    }

    public List<Duel> getDuels() {
        return duels;
    }

    public EntityNPCInterface getNpcEntity(String name) {
        //System.out.println("Searching for " + name + " among NPC's entities.");
        //if (npcsMap.containsKey(name)) {
        //System.out.println("Found it!");
        //}
        EntityNPCInterface elb = npcsMap.get(name);
        if (elb != null) {
//            System.out.println(elb.wrappedNPC);
            IWorld iWorld = NpcAPI.Instance().getIWorld(elb.dimension);
//            System.out.println(iWorld);
            IEntity[] list = iWorld.getAllEntities(EntityType.NPC);
            for (IEntity iEntity : list) {
//                System.out.println(iEntity.getName());
                if (iEntity.getName().equals(name)) return (EntityNPCInterface) iEntity.getMCEntity();
            }
        }
        return npcsMap.get(name);
    }

    public void spawnBoomNpc(String name, EntityPlayer creator, BlockPos blockPos, int damage, String type) {
        IWorld iWorld = NpcAPI.Instance().getIWorld(creator.dimension);
        IEntity[] list = iWorld.getAllEntities(EntityType.NPC);
        int i = 0;
        for (IEntity iEntity : list) {
            if (iEntity.getName().equals(name)) i++;
            if (i > 1) {
                TextComponentString str = new TextComponentString("На сервере больше одного НПЦ с именем " + name + "!");
                creator.sendMessage(str);
                return;
            }
        }

        NPCWrapper npc = (NPCWrapper) NpcAPI.Instance().getClones().spawn(blockPos.getX() + 0.5, blockPos.getY(), blockPos.getZ() + 0.5, 9, "[ГМ] Взрыв", iWorld);
        npc.getDisplay().setName(name);
        npc.getDisplay().setTitle(type + " [" + String.valueOf(damage) + "]");
    }

    public EntityNPCInterface getNpcBoom(String name, int dimension) {
        //System.out.println("Searching for " + name + " among NPC's entities.");
        //if (npcsMap.containsKey(name)) {
        //System.out.println("Found it!");
        //}
//            System.out.println(elb.wrappedNPC);
            IWorld iWorld = NpcAPI.Instance().getIWorld(dimension);
//            System.out.println(iWorld);
            IEntity[] list = iWorld.getAllEntities(EntityType.NPC);
            for (IEntity iEntity : list) {
//                System.out.println(iEntity.getName());
                if (iEntity.getName().equals(name)) return (EntityNPCInterface) iEntity.getMCEntity();
            }
        return null;
    }

    public boolean isNpc(String name) {
        for (NPC npc : getNpcList()) {
            if (npc.getName().equals(name))
                return true;
        }
        return false;
    }

    public Player getMasterForSubordinate(String subName) {
        for (Player pl : getPlayerList()) {
            if (pl.getSubordinate() == null) continue;
            if (pl.getSubordinate().getName().equals(subName)) return pl;
        }
        return null;
    }

    public Player getMasterForNpcInCombat(Player player) {
        if (!isNpc(player.getName())) {
            System.out.println("player " + player.getName() + " is no npc, returning null");
            return null;
        }
        Combat combat = getCombatForPlayer(player.getName());
        if (combat == null) {
            System.out.println("combat is null, returning null");
            return null;
        }

        return getMasterForCombat(combat.getId());
    }

    public Player getMasterForPlayerInCombat(Player player) {
        Combat combat = getCombatForPlayer(player.getName());
        if (combat == null) {
            System.out.println("combat is null, returning null");
            return null;
        }

        return getMasterForCombat(combat.getId());
    }

    public List<Player> getPlayerAndNpcList() {
        List<Player> result = new ArrayList<>();
        result.addAll(getPlayerList());
        result.addAll(getNpcList());
        return result;
    }


    public void resetWounds(Player player) {
        player.getWoundPyramid().reset();
    }

    public void removeNpc(String customName) {
        NPC npc = (NPC) getPlayer(customName);
        Combat combat = getCombatForPlayer(customName);
        if (combat != null) {
            combat.removeFighter(customName);
        }
        System.out.println(npcsMap);
        npcList.remove(npc);
        npcsMap.remove(customName);
        removePlayer(customName);
        System.out.println(npcsMap);
    }

    public boolean eligibleForCounter(Player defender) {
        Combat combat = DataHolder.inst().getCombatForPlayer(defender.getName());
        try {
            System.out.println("1");
            if (combat == null) return false;
            System.out.println("2");
            if (combat.getReactionList() == null) return false;
            System.out.println("3");
            if (defender.hasAttacked()) return false;
            System.out.println("4 " + combat.getReactionList().getQueue().getResultFor(defender.getName()));
            if (combat.getReactionList().getQueue().getResultFor(defender.getName()) < 2) return false; //TODO: поменять потом
            System.out.println("5");
            if (defender.getCombatState() == CombatState.ACTED) return false;
            System.out.println("6");
            Duel duel = DataHolder.inst().getDuelForDefender(defender.getName());
            if (duel.getAttacks().size() > 1) return false;
            if (defender.hasStatusEffect(StatusType.STUNNED) && !defender.cantBeStunned()) return false;
            System.out.println("7");
        } catch (NullPointerException e) {
            System.out.println("8");
            return false;
        }

//        try {
//            if (duel.
//                    getAttacker().
//                    getModifiersState().getModifier("serial") != 0)
//                return false;
//        } catch (NullPointerException ignored) {
//        }
        System.out.println("TRUE");
        return true;


    }

    public List<Player> getMasters() {
        List<Player> result = new ArrayList<>();
        for (Player p : getPlayerList()) {
            if (p.hasPermission(Permission.GM)) {
                result.add(p);
            }
        }
        return result;
    }

    public void informMasterForCombat(String fighterName, TextComponentString mesToGm) {
        System.out.println("informing masters for combat " + fighterName);
        Player pl = getPlayer(fighterName);
        if (pl == null) return;
        System.out.println("player not null passed");
        Combat c = getCombatForPlayer(fighterName);
        if (c == null) return;
        System.out.println("checks passed");
        for (Player master : getMasters()) {
            System.out.println("master: " + master.getName());
            if (master.getAttachedCombatID() == c.getId()) {
                EntityPlayerMP forgePlayer = ServerProxy.getForgePlayer(master.getName());
                System.out.println("forge player is " + forgePlayer);
                if (forgePlayer != null) {
                    forgePlayer.sendMessage(mesToGm);
                }
            }
        }

    }

    public double getMana(String player) {
        try {
            IData data = NpcAPI.Instance().getIWorld(0).getStoreddata();
            if (data.has("mana-" + player)) {
                return (double) data.get("mana-" + player);
            } else {
                return 0;
            }
        } catch (Exception ignored) {
            System.out.println(ignored);
            return -666;
        }
    }

    public double getMaxMana(String player) {
        try {
            IData data = NpcAPI.Instance().getIWorld(0).getStoreddata();

            if (data.has("maxmana-" + player)) {
                return (double) data.get("maxmana-" + player);
            } else {
                return 0;
            }
        } catch (Exception ignored) {
            System.out.println(ignored);
            return -666;
        }
    }

    public boolean hasMana(String player) {
        try {
            IData data = NpcAPI.Instance().getIWorld(0).getStoreddata();
            return data.has("mana-" + player);
        } catch (Exception ignored) {
            System.out.println(ignored);
            return false;
        }
    }

    public boolean hasMana(String player, int needed) {
        try {
            IData data = NpcAPI.Instance().getIWorld(0).getStoreddata();
            if (data.has("mana-" + player)) {
                double mana = (double) data.get("mana-" + player);
                return mana >= needed;
            } else {
                return false;
            }
        } catch (Exception ignored) {
            System.out.println(ignored);
            return false;
        }
    }

    public boolean useMana(String player, int used) {
        try {
            IData data = NpcAPI.Instance().getIWorld(0).getStoreddata();
            if (data.has("mana-" + player)) {
                double mana = (double) data.get("mana-" + player);
                if (mana >= used) {
                    mana = mana - used;
                    data.put("mana-" + player, mana);
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception ignored) {
            System.out.println(ignored);
            return false;
        }
    }

    public boolean addMana(String player, int added) {
        try {
            IData data = NpcAPI.Instance().getIWorld(0).getStoreddata();
            if (data.has("mana-" + player)) {
                double mana = (double) data.get("mana-" + player);
                mana = Math.min(getMaxMana(player), mana + added);
                data.put("mana-" + player, mana);
                return true;
            } else {
                return false;
            }
        } catch (Exception ignored) {
            System.out.println(ignored);
            return false;
        }
    }

    public boolean setMaxMana(String player, int max) {
        try {
            IData data = NpcAPI.Instance().getIWorld(0).getStoreddata();
            if (!data.has("maxmana-" + player) || !data.has("mana-" + player)) {
                data.put("mana-" + player, max);
            }
            data.put("maxmana-" + player, max);
            return true;
        } catch (Exception ignored) {
            return false;
        }
    }

    public void updateMaxWeight(String playerName) {
        Player player = DataHolder.inst().getPlayer(playerName);
        if (player.getSkill("сила") == null) return;
        int strength = player.getSkillLevelAsInt("сила");
        float percentStrength = player.getSkill("сила").getPercent();
        int maxWeight = (int) (150 + 100 * strength + Math.floor(100 * (percentStrength / 100)));
        //player.countWeight();
        player.setMaxWeight(maxWeight);
        int psy = player.getSkillLevelAsInt("психика");
        float percentPsyStrength = player.getSkill("психика").getPercent();
        player.setMaxPsyWeight((int) (10 + 10 * psy + Math.floor(10 * (percentPsyStrength / 100))));
        player.setOptimalWeight(0);
    }

    public boolean areSkillsComparable(FudgeDiceMessage playerDice, FudgeDiceMessage opponentDice) {
        if (playerDice.getDice().getSkillName().isEmpty() || opponentDice.getDice().getSkillName().isEmpty()) return false;
        int playerLevel = DataHolder.inst().getPlayer(playerDice.getPlayerName()).getSkillLevelAsInt(playerDice.getDice().getSkillName()) * 100;
        for (Modificator mod : playerDice.getDice().mods.getMods()) {
            if (mod.getMod() > 0) playerLevel += mod.getMod() * 100;
        }
        PercentDice pd = playerDice.getDice().getPercentDice();
        playerLevel += pd.getSkill();
        if (pd.getAmmo() > 0) playerLevel += pd.getAmmo();
        if (pd.getBloodloss() > 0) playerLevel += pd.getBloodloss();
        if (pd.getBipod() > 0) playerLevel += pd.getBipod();
        if (pd.getBuff() > 0) playerLevel += pd.getBuff();
        if (pd.getCustom() > 0) playerLevel += pd.getCustom();
        if (pd.getFatigue() > 0) playerLevel += pd.getFatigue();
        if (pd.getLimbInjury() > 0) playerLevel += pd.getLimbInjury();
        if (pd.getMentalDebuff() > 0) playerLevel += pd.getMentalDebuff();
        if (pd.getModules() > 0) playerLevel += pd.getModules();
        if (pd.getRipost() > 0) playerLevel += pd.getRipost();
        if (pd.getStock() > 0) playerLevel += pd.getStock();
        if (pd.getWeakened() > 0) playerLevel += pd.getWeakened();
        if (pd.getWound() > 0) playerLevel += pd.getWound();
        if (pd.getWeightDebuff() > 0) playerLevel += pd.getWeightDebuff();

        int opponentLevel = DataHolder.inst().getPlayer(opponentDice.getPlayerName()).getSkillLevelAsInt(opponentDice.getDice().getSkillName()) * 100;
        for (Modificator mod : opponentDice.getDice().mods.getMods()) {
            if (mod.getMod() < 0) opponentLevel += mod.getMod() * 100;
        }
        PercentDice od = opponentDice.getDice().getPercentDice();
        opponentLevel += od.getSkill();
        if (od.getAmmo() < 0) opponentLevel += od.getAmmo();
        if (od.getBloodloss() < 0) opponentLevel += od.getBloodloss();
        if (od.getBipod() < 0) opponentLevel += od.getBipod();
        if (od.getBuff() < 0) opponentLevel += od.getBuff();
        if (od.getCustom() < 0) opponentLevel += od.getCustom();
        if (od.getFatigue() < 0) opponentLevel += od.getFatigue();
        if (od.getLimbInjury() < 0) opponentLevel += od.getLimbInjury();
        if (od.getMentalDebuff() < 0) opponentLevel += od.getMentalDebuff();
        if (od.getModules() < 0) opponentLevel += od.getModules();
        if (od.getRipost() < 0) opponentLevel += od.getRipost();
        if (od.getStock() < 0) opponentLevel += od.getStock();
        if (od.getWeakened() < 0) opponentLevel += od.getWeakened();
        if (od.getWound() < 0) opponentLevel += od.getWound();
        if (od.getWeightDebuff() < 0) opponentLevel += od.getWeightDebuff();

        System.out.println("p " + playerLevel + " o " + opponentLevel);
        return (playerLevel <= (opponentLevel + 30));
    }

    public static MutablePair<Integer, String> rollMegaDice(String input, String type) throws ScriptException {
            ScriptEngineManager manager = new ScriptEngineManager();
            ScriptEngine engine = manager.getEngineByName("JavaScript");
            String meganumber = "";
            String counting = "";
            List<Integer> diceAmount = new ArrayList<>();
            Random random = new Random();
            Pattern linepattern = Pattern.compile("(\\d+d\\d+|d\\w+|\\d+|[+]|[()]|[-]+|cos|tg|log|sin|[*]|[/])");
            Pattern anypattern = Pattern.compile("(\\d+d\\d+|d\\w+|\\d+|[+]|[()]|[-]+|cos[(]\\d+[)]|tg[(]\\d+[)]|log[(]\\d+[)]|sin[(]\\d+[)]|[*]|[/])");
            Pattern rollpattern1 = Pattern.compile("(\\d+d\\d+)");
            Pattern rollpattern2 = Pattern.compile("d\\d+");
            Pattern symbolpattern = Pattern.compile("(\\d+|[+]|[()]|[-]|cos|tg|log|sin|[*]|[/])");
            Matcher m = linepattern.matcher(input);
            String init = input;
            String megastring = "";
            while (m.find()){
                Matcher m1 = rollpattern1.matcher(m.group());
                if (m1.matches()){
                    diceAmount.clear();
                    String[] justDice = m.group().split("d");
                    int arg1 = Integer.parseInt(justDice[0]);
                    int arg2 = Integer.parseInt(justDice[1]);
                    for (int i = 0; i < arg1; i++) {
                        diceAmount.add(random.nextInt(arg2)+1);
                    }
                    meganumber += diceAmount.stream().mapToInt(Integer::intValue).sum();
                    System.out.println(diceAmount);
                }
                Matcher m2 = rollpattern2.matcher(m.group());
                if (m2.matches()){
                    int arg = Integer.parseInt(m.group().replace("d",""));
                    arg = random.nextInt(arg)+1;
                    meganumber += arg;
                }
                Matcher m3 = symbolpattern.matcher(m.group());
                if (m3.matches()){
                    meganumber += m3.group();
                }
            }
            System.out.println(input);
            System.out.println(meganumber);
            if (!input.equals(meganumber)) {
                megastring += ("\n\n" + type + ":\n" + input + "\n" + meganumber);
                init = meganumber;
            }
            Pattern cos = Pattern.compile("cos[(]\\d+[)]");
            Pattern sin = Pattern.compile("sin[(]\\d+[)]");
            Pattern tg = Pattern.compile("tg[(]\\d+[)]");
            Pattern log = Pattern.compile("log[(]\\d+[)]");
            Matcher mm = anypattern.matcher(meganumber);
            while (mm.find()){
                Matcher mm1 = cos.matcher(mm.group());
                if (mm1.matches()){
                    counting = mm1.group().replaceAll("cos[(]","");
                    counting = counting.replaceAll("[)]","");
                    meganumber = meganumber.replace(mm1.group(),String.valueOf(Math.cos(Double.valueOf(counting))));
                }
                Matcher mm2 = sin.matcher(mm.group());
                if (mm2.matches()){
                    counting = mm2.group().replaceAll("sin[(]","");
                    counting = counting.replaceAll("[)]","");
                    meganumber = meganumber.replace(mm2.group(),String.valueOf(Math.cos(Double.valueOf(counting))));
                }
                Matcher mm3 = tg.matcher(mm.group());
                if (mm3.matches()){
                    counting = mm3.group().replaceAll("tg[(]","");
                    counting = counting.replaceAll("[)]","");
                    meganumber = meganumber.replace(mm3.group(),String.valueOf(Math.tan(Double.valueOf(counting))));
                }
                Matcher mm4 = log.matcher(mm.group());
                if (mm4.matches()){
                    counting = mm4.group().replaceAll("log[(]","");
                    counting = counting.replaceAll("[)]","");
                    meganumber = meganumber.replace(mm4.group(),String.valueOf(Math.log(Double.valueOf(counting))));
                }
            }
            System.out.println(meganumber);
            if (!meganumber.equals(init)) {
                megastring += ("\n" + meganumber);
                init = meganumber;
            }
            String stringOut = String.valueOf(engine.eval(meganumber));
        System.out.println(meganumber);
        if (!stringOut.equals(init)) {
            megastring += ("\n" + stringOut);
        }
            MutablePair<Integer, String> output = new MutablePair((int) Math.floor((Double.valueOf(stringOut))), megastring);
        return output;
        }

}


