package ru.konungstvo.exceptions;

public class CombatException extends GenericException {
    public CombatException(String errorMessage) {this("CombatException", errorMessage);}
    public CombatException(String errorName, String errorMessage) {
        super(errorName, errorMessage);
    }
}
