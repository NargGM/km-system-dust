package ru.konungstvo.exceptions;

public class PermissionException extends GenericException {
    public PermissionException(String errorMessage) {this("PermissionException", errorMessage);}

    public PermissionException(String errorName, String errorMessage) {
        super(errorName, errorMessage);
    }
}
