package ru.konungstvo.exceptions;

public class RuleException extends GenericException {
    public RuleException(String errorMessage) {this("RuleException", errorMessage);}
    public RuleException(String errorName, String errorMessage) {
        super(errorName, errorMessage);
    }
}
