package ru.konungstvo.exceptions;

public class WoundsException extends GenericException {
    public WoundsException(String errorMessage) {this("WoundsException", errorMessage);}

    public WoundsException(String errorName, String errorMessage) {
        super(errorName, errorMessage);
    }
}
