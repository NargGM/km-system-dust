package ru.konungstvo.exceptions;

public class NetworkException extends GenericException {
    public NetworkException(String errorMessage) {this("NetworkException", errorMessage);}

    public NetworkException(String errorName, String errorMessage) {
        super(errorName, errorMessage);
    }
}
