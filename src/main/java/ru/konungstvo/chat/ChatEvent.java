package ru.konungstvo.chat;

import net.minecraft.util.text.TextComponentString;
import ru.konungstvo.chat.message.RoleplayMessage;
import ru.konungstvo.chat.message.infrastructure.MessageFactory;
import ru.konungstvo.control.Logger;
import ru.konungstvo.player.Player;

public class ChatEvent {
    private Player player;
    private RoleplayMessage roleplayMessage;
    private static Logger logger = new Logger("ChatEventLog");;

    public ChatEvent(Player player, String message) throws Exception {
        this.player = player;
        this.roleplayMessage = MessageFactory.createMessage(player, message.trim());

    }

    // Forge dependent
    public static TextComponentString getPlayerJoin(String playerName) {
        String format = "§e%s§f входит в игру";
        TextComponentString res = new TextComponentString(playerName);
        res.getStyle().setColor(ChatColor.NICK.get());
        res.appendSibling(new TextComponentString(" входит в игру"));
        return res;
    }

    // Forge dependent
    public static TextComponentString getPlayerLeave(String playerName) {
        String format = "§e%s§f выходит из игры";
        TextComponentString res = new TextComponentString(playerName);
        res.getStyle().setColor(ChatColor.NICK.get());
        res.appendSibling(new TextComponentString(" выходит из игры"));
        return res;
    }

    // TextComponentString is forge dependent!
    public RoleplayMessage getResult() {
        if (roleplayMessage == null) return null;
        roleplayMessage.build();
        return roleplayMessage;
    }

    public RoleplayMessage getRoleplayMessage() {
        return roleplayMessage;
    }
}
