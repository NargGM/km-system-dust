package ru.konungstvo.chat.message;

import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.combat.dice.CommonDice;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.Logger;

import java.util.List;

public class CommonDiceMessage extends RoleplayMessage {
    public CommonDiceMessage(String playerName, String content) {
        this(playerName, content, Range.NORMAL_DIE);
    }

    public CommonDiceMessage(String playerName, String content, Range range) {
        super(playerName, content, range);
    }

    @Override
    public List<MessageComponent> buildTemplate(List<MessageComponent> template) {
        Logger logger = new Logger(this.getClass().getName());
        logger.debug("Achieved " + range);
        range = Helpers.convertToDieRange(range);
        logger.debug("Converted to " + range);

        CommonDice commonDice = new CommonDice(getContent());
        if (range.toString().contains("GM")) {

            template.add(new MessageComponent("(( ", ChatColor.DICE));
            template.add(new MessageComponent("{playerName}", ChatColor.NICK));
            template.add(new MessageComponent(" (to GM)"));
            template.add(new MessageComponent(" бросает " + commonDice.getCountAsString() + "d" + commonDice.getMax() + commonDice.getModAsString() +
                    commonDice.getComment() + ". " + commonDice.getResultAsString() + " ))",
                    ChatColor.DICE));

            /*
            template = ChatColor.DICE.get() + "(( " +
                    ChatColor.NICK.get() + "{playerName}" + " §f(to GM)" + ChatColor.DICE.get() + " бросает " +
                    commonDice.getCountAsString() + "d" + commonDice.getMaxAsString() + commonDice.getModAsString() +
                    commonDice.getComment() +
                    ". " + commonDice.getResultAsString() + " ))";

             */
        } else if (range.toString().contains("CM")) {
            template.add(new MessageComponent("(( ", ChatColor.DICE));
            template.add(new MessageComponent("{playerName}", ChatColor.NICK));
            template.add(new MessageComponent(" (to CM)", ChatColor.CMCHAT));
            template.add(new MessageComponent(" бросает " + commonDice.getCountAsString() + "d" + commonDice.getMax() + commonDice.getModAsString() +
                    commonDice.getComment() + ". " + commonDice.getResultAsString() + " ))",
                    ChatColor.DICE));
            /*
            template = ChatColor.DICE.get() + "(( " +
                    ChatColor.NICK.get() + "{playerName}" + ChatColor.BDCHAT.get() + " (to BD) " + ChatColor.DICE.get() + " бросает " +
                    commonDice.getCountAsString() + "d" + commonDice.getMaxAsString() + commonDice.getModAsString() +
                    commonDice.getComment() +
                   ". " + commonDice.getResultAsString() + " ))";

             */
        } else {
            template.add(new MessageComponent("(( ", ChatColor.DICE));
            template.add(new MessageComponent("{playerName}", ChatColor.NICK));
            template.add(new MessageComponent(" {rangeDescription} " + commonDice.getCountAsString() + "d" + commonDice.getMax() + commonDice.getModAsString() +
                    commonDice.getComment() + ". " + commonDice.getResultAsString() + " ))",
                    ChatColor.DICE));
            /*
            template = ChatColor.DICE.get() + "(( " +
                    ChatColor.NICK.get() + "{playerName}" + ChatColor.DICE.get() + " {rangeDescription} " +
                    commonDice.getCountAsString() + "d" + commonDice.getMaxAsString() + commonDice.getModAsString() +
                    commonDice.getComment() +
                    ". " + commonDice.getResultAsString() + " ))";

             */
        }
        this.setRange(range);
        return template;
    }

}
