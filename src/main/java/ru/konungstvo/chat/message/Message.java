package ru.konungstvo.chat.message;

import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;

import java.util.LinkedList;
import java.util.List;

public class Message {
    List<MessageComponent> components;

    public Message() {
        this.components = new LinkedList<>();
    }

    public Message(String content) {
        this(content, ChatColor.DEFAULT);
    }

    public Message(String content, ChatColor color) {
        this.components = new LinkedList<>();
        this.components.add(new MessageComponent(content, color));
    }

    public List<MessageComponent> getComponents() {
        return components;
    }

    public void setComponents(List<MessageComponent> components) {
        this.components = components;
    }

    public void addComponent(Message right) {
        for (MessageComponent component : right.getComponents()) {
            this.addComponent(component);
        }
    }

    public void addComponentLn(Message right) {
        for (MessageComponent component : right.getComponents()) {
            addComponent(component);
        }
        addComponent(new MessageComponent("\n"));
    }

    public void addComponent(MessageComponent component) {
        this.components.add(component);
    }

    public void addComponentLn(MessageComponent component) {
        addComponent(component);
        addComponent(new MessageComponent("\n"));
    }


    @Override
    public String toString() {
        if (getComponents() == null) return "";
        StringBuilder builder = new StringBuilder();
        for (MessageComponent component : getComponents()) {
            builder.append(component.getText());
        }
        return builder.toString();
    }

    public void purge() {
        this.components = new LinkedList<>();
    }

    public void dim() {
        for (MessageComponent component : getComponents()) {
            component.setColor(ChatColor.DIM);
        }
    }

    public void italic() {
        for (MessageComponent component : getComponents()) {
            component.setItalic(true);
        }
    }
}
