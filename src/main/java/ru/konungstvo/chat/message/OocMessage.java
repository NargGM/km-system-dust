package ru.konungstvo.chat.message;

import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;

import java.util.List;

public class OocMessage extends RoleplayMessage {
    public OocMessage(String playerName, String message, Range range) {
        super(playerName, message, range);
        /*
        String template = ChatColor.NICK.get() + "{playerName}" + ChatColor.DEFAULT + " ({rangeDescription} в ООС): " + ChatColor.OOC + "(( {content} ))";
        if (this.getRange() == Range.NORMAL) {
            template = ChatColor.NICK.get() + "{playerName}" + ChatColor.DEFAULT + " (ООС): " + ChatColor.OOC + "(( {content} ))";
        }

         */
    }

    @Override
    public List<MessageComponent> buildTemplate(List<MessageComponent> template) {
        template.add(new MessageComponent("{playerName}", ChatColor.NICK));
        if (getRange() != Range.NORMAL) {
            template.add(new MessageComponent(" ({rangeDescription} в ООС): "));
        } else {
            template.add(new MessageComponent(" (в ООС): "));
        }
        template.add(new MessageComponent("(( {content} ", ChatColor.OOC));
        template.add(new MessageComponent("))", ChatColor.OOC));
        return template;
    }
}
