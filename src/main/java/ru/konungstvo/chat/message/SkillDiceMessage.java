package ru.konungstvo.chat.message;

import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.combat.SkillType;
import ru.konungstvo.combat.Trait;
import ru.konungstvo.combat.dice.FudgeDice;
import ru.konungstvo.combat.dice.SkillDice;
import ru.konungstvo.combat.dice.modificator.Modificator;
import ru.konungstvo.combat.dice.modificator.ModificatorType;
import ru.konungstvo.combat.dice.modificator.PercentDice;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Logger;
import ru.konungstvo.exceptions.DataException;
import ru.konungstvo.player.Player;

import java.io.Serializable;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SkillDiceMessage
        extends FudgeDiceMessage
        implements Comparable, Serializable {

    private SkillDice skillDice;
    private String comment = "";

    private String blocksInfo = "";
    private Logger logger = new Logger(this.getClass().getSimpleName());

    public SkillDiceMessage(String playerName, String content, SkillDice skillDice) {
        this(playerName, content, Range.NORMAL, skillDice);
    }

    public SkillDiceMessage(String playerName, String content) {
        this(playerName, content, Range.NORMAL_DIE);
    }

    public SkillDiceMessage(String playerName, String content, Range range) {
        this(playerName, content, range, null, 0);
    }

    public SkillDiceMessage(String playerName, String content, Range range, SkillDice skilldice) {
        this(playerName, content, range, skilldice, 0);
    }

    public SkillDiceMessage(String playerName, String message, Range range, int previousDefenses) {
        this(playerName, message, range, null, previousDefenses);
    }

    public SkillDiceMessage(String playerName, String content, Range range, SkillDice skillDice, int previousDefenses) {
        super(playerName, content, range);

//        logger.debug("Creating SDM: " + content);
        String comment = "";
        if (skillDice == null) {
            // parse skillName and skillLevel from content, using player's skillset

            Player player = DataHolder.inst().getPlayer(playerName);
            if (player == null) throw new DataException("1", "Игрок " + playerName + " не найден!");
            String overrideskillname = "";
            String skill = player.getSkillNameFromContext(content);
            String attribute = "";
            boolean ignoreAttribute = false;
            boolean attributeRules = false;
            if (content.contains(skill+"+")) {
                attribute = player.getSkillNameFromContext(content.replace(skill+"+", ""));
                if (player.getSkill(attribute).getSkillType().equals(SkillType.ATTRIBUTE) && !player.getSkill(skill).getSkillType().equals(SkillType.ATTRIBUTE)) attributeRules = true;
            } else if (content.contains(skill+"-")) {
                ignoreAttribute = true;
                overrideskillname = skill + "-";
            }
            int convenience = 0;


            PercentDice percentDice = new PercentDice();

            if (skill == null) {
                throw new DataException("1", "Неверный ввод. Попробуйте: % название_навыка, либо % уровень_навыка.");
            }

            // consider situation where we used alias for skill
//            if (!content.substring(2).toLowerCase().startsWith(skill)) {
//                content = content.replaceFirst(player.getAliasFor(skill), skill);
//            }

            int initial = player.getSkillLevel(skill);

            //parse message to extract needed values
            Pattern pattern = Pattern.compile(SkillDice.regex);

//            logger.debug("Trying to trim " + skill + " from " + content);
            Matcher matcher = pattern.matcher(content.replaceFirst("%", "").trim().substring(skill.length() + (attribute.isEmpty() ? 0 : attribute.length() + 1)));
            if (!matcher.find()) throw new IllegalStateException("Неверно введённый 4dF дайс!");

            int mod = 0;

            if (matcher.group(1) != null && !matcher.group(1).equals("")) {
                //System.out.println("Matcher" + (matcher.group(1)));
                String modifier = matcher.group(1);
                //System.out.println("MOD" + modifier);
                if(modifier.contains("%")) {
                    System.out.println("TRUE");
                    int percentMod = Integer.parseInt(modifier.replace("%","").trim());
                    percentDice.setCustom(percentDice.getCustom() + percentMod);
                } else {
                    mod = Integer.parseInt(modifier.substring(1));
                    if(mod < -9) mod = -9;
                    if(mod > 9) mod = 9;
                }
            }
            //System.out.println("MOD" + mod);
            if (matcher.group(2) != null && !matcher.group(2).equals("")) {
                comment = matcher.group(2).substring(1).replaceAll("§.", "").replaceAll("&([a-z0-9])", "§$1");
            }
            this.comment = comment;

            if (!attribute.isEmpty()) overrideskillname = skill + "+" + attribute;

            if (skill.equals("скорость")) {
                attributeRules = true;
                attribute = "ловкость";
                skill = "атлетика";
                overrideskillname = "скорость";
                initial = player.getSkillLevel(skill);
                convenience = player.countConvenience();
            } else if (skill.equals("скрытность")) {
                convenience = player.countConvenience();
            }


            int armorMod = 0;
//            if (SkillsHelper.isArmorDependent(skill)) {
//                armorMod = player.getArmorMod();
//            }

            int shieldMod = 0;
//            if (SkillsHelper.isArmorDependent(skill)) {
//                shieldMod = player.getShieldMod();
//            }

            int woundsMod = -player.getWoundsMod();
            if (player.hasTrait(Trait.ISME)) {
                int woundsModPercent = woundsMod * 100;
                woundsModPercent = woundsModPercent / 2;
                woundsMod = woundsMod / 100;
                woundsModPercent = woundsModPercent % 100;
                percentDice.setWound(woundsModPercent);
            } else if (player.hasTrait(Trait.ISM)) {
                woundsMod = 0;
            }

//            System.out.println("WoundsPercentMod is " + woundsPercentMod);

            int bloodloss = player.getBloodloss();
            percentDice.setBloodloss(-bloodloss);
            System.out.println("FudgeDiceMessage: " + "bloodloss=" + -bloodloss);

            int mentalDebuff = player.getMentalDebuff();
            percentDice.setMentalDebuff(-mentalDebuff);
            System.out.println("FudgeDiceMessage: " + "mentalDebuff=" + -mentalDebuff);

            percentDice.setFatigue(player.getFatigue());
            percentDice.setWeakened(player.getWeakened());




//            if (skill.toLowerCase().equals("выносливость")) {
//                woundsMod = 0;
//                percentDice.setWound(0);
//                percentDice.setBloodloss(0);
//                percentDice.setMentalDebuff(0);
//                percentDice.setFatigue(0);
//                percentDice.setWeakened(0);
//            }

            //конечности
            if (skill.toLowerCase().equals("уклонение") || skill.toLowerCase().equals("передвижение")) {
                percentDice.checkAndGetAndSetLimbInjurty(3, playerName);
            }
            //

            //конечности
            if (skill.toLowerCase().equals("рукопашный бой")) {
                percentDice.checkAndGetAndSetLimbInjurty(2, playerName);
            }
            //

            //вес
            int weightMod = 0;
            int psyWeightMod = 0;
            int weightPerc = 0;
            int weight = player.countWeight(); //TODO Оптимизировать?
            int optimalWeight = player.getOptimalWeight();
            int maxWeight = player.getMaxWeight();
            System.out.println("FudgeDiceMessage: " + "weight=" + weight);
            System.out.println("FudgeDiceMessage: " + "optimalWeight=" + optimalWeight);
            System.out.println("FudgeDiceMessage: " + "maxWeight=" + maxWeight);


//            int weightCutPart = weightPerc % 100;
//            percentDice.setWeightDebuff(-weightCutPart);
//            if (weightPerc >= 100) {
//                weightMod = -(weightPerc / 100);
//            }



            int coverMod = 0;

            int concentratedMod = 0;
            if (player.hasCourage(skill)) {
                System.out.println("!!!!!!!!!!!!found concentration on " + skill);
                concentratedMod = 3;
                player.removeCourage(skill);
                if (skill.equals("физическая защита") || skill.equals("психическая защита")) concentratedMod = 2;
                player.courage = Math.max(player.courage - 1, 0);
                DataHolder.inst().getModifiers().updateCourage(playerName, player.courage, player.maxcourage);
            }

            int percentSkill = player.getSkill(skill).getPercent();




            if (!ignoreAttribute && attribute.isEmpty()) attribute = player.getAttribute(skill);
            int attributeMod = 0;
            if (!attribute.equals("")) {
                if (!player.getSkill(skill).getSkillType().equals(SkillType.ATTRIBUTE)) attributeRules = true;
                if (attribute.contains("/")) {
                    String attr1 = attribute.split("/")[0];
                    String attr2 = attribute.split("/")[1];
                    double attrint1 = player.getSkillLevel(attr1);
                    double attrint2 = player.getSkillLevel(attr2);
                    attributeMod = (int) Math.floor((attrint1 + attrint2) / 2);
                    percentSkill += (int) Math.floor(((double) player.getSkill(attr1).getPercent() + (double) player.getSkill(attr2).getPercent()) / 2);
                } else if (attribute.contains("|")) {
                    String attr1 = attribute.split("\\|")[0];
                    String attr2 = attribute.split("\\|")[1];
                    int attrint1 = player.getSkillLevel(attr1);
                    int attrint2 = player.getSkillLevel(attr2);
                    if (attrint2 < attrint1) {
                        attribute = attr2;
                        attributeMod = attrint2;
                    } else {
                        attribute = attr1;
                        attributeMod = attrint1;
                    }
                    percentSkill += player.getSkill(attribute).getPercent();
                } else {
                    attributeMod = player.getSkillLevel(attribute);
                    percentSkill += player.getSkill(attribute).getPercent();
                }
            }

            if (player.hasCourage(attribute)) {
                System.out.println("!!!!!!!!!!!!found concentration on " + attribute);
                concentratedMod = 3;
                player.removeCourage(attribute);
                if (attribute.equals("физическая защита") || attribute.equals("психическая защита")) concentratedMod = 2;
                if (skill.equals("физическая защита") || skill.equals("психическая защита")) concentratedMod = 2;
                player.courage = Math.max(player.courage - 1, 0);
                DataHolder.inst().getModifiers().updateCourage(playerName, player.courage, player.maxcourage);
            }

            if (weight > maxWeight) {
                if ((skill.toLowerCase().matches("физическая защита|акробатика|скрытность")) || attribute.toLowerCase().matches("физическая защита|акробатика|скрытность") || overrideskillname.equals("скорость")) {
                    int w = weight;
                    while (w > maxWeight) {
                        weightMod--;
                        w -= 5;
                    }
                }
            }





            if (player.getPsychweight() > player.getMaxPsychweight()) {
                if ((skill.toLowerCase().matches("психическая защита|опознание|пси-скрытность|реакция|внимательность")) || attribute.toLowerCase().matches("психическая защита|опознание|пси-скрытность|реакция|внимательность")) {
                    int w = player.getPsychweight();
                    while (w > player.getMaxPsychweight()) {
                        psyWeightMod--;
                        w -= 5;
                    }

                }
            }

            int buff = 0;

            buff += player.getBuff(skill);
            buff += player.getBuffFromNeeds(skill);

            int eqBuff = player.getBuffsFromEquipment(skill);


            System.out.println("skill" + " " + skill + " " + "attr "  + attribute + " " + eqBuff);

            if (!attribute.isEmpty()) {
                eqBuff += player.getBuffsFromEquipment(attribute);
                System.out.println(eqBuff + " test");
                buff += player.getBuff(attribute);
                buff += player.getBuffFromNeeds(attribute);
            }

            if (overrideskillname.equals("скорость")) {
                eqBuff +=  player.getBuffsFromEquipment(overrideskillname);
                buff += player.getBuff(overrideskillname);
                buff += player.getBuffFromNeeds(overrideskillname);
                System.out.println(eqBuff + " test");
            }

            int percentBuff = eqBuff % 100;
            eqBuff = eqBuff / 100;


            percentDice.setBuff(buff);

            percentDice.setEquipment(percentBuff);


            percentDice.setSkill(percentSkill);
            skillDice = new SkillDice(initial, skill, mod, woundsMod, armorMod, coverMod, shieldMod, weightMod, psyWeightMod, concentratedMod, percentDice, attribute, attributeMod, eqBuff);
            if (player.getCustomAdvantage() != 0) {
                if (player.getCustomAdvantage() > 0) skillDice.addAdvantage("Кастом");
                else skillDice.addDisadvantage("Кастом");
            }



            skillDice.setOverrideskillname(overrideskillname);
            skillDice.setAttributeRules(attributeRules);
            if (convenience != 0) skillDice.addMod(new Modificator(convenience, ModificatorType.CONVENIENCE));

        } else {
            System.out.println("skilldice already set");
            skillDice.getPercentDice().setSkill(DataHolder.inst().getPlayer(playerName).getSkill(skillDice.getSkillName()).getPercent());
        }

        skillDice.considerDefenseMod(previousDefenses);


        this.skillDice = skillDice;
    }

    @Override
    public void build() {
        if (isBuilt()) {
            System.out.println("already built1");
            return;
        }

        skillDice.cast();

        if (comment!= null && !comment.equals("")) comment = " (" + comment.replaceAll("&([a-z0-9])" + "", "§$1") + "§e)";
        this.addInput("info", skillDice.getInfo());
        this.addInput("baseLevel", skillDice.getBaseWoDefAsString());
        this.addInput("skillInfo", skillDice.getSkillInfo());
        this.addInput("skillInfo2", skillDice.getSkillInfo2());
        this.addInput("result", skillDice.getResultAsString());
        this.addInput("comment", comment);
        //if (getDice().wasChangedByPercentDice()) {
            int percentDice = getDice().getPercentDice().get();
            String percentDiceStr = String.valueOf(percentDice);
            percentDiceStr += "%";
            String dbuff = "Бафф: ";
            if (percentDice < 0 ) dbuff = "Дебафф: ";
            String debug = "\n" + TextFormatting.GRAY + dbuff + percentDiceStr +
                    " " + (Math.abs(percentDice) >= getDice().getPersentResult() && percentDice != 0 ? (percentDice < 0 ? TextFormatting.RED : TextFormatting.DARK_GREEN) : "") + "Бросок: " + getDice().getPersentResult() + "/100" + TextFormatting.GRAY;
        debug += "\n" + getDice().getPercentDice().toString();
        this.addInput("debug", debug);

       // }
        super.build();
    }




    public void considerDefenseMod(int previousDefenses) {
        this.skillDice.considerDefenseMod(previousDefenses);
    }

    @Override
    public List<MessageComponent> buildTemplate(List<MessageComponent> template) {
        template.add(new MessageComponent("", ChatColor.DICE));
        template.add(new MessageComponent("{playerName}", ChatColor.NICK));
        if (range.toString().contains("GM")) {
            template.add(new MessageComponent(" (to GM)"));
            //template.add(new MessageComponent(" бросает 4dF {info} от {baseLevel} [{skillInfo}]{comment}. Результат: {result}{debug} ))", ChatColor.DICE));
            template.add(new MessageComponent(" бросает", ChatColor.DICE));
        } else if (range.toString().contains("CM")) {
            template.add(new MessageComponent(" (to CM)", ChatColor.CMCHAT));
            //template.add(new MessageComponent(" бросает 4dF {info} от {baseLevel} [{skillInfo}]{comment}. Результат: {result}{debug} ))", ChatColor.DICE));
            template.add(new MessageComponent(" бросает", ChatColor.DICE));
        } else {
            //template.add(new MessageComponent(" {rangeDescription} 4dF {info} от {baseLevel} [{skillInfo}]{comment}. Результат: {result}{debug} ))", ChatColor.DICE));
            template.add(new MessageComponent(" {rangeDescription} ", ChatColor.DICE));
        }
        template.add(new MessageComponent("{baseLevel}", ChatColor.DICE));
        if (skillDice.getEnemyDice() != null) {
            MessageComponent modInfo = new MessageComponent(" [{skillInfo2}] ", ChatColor.DICE);
            modInfo.setHoverText((skillDice.getFinalHover(false)).trim(), TextFormatting.GRAY);
            template.add(modInfo);
            MessageComponent against =  new MessageComponent("против " + skillDice.getEnemyDice().skillDice.getBaseWoDefAsString(), ChatColor.DICE);
            template.add(against);

            MessageComponent modInfo2 = new MessageComponent(" [" + skillDice.getEnemyDice().skillDice.getSkillInfo2() + "]", ChatColor.DICE);
            modInfo2.setHoverText((skillDice.getEnemyDice().skillDice.getFinalHover(false)).trim(), TextFormatting.GRAY);
            template.add(modInfo2);
            MessageComponent modInfo3 = new MessageComponent(" [" + skillDice.getFinalModAsString(true) + "] ", ChatColor.DICE);
            modInfo3.setHoverText((skillDice.getFinalHover(true)).trim(), TextFormatting.GRAY);
            template.add(modInfo3);
        } else {
            MessageComponent modInfo = new MessageComponent(" [{skillInfo}] ", ChatColor.DICE);
            modInfo.setHoverText((skillDice.getFinalHover()).trim(), TextFormatting.GRAY);
            template.add(modInfo);
        }
        MessageComponent percentage = new MessageComponent("{info}", ChatColor.DICE);
        if (!skillDice.getDices().trim().isEmpty()) percentage.setHoverText(skillDice.getDices().trim(), TextFormatting.GOLD);
        template.add(percentage);
        //template.add(new MessageComponent(" от {baseLevel} [{skillInfo}]{comment}. Результат: {result} ))", ChatColor.DICE));

        if (!skillDice.getDiff().isEmpty()) {
            MessageComponent diff = new MessageComponent(skillDice.getDiff(), ChatColor.DICE);
            diff.setHoverText(skillDice.getAdvDisadvString(), TextFormatting.GOLD);
            template.add(diff);
            MessageComponent percentage2 = new MessageComponent(skillDice.getInfo2(), ChatColor.DICE);
            if (!skillDice.getDices2().trim().isEmpty()) percentage2.setHoverText(skillDice.getDices2().trim(), TextFormatting.GOLD);
            template.add(percentage2);
        }

//        template.add()"{modInfo}";
        template.add(new MessageComponent("{comment}", ChatColor.DICE));
        template.add(new MessageComponent(". Результат: {result} " + getPlural2(skillDice.getResult(), skillDice.isCritFail()), ChatColor.DICE));
        if (skillDice.getResult() > 4) template.add(new MessageComponent(" §l[КРИТИЧЕСКИЙ УСПЕХ]§e", ChatColor.NICK));
        template.add(new MessageComponent(".", ChatColor.DICE));
        //else if (skillDice.isCritFail()) template.add(new MessageComponent(" [КРИТИЧЕСКИЙ ПРОВАЛ]", ChatColor.DARK_RED));
        if (!blocksInfo.isEmpty()) template.add(new MessageComponent(" " + blocksInfo + ".", ChatColor.DICE));
        return template;
    }

    /*
    @Override
    protected void generateTemplate() {
        String template;
        if (range.toString().contains("GM")) {
            template = ChatColor.DICE.get() + "(( " +
                    ChatColor.NICK.get() + "{playerName}" + " §f(to GM)" +
                    ChatColor.DICE.get() + " {rangeDescription} 4dF {info} от {baseLevel} [{skillInfo}]{comment}. Результат: {result} ))";
        } else if (range.toString().contains("BD")) {
            template = ChatColor.DICE.get() + "(( " +
                    ChatColor.NICK.get() + "{playerName}" + ChatColor.BDCHAT.get() + " (to BD)" +
                    ChatColor.DICE.get() + " {rangeDescription} 4dF {info} от {baseLevel} [{skillInfo}]{comment}. Результат: {result} ))";
        } else {
            template = ChatColor.DICE.get() + "(( " +
                    ChatColor.NICK.get() + "{playerName}" +
                    ChatColor.DICE.get() + " {rangeDescription} 4dF {info} от {baseLevel} [{skillInfo}]{comment}. Результат: {result} ))";
        }
        //this.setTemplate(template);
    }
     */




    @Override
    public FudgeDice getDice() {
        return this.skillDice;
    }

    @Override
    public int compareTo(Object o) {
        SkillDiceMessage other = (SkillDiceMessage) o;
        int leftDie = this.getDice().getResult();
        int rightDie = other.getDice().getResult();
        if (leftDie == rightDie) {
            return 0;
        } else if (leftDie < rightDie) {
            return -1;
        }
        return 1;

    }

    public int compareToButBetter(Object o) {
        SkillDiceMessage other = (SkillDiceMessage) o;
        int leftDie = this.getDice().getFirstResult();
        int rightDie = other.getDice().getFirstResult();
        System.out.println(this.getPlayerName() + " first result " + leftDie + "|" + other.getPlayerName() + " first result " + rightDie);
        if (leftDie == rightDie) {
            System.out.println("EQUAL");
//            System.out.println(this.getPlayerName() + " is rerolled " + this.getDice().isRerolled() + "|" + other.getPlayerName() + " is rerolled " + other.getDice().isRerolled());
//            if (this.getDice().isRerolled() && other.getDice().isRerolled()) { //Probably should comment this part but who actually cares?
//                System.out.println(this.getPlayerName() + " last result " + this.getDice().getResult() + "|" + other.getPlayerName() + " last result " + other.getDice().getResult());
//                if (this.getDice().getResult() > other.getDice().getResult()) {
//                    System.out.println("1");
//                    return 1;
//                } else if (this.getDice().getResult() < other.getDice().getResult()) {
//                    System.out.println("-1");
//                    return -1;
//                }
//            }
            for (int i = 0; i < 15; i++) {
                System.out.println("RE-ROLL TIME " + i);
                this.reroll();
                other.reroll();
                System.out.println(this.getPlayerName() + " last result " + this.getDice().getResult() + "|" + other.getPlayerName() + " last result " + other.getDice().getResult());
                if (this.getDice().getResult() > other.getDice().getResult()) {
                    System.out.println("1");
                    return 1;
                } else if (this.getDice().getResult() < other.getDice().getResult()) {
                    System.out.println("-1");
                    return -1;
                }
            }
            System.out.println(this.getPlayerName() + " base " + this.getDice().getBase() + "|" + other.getPlayerName() + " base " + other.getDice().getBase());
            if (this.getDice().getBase() > other.getDice().getBase()) {
                return 1;
            } else if (this.getDice().getBase() < other.getDice().getBase()) {
                return -1;
            }
            Random rand = new Random();
            System.out.println("EPIC FAIL - FLIP A COIN");
            if (rand.nextInt(2) == 0) {
                System.out.println("1");
                return 1;
            } else {
                System.out.println("-1");
                return -1;
            }
            //return 0;
        } else if (leftDie < rightDie) {
            System.out.println("-1");
            return -1;
        }
        System.out.println("1");
        return 1;

    }

    public void reroll() {
        String snapshot = this.getResult();
        String prevResult = DataHolder.inst().getSkillTable().get(this.getDice().getResult());
        this.skillDice.reroll();
        this.addInput("result", skillDice.getResultAsString());
        this.build();
        String debug = "Rerolled \"" + snapshot + "\": " + prevResult + " --> " +
                DataHolder.inst().getSkillTable().get(this.getDice().getResult()
                );
        logger.info(debug);
        //DiscordBridge.sendMessage(debug);
    }

    @Override
    public String getComment() {
        return comment;
    }

    @Override
    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPlural2(int num, boolean critfail) {
        if (num < 1 && critfail) return "§4§l[КРИТИЧЕСКИЙ ПРОВАЛ]§e";
        if (num < 1) return "§4[Провал]§e";
        int preLastDigit = num % 100 / 10;
        if (preLastDigit == 1) {
            return "Успехов";
        }

        switch (num % 10) {
            case 1:
                return "Успех";
            case 2:
            case 3:
            case 4:
                return "Успеха";
            default:
                return "Успехов";
        }

    }

    public void setBlocksInfo(String blocksInfo) {
        this.blocksInfo = blocksInfo;
    }
}
