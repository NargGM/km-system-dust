package ru.konungstvo.chat.message.infrastructure;

import net.minecraft.entity.player.EntityPlayer;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.bridge.discord.DiscordBridge;
import ru.konungstvo.chat.message.*;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.chat.range.RangeFactory;
import ru.konungstvo.combat.dice.CommonDice;
import ru.konungstvo.combat.dice.FudgeDice;
import ru.konungstvo.combat.dice.SkillDice;
import ru.konungstvo.commands.executor.CombatExecutor;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.Logger;
import ru.konungstvo.exceptions.DataException;
import ru.konungstvo.player.NPC;
import ru.konungstvo.player.Permission;
import ru.konungstvo.player.Player;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses message contents and creates an according Message subclass, or directs message to command executors.
 */


public class MessageFactory {
    private static final Pattern pattern = Pattern.compile("(^\\?[^?]*\\?\\s?)");
    public static RoleplayMessage createMessage(Player player, String message) throws Exception {
        String playerName = player.getDisplayedName();
        Player executor = DataHolder.inst().getPlayer(player.getName());
        Logger logger = new Logger("MessageFactory");

        try {
            if (message.substring(0, 4).contains("№") && player.hasPermission(Permission.GM) && !playerHasRadiomic(player)) {
                message = message.replaceFirst("№", "#");
            }
        } catch (IndexOutOfBoundsException ignored) {}

        boolean inMessagePseudonym = false;

        if (player.hasPermission(Permission.GM)) {
            Matcher matcher = pattern.matcher(message);
            if (matcher.find()) {
                player.setPseudonym(matcher.group().replaceFirst("\\?","").replaceFirst("\\?", "").trim());
                message = matcher.replaceFirst("");
                inMessagePseudonym = true;
            }
        }


        Range range = RangeFactory.build(message);
        Range originalRange = RangeFactory.build(message);
        message = message.substring(range.getNumberOfSymbolsToStrip());

        // Handle default range function
        if (message.equals(":")) {
            player.setDefaultRange(Range.NORMAL);
            player.sendMessage("§7Стандартная дистанция сброшена.");
            return null;
        } else if (message.isEmpty()) {
            if (player.getDefaultRange() == Range.NORMAL || !player.getDefaultRange().equals(range)) {
                player.setDefaultRange(range);
                player.sendMessage("§7Стандартная дистанция установлена: " + range.getDescription());
                return null;
            } else {
                player.setDefaultRange(Range.NORMAL);
                player.sendMessage("§7Стандартная дистанция сброшена.");
                return null;
            }
        }

        if (player.hasPermission(Permission.GM) && message.equals("forsub")) {
            Player subordinate = player.getSubordinate();
            if (subordinate != null) {
                if (subordinate.getDefaultRange() == Range.NORMAL || !subordinate.getDefaultRange().equals(range)) {
                    subordinate.setDefaultRange(range);
                    player.sendMessage("§7Стандартная для подчиненного дистанция установлена: " + range.getDescription());
                    return null;
                } else {
                    subordinate.setDefaultRange(Range.NORMAL);
                    player.sendMessage("§7Стандартная для подчиненного дистанция сброшена.");
                    return null;
                }
            }
            player.sendMessage("§4Нет подчиненного.");
            return null;
        }


        //check if player uses default range. Some ranges still have priority
        if (player.getDefaultRange() != Range.NORMAL && range != Range.GM && range != Range.GM_DIE && range != Range.CM && !range.toString().contains("STORY")) {
            if (range == Range.NORMAL || range == Range.NORMAL_DIE) {
//            if (!(message.startsWith("((") && message.endsWith("))") || message.startsWith("_"))
//                    && (player.getDefaultRange() != Range.GM && !player.hasPermission(Permission.GM) {
                if (range.toString().contains("DIE")) {
                    range = Helpers.convertToDieRange(player.getDefaultRange());
                } else {
                    if (!inMessagePseudonym) range = player.getDefaultRange();
                    else if (player.getDefaultRange() != Range.GM) range = player.getDefaultRange();
                }
            }
        }

        //check if player has permissions to use certain range
        if (range.toString().contains("STORY")) {
            if (!player.hasPermission(Permission.GM)) {
                range = Range.NORMAL;
            }
        }
//        if (range.toString().contains("BD")) {
//            if (!player.hasPermission(Permission.GM) && !player.hasPermission(Permission.BD)) {
//                range = Range.NORMAL;
//            }
//        }


        // ACTION
        if (range == Range.NORMAL && message.startsWith("*")) {
//        if (range.name().contains("ACTION")) {
            if (player.hasPermission(Permission.GM) && !player.getPseudonym().isEmpty()) playerName = player.getPseudonym();
            return new ActionMessage(playerName, message.substring(1), range);

            //GLOBAL
        } else
            if (message.startsWith("^")) {
                return new GlobalMessage(playerName, message.substring(1), range);

            } else if (message.startsWith("№") && playerHasRadiomic(player)) {
                if (message.startsWith("№-") && player.hasPermission(Permission.GM)) {
                    message = message.replaceFirst("№-", "№");
                    range = Range.NORANGE;
                }
                if (message.startsWith("№((") && message.endsWith("))")) {
                    String substring = message.substring(3, message.length() - 2);
//                    String radiomessage = " (в ООС): (( " + substring.trim() + " ))";
//                    ServerProxy.sendRadioMessage(player, radiomessage);
                    return new RadioOocMessage(playerName, substring.trim(), range);
                }

                if (message.startsWith("№_")) {
//                    String radiomessage = " (в ООС): (( " + message.substring(2) + " ))";
//                    ServerProxy.sendRadioMessage(player, radiomessage);
                    return new RadioOocMessage(playerName, message.substring(2), range);
                }
                String radiomessage = ": " + message.substring(1);
//                ServerProxy.sendRadioMessage(player, radiomessage);
                if (player.hasPermission(Permission.GM) && !player.getPseudonym().isEmpty()) playerName = player.getPseudonym();
                return new RadioMessage(playerName, message.substring(1), range);
            // OOC
        } else if (message.startsWith("_") || (message.startsWith("((") && message.endsWith("))"))) {
            if (range.toString().contains("GM")) {
                if (!player.hasPermission(Permission.GM)) {
                    return new RoleplayMessage(playerName, message, range);
                }
                range = originalRange;
            }
            if (message.startsWith("_"))
                return new OocMessage(playerName, message.substring(1), range);
            return new OocMessage(playerName, message.substring(2, message.length() - 2).trim(), range);

            // COMMON DICE NdN+N
        } else if (CommonDice.matches(message)) {
            //don't use default range if it's GM-range
            if (range.toString().contains("GM")) {
                range = originalRange;
            }
            if (player.hasPermission(Permission.GM) && !player.getPseudonym().isEmpty()) playerName = player.getPseudonym();
            return new CommonDiceMessage(playerName, message, range);

            // FUDGE DICE
        } else if (message.startsWith("%")) { // handle all Fudge dice

            DataHolder.inst().updatePlayer(player.getName());
            if (!message.contains("% ")) {
                message = message.replace("%", "% ");
            }

            // GM can throw die using another player's name and skill
            if (player.hasPermission(Permission.GM)) {

                //don't use default range if it's GM-range
                if (range.toString().contains("GM")) {
                    range = originalRange;
                }

                String helpVar = message.substring(2);
                try {
                    String firstArg = helpVar.substring(0, helpVar.indexOf(" "));
                    // Throwing die in name of other player or NPC
                    if (DataHolder.inst().getPlayer(firstArg) != null) {
                        String mes = playerName + " кидает дайс за игрока " + firstArg + ": " + message;
                        DiscordBridge.sendMessage(mes);
                        logger.info(mes);
                        message = message.replaceFirst(firstArg + " ", "");
                        player = DataHolder.inst().getPlayer(firstArg);
                        if (player instanceof NPC) {
                            logger.debug("Found NPC");
                        } else {
                            logger.debug("Found player");
                        }
                    }
                } catch (IndexOutOfBoundsException ignored) {

                }
            }
            boolean isCombat;
            isCombat = CombatExecutor.processCombatDice(player, message, range);

            // consider aliases
            try {
                String skill = player.getSkillNameFromContext(message);
                // consider situation where we used alias for skill
                if (!message.substring(2).toLowerCase().startsWith(skill)) {
                    message = message.replaceFirst(player.getAliasFor(skill), skill);
                }
            } catch (DataException ignored) {
            }


            // consider different movement types
            /*
            String movementType = DataHolder.inst().getMovementHelper().getMovementType(message);
            if (!movementType.isEmpty()) {
                if (movementType.equals("быстрый бег") && player.getMovementTrait() == null) {
                    throw new RuleException("Вы не можете пользоваться быстрым бегом!");
                }
                if (movementType.equals("супербег") && player.getMovementTrait() != MovementTrait.SUPERFAST) {
                    throw new RuleException("Вы не можете пользоваться сверхбыстрым бегом!");
                }
                if (player.getMovementTrait() == MovementTrait.FAST && movementType.equals("передвижение")) {
                    movementType = "быстрый бег";
                }
                if (player.getMovementTrait() == MovementTrait.SUPERFAST && movementType.equals("передвижение")) {
                    movementType = "супербег";

                }
             */

                /*
                if (player.getMovementHistory() == MovementHistory.NONE) {
                    switch (movementType) {
                        case "передвижение":
                            movementType = "разбег";
                            break;
                        case "быстрый бег":
                            movementType = "передвижение";
                            break;
                        case "супербег":
                            movementType = "быстрый бег";
                            break;
                    }
                }
                message = message.replace(movementType, "передвижение");
            }
                */


            isCombat = false;
            if (isCombat) {
                // combat dice e.g. "% владение мечом Гоблин1"
                logger.debug("Found combat dice!");
                return null;

                // handle simple Fudge dice such as "% нормально"
            } else if (FudgeDice.matches(message.toLowerCase())) {
                //DataHolder.inst().updatePlayerWounds(player.getName());
                return new FudgeDiceMessage(player.getName(), message, range);

                // skill dice such as "% бег"
            } else if (SkillDice.matches(player.getName(), message.toLowerCase())) {
                DataHolder.inst().updatePlayerWounds(player.getName());
                DataHolder.inst().updatePlayerArmor(player.getName());
                DataHolder.inst().updatePlayerShield(player.getName());

                SkillDiceMessage sdm = new SkillDiceMessage(player.getName(), message.toLowerCase(), range);

                // MOVEMENT
                /*
                System.out.println("DEBUG: " + sdm.getDice().getSkillName());
                System.out.println("DEBUG2: " + player.getByAlias("передвижение"));
                if (MovementHelper.isMovement(message, player.getByAlias(sdm.getDice().getSkillName()).getName())) {
                    if ((player.getMovementHistory() != MovementHistory.LAST_TIME && player.getMovementHistory() != MovementHistory.NOW &&
                            (movementType.equals("передвижение") || movementType.equals("быстрый бег") || movementType.equals("супербег")))) {
                        executor.sendMessage(ChatColor.RED + "Нельзя бежать без разбега! Используйте % разбег.");
                        return null;
                    }
                    if (!movementType.equals("сп") && !movementType.equals("свободное передвижение")) {
//                        ForgeCore.sendMessage(player, range, sdm.getResult());
                    }
                    if (player instanceof NPC && player != executor) {
                        executor.sendMessage(NpcCommand.execute(executor, "tp", player.getName()));
                    }
                    GoExecutor.executeMovement(player, sdm, movementType, executor);
                    return null;
                }

                 */
                player.getPercentDice().setLimbInjury(0);// иначе приклеивается. По хорошему надо куда-то в другое место пихнуть потому что работает неидеально
                return sdm;
            }
        }


        // COMMON MESSAGE
        if (player.hasPermission(Permission.GM) && !player.getPseudonym().isEmpty() && range != Range.GM) playerName = player.getPseudonym();
        return new RoleplayMessage(playerName, message, range);
    }

    static boolean playerHasRadiomic(Player player1) {
        EntityPlayer player = ServerProxy.getForgePlayer(player1.getName());
        if (player.getHeldItemMainhand().hasTagCompound()) {
            if (player.getHeldItemMainhand().getTagCompound() != null) {
                if (player.getHeldItemMainhand().getTagCompound().hasKey("radio")) {
                    return true;
                }
            }
        }
        if (player.getHeldItemOffhand().hasTagCompound()) {
            if (player.getHeldItemOffhand().getTagCompound() != null) {
                if (player.getHeldItemOffhand().getTagCompound().hasKey("radio")) {
                    return true;
                }
            }
        }
        return false;
    }

    static String getRadioChannel(Player player1) {
        EntityPlayer player = ServerProxy.getForgePlayer(player1.getName());
        if (player.getHeldItemMainhand().hasTagCompound()) {
            if (player.getHeldItemMainhand().getTagCompound() != null) {
                if (player.getHeldItemMainhand().getTagCompound().hasKey("radio")) {
                    if (player.getHeldItemMainhand().getTagCompound().getCompoundTag("radio").hasKey("channel")) {
                        return player.getHeldItemMainhand().getTagCompound().getCompoundTag("radio").getString("channel");
                    }
                }
            }
        }
        if (player.getHeldItemOffhand().hasTagCompound()) {
            if (player.getHeldItemOffhand().getTagCompound() != null) {
                if (player.getHeldItemOffhand().getTagCompound().hasKey("radio")) {
                    if (player.getHeldItemOffhand().getTagCompound().getCompoundTag("radio").hasKey("channel")) {
                        return player.getHeldItemOffhand().getTagCompound().getCompoundTag("radio").getString("channel");
                    }
                }
            }
        }
        return "0.0";
    }
}
