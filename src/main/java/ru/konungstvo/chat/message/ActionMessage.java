package ru.konungstvo.chat.message;

import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;

import java.util.List;

public class ActionMessage extends RoleplayMessage {

    //private String template = "* " + ChatColor.NICK.get() + "{playerName}" + ChatColor.DEFAULT.get() + " {content}";

    public ActionMessage(String playerName, String content, Range range) {
        super(playerName, content, range);
    }

    @Override
    public List<MessageComponent> buildTemplate(List<MessageComponent> template) {
        template.add(new MessageComponent("* "));
        template.add(new MessageComponent("{playerName}", ChatColor.NICK));
//        template.add(new MessageComponent("{rangeDescription}"));
        template.add(new MessageComponent( " {content}"));
        return template;
    }
}
