package ru.konungstvo.chat.message;

import jdk.nashorn.internal.objects.annotations.Getter;
import net.minecraft.util.text.TextFormatting;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.combat.dice.FudgeDice;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.exceptions.DataException;
import ru.konungstvo.player.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FudgeDiceMessage extends RoleplayMessage {
    private FudgeDice fudgeDice;
    private String comment = "";

    public FudgeDiceMessage(String playerName, String content) {
        this(playerName, content, Range.NORMAL_DIE);
    }

    public FudgeDiceMessage(String playerName, String content, Range range) {
        super(playerName, content, range);

        if (this instanceof SkillDiceMessage) return; // TODO this feels wrong. Maybe refactor?

        Player player = DataHolder.inst().getPlayer(playerName);
        if (player == null) throw new DataException("Игрок " + playerName + " не найден!");

//        int woundsPercentMod = player.getWoundsMod();
//        System.out.println("WoundsPercentMod is " + woundsPercentMod);
//        int woundsMod = 0;
//        int cutPart = woundsPercentMod % 100;
//        System.out.println("cutPart is " + -cutPart);
//        player.getPercentDice().setWound(-cutPart);
//        if (woundsPercentMod > 100) {
//            woundsMod = -(woundsPercentMod / 100);
//        }

//        System.out.println("FudgeDiceMessage: " + "woundsMod=" + woundsMod);

//        int bloodloss = player.getBloodloss();
//        player.getPercentDice().setBloodloss(-bloodloss);
//        System.out.println("FudgeDiceMessage: " + "bloodloss=" + -bloodloss);

//        int mentalDebuff = player.getMentalDebuff();
//        player.getPercentDice().setMentalDebuff(-mentalDebuff);
//        System.out.println("FudgeDiceMessage: " + "mentalDebuff=" + -mentalDebuff);
        //parse message to extract needed values
        Pattern pattern = Pattern.compile(FudgeDice.regex);
        Matcher matcher = pattern.matcher(content.toLowerCase());
        if (!matcher.find()) throw new IllegalStateException("Неверно введённый 4dF дайс!");
        int initial = Integer.parseInt(matcher.group(1));
        if (initial > 50) initial = 50;
        int mod = 0;
        if (matcher.group(2) != null && !matcher.group(2).equals("")) {
            String modifier = matcher.group(2).substring(1);
            mod = Integer.parseInt(modifier);
        }
        player.getPercentDice().setSkill(0);
        FudgeDice fudgeDice = new FudgeDice(initial, mod, 0, 0, 0, player.getPercentDice());
        if (player.getCustomAdvantage() != 0) {
            if (player.getCustomAdvantage() > 0) fudgeDice.addAdvantage("Кастом");
            else fudgeDice.addDisadvantage("Кастом");
        }
        this.fudgeDice = fudgeDice;

        String comment = "";
        if (matcher.group(3) != null && !matcher.group(3).equals("")) {
            comment = matcher.group(3).substring(1).replaceAll("§.", "").replaceAll("&([a-z0-9])", "§$1");
        }
        this.comment = comment;
    }

    @Override
    public void build() {
        //if (isBuilt()) return;
        if (fudgeDice != null) {

            fudgeDice.cast();
            // get info about mod if needed
            String modInfoString;
            //modInfoString = " [" + fudgeDice.getInitial() + fudgeDice.getModAsString() + "]";
            modInfoString = "[" + fudgeDice.getFinalModAsString() + "] ";
            if (fudgeDice.getFinalModAsString().isEmpty()) modInfoString = "";
            if (!comment.equals("")) comment = " (" + comment.replaceAll("&([a-z0-9])", "§$1") + "§e)";

            this.addInput("info", fudgeDice.getInfo());
            this.addInput("baseLevel", fudgeDice.getBaseAsString());
            this.addInput("modInfo", " " + modInfoString);
            this.addInput("result", fudgeDice.getResultAsString());
            this.addInput("comment", comment);

            //if (getDice().wasChangedByPercentDice()) {
//            int percentDice = getDice().getPercentDice().get();
//            String percentDiceStr = String.valueOf(percentDice);
//            percentDiceStr += "%";
//            String dbuff = "Бафф: ";
//            if (percentDice < 0) dbuff = "Дебафф: ";
//            String debug = "\n" + TextFormatting.GRAY + dbuff + percentDiceStr +
//                    " Бросок: " + getDice().getPersentResult() + "/100";
//            debug += "\n" + getDice().getPercentDice().toString();
//            this.addInput("debug", debug);

            // }
        } else {
            System.out.println("fudgeDice seems to appear null");
        }
        super.build();
    }

    public void rebuild() {
        unbuild();
        this.build();
    }

    public String getPercentInfo() {
        int percentDice = getDice().getPercentDice().get();
        if (percentDice == 0) return "";
        if (percentDice >= 100) percentDice = percentDice % 100;
        if (percentDice <= -100) percentDice = percentDice % 100;
        String percentDiceStr = String.valueOf(percentDice);
        percentDiceStr += "%";

        String dbuff = "Бафф: ";
        if (percentDice < 0) dbuff = "Дебафф: ";
        return "\n" + TextFormatting.GRAY + dbuff + percentDiceStr +
                " " + (Math.abs(percentDice) >= getDice().getPersentResult() && percentDice != 0 ? (percentDice < 0 ? TextFormatting.RED : TextFormatting.GREEN) : "") + "Бросок: " + getDice().getPersentResult() + "/100" + TextFormatting.GRAY
                + "" + getDice().getPercentDice().toString()
        ;
    }

    @Override
    public List<MessageComponent> buildTemplate(List<MessageComponent> template) {
        //template.add(new MessageComponent("(( ", ChatColor.DICE));
        template.add(new MessageComponent("{playerName}", ChatColor.NICK));

        if (range.toString().contains("GM")) {
            template.add(new MessageComponent(" (to GM)"));
            template.add(new MessageComponent(" бросает ", ChatColor.DICE));
            //template.add(new MessageComponent(" бросает 4dF {info} от {baseLevel}{modInfo}{comment}. Результат: {result} ))", ChatColor.DICE));
        } else if (range.toString().contains("CM")) {
            template.add(new MessageComponent(" (to CM)", ChatColor.CMCHAT));
            template.add(new MessageComponent(" бросает ", ChatColor.DICE));
            // {info} от {baseLevel}{modInfo}{comment}. Результат: {result} ))", ChatColor.DICE));

        } else {
            //template.add(new MessageComponent(" {rangeDescription} 4dF {info} от {baseLevel}{modInfo}{comment}. Результат: {result}{debug} ))", ChatColor.DICE));
            template.add(new MessageComponent(" {rangeDescription} ", ChatColor.DICE));
        }
        template.add(new MessageComponent("{baseLevel} " + getPlural(fudgeDice.getBase()), ChatColor.DICE));
        MessageComponent modInfo = new MessageComponent("{modInfo}", ChatColor.DICE);
        modInfo.setHoverText((fudgeDice.getModAsString() + "" + getPercentInfo()).trim(), TextFormatting.GRAY);
        template.add(modInfo);
        MessageComponent percentage = new MessageComponent("{info}", ChatColor.DICE);
        percentage.setHoverText(fudgeDice.getDices().trim(), TextFormatting.GOLD);
        template.add(percentage);

        if (!fudgeDice.getDiff().isEmpty()) {
            MessageComponent diff = new MessageComponent(fudgeDice.getDiff(), ChatColor.DICE);
            diff.setHoverText(fudgeDice.getAdvDisadvString(), TextFormatting.GOLD);
            template.add(diff);
            MessageComponent percentage2 = new MessageComponent(fudgeDice.getInfo2(), ChatColor.DICE);
            percentage2.setHoverText(fudgeDice.getDices2().trim(), TextFormatting.GOLD);
            template.add(percentage2);
        }

        //template.add(new MessageComponent(" от {baseLevel}{modInfo}{comment}. Результат: {result} ))", ChatColor.DICE));

//        template.add()"{modInfo}";
        template.add(new MessageComponent("{comment}", ChatColor.DICE));
        template.add(new MessageComponent(". Результат: {result} " + getPlural2(fudgeDice.getResult(), fudgeDice.isCritFail()) + ".", ChatColor.DICE));
        if (fudgeDice.getResult() > 4) template.add(new MessageComponent(" §l[КРИТИЧЕСКИЙ УСПЕХ]§e", ChatColor.NICK));



        return template;
    }

    /*
    @Override
    protected void generateTemplate() {
        String template;
        if (range.toString().contains("GM")) {
            template = ChatColor.DICE.get() + "(( " +
                    ChatColor.NICK.get() + "{playerName}" + " §f(to GM)" +
                    ChatColor.DICE.get() + " бросает 4dF {info} от {baseLevel}{modInfo}{comment}. Результат: {result} ))";
        } else if (range.toString().contains("BD")) {
            template = ChatColor.DICE.get() + "(( " +
                    ChatColor.NICK.get() + "{playerName}" + ChatColor.BDCHAT.get() + " (to BD)" +
                    ChatColor.DICE.get() + " бросает 4dF {info} от {baseLevel}{modInfo}{comment}. Результат: {result} ))";
        } else {
            template = ChatColor.DICE.get() + "(( " +
                    ChatColor.NICK.get() + "{playerName}" +
                    ChatColor.DICE.get() + " {rangeDescription} 4dF {info} от {baseLevel}{modInfo}{comment}. Результат: {result} ))";
        }
      //  this.setTemplate(template);
    }

     */

    public FudgeDice getDice() {
        return fudgeDice;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPlural(int num) {

        int preLastDigit = num % 100 / 10;
        if (preLastDigit == 1) {
            return "кубиков";
        }

        switch (num % 10) {
            case 1:
                return "кубик";
            case 2:
            case 3:
            case 4:
                return "кубика";
            default:
                return "кубиков";
        }

    }

    public String getPlural2(int num, boolean critfail) {
        if (num < 1 && critfail) return "§4§l[КРИТИЧЕСКИЙ ПРОВАЛ]§e";
        if (num < 1) return "§4[Провал]§e";
        int preLastDigit = num % 100 / 10;
        if (preLastDigit == 1) {
            return "Успехов";
        }

        switch (num % 10) {
            case 1:
                return "Успех";
            case 2:
            case 3:
            case 4:
                return "Успеха";
            default:
                return "Успехов";
        }

    }

}
