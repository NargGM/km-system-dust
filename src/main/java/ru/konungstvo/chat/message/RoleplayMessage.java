package ru.konungstvo.chat.message;

import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.chat.range.RangeFactory;
import ru.konungstvo.control.Logger;

import java.util.*;

public class RoleplayMessage extends Message {
    private List<MessageComponent> components;
    private String playerName;
    private String content;
    protected Range range;
    Logger logger = new Logger("Message");
    private HashMap<String, String> inputs = new HashMap<>();
    private boolean built;

    public RoleplayMessage(String playerName, String content, Range range) {
        this.components = null;
        this.playerName = playerName;
        this.content = content.replaceAll("&([a-z0-9])", "§$1");
        this.range = range;
        this.built = false;

        //logger.debug("Created message with content \"" + content + "\" and range " + range + " and template " + template);
    }

    private void generateTemplate() {
        if (this.components != null) return;
        List<MessageComponent> template = new LinkedList<>();
        setComponents(buildTemplate(template));
    }

    // This is the method that should be overriden by subclasses to generate different template
    public List<MessageComponent> buildTemplate(List<MessageComponent> template) {
        MessageComponent gmes = new MessageComponent("{playerName}", ChatColor.NICK);
        if (range.toString().contains("CM")) {
            template.add(gmes);
            template.add(new MessageComponent(" ({rangeDescription}): "));
            template.add(new MessageComponent(" (( {content} ", ChatColor.CMCHAT));
            template.add(new MessageComponent("))", ChatColor.CMCHAT));
            //template = ChatColor.NICK.get() + "{playerName}" + ChatColor.DEFAULT.get() + " ({rangeDescription}): " + ChatColor.BDCHAT.get() + "(( {content} ))";

        } else if (range.toString().contains("GM")) {
            template.add(gmes);
            template.add(new MessageComponent(" ({rangeDescription}): "));
            template.add(new MessageComponent(" (( {content} ", ChatColor.GMCHAT));
            template.add(new MessageComponent("))", ChatColor.GMCHAT));


//            template = ChatColor.NICK.get() + "{playerName}" + ChatColor.DEFAULT.get() + " ({rangeDescription}): " + ChatColor.GMCHAT.get() + "(( {content} ))";

        } else if (range.toString().contains("STORY")) {
            template.add(new MessageComponent("{rangeDescription}{content}", ChatColor.STORY));
            template.add(new MessageComponent("{rangeDescription}", ChatColor.STORY));

            //template = ChatColor.STORY.get() + "{rangeDescription}{content}{rangeDescription}";

        } else if (range == Range.NORMAL) {
            template.add(gmes);
            template.add(new MessageComponent(": {content}"));
            //template = ChatColor.NICK.get() + "{playerName}" + ChatColor.DEFAULT.get() + ": {content}";
        } else {
            template.add(gmes);
            template.add(new MessageComponent(" ({rangeDescription}): {content}"));
            //template = ChatColor.NICK.get() + "{playerName}" + ChatColor.DEFAULT.get() + " ({rangeDescription}): {content}";
        }
        return template;
    }

    public String getContent() {
        return this.content;
    }



    // replace {variables} in template with according values with the help of MessageBuilder
    public void build() {
        if (isBuilt()) {
            System.out.println("already built1");
            return;
        }
        generateTemplate();
        inputs.put("content", content);
        inputs.put("playerName", playerName);
        inputs.put("rangeDescription", range.getDescription());

        for (MessageComponent component : components) {
           for (Map.Entry<String, String> input : inputs.entrySet()) {
               String variable = '{' + input.getKey() + '}';
               if (component.getText().contains(variable)) {
                   // put actual content in templates
                   component.setText(component.getText().replace(variable, input.getValue()));
               }
           }
        }
        built = true;
        try {
            //DataHolder.inst().getPlayer(playerName).getPercentDice().setSkill(0); // reset percent dice for skill //TODO sus
            //DataHolder.inst().getPlayer(playerName).getPercentDice().setCustom(0);
        } catch (Exception ignored) {}
        /*
        inputs.put("content", content);
        inputs.put("playerName", playerName);
        inputs.put("rangeDescription", range.getDescription());
        try {
            String templateStr = template.toString();
            result = MessageBuilder.build(templateStr, inputs);
        } catch (MissingFormatArgumentException e) {
            this.result = new MessageComponent(e.getCause() + " " + e.getMessage(), ChatColor.RED);
            //return new GameMessage(e.getCause() + " " + e.getMessage(), ChatColor.RED);
        }

         */
    }

    public boolean isBuilt() {
        return built;
    }

    public void unbuild() {
        this.built = false;
    }

    private void defineRange() {
        range = RangeFactory.build(content);
        content = content.substring(range.getSign().length());
    }

    @Deprecated
    public String getResult() {
        return "";
    }

    public String getPlayerName() {
        return playerName;
    }

    public Range getRange() {
        return this.range;
    }

    public void setComponents(List<MessageComponent> components) {
        this.components = components;
    }

    public List<MessageComponent> getComponents() {
        return components;
    }

    public int getRangeAsInt() {
        return this.range.getDistance();
    }

    protected void addInput(String var, String input) {
        inputs.put(var, input);
    }

    protected String getInput(String input) {
        return inputs.get(input);
    }

    protected void setContent(String content) {
        this.content = content;
    }

    public void setRange(Range range) {
        this.range = range;
//        generateTemplate();
    }
}
