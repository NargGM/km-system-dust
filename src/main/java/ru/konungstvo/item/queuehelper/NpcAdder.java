package ru.konungstvo.item.queuehelper;

import com.mongodb.client.MongoCursor;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import noppes.npcs.api.IWorld;
import noppes.npcs.api.NpcAPI;
import noppes.npcs.api.constants.EntityType;
import noppes.npcs.api.entity.IEntity;
import noppes.npcs.entity.EntityNPCInterface;
import org.bson.Document;
import ru.konungstvo.bridge.MongoBridge;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.commands.helpercommands.gm.npcadder.ChooseNpcCommand;
import ru.konungstvo.commands.helpercommands.gm.npcadder.NpcCommand;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.exceptions.DataException;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

public class NpcAdder extends Item {
    public NpcAdder() {
        super();
    }

    @Override
    @SideOnly(Side.SERVER)
    public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity) {
        String coords = "";
        coords += entity.posX + " " + entity.posY + " " + entity.posZ;

        if (entity instanceof EntityNPCInterface) {
            EntityNPCInterface npc = (EntityNPCInterface) entity;
            if (npc.wrappedNPC.getDisplay().getName().contains(" ")) {
                npc.wrappedNPC.getDisplay().setName(npc.wrappedNPC.getDisplay().getName().replace(" ", "_"));
            }
        }

        IWorld iWorld = NpcAPI.Instance().getIWorld(entity.dimension);
        IEntity[] list = iWorld.getAllEntities(EntityType.NPC);
        int i = 0;
        for (IEntity iEntity : list) {
            if (iEntity.getName().equals(entity.getName())) i++;
            if (i > 1) {
                TextComponentString str = new TextComponentString("На сервере больше одного НПЦ с именем " + entity.getName() + "!");
                player.sendMessage(str);
                return false;
            }
        }

        if (DataHolder.inst().npcExist(entity.getName())) {
            int id = -1;
            if (DataHolder.inst().getPlayer(entity.getName()) != null) id = DataHolder.inst().getPlayer(entity.getName()).getAttachedCombatID();
            TextComponentString str = new TextComponentString("НПС с именем " + entity.getName() + " уже существует в системе!" + (id != -1 ? " Существующий НПС привязан к бою с id " + id + ", не забудьте закончить бой!" : ""));
            player.sendMessage(str);
            return false;
        }

        if (stack.hasDisplayName()) {
            boolean react = stack.getDisplayName().startsWith("§6");
            String stat = stack.getDisplayName().replaceAll("§.", "");
            if (getListOfNpcStats().contains(stat)) {
                if (react) DataHolder.inst().getPlayer(player.getName()).performCommand("/" + NpcCommand.NAME + " addr " + stat + " " + entity.getDisplayName().getUnformattedText() + " " + coords);
                else DataHolder.inst().getPlayer(player.getName()).performCommand("/" + NpcCommand.NAME + " add " + stat + " " + entity.getDisplayName().getUnformattedText() + " " + coords);
            } else {
                DataHolder.inst().getPlayer(player.getName()).sendMessage(new Message("Не найден скиллсет " + stat + "!", ChatColor.RED));
                String command = ChooseNpcCommand.NAME + " " + entity.getDisplayName().getUnformattedText() + " " + coords;
                DataHolder.inst().getPlayer(player.getName()).performCommand(command);
            }
        } else {
            String command = ChooseNpcCommand.NAME + " " + entity.getDisplayName().getUnformattedText() + " " + coords;
            DataHolder.inst().getPlayer(player.getName()).performCommand(command);
        }

        // КОСТЫЛЬ?
        try {
            DataHolder.inst().registerNpc(entity.getDisplayName().getUnformattedText(), (EntityNPCInterface) entity);
        } catch (DataException e) {
            TextComponentString str = new TextComponentString("НПС с именем " + entity.getName() + " уже существует в системе!");
            player.sendMessage(str);
            e.printStackTrace();
            return false;
        }

        if (entity instanceof EntityNPCInterface) {
            EntityNPCInterface npc = (EntityNPCInterface) entity;
            npc.wrappedNPC.getAi().setStandingType(1);
        }

        return true;
    }

    @Override
    public boolean canDestroyBlockInCreative(World world, BlockPos pos, ItemStack stack, EntityPlayer player)
    {
        return false;
    }

    private List<String> getListOfNpcStats() {
        ArrayList<String> result = new ArrayList<>();

        try (MongoCursor<Document> cursor = MongoBridge.INSTANCE.client.getDatabase("dustrpmongo").getCollection("charsheets").find(new Document("isnpc", true)).iterator()) {
            while (cursor.hasNext()) {
                Document document = cursor.next();
                if ((boolean) document.getOrDefault("isnpc", false)) {
                    result.add((String) document.getOrDefault("character", "NO CHARACTER NAME FOR SOME REASON WTF"));
                }
            }
        }

        Collections.sort(result);
        return result;
    }
}
