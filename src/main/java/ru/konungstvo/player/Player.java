package ru.konungstvo.player;

import com.dust.fairyneeds.buffstools.BuffStats;
import com.dust.fairyneeds.buffstools.buffCapability.BuffsProvider;
import com.dust.fairyneeds.buffstools.buffCapability.IBuffs;
import com.dust.fairyneeds.capability.unitedNeeds.IUnitedNeeds;
import com.dust.fairyneeds.capability.unitedNeeds.NeedStats;
import com.dust.fairyneeds.capability.unitedNeeds.UnitedNeedsProvider;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.FMLCommonHandler;
import noppes.mpm.ModelData;
import noppes.mpm.constants.EnumAnimation;
import noppes.npcs.api.NpcAPI;
import noppes.npcs.api.constants.AnimationType;
import noppes.npcs.entity.EntityNPCInterface;
import org.json.simple.parser.ParseException;
import ru.konungstvo.bridge.MessageGenerator;
import ru.konungstvo.bridge.ServerProxy;
import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.chat.range.Range;
import ru.konungstvo.combat.*;
import ru.konungstvo.combat.dice.modificator.PercentDice;
import ru.konungstvo.combat.duel.Defense;
import ru.konungstvo.combat.duel.DefenseType;
import ru.konungstvo.combat.duel.Duel;
import ru.konungstvo.combat.equipment.Fist;
import ru.konungstvo.combat.equipment.Shield;
import ru.konungstvo.combat.equipment.Armor;
import ru.konungstvo.combat.equipment.BodyPart;
import ru.konungstvo.combat.movement.ChargeHistory;
import ru.konungstvo.combat.movement.MovementHistory;
import ru.konungstvo.combat.movement.MovementTrait;
import ru.konungstvo.combat.movement.MovementBadge;
import ru.konungstvo.combat.reactions.Queue;
import ru.konungstvo.commands.executor.RemoveSubordinateExecutor;
import ru.konungstvo.commands.executor.ServerPurgeContainersExecutor;
import ru.konungstvo.commands.executor.SubExecutor;
import ru.konungstvo.commands.helpercommands.ClickContainer;
import ru.konungstvo.commands.helpercommands.modifiers.SyncModifiers;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.control.Helpers;
import ru.konungstvo.control.Logger;
import ru.konungstvo.exceptions.DataException;
import ru.konungstvo.kmrp_lore.command.BoltCommand;
import ru.konungstvo.kmrp_lore.helpers.WeaponDurabilityHandler;
import ru.konungstvo.kmrp_lore.helpers.WeaponTagsHandler;
import ru.konungstvo.player.wounds.WoundPyramid;
import ru.konungstvo.player.wounds.WoundType;

import java.io.Serializable;
import java.util.*;


/**
 * Represents player, stores information about their nick, armor, skills and so on.
 */
public class Player extends Observable implements Serializable {


    //basic stuff
    private String name;
    private String prefix;
    private String pseudonym;
    private String displayedName;
    private List<Permission> permissions;

    //combat stuff
    private List<Skill> skills;
    private HashMap<String, String> aliases;
    private Armor armor;
    private WoundPyramid woundPyramid;
    private int bloodloss;
    private int mentalDebuff;
    private int woundsMod;
    private List<Weapon> weapons;
    private Shield shield;
    private int previousDefenses; //number of times pla.yer defended themselves this turn
    private CombatState combatState;
    private boolean shifted;
    private boolean superFighter;
    private BlockPos location;
    private Skill autoDefense;
    private Range autoDefenseRange;
    private boolean hasAttacked;
    private int customAdvantage;
    private int weight;
    private int psychweight;

    private int maxpsychweight;
    private int optimalWeight;
    private int maxWeight;
    //private int percentDice;
    private PercentDice percentDice = new PercentDice();
    private boolean isFrozen;
    private int recoil;
    public boolean countered = false;
    public Player riposted = null;

    // movement stuff
    private MovementHistory movementHistory;
    private ChargeHistory chargeHistory;
    private MovementBadge movementBadge;
    private int remainingBlocks;

    //traits //TODO сделать отдельный класс
    private HashMap<String, Object> overridedTraits;
    private HashMap<Trait, Integer> traits;
    private MovementTrait movementTrait = MovementTrait.NORMAL;
    private int hardened;

    private boolean cantBeCharged;
    private boolean cantBleed;
    private boolean cantBeStunned;
    private boolean cantGetLimbInjury;
    private boolean cantBurn;
    private boolean apatic;


    private boolean small;
    private boolean big;

    private int crushingRes;
    private int slashingRes;
    private int piercingRes;

    private int modernFirearmRes;
    private int rangedOldRes;

    private int meleeRes;
    private int rangedRes;

    private boolean insensitive;
    private boolean collected;
    private boolean persistent;
    private boolean irresolute;
    private boolean nimble;
    private boolean sluggish;
    private boolean fragile;
    private boolean accurate;

    //
    private List<StatusEffect> statusEffects;

    //other stuff
    private Logger logger;
    private Range defaultRange;
    private ModifiersState modifiersState;

    //GM stuff
    private double rangePreference; //not to mistaken for DefaultRange! This variable is used for /setrange command.
    private int attachedCombatID;
    private Player walker;
    private Player walkedBy;
    private Player subordinate;

    private Player engagedWith;
    private Player engagedWithFading;
    private ArrayList<Player> engagedBy;
    private Duel opportunity;

    private Player defends;
    private ArrayList<Player> defendedBy;

    public int courage;
    public int maxcourage;
    public int size;

    private boolean defensiveStance;



    //constructors
    public Player(String nick) {
        this.name = nick;
        this.prefix = "";
        this.pseudonym = "";
        this.displayedName = nick;
        this.permissions = new ArrayList<>();

        this.skills = new ArrayList<>();
        this.aliases = new HashMap<>();
//        aliases.put("бег", "передвижение");
//        aliases.put("физическая сила", "сила");
//        aliases.put("борьба", "рукопашный бой");
        this.armor = new Armor("Броня");
        this.woundPyramid = WoundPyramid.createFromEndurance(0, 5);
        this.bloodloss = DataHolder.inst().getModifiers().getBloodloss(nick);
        this.mentalDebuff = DataHolder.inst().getModifiers().getMentalDebuff(nick);
        this.weapons = new ArrayList<>();
        this.weapons.add(new Weapon("рукопашный бой", 0));
        this.shield = null;
        this.previousDefenses = 0;
        this.combatState = CombatState.SHOULD_WAIT;
        this.shifted = false;
        this.superFighter = false;

        this.overridedTraits = new HashMap<String, Object>();
        this.traits = new HashMap<Trait, Integer>();
        this.movementTrait = MovementTrait.NORMAL;
        this.hardened = 0;
        this.cantBeCharged = false;
        this.cantBleed = false;
        this.cantBeStunned = false;
        this.cantGetLimbInjury = false;
        this.cantBurn = false;
        this.apatic = false;
        this.crushingRes = 0;
        this.piercingRes = 0;
        this.slashingRes = 0;
        this.modernFirearmRes = 0;
        this.rangedOldRes = 0;
        this.meleeRes = 0;
        this.rangedRes = 0;
        this.insensitive = false;
        this.collected = false;
        this.persistent = false;
        this.irresolute = false;
        this.nimble = false;
        this.small = false;
        this.big = false;
        this.sluggish = false;
        this.fragile = false;
        this.accurate = false;

        this.statusEffects = new ArrayList<>();

        this.remainingBlocks = -1;
        this.movementHistory = MovementHistory.NONE;
        this.chargeHistory = ChargeHistory.NONE;
        this.location = null;
        this.autoDefense = null;
        this.autoDefenseRange = Range.NORMAL_DIE;
        this.hasAttacked = false;
        this.isFrozen = false;
        this.recoil = 0;
        this.customAdvantage = 0;
        this.weight = 0;
        this.psychweight = 0;
        this.optimalWeight = 0;
        this.maxWeight = 0;

        this.modifiersState = new ModifiersState();

        this.logger = new Logger("Player");
        this.defaultRange = Range.NORMAL;
        this.rangePreference = 0;
        this.attachedCombatID = -1;
        this.walker = null;
        this.walkedBy = null;
        this.subordinate = null;
        this.engagedWith = null;
        this.engagedWithFading = null;
        this.engagedBy = new ArrayList<>();
        this.opportunity = null;
        this.defends = null;
        this.defendedBy = new ArrayList<>();
        this.courage = 0;
        this.maxcourage = 0;
        this.size = 0;
        this.defensiveStance = false;
        logger.debug("Created player " + name + " in state " + combatState);
    }

    //~~~~~~~~~~~~~~~~~Setters and getters

    public String getName() {
        return name;
    }

    public String getPseudonym() {
        return pseudonym;
    }

    public void setPseudonym(String pseudonym) {
        this.pseudonym = pseudonym.replaceAll("&([a-z0-9])", "§$1");;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        if (hasPermission(Permission.GM)) return ChatColor.PREFIX.get() + "[GM]" + ChatColor.NICK.get();
        if (hasPermission(Permission.CM)) return ChatColor.PREFIX.get() + "[" + ChatColor.DARK_AQUA.get() + "CM" + ChatColor.PREFIX.get() + "]" + ChatColor.NICK.get();
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getDisplayedName() {
        if (displayedName.equals(name)) return getPrefix() + name;
        return displayedName;
    }

    public boolean isOnline() {
        return FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUsername(name) != null;
    }

    public void setDisplayedName(String displayedName) {
        this.displayedName = displayedName;
    }

    public Range getDefaultRange() {
        return defaultRange;
    }

    public void setDefaultRange(Range defaultRange) {
        this.defaultRange = defaultRange;
    }

    public double getRangePreference() {
        return rangePreference;
    }

    public void setRangePreference(double rangePreference) {
        this.rangePreference = rangePreference;
        logger.debug("Setting rangePreference to " + rangePreference + " for " + getName());
    }

    public List<Weapon> getWeapons() {
        return weapons;
    }

    public void addWeapon(Weapon weapon) {
        weapons.remove(weapon);
        weapons.add(weapon);
    }

    public void addTrait(String trait, int value) {
        for (Trait traitType : Trait.values()) {
            if (traitType.toString().equals(trait)) {
                traits.put(traitType, value);
                return;
            }
        }
    }

    public int getTraitLevel(Trait trait) {
        if (!traits.containsKey(trait)) return 0;
        return traits.get(trait);
    }

    public boolean hasTrait(Trait trait) {
        return hasTraitLevel(trait, 1);
    }

    public boolean hasTraitLevel(Trait trait, int level) {
        if (!traits.containsKey(trait)) return false;
        return traits.get(trait) >= level;
    }

    public void overrideTrait(String trait) {
            switch (trait) {
                case "нечувствительность": //чек
                    overridedTraits.put("нечувствительность", true);
                    break;
                case "сосредоточенность": //чек
                    overridedTraits.put("сосредоточенность", true);
                    break;
                case "стойкость": //чек
                    overridedTraits.put("стойкость", true);
                    break;

                case "хрупкость": //чек
                    overridedTraits.put("хрупкость", true);
                    break;
                case "точность": //чек
                    overridedTraits.put("точность", true);
                    break;
                case "нерасторопность": //чек
                    overridedTraits.put("нерасторопность", true);
                    break;
                case "юркость": //чек
                    overridedTraits.put("юркость", true);
                    break;


                case "быстрота": //чек
                    overridedTraits.put("movement", MovementTrait.FAST);
                    break;
                case "сверхбыстрота": //чек
                    overridedTraits.put("movement", MovementTrait.SUPERFAST);
                    break;

                case "толстокожесть": //чек
                    overridedTraits.put("толстокожесть", 1);
                    break;
                case "толстокожесть+": //чек
                    overridedTraits.put("толстокожесть", 2);
                    break;
                case "толстокожесть++": //чек
                    overridedTraits.put("толстокожесть", 3);
                    break;

                case "невосприимчивость к натиску": //чек
                    overridedTraits.put("невосприимчивость к натиску", true);
                    break;
                case "невосприимчивость к кровотечению": //чек
                    overridedTraits.put("невосприимчивость к кровотечению", true);
                    break;
                case "невосприимчивость к оглушению": //чек
                    overridedTraits.put("невосприимчивость к оглушению", true);
                    break;
                case "невосприимчивость к повреждению конечностей": //чек
                    overridedTraits.put("невосприимчивость к повреждению конечностей", true);
                    break;
                case "невосприимчивость к горению": //чек
                    overridedTraits.put("невосприимчивость к горению", true);
                    break;
                case "невосприимчивость к ментальным ранам": //чек
                    overridedTraits.put("невосприимчивость к ментальным ранам", true);
                    break;

                case "крохотность": //чек
                    overridedTraits.put("крохотность", true);
                    break;
                case "гигантизм": //чек
                    overridedTraits.put("гигантизм", true);
                    break;

                case "сопротивляемость к дробящему": //чек
                    overridedTraits.put("сопротивляемость к дробящему", 1);
                    break;
                case "уязвимость к дробящему": //чек
                    overridedTraits.put("сопротивляемость к дробящему", -1);
                    break;
                case "сопротивляемость к колющему": //чек
                    overridedTraits.put("сопротивляемость к колющему", 1);
                    break;
                case "уязвимость к колющему": //чек
                    overridedTraits.put("сопротивляемость к колющему", -1);
                    break;
                case "сопротивляемость к режуще-рубящему": //чек
                    overridedTraits.put("сопротивляемость к режуще-рубящему", 1);
                    break;
                case "уязвимость к режуще-рубящему": //чек
                    overridedTraits.put("сопротивляемость к режуще-рубящему", -1);
                    break;
                case "сопротивляемость к современному огнестрельному": //чек
                    overridedTraits.put("сопротивляемость к современному огнестрельному", 1);
                    break;
                case "уязвимость к современному огнестрельному": //чек
                    overridedTraits.put("сопротивляемость к современному огнестрельному", -1);
                    break;
                case "сопротивляемость к устаревшему дальнему": //чек
                    overridedTraits.put("сопротивляемость к устаревшему дальнему", 1);
                    break;
                case "уязвимость к устаревшему дальнему": //чек
                    overridedTraits.put("сопротивляемость к устаревшему дальнему", -1);
                    break;
                case "сопротивляемость к ближним атакам": //чек
                    overridedTraits.put("сопротивляемость к ближним атакам", 1);
                    break;
                case "уязвимость к ближним атакам": //чек
                    overridedTraits.put("сопротивляемость к ближним атакам", -1);
                    break;
                case "сопротивляемость к дальним атакам": //чек
                    overridedTraits.put("сопротивляемость к дальним атакам", 1);
                    break;
                case "уязвимость к дальним атакам": //чек
                    overridedTraits.put("сопротивляемость к дальним атакам", -1);
                    break;
            }
    }

    public void clearEffects() {
        statusEffects = new ArrayList<>();
        overridedTraits = new HashMap<String, Object>();
        if (!DataHolder.inst().isNpc(name)) performCommand("/geteffects");
    }

    public void processStatusEffects(StatusEnd statusEnd) {
        System.out.println(statusEffects);
        List<StatusEffect> toRemove = new ArrayList<>();;
        for (StatusEffect statusEffect : statusEffects) {
            if(statusEffect.getStatusEnd() == statusEnd) {
                if (statusEffect.isJustActivated()) {
                    statusEffect.setJustActivated(false);
                } else if (statusEffect.processTurnsLeft() <= 0) {
                    toRemove.add(statusEffect);
                }
            }
        }
        for (StatusEffect statusEffect : toRemove) {
            if (statusEffect.getStatusType() == StatusType.TRAIT_EFFECT || statusEffect.getStatusType() == StatusType.MEDS) {
                if (statusEffect.getContext().contains(":")) {
                    for (String trait : statusEffect.getContext().split(":")) {
                        if (trait.contains("толстокожесть")) trait = "толстокожесть";
                        overridedTraits.remove(trait);
                    }
                } else {
                    if (statusEffect.getContext().contains("толстокожесть")) overridedTraits.remove("толстокожесть");
                    else overridedTraits.remove(statusEffect.getContext());
                }
            }
            if (statusEffect.getFollowUp() != null && statusEffect.getFollowUp().getTurnsLeft() > 0) statusEffects.add(statusEffect.getFollowUp());
            statusEffects.remove(statusEffect);
        }
        if (statusEnd.equals(StatusEnd.TURN_END)) processBuffFromNeeds();
        if (!DataHolder.inst().isNpc(name)) performCommand("/geteffects");
        System.out.println(statusEffects);
    }



    public void addStatusEffect(String name, StatusEnd statusEnd, int turnsLeft, StatusType statusType, String context) {
        StatusEffect statusEffect = new StatusEffect(name, statusEnd, turnsLeft, statusType, context, -666, null, false);
        statusEffects.add(statusEffect);
        if (statusEffect.getStatusType() == StatusType.TRAIT_EFFECT || statusEffect.getStatusType() == StatusType.MEDS) {
            if (statusEffect.getContext().contains(":")) {
                for (String trait : statusEffect.getContext().split(":")) {
                    overrideTrait(trait);
                }
            } else {
                overrideTrait(statusEffect.getContext());
            }
        }
        if (!DataHolder.inst().isNpc(name)) performCommand("/geteffects");
    }

    public void addStatusEffect(String name, StatusEnd statusEnd, int turnsLeft, StatusType statusType, String context, boolean justActivated) {
        StatusEffect statusEffect = new StatusEffect(name, statusEnd, turnsLeft, statusType, context, -666, null, justActivated);
        statusEffects.add(statusEffect);
        if (statusEffect.getStatusType() == StatusType.TRAIT_EFFECT || statusEffect.getStatusType() == StatusType.MEDS) {
            if (statusEffect.getContext().contains(":")) {
                for (String trait : statusEffect.getContext().split(":")) {
                    overrideTrait(trait);
                }
            } else {
                overrideTrait(statusEffect.getContext());
            }
        }
        if (!DataHolder.inst().isNpc(name)) performCommand("/geteffects");
    }

    public void addStatusEffect(String name, StatusEnd statusEnd, int turnsLeft, StatusType statusType, int contextNumber) {
        StatusEffect statusEffect = new StatusEffect(name, statusEnd, turnsLeft, statusType, "", contextNumber, null, false);
        statusEffects.add(statusEffect);
        if (!DataHolder.inst().isNpc(name)) performCommand("/geteffects");
    }

    public void addStatusEffect(String name, StatusEnd statusEnd, int turnsLeft, StatusType statusType, int contextNumber, boolean justActivated) {
        StatusEffect statusEffect = new StatusEffect(name, statusEnd, turnsLeft, statusType, "", contextNumber, null, justActivated);
        statusEffects.add(statusEffect);
        if (!DataHolder.inst().isNpc(name)) performCommand("/geteffects");
    }

    public void addStatusEffect(String name, StatusEnd statusEnd, int turnsLeft, StatusType statusType) {
        StatusEffect statusEffect = new StatusEffect(name, statusEnd, turnsLeft, statusType);
        statusEffects.add(statusEffect);
        if (!DataHolder.inst().isNpc(name)) performCommand("/geteffects");
    }

    public boolean hasStatusEffects() {
        return statusEffects.size() > 0;
    }


    public boolean hasStatusEffect(StatusType statusType) {
        for (StatusEffect statusEffect : statusEffects) {
            if (statusEffect.getStatusType() == statusType) return true;
        }
        return false;
    }

    public boolean hasStatusEffect(String name) {
        for (StatusEffect statusEffect : statusEffects) {
            if (statusEffect.getName().equals(name)) return true;
        }
        return false;
    }

    public void addStatusEffect(String name, StatusEnd statusEnd, int turnsLeft, StatusType statusType, String context, int contextNumber, StatusEffect followUp) {
        StatusEffect statusEffect = new StatusEffect(name, statusEnd, turnsLeft, statusType, context, contextNumber, followUp, false);
        statusEffects.add(statusEffect);
        if (statusEffect.getStatusType() == StatusType.TRAIT_EFFECT || statusEffect.getStatusType() == StatusType.MEDS) {
            if (statusEffect.getContext().contains(":")) {
                for (String trait : statusEffect.getContext().split(":")) {
                    overrideTrait(trait);
                }
            } else {
                overrideTrait(statusEffect.getContext());
            }
        }
        if (!DataHolder.inst().isNpc(name)) performCommand("/geteffects");
    }

    public void addStatusEffect(String name, StatusEnd statusEnd, int turnsLeft, StatusType statusType, String context, int contextNumber, StatusEffect followUp, boolean justActivated) {
        StatusEffect statusEffect = new StatusEffect(name, statusEnd, turnsLeft, statusType, context, contextNumber, followUp, justActivated);
        statusEffects.add(statusEffect);
        if (statusEffect.getStatusType() == StatusType.TRAIT_EFFECT || statusEffect.getStatusType() == StatusType.MEDS) {
            if (statusEffect.getContext().contains(":")) {
                for (String trait : statusEffect.getContext().split(":")) {
                    overrideTrait(trait);
                }
            } else {
                overrideTrait(statusEffect.getContext());
            }
        }
        if (!DataHolder.inst().isNpc(name)) performCommand("/geteffects");
    }

    public boolean hasCourage() {
        for (StatusEffect statusEffect : statusEffects) {
            if (statusEffect.getStatusType() == StatusType.CONCENTRATED) {
                return true;
            }
        }
        return false;
    }

    public int getPreparingDebuff() {
        int debuff = 0;
        for (StatusEffect statusEffect : statusEffects) {
            if (statusEffect.getStatusType() == StatusType.PREPARING) {
                if (statusEffect.getContextNumber() != -666) debuff += statusEffect.getContextNumber();
            }
        }
        return debuff;
    }

    public String translate(String skill) {
        switch (skill) {
            case "strength":
                return "сила";
            case "dexterity":
                return "ловкость";
            case "psyche":
                return "психика";
            case "luck":
                return "удача";
            case "athletics":
                return "атлетика";
            case "stamina":
                return "выносливость";
            case "physical_defense":
                return "физическая защита";
            case "melee_furious":
                return "ближнее яростное";
            case "ranged_furious":
                return "дальнее яростное";
            case "stealth":
                return "скрытность";
            case "acrobatics":
                return "акробатика";
            case "melee_sneaky":
                return "ближнее подлое";
            case "ranged_sneaky":
                return "дальнее подлое";
            case "identification":
                return "опознание";
            case "psychic_stealth":
                return "психическая скрытность";
            case "psychic_defense":
                return "психическая защита";
            case "melee_arcane":
                return "ближнее тайное";
            case "ranged_arcane":
                return "дальнее тайное";
            case "reaction":
                return "реакция";
            case "perception":
                return "внимательность";
            case "melee_brisk":
                return "ближнее бойкое";
            case "ranged_brisk":
                return "дальнее бойкое";
            default:
                return "";
        }
    }

    public int getBuffFromNeeds(String skill) {
        int buff = 0;
        IBuffs buffsCap = (IBuffs)ServerProxy.getForgePlayer(name).getCapability(BuffsProvider.BUFFS_CAP, (EnumFacing)null);
        assert buffsCap != null;
        List<BuffStats> buffs = buffsCap.getListAllBuffs();
        Iterator ita = buffs.iterator();

        while(ita.hasNext()) {
            BuffStats buffStat = (BuffStats) ita.next();
            if (buffStat.getDuration() < 1) continue;
            System.out.println(translate(buffStat.getSkill().getSkillName()) + " " + buffStat.getPoints() + " " + buffStat.getDuration());
            if (translate(buffStat.getSkill().getSkillName()).equals(skill)) {
                buff += (int) ((buffStat.getPoints() - 1.0) * 100);
            }
        }

        IUnitedNeeds needsCap = (IUnitedNeeds)ServerProxy.getForgePlayer(name).getCapability(UnitedNeedsProvider.UNITEDNEEDS_CAP, (EnumFacing)null);
        assert needsCap != null;
        List<NeedStats> needs = needsCap.getAllNeeds();

        Iterator ita2 = buffs.iterator();

        while(ita2.hasNext()) {
            NeedStats needStat = (NeedStats) ita.next();
            buff += (int) ((needStat.getEffect()) * 100);

        }

        return buff;
    }

    public void processBuffFromNeeds() {
        IBuffs buffsCap = (IBuffs)ServerProxy.getForgePlayer(name).getCapability(BuffsProvider.BUFFS_CAP, (EnumFacing)null);
        assert buffsCap != null;
        List<BuffStats> buffs = buffsCap.getListAllBuffs();
        Iterator ita = buffs.iterator();

        while(ita.hasNext()) {
            BuffStats buffStat = (BuffStats) ita.next();
            if (buffStat.getDuration() < 1 || !buffStat.getIsDurationInTurns()) continue;
            System.out.println(translate(buffStat.getSkill().getSkillName()) + " " + buffStat.getPoints());
            buffStat.setDuration(buffStat.getDuration()-1);
        }
    }

    public boolean hasCourage(String skill) {
        for (StatusEffect statusEffect : statusEffects) {
            if (statusEffect.getStatusType() == StatusType.COURAGE) {
                if (statusEffect.getContext().equalsIgnoreCase(skill)) return true;
            }
        }
        return false;
    }

    public boolean hasPrepared(String spell) {
        for (StatusEffect statusEffect : statusEffects) {
            if (statusEffect.getStatusType() == StatusType.PREPARED) {
                if (statusEffect.getContext().equalsIgnoreCase(spell)) return true;
            }
        }
        return false;
    }

    public boolean onCooldown(String spell) {
        for (StatusEffect statusEffect : statusEffects) {
            if (statusEffect.getStatusType() == StatusType.COOLDOWN) {
                if (statusEffect.getContext().equalsIgnoreCase(spell)) return true;
            }
        }
        return false;
    }

    public int getStrongestBleeding() {
        int bleeding = 0;
        for (StatusEffect statusEffect : statusEffects) {
            if (statusEffect.getStatusType() == StatusType.BLEEDING) {
                bleeding = Math.max(bleeding, statusEffect.getContextNumber());
            }
        }
        return bleeding;
    }

    public int getRelentless() {
        int bleeding = 0;
        for (StatusEffect statusEffect : statusEffects) {
            if (statusEffect.getStatusType() == StatusType.RELENTLESS) {
                bleeding = Math.max(bleeding, statusEffect.getContextNumber());
            }
        }
        return bleeding;
    }

    public int getRelentlessPsych() {
        int bleeding = 0;
        for (StatusEffect statusEffect : statusEffects) {
            if (statusEffect.getStatusType() == StatusType.RELENTLESS_PSYCH) {
                bleeding = Math.max(bleeding, statusEffect.getContextNumber());
            }
        }
        return bleeding;
    }

    public int getMagicShield() {
        for (StatusEffect statusEffect : statusEffects) {
            if (statusEffect.getStatusType() == StatusType.SHIELD) {
                return statusEffect.getContextNumber();
            }
        }
        return 0;
    }

    public int getWeakened() {
        int mod = 0;
        for (StatusEffect statusEffect : statusEffects) {
            if (statusEffect.getStatusType() == StatusType.HANGOVER || statusEffect.getStatusType() == StatusType.WEAKENED) {

                mod -= Math.abs(statusEffect.getContextNumber());
            }
        }
        return mod;
    }

    public int getBuff(String skill) {
        int mod = 0;
        for (StatusEffect statusEffect : statusEffects) {
            if (statusEffect.getStatusType() == StatusType.MEDS2 || statusEffect.getStatusType() == StatusType.BUFF) {
                if (statusEffect.getContext().equals("")) mod += statusEffect.getContextNumber();
                else {
                    for (String buff : statusEffect.getContext().split(":")) {
                        if (skill.equalsIgnoreCase(buff)) {
                            mod += statusEffect.getContextNumber();
                            break;
                        } else if (buff.contains("=") && skill.equalsIgnoreCase(buff.split("=")[0])) {
                            mod += Integer.parseInt(buff.split("=")[1]);
                            break;
                        }
                    }
                }
            }
        }
        return mod;
    }

    public void adjustMagicShield(int damage) {
        int howMuchBlocked = 0;
        List<StatusEffect> toRemove = new ArrayList<>();
        for (StatusEffect statusEffect : statusEffects) {
            if (statusEffect.getStatusType() == StatusType.SHIELD) {
                howMuchBlocked = Math.min(damage, statusEffect.getContextNumber());
                statusEffect.setContextNumber(statusEffect.getContextNumber() - howMuchBlocked);
                if (statusEffect.getContextNumber() <= 0) toRemove.add(statusEffect);
            }
        }

        for (StatusEffect st : toRemove) {
            statusEffects.remove(st);
        }

        if (howMuchBlocked != 0) {
            if (!DataHolder.inst().isNpc(name)) performCommand("/geteffects");
        }
    }

    public void removeCourage(String skill) {
        for (StatusEffect statusEffect : statusEffects) {
            if (statusEffect.getStatusType() == StatusType.COURAGE) {
                if (statusEffect.getContext().equalsIgnoreCase(skill)) {
                    statusEffects.remove(statusEffect);
                    if (!DataHolder.inst().isNpc(name)) performCommand("/geteffects");
                    return;
                }
            }
        }
    }

    public void removeStatusEffect(StatusType statusType) {
        List<StatusEffect> toRemove = new ArrayList<>();;
        for (StatusEffect statusEffect : statusEffects) {
            if(statusEffect.getStatusType() == statusType) {
                    toRemove.add(statusEffect);
            }
        }
        for (StatusEffect statusEffect : toRemove) {
            if (statusEffect.getStatusType() == StatusType.TRAIT_EFFECT || statusEffect.getStatusType() == StatusType.MEDS) {
                if (statusEffect.getContext().contains(":")) {
                    for (String trait : statusEffect.getContext().split(":")) {
                        overridedTraits.remove(trait);
                    }
                } else {
                    overridedTraits.remove(statusEffect.getContext());
                }
            }
            //if (statusEffect.getFollowUp() != null) statusEffects.add(statusEffect.getFollowUp());
            statusEffects.remove(statusEffect);
        }
        if (!DataHolder.inst().isNpc(name)) performCommand("/geteffects");
    }

    public String getEffectMessage(boolean needString) {
        Message message = new Message("", ChatColor.COMBAT);
        boolean worthLooking = false;
        for (StatusType statusType : StatusType.values()) {
            MessageComponent strong = null;
            StatusEffect strongest = null;
            int turns = 0;
            int contextNumber = -666;
            switch (statusType) {
                case BLEEDING:
                    strong = null;
                        for (StatusEffect statusEffect : statusEffects) {
                            if (statusEffect.getStatusType() != statusType) continue;
                            if (statusEffect.getContextNumber() > contextNumber) {
                                strongest = statusEffect;
                                contextNumber = statusEffect.getContextNumber();
                            }
                            if (strongest != null) {
                                strong = new MessageComponent(Character.toUpperCase(strongest.getName().charAt(0)) + strongest.getName().substring(1) + " " + strongest.getTurnsLeft() + strongest.getStatusEnd().toString() + "", strongest.getStatusType().color());
                                worthLooking = true;
                                StringBuilder hover = new StringBuilder("");
                                if (strongest.getContextNumber() != -666)
                                    hover.append("§l").append(strongest.getContextNumber()).append("% в ход");
                                for (StatusEffect statusEffect1 : statusEffects) {
                                    if (statusEffect1.getStatusType() != statusType) continue;
                                    if (statusEffect1 == strongest) continue;
                                    hover.append("\n").append(statusEffect1.getName()).append(" ").append(statusEffect1.getTurnsLeft()).append(statusEffect1.getStatusEnd().toString()).append((statusEffect1.getContextNumber() == -666 ? "" : " (" + statusEffect1.getContextNumber() + "%)"));
                                }
                                String ccc = hover.toString();
                                if (!ccc.isEmpty()) {
                                    strong.setHoverText(ccc.trim().trim(), strongest.getStatusType().color().get());
                                    strong.setUnderlined(true);
                                }
                            }
                        }
                    if (strong != null) {
                        message.addComponent(strong);
                        message.addComponent(new MessageComponent(" "));
                    }
                        break;
                case SHIELD:
                    strong = null;
                    for (StatusEffect statusEffect : statusEffects) {
                        if (statusEffect.getStatusType() != statusType) continue;
                        if (statusEffect.getContextNumber() > contextNumber) {
                            strongest = statusEffect;
                            contextNumber = statusEffect.getContextNumber();
                        }
                        if (strongest != null) {
                            strong = new MessageComponent(Character.toUpperCase(strongest.getName().charAt(0)) + strongest.getName().substring(1) + " " + strongest.getTurnsLeft() + strongest.getStatusEnd().toString() + "", strongest.getStatusType().color());
                            worthLooking = true;
                            StringBuilder hover = new StringBuilder("");
                            if (strongest.getContextNumber() != -666)
                                hover.append("§l").append(strongest.getContextNumber()).append(" ед. защиты");
                            for (StatusEffect statusEffect1 : statusEffects) {
                                if (statusEffect1.getStatusType() != statusType) continue;
                                if (statusEffect1 == strongest) continue;
                                hover.append("\n").append(statusEffect1.getName()).append(" ").append(statusEffect1.getTurnsLeft()).append(statusEffect1.getStatusEnd().toString()).append((statusEffect1.getContextNumber() == -666 ? "" : " (" + statusEffect1.getContextNumber() + "%)"));
                            }
                            String ccc = hover.toString();
                            if (!ccc.isEmpty()) {
                                strong.setHoverText(ccc.trim().trim(), strongest.getStatusType().color().get());
                                strong.setUnderlined(true);
                            }
                        }
                    }
                    if (strong != null) {
                        message.addComponent(strong);
                        message.addComponent(new MessageComponent(" "));
                    }
                    break;
                case MEDS2:
                case BUFF:
                    strong = null;
                    for (StatusEffect statusEffect : statusEffects) {
                        if (statusEffect.getStatusType() != statusType) continue;
                        if (statusEffect.getTurnsLeft() > turns) {
                            strongest = statusEffect;
                            turns = statusEffect.getTurnsLeft();
                        }
                        if (strongest != null) {
                            strong = new MessageComponent(Character.toUpperCase(strongest.getName().charAt(0)) + strongest.getName().substring(1) + " " + strongest.getTurnsLeft() + strongest.getStatusEnd().toString() + "", strongest.getStatusType().color());
                            worthLooking = true;
                            StringBuilder hover = new StringBuilder("");
                            for (String skill : strongest.getContext().split(";")) {
                                hover.append(skill.replace("="," ")).append("\n");
                            }
                            hover.append("§l").append(strongest.getContextNumber()).append(" (бафф для нормально)");
                            for (StatusEffect statusEffect1 : statusEffects) {
                                if (statusEffect1.getStatusType() != statusType) continue;
                                if (statusEffect1 == strongest) continue;
                                hover.append("\n").append(statusEffect1.getName()).append(" ").append(statusEffect1.getTurnsLeft()).append(statusEffect1.getStatusEnd().toString()).append((statusEffect1.getContext().isEmpty() ? "" : " (" + statusEffect1.getContext() + ")")).append(statusEffect1.getContextNumber() == -666 ? "" : " (" + statusEffect1.getContextNumber() + ")");
                            }
                            String ccc = hover.toString();
                            if (!ccc.isEmpty()) {
                                strong.setHoverText(ccc.trim().trim(), strongest.getStatusType().color().get());
                                strong.setUnderlined(true);
                            }
                        }
                    }
                    if (strong != null) {
                        message.addComponent(strong);
                        message.addComponent(new MessageComponent(" "));
                    }
                        break;

                    default:
                            for (StatusEffect statusEffect : statusEffects) {
                                if (statusEffect.getStatusType() != statusType) continue;
                                if (statusEffect.getTurnsLeft() > turns) {
                                    strongest = statusEffect;
                                    turns = statusEffect.getTurnsLeft();
                                }
                                if (strongest != null) {
                                    strong = new MessageComponent(Character.toUpperCase(strongest.getName().charAt(0)) + strongest.getName().substring(1) + " " + strongest.getTurnsLeft() + strongest.getStatusEnd().toString() + "", strongest.getStatusType().color());
                                    worthLooking = true;
                                    StringBuilder hover = new StringBuilder("");
                                    if (!strongest.getContext().isEmpty()) {
                                        hover.append("§l").append(strongest.getContext()).append(strongest.getContextNumber() == -666 ? "" : " (" + strongest.getContextNumber() + ")");
                                    } else if (strongest.getContextNumber() != -666) {
                                        hover.append("§l").append(strongest.getContextNumber());
                                    }
                                    for (StatusEffect statusEffect1 : statusEffects) {
                                        if (statusEffect1.getStatusType() != statusType) continue;
                                        if (statusEffect1 == strongest) continue;
                                        hover.append("\n").append(statusEffect1.getName()).append(" ").append(statusEffect1.getTurnsLeft()).append(statusEffect1.getStatusEnd().toString()).append((statusEffect1.getContext().isEmpty() ? "" : " (" + statusEffect1.getContext() + ")")).append(statusEffect1.getContextNumber() == -666 ? "" : " (" + statusEffect1.getContextNumber() + ")");
                                    }
                                    String ccc = hover.toString();
                                    if (!ccc.isEmpty()) {
                                        strong.setHoverText(ccc.trim().trim(), strongest.getStatusType().color().get());
                                        strong.setUnderlined(true);
                                    }
                                }
                            }
                        if (strong != null) {
                            message.addComponent(strong);
                            message.addComponent(new MessageComponent(" "));
                        }
                }
            }

            if (engagedWith != null || engagedBy.size() > 0) {
                worthLooking = true;
                System.out.println("TEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEST");
                MessageComponent engaged = new MessageComponent((engagedBy.size() > 0 ? "[Связан боем]" : "[Связывает боем]"), ChatColor.COMBAT);
                engaged.setUnderlined(true);
                StringBuilder engagedList = new StringBuilder();
                if (engagedWith != null) engagedList.append("§2Связывает боем:\n").append("§a[").append(engagedWith.getName()).append("]\n");
                if (engagedWith != null && engagedBy.size() > 0) engagedList.append("\n");
                if (engagedBy.size() > 0) {
                    engagedList.append("§4Связан боем:\n");
                    for (Player engagedPl : engagedBy) {
                        engagedList.append("§c[").append(engagedPl.getName()).append("]\n");
                    }
                }
                engaged.setHoverText(engagedList.toString().trim(), TextFormatting.BLUE);
                message.addComponent(engaged);
                if (engagedWith != null && !needString) {
                    MessageComponent stop = new MessageComponent((" [x]"), ChatColor.COMBAT);
                    stop.setHoverText("Прекратить связывать боем [" + engagedWith.getName() + "]", TextFormatting.RED);
                    stop.setClickCommand("/triggeropportunity stop");
                    message.addComponent(stop);
                }
                message.addComponent(new MessageComponent(" "));
            }

            if (defends != null || defendedBy.size() > 0) {
                worthLooking = true;
                MessageComponent defended = new MessageComponent((defendedBy.size() > 0 ? "[Под защитой]" : "[Защищает]"), ChatColor.GRAY);
                defended.setUnderlined(true);
                StringBuilder defendedList = new StringBuilder();
                if (defends != null) defendedList.append("§2Защищает:\n").append("§a[").append(defends.getName()).append("]\n");
                if (defends != null && defendedBy.size() > 0) defendedList.append("\n");
                if (defendedBy.size() > 0) {
                    defendedList.append("§7Под защитой:\n");
                    for (Player engagedPl : defendedBy) {
                        defendedList.append("§c[").append(engagedPl.getName()).append("]\n");
                    }
                }
                defended.setHoverText(defendedList.toString().trim(), TextFormatting.BLUE);
                message.addComponent(defended);
                if (defends != null && !needString) {
                    MessageComponent stop = new MessageComponent((" [x]"), ChatColor.COMBAT);
                    stop.setHoverText("Прекратить защищать [" + defends.getName() + "]", TextFormatting.RED);
                    stop.setClickCommand("/setfeint cleardefends");
                    message.addComponent(stop);
                }
                message.addComponent(new MessageComponent(" "));
            }

            if (defensiveStance) {
                worthLooking = true;
                MessageComponent def = new MessageComponent(("[Глухая оборона]"), ChatColor.BLUE);
                message.addComponent(def);
                MessageComponent stop = new MessageComponent((" [x]"), ChatColor.BLUE);
                stop.setHoverText("Прервать глухую оборону", TextFormatting.RED);
                stop.setClickCommand("/defensivestance turnoff");
                message.addComponent(stop);
                message.addComponent(new MessageComponent(" "));
            }

            if (Math.abs(recoil) > 0) {
                worthLooking = true;
                MessageComponent rec = new MessageComponent(("[Отдача -" + Math.abs(recoil) + "]"), ChatColor.DARK_RED);
                message.addComponent(rec);
                message.addComponent(new MessageComponent(" "));
            }

            if (!worthLooking) return "";
            if (needString) return message.toString();
            return ITextComponent.Serializer.componentToJson(MessageGenerator.generate(message));

    }



    public int getAttachedCombatID() {
        return attachedCombatID;
    }


    public void setAttachedCombatID(int attachedCombatID) {
        this.attachedCombatID = attachedCombatID;
    }

    public int getPreviousDefenses() {
        if (isSuperFighter()) return 0;
        return previousDefenses;
    }

    public void setPreviousDefenses(int previousDefenses) {
        System.out.println("Prev defenses for " + name + " set to " + previousDefenses);
        this.previousDefenses = previousDefenses;
    }

    public void incrementPreviousDefenses() {
        this.previousDefenses++;
    }

    public CombatState getCombatState() {
        return this.combatState;
    }

    public void setCombatState(CombatState combatState) {
        logger.debug(name + "'s combat state changed from " + this.combatState + " to " + combatState);
        //MinecraftServer cm = FMLCommonHandler.instance().getMinecraftServerInstance();
        // cm.getCommandManager().executeCommand(cm,"/freezeplayer " + this.getName());
        //Combat c = DataHolder.inst().getCombatForPlayer(this.getName());
        //if (c != null && !DataHolder.inst().isNpc(this.getName()))
        //DataHolder.inst().getMasterForCombat(c.getId()).performCommand("/freezeplayer " + this.getName());

        // TODO: doesn't really belong here?
        if (combatState == CombatState.ACTED) {
            this.sendMessage(new Message("Твой ход окончен.", ChatColor.COMBAT));
        }
        this.combatState = combatState;
    }

    public Shield getShield() {
        return shield;
    }

    public void setShield(Shield shield) {
        this.shield = shield;
    }

    public boolean hasShifted() {
        return shifted;
    }

    public void setShifted(boolean shifted) {
        this.shifted = shifted;
    }

    public HashMap<String, String> getAliases() {
        return aliases;
    }


    public BlockPos getLocation() {
        if (location == null) {
            if(DataHolder.inst().isNpc(name)) return DataHolder.inst().getNpcEntity(name).getPosition();
            return ServerProxy.getForgePlayer(name).getPosition();
        }
        return location;
    }

    public void setLocation(BlockPos location) {
        this.location = location;
    }

    public Skill getAutoDefense() {
        return autoDefense;
    }

    public void setAutoDefense(String skillName) {
        this.autoDefense = getSkill(skillName);
    }

    public Range getAutoDefenseRange() {
        return autoDefenseRange;
    }

    public void setAutoDefenseRange(Range autoDefenseRange) {
        if (!autoDefenseRange.toString().contains("DIE")) {
            autoDefenseRange = Helpers.convertToDieRange(autoDefenseRange);
        }
        this.autoDefenseRange = autoDefenseRange;
    }

    public boolean hasAttacked() {
        return hasAttacked;
    }

    public void setHasAttacked(boolean hasAttacked) {
        this.hasAttacked = hasAttacked;
    }

    //~~~~~~~~~~~~~~~~~Permission management
    public void addPermission(Permission permission) {
        if (permissions.contains(permission)) return;
        this.permissions.add(permission);
    }

    public void removePermission(Permission permission) {
        this.permissions.remove(permission);
    }

    public boolean hasPermission(Permission permission) {
        //   return this.permissions.contains(permission);
        return ServerProxy.hasPermission(this.name, permission.get());
    }

    //~~~~~~~~~~~~~~~~~Armor management
    public void setArmor(Armor armor) {
        this.armor = armor;
    }

    public void purgeArmor() {
        this.armor = new Armor("PlayerArmor");
    }

    public void addToArmor(NBTTagCompound nbtTagCompound) throws ParseException {
        if (this.armor == null) System.out.println("Armor is null! Why?");
        this.armor.fillFromNBT(nbtTagCompound);
    }

    public int getArmorMod() {
        if (armor == null) return 0;
        int mod = armor.getMod();
        // CHECK FOR STRENGTH
        int req = armor.getStrengthRequirement();
        if (req > 0) {
            if (getSkillLevelAsInt("сила") < req) {
                mod += getSkillLevelAsInt("сила") - req;
            }
        }
        return mod;
    }

    public Armor getArmor() {
        return this.armor;
    }

    @Deprecated
    public int getDefenseMod() {
        return getDefenseModFor(BodyPart.THORAX);
        //if (armor == null) return 0;
        //return armor.getDefense();
    }

    public boolean isHardened() {
        if (overridedTraits.containsKey("толстокожесть")) return (int) overridedTraits.get("толстокожесть") != 0;
        return hardened != 0;
    }

    public void setHardened(int hardened) {
        this.hardened = hardened;
    }

    public int getHardened() {
        if (overridedTraits.containsKey("толстокожесть")) return (int) overridedTraits.get("толстокожесть");
        return hardened;
    }

    public boolean cantBeCharged() {
        if (overridedTraits.containsKey("невосприимчивость к натиску")) return (boolean) overridedTraits.get("невосприимчивость к натиску");
        return cantBeCharged;
    }

    public void setCantBeCharged(boolean cantBeCharged) {
        this.cantBeCharged = cantBeCharged;
    }

    public boolean cantBleed() {
        if (overridedTraits.containsKey("невосприимчивость к кровотечению")) return (boolean) overridedTraits.get("невосприимчивость к кровотечению");
        return cantBleed;
    }

    public void setCantBleed(boolean cantBleed) { this.cantBleed = cantBleed; }

    public boolean cantBeStunned() {
        if (overridedTraits.containsKey("невосприимчивость к оглушению")) return (boolean) overridedTraits.get("невосприимчивость к оглушению");
        return cantBeStunned;
    }

    public void setCantBeStunned(boolean cantBeStunned) { this.cantBeStunned = cantBeStunned; }

    public boolean cantGetLimbInjury() {
        if (overridedTraits.containsKey("невосприимчивость к повреждению конечностей")) return (boolean) overridedTraits.get("невосприимчивость к повреждению конечностей");
        return cantGetLimbInjury;
    }

    public void setCantGetLimbInjury(boolean cantGetLimbInjury) { this.cantGetLimbInjury = cantGetLimbInjury; }

    public boolean cantBurn() {
        if (overridedTraits.containsKey("невосприимчивость к горению")) return (boolean) overridedTraits.get("невосприимчивость к горению");
        return cantBurn;
    }

    public void setCantBurn(boolean cantBurn) { this.cantBurn = cantBurn; }


    public boolean isApatic() {
        if (overridedTraits.containsKey("невосприимчивость к ментальным ранам")) return (boolean) overridedTraits.get("невосприимчивость к ментальным ранам");
        return apatic;
    }
    public void setApatic(boolean cantBurn) { this.apatic = apatic; }

    public boolean isSmall() {
        if (overridedTraits.containsKey("крохотность")) return (boolean) overridedTraits.get("крохотность");
        return small;
    }

    public void setSmall(boolean small) {
        this.small = small;
    }

    public boolean isBig() {
        if (overridedTraits.containsKey("гигантизм")) return (boolean) overridedTraits.get("гигантизм");
        return big;
    }

    public void setBig(boolean big) {
        this.big = big;
    }

    public void setCrushingRes(int crushingRes) {
        this.crushingRes = crushingRes;
    }

    public int getCrushingRes() {
        if (overridedTraits.containsKey("сопротивляемость к дробящему")) return (int) overridedTraits.get("сопротивляемость к дробящему");
        return crushingRes;
    }

    public void setSlashingRes(int slashingRes) {
        this.slashingRes = slashingRes;
    }

    public int getSlashingRes() {
        if (overridedTraits.containsKey("сопротивляемость к режуще-рубящему")) return (int) overridedTraits.get("сопротивляемость к режуще-рубящему");
        return slashingRes;
    }

    public void setPiercingRes(int piercingRes) {
        this.piercingRes = piercingRes;
    }

    public int getPiercingRes() {
        if (overridedTraits.containsKey("сопротивляемость к колющему")) return (int) overridedTraits.get("сопротивляемость к колющему");
        return piercingRes;
    }

    public void setModernFirearmRes(int modernFirearmRes) {
        this.modernFirearmRes = modernFirearmRes;
    }

    public int getModernFirearmRes() {
        if (overridedTraits.containsKey("сопротивляемость к современному огнестрельному")) return (int) overridedTraits.get("сопротивляемость к современному огнестрельному");
        return modernFirearmRes;
    }

    public void setRangedOldRes(int rangedOldRes) {
        this.rangedOldRes = rangedOldRes;
    }

    public int getRangedOldRes() {
        if (overridedTraits.containsKey("сопротивляемость к устаревшему дальнему")) return (int) overridedTraits.get("сопротивляемость к устаревшему дальнему");
        return rangedOldRes;
    }

    public void setMeleeRes(int meleeRes) {
        this.meleeRes = meleeRes;
    }

    public int getMeleeRes() {
        if (overridedTraits.containsKey("сопротивляемость к ближним атакам")) return (int) overridedTraits.get("сопротивляемость к ближним атакам");
        return meleeRes;
    }

    public void setRangedRes(int rangedRes) {
        this.rangedRes = rangedRes;
    }

    public int getRangedRes() {
        if (overridedTraits.containsKey("сопротивляемость к дальним атакам")) return (int) overridedTraits.get("сопротивляемость к дальним атакам");
        return rangedRes;
    }

    public boolean isCollected() {
        if (overridedTraits.containsKey("сосредоточенность")) return (boolean) overridedTraits.get("сосредоточенность");
        return collected;
    }

    public void setCollected(boolean collected) {
        this.collected = collected;
    }

    public boolean isPersistent() {
        if (overridedTraits.containsKey("стойкость")) return (boolean) overridedTraits.get("стойкость");
        return persistent;
    }

    public void setPersistent(boolean persistent) {
        this.persistent = persistent;
    }

    public boolean isIrresolute() {
        if (overridedTraits.containsKey("нерешительность")) return (boolean) overridedTraits.get("нерешительность");
        return irresolute;
    }

    public void setIrresolute(boolean irresolute) {
        this.irresolute = irresolute;
    }


    public boolean isNimble() {
        if (overridedTraits.containsKey("юркость")) return (boolean) overridedTraits.get("юркость");
        return nimble;
    }

    public boolean isSluggish() {
        if (overridedTraits.containsKey("нерасторопность")) return (boolean) overridedTraits.get("нерасторопность");
        return sluggish;
    }

    public boolean isFragile() {
        if (overridedTraits.containsKey("хрупкость")) return (boolean) overridedTraits.get("хрупкость");
        return fragile;
    }

    public void setNimble(boolean nimble) {
        this.nimble = nimble;
    }

    public void setSluggish(boolean sluggish) {
        this.sluggish = sluggish;
    }

    public void setFragile(boolean fragile) {
        this.fragile = fragile;
    }

    public void setAccurate(boolean accurate) {
        this.accurate = accurate;
    }

    public boolean isAccurate() {
        if (overridedTraits.containsKey("точность")) return (boolean) overridedTraits.get("точность");
        return accurate;
    }

    public boolean isInsensitive() {
        if (overridedTraits.containsKey("нечувствительность")) return (boolean) overridedTraits.get("нечувствительность");
        return insensitive;
    }

    public void setInsensitive(boolean insensitive) {
        this.insensitive = insensitive;
    }

    public void clearTraits() {
        this.movementTrait = MovementTrait.NORMAL;
        this.hardened = 0;
        this.cantBeCharged = false;
        this.cantBleed = false;
        this.cantBeStunned = false;
        this.cantGetLimbInjury = false;
        this.cantBurn = false;
        this.crushingRes = 0;
        this.piercingRes = 0;
        this.slashingRes = 0;
        this.modernFirearmRes = 0;
        this.rangedOldRes = 0;
        this.meleeRes = 0;
        this.rangedRes = 0;
        this.insensitive = false;
        this.collected = false;
        this.persistent = false;
        this.nimble = false;
        this.small = false;
        this.big = false;
        this.sluggish = false;
        this.fragile = false;
        this.accurate = false;
    }

    public String getTraitsString() {
        System.out.println("TEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEST");
        StringBuilder lol = new StringBuilder();
        if (isInsensitive()) lol.append("Нечувствительность\n");
        if (isCollected()) lol.append("Сосредоточенность\n");
        if (isPersistent()) lol.append("Стойкость\n");
        if (isNimble()) lol.append("Юркость\n");
        if (isSluggish()) lol.append("Нерасторопность\n");
        if (isFragile()) lol.append("Хрупкость\n");
        if (isSmall()) lol.append("Крохотность\n");
        System.out.println("test " + big);
        System.out.println("test " + isBig());
        if (isBig()) lol.append("Гигантизм\n");
        if (getMovementTrait() != MovementTrait.NORMAL) {
            if (getMovementTrait() == MovementTrait.FAST) lol.append("Быстрота\n");
            if (getMovementTrait() == MovementTrait.SUPERFAST) lol.append("Сверхбыстрота\n");
        }
        if (isAccurate()) lol.append("Точность\n");
        switch (getHardened()) {
            case 1:
                lol.append("Толстокожесть\n");
                break;
            case 2:
                lol.append("Толстокожесть+\n");
                break;
            case 3:
                lol.append("Толстокожесть++\n");
                break;
        }
        if (cantBeCharged()) lol.append("Невосприимчивость к натиску\n");
        if (cantBleed()) lol.append("Невосприимчивость к кровотечению\n");
        if (cantBeStunned()) lol.append("Невосприимчивость к оглушению\n");
        if (cantGetLimbInjury()) lol.append("Невосприимчивость к повреждению конечностей\n");
        if (cantBurn()) lol.append("Невосприимчивость к горению\n");
        if (getCrushingRes() != 0) lol.append("Сопротивляемость к дробящему ").append(getCrushingRes()).append("\n");
        if (getPiercingRes() != 0) lol.append("Сопротивляемость к колющему ").append(getPiercingRes()).append("\n");
        if (getSlashingRes() != 0) lol.append("Сопротивляемость к режуще-рубящему ").append(getSlashingRes()).append("\n");
        if (getModernFirearmRes() != 0) lol.append("Сопротивляемость к современному огнестрелу ").append(getModernFirearmRes()).append("\n");
        if (getRangedOldRes() != 0) lol.append("Сопротивляемость к устаревшему дальнему ").append(getRangedOldRes()).append("\n");
        if (getMeleeRes() != 0) lol.append("Сопротивляемость к ближнему ").append(getMeleeRes()).append("\n");
        if (getRangedRes() != 0) lol.append("Сопротивляемость к дальнему ").append(getRangedRes()).append("\n");
        System.out.println(lol.toString());
        return lol.toString().trim();
    }

    public void updateArmor() {
        DataHolder.inst().updatePlayerArmor(this.name);
    }

    //~~~~~~~~~~~~~~~~~Wounds management

    public int getWoundsMod() {
        /*
        DataHolder.inst().updatePlayerWounds(this.name);
        if (undead) {
            if (woundsMod == -1) return 0;
            if (woundsMod == -2) return -1;
            return woundsMod;
        }
        //return this.woundsMod;

         */
        int woundsMod = getWoundPyramid().getWoundsMod();
        if (insensitive) {
            woundsMod = woundsMod/2;
        }
        return woundsMod;
    }

    public void setWoundMod(int woundsMod) {
        this.woundsMod = woundsMod;
    }

    public int getBloodloss() {
        return bloodloss;
    }

    public void setBloodloss(int bloodloss) {this.bloodloss = bloodloss;}

    public int getMentalDebuff() {
        return mentalDebuff;
    }

    public void setMentalDebuff(int mentalDebuff) {this.mentalDebuff = mentalDebuff;}

    public String addWound(WoundType woundType, String description) {
        return this.woundPyramid.addWound(woundType, this.name, description);
    }

    public String addWound(int damage, WoundType woundType, String description) {
        return this.woundPyramid.addWound(damage, woundType, this.name, description);
    }

    public int getFatigue() {
        int fatigue = 0;
        try {
            fatigue = NpcAPI.Instance().getIWorld(ServerProxy.getForgePlayer(name).dimension).getPlayer(name).getFactionPoints(3);
        } catch (Exception ignored) {
            
        }
        return Math.min(fatigue, 0);
    }

    public void updateWounds(List<WoundType> wounds) {
        if (wounds == null) return;

        this.woundPyramid.reset();
        for (WoundType wound : wounds) {
            this.woundPyramid.addWound(wound);
        }
    }


    @Deprecated
    public String inflictDamage(double damage, String description) {
//        System.out.println("Inflicting " + damage + " to " + this.name);
        if (damage < 0) return null;
        WoundType woundType;
        if (damage < 4) {
            woundType = WoundType.NONLETHAL;
        } else if (damage < 7) {
            woundType = WoundType.LETHAL;
        } else if (damage < 10) {
            woundType = WoundType.CRITICAL;
        } else if (damage < 13) {
            woundType = WoundType.CRITICAL_DEPR;
        } else {
            woundType = WoundType.DEADLY;
        }

//        System.out.println("Wound type: " + woundType);

        return addWound(woundType, description);
    }

    public String inflictDamage(int damage, String description, boolean persisted, boolean forcedDeadly, WoundType woundType) {
//        System.out.println("Inflicting " + damage + " to " + this.name);
        if (damage <= 0) {
            if (hasTrait(Trait.STAMINA)) return "нанесено 0 урона";
            return addWound(1, WoundType.NONLETHAL, description);
        }

//        System.out.println("Wound type: " + woundType);

        return addWound(damage, woundType, description);
    }

    public WoundPyramid getWoundPyramid() {
        return this.woundPyramid;
    }


    //~~~~~~~~~~~~~~~~~Skill management

    public List<Skill> getSkills() {
        return skills;
    }

    public Skill getByAlias(String alias) {
        for (Map.Entry<String, String> entry : aliases.entrySet()) {
            if (entry.getKey().equals(alias.toLowerCase())) {
                return getSkill(entry.getValue());
            }
        }
        return getSkill(alias);
    }

    public Skill getSkill(String skill) {
        for (Skill skill_iter : skills) {
            if (skill_iter.getName().equals(skill)) {
                return skill_iter;
            }
        }
        return null;
    }

    public int getSkillLevel(String skill) {
        Skill sk = getByAlias(skill);
        if (sk != null) {
            return sk.getLevel();
        }
        return 0;
    }

    public String getAttribute(String skill) {
        Skill sk = getByAlias(skill);
        if (sk != null) {
            return sk.getAttribute();
        }
        return "";
    }

    public int getSkillLevelAsInt(String skill) {
        Skill sk = getByAlias(skill);
        return sk.getLevel();
    }


    public void addSkill(String skillName, int level) {
        addSkill(skillName, level, SkillType.SIMPLE, "", 0);
    }

//    public void addSkill(String skillName, int level, SkillType skillType) {
//        addSkill(skillName, level, skillType, 0);
//    }

    public void addSkill(String skillName, int level, SkillType type, String attribute, int percent) {
        logger.debug("Adding skill: " + skillName + " " + level + " " + attribute + " " + percent);
        Skill skill = new Skill(skillName, level, type, attribute, percent);
        skills.remove(skill);
        skills.add(skill);
    }

    public String getSkillNameFromContext(String content) throws DataException {
        if (content.startsWith("%")) content = content.substring(1);

        skills.sort(new Skill.SkillComparator());
        Collections.reverse(skills);

        content = content.trim().toLowerCase();
        for (Skill skill : skills) {
            if (content.startsWith(skill.getName())) {
                return skill.getName();
            }
        }

        for (Map.Entry<String, String> entry : aliases.entrySet()) {
            if (content.equals(entry.getKey())) {
                Skill skill = getByAlias(entry.getKey());
                if (skill != null) return skill.getName();
            }
        }
        if (content.startsWith("скорость")) return "скорость";
        throw new DataException("1", "В контексте \"" + content + "\" не был найден навык! Проверьте список своих навыков: /update skills.");
    }

    public String getAliasFor(String skill) {
        for (Map.Entry<String, String> entry : aliases.entrySet()) {
            if (entry.getValue().equals(skill.toLowerCase())) {
                return entry.getKey();
            }
        }
        return null;
    }

    public void clearSkills() {
        skills = new ArrayList<>();
    }

    //~~~~~~~~~~~~~~~~~Combat stuff

    public int getWeaponMod(String skillName) {
        for (Weapon weapon : weapons) {
            if (weapon.getSkillName().equals(skillName)) {
                return weapon.getMod();
            }
        }
        return 0;
    }


    //~~~~~~~~~~~~~~~~~Overridden methods and other stuff
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof String) {
            return obj.equals(this.name);
        }
        if (obj instanceof Player) {
            return ((Player) obj).name.equals(this.name);
        }
        return false;
    }


    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("[Object of class Player]\n");
        result.append("Name : ").append(this.name).append('\n');
        result.append("Prefix : ").append(this.prefix).append('\n');
        result.append("Displayed name : ").append(getDisplayedName()).append('\n');
        result.append("Skills:\n");
        for (Skill skill : skills) {
            result.append(skill.getName()).append(":").append(skill.getLevelAsString()).append('\n');
        }
        result.append("Armor : ").append(armor.toString()).append('\n');
        result.append("WoundsPyramid : ").append(woundPyramid.toString());
        result.append("Default Range : ").append(getDefaultRange()).append('\n');

        return result.toString();
    }

    public void updateWith(Object arg) {
        setChanged();
        notifyObservers(arg);
    }

    @Deprecated
    public void sendMessage(String message) {
        ServerProxy.sendMessage(this, message);
    }


    public void sendMessage(Message message) {
        ServerProxy.sendMessage(this, message);
    }


    public void addAlias(String newAlias, String skillName) {
        if (aliases.containsValue(skillName)) {
            removeAlias(getAliasFor(skillName));
        }
        this.aliases.put(newAlias, skillName);
    }

    public void removeAlias(String alias) {
        this.aliases.remove(alias);
    }

    public Weapon getWeapon(String skillName) {
        for (Weapon w : this.weapons) {
            if (w.getSkillName().equals(skillName)) {
                return w;
            }
        }
        return null;
    }

    public boolean isSuperFighter() {
        return superFighter;
    }

    public void setSuperFighter(boolean superFighter) {
        this.superFighter = superFighter;
    }

    public MovementTrait getMovementTrait() {
        if (overridedTraits.containsKey("movement")) return (MovementTrait) overridedTraits.get("movement");
        return movementTrait;
    }

    public void setMovementTrait(MovementTrait movementTrait) {
        this.movementTrait = movementTrait;
    }

    public int getRemainingBlocks() {
        return (int) (movementBadge.getBlocksToCover() - movementBadge.getDistanceBehind());
    }

  /*  public int getRemainingBlocks() {
        if (remainingBlocks < 0) return 0;
        return remainingBlocks;
    }

   */

    public void setRemainingBlocks(int remainingBlocks) {
        this.remainingBlocks = remainingBlocks;
    }

    public MovementHistory getMovementHistory() {
        System.out.println("Getting Movement History of " + name + " — " + movementHistory);
        return movementHistory;
    }

    public void setMovementHistory(MovementHistory movementHistory) {
        System.out.println("Movement History of " + name + " set to " + movementHistory);
        this.movementHistory = movementHistory;
    }

    public ChargeHistory getChargeHistory() {
        System.out.println("Getting Charge History of " + name + " — " + chargeHistory);
        return chargeHistory;
    }

    public void setChargeHistory(ChargeHistory chargeHistory) {
        System.out.println("Charge History of " + name + " set to " + chargeHistory);
        this.chargeHistory = chargeHistory;
    }

    public void processMovement() {
//        switch (movementHistory) {
//            case NOW:
//            case RUN_UP:
//                this.movementHistory = MovementHistory.LAST_TIME;
//                break;
//            default:
//                this.movementHistory = MovementHistory.NONE;
//                break;
//        }
//        switch (chargeHistory) {
//            case CHARGE:
//                this.chargeHistory = ChargeHistory.CHARGE_FADING;
//                break;
//            case CHARGE_FADING:
//            default:
//                this.chargeHistory = ChargeHistory.NONE;
//        }
        this.movementHistory = MovementHistory.NONE;
        System.out.println("(process) Movement History of " + name + " set to " + movementHistory);
        System.out.println("(process) Charge History of " + name + " set to " + chargeHistory);
//        this.movementHistory = movementHistory;
//        this.chargeHistory = chargeHistory;
    }



    public String bukkitExecute(String command) {
        try {
            //ForgeCore.getForgePlayer(name).performCommand(command);
        } catch (NullPointerException e) {
            e.printStackTrace();
            return "Null!";
        }
        return null;
    }


    public void changeWoundsNumber(WoundType woundType, int number) {
        this.woundPyramid.changeWoundsNumber(woundType, number);
    }

    public Player getWalker() {
        return walker;
    }

    public void setWalker(Player walker) {
        this.walker = walker;
        if (walker != null) {
            walker.setWalkedBy(this);
        }
    }

    public Player getWalkedBy() {
        return walkedBy;
    }

    private void setWalkedBy(Player walkedBy) {
        this.walkedBy = walkedBy;
    }

    public List<Skill> getAttackSkills() {
        ArrayList<Skill> result = new ArrayList<>();
        for (Skill skill : getSkills()) {
            if (skill.getName().toLowerCase().contains("владение") || skill.getName().toLowerCase().contains("метание")) {
                result.add(skill);
            }
        }
        return result;
    }

    public void performCommand(String command) {
        try {
            System.out.println(getName() + " " + command);
            ServerProxy.getForgePlayer(getName()).getServer().getCommandManager().executeCommand(ServerProxy.getForgePlayer(getName()), command); // TODO: fix NullPointerException
        } catch (Exception e) {
            System.out.println(e.toString());
        }

    }

    public List<Skill> getDefenseSkills() {
        ArrayList<Skill> result = new ArrayList<>();
        for (Skill skill : getSkills()) {
            if (skill.getName().toLowerCase().contains("уклонение") || skill.getName().toLowerCase().contains("блокирование") || skill.getName().toLowerCase().contains("парирование") || skill.getName().toLowerCase().contains("группировка")) {
                result.add(skill);
            }
        }
        return result;
    }

    public List<Skill> getParrySkills() {
        ArrayList<Skill> result = new ArrayList<>();
        for (Skill skill : getSkills()) {
            if (skill.getName().toLowerCase().contains("парирование")) {
                result.add(skill);
            }
        }
        return result;
    }

    public Skill getBlockSkill() {
        for (Skill skill : skills) {
            if (skill.getName().equals("блокирование")) {
                return skill;
            }
        }
        return null;
    }

    public Skill getGroupSkill() {
        for (Skill skill : skills) {
            if (skill.getName().equals("группировка")) {
                return skill;
            }
        }
        return null;
    }

    public Skill getWillpowerSkill() {
        for (Skill skill : skills) {
            if (skill.getName().equals("сила воли")) {
                return skill;
            }
        }
        return null;
    }

    public MovementBadge getMovementBadge() {
        return movementBadge;
    }

    public void setMovementBadge(MovementBadge movementBadge) {
        this.movementBadge = movementBadge;
    }

    public void sendError(Exception e) {
        Message message = new Message(e.toString(), ChatColor.RED);
        sendMessage(message);

    }


    public Skill getEvadeSkill() {
        for (Skill skill : skills) {
            if (skill.getName().equals("уклонение")) {
                return skill;
            }
        }
        return null;
    }

    public Skill getMentalSkill() {
        for (Skill skill : skills) {
            if (skill.getName().equals("психическая защита")) {
                return skill;
            }
        }
        return null;
    }

    public Skill getPhysicSkill() {
        for (Skill skill : skills) {
            if (skill.getName().equals("физическая защита")) {
                return skill;
            }
        }
        return null;
    }

    public Skill getHandToHandSkill() {
        for (Skill skill : skills) {
            System.out.println(skill.getName() + " = " + DefenseType.HAND_TO_HAND.toNameString());
            if (skill.getName().equals(DefenseType.HAND_TO_HAND.toNameString())) {
                return skill;
            }
        }
        return null;
    }


    public Player getSubordinate() {
        return subordinate;
    }

    public void setSubordinate(String subordinateName) {
        Player subordinate = DataHolder.inst().getPlayer(subordinateName);
        System.out.println("Player found while setting subordinate: " + subordinate.getName());
        setSubordinate(subordinate);
    }

    public void setSubordinate(Player subordinate) {

        Player prevMaster = DataHolder.inst().getMasterForSubordinate(subordinate.getName());
        if (prevMaster != null && !prevMaster.getName().equals(name)) {
            Message prevInfo = new Message(this.name + " забрал у вас подчинённого " + subordinate.getName(), ChatColor.RED);
            prevMaster.sendMessage(prevInfo);
            ClickContainer.deleteContainer("Subordinate", ServerProxy.getForgePlayer(prevMaster.getName()));
            prevMaster.removeSubordinate();
        }

        if (!DataHolder.inst().isNpc(subordinate.getName())) {
            Message subInfo = new Message(this.name + " перехватил у вас управление. Ожидайте.", ChatColor.RED);
            subordinate.sendMessage(subInfo);
            subordinate.performCommand("/" + ServerPurgeContainersExecutor.NAME);
        }


        Message message = new Message("Ваш подчинённый:");
        MessageComponent who = new MessageComponent( " [" + subordinate.getName() + "]", ChatColor.GRAY);
        who.setHoverText("ЛКМ для телепортации к подчиненному!", TextFormatting.BLUE);
        who.setClickCommand("/" + SubExecutor.NAME + " " + subordinate.getName() +" tp");
        message.dim();
        MessageComponent click = new MessageComponent(" [Удалить]");
        click.setColor(ChatColor.GRAY);
        click.setClickCommand("/" + RemoveSubordinateExecutor.NAME);
        message.addComponent(who);
        message.addComponent(click);

        Helpers.sendClickContainerToClient(message, "Subordinate", ServerProxy.getForgePlayer(this.name));
        this.subordinate = subordinate;
        syncModifiers(subordinate);

    }

    public String getModString() {
        // CREATE DEBUG MESSAGE IF NEEDED
        String mods = "";
        //    if (clickContainer.getName().equals("ToolPanel") ||
        //            clickContainer.getName().equals("PerformAction")
        //    ) {

        if (getModifiersState().getModifier("damage")!=null) {
            int modifiedDamage = getModifiersState().getModifier("damage");
            if (modifiedDamage != -666) {
                String mDamageStr = String.valueOf(modifiedDamage);
                if (modifiedDamage > 0) mDamageStr = "+" + mDamageStr;
                mods = " Урон: [" + mDamageStr + "] ";
            }
        }
        if (getModifiersState().getModifier("dice")!=null) {
            int modifiedDice = getModifiersState().getModifier("dice");
            if (modifiedDice != -666) {
                String mDiceStr = String.valueOf(modifiedDice);
                if (modifiedDice > 0) mDiceStr = "+" + mDiceStr;
                mods += " Уровень навыка: [" + mDiceStr + "] ";
            }
        }
        if (getModifiersState().getModifier("percent")!=null) {
            int percent = getModifiersState().getModifier("percent");
            if (percent != -666) {
                mods += " Процентный: " + percent + "%";
            }
        }
        if (getModifiersState().getModifier("bodyPart")!=null) {
            int bodyPart = getModifiersState().getModifier("bodyPart");
            if (bodyPart != -666) {
                mods += "Точечная: " + BodyPart.getBodyPartFromInt(bodyPart).getName();
            }
        }
        if (getModifiersState().getModifier("serial")!=null) {
            int serial = getModifiersState().getModifier("serial");
            if (serial == 1) {
                mods += " Серийный: да";
            }
        }
        if (getModifiersState().getModifier("stationary")!=null) {
            int stationary = getModifiersState().getModifier("stationary");
            if (stationary == 1) {
                mods += " Стационарно: да";
            }
        }
        if (getModifiersState().getModifier("cover")!=null) {
            int cover = getModifiersState().getModifier("cover");
            if (cover == 1) {
                mods += " Укрытие: да";
            }
        }
        if (getModifiersState().getModifier("optic")!=null) {
            int optic = getModifiersState().getModifier("optic");
            if (optic == 1) {
                mods += " Прицеливание: да";
            }
        }
        if (getModifiersState().getModifier("bipod")!=null) {
            int bipod = getModifiersState().getModifier("bipod");
            if (bipod == 1) {
                mods += " Сошки установлены: да";
            }
        }
        if (getModifiersState().getModifier("stunning")!=null) {
            int stunning = getModifiersState().getModifier("stunning");
            if (stunning == 1) {
                mods += " Оглушение: да";
            }
        }
        if (getModifiersState().getModifier("capture")!=null) {
            int capture = getModifiersState().getModifier("capture");
            if (capture == 1) {
                mods += " Призыв к сдаче: да";
            }
        }
        if (getModifiersState().getModifier("noriposte")!=null) {
            int noriposte = getModifiersState().getModifier("noriposte");
            if (noriposte == 1) {
                mods += " Не использовать рипост: да";
            }
        }
        if (getModifiersState().getModifier("shieldwall")!=null) {
            int shieldwall = getModifiersState().getModifier("shieldwall");
            if (shieldwall == 1) {
                mods += " Глухая оборона: да";
            }
        }
        if (getModifiersState().getModifier("suppressing")!=null) {
            int suppressing = getModifiersState().getModifier("suppressing");
            if (suppressing == 1) {
                mods += " Огонь на подавление: да";
            }
        }

        return mods;
        //   }
    }

    private void syncModifiers(Player subordinate) {
        int dice = -666;
        int damage = -666;
        int bodyPart = -666;
        int serial = -666;
        int percent = -666;
        int stationary = -666;
        int cover = -666;
        int optic = -666;
        int bipod = -666;
        int stunning = -666;
        int capture = -666;
        int noriposte = -666;
        int shieldwall = -666;
        int suppressing = -666;

        if (subordinate.getModifiersState().getModifier("dice")!=null) {
            dice = subordinate.getModifiersState().getModifier("dice");
            System.out.println("DICE SUB: " + dice);
        }
        if (subordinate.getModifiersState().getModifier("damage")!=null) {
            damage = subordinate.getModifiersState().getModifier("damage");
        }
        if (subordinate.getModifiersState().getModifier("percent")!=null) {
            percent = subordinate.getModifiersState().getModifier("percent");
        }
        if (subordinate.getModifiersState().getModifier("bodypart")!=null) {
            bodyPart = subordinate.getModifiersState().getModifier("bodypart");
        }
        if (subordinate.getModifiersState().getModifier("serial")!=null) {
            serial = subordinate.getModifiersState().getModifier("serial");
        }
        if (subordinate.getModifiersState().getModifier("stationary")!=null) {
            stationary = subordinate.getModifiersState().getModifier("stationary");
        }
        if (subordinate.getModifiersState().getModifier("cover")!=null) {
            cover = subordinate.getModifiersState().getModifier("cover");
        }
        if (subordinate.getModifiersState().getModifier("optic")!=null) {
            optic = subordinate.getModifiersState().getModifier("optic");
        }
        if (subordinate.getModifiersState().getModifier("bipod")!=null) {
            bipod = subordinate.getModifiersState().getModifier("bipod");
        }
        if (subordinate.getModifiersState().getModifier("stunning")!=null) {
            stunning = subordinate.getModifiersState().getModifier("stunning");
        }
        if (subordinate.getModifiersState().getModifier("capture")!=null) {
            capture = subordinate.getModifiersState().getModifier("capture");
        }
        if (subordinate.getModifiersState().getModifier("noriposte")!=null) {
            noriposte = subordinate.getModifiersState().getModifier("noriposte");
        }
        if (subordinate.getModifiersState().getModifier("shieldwall")!=null) {
            shieldwall = subordinate.getModifiersState().getModifier("shieldwall");
        }
        if (subordinate.getModifiersState().getModifier("suppressing")!=null) {
            suppressing = subordinate.getModifiersState().getModifier("suppressing");
        }

        this.performCommand(SyncModifiers.NAME + " " + dice + " " + damage + " " + percent + " " + bodyPart + " " + serial + " " + stationary + " " + cover + " " + optic + " " + bipod + " " + stunning + " " + capture + " " + noriposte + " " + shieldwall + " " + suppressing);
//        this.performCommand(SetModifier.NAME + " damage " + damage);
//        this.performCommand(SetModifier.NAME + " percent " + percent);
//        this.performCommand(SetModifier.NAME + " bodypart " + bodyPart);
//        this.performCommand(SetModifier.NAME + " serial " + serial);
//        this.performCommand(SetModifier.NAME + " stationary " + stationary);
//        this.performCommand(SetModifier.NAME + " cover " + cover);
//        this.performCommand(SetModifier.NAME + " optic " + optic);
//        this.performCommand(SetModifier.NAME + " bipod " + bipod);
//        this.performCommand(SetModifier.NAME + " stunning " + stunning);
//        this.performCommand(SetModifier.NAME + " capturing " + capturing);

    }

    public void removeSubordinate() {
        this.subordinate = null;
    }

    public void setWoundPyramind(WoundPyramid fromEndurance) {
        this.woundPyramid = fromEndurance;
    }

    public PercentDice getPercentDice() {
        return percentDice;
    }


    public void setPercentDice(PercentDice percentDice) {
        this.percentDice = percentDice;
    }


    public int getDefenseModFor(BodyPart targetedPart) {
        return armor.getDamageFor(targetedPart);
    }

    public ModifiersState getModifiersState() {
        return modifiersState;
    }

    public int getShieldMod() {
        if (getShield() == null) return 0;
        int req = getShield().getStrengthRequirement();
        if (getSkillLevelAsInt("сила") < req) {
            return getSkillLevelAsInt("сила") - req;
        }
        return 0;
    }


    public void freeze() {
        if (isFrozen) return;
        setFrozen(true);
    }

    public void unfreeze() {
        if (!isFrozen) return;
        setFrozen(false);
    }

    private boolean isFrozen() {
        return isFrozen;
    }

    private void setFrozen(boolean needToFreeze) {
        Combat c = DataHolder.inst().getCombatForPlayer(this.getName());
//        if (c != null && !DataHolder.inst().isNpc(this.getName()))
//        DataHolder.inst().getMasterForCombat(c.getId()).performCommand("/freezeplayer " + this.getName());
        isFrozen = needToFreeze;
    }

    public int getRecoil() {
        int recoil1 = 0;
        for (StatusEffect se : statusEffects) {
            if (!se.getStatusType().equals(StatusType.RECOIL)) continue;
            recoil1 = Math.min(recoil1, se.getContextNumber());
        }
        return recoil1;
    }

    public int getConfused() {
        int confused = 0;
        for (StatusEffect se : statusEffects) {
            if (!se.getStatusType().equals(StatusType.CONFUSED)) continue;
            confused--;
        }
        return Math.max(-5, confused);
    }

    public void removeFeints() {
        removeStatusEffect(StatusType.RECKLESS_ATTACK);
        removeStatusEffect(StatusType.FEINT);
    }

    public boolean hasRemovableFeints() {
        return hasStatusEffect(StatusType.RECKLESS_ATTACK) || hasStatusEffect(StatusType.FEINT);
    }

    public void setRecoil(int recoil) {
        System.out.println(name + " recoil set to " + recoil);
        this.recoil = recoil;
        if (!DataHolder.inst().isNpc(name)) performCommand("/geteffects");
    }

    public boolean fireProjectile(int hand) {
        System.out.println("Firing some shit for " + name + " hand: " + hand);
        EntityLivingBase forgeEntity = ServerProxy.getForgePlayer(name);
        Player master = null;
        if (DataHolder.inst().isNpc(name)) {
            forgeEntity = DataHolder.inst().getNpcEntity(name);
            master = DataHolder.inst().getMasterForNpcInCombat(this);
        }
        ItemStack weapon;
        if (hand == 0) {
            weapon = forgeEntity.getHeldItemOffhand();
        } else {
            weapon = forgeEntity.getHeldItemMainhand();
        }
        WeaponTagsHandler handler = new WeaponTagsHandler(weapon);
        handler.fireProjectile();

        if (!DataHolder.inst().isNpc(name)) {
            performCommand("/bolt " + hand);
        } else if (master != null) {
            System.out.println("trying to execute bolt");
            BoltCommand.executeForEntity(
                    ServerProxy.getForgePlayer(master.getName()),
                    forgeEntity,
                    hand
            );

        }
        return handler.isFirearmLoaded();
    }

    public boolean hasDurabilityHand(int hand) {
        EntityLivingBase forgeEntity = ServerProxy.getForgePlayer(name);
        Player master = null;
        if (DataHolder.inst().isNpc(name)) {
            forgeEntity = DataHolder.inst().getNpcEntity(name);
            master = DataHolder.inst().getMasterForNpcInCombat(this);
        }
        ItemStack weapon;
        if (hand == 0) {
            weapon = forgeEntity.getHeldItemOffhand();
        } else {
            weapon = forgeEntity.getHeldItemMainhand();
        }
        WeaponDurabilityHandler handler = new WeaponDurabilityHandler(weapon);
        return handler.hasDurabilityDict();
    }

    public WeaponDurabilityHandler getDurHandlerForHand(int hand) {
        EntityLivingBase forgeEntity = ServerProxy.getForgePlayer(name);
        Player master = null;
        if (DataHolder.inst().isNpc(name)) {
            forgeEntity = DataHolder.inst().getNpcEntity(name);
            master = DataHolder.inst().getMasterForNpcInCombat(this);
        }
        ItemStack weapon;
        if (hand == 0) {
            weapon = forgeEntity.getHeldItemOffhand();
        } else {
            weapon = forgeEntity.getHeldItemMainhand();
        }
        return new WeaponDurabilityHandler(weapon);
    }

    public WeaponDurabilityHandler getDurHandlerForArmorPiece(String part) {
        try {
            EntityLivingBase forgePlayer = ServerProxy.getForgePlayer(getName());
            if (DataHolder.inst().isNpc(getName())) forgePlayer = DataHolder.inst().getNpcEntity(getName());
            System.out.println("forgePlayer is " + forgePlayer.getName());

            for (ItemStack armorPiece : forgePlayer.getArmorInventoryList()) {
                WeaponTagsHandler armorHandler = new WeaponTagsHandler(armorPiece);
                if (armorHandler.isArmor()) {
                    for (NBTTagCompound piece : armorHandler.getAllWeapons()) {

                        try {
                            NBTTagCompound subpiece = piece.getCompoundTag("armor");
                            System.out.println(subpiece.toString());
                            if(subpiece.hasKey(part)) return new WeaponDurabilityHandler(armorPiece);
                        } catch (Exception i) {

                        }
                        if(piece.hasKey(part)) return new WeaponDurabilityHandler(armorPiece);
                    }
                }
            }
        } catch (NullPointerException ignored) {
            return null;
        }
        return null;
    }

    public void takeAwayDurFromAllArmor(int damage) {
        try {
            EntityLivingBase forgePlayer = ServerProxy.getForgePlayer(getName());
            if (DataHolder.inst().isNpc(getName())) forgePlayer = DataHolder.inst().getNpcEntity(getName());
            System.out.println("forgePlayer is " + forgePlayer.getName());

            for (ItemStack armorPiece : forgePlayer.getArmorInventoryList()) {
                WeaponTagsHandler armorHandler = new WeaponTagsHandler(armorPiece);
                if (armorHandler.isArmor()) {
                    WeaponDurabilityHandler armordur = new WeaponDurabilityHandler(armorPiece);
                    armordur.takeAwayDurability(damage);
                }
            }
        } catch (NullPointerException ignored) {
            return;
        }
        return;
    }

    public ItemStack getItemForHand(int hand) {
        EntityLivingBase forgeEntity = ServerProxy.getForgePlayer(name);
        Player master = null;
        if (DataHolder.inst().isNpc(name)) {
            forgeEntity = DataHolder.inst().getNpcEntity(name);
            master = DataHolder.inst().getMasterForNpcInCombat(this);
        }
        if (hand == 0) {
            return forgeEntity.getHeldItemOffhand();
        } else {
            return forgeEntity.getHeldItemMainhand();
        }
    }

    public EntityLivingBase getEntity() {
        EntityLivingBase forgeEntity;
        if (DataHolder.inst().isNpc(name)) {
            forgeEntity = DataHolder.inst().getNpcEntity(name);
        } else {
            forgeEntity = ServerProxy.getForgePlayer(name);
        }
        return forgeEntity;
    }

    public void purgeModifierSet() {
        this.modifiersState = new ModifiersState();
    }

    public void removeSkill(String skill) {
        Skill toRemove = getSkill(skill);
        skills.remove(toRemove);
    }

    public boolean isLying() {
        if (DataHolder.inst().isNpc(getName())) {
            EntityNPCInterface npc = (EntityNPCInterface) DataHolder.inst().getNpcEntity(getName());
            return (npc.wrappedNPC.getAi().getAnimation() == AnimationType.CRAWL || npc.wrappedNPC.getAi().getAnimation() == AnimationType.SLEEP);
        } else {
            EntityPlayerMP playerMP = ServerProxy.getForgePlayer(getName());
            try {
                ModelData md = playerMP.getCapability(ModelData.MODELDATA_CAPABILITY, null);
                return  (md.animationEquals(EnumAnimation.CRAWLING) || md.animationEquals(EnumAnimation.SLEEPING_NORTH) || md.animationEquals(EnumAnimation.SLEEPING_EAST) || md.animationEquals(EnumAnimation.SLEEPING_SOUTH) || md.animationEquals(EnumAnimation.SLEEPING_WEST));
            } catch (Exception e) {
                return false;
            }
        }
    }

    public void makeFall() {
        if (DataHolder.inst().isNpc(getName())) {
            EntityNPCInterface npc = (EntityNPCInterface) DataHolder.inst().getNpcEntity(getName());
            npc.wrappedNPC.getAi().setAnimation(AnimationType.SLEEP);
        } else {
            EntityPlayerMP playerMP = ServerProxy.getForgePlayer(getName());
            ServerProxy.sendMessageFromAndInformMasters(this, new Message(getName() + " падает!", ChatColor.RED));
            performCommand("/sleep");
            try {
                ModelData md = playerMP.getCapability(ModelData.MODELDATA_CAPABILITY, null);
                List<EnumAnimation> list = new ArrayList<EnumAnimation>();
                list.add(EnumAnimation.SLEEPING_NORTH);
                list.add(EnumAnimation.SLEEPING_EAST);
                list.add(EnumAnimation.SLEEPING_SOUTH);
                list.add(EnumAnimation.SLEEPING_WEST);
                Random rand = new Random();
                rand.nextInt(list.size());
                md.setAnimation(list.get(rand.nextInt(list.size())));
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    public void updateWeight() {
        if (getSkill("сила") == null) return;
        int strength = getSkillLevelAsInt("сила");
        float percentStrength = getSkill("сила").getPercent();
        optimalWeight = (int) (30 + 10 * strength + Math.floor(10 * (percentStrength / 100)));
        System.out.println("ow " + optimalWeight);
        maxWeight = optimalWeight * 2;
        weight = countWeight();
    }

    public int countWeight() {
        int weight = 0;
        int psychweight = 0;
        if (DataHolder.inst().isNpc(name)) {
            if (getWeight() == 0) {
                int [] test = ServerProxy.countWeightNpc(name); //не надо кучу раз считать, пожалуй
                weight = test[0];
                psychweight = test[1];
                DataHolder.inst().updateMaxWeight(name);
            } else {
                weight = this.getWeight();
            }
        } else {
            int [] test = ServerProxy.countWeight(name);
            weight = test[0];
            psychweight = test[1];
        }
        this.weight = weight;
        this.psychweight = psychweight;
        return weight;
    }

    public int getBuffsFromEquipment(String skill) {
        int buff = 0;
        if (DataHolder.inst().isNpc(name)) {
                buff = ServerProxy.countBuffsNpc(name, skill);
        } else {
            buff = ServerProxy.countBuffs(name, skill);
        }
        return buff;
    }

    public int countConvenience() {
        int convenience = 0;
        if (DataHolder.inst().isNpc(name)) {
            convenience = ServerProxy.countConvenienceNPC(name); //не надо кучу раз считать, пожалуй
                //DataHolder.inst().updateMaxWeight(name);
        } else {
            convenience = ServerProxy.getTotalConvenience(name);
        }
        //this.weight = weight;
        return convenience;
    }

    public int countDef() {
        int convenience = 0;
        if (DataHolder.inst().isNpc(name)) {
            convenience = ServerProxy.countDefNPC(name); //не надо кучу раз считать, пожалуй
            //DataHolder.inst().updateMaxWeight(name);
        } else {
            convenience = ServerProxy.getTotalDef(name);
        }
        //this.weight = weight;
        return convenience;
    }

    public int countWeightForced() {
        int weight = 0;
        int psychweight = 0;
        if (DataHolder.inst().isNpc(name)) {
            int [] test = ServerProxy.countWeightNpc(name);
        } else {
            int [] test = ServerProxy.countWeight(name);
            weight = test[0];
            psychweight = test[1];
        }
        this.psychweight = psychweight;
        this.weight = weight;
        return weight;
    }

    public void setMaxWeight(int maxWeight) {
        this.maxWeight = maxWeight;
    }

    public void setMaxPsyWeight(int maxpsychweight) {
        this.maxpsychweight = maxpsychweight;
    }


    public void setOptimalWeight(int optimalWeight) {
        this.optimalWeight = optimalWeight;
    }

    public int getWeight() {
        return weight;
    }

    public int getPsychweight() {
        return psychweight;
    }

    public int getMaxPsychweight() {
        return maxpsychweight;
    }

    public int getMaxWeight() {
        return maxWeight;
    }

    public int getOptimalWeight() {
        return optimalWeight;
    }

    public void vampiricHeal(Integer damage, String victim) {
        if (damage > 9) {
            DataHolder.inst().getModifiers().vampHealWound(this, 2, victim);
        } else if (damage > 6) {
            DataHolder.inst().getModifiers().vampHealWound(this, 1, victim);
        } else if (damage > 3) {
            DataHolder.inst().getModifiers().vampHealWound(this, 0, victim);
        }
    }

    public void setOpportunity(Duel opportunity, Player engagedWith) {
        this.opportunity = opportunity;
        this.engagedWith = engagedWith;
        if (!engagedWith.engagedBy.contains(this)) engagedWith.engagedBy.add(this);
        System.out.println(engagedWith.getName() + " втянут в бой by " + getName());
    }

    public void clearOpportunities() {
        Player eng = engagedWith;
        this.opportunity = null;
        if (engagedWith != null) {
            engagedWith.engagedBy.remove(this);
        }
        engagedWith = null;
        engagedWithFading = null;
        if (!DataHolder.inst().isNpc(name)) {
            performCommand("/geteffects");
        } else {
            Player master = DataHolder.inst().getMasterForNpcInCombat(this);
            if (master != null && master.getSubordinate() == this) master.performCommand("/geteffects");
        }

        if (eng != null) {
            if (!DataHolder.inst().isNpc(eng.getName())) {
                eng.performCommand("/geteffects");
            } else {
                Player master = DataHolder.inst().getMasterForNpcInCombat(eng);
                if (master != null && master.getSubordinate() == this) master.performCommand("/geteffects");
            }
        }
    }
    public void clearOpportunities2() {
        this.opportunity = null;
        if (engagedWith != null) {
            engagedWith.engagedBy.remove(this);
        }
        engagedWithFading = engagedWith;
        engagedWith = null;
    }

    public void clearOpportunities25() {
        this.opportunity = null;
        if (engagedWith != null) {
            engagedWith.engagedBy.remove(this);
        }
        engagedWithFading = null;
        engagedWith = null;
    }

    public void clearFadingOpportunity() {
        engagedWithFading = null;
    }

    public void clearOpportunities3() {
        this.opportunity = null;
        engagedWith = null;
        engagedWithFading = null;
        if (!DataHolder.inst().isNpc(name)) {
            performCommand("/geteffects");
        } else {
            Player master = DataHolder.inst().getMasterForNpcInCombat(this);
            if (master != null && master.getSubordinate() == this) master.performCommand("/geteffects");
        }
    }

    public void clearAllOpportunities() {
        clearOpportunities();
        for (Player engager : engagedBy) {
            if (engager.engagedWith == this) {
                engager.clearOpportunities3();
            }
        }
        engagedBy = new ArrayList<>();
    }

    public boolean executeOpportunity() {
        boolean proced = false;
        try {
            ru.konungstvo.combat.equipment.Weapon weapon = null;
            WeaponTagsHandler weaponTagsHandler = null;
            if (!opportunity.getDefender().hasTrait(Trait.CONCEIT) && !isLying()) {
                if (!(opportunity.getAttacks().get(0).getWeapon() instanceof Fist)) {
                    weaponTagsHandler = new WeaponTagsHandler(getItemForHand(opportunity.getActiveHand()));
                    weapon = new ru.konungstvo.combat.equipment.Weapon(weaponTagsHandler.getDefaultWeaponName(), weaponTagsHandler.getDefaultWeapon(), weaponTagsHandler);
                }
                if (((opportunity.getAttacks().get(0).getWeapon() instanceof Fist) || (weapon != null && weapon.toString().equals(opportunity.getAttacks().get(0).getWeapon().toString()))) && !hasStatusEffect(StatusType.STUNNED) && !isLying()) {
                    DataHolder.inst().registerDuel(opportunity);
                    opportunity.initDefense();
                    proced = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!proced) sendMessage("§4Не удалось провести атаку по возможности.");
        clearOpportunities();
        return proced;
    }

    public void executeOpportunity(Defense defense) { //TODO: потом
        DataHolder.inst().registerDuel(opportunity);
        opportunity.setDefense(defense);
        clearOpportunities();
    }

    public void setDefends(Player target) {
        this.defends = target;
        if (!defends.defendedBy.contains(this)) defends.defendedBy.add(this);
        System.out.println(defends.getName() + " защищаем by " + getName());
    }

    public Player getDefends() {
        return defends;
    }

    public ArrayList<Player> getDefendedBy() {
        return defendedBy;
    }

    public void clearDefends() {
        if (defends != null) {
            defends.defendedBy.remove(this);
        }
        defends = null;
    }

    public void clearAllDefends() {
        clearDefends();
        for (Player def : defendedBy) {
            if (def.defends == this) {
                def.clearDefends();
            }
        }
        defendedBy = new ArrayList<>();
    }

    public boolean hasDefensiveStance()  {
        return this.defensiveStance;
    }

    public void setDefensiveStance(boolean defensiveStance) {
        this.defensiveStance = defensiveStance;
        System.out.println("Defensive Stance set to " + defensiveStance);
        if (!DataHolder.inst().isNpc(name)) performCommand("/geteffects");
    }

    public ArrayList<Player> getEngagedBy() {
        return engagedBy;
    }

    public Player getEngagedWith() {
        return engagedWith;
    }

    public Player getEngagedWithFading() {
        return engagedWithFading;
    }

    public  boolean hasActingMount() {
        EntityLivingBase rider;
        Player mount = null;
        if (DataHolder.inst().isNpc(name)) {
            rider = DataHolder.inst().getNpcEntity(name);
        } else {
            rider = ServerProxy.getForgePlayer(name);
        }
        if (rider != null && rider.getRidingEntity() != null) {
            System.out.println(rider.getRidingEntity());
            System.out.println(rider.getRidingEntity().getName());
            mount = DataHolder.inst().getPlayer(rider.getRidingEntity().getName());
            if (mount != null && DataHolder.inst().getCombatForPlayer(mount.getName()) != null) {
                return DataHolder.inst().getCombatForPlayer(mount.getName()).getReactionList().getQueue().contains(mount.getName());
            }
        }
        return false;
    }

    public boolean hasMountOrRidersEngaged() {
        EntityLivingBase rider;
        Player mount = null;
        if (DataHolder.inst().isNpc(name)) {
            rider = DataHolder.inst().getNpcEntity(name);
        } else {
            rider = ServerProxy.getForgePlayer(name);
        }
        if (rider.getRidingEntity() != null) {
            mount = DataHolder.inst().getPlayer(rider.getRidingEntity().getName());
            if (mount != null && mount.getEngagedBy().size() > 0) return true;
        }

        List<Entity> riders = rider.getPassengers();
        for (Entity pass : riders) {
            Player passenger = DataHolder.inst().getPlayer(pass.getName());
            if (passenger != null && passenger.getEngagedBy().size() > 0) return true;
        }
        return false;
    }

    public void triggerOpportunityForRiders() {
        EntityLivingBase rider;
        Player mount = null;
        if (DataHolder.inst().isNpc(name)) {
            rider = DataHolder.inst().getNpcEntity(name);
        } else {
            rider = ServerProxy.getForgePlayer(name);
        }
        if (rider.getRidingEntity() != null) {
            mount = DataHolder.inst().getPlayer(rider.getRidingEntity().getName());
        }
        if (mount != null && mount.getEngagedBy().size() > 0) {
            boolean proced = false;
            ArrayList<Player> opp = new ArrayList<Player>(mount.getEngagedBy());
            for (Player opponent : opp) {
                proced = opponent.executeOpportunity();
                if (proced) break;
            }
        }

        List<Entity> riders = rider.getPassengers();
        for (Entity pass : riders) {
            Player passenger = DataHolder.inst().getPlayer(pass.getName());
            if (passenger != null && passenger.getEngagedBy().size() > 0) {
                boolean proced = false;
                ArrayList<Player> opp = new ArrayList<Player>(passenger.getEngagedBy());
                for (Player opponent : opp) {
                    proced = opponent.executeOpportunity();
                    if (proced) break;
                }
            }
        }
    }

    public void setCustomAdvantage(int customAdvantage) {
        this.customAdvantage = customAdvantage;
    }

    public int getCustomAdvantage() {
        return customAdvantage;
    }

//ICAPCustomInventory cap = player.getCapability(INVENTORY_CAP, null);
//    @SideOnly(Side.SERVER)
//    public void playSound(SoundEventKM sound) {
//        BlockPos blockPos;
//        World world;
//        if(DataHolder.inst().isNpc(getName())) {
//            EntityLivingBase entityLivingBase = DataHolder.inst().getNpcEntity(getName());
//            blockPos = entityLivingBase.getPosition();
//            world = entityLivingBase.world;
//        } else {
//            EntityLivingBase entityLivingBase = ServerProxy.getForgePlayer(getName());
//            blockPos = entityLivingBase.getPosition();
//            world = entityLivingBase.world;
//        }
//        world.playSound(null, blockPos, SOUND_SHOOT, SoundCategory.HOSTILE, 1.0F, 1.0F);
//
//    }
    public void processTurnStart(Player nextOne, Combat combat) {
        Queue queue = combat.getReactionList().getQueue();
        nextOne.processMovement();
        nextOne.clearOpportunities2();
        nextOne.clearDefends();
        nextOne.processStatusEffects(StatusEnd.TURN_START);


        if (!DataHolder.inst().isNpc(nextOne.getName())) {
            //TODO перенести эту кашу в отдельную функцию

            try {
                EntityLivingBase rider = ServerProxy.getForgePlayer(nextOne.getName());
                if (rider != null && rider.getRidingEntity() != null) {
                    Entity mount = rider.getLowestRidingEntity();
                    Player mountPlayer = null;
                    if(mount instanceof EntityNPCInterface) {
                        EntityNPCInterface npc = (EntityNPCInterface) mount;
                        mountPlayer = DataHolder.inst().getPlayer(npc.wrappedNPC.getName());
                        if (rider.getRidingEntity() == mount) {
                            if(!queue.contains(mountPlayer.getName())) mountPlayer.processMovement();
                            nextOne.setMovementHistory(mountPlayer.getMovementHistory());
                        } else if (mountPlayer.getMovementHistory() == MovementHistory.NOW) {
                            nextOne.setMovementHistory(mountPlayer.getMovementHistory());
                        }
                    } else if (mount instanceof EntityPlayerMP) {
                        EntityPlayerMP playerMP = (EntityPlayerMP) mount;
                        Player player = DataHolder.inst().getPlayer(playerMP.getName());
                        if (player.getMovementHistory() == MovementHistory.NOW) {
                            nextOne.setMovementHistory(player.getMovementHistory());
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println(e);
            }

        } else {

            try {
                EntityLivingBase rider = null;
                rider = (EntityLivingBase) DataHolder.inst().getNpcEntity(nextOne.getName());
                if (rider != null && rider.getRidingEntity() != null) {
                    Entity mount = rider.getLowestRidingEntity();
                    Player mountPlayer = null;
                    if(mount instanceof EntityNPCInterface) {
                        EntityNPCInterface npc = (EntityNPCInterface) mount;
                        mountPlayer = DataHolder.inst().getPlayer(npc.wrappedNPC.getName());
                        if (rider.getRidingEntity() == mount) {
                            if(!queue.contains(mountPlayer.getName())) mountPlayer.processMovement();
                            nextOne.setMovementHistory(mountPlayer.getMovementHistory());
                        } else if (mountPlayer.getMovementHistory() == MovementHistory.NOW) {
                            nextOne.setMovementHistory(mountPlayer.getMovementHistory());
                        }
                    } else if (mount instanceof EntityPlayerMP) {
                        EntityPlayerMP playerMP = (EntityPlayerMP) mount;
                        Player player = DataHolder.inst().getPlayer(playerMP.getName());
                        if (player.getMovementHistory() == MovementHistory.NOW) {
                            nextOne.setMovementHistory(player.getMovementHistory());
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println(e);
            }

            Player gm = DataHolder.inst().getMasterForNpcInCombat(nextOne);
            if (gm == null) {
                System.out.println("gm null wtf");
            }

        }

    }

    public void processTurnEnd(Player prev, String type, Combat combat) {
        Queue queue = combat.getReactionList().getQueue();
        String previous = prev.getName();

        Message info = new Message();
        boolean burned = false;
        boolean ignore = false;
        if (prev.hasStatusEffect(StatusType.BURNING)) {
            String wound = prev.inflictDamage(3, "ожог от горения");
            if (wound.equals("критическая рана") || wound.equals("смертельная рана")) burned = true;
        }

        if (type.equals("combat")) {
            prev.setRecoil(0);
        }

        if (false && prev.getWoundPyramid().getNumberOfWounds(WoundType.CRITICAL) + prev.getWoundPyramid().getNumberOfWounds(WoundType.LETHAL) >= prev.getWoundPyramid().getHealthPool()) {
            prev.inflictDamage(1, "кровотечение", false, false, WoundType.CRITICAL);
        }

        if (burned) {
            info.addComponent(new MessageComponent(previous + " заканчивает свой ход и падает обугленный.", ChatColor.DARK_RED));
            queue.removePlayerBc(previous, "обуглен");
        } else if (prev.getWoundPyramid().getNumberOfWounds(WoundType.CRITICAL) >= prev.getWoundPyramid().getHealthPool()) {
            info.addComponent(new MessageComponent(previous + " заканчивает свой ход и теряет сознание от кровопотери.", ChatColor.DARK_RED));
            queue.removePlayerBc(previous, "в отключке");
        } else if (type.equals("resetmovement")) {
            info.addComponent(new MessageComponent(previous + " заканчивает свой ход небоевым действием.", ChatColor.COMBAT));
        } else if (type.equals("extinguish")) {
            info.addComponent(new MessageComponent(previous + " заканчивает свой ход катанием по земле и тушится.", ChatColor.COMBAT));
            prev.removeStatusEffect(StatusType.BURNING);
        } else if (type.equals("charge")) {
            info = new Message(previous + " заканчивает свой ход началом натиска.", ChatColor.COMBAT);
        } else if (type.equals("concentrated")) {
//            String skill = String.join(" ", Arrays.copyOfRange(args, 2, args.length));
//            info = new Message(previous + " заканчивает свой ход концентрацией на навыке " + skill + ".", ChatColor.COMBAT);
        } else if (type.equals("magic")) {
            info = new Message(previous + " заканчивает свой ход подготовкой заклинания.", ChatColor.COMBAT);
        } else if (type.equals("defensivestance")) {
            info = new Message(previous + " заканчивает свой ход переходом в глухую оборону.", ChatColor.COMBAT);
        } else {
            info.addComponent(new MessageComponent(previous + " заканчивает свой ход.", ChatColor.COMBAT));
            ignore = true;
        }

        prev.riposted = null;

        System.out.println("processing1 " + prev.getName());
        prev.processStatusEffects(StatusEnd.TURN_END);
        prev.clearFadingOpportunity();
        if (!ignore) {
            for (Player p : combat.getFighters()) {
                p.sendMessage(info);
            }
            ServerProxy.informMasters(info, prev);
            //DiscordBridge.sendMessage(info.toString());
            logger.info(info.toString());
        }
    }

    public int setSize(int size) {
        return this.size = size;
    }
    public int getSize() {
        return size;
    }
}