package ru.konungstvo.player.wounds;

import ru.konungstvo.chat.ChatColor;
import ru.konungstvo.chat.message.Message;
import ru.konungstvo.chat.message.infrastructure.MessageComponent;
import ru.konungstvo.combat.Trait;
import ru.konungstvo.control.DataHolder;
import ru.konungstvo.player.NPC;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class WoundPyramid implements Serializable {
    public final static int CRIT_DEADLY_WEIGHT = 200;
    public final static int SEVERE_WEIGHT = 50;
    public final static int LIGHT_WEIGHT = 25;
    public final static int SCRATCH_WEIGHT = 5;

    public int getAlmostCuredBuff() {
        return almostCuredBuff;
    }

    public void setAlmostCuredBuff(int almostCuredBuff) {
        this.almostCuredBuff = almostCuredBuff;
    }

    private int almostCuredBuff = 0;

    public static WoundPyramid createFromEndurance(int enduranceLevel, int size) {

        return new WoundPyramid(enduranceLevel + size + 2, enduranceLevel + size + 2, enduranceLevel + size + 2, enduranceLevel + size + 2);
    }

    private List<WoundType> woundList;
    private int deadlyNumber = 1;
    private int criticalNumber = 1;
    private int severeNumber;
    private int lightNumber;
    private int scratchNumber;
    private int healthPool;



    public WoundPyramid(int severeNumber, int lightNumber, int scratchNumber, int healthPool) {
        this.woundList = new ArrayList<>();
        this.severeNumber = severeNumber;
        this.lightNumber = lightNumber;
        this.scratchNumber = scratchNumber;
        this.healthPool = healthPool;
    }

    public Iterator iterator() {
        return new WoundPyramidIterator();
    }

    private class WoundPyramidIterator implements Iterator {
        private WoundType woundType;
        private int cursor;

        public WoundPyramidIterator() {
            this.cursor = 0;
        }


        @Override
        public boolean hasNext() {
            return cursor < woundList.size();
        }

        @Override
        public WoundType next() {
            if (!hasNext()) return null;
            return woundList.get(this.cursor++);
        }
    }


    public int getNumberOfWounds(WoundType woundType) {
        WoundPyramidIterator iterator = new WoundPyramidIterator();
        int count = 0;
        while (iterator.hasNext()) {
            WoundType wound = iterator.next();
            if (wound == woundType) {
                count++;
            }
        }
        return count;
    }

    public void addWound(WoundType woundType) {
        addWound(woundType, null, null);
    }

    public String addWound(WoundType woundType, String name, String description) {
        switch (woundType) {
            case NONLETHAL:
                if (getNumberOfWounds(WoundType.NONLETHAL) == scratchNumber) {
                    return addWound(WoundType.LETHAL, name, description);
                }
                break;
            case LETHAL:
                if (getNumberOfWounds(WoundType.LETHAL) == lightNumber) {
                    return addWound(WoundType.CRITICAL, name, description);
                }
                break;
            case CRITICAL:
                if (getNumberOfWounds(WoundType.CRITICAL) == severeNumber) {
                    return addWound(WoundType.CRITICAL_DEPR, name, description);
                }
                break;
            case CRITICAL_DEPR:
                if (getNumberOfWounds(WoundType.CRITICAL_DEPR) == criticalNumber) {
                    return addWound(WoundType.DEADLY, name, description);
                }
                break;
            case DEADLY:
                if (getNumberOfWounds(WoundType.DEADLY) == deadlyNumber) {
//                    throw new WoundsException("Wounds:", "Нельзя добавить смертельную рану! Её слот уже занят.");
                    System.out.println("Нельзя добавить смертельную рану! Её слот уже занят.");
                }
        }

        if ((woundType == WoundType.DEADLY || woundType == WoundType.CRITICAL_DEPR) && getNumberOfWounds(WoundType.CRITICAL) < severeNumber) {
            if (name != null && DataHolder.inst().getPlayer(name).isPersistent()) return addWound(WoundType.CRITICAL, name, description);
        }

        try {
            if (woundType != WoundType.DEADLY) {
                DataHolder.inst().getModifiers().sendWound(name, woundType, description);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (DataHolder.inst().isNpc(name)) ((NPC) DataHolder.inst().getPlayer(name)).setWoundsInDescription();

        this.woundList.add(woundType);
        return woundType.getDesc();
    }

    public String addWound(int damage, WoundType woundType, String name, String description) {
        int counter1 = 0;
        int counter2 = 0;
        int counter3 = 0;



        System.out.println(getNumberOfWounds(WoundType.NONLETHAL) + " " + getNumberOfWounds(WoundType.LETHAL) + " " + getNumberOfWounds(WoundType.CRITICAL) + " " + healthPool + " woundtest");
        if (woundType.equals(WoundType.NONLETHAL) && (getNumberOfWounds(WoundType.NONLETHAL) + getNumberOfWounds(WoundType.LETHAL) + getNumberOfWounds(WoundType.CRITICAL) >= healthPool)) woundType = WoundType.LETHAL;
        if (woundType.equals(WoundType.LETHAL) && (getNumberOfWounds(WoundType.LETHAL) + getNumberOfWounds(WoundType.CRITICAL) >= healthPool)) woundType = WoundType.CRITICAL;
        if (DataHolder.inst().getPlayer(name).hasTrait(Trait.FRAGILE)) {
            if (woundType.equals(WoundType.LETHAL)) woundType = WoundType.CRITICAL;
        }
        WoundType woundTypeKostil1 = woundType;
        WoundType woundTypeKostil2 = null;
        WoundType woundTypeKostil3 = null;

        System.out.println(woundTypeKostil1);
        for (int i = 0; i < damage; i++) {

            if (woundType.equals(WoundType.NONLETHAL)) {
                if (getNumberOfWounds(WoundType.NONLETHAL) + getNumberOfWounds(WoundType.LETHAL) + getNumberOfWounds(WoundType.CRITICAL) >= healthPool) {
                    woundType = WoundType.LETHAL;
                    woundTypeKostil2 = WoundType.LETHAL;
                }
            }
            if (woundType.equals(WoundType.LETHAL)) {
                if (getNumberOfWounds(WoundType.LETHAL) + getNumberOfWounds(WoundType.CRITICAL) >= healthPool) {
                    woundType = WoundType.CRITICAL;
                    if (woundTypeKostil2 == null) {
                        woundTypeKostil2 = WoundType.CRITICAL;
                    } else {
                        woundTypeKostil3 = WoundType.CRITICAL;
                    }
                }
            }
            if (woundType.equals(WoundType.CRITICAL)) {
                if (getNumberOfWounds(WoundType.CRITICAL) >= healthPool) {
                    break;
                }
            }
            if (woundTypeKostil2 != null && woundTypeKostil3 == null) {
                //i++;
                counter2++;
            } else if (woundTypeKostil3 != null) {
                //i+=2;
                counter3++;
            } else {
                counter1++;
            }
            this.woundList.add(woundType);
        }
        try {
            DataHolder.inst().getModifiers().sendWounds(name, woundTypeKostil1, description, counter1);
            if (woundTypeKostil2 != null) DataHolder.inst().getModifiers().sendWounds(name, woundTypeKostil2, description, counter2);
            if (woundTypeKostil3 != null) DataHolder.inst().getModifiers().sendWounds(name, woundTypeKostil3, description, counter3);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (getNumberOfWounds(WoundType.NONLETHAL) + getNumberOfWounds(WoundType.LETHAL) + getNumberOfWounds(WoundType.CRITICAL) > healthPool) {
            trimWounds(name);
        }

        if (DataHolder.inst().isNpc(name)) ((NPC) DataHolder.inst().getPlayer(name)).setWoundsInDescription();

        return getPlural(counter1, woundTypeKostil1, counter2, woundTypeKostil2, counter3, woundTypeKostil3);
    }

    public String getPlural(int damage, WoundType woundType, int damage2, WoundType woundType2, int damage3, WoundType woundType3) {
        String result = "";
        int preLastDigit = damage % 100 / 10;
            if (preLastDigit == 1) {
                result = "нанесено ";
            }

            switch (damage % 10) {
                case 1:
                    result = "нанесен ";
                    break;
                case 2:
                case 3:
                case 4:
                    result = "нанесено ";
                    break;
                default:
                    result = "нанесено ";
            }

            result += getPlural1(damage, woundType);
            if (woundType2 == null && woundType3 == null) {
                return result + getEnder(damage);
            } else if (woundType2 != null && woundType3 == null) {
                return result + " и " + getPlural1(damage2, woundType2) + getEnder(damage2);
            } else if (woundType2 != null && woundType3 != null) {
                return result + ", " + getPlural1(damage2, woundType2) + " и " + getPlural1(damage3, woundType3) + getEnder(damage3);
            } else {
                int preLastDigit2 = (damage + damage2 + damage3) % 100 / 10;
                if (preLastDigit2 == 1) {
                    result = "нанесено ";
                }

                switch (damage + damage2 + damage3 % 10) {
                    case 1:
                        result = "нанесен ";
                        break;
                    case 2:
                    case 3:
                    case 4:
                        result = "нанесено ";
                        break;
                    default:
                        result = "нанесено ";
                }
                result += (damage + damage2 + damage3) + getEnder(damage + damage2 + damage3);
                return result;
            }
    }

    public String getPlural1(int damage, WoundType woundType) {
        int preLastDigit = damage % 100 / 10;
        if (preLastDigit == 1) {
            switch (woundType) {
                case CRITICAL:
                    return damage + " критического ";
                case LETHAL:
                    return damage + " летального ";
                case NONLETHAL:
                    return damage + " нелетального ";

            }
        }

        switch (damage % 10) {
            case 1:
                switch (woundType) {
                    case CRITICAL:
                        return damage + " критический";
                    case LETHAL:
                        return damage + " летальный";
                    case NONLETHAL:
                        return damage + " нелетальный";

                }
            case 2:
            case 3:
            case 4:
                switch (woundType) {
                    case CRITICAL:
                        return  damage + " критического";
                    case LETHAL:
                        return damage + " летального";
                    case NONLETHAL:
                        return damage + " нелетального";

                }
            default:
                switch (woundType) {
                    case CRITICAL:
                        return damage + " критического";
                    case LETHAL:
                        return damage + " летального";
                    case NONLETHAL:
                        return damage + " нелетального";

                }
        }

        return " " + damage;

    }

    public String getEnder(int damage) {
        int preLastDigit = damage % 100 / 10;
        if (preLastDigit == 1) {
            return " урона";
        }

        switch (damage % 10) {
            case 1:
                return " урон";
            case 2:
            case 3:
            case 4:
                return " урона";
            default:
                return " урона";
        }

    }

    public void trimWounds(String name) {
        int overlimit = (getNumberOfWounds(WoundType.NONLETHAL) + getNumberOfWounds(WoundType.LETHAL) + getNumberOfWounds(WoundType.CRITICAL) - healthPool);
        int overlimitcopy = overlimit;
        while (overlimit > 0) {
            overlimit--;
            if (getNumberOfWounds(WoundType.NONLETHAL) > 0) woundList.remove(WoundType.NONLETHAL);
            else if (getNumberOfWounds(WoundType.LETHAL) > 0) woundList.remove(WoundType.LETHAL);
        }
        DataHolder.inst().getModifiers().trimWounds(name, healthPool);
    }

    public int getWoundsMod() {
        int woundsMod = 0;
        if (getNumberOfWounds(WoundType.CRITICAL) >= 1 || getNumberOfWounds(WoundType.LETHAL) >= Math.ceil((double) healthPool/2)) woundsMod++;
        if (getNumberOfWounds(WoundType.CRITICAL) >= 1 || getNumberOfWounds(WoundType.LETHAL) >= healthPool) woundsMod++;
        if (getNumberOfWounds(WoundType.CRITICAL) >= Math.ceil((double) healthPool/2)) woundsMod++;
        return woundsMod;
    }

    @Deprecated
    public int getWoundsModOld() {

        if (getNumberOfWounds(WoundType.CRITICAL_DEPR) > 0 ||
                getNumberOfWounds(WoundType.DEADLY) > 0)
            return -3;
        if (getNumberOfWounds(WoundType.CRITICAL) > 0)
            return -2;
        if (getNumberOfWounds(WoundType.LETHAL) > 0)
            return -1;
        return 0;

    }

    public void reset() {
        this.woundList = new ArrayList<>();
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("Deadly: ").append(getNumberOfWounds(WoundType.DEADLY)).append("/").append(deadlyNumber).append('\n');
        result.append("Critical: ").append(getNumberOfWounds(WoundType.CRITICAL_DEPR)).append("/").append(criticalNumber).append('\n');
        result.append("Severe: ").append(getNumberOfWounds(WoundType.CRITICAL)).append("/").append(severeNumber).append('\n');
        result.append("Light: ").append(getNumberOfWounds(WoundType.LETHAL)).append("/").append(lightNumber).append('\n');
        result.append("Scratch: ").append(getNumberOfWounds(WoundType.NONLETHAL)).append("/").append(scratchNumber).append('\n');
        return result.toString();
    }

    public List<Integer> getListOfMaximums() {
        List<Integer> result = new ArrayList<>();
        result.add(deadlyNumber);
        result.add(criticalNumber);
        result.add(severeNumber);
        result.add(lightNumber);
        result.add(scratchNumber);
        return result;
    }

    public void changeWoundsNumber(WoundType woundType, int number) {


        int max = getNumberOfWounds(woundType);
        for (int i = 0; i <= max; i++) {
            woundList.remove(woundType);
        }
        for (int i = 0; i < number; i++) {
            woundList.add(woundType);
        }

    }

    public void setInitialWoundsNumber(WoundType woundType, int number) {
        switch (woundType) {
            case NONLETHAL:
                this.scratchNumber = number;
                return;
            case LETHAL:
                this.lightNumber = number;
                return;
            case CRITICAL:
                this.severeNumber = number;
                return;
            case CRITICAL_DEPR:
                this.criticalNumber = number;
                return;
            case DEADLY:
                this.deadlyNumber = number;
        }

    }

    public Message toNiceMessage() {
        Message result = new Message();
        int maximum = healthPool;
        int crits = getNumberOfWounds(WoundType.CRITICAL);
        int lethals = getNumberOfWounds(WoundType.LETHAL);
        int nonlethals = getNumberOfWounds(WoundType.NONLETHAL);
        for (int i = 0; i < healthPool; i++) {
            if (crits > 0) {
                result.addComponent(new MessageComponent("⊠", ChatColor.DARK_RED));
                crits--;
            } else if (lethals > 0) {
                result.addComponent(new MessageComponent("⊟", ChatColor.RED));
                lethals--;
            } else if (nonlethals > 0) {
                result.addComponent(new MessageComponent("⊡", ChatColor.YELLOW));
                nonlethals--;
            } else {
                result.addComponent(new MessageComponent("□", ChatColor.STORY));
            }
        }

        return result;
    }


    public String toNiceLine() {
        StringBuilder result = new StringBuilder();
        int maximum = healthPool;
        int crits = getNumberOfWounds(WoundType.CRITICAL);
        int lethals = getNumberOfWounds(WoundType.LETHAL);
        int nonlethals = getNumberOfWounds(WoundType.NONLETHAL);
        for (int i = 0; i < healthPool; i++) {
            if (crits > 0) {
                result.append("⊠");
                crits--;
            } else if (lethals > 0) {
                result.append("⊟");
                lethals--;
            } else if (nonlethals > 0) {
                result.append("⊡");
                nonlethals--;
            } else {
                result.append("□");
            }
        }

        return result.toString();
    }

    public String toNiceString(String name) {
        StringBuilder result = new StringBuilder();
        int maximum = healthPool;
        int crits = getNumberOfWounds(WoundType.CRITICAL);
        int lethals = getNumberOfWounds(WoundType.LETHAL);
        int nonlethals = getNumberOfWounds(WoundType.NONLETHAL);
        for (int i = 0; i < healthPool; i++) {
            if (crits > 0){
                result.append("§4⊠");
                crits--;
            } else if (lethals > 0) {
                result.append("§c⊟");
                lethals--;
            } else if (nonlethals > 0) {
                result.append("§e⊡");
                nonlethals--;
            } else {
                result.append("§6□");
            }
        }

        result.append("\n");

        ArrayList<String> injList = DataHolder.inst().getModifiers().getInjuriesList(name);

        for (String inj : injList) {
            result.append(inj).append("\n");
        }

        return result.toString().trim();
    }

    public int getHealthPool() {
        return healthPool;
    }
}
