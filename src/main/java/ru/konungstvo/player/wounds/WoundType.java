package ru.konungstvo.player.wounds;

public enum WoundType {
    DEADLY(4, "смертельная рана"),
    CRITICAL_DEPR(3, "критическая рана"),
    CRITICAL(2, "критический урон"),
    LETHAL(1, "летальный урон"),
    NONLETHAL(0, "нелетальный урон");

    private int mod;
    private String desc;
    WoundType(int mod, String desc) {
        this.mod = mod;
        this.desc = desc;
    }

    public int getMod() {
        if (mod < 0) {
            return 0;
        }
        return -mod;
    }

    public int getInt() {
        return mod;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static String getDescFromDamage(int damage) {
        if (damage < 0) return null;
        if (damage <= 3) return NONLETHAL.getDesc();
        if (damage <= 6) return LETHAL.getDesc();
        if (damage <= 9) return CRITICAL.getDesc();
        if (damage <= 12) return CRITICAL_DEPR.getDesc();
        return DEADLY.getDesc();
    }
}
